﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.IO;
using Subgurim.Controles;

namespace Tracking
{
    public partial class Vehiclestoppagereport : System.Web.UI.Page
    {

        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";

        DBClass db = new DBClass();
 
 
        int time = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                    ddlMapTOVehicle.DataBind();
                    //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                    //ddlMapTOVehicle.DataBind();
                }
                else
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                    ddlMapTOVehicle.DataBind();
                }
            }
        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false; txtdate.Enabled = false;
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            disable();
            string fdate = txtdate.Text + " " + "12:00 AM";
            string tdate = txtdate.Text + " " + "11:59 PM";
            DataTable dt = db.getdevicesid(ddlMapTOVehicle.SelectedValue.ToString());

            DateTime dhdh = Convert.ToDateTime(fdate);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {

                //Binddata(fdate, tdate, dt.Rows[0][0].ToString());
                Fuzio_Bindstopdata(fdate, tdate, dt.Rows[0][0].ToString(), "Tracking");
            }
            else
            {
                //Binddataold(fdate, tdate, dt.Rows[0][0].ToString());
                Fuzio_Bindstopdata(fdate, tdate, dt.Rows[0][0].ToString(), "OldTrack");
            }
        }
        public void Binddata(string fdate, string tdate, string did)
        {
            DataTable dt = new DataTable();
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("location", typeof(string));
            dt.Columns.Add("ftime", typeof(string));
            dt.Columns.Add("ttime", typeof(string));
            dt.Columns.Add("duration", typeof(string));
            int first = 0, last = 1;
            string stradd = "";
            DateTime ftime = DateTime.Now;
            DateTime ttime;
            DataTable dt2 = db.igstondetails(fdate, tdate, did);
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                if (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) <= 0)
                {
                    if (first == 0)
                    {
                        string latitude = dt2.Rows[i][0].ToString();
                        string longitude = dt2.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            stradd = formatted_address;
                        }

                        ftime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        first++; last = 0;
                    }
                    else if (Convert.ToDateTime(tdate) == Convert.ToDateTime(dt2.Rows[i][3].ToString()))
                    {
                        if (last == 0)
                        {
                            ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            DataTable dtigst = db.totaligstoff(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            dr["location"] = stradd;
                            dr["ftime"] = ftime.ToShortTimeString();
                            dr["ttime"] = ttime.ToShortTimeString();
                            dr["duration"] = str.Substring(0, 5);
                            first = 0; last++;
                            dt.Rows.Add(dr);
                        }
                    }
                }
                else if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 1)
                {
                    if (last == 0)
                    {
                        ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        DataTable dtigst = db.totaligstoff(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                        TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                        string str = ts.ToString();
                        dr["location"] = stradd;
                        dr["ftime"] = ftime.ToShortTimeString();
                        dr["ttime"] = ttime.ToShortTimeString();
                        dr["duration"] = str.Substring(0, 5);
                        first = 0; last++;
                        dt.Rows.Add(dr);
                    }
                }
            }
            Binddataidel(fdate, tdate, did);
            if (dt.Rows.Count != 0)
            {
                btndownload.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                DateTime st1 = Convert.ToDateTime(txtdate.Text);
                DateTime st2 = DateTime.Now;
                TimeSpan ts1;
                TimeSpan ts2;
                string tt = "";
                if (st1.ToShortDateString() == st2.ToShortDateString())
                {
                    ts1 = new TimeSpan(st2.Hour, st2.Minute, st2.Second);
                    ts2 = new TimeSpan(st1.Hour, st1.Minute, st1.Second);
                    tt = (ts1 - ts2).ToString();
                }
                else if (st1 < st2)
                {
                    tt = "24:00";
                }

                lblnoact.Text = " Vehicle Stoped " + ":  " + tt.Substring(0, 2) + " Hr " + tt.Substring(3, 2) + " Min ";
            }
            gvreport.DataSource = dt;
            gvreport.DataBind();

        }
        public void Binddataidel(string fdate, string tdate, string did)
        {
            DataTable dt12 = new DataTable();
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt12.Columns.Add("location1", typeof(string));
            dt12.Columns.Add("ftime1", typeof(string));
            dt12.Columns.Add("ttime1", typeof(string));
            dt12.Columns.Add("duration1", typeof(string));
            int first = 0, last = 1;
            string stradd = "";
            DateTime ftime = DateTime.Now;
            DateTime ttime;
            DataTable dt2 = db.igstondetails(fdate, tdate, did);
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt12.NewRow();
                if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) <= 1)
                {
                    if (first == 0)
                    {

                        string latitude = dt2.Rows[i][0].ToString();
                        string longitude = dt2.Rows[i][1].ToString();


                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            stradd = formatted_address;
                        }
                        ftime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        first++; last = 0;
                    }
                    else if (Convert.ToDateTime(tdate) == Convert.ToDateTime(dt2.Rows[i][3].ToString()))
                    {
                        if (last == 0)
                        {
                            ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            DataTable dtigst = db.totalidealton(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            int kk = Convert.ToInt32(dtigst.Rows[0][0].ToString());
                            if (kk > 5)
                            {
                                TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                                string str = ts.ToString();
                                dr["location1"] = stradd;
                                dr["ftime1"] = ftime.ToShortTimeString();
                                dr["ttime1"] = ttime.ToShortTimeString();
                                dr["duration1"] = str.Substring(0, 5);
                                first = 0; last++;
                                dt12.Rows.Add(dr);
                            }
                            else
                            {
                                first = 0; last++;
                                ftime = DateTime.Now;

                            }

                        }
                    }
                }
                else if ((dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 3) || (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) == 0))
                {
                    if (last == 0)
                    {
                        ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        DataTable dtigst = db.totalidealton(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                        int kk = Convert.ToInt32(dtigst.Rows[0][0].ToString());
                        if (kk > 5)
                        {
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            dr["location1"] = stradd;
                            dr["ftime1"] = ftime.ToShortTimeString();
                            dr["ttime1"] = ttime.ToShortTimeString();
                            dr["duration1"] = str.Substring(0, 5);
                            first = 0; last++;
                            dt12.Rows.Add(dr);
                        }
                        else
                        {
                            first = 0; last++;
                            ftime = DateTime.Now;

                        }

                    }
                }

            }

            gvideal.DataSource = dt12;
            gvideal.DataBind();

        }

        public void Fuzio_Bindstopdata(string fdate, string tdate, string did, string tablename)
        {
            DataTable dt = new DataTable();
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("location1", typeof(string));
            dt.Columns.Add("ftime1", typeof(string));
            dt.Columns.Add("ttime1", typeof(string));
            dt.Columns.Add("duration1", typeof(string));
            int igstatus = 0;
            string stradd = "";
            string stoploc = "";
            string prevtime = "";
            string fromtime = "";
            string totime = "";
            string duration = "";
            DataTable bygtim = db.fuzio_getbygtim(ddlMapTOVehicle.SelectedValue.ToString(), txtdate.Text, tablename);
            int previgst = 1;
            fromtime = "";

            string diff = string.Empty;
            DateTime dateFirst, dateSecond;

            for (int i = 0; i < bygtim.Rows.Count; i++)
            {
                
                if(/*bygtim.Rows[i][4].ToString() == "0" && */ Convert.ToInt32(bygtim.Rows[i][3].ToString()) <= 0)
                {
                    if(stoploc == "")
                    {
                        

                        string latitude = bygtim.Rows[i][0].ToString();
                        string longitude = bygtim.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            stradd = formatted_address;
                        }
                        stoploc = stradd;
                        fromtime = bygtim.Rows[i][2].ToString();
                        //dr["location1"] = stradd;
                       // dr["ftime1"] = fromtime;
                    }

                    igstatus = 0;
                }
                else if(bygtim.Rows[i][4].ToString() == "1" && Convert.ToInt32(bygtim.Rows[i][3].ToString()) > 0)
                {
                    if(previgst == 0)
                    {
                        DataRow dr = dt.NewRow();
                        totime = prevtime;
                        stoploc = "";

                        string sDateFrom = fromtime;
                        string sDateTo = totime;
                        if (DateTime.TryParse(sDateFrom, out dateFirst) && DateTime.TryParse(sDateTo, out dateSecond))
                        {
                            TimeSpan timespan = dateSecond - dateFirst;
                            int hour = timespan.Hours;
                            int mins = timespan.Minutes;
                            int secs = timespan.Seconds;
                            diff = hour.ToString("00") + ":" + mins.ToString("00");
                        }



                        duration = diff;

                        dr["location1"] = stradd;
                        dr["ftime1"] = fromtime;
                        dr["ttime1"] = totime;
                        dr["duration1"] = duration.ToString();
                        dt.Rows.Add(dr);
                    }
                    igstatus = 1;
                }










                if(i == bygtim.Rows.Count-1)
                {
                    if (bygtim.Rows[i][4].ToString() == "0" && Convert.ToInt32(bygtim.Rows[i][3].ToString()) <= 0)
                    {
                        DataRow dr = dt.NewRow();
                        totime = bygtim.Rows[i][2].ToString();
                        stoploc = "";

                        string sDateFrom = fromtime;
                        string sDateTo = totime;
                        if (DateTime.TryParse(sDateFrom, out dateFirst) && DateTime.TryParse(sDateTo, out dateSecond))
                        {
                            TimeSpan timespan = dateSecond - dateFirst;
                            int hour = timespan.Hours;
                            int mins = timespan.Minutes;
                            int secs = timespan.Seconds;
                            diff = hour.ToString("00") + ":" + mins.ToString("00");
                        }



                        duration = diff;

                        dr["location1"] = stradd;
                        dr["ftime1"] = fromtime;
                        dr["ttime1"] = totime;
                        dr["duration1"] = duration.ToString();
                        dt.Rows.Add(dr);
                    }
                    igstatus = 1;
                }
                previgst = igstatus;
                prevtime = bygtim.Rows[i][2].ToString();
            }
            //gvideal.DataSource = dt;
            //gvideal.DataBind();
            gvreport.DataSource = dt;
            gvreport.DataBind();
        }

        public void Binddataold(string fdate, string tdate, string did)
        {
            DataTable dt = new DataTable();
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("location", typeof(string));
            dt.Columns.Add("ftime", typeof(string));
            dt.Columns.Add("ttime", typeof(string));
            dt.Columns.Add("duration", typeof(string));
            int first = 0, last = 1;
            string stradd = "";
            DateTime ftime = DateTime.Now;
            DateTime ttime;
            DataTable dt2 = db.igstondetailsold(fdate, tdate, did);
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                if (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) <= 0)
                {
                    if (first == 0)
                    {

                        string latitude = dt2.Rows[i][0].ToString();
                        string longitude = dt2.Rows[i][1].ToString();

                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            stradd = formatted_address;
                        }
                        ftime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        first++; last = 0;
                    }
                    else if (Convert.ToDateTime(tdate) == Convert.ToDateTime(dt2.Rows[i][3].ToString()))
                    {
                        if (last == 0)
                        {
                            ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            DataTable dtigst = db.totaligstoff(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            dr["location"] = stradd;
                            dr["ftime"] = ftime.ToShortTimeString();
                            dr["ttime"] = ttime.ToShortTimeString();
                            dr["duration"] = str.Substring(0, 5);
                            first = 0; last++;
                            dt.Rows.Add(dr);
                        }
                    }
                }
                else if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 1)
                {
                    if (last == 0)
                    {
                        ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        DataTable dtigst = db.totaligstoff(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                        TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                        string str = ts.ToString();
                        dr["location"] = stradd;
                        dr["ftime"] = ftime.ToShortTimeString();
                        dr["ttime"] = ttime.ToShortTimeString();
                        dr["duration"] = str.Substring(0, 5);
                        first = 0; last++;
                        dt.Rows.Add(dr);
                    }
                }
            }
            Binddataidelold(fdate, tdate, did);
            if (dt.Rows.Count != 0)
            {
                btndownload.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                DateTime st1 = Convert.ToDateTime(txtdate.Text);
                DateTime st2 = DateTime.Now;
                TimeSpan ts1;
                TimeSpan ts2;
                string tt = "";
                if (st1.ToShortDateString() == st2.ToShortDateString())
                {
                    ts1 = new TimeSpan(st2.Hour, st2.Minute, st2.Second);
                    ts2 = new TimeSpan(st1.Hour, st1.Minute, st1.Second);
                    tt = (ts1 - ts2).ToString();
                }
                else if (st1 < st2)
                {
                    tt = "24:00";
                }

                lblnoact.Text = " Vehicle Stoped " + ":  " + tt.Substring(0, 2) + " Hr " + tt.Substring(3, 2) + " Min ";
            }
            gvreport.DataSource = dt;
            gvreport.DataBind();

        }
        public void Binddataidelold(string fdate, string tdate, string did)
        {
            DataTable dt12 = new DataTable();
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt12.Columns.Add("location1", typeof(string));
            dt12.Columns.Add("ftime1", typeof(string));
            dt12.Columns.Add("ttime1", typeof(string));
            dt12.Columns.Add("duration1", typeof(string));
            int first = 0, last = 1;
            string stradd = "";
            DateTime ftime = DateTime.Now;
            DateTime ttime;
            DataTable dt2 = db.igstondetails(fdate, tdate, did);
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt12.NewRow();
                if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) <= 1)
                {
                    if (first == 0)
                    {
                        /*gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt2.Rows[i][0]), Convert.ToDouble(dt2.Rows[i][1]));
                        GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        GMap gMap1 = new GMap();
                        StringBuilder sb1 = new StringBuilder();
                        gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        if ((null != geocode) && geocode.valid)
                        {
                            sb1.AppendFormat(geocode.Placemark.address);
                        }
                        stradd = sb1.ToString(); */

                        string latitude = dt2.Rows[i][0].ToString();
                        string longitude = dt2.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            stradd = formatted_address;
                        }

                        ftime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        first++; last = 0;
                    }
                    else if (Convert.ToDateTime(tdate) == Convert.ToDateTime(dt2.Rows[i][3].ToString()))
                    {
                        if (last == 0)
                        {
                            ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            DataTable dtigst = db.totalidealtonold(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            int kk = Convert.ToInt32(dtigst.Rows[0][0].ToString());
                            if (kk > 5)
                            {
                                TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                                string str = ts.ToString();
                                dr["location1"] = stradd;
                                dr["ftime1"] = ftime.ToShortTimeString();
                                dr["ttime1"] = ttime.ToShortTimeString();
                                dr["duration1"] = str.Substring(0, 5);
                                first = 0; last++;
                                dt12.Rows.Add(dr);
                            }
                            else
                            {
                                first = 0; last++;
                                ftime = DateTime.Now;

                            }

                        }
                    }
                }
                else if ((dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 3) || (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) == 0))
                {
                    if (last == 0)
                    {
                        ttime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                        DataTable dtigst = db.totalidealtonold(ftime.ToString(), ttime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                        int kk = Convert.ToInt32(dtigst.Rows[0][0].ToString());
                        if (kk > 5)
                        {
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            dr["location1"] = stradd;
                            dr["ftime1"] = ftime.ToShortTimeString();
                            dr["ttime1"] = ttime.ToShortTimeString();
                            dr["duration1"] = str.Substring(0, 5);
                            first = 0; last++;
                            dt12.Rows.Add(dr);
                        }
                        else
                        {
                            first = 0; last++;
                            ftime = DateTime.Now;

                        }

                    }
                }

            }

            gvideal.DataSource = dt12;
            gvideal.DataBind();

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            DateTime dt = Convert.ToDateTime(txtdate.Text);
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Substring(2);
            str1 = str1.Replace(" ", "");
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VSR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gvreport);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
            // TableCell cell2 = new TableCell();
            cell1.Controls.Add(gvideal);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
            tb.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbon = (Label)e.Row.FindControl("lblduration") as Label;
                string tt = txtdate.Text + " " + lbon.Text;
                DateTime ttt = new DateTime();
                ttt = Convert.ToDateTime(tt);
                TimeSpan tsp = ttt - Convert.ToDateTime(txtdate.Text + " 12:00 AM");
                int minit = Convert.ToInt32(tsp.TotalMinutes.ToString());
                time += minit;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                //string fdt = txtdate.Text + " " + "12:00 AM";
                //string ldt = txtdate.Text + " " + "11:59 PM";
                //DataTable dt = db.totaligstoff(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());

                TimeSpan ts = new TimeSpan(0, time, 0);
                Label lbon1 = (Label)e.Row.FindControl("lblftotal");
                string str = ts.ToString();
                lbon1.Text = str.Substring(0, 5);
            }
        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.ColumnSpan = 1;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                //HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.ColumnSpan = 3;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                DateTime hdt = Convert.ToDateTime(txtdate.Text);
                HeaderCell1.Text = "Date  :" + hdt.ToString("dd-MM-yyyy") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Driver Name :" + DriDet.Rows[0][0].ToString();
                HeaderCell1.ColumnSpan = 1;
                HeaderCell1.Font.Bold = true;
                //HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);

                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                HeaderCell4.ColumnSpan = 4;
                HeaderCell4.Font.Bold = true;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);
            }
        }

        protected void gvideal_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical Idling Report ";
                HeaderCell.ColumnSpan = 4;
                HeaderCell.Font.Bold = true;
                HeaderCell.Font.Italic = true;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                gvideal.Controls[0].Controls.AddAt(0, HeaderGridRow);
            }

        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

namespace Tracking
{
    public partial class TrackManager : System.Web.UI.MasterPage
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
 
 
                    DBClass db = new DBClass();
 
 
                    DataTable getcount = db.GetVehiclescount(Session["cUser_id"].ToString());
                    if (getcount.Rows.Count > 0)
                        lblenable.Text = "Number of Devices Enabled :" + " " + (getcount.Rows[0][0].ToString());
                    DataTable getdesablecount = db.GetdesabledVehiclescount(Session["cUser_id"].ToString());
                    if (getdesablecount.Rows.Count > 0)
                        lbldisble.Text = "Number of Devices Disabled :" + " " + (getdesablecount.Rows[0][0].ToString());


                    lblpage.Text = Session["page"].ToString();
                    lblpage2.Text = Session["page1"].ToString();
                    lblusername3.Text = "Branch Name: " + "" + Session["Branchname"].ToString();
                    lblusername.Text = Session["UserName"].ToString();
                    DataTable dt = db.Set_Image_Logo(Session["UserID"].ToString());
                    if (dt.Rows[0][0].ToString() != "" && dt.Rows[0][1].ToString() != "")
                    {
                        Image1.ImageUrl = "Handler.ashx?EmpID=" + Session["UserID"].ToString();
                        Image1.Width = Convert.ToInt32(dt.Rows[0][1].ToString());
                    }
                    else
                    {
                        Image1.ImageUrl = "~/Images/ESL1.JPG";
                        Image1.Width = 50;
                    }
                    if (Session["Logotext"].ToString() != "")
                    {
                        lblhead.Text = Session["Logotext"].ToString();
                    }
                    string name = Page.ToString();
                    if (name == "ASP.managerhome_aspx")
                    {
                        LinkButton1.Visible = false;
                    }
                    imgst.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    imgidl.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    imgmv.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    binddata();
                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            }
            else LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
        }

        public void binddata()
        {
            if (Session["UserID"] != null && Session["UserRole"].ToString() == "3")
            {
                Vehicle_Bind();
                // check_General_Alerts();
            }
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            // binddata();
            LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
        }
        //public void check_General_Alerts()
        //{
        //    DBClass db = new DBClass();
        //    DataTable dt = new DataTable();
        //    if (Session["UserRole"].ToString() == "3")
        //    {
        //        dt = db.Check_Genaral_Alerts(Session["cUser_id"].ToString());
        //        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
        //        //dt = db.Check_Genaral_Alerts1(Session["UserID"].ToString(), CUsers);
        //    }
        //    else
        //    {
        //        dt = db.Check_Genaral_Alerts(Session["UserID"].ToString());
        //    }
        //    if (dt.Rows.Count > 0)
        //    {
        //        lblmalerts.Visible = true;
        //        StringBuilder sba = new StringBuilder();
        //        sba.Append("You have ALERT-click The ");
        //        sba.Append("<span style=\"color:Red;\">");
        //        sba.Append("RED Button ");
        //        sba.Append("</span>");
        //        sba.Append("Corresponding to The Vehicle No");
        //        lblmalerts.Text = sba.ToString();
        //    }
        //}
        public void Vehicle_Bind()
        {
 
 
            DBClass db = new DBClass();
 
 
            lbloffalerts.Text = "";
            lblalerts2.Text = "";
            //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
            //DataTable stat = db.Get_CAminVeh(Session["UserID"].ToString(), CUsers);
            DataTable stat = db.Select_GetVehicles(Session["cUser_id"].ToString());
            if (stat.Columns.Count > 0)
            {
                for (int i = 0; i < stat.Rows.Count; i++)
                {
                    string VN = Convert.ToString(stat.Rows[i][0].ToString());
                    DataTable VS = db.GetVehStat(Session["UserID"].ToString(), VN);
                    if (VS.Rows.Count > 0)
                    {
                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                        StringDT = StringDT.AddMinutes(360);
                        if (sysDT > StringDT)
                        {
                            //stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ?";

                            //commenting tempororry alert in merque
                            //lbloffalerts.Text += alert2(stat.Rows[i]["VehicalNumber"].ToString(), 1);
                        }
                        else //if(sysDT <= StringDT)
                        {
                            //stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ?";
                            //commenting tempororry alert in merque
                            //lbloffalerts.Text += alert2(stat.Rows[i]["VehicalNumber"].ToString(), 0);
                        }
                    }
                    else
                    {
                           DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), VN);
                           if (VSoff.Rows.Count > 0)
                           {
                               DateTime sysDT = DateTime.Now;
                               DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                               StringDT = StringDT.AddMinutes(360);
                               if (sysDT > StringDT)
                               {
                                   //stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ?";
                                   //commenting tempororry alert in merque
                                   //lbloffalerts.Text += alert2(stat.Rows[i]["VehicalNumber"].ToString(), 1);
                               }
                               else //if(sysDT <= StringDT)
                               {
                                   //stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ?";
                                   //commenting tempororry alert in merque
                                   //lbloffalerts.Text += alert2(stat.Rows[i]["VehicalNumber"].ToString(), 0);
                               }
                           }

                    }
                }
            }


        }
        protected string alert2(string Vno, int no)
        {
            StringBuilder builder = new StringBuilder();
            int maxtemp = 0, min1 = 0, delay = 0;
 
 
            DBClass db = new DBClass();
 
 
            DataTable VS = db.GetVehiclePresentlocation(Vno, Session["cUser_id"].ToString());
            if (no == 0)
            {
                DataTable gadt = db.vehicledetailsalerts(Vno);
                if (gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][14].ToString() != "")
                {
                    maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    min1 = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    delay = Convert.ToInt32(gadt.Rows[0][14].ToString());
                    int dttemp = 0;
                    if (maxtemp != 0)
                    {
                        if (maxtemp > 0)
                        {
                            dttemp = db.Temp_alert2(Vno, min1.ToString(), maxtemp.ToString());
                        }
                        else if (maxtemp < 0)
                        {
                            maxtemp = maxtemp * -1 + 1000;
                            dttemp = db.Temp_alert3(Vno, min1.ToString(), maxtemp.ToString());
                        }
                        if (delay <= dttemp)
                        {
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("||");
                            builder.Append("</span>");
                            builder.Append(" " + Vno + "- ");
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("Temperature");
                            builder.Append("</span>");
                            builder.Append("> " + gadt.Rows[0][9].ToString() + "°C ");
                        }
                    }
                }
#pragma warning disable CS0219 // The variable 'vid' is assigned but its value is never used
                int vsta = 0, vid = 0, vmng = 0, hours, min;
#pragma warning restore CS0219 // The variable 'vid' is assigned but its value is never used
                if (VS.Rows[0][8].ToString() == "1" && VS.Rows[0][2].ToString() != "0")
                {
                    if (gadt.Rows[0][12].ToString() != "" && gadt.Rows[0][12].ToString() != "0")
                    {
                        DataTable pmdt = db.GetPreviMNG(VS.Rows[0][11].ToString(), Session["cUser_id"].ToString());
                        vmng = Convert.ToInt32(gadt.Rows[0][12].ToString());
                        hours = vmng / 60;
                        min = vmng % 60;
                        DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                        if (pmdt.Rows.Count != 0)
                        {
                            DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                            TimeSpan ts = dtmng - dtmng1;
                            if (vmng < ts.TotalMinutes)
                            {
                                //alerttext3 = lbla2.Text+ Vno + "- Moving > "+hours.ToString()+":"+min.ToString()+" Hr ";
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Moving");
                                builder.Append("</span>");
                                builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                            }
                        }
                        else
                        {
                            DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                            TimeSpan ts = dtmng - dtmng1;
                            if (vmng < ts.TotalMinutes)
                            {
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Moving");
                                builder.Append("</span>");
                                builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                            }
                        }
                    }
                }
                else
                {
                    string sft = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][15].ToString();
                    string stt = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][16].ToString();
                    if (gadt.Rows[0][10].ToString() != "" && gadt.Rows[0][10].ToString() != "0" && Convert.ToDateTime(sft) < DateTime.Now && Convert.ToDateTime(stt) > DateTime.Now)
                    {
                        DataTable pdt = db.GetPreviIGST11(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                        vsta = Convert.ToInt32(gadt.Rows[0][10].ToString());
                        DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                        hours = vsta / 60;
                        min = vsta % 60;
                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            if (dtst1 > Convert.ToDateTime(sft))
                            {
                                TimeSpan ts = dtst - dtst1;
                                if (vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                            else
                            {
                                DataTable dtchk = db.firstigston(Convert.ToDateTime(sft).ToString(), DateTime.Now.ToString(), VS.Rows[0][11].ToString());
                                TimeSpan ts = DateTime.Now - Convert.ToDateTime(sft);
                                if (dtchk.Rows.Count == 0 && vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                        }
                        else
                        {
                            DataTable pdt1 = db.GetPreviIGST22(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            if (pdt1.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                if (vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                        }
                    }
                }
                string dnt = DateTime.Now.ToShortDateString();
                DateTime ftime = Convert.ToDateTime(dnt + " 12:00 AM");
                DateTime ttime = Convert.ToDateTime(dnt + " 11:59 PM");
                DataTable Towesdt = db.Towed_Alerts(Vno, "TOWED", ftime.ToString(), ttime.ToString());
                if (Towesdt.Rows.Count >= 8)
                {
                    //DateTime dnt1 = Convert.ToDateTime(Towesdt.Rows[0][0].ToString());
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("||");
                    builder.Append("</span>");
                    builder.Append(" " + Vno + "- ");
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("Towed At");
                    builder.Append("</span>");
                    // builder.Append(": " + dnt1.ToShortTimeString());
                }
            }
            else if (no == 1)
            {
                //DateTime spdt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                //DateTime pdt = DateTime.Now;
                //TimeSpan ts = pdt - spdt;
                //if (ts.TotalHours >= 6)
                //{
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("||");
                    builder.Append("</span>");
                    builder.Append(" " + Vno + "- ");
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("Offline");
                    builder.Append("</span>");
                    builder.Append(" > 6Hr ");
                //}
            }
            return builder.ToString();
        }

        protected void Btndalerts_Click(object sender, EventArgs e)
        {
            if ((lblalerts2.Text != "") || (lbloffalerts.Text != ""))
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                string stralert = "";
                string stralert1 = "";

                if (lblalerts2.Text != "")
                {
                    stralert = "Daily Live Alerts ,";
                }
                if (lbloffalerts.Text != "")
                {
                    stralert1 = ",,, Offline More than 6 Hr ,";
                }
                stralert += lblalerts2.Text;
                string stralert2 = stralert.Replace("||", ",");
                stralert2 = stralert2.Replace("<span style=\"color:Red;\">", "");
                stralert2 = stralert2.Replace("</span>", "");

                stralert1 += lbloffalerts.Text;
                string stralert3 = stralert1.Replace("||", ",");
                stralert3 = stralert3.Replace("<span style=\"color:Red;\">", "");
                stralert3 = stralert3.Replace("</span>", "");

                stralert2 = stralert2 + stralert3;

                string[] results = stralert2.Split(new[] { ',' });
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", results) + "'.split(','));", true);

            }
        }

        protected void bntnLogOut_Click1(object sender, EventArgs e)
        {
            //Session["UserRole"] = null;
            // Session["UserID"] = null;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Session["timerc"] = null;
            Session["Cusername"] = null;
            Session["cUser_id"] = null;
            Session["Branchname"] = null;
            //Session["Logotext"] = null;
            //Session["APIKey"] = null;
            Response.Redirect("ManagerHome1.aspx");
        }

    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Tracking
{
    public partial class report2 : System.Web.UI.Page
    {
        public int igst1 = 0;
        public int idle1 = 0;
        public int sta1 = 0;
        public double tkmr1 = 0;
        public int Acstatus = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null)
                {
 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }          

        }
       
        double getkm(string PULSE_CTR, string date, string vn)
        {
 
 
            DBClass db1 = new DBClass();
 
 
            string vid = ddlMapTOVehicle.SelectedValue.ToString();
            string vno = vn;
            double tkmr = 0;
            DataTable vdt = db1.DriverDetails(vid);//, Session["UserId"].ToString());
            double KM_Pulse = Convert.ToDouble(vdt.Rows[0][4].ToString());
            double PCTR = Convert.ToDouble(PULSE_CTR);
            double KMR = PCTR / KM_Pulse;
            double TOTKMRU = 0;
            if (PCTR != 100000)
            {
                DataTable TOTKMR = db1.getTOTKMR1(vno, date);
                TOTKMRU = Convert.ToDouble(TOTKMR.Rows[0][0].ToString());
                TOTKMRU = TOTKMRU / KM_Pulse;
            }
            KMR = KMR + TOTKMRU;

            DataTable NwEMR = db1.SelectNEMR(Session["UserID"].ToString(), vid);
            if (NwEMR.Rows.Count == 0)
            {
                //double nw = 0;
                DataTable P = db1.getPFE(vid);
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                KMR = KMR + nw;

                tkmr = KMR;
                //return KMR;
                //lblTotKmRun.Text = KMR.ToString();
            }
            else if (NwEMR.Rows.Count != 0)
            {
                //DataTable P = db1.getPFE(vid);
                //double nw1 = Convert.ToDouble(P.Rows[0][0].ToString());
                double nw = Convert.ToDouble(NwEMR.Rows[0][0].ToString());
                KMR = KMR + nw; //+ nw1;
                tkmr = KMR;
                // return KMR;
                //lblTotKmRun.Text = KMR.ToString();
            }
            return tkmr;
        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false; date.Enabled = false;
        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            string mdate = date.Text + " 12:00 AM";
            DateTime dhdh = Convert.ToDateTime(mdate);


            string cmdate = "9/24/2018" + " 12:00 AM";
            DateTime cdhdh = Convert.ToDateTime(cmdate);

            
            string cmdatenew = "12/19/2018" + " 12:00 AM";
            DateTime cdhdhnew = Convert.ToDateTime(cmdatenew);




            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;

            if(dhdh > cdhdhnew)
            {
                Fuzio_bind_new("Fuzio_TKMRH");
            }
            else
            {

                if (dhdh > cdhdh)
                {
                    //binddatanew();
                    Fuzio_bind("Tracking");

                }
                else
                {
                    Fuzio_bind("OldTrack");
                }
            }


        }

        public void binddatanew()
        {
            disable();
            string d = Session["ParentUser"].ToString();
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable dt = db1.getRhours();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Fhours", typeof(string));
            dt1.Columns.Add("Thours", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Columns.Add("Ac", typeof(string));
            string fdate12 = date.Text + " 12:00 AM";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 3 || i == 6 || i == 9 || i == 12 || i == 15 || i == 18 || i == 21) //if (i == 12)
                {
                    DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = "";
                    dr1["Thours"] = "";
                    dr1["igst"] = "";
                    dr1["idle"] = "";
                    dr1["Stat"] = "";
                    dr1["TKMR"] = "";
                    dr1["AC"] = "";
                    dt1.Rows.Add(dr1);
                }
                DataRow dr = dt1.NewRow();
                string fdate = date.Text + " " + dt.Rows[i][0];
                string tdate = date.Text + " " + dt.Rows[i][1];
                dr["Fhours"] = date.Text + " " + dt.Rows[i][0];
                dr["Thours"] = date.Text + " " + dt.Rows[i][1];

                //for (int j = 0; j < dt.Rows.Count; j++)
                //{
                DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                int igst = 0; int stat = 0; int idel = 0; double fkm = 0; double tkm = 0; double TKMR1 = 0; double Acstatus = 0;
                for (int k = 0; k < dt2.Rows.Count; k++)
                {
                    if (dt2.Rows[k][4].ToString() == "0")
                    {
                        stat++;

                    }
                    if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                    {
                        igst++;
                    }
                    if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                    {
                        idel++;
                    }
                }
                if (dt2.Rows.Count != 0)
                {
                    DataTable dt12 = db1.Select_TotalRunkm(ddlMapTOVehicle.SelectedValue.ToString(), fdate12, tdate);

                    for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                    {
                        TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                    }
                    if (dt12.Rows.Count != 0)
                    {
                        fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                    }
                    TKMR1 = (double)Math.Round(TKMR1, 2);
                    DataTable acstat = db1.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
                    if (acstat.Rows[0][1].ToString() == "On")
                    {

                        DataTable dtfs1 = db1.getacontime1(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                        if (dtfs1.Rows.Count != 0)
                        {
                            int m = Convert.ToInt32(dtfs1.Rows[0][0].ToString());

                            Acstatus = m; 
                        }
                    }
                    else
                    {
                        Acstatus =0;
                    }
                }
                dr["igst"] = igst.ToString();
                dr["idle"] = idel.ToString();
                dr["Stat"] = stat.ToString();
                dr["TKMR"] = TKMR1.ToString();
                dr["AC"] = Acstatus.ToString();
                dt1.Rows.Add(dr);
            }
            //}
            gv.DataSource = dt1;
            gv.DataBind();
        }

        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }


        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Items.Clear();
            Response.Redirect(Request.RawUrl);
        }

        public void binddataold()
        {
            disable();
            string d = Session["ParentUser"].ToString();
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable dt = db1.getRhours();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Fhours", typeof(string));
            dt1.Columns.Add("Thours", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Columns.Add("AC", typeof(string));
            string fdate12 = date.Text + " 12:00 AM";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 3 || i == 6 || i == 9 || i == 12 || i == 15 || i == 18 || i == 21) //if (i == 12)
                {
                    DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = "";
                    dr1["Thours"] = "";
                    dr1["igst"] = "";
                    dr1["idle"] = "";
                    dr1["Stat"] = "";
                    dr1["TKMR"] = "";
                    dr1["AC"] = "";
                    dt1.Rows.Add(dr1);
                }
                DataRow dr = dt1.NewRow();
                string fdate = date.Text + " " + dt.Rows[i][0];
                string tdate = date.Text + " " + dt.Rows[i][1];
                dr["Fhours"] = date.Text + " " + dt.Rows[i][0];
                dr["Thours"] = date.Text + " " + dt.Rows[i][1];

                //for (int j = 0; j < dt.Rows.Count; j++)
                //{
                DataTable dt2 = db1.searchold(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                int igst = 0; int stat = 0; int idel = 0; double fkm = 0; double tkm = 0; double TKMR1 = 0; double Acstatus = 0;
                for (int k = 0; k < dt2.Rows.Count; k++)
                {
                    if (dt2.Rows[k][4].ToString() == "0")
                    {
                        stat++;

                    }
                    if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                    {
                        igst++;
                    }
                    if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                    {
                        idel++;
                    }
                }
                if (dt2.Rows.Count != 0)
                {
                    DataTable dt12 = db1.Select_TotalRunkmold(ddlMapTOVehicle.SelectedValue.ToString(), fdate12, tdate);

                    for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                    {
                        TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                    }
                    if (dt12.Rows.Count != 0)
                    {
                        fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                    }
                    TKMR1 = (double)Math.Round(TKMR1, 2);

                    DataTable acstat = db1.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
                    if (acstat.Rows[0][1].ToString() == "On")
                    {

                        DataTable dtfs1 = db1.getacontime1old(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                        if (dtfs1.Rows.Count != 0)
                        {
                            int m = Convert.ToInt32(dtfs1.Rows[0][0].ToString());

                            Acstatus = m;
                        }
                    }
                    else
                    {
                        Acstatus = 0;
                    }
                }
                dr["igst"] = igst.ToString();
                dr["idle"] = idel.ToString();
                dr["Stat"] = stat.ToString();
                dr["TKMR"] = TKMR1.ToString();
                dr["AC"] = Acstatus.ToString();
                dt1.Rows.Add(dr);
            }
            //}
            gv.DataSource = dt1;
            gv.DataBind();
        }






        public void Fuzio_bind_new(string tablename)
        {
            disable();
            DBClass db1 = new DBClass();

            DataTable acstat = db1.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
            string acstatus = acstat.Rows[0][1].ToString();

              
            string[] hourarray1 = new string[24] { "12 AM", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};
            string[] hourarray2 = new string[24] { "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "12 AM" };

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Fhours", typeof(string));
            dt1.Columns.Add("Thours", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Columns.Add("AC", typeof(string));



            //DataTable bygtim = db1.fuzio_getbygtim(ddlMapTOVehicle.SelectedValue.ToString(), date.Text, tablename);
            DataTable bygtim = db1.fuzio_daysummary(ddlMapTOVehicle.SelectedValue.ToString(), date.Text, tablename);

            Double hourlykmreport = 0;
            String hourlyigstreport = "0";
            String hourlyidlereport = "0";
            String hourlystatreport = "0";
            String hourlyacreport = "0";


            int rowcount = 0;

            for (int i=0; i< bygtim.Rows.Count;i++)
            {
                //assign row values here
                hourlykmreport = Math.Round(Convert.ToDouble(bygtim.Rows[i][4]),2);
                hourlyigstreport = bygtim.Rows[i][7].ToString();
                hourlyidlereport = bygtim.Rows[i][8].ToString();
                hourlystatreport = bygtim.Rows[i][6].ToString();
                hourlyacreport = bygtim.Rows[i][9].ToString();
                DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = hourarray1[rowcount];
                    dr1["Thours"] = hourarray2[rowcount];
                    dr1["igst"] = hourlyigstreport;
                    dr1["idle"] = hourlyidlereport;
                    dr1["Stat"] = hourlystatreport;
                    dr1["TKMR"] = hourlykmreport;
                    dr1["AC"] = hourlyacreport;
                    dt1.Rows.Add(dr1);
                    hourlykmreport = 0;
                    hourlystatreport = "0";
                    hourlyidlereport = "0";
                    hourlyigstreport = "0";
                    hourlyacreport = "0";
                rowcount++;

            }
            dt1.AcceptChanges();
            gv.DataSource = dt1;
            gv.DataBind();

        }

        


        public void Fuzio_bind(string tablename)
        {
            disable();
            DBClass db1 = new DBClass();

            DataTable acstat = db1.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
            string acstatus = acstat.Rows[0][1].ToString();

              
            string[] hourarray1 = new string[24] { "12 AM", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};
            string[] hourarray2 = new string[24] { "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM", "12 AM" };

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Fhours", typeof(string));
            dt1.Columns.Add("Thours", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Columns.Add("AC", typeof(string));



            DataTable bygtim = db1.fuzio_getbygtim(ddlMapTOVehicle.SelectedValue.ToString(), date.Text, tablename);

            double hourlykmreport = 0;
            double hourlyigstreport = 0;
            double hourlyidlereport = 0;
            double hourlystatreport = 0;
            double hourlyacreport = 0;
            int prevhour = 0;
            int currenthour = 0;

            int rowcount = 0;

            for (int i=0,j=1;i< bygtim.Rows.Count-1;i++,j++)
            {
                string tmpTime = bygtim.Rows[i][2].ToString();
                DateTime dateValue;
                DateTime dateValuenew;
                int Timehour = 0;

                if (DateTime.TryParse(tmpTime, out dateValue))
                {
                    Timehour = dateValue.Hour;
                }

                string tmpTimenew = bygtim.Rows[i + 1][3].ToString();
                if (DateTime.TryParse(tmpTimenew, out dateValuenew))
                {
                    int Timehournew = dateValuenew.Hour;
                }


                currenthour = Timehour;


                if (i == 0)
                {
                    prevhour = currenthour;
                }

                if(prevhour == currenthour)
                {
                    hourlykmreport += CalcDistance(Convert.ToDouble(bygtim.Rows[i][0].ToString()), Convert.ToDouble(bygtim.Rows[i][1].ToString()), Convert.ToDouble(bygtim.Rows[j][0].ToString()), Convert.ToDouble(bygtim.Rows[j][1].ToString()));
                    if (bygtim.Rows[i][4].ToString() == "0")
                    {
                        hourlystatreport++;

                    }
                    if (bygtim.Rows[i][3].ToString() != "0" && bygtim.Rows[i][4].ToString() == "1")
                    {
                        hourlyigstreport++;
                    }
                    if (bygtim.Rows[i][3].ToString() == "0" && bygtim.Rows[i][4].ToString() == "1")
                    {
                        hourlyidlereport++;
                    }
                    if(bygtim.Rows[i][6].ToString() == "0.00" && acstatus == "On")
                    {
                        hourlyacreport++;
                    }
                   

                }
                else
                {
                    DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = hourarray1[rowcount];
                    dr1["Thours"] = hourarray2[rowcount];
                    dr1["igst"] = hourlyigstreport;
                    dr1["idle"] = hourlyidlereport;
                    dr1["Stat"] = hourlystatreport;
                    dr1["TKMR"] = Math.Round(hourlykmreport, 2);
                    dr1["AC"] = hourlyacreport;
                    dt1.Rows.Add(dr1);
                    hourlykmreport = 0;
                    hourlystatreport = 0;
                    hourlyidlereport = 0;
                    hourlyigstreport = 0;
                    hourlyacreport = 0;
                    rowcount++;
                }

                if (i == bygtim.Rows.Count - 2)
                {
                    DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = hourarray1[rowcount];
                    dr1["Thours"] = hourarray2[rowcount];
                    dr1["igst"] = hourlyigstreport;
                    dr1["idle"] = hourlyidlereport;
                    dr1["Stat"] = hourlystatreport;
                    dr1["TKMR"] = Math.Round(hourlykmreport, 2);
                    dr1["AC"] = hourlyacreport;
                    dt1.Rows.Add(dr1);
                    hourlykmreport = 0;
                    hourlystatreport = 0;
                    hourlyidlereport = 0;
                    hourlyigstreport = 0;
                    hourlyacreport = 0;
                    rowcount++;
                }
                prevhour = currenthour;
            }
            dt1.AcceptChanges();
            gv.DataSource = dt1;
            gv.DataBind();

        }


        protected void Download_Click(object sender, EventArgs e)
        {
            if (date.Text != "")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = Convert.ToDateTime(date.Text);
                string str = dt.ToString("yyyy-MM-dd");
                string str1 = str.Replace("-", "");
                str1 = str1.Replace(" ", "");
                str1 = str1.Substring(2);
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VDSR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gv);
                tr1.Cells.Add(cell1);
                tb.Rows.Add(tr1);

                tb.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
             
            
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }   
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    if (e.Row.Cells[1].Text == "&nbsp;")
                    {
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                    }
                    Label lbl = e.Row.FindControl("igst9") as Label;
                    //lbl.Text = igst9.ToString();
                    if (lbl.Text != "&nbsp;" && lbl.Text != "")
                    {
                        igst1 += Convert.ToInt32(lbl.Text);//e.Row.Cells[2].Text.ToString());
                    }
                    //Tigst.Text = igst1.ToString();
                    Label lbl1 = e.Row.FindControl("idle9") as Label;
                    if (lbl1.Text != "&nbsp;" && lbl1.Text != "")
                    {
                        idle1 += Convert.ToInt32(lbl1.Text);
                    }

                    //Tidle.Text = idle1.ToString();
                    Label lbl2 = e.Row.FindControl("Stat9") as Label;
                    if (lbl2.Text != "&nbsp;" && lbl2.Text != "")
                    {
                        sta1 += Convert.ToInt32(lbl2.Text);
                    }
                    //Tsta.Text = sta1.ToString();
                    Label lbl3 = e.Row.FindControl("TKMR9") as Label;
                    if (lbl3.Text != "&nbsp;" && lbl3.Text != "")
                    {
                        tkmr1 += Convert.ToDouble(lbl3.Text);
                    }
                    //Tkmr.Text = tkmr1.ToString();
                    Label lbl4 = e.Row.FindControl("ac") as Label;
                    if (lbl4.Text != "&nbsp;" && lbl4.Text != "0" && lbl4.Text != "")
                    {
                        Acstatus += Convert.ToInt32 (lbl4.Text);
                    }

                    //Tfuel.Text = Fuelstat.ToString();

                }
                catch (Exception ex) { }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl = e.Row.FindControl("ftigst") as Label;
                int value = igst1;
                int hours = value / 60; // 2
                int minutes = value % 60; // 1

                string strHours = hours.ToString();
                string strMinutes = minutes.ToString();
                lbl.Text = strHours + " Hr " + strMinutes + " Min";

                Label lbl1 = e.Row.FindControl("ftidle") as Label;
                int value1 = idle1;
                int hours1 = value1 / 60; // 2
                int minutes1 = value1 % 60; // 1

                string strHours1 = hours1.ToString();
                string strMinutes1 = minutes1.ToString();

                lbl1.Text = strHours1 + " Hr " + strMinutes1 + " Min";

                Label lbl2 = e.Row.FindControl("ftstat") as Label;
                int value2 = sta1;
                int hours2 = value2 / 60; // 2
                int minutes2 = value2 % 60; // 1

                string strHours2 = hours2.ToString();
                string strMinutes2 = minutes2.ToString();
                lbl2.Text = strHours2 + " Hr " + strMinutes2 + " Min";

                Label lbl3 = e.Row.FindControl("ftTKMR") as Label;
                lbl3.Text = tkmr1.ToString() + " KM";
                Label lbl4 = e.Row.FindControl("ftFuelS") as Label;
                int value3 = Acstatus;
                int hours3 = value3 / 60; // 2
                int minutes3 = value3 % 60; // 1

                string strHours3 = hours3.ToString();
                string strMinutes3 = minutes3.ToString();

                lbl4.Text = strHours3 + " Hr " + strMinutes3 + " Min";
            }
        }





        protected void gv_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                //DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
                string fdate = date.Text + " 12:00 AM ";
                string tdate = date.Text + " 11:59 PM ";
                DataTable dt3 = db.search(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                string maxspeed = "";
                if (dt3.Rows.Count != 0)
                {
                    DataTable dt1 = db.getMaxspedandtime(dt3.Rows[0][5].ToString(), fdate, tdate);
                    double maxsped = Convert.ToDouble(dt1.Rows[0][1].ToString());
                    string time = dt1.Rows[0][0].ToString();
                    maxsped = maxsped * 1.85;
                    maxsped = Math.Floor(maxsped);
                    maxspeed = Convert.ToString(maxsped) + " Km/H " + time;

                }
                GridViewRow HeaderGridRow = new GridViewRow(0,0,DataControlRowType .Header ,DataControlRowState .Insert );
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.ColumnSpan = 2;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.ColumnSpan = 2;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + dt.Rows[0][1].ToString();
                HeaderCell4.ColumnSpan = 3;
                HeaderCell4.Font.Bold = true;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell4);
                gv.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                DateTime hdt = Convert.ToDateTime(date.Text);
                HeaderCell1.Text = "Date  :" + hdt.ToString("dd-MM-yyyy");
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.Font.Bold = true;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                TableCell HeaderCell3 = new TableCell();
                HeaderCell3.Text = "Driver Name :" + dt.Rows[0][0].ToString();
                HeaderCell3.ColumnSpan = 2;
                HeaderCell3.Font.Bold = true;
                HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell3);
                TableCell HeaderCell5 = new TableCell();
                HeaderCell5.Text = "Driver Mobile No :" + dt.Rows[0][2].ToString();
                HeaderCell5.ColumnSpan = 3;
                HeaderCell5.Font.Bold = true;
                HeaderCell5.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell5.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell5);
                gv.Controls[0].Controls.AddAt(1, HeaderGridRow1);

                GridViewRow HeaderGridRow2 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell21 = new TableCell();
                HeaderCell21.Text = "Maximum Speed :" + maxspeed;
                HeaderCell21.ColumnSpan = 2;
                HeaderCell21.Font.Bold = true; HeaderCell21.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell21.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell21.ForeColor = System.Drawing.Color.Red;
                HeaderGridRow2.Cells.Add(HeaderCell21);
                TableCell HeaderCell22 = new TableCell();
                HeaderCell22.Text = "Transmission Frequency : 60 sec";
                HeaderCell22.ColumnSpan = 2;
                HeaderCell22.Font.Bold = true;
                HeaderCell22.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell22.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow2.Cells.Add(HeaderCell22);
                TableCell HeaderCell23 = new TableCell();
                HeaderCell23.Text = "Vehicle Maker :" + dt.Rows[0][3].ToString();
                HeaderCell23.ColumnSpan = 3;
                HeaderCell23.Font.Bold = true;
                HeaderCell23.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell23.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow2.Cells.Add(HeaderCell23);
                gv.Controls[0].Controls.AddAt(2, HeaderGridRow2);
            }

        }
    }
}
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;

namespace Tracking
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                //GDirection direction = new GDirection();
                //direction.autoGenerate = false;
                //direction.buttonElementId = "bt_Go";
                //direction.fromElementId = tb_fromPoint.ClientID;
                //direction.toElementId = tb_endPoint.ClientID;
                //tb_fromPoint.Text = "24.6669860000,73.6303710000";
                //tb_endPoint.Text = "24.6669860000,73.6083980000";
                //direction.divElementId = "div_directions"; direction.clearMap = true;
                

                //// Optional
                //// direction.locale = "es-ES";

                //GMap1.Add(direction);




                
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
#pragma warning disable CS0618 // 'GMap.enableContinuousZoom' is obsolete: 'Maps v3 enables it automatically in browsers that support it.'
            GMap1.enableContinuousZoom = true;
#pragma warning restore CS0618 // 'GMap.enableContinuousZoom' is obsolete: 'Maps v3 enables it automatically in browsers that support it.'
            GLatLng latlng = new GLatLng(46, 21);
            GMarker marker = new GMarker(latlng);
            GMap1.setCenter(latlng, 4);
#pragma warning disable CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'
            GMap1.enableScrollWheelZoom = true;
#pragma warning restore CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'
            List<GLatLng> points = new List<GLatLng>();
            points.Add(latlng + new GLatLng(0, 8));
            marker = new GMarker(latlng + new GLatLng(0, 8));
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(marker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            points.Add(latlng + new GLatLng(-0.5, 4.2));
            marker = new GMarker(latlng + new GLatLng(-0.5, 4.2));
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(marker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            points.Add(latlng);
            marker = new GMarker(latlng);
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(marker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            points.Add(latlng + new GLatLng(3.5, -4));
            marker = new GMarker(latlng + new GLatLng(3.5, -4));
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(marker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            points.Add(latlng + new GLatLng(4.79, +2.6));
            marker = new GMarker(latlng + new GLatLng(4.79, +2.6));
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(marker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GPolyline line = new GPolyline(points, "FF0000", 2);
#pragma warning disable CS0618 // 'GMap.addPolyline(GPolyline)' is obsolete: 'Use Add(GPolyline) instead.'
            GMap1.addPolyline(line);
#pragma warning restore CS0618 // 'GMap.addPolyline(GPolyline)' is obsolete: 'Use Add(GPolyline) instead.'


            GInfoWindowOptions options = new GInfoWindowOptions();
#pragma warning disable CS0618 // 'GInfoWindowOptions.zoomLevel' is obsolete: 'There is no MapBlowUp in v3 API'
            options.zoomLevel = 10;
#pragma warning restore CS0618 // 'GInfoWindowOptions.zoomLevel' is obsolete: 'There is no MapBlowUp in v3 API'
#pragma warning disable CS0618 // 'GInfoWindowOptions.mapType' is obsolete: 'There is no MapBlowUp in v3 API'
            options.mapType = GMapType.GTypes.Hybrid;
#pragma warning restore CS0618 // 'GInfoWindowOptions.mapType' is obsolete: 'There is no MapBlowUp in v3 API'
#pragma warning disable CS0618 // 'GShowMapBlowUp' is obsolete: 'There is no MapBlowUp in v3 API'
#pragma warning disable CS0618 // 'GShowMapBlowUp' is obsolete: 'There is no MapBlowUp in v3 API'
            GShowMapBlowUp mBlowUp = new GShowMapBlowUp(new GLatLng(string.Format("{0}.getLatLng()", marker.ID)), options);
#pragma warning restore CS0618 // 'GShowMapBlowUp' is obsolete: 'There is no MapBlowUp in v3 API'
#pragma warning restore CS0618 // 'GShowMapBlowUp' is obsolete: 'There is no MapBlowUp in v3 API'


            GListener listener = new GListener(GMap1.GMap_Id, GListener.Event.click, string.Format(@"function(){{{0};}}", mBlowUp.ToString(GMap1.GMap_Id)));

#pragma warning disable CS0618 // 'GMap.addListener(GListener)' is obsolete: 'Use Add(GListener) instead'
            GMap1.addListener(listener);
#pragma warning restore CS0618 // 'GMap.addListener(GListener)' is obsolete: 'Use Add(GListener) instead'

#pragma warning disable CS0618 // 'GMap.addSnapToRoute(SnapToToute)' is obsolete: 'Use Add(SnapToToute) instead.'
#pragma warning disable CS0618 // 'SnapToToute' is obsolete: 'Not available in Google Maps v3'
            GMap1.addSnapToRoute(new SnapToToute(marker, line)); 
#pragma warning restore CS0618 // 'SnapToToute' is obsolete: 'Not available in Google Maps v3'
#pragma warning restore CS0618 // 'GMap.addSnapToRoute(SnapToToute)' is obsolete: 'Use Add(SnapToToute) instead.'
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {

        }
    }
}

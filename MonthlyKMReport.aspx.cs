﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace Tracking
{
    public partial class MonthlyKMReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        double rcount = 0;
        double count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    ddlmonthbind();
                }
            }

        }
        public void ddlmonthbind()
        {
            DateTime dt = DateTime.Now;
            int m = Convert.ToInt32(dt.Month);
            if (m == 1)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));

            }
            else if (m == 2)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));

            }
            else if (m == 3)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));

            }
            else if (m == 4)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
            }
            else if (m == 5)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
            }
            else if (m == 6)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
            }
            else if (m == 7)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));

            }
            else if (m == 8)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));

            }
            else if (m == 9)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));

            }
            else if (m == 10)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));

            }
            else if (m == 11)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));

            }
            else if (m == 12)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));

            }
        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
             DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (month == cmonth)
            {
                bindmonthkm();
            }
            else
            {
                bindmonthkmold();

            }

        }
        public void bindmonthkm()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dttemp = db.Select_KMMonth(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DataTable dthours = db.getRhours();
            if (dttemp.Rows.Count == 0)
            {
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                for (int i = 0; i < 31; i++)
                {
                    if ((fdt.Month <= month && fdt < dt))
                    {
                        db.Insert_MonthKM(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString(), fdt.ToShortDateString());
                        DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                        for (int j = 0; j < dthours.Rows.Count; j++)
                        {

                            DataTable dt12 = db.Select_TotalRunkm(ddlMapTOVehicle.SelectedValue.ToString(), fdt1.ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                            double TKMR = 0;
                            for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                            {
                                TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                            }
                            if (dt12.Rows.Count != 0)
                            {
                                fdt1 = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                            }
                            TKMR = (double)Math.Round(TKMR, 2);
                            db.Update_KMMonth(Vno, month.ToString(), TKMR, j, fdt.ToString("dd/MM/yyyy"));

                        }
                        fdt = fdt.AddDays(1);

                    }
                    else break;
                }
            }
            else
            {
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
                string oldstr = dttemp.Rows[0][3].ToString();
                string strDate = DateTime.ParseExact(oldstr, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");
                DateTime fdt = Convert.ToDateTime(strDate);
                fdt = fdt.AddDays(1);
                for (int i = 0; i < 31; i++)
                {
                    if ((fdt.Month <= month && fdt < dt))
                    {
                        db.Insert_MonthKM(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString(), fdt.ToShortDateString());
                        DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                        for (int j = 0; j < dthours.Rows.Count; j++)
                        {

                            DataTable dt12 = db.Select_TotalRunkm(ddlMapTOVehicle.SelectedValue.ToString(), fdt1.ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                            double TKMR = 0;
                            for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                            {
                                TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                            }
                            if (dt12.Rows.Count != 0)
                            {
                                fdt1 = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                            }
                            TKMR = (double)Math.Round(TKMR, 2);
                            db.Update_KMMonth(Vno, month.ToString(), TKMR, j, fdt.ToString("dd/MM/yyyy"));

                        }
                        fdt = fdt.AddDays(1);
                    }
                    else break;
                }

            }
            DataTable dt1 = db.get_KMMonth(Vno, month);
            gvreport.DataSource = dt1;
            gvreport.DataBind();
            Enable();

        }
        public void Enable()
        {

            ddlMapTOVehicle.Enabled = false;
            ddlmonth.Enabled = false;
        }
        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime, int Month)
        {
            return new DateTime(dateTime.Year, Month, 1);
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void Download_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "" && ddlmonth.SelectedItem.Text != "--select Month--")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                string str1 = ddlmonth.SelectedItem.Text + "_" + fdt.Year;
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VKM_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);
                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }
        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Items.Clear();
            Response.Redirect(Request.RawUrl);
        }
        public void bindmonthkmold()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dttemp = db.Select_KMMonth(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DataTable dthours = db.getRhours();
            if (dttemp.Rows.Count == 0)
            {
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                for (int i = 0; i < 31; i++)
                {
                    if ((fdt.Month <= month && fdt < dt))
                    {
                        db.Insert_MonthKM(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString(), fdt.ToShortDateString());
                        DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                        for (int j = 0; j < dthours.Rows.Count; j++)
                        {

                            DataTable dt12 = db.Select_TotalRunkmold(ddlMapTOVehicle.SelectedValue.ToString(), fdt1.ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                            double TKMR = 0;
                            for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                            {
                                TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                            }
                            if (dt12.Rows.Count != 0)
                            {
                                fdt1 = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                            }
                            TKMR = (double)Math.Round(TKMR, 2);
                            db.Update_KMMonth(Vno, month.ToString(), TKMR, j, fdt.ToString("dd/MM/yyyy"));

                        }
                        fdt = fdt.AddDays(1);

                    }
                    else break;
                }
            }
            else
            {
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
                string oldstr = dttemp.Rows[0][3].ToString();
                string strDate = DateTime.ParseExact(oldstr, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");
                DateTime fdt = Convert.ToDateTime(strDate);
                fdt = fdt.AddDays(1);
                for (int i = 0; i < 31; i++)
                {
                    if ((fdt.Month <= month && fdt < dt))
                    {
                        db.Insert_MonthKM(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString(), fdt.ToShortDateString());
                        DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                        for (int j = 0; j < dthours.Rows.Count; j++)
                        {

                            DataTable dt12 = db.Select_TotalRunkmold(ddlMapTOVehicle.SelectedValue.ToString(), fdt1.ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                            double TKMR = 0;
                            for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                            {
                                TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                            }
                            if (dt12.Rows.Count != 0)
                            {
                                fdt1 = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                            }
                            TKMR = (double)Math.Round(TKMR, 2);
                            db.Update_KMMonth(Vno, month.ToString(), TKMR, j, fdt.ToString("dd/MM/yyyy"));

                        }
                        fdt = fdt.AddDays(1);
                    }
                    else break;
                }

            }
            DataTable dt1 = db.get_KMMonth(Vno, month);
            gvreport.DataSource = dt1;
            gvreport.DataBind();
            Enable();

        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dt1 = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 9;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 8;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);
                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + dt.Rows[0][1].ToString();
                HeaderCell4.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell4.ColumnSpan = 9;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell4.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow3 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell12 = new TableCell();
                HeaderCell12.Text = "Report for the Month :" + ddlmonth.SelectedItem.Text + " " + DateTime.Now.Year;
                HeaderCell12.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell12.ColumnSpan = 26;
                HeaderCell12.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell12.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell12.Font.Bold = true;
                HeaderGridRow3.Cells.Add(HeaderCell12);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow3);

                GridViewRow HeaderGridRow2 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell21 = new TableCell();
                HeaderCell21.Text = "";
                HeaderCell21.HorizontalAlign = HorizontalAlign.Right;
                HeaderCell21.ColumnSpan = 1;
                HeaderCell21.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell21.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell21.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell21);
                TableCell HeaderCell22 = new TableCell();
                HeaderCell22.Text = "Time of the Day in Hrs (24 Hours)";
                HeaderCell22.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell22.ColumnSpan = 24;
                HeaderCell22.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell22.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell22.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell22);
                TableCell HeaderCell23 = new TableCell();
                HeaderCell23.Text = "";
                HeaderCell23.HorizontalAlign = HorizontalAlign.Right;
                HeaderCell23.ColumnSpan = 1;
                HeaderCell23.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell23.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell23.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell23);
                gvreport.Controls[0].Controls.AddAt(2, HeaderGridRow2);


            }
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                count = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rcount = 0;
                Label lbl1 = e.Row.FindControl("lblH1") as Label;
                rcount += Convert.ToDouble(lbl1.Text);
                if (Convert.ToDouble(lbl1.Text) == 0)
                {
                    lbl1.Text = "  ";
                }
                else if (Convert.ToDouble(lbl1.Text) > 0)
                {
                    e.Row.Cells[1].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl2 = e.Row.FindControl("lblH2") as Label;
                rcount += Convert.ToDouble(lbl2.Text);
                if (Convert.ToDouble(lbl2.Text) == 0)
                {
                    lbl2.Text = "  ";
                }
                else if (Convert.ToDouble(lbl2.Text) > 0)
                {
                    e.Row.Cells[2].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl3 = e.Row.FindControl("lblH3") as Label;
                rcount += Convert.ToDouble(lbl3.Text);
                if (Convert.ToDouble(lbl3.Text) == 0)
                {
                    lbl3.Text = "  ";
                }
                else if (Convert.ToDouble(lbl3.Text) > 0)
                {
                    e.Row.Cells[3].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl4 = e.Row.FindControl("lblH4") as Label;
                rcount += Convert.ToDouble(lbl4.Text);
                if (Convert.ToDouble(lbl4.Text) == 0)
                {
                    lbl4.Text = "  ";
                }
                else if (Convert.ToDouble(lbl4.Text) > 0)
                {
                    e.Row.Cells[4].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl5 = e.Row.FindControl("lblH5") as Label;
                rcount += Convert.ToDouble(lbl5.Text);
                if (Convert.ToDouble(lbl5.Text) == 0)
                {
                    lbl5.Text = "  ";
                }
                else if (Convert.ToDouble(lbl5.Text) > 0)
                {
                    e.Row.Cells[5].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl6 = e.Row.FindControl("lblH6") as Label;
                rcount += Convert.ToDouble(lbl6.Text);
                if (Convert.ToDouble(lbl6.Text) == 0)
                {
                    lbl6.Text = "  ";
                }
                else if (Convert.ToDouble(lbl6.Text) > 0)
                {
                    e.Row.Cells[6].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl7 = e.Row.FindControl("lblH7") as Label;
                rcount += Convert.ToDouble(lbl7.Text);
                if (Convert.ToDouble(lbl7.Text) == 0)
                {
                    lbl7.Text = "  ";
                }
                else if (Convert.ToDouble(lbl7.Text) > 0)
                {
                    e.Row.Cells[7].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl8 = e.Row.FindControl("lblH8") as Label;
                rcount += Convert.ToDouble(lbl8.Text);
                if (Convert.ToDouble(lbl8.Text) == 0)
                {
                    lbl8.Text = "  ";
                }
                else if (Convert.ToDouble(lbl8.Text) > 0)
                {
                    e.Row.Cells[8].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl9 = e.Row.FindControl("lblH9") as Label;
                rcount += Convert.ToDouble(lbl9.Text);
                if (Convert.ToDouble(lbl9.Text) == 0)
                {
                    lbl9.Text = "  ";
                }
                else if (Convert.ToDouble(lbl9.Text) > 0)
                {
                    e.Row.Cells[9].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl10 = e.Row.FindControl("lblH10") as Label;
                rcount += Convert.ToDouble(lbl10.Text);
                if (Convert.ToDouble(lbl10.Text) == 0)
                {
                    lbl10.Text = "  ";
                }
                else if (Convert.ToDouble(lbl10.Text) > 0)
                {
                    e.Row.Cells[10].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl11 = e.Row.FindControl("lblH11") as Label;
                rcount += Convert.ToDouble(lbl11.Text);
                if (Convert.ToDouble(lbl11.Text) == 0)
                {
                    lbl11.Text = "  ";
                }
                else if (Convert.ToDouble(lbl11.Text) > 0)
                {
                    e.Row.Cells[11].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl12 = e.Row.FindControl("lblH12") as Label;
                rcount += Convert.ToDouble(lbl12.Text);
                if (Convert.ToDouble(lbl12.Text) == 0)
                {
                    lbl12.Text = "  ";
                }
                else if (Convert.ToDouble(lbl12.Text) > 0)
                {
                    e.Row.Cells[12].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl13 = e.Row.FindControl("lblH13") as Label;
                rcount += Convert.ToDouble(lbl13.Text);
                if (Convert.ToDouble(lbl13.Text) == 0)
                {
                    lbl13.Text = "  ";
                }
                else if (Convert.ToDouble(lbl13.Text) > 0)
                {
                    e.Row.Cells[13].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl14 = e.Row.FindControl("lblH14") as Label;
                rcount += Convert.ToDouble(lbl14.Text);
                if (Convert.ToDouble(lbl14.Text) == 0)
                {
                    lbl14.Text = "  ";
                }
                else if (Convert.ToDouble(lbl14.Text) > 0)
                {
                    e.Row.Cells[14].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl15 = e.Row.FindControl("lblH15") as Label;
                rcount += Convert.ToDouble(lbl15.Text);
                if (Convert.ToDouble(lbl15.Text) == 0)
                {
                    lbl15.Text = "  ";
                }
                else if (Convert.ToDouble(lbl15.Text) > 0)
                {
                    e.Row.Cells[15].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl16 = e.Row.FindControl("lblH16") as Label;
                rcount += Convert.ToDouble(lbl16.Text);
                if (Convert.ToDouble(lbl16.Text) == 0)
                {
                    lbl16.Text = "  ";
                }
                else if (Convert.ToDouble(lbl16.Text) > 0)
                {
                    e.Row.Cells[16].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl17 = e.Row.FindControl("lblH17") as Label;
                rcount += Convert.ToDouble(lbl17.Text);
                if (Convert.ToDouble(lbl17.Text) == 0)
                {
                    lbl17.Text = "  ";
                }
                else if (Convert.ToDouble(lbl17.Text) > 0)
                {
                    e.Row.Cells[17].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl18 = e.Row.FindControl("lblH18") as Label;
                rcount += Convert.ToDouble(lbl18.Text);
                if (Convert.ToDouble(lbl18.Text) == 0)
                {
                    lbl18.Text = "  ";
                }
                else if (Convert.ToDouble(lbl18.Text) > 0)
                {
                    e.Row.Cells[18].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl19 = e.Row.FindControl("lblH19") as Label;
                rcount += Convert.ToDouble(lbl19.Text);
                if (Convert.ToDouble(lbl19.Text) == 0)
                {
                    lbl19.Text = "  ";
                }
                else if (Convert.ToDouble(lbl19.Text) > 0)
                {
                    e.Row.Cells[19].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl20 = e.Row.FindControl("lblH20") as Label;
                rcount += Convert.ToDouble(lbl20.Text);
                if (Convert.ToDouble(lbl20.Text) == 0)
                {
                    lbl20.Text = "  ";
                }
                else if (Convert.ToDouble(lbl20.Text) > 0)
                {
                    e.Row.Cells[20].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl21 = e.Row.FindControl("lblH21") as Label;
                rcount += Convert.ToDouble(lbl21.Text);
                if (Convert.ToDouble(lbl21.Text) == 0)
                {
                    lbl21.Text = "  ";
                }
                else if (Convert.ToDouble(lbl21.Text) > 0)
                {
                    e.Row.Cells[21].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl22 = e.Row.FindControl("lblH22") as Label;
                rcount += Convert.ToDouble(lbl22.Text);
                if (Convert.ToDouble(lbl22.Text) == 0)
                {
                    lbl22.Text = "  ";
                }
                else if (Convert.ToDouble(lbl22.Text) > 0)
                {
                    e.Row.Cells[22].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl23 = e.Row.FindControl("lblH23") as Label;
                rcount += Convert.ToDouble(lbl23.Text);
                if (Convert.ToDouble(lbl23.Text) == 0)
                {
                    lbl23.Text = "  ";
                }
                else if (Convert.ToDouble(lbl23.Text) > 0)
                {
                    e.Row.Cells[23].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl24 = e.Row.FindControl("lblH24") as Label;
                rcount += Convert.ToDouble(lbl24.Text);
                if (Convert.ToDouble(lbl24.Text) == 0)
                {
                    lbl24.Text = "  ";
                }
                else if (Convert.ToDouble(lbl24.Text) > 0)
                {
                    e.Row.Cells[24].BackColor = System.Drawing.Color.LightPink;
                }

                Label total = e.Row.FindControl("lbltotal") as Label;
                total.Text = rcount.ToString();
                count += rcount;

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label;
                ftotal.Text = count.ToString()+" ";

            }
        }

    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class SetAlerts : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
                binddatatoddls();

            }
        }
        public void binddatatoddls()
        {

            ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlFromHOURS.DataBind();

            ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
            ddlFromMINUTES.DataBind();

            ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlToHOURS.DataBind();

            ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
            ddlToMINUTES.DataBind();

            ddlFromHOURS1.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlFromHOURS1.DataBind();

            ddlFromMINUTES1.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
            ddlFromMINUTES1.DataBind();

            ddlToHOURS1.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlToHOURS1.DataBind();

            ddlToMINUTES1.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
            ddlToMINUTES1.DataBind();

        }
        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Vehicle--");
        }
        public void refresh()
        {
            Response.Redirect(Request.RawUrl);


        }
        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue != "--Select Vehicle--")
            {
                bindvehicledata(ddlMapTOVehicle.SelectedValue.ToString());

            }
            else
            {
                refresh();
            }
        }
        public void bindvehicledata(string Vno)
        {
#pragma warning disable CS0168 // The variable 'vi1' is declared but never used
            int vs1, vi1, vm1 = 0;
#pragma warning restore CS0168 // The variable 'vi1' is declared but never used
            int hour, min = 0;
            DataTable EditV = db.LoadVeh(Vno);
            txtGPRSActDt.Text = EditV.Rows[0]["GPRS_Activated_Date"].ToString();
            txtVehInsExpDt.Text = EditV.Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            txtDrivLic.Text = EditV.Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            txtrtodate.Text = EditV.Rows[0]["Rto_Renewal_Date"].ToString();
            txtspeed.Text = EditV.Rows[0]["Overspeed"].ToString();
            txtconfdate.Text = EditV.Rows[0]["Contract_Fromdate"].ToString();
            txtcontdate.Text = EditV.Rows[0]["Contract_Todate"].ToString();
            txtconname.Text = EditV.Rows[0]["Contract_Name"].ToString();
            txtfcdate.Text = EditV.Rows[0]["Rto_fc_date"].ToString();
            txtems.Text = EditV.Rows[0]["Emission_date"].ToString();
            txtroadtax.Text = EditV.Rows[0]["Road_tax_date"].ToString();
            maxtemp.Text = EditV.Rows[0]["Max_Temp"].ToString();
            string vs = EditV.Rows[0]["Alert_stationary"].ToString();
            string vi = EditV.Rows[0]["Alert_idling"].ToString();
            string vm = EditV.Rows[0]["Alert_moving"].ToString();
            mintemp.Text = EditV.Rows[0]["Min_Temp"].ToString();
            txtdelay.Text = EditV.Rows[0]["Delay_Temp"].ToString();
            string sft = EditV.Rows[0]["Stationary_Fromtime"].ToString();
            string stt = EditV.Rows[0]["Stationary_Totime"].ToString();
            string saft = EditV.Rows[0]["Vehicle_Security_Ftime"].ToString();
            string satt = EditV.Rows[0]["Vehicle_Security_Ttime"].ToString();
            string saMin = EditV.Rows[0]["Vehicle_Security_Min"].ToString();
            if (txtVehInsExpDt.Text == "")
            {
                cbins.Checked = false;
                txtVehInsExpDt.Visible = false;
                cbins.Text = "Enable";
            }
            else
            {
                txtVehInsExpDt.Visible = true;
                cbins.Checked = true;
                cbins.Text = "Disable";

            }
            if (txtDrivLic.Text == "")
            {
                txtDrivLic.Visible = false;
                cblicdate.Checked = false;
                cblicdate.Text = "Enable";
            }
            else
            {
                cblicdate.Checked = true;
                cblicdate.Text = "Disable";
            }
            if (txtrtodate.Text == "")
            {
                cbrtodate.Checked = false;
                txtrtodate.Visible = false;
                cbrtodate.Text = "Enable";

            }
            else
            {
                cbrtodate.Checked = true;
                txtrtodate.Visible = true;
                cbrtodate.Text = "Disable";
            }
            if (txtDrivLic.Text == "")
            {
                txtDrivLic.Visible = false;
                cblicdate.Checked = false;
                cblicdate.Text = "Enable";
            }
            else
            {
                txtDrivLic.Visible = true;
                cblicdate.Checked = true;
                cblicdate.Text = "Disable";
            }
            if (txtfcdate.Text == "")
            {
                txtfcdate.Visible = false;
                cbfcdate.Checked = false;
                cbfcdate.Text = "Enable";
            }
            else
            {
                txtfcdate.Visible = true;
                cbfcdate.Checked = true;
                cbfcdate.Text = "Disable";
            }
            if (txtems.Text == "")
            {
                txtems.Visible = false;
                cbems.Checked = false;
                cbems.Text = "Enable";
            }
            else
            {
                txtems.Visible = true;
                cbems.Checked = true;
                cbems.Text = "Disable";
            }
            if (txtroadtax.Text == "")
            {
                txtroadtax.Visible = false;
                cbroadtax.Checked = false;
                cbroadtax.Text = "Enable";
            }
            else
            {
                txtroadtax.Visible = true;
                cbroadtax.Checked = true;
                cbroadtax.Text = "Disable";
            }
            if (txtspeed.Text == "" || txtspeed.Text == "0")
            {
                txtspeed.Visible = false;
                cbovers.Checked = false;
                cbovers.Text = "Enable";
            }
            else
            {
                txtspeed.Visible = true;
                cbovers.Checked = true;
                cbovers.Text = "Disable";
            }
            if (txtconname.Text == "")
            {
                txtconname.Visible = false;
                txtconfdate.Visible = false;
                txtcontdate.Visible = false;
                cbcontd.Checked = false;
                cbcontd.Text = "Enable";
            }
            else
            {
                txtconname.Visible = true;
                txtconfdate.Visible = true;
                txtcontdate.Visible = true;
                cbcontd.Checked = true;
                cbcontd.Text = "Disable";
            }
            if ((maxtemp.Text == "" || maxtemp.Text == "0") && (mintemp.Text == "" || mintemp.Text == "0") && (txtdelay.Text == "" || txtdelay.Text == "0"))
            {
                pnltemp.Visible = false;
                chkbtemp.Checked = false;
                chkbtemp.Text = "Enable";
            }
            else
            {
                pnltemp.Visible = true;
                chkbtemp.Checked = true;
                chkbtemp.Text = "Disable";
            }
            if (vs != "" && vs != "0")
            {
                vs1 = Convert.ToInt32(vs);
                hour = vs1 / 60;
                min = vs1 % 60;
                txthr.Text = hour.ToString();
                txtmin.Text = min.ToString();
                txthr.Visible = true;
                txtmin.Visible = true;
                chkbvs.Checked = true;
                PnlS.Visible = true;
                string str = sft.Replace(" ", ":");
                string[] results = str.Split(new[] { ':' });
                if (results.Length == 3)
                {
                    ddlFromHOURS.SelectedValue = results[0].ToString();
                    ddlFromMINUTES.SelectedValue = ":" + results[1].ToString();
                    rbtnAMPM.SelectedValue = results[2].ToString();
                }
                string str1 = stt.Replace(" ", ":");
                string[] results1 = str1.Split(new[] { ':' });
                if (results1.Length == 3)
                {
                    ddlToHOURS.SelectedValue = results1[0].ToString();
                    ddlToMINUTES.SelectedValue = ":" + results1[1].ToString();
                    RadioButtonList1.SelectedValue = results1[2].ToString();
                }
                chkbvs.Text = "Disable";
            }
            else
            {
                chkbvs.Checked = false;
                chkbvs.Text = "Enable";
                txthr.Visible = false;
                txtmin.Visible = false;
                PnlS.Visible = false;
            }

            if (saMin != "0" && saMin != "")
            {
                txtsamin.Text = saMin;
                txtsamin.Visible = true;
                CkbSecAMin.Checked = true;
                CkbSecAMin.Text = "Disable";
                PnlSA.Visible = true;

                string str = saft.Replace(" ", ":");
                string[] results = str.Split(new[] { ':' });
                if (results.Length == 3)
                {
                    ddlFromHOURS1.SelectedValue = results[0].ToString();
                    ddlFromMINUTES1.SelectedValue = ":" + results[1].ToString();
                    RadioButtonList2.SelectedValue = results[2].ToString();
                }
                string str1 = satt.Replace(" ", ":");
                string[] results1 = str1.Split(new[] { ':' });
                if (results1.Length == 3)
                {
                    ddlToHOURS1.SelectedValue = results1[0].ToString();
                    ddlToMINUTES1.SelectedValue = ":" + results1[1].ToString();
                    RadioButtonList3.SelectedValue = results1[2].ToString();
                }
            }
            else
            {
                txtsamin.Text = "";
                txtsamin.Visible = false;
                CkbSecAMin.Checked = false;
                CkbSecAMin.Text = "Enable";
                PnlSA.Visible = false;
            }

            if (vm != "" && vm != "0")
            {
                vm1 = Convert.ToInt32(vm);
                hour = vm1 / 60;
                min = vm1 % 60;
                txthr2.Text = hour.ToString();
                txtmin2.Text = min.ToString();
                txthr2.Visible = true;
                txtmin2.Visible = true;
                chkbvm.Checked = true;
                chkbvm.Text = "Disable";

            }
            else
            {
                txthr2.Visible = false;
                txtmin2.Visible = false;
                chkbvm.Checked = false;
                chkbvm.Text = "Enable";

            }
        }

        protected void cbins_CheckedChanged(object sender, EventArgs e)
        {
            if (cbins.Checked == true)
            {
                txtVehInsExpDt.Visible = true;
                cbins.Text = "Disable";
            }
            else
            {
                txtVehInsExpDt.Text = string.Empty;
                txtVehInsExpDt.Visible = false;
                cbins.Text = "Enable";
            }
        }

        protected void cbrtodate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbrtodate.Checked == true)
            {
                txtrtodate.Visible = true;
                cbrtodate.Text = "Disable";
            }
            else
            {
                txtrtodate.Text = string.Empty;
                txtrtodate.Visible = false;
                cbrtodate.Text = "Enable";
            }
        }

        protected void cblicdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cblicdate.Checked == true)
            {
                txtDrivLic.Visible = true;
                cblicdate.Text = "Disable";
            }
            else
            {
                txtDrivLic.Text = string.Empty;
                txtDrivLic.Visible = false;
                cblicdate.Text = "Enable";
            }
        }

        protected void cbfcdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbfcdate.Checked == true)
            {
                txtfcdate.Visible = true;
                cbfcdate.Text = "Disable";
            }
            else
            {
                txtfcdate.Text = string.Empty;
                txtfcdate.Visible = false;
                cbfcdate.Text = "Enable";
            }
        }

        protected void cbems_CheckedChanged(object sender, EventArgs e)
        {
            if (cbems.Checked == true)
            {
                txtems.Visible = true;
                cbems.Text = "Disable";
            }
            else
            {
                txtems.Text = string.Empty;
                txtems.Visible = false;
                cbems.Text = "Enable";
            }
        }

        protected void cbroadtax_CheckedChanged(object sender, EventArgs e)
        {
            if (cbroadtax.Checked == true)
            {
                txtroadtax.Visible = true;
                cbroadtax.Text = "Disable";
            }
            else
            {
                txtroadtax.Text = string.Empty;
                txtroadtax.Visible = false;
                cbroadtax.Text = "Enable";
            }
        }

        protected void cbovers_CheckedChanged(object sender, EventArgs e)
        {
            if (cbovers.Checked == true)
            {
                txtspeed.Visible = true;
                cbovers.Text = "Disable";
            }
            else
            {
                txtspeed.Text = string.Empty;
                txtspeed.Visible = false;
                cbovers.Text = "Enable";
            }
        }

        protected void cbcontd_CheckedChanged(object sender, EventArgs e)
        {
            if (cbcontd.Checked == true)
            {
                txtconname.Visible = true;
                txtcontdate.Visible = true;
                txtconfdate.Visible = true;
                cbcontd.Text = "Disable";
            }
            else
            {
                txtconname.Text = string.Empty;
                txtcontdate.Text = string.Empty;
                txtconfdate.Text = string.Empty;
                txtconname.Visible = false;
                txtcontdate.Visible = false;
                txtconfdate.Visible = false;
                cbcontd.Text = "Enable";
            }
        }

        protected void chkbvs_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbvs.Checked == true)
            {
                txthr.Visible = true;
                txtmin.Visible = true;
                PnlS.Visible = true;
                chkbvs.Text = "Disable";
            }
            else
            {
                txthr.Text = string.Empty;
                txtmin.Text = string.Empty;
                txthr.Visible = false;
                txtmin.Visible = false;
                PnlS.Visible = false;
                chkbvs.Text = "Enable";
            }
        }

        protected void chkbvm_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbvm.Checked == true)
            {
                txthr2.Visible = true;
                txtmin2.Visible = true;
                chkbvm.Text = "Disable";
            }
            else
            {
                txthr2.Text = string.Empty;
                txtmin2.Text = string.Empty;
                txthr2.Visible = false;
                txtmin2.Visible = false;
                chkbvm.Text = "Enable";
            }
        }

        protected void chkbtemp_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbtemp.Checked == true)
            {
                pnltemp.Visible = true;
                chkbtemp.Text = "Disable";
            }
            else
            {
                maxtemp.Text = string.Empty;
                mintemp.Text = string.Empty;
                txtdelay.Text = string.Empty;
                pnltemp.Visible = false;
                chkbtemp.Text = "Enable";
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
#pragma warning disable CS0219 // The variable 'vi' is assigned but its value is never used
            int vs = 0, vi = 0, vm = 0, hr = 0, min = 0;
#pragma warning restore CS0219 // The variable 'vi' is assigned but its value is never used
            string Vsft = "", vstt = "", vsaft = "", vsatt = "";
            if (txthr.Text != "" && txtmin.Text != "")
            {
                hr = Convert.ToInt32(txthr.Text);
                min = Convert.ToInt32(txtmin.Text);
                vs = hr * 60 + min;
            }
            if (txthr2.Text != "" && txtmin2.Text != "")
            {
                hr = Convert.ToInt32(txthr2.Text);
                min = Convert.ToInt32(txtmin2.Text);
                vm = hr * 60 + min;
            }
            if (chkbvs.Checked == true)
            {
                Vsft = ddlFromHOURS.SelectedValue.ToString() + ddlFromMINUTES.SelectedValue.ToString() + " " + rbtnAMPM.SelectedValue.ToString();
                vstt = ddlToHOURS.SelectedValue.ToString() + ddlToMINUTES.SelectedValue.ToString() + " " + RadioButtonList1.SelectedValue.ToString();
            }
            if (CkbSecAMin.Checked == true)
            {
                vsaft = ddlFromHOURS1.SelectedValue.ToString() + ddlFromMINUTES1.SelectedValue.ToString() + " " + RadioButtonList2.SelectedValue.ToString();
                vsatt = ddlToHOURS1.SelectedValue.ToString() + ddlToMINUTES1.SelectedValue.ToString() + " " + RadioButtonList3.SelectedValue.ToString();
            }
            if (chkbvs.Checked == false || Convert.ToDateTime(Vsft) < Convert.ToDateTime(vstt))
            {
               // db.UpdateVeh1(ddlMapTOVehicle.SelectedValue.ToString(), txtVehInsExpDt.Text, txtDrivLic.Text, txtrtodate.Text, txtspeed.Text, txtconfdate.Text, txtcontdate.Text, txtconname.Text, txtfcdate.Text, txtems.Text, txtroadtax.Text, maxtemp.Text, vs.ToString(), vi.ToString(), vm.ToString(), mintemp.Text, txtdelay.Text, txtGPRSActDt.Text, Vsft, vstt, vsaft, vsatt, txtsamin.Text);
                //db.UpdateVehicle1(ddlMapTOVehicle.SelectedValue.ToString(), txtVehInsExpDt.Text, txtDrivLic.Text, txtrtodate.Text, txtspeed.Text, txtconfdate.Text, txtcontdate.Text, txtconname.Text, txtfcdate.Text, txtems.Text, txtroadtax.Text, maxtemp.Text, vs.ToString(), vi.ToString(), vm.ToString(), mintemp.Text, txtdelay.Text, txtGPRSActDt.Text, Vsft, vstt, vsaft, vsatt, txtsamin.Text);
               // db.Delete_TempMonth(ddlMapTOVehicle.SelectedValue.ToString());
                refresh();
            }
            else
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('From Time Not Greater than To Time');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'

            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Track.aspx?username=" + Session["UserName"].ToString());

        }

        protected void CkbSecAMin_CheckedChanged(object sender, EventArgs e)
        {
            if (CkbSecAMin.Checked == true)
            {
                txtsamin.Visible = true;
                PnlSA.Visible = true;
                CkbSecAMin.Text = "Disable";
            }
            else
            {
                txtsamin.Text = string.Empty;
                txtsamin.Visible = false;
                PnlSA.Visible = false;
                CkbSecAMin.Text = "Enable";
            }
        }

    }
}

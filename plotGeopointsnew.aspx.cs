﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
namespace Tracking
{
    public partial class plotGeopointsnew : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt = 0, lng = 0;
        static int p = 0;
        static int sno = 0;
        static string rname = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;

                ddlrno.DataSource = db.get_Geo_RouteNo12(Session["userID"].ToString());
                ddlrno.DataBind();
                btnfinish.Enabled = false;
            }


        }

        protected void ddlrno_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--");
        }

        protected void ddlrno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrno.SelectedValue != "--")
            {
                GMap1.enableServerEvents = false ;
                DataTable dt = db.get_Geo_Route12(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                rname = dt.Rows[0][0].ToString();               
                ddlrttype.SelectedValue = dt.Rows[0][0].ToString();
                bindpoints(ddlrno.SelectedValue.ToString());
                binddata();
                GMap1.enableServerEvents = true ;
              
            }
        }
        public void binddata()
        {
 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserID"] != null)
            {
                //DBClass loc = new DBClass();

                //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

                //GMap1.resetMarkers();
                //GMap1.resetPolylines();

#pragma warning disable CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'
                GMap1.enableScrollWheelZoom = true;
#pragma warning restore CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'
                DataTable getrno = db.get_Geornos(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
               
                string TextBox1 = getrno .Rows [0][3].ToString ();
                string TextBox3 = getrno .Rows [0][4].ToString ();
#pragma warning disable CS0219 // The variable 'rkm' is assigned but its value is never used
                double rkm = 0;
#pragma warning restore CS0219 // The variable 'rkm' is assigned but its value is never used


                DataTable report = db.getMovinggeoData12(getrno.Rows[0][5].ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));
                List<GLatLng> points = new List<GLatLng>();
                GLatLng latlng = null;
                Subgurim.Controles.GMarker oMarker = null;

                Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

               
                for (int i = 0; i < report.Rows.Count; i++)
                {                  
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
#pragma warning disable CS0618 // 'GMap.addControl(GControl)' is obsolete: 'Use Add(GControl) instead'
                    GMap1.addControl(extMapType);
#pragma warning restore CS0618 // 'GMap.addControl(GControl)' is obsolete: 'Use Add(GControl) instead'
                    GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                    //GMarker marker = new GMarker(gLatLng);
                    oMarker = new Subgurim.Controles.GMarker(gLatLng);
                    //GMap1.setCenter(latlng);
                    SPin sPin;
                    if (i == 0)
                    {
                        sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                    }
                    else if (i < report.Rows.Count - 1)
                    {
                        if (Convert.ToInt32(report.Rows[i][4]) == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }

                        else if (Convert.ToInt32(report.Rows[i][3]) == 0)
                        {

                            sPin = new SPin(0.5, 0, System.Drawing.Color.Blue, 14, PinFontStyle.bold, "");

                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                        }
                    }

                    else
                    {
                        sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                    }
                    //sPin.text = "Start Point";
                    icon = new Subgurim.Controles.GIcon(sPin.ToString());

                    Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                    oMarker.options = options;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
                    GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
                    latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                    points.Add(latlng);
                    options.icon = icon;
                  

                }
                if (report.Rows.Count > 0)
                {
                    //points.Add(latlng + new GLatLng(-0.5, 4.2));
                    GPolyline line = new GPolyline(points, "FF0000", 2);
#pragma warning disable CS0618 // 'GMap.addPolyline(GPolyline)' is obsolete: 'Use Add(GPolyline) instead.'
                    GMap1.addPolyline(line);
#pragma warning restore CS0618 // 'GMap.addPolyline(GPolyline)' is obsolete: 'Use Add(GPolyline) instead.'

                }

            }

        }      

        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {

            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);
           // return default(string);
        }

        protected void Btn_Clear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        public void drawcircle(double lt, double lg, double r, int gp, string vn,string adress)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1);// 16, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            options.title = "Route No :-" + vn+","+"Location:-"+adress ;
            oMarker.options = options;

        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            refreshmap();
            binddata();
        }
        public void refreshmap()
        {
            p = 0;
            lt = 0; lng = 0;          
            txtradius.Text = string.Empty;
            txtlocation.Text = string.Empty;
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            GMap1.resetPolylines();     
            bindpoints(ddlrno.SelectedValue.ToString());

        }      

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            if (lt != 0 && lng != 0)
            {
                DataTable getrno = db.get_Geornos(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());    
                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                GeoCode objAddress = new GeoCode();
                objAddress = GMap.geoCodeRequest(new GLatLng(lt, lng), sMapKey);
                StringBuilder sb = new StringBuilder();
                string address = txtlocation.Text;             
                int n;
                string date = "";
                string ftime = "";
                string ttime = "";

                        
                    date = DateTime .Now .ToString ();
                    ftime =getrno.Rows[0][3].ToString();
                    ttime =getrno.Rows[0][4].ToString();
              


                    n = db.create_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString(), lt.ToString(), lng.ToString(), txtradius.Text, address, ddlrttype.SelectedValue.ToString(), date);
                if (n == 0)
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Successfully Point Created ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    btnfinish.Enabled = true ;
                    refreshmap(); 
                    binddata();

                }
                else
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('You Reached Maximum Points ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                }

            }
            else
            {
                Response.Redirect(Request.RawUrl);
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point On Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
               
            }
            //bindpoints(ddlrno.SelectedValue.ToString());
        }
        public void bindpoints(string rno)
        {
            if (rno != "--")
            {
                GMap1.resetMarkers();
                GMap1.resetPolygon();
                DataTable dt = db.get_Geo_Routepointsnew(rno, Session["UserID"].ToString());
                for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                {
                    drawcircle(Convert.ToDouble(dt.Rows[i][3].ToString()), Convert.ToDouble(dt.Rows[i][4].ToString()), Convert.ToDouble(dt.Rows[i][5].ToString()) / 1609.3, j, rno, dt.Rows[i][6].ToString());
                }
                gvmappoints.DataSource = dt;
                gvmappoints.DataBind();
            }

        }

        protected void gvmappoints_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                sno = 1;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblsno = (Label)e.Row.FindControl("lblsno");
                lblsno.Text = sno.ToString();
                sno++;
            }
        }

        protected void btnfinish_Click(object sender, EventArgs e)
        {
            p = 1;
            lt = 0; lng = 0;
            txtradius.Text = string.Empty;
            txtlocation.Text = string.Empty;
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            GMap1.resetPolylines();
            bindpoints(ddlrno.SelectedValue.ToString());
            BtnSubmit.Enabled = false;
            BtnClear.Enabled = false;

        }

       

    }
}

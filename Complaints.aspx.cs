﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
namespace Tracking
{
    public partial class Complaints : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            binddata();
        }
        public void binddata()
        {
            gvreport.DataSource = null;
            gvreport.DataBind();
            Label2.Text = "";
            DataTable dt = db.GetCompalints(Session["UserID"].ToString());
            DataTable dt1 = new DataTable();

            dt1.Columns.Add("cid", typeof(string));
            dt1.Columns.Add("cdate", typeof(string));
            dt1.Columns.Add("vno", typeof(string));
            dt1.Columns.Add("ctype", typeof(string));
            dt1.Columns.Add("ctypemsg", typeof(string));
            dt1.Columns.Add("availbledate", typeof(string));
            dt1.Columns.Add("availblelocation", typeof(string));
            dt1.Columns.Add("cstatus", typeof(string));
            dt1.Columns.Add("ccloseddate", typeof(string));
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt1.NewRow();
                    if (!string.IsNullOrEmpty(dt.Rows[i][0].ToString()))
                    {
                        DateTime cdate = Convert.ToDateTime(dt.Rows[i][0].ToString());
                        string id = dt.Rows[i][1].ToString();
                        string tdate = cdate.ToString("yyyyMMdd");
                        string ticketid = tdate + id;
                        string cdate12 = cdate.ToString("dd/MM/yyyy hh:mm tt");
                        string vno = dt.Rows[i][2].ToString();
                        string ctype = dt.Rows[i][4].ToString();
                        string ctypemsg = dt.Rows[i][5].ToString();
                        DateTime availbledate = Convert.ToDateTime(dt.Rows[i][6].ToString());
                        string availbledate12 = availbledate.ToString("dd/MM/yyyy hh:mm tt");
                        string availblelocation = dt.Rows[i][7].ToString()+","+ dt.Rows[i][11].ToString();
                        string cstatus = "";
                        string ccloseddate = "";
                        if (dt.Rows[i][10].ToString() == "0")
                        {
                            cstatus = "Opened";
                        }
                        else
                        {
                            cstatus = "Closed";
                            if (!string.IsNullOrEmpty(dt.Rows[i][13].ToString()))
                            {
                                DateTime closedtt = Convert.ToDateTime(dt.Rows[i][13].ToString());
                                ccloseddate = closedtt.ToString("dd/MM/yyyy hh:mm tt");
                            }
                           
                        }

                        // string ccloseddate=dt.Rows[i][13].ToString();

                        dr["cid"] = ticketid;
                        dr["cdate"] = cdate12;
                        dr["vno"] = vno;  
                        dr["ctype"] = ctype;
                        dr["ctypemsg"] = ctypemsg;
                        dr["availbledate"] = availbledate12;
                        dr["availblelocation"] = availblelocation;
                        dr["cstatus"] = cstatus;
                        dr["ccloseddate"] = ccloseddate;
                        dt1.Rows.Add(dr);

                    }

                }
            }
            if (dt1.Rows.Count <= 0)
            {
                Label2.Text = "Complaint History not found....";
               // btnDownload.Enabled = false;
            }
            else
            {
                //dt1.AcceptChanges();
                //DataView ddddd = new DataView(dt1);
                //ddddd.Sort = "Datetime ASC";

                gvreport.DataSource = dt1;

                gvreport.DataBind();
                //btnDownload.Enabled = true;
            }

        }
    }
}
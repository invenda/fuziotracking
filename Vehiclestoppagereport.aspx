﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Vehiclestoppagereport.aspx.cs"
    Inherits="Tracking.Vehiclestoppagereport" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout ="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <div style="width: 100%; height: 30px; font-size: 25px; text-align: center; color: Blue;">
            Vehicle Stoppage/Idling Report
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="height: 20px;">
        </div>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label1" Text="Select the Vehicle" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                ControlToValidate="ddlMapTOVehicle" ErrorMessage="-Select the Vehicle">*</asp:RequiredFieldValidator></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                DataTextField="Vehicalnumber" Height="23px" Width="188px">
                            </asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="Label2" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="txtdate"
                                ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            &nbsp;:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtdate" runat="server" Width="175px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtdate"
                                OnClientDateSelectionChanged="checkDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:Button ID="btnsubmit" runat="server" Text="Get Report" ValidationGroup="Group1"
                                OnClick="btnsubmit_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btndownload" runat="server" Text="DownLoad" Visible="false" OnClick="btndownload_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnclear" runat="server" Text="Clear" Width="60px" OnClick="btnclear_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
      
            <div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div style="height: auto; font-family: Times New Roman;">
                    <div style="height: 20px;">
                    </div>
                    <div style="vertical-align: middle; text-align: center;">
                        <asp:Label ID="lblnoact" runat="server" Font-Bold="true" Font-Size="20px" ForeColor="Red"></asp:Label>
                    </div>
                    <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                        RowStyle-ForeColor="Black" ShowFooter="true" Width="1000px" OnRowDataBound="gvreport_RowDataBound"
                        OnRowCreated="gvreport_RowCreated">
                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                        <Columns>
                            <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="600px"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Time" ItemStyle-HorizontalAlign="Center" FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lblftime" runat="server" Text='<%# Bind("ftime1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Time" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocation1" runat="server" Text='<%# Bind("ttime1") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal" runat="server" Text="Total (hh:mm)"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lblduration" runat="server" Text='<%# Bind("duration1") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblftotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvideal" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                        RowStyle-ForeColor="Black" ShowFooter="true" Width="1000px" OnRowCreated="gvideal_RowCreated">
                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                        <Columns>
                            <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="600px"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocation" runat="server" Text='<%# Bind("location1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Time" ItemStyle-HorizontalAlign="Center" FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lblftime" runat="server" Text='<%# Bind("ftime1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Time" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocation" runat="server" Text='<%# Bind("ttime1") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lbltotal" runat="server" Text="Total (hh:mm)"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"
                                FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lblduration" runat="server" Text='<%# Bind("duration1") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblftotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                
               <asp:PostBackTrigger ControlID="btndownload" />
            </Triggers>
        </asp:UpdatePanel>
        </div>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

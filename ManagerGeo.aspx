﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagerGeo.aspx.cs"  MasterPageFile="~/Manager_geo.Master" Inherits="Tracking.ManagerGeo" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

   

 <div style="width: 100%;">
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ValidationGroup="circleinfo" ShowSummary="False" />
    <div >
        <div style="text-align: center; color: Blue; font-size: x-large; font-style: italic;">
           <asp:Label ID="lblhead" runat="server" Text="Create Manager Geofence Page"></asp:Label>
        </div>
        <div style="height: 40px; width: 95%; text-align: center; color :Black;">
            <table width="100%">
                <tr>
                    <td align="center" class="style1">
                        &nbsp;<asp:Label ID="lblradous" runat="server" Font-Bold="true" Text="Enter Radius in meters:"></asp:Label>
                        &nbsp; &nbsp;<asp:TextBox ID="txtradius" runat="server" Width="120px" Height="25px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtradius"
                            ErrorMessage="Enter radius" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                        ( in Mtrs)
                    </td>
                    <td align="left"  style="vertical-align:bottom;">
                         &nbsp;<asp:Label ID="lblname" runat="server" Font-Bold="true" Text="Enter Geo Name:"></asp:Label>
                        &nbsp; &nbsp;<asp:TextBox ID="txtGname" runat="server" Width="230px" Height="25px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtradius"
                            ErrorMessage="Enter Geo Name" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>                       
                    </td>
                    <td align="left">
                        <asp:Button ID="btnSubmit" runat="server" Font-Bold="true" Text="Submit" ValidationGroup="circleinfo"
                            Width="77px" onclick="btnSubmit_Click" />
                    </td>
                     <td align="left">
                        <asp:Button ID="btnClear" runat="server" Font-Bold="true" Text="Clear" 
                             Width="77px" onclick="btnClear_Click"  />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <cc1:GMap ID="GMap1" runat="server"  enableServerEvents="true" Width="100%" Height="600px" 
                OnClick="GMap1_Click" />
        </div>
    </div>
   </div>
</asp:Content>

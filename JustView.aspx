﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JustView.aspx.cs" Inherits="Tracking.JustView"
    MasterPageFile="~/ESMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
        <div id="sublogo" style="text-align: center; color: Blue;">
            <h1>
                <a>View Customer Registration Details</a>
            </h1>
        </div>
    </div>
    <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0; color: Black;">
        <table width="100%">
            <tr>
                <td style="width: 200px;">
                </td>
                <td style="width: auto;">
                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                </td>
                <td style="width: auto;">
                </td>
                <td style="width: auto;">
                </td>
            </tr>
            <tr>
                <td>
                    Customer Type
                </td>
                <td>
                    :<asp:Label ID="lblCustType" runat="server"></asp:Label></td>
                <td>
                    Name of the Customer/Company
                </td>
                <td>
                    :<asp:Label ID="lblCustName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Billing Address Line 1
                </td>
                <td>
                    :<asp:Label ID="lblBillAdd1" runat="server"></asp:Label></td>
                <td>
                    Billing Address Line 2
                </td>
                <td>
                    :<asp:Label ID="lblBillAdd2" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    City
                </td>
                <td>
                    :<asp:Label ID="lblCity" runat="server"></asp:Label></td>
                <td>
                    State
                </td>
                <td>
                    :<asp:Label ID="lblState" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Contact Number: (Area Code)
                </td>
                <td>
                    :<asp:Label ID="lblAreaCode" runat="server"></asp:Label><asp:Label ID="lblConNum" runat="server"></asp:Label></td>
                <td>
                   Default Mobile Number for SMS Alert:
                </td>
                <td>
                    :<asp:Label ID="lblMobNum" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Date of Registration
                </td>
                <td>
                    :<asp:Label ID="lblDOR" runat="server"></asp:Label></td>
                <td>
                    Email ID
                </td>
                <td>
                    :<asp:Label ID="lblEmail" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    </td>
                     <td>
                    </td>
                     <td>
                         Alternate Email ID:</td>
                     <td>
                     :<asp:Label ID="lblaEmail" runat="server"></asp:Label></td>
                    </td>
            </tr>
            <tr>
                <td style="height: 20px;" colspan="4">
                    </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="btnClose" runat="server" Text="Home" OnClick="btnClose_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="BtnEdit" runat="server" Text="Edit" Width="70px" 
                        onclick="BtnEdit_Click" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

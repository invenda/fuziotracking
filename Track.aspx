﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Track.aspx.cs" Inherits="Tracking.Track"
    MasterPageFile="~/ESL1.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>

<asp:Content ContentPlaceHolderID="MainContent" ID="Content1" runat="server">
    
    <link href="CSS/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
        function myclick(a, b) {
            map.panTo(new GLatLng(a, b));
            //GEvent.trigger(gmarkers[i],"click");
        }
        function openTrackHistory() {
            window.open("TrackingHistory.aspx");
            return false;
        }
        function openRouteHistory() {
            window.open("RouteHistory.aspx");
            return false;
        }
        function openDownloadReport() {
            window.open("GenerateReport.aspx");
            return false
        }
        function openCustomerMgnmt() {
            window.open("CustDetails.aspx");
            return false
        }
        function openViewDetails() {

            window.open("JustView.aspx");
            return false
        }
        function openVehMgmt() {
            window.open("CreateVehicles.aspx");
            return false
        }
        function openVehView() {
            window.open("ViewCustVeh.aspx");
            return false
        }
        function openEngineStatusReport() {
            window.open("EngineStatusReport.aspx");
            return false
        }
        function openFuelEntry() {
            window.open("FuelFill.aspx");
            return false
        }
        function openFuelEntryRpt() {
            window.open("FuelRpt.aspx");
            return false
        }
        function openKMcor() {
            window.open("Corrections.aspx");
            return false
        }
        function openTotalKMRpt() {
            window.open("report2.aspx");
            return false;
        }
        function opengeo() {
            window.open("Geo.aspx");
            return false;
        }
        function openoverspeed() {
            window.open("OverSpeedreport.aspx");
        }
        function openalertreport() {
            window.open("VehicleAlertReport.aspx");
        }
        function opengeotrip() {
            window.open("GeoTrip.aspx");
            return false;
        }
        function alertsdetails(x) {
            alert(x.join('\n'));
        }
        function opendategeo() {
            window.open("dategeofence.aspx");
            return false;
        }
        function opendailyreport() {
            window.open("VehicalDailyReport.aspx");
            return false;
        }
        function stopreport() {
            window.open("Vehiclestoppagereport.aspx");
            return false;
        }
        function BSreport() {
            window.open("BlackSpotreport.aspx");
            return false;
        }
        function openAlerts() {
            window.open("SetAlerts.aspx");
            return false;
        }
        function opengeoview() {
            window.open("ViewGeoRoutePoints.aspx");
            return false;
        }
        function Towereport() {
            window.open("TowedReport.aspx");
            return false;
        }
        function Daykmreport() {
            window.open("DayKMRunReport.aspx");
            return false;
        }
        function openDaygraph() {
            window.open("Pieday.aspx");
            return false
        }
        function openFuelgraph() {
            window.open("Fuel.aspx");
            return false
        }
        function opentemplog() {
            window.open("Tempdatalog.aspx");
            return false
        }
        
    </script>

    <script language="javascript" type="text/javascript">
        function blinkFont() {
            document.getElementById("lbltemp").style.color = "red"
            setTimeout("setblinkFont()", 1000)
        }
    </script>

    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
    <div style="width: auto; height: 100%; background-color: Gray;">
        <table style="width: 100%;">
            <tr>
                <td colspan="2" style="width: 100%; height: 40px;">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table id="pnlToolbar" class="submenu-control" style="vertical-align: middle;" cellpadding="0"
                                cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align:top; border-bottom-style: solid; height: 40px; width: 200px;
                                            border-bottom-width: thin;">
                                            <div style="height: 30px; width: 100px; background-color: Silver; border-bottom-width: thin; text-align:left;">                                              
                                               
                                                <asp:Button ID="btnhome" runat="server" Text="Home"  Font-Bold="true" CssClass="button123"
                                                    Font-Size="Small" Width="99px" Font-Italic="True" OnClick="btnhome_Click1" Height="25px" />
                                            </div>
                                            <div style="height: 20px; width: 100px;">
                                                <%--<asp:Label ID="Label4" runat="server" Font-Bold="true" ForeColor="White" Text="Daily  Alerts:"></asp:Label>--%>
                                                <asp:Button ID="Btndalerts" runat="server" Text="Daily  Alerts :" Font-Bold="true"
                                                    Visible="false" BackColor="Green" ForeColor="White" Height="20px" Width="100%"
                                                    OnClick="Btndalerts_Click" />
                                            </div>
                                        </td>
                                        <td style="width: 100%; text-align: left; border-bottom-style: solid; border-bottom-width: thin;">
                                            <div style="height: 20px; border-bottom-style: solid; border-bottom-width: thin;">
                                                <table style="width: 100%; height: 20px;">
                                                    <tr>
                                                        <td style="width: 50%; height: 100%; vertical-align: middle;" align="center">
                                                            <asp:Label ID="lblmalerts" runat="server" Font-Bold="true" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblbranch" runat="server" Font-Bold="true" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%; height: 20px; vertical-align: middle; font-weight: bold;"
                                                            align="right">
                                                            <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Black" Text="Engine Status :"></asp:Label>
                                                            &nbsp;&nbsp;
                                                            <asp:Image ID="imgst" runat="server" Width="14px" Height="14px" />
                                                            <asp:Label Text="Stopped" runat="server" ID="Lbl1"></asp:Label>&nbsp;&nbsp;
                                                            <asp:Image ID="imgidl" runat="server" Width="14px" Height="14px" />
                                                            <asp:Label Text="Idle" runat="server" ID="lbl2"></asp:Label>&nbsp;&nbsp;
                                                            <asp:Image ID="imgmv" runat="server" Width="14px" Height="14px" />
                                                            <asp:Label ID="lbl3" runat="server" Text="Moving"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            Current Time &nbsp;:
                                                            <asp:Label ID="LblSCT" runat="server" Font-Bold="True" Font-Italic="False"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="height: 20px; border-top-width: medium; vertical-align: middle;">
                                                <table style="width: 100%; height: 100%;">
                                                    <tr>
                                                        <%-- <td style="width: 50px; vertical-align: middle;">
                                                            <asp:Image Height="20px" Width="50px" runat="server" ID="imgarrow" ImageUrl="~//Images/Larrow1.PNG" />
                                                        </td>
                                                        <td style="width: 10%; vertical-align: middle;">
                                                            &nbsp;
                                                            <asp:Label ID="lblarrow" runat="server" Text="Click For Details" Font-Bold="true"></asp:Label>
                                                        </td>--%>
                                                        <td>
                                                            <marquee> 
                                                            <asp:Label ID="lblalerts2" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                            <asp:Label ID="lbloffalerts" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                            </marquee>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="height: auto; width: 100%;">
                    <tr style="height: 100%; width: 100%;">
                        <td style="width: 71%; height: 100%;">
                            <asp:Timer ID="Timer2" runat="server" Interval="1" OnTick="Timer2_Tick">
                            </asp:Timer>
                            <cc1:GMap ID="GMap1" runat="server" Height="805px" Width="100%" OnClick="GMap1_Click" />
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="Panel1" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table style="border-collapse: collapse; height: 100%;" border="0" bordercolor="#111111"
                                        cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr valign="top" style="background-color: Gray; height: 390px">
                                                <td width="100%" valign="top">
                                                    <div style="position: relative; vertical-align: top; height: 384px; overflow: auto;
                                                        top: 9px; left: 0px;">
                                                        <asp:DataGrid ID="gdVehicles" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            DataKeyField="VehicalNumber" SelectedItemStyle-BackColor="Silver" SelectedItemStyle-Font-Bold="false"
                                                            DataKeyNames="VehicalNumber" AlternatingItemStyle-ForeColor="Black" 
                                                            BorderWidth="2" BorderColor="Black" OnItemDataBound="gdVehicles_ItemDataBound">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-BackColor="Green" HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black"
                                                                    HeaderStyle-BorderWidth="2">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rbtn" runat="server" OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                                                            AutoPostBack="true" OnCheckedChanged="rbtn_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Vehicle RTO No." HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                                                    ItemStyle-BorderColor="Black">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="vehicleno" Text='<%# Bind("VehicalNumber") %>' OnClick="vehicleno_Click"
                                                                            Font-Strikeout="false" runat="server" Font-Bold="true"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderStyle-BackColor="Green" HeaderStyle-Width="16px" HeaderStyle-ForeColor="White"
                                                                    HeaderStyle-BorderColor="Black">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="Simg" ImageUrl='<%# Eval("ImageUrl") %>' runat="server" Height="16px" Width="16px" BackColor="Transparent"   meta:resourcekey="SimgResource1"/>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                                                    HeaderStyle-Width="50px" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Black"
                                                                    HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2" ItemStyle-BorderColor="Black">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelstatus" Text='<%# Bind("Status") %>' ToolTip='<%# Eval("ToolTip") %>' runat="server" Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Unit_Serial No." HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                                                    HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Black" HeaderStyle-BorderColor="Black"
                                                                    HeaderStyle-BorderWidth="2" ItemStyle-BorderColor="Black">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Alerts" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                                                    HeaderStyle-Width="50px" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-VerticalAlign="Middle" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                                                    ItemStyle-BorderColor="Black">
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btnalerts" runat="server" Font-Size="9px" Height="17px" Text='<%# Bind("Vehicle_Alerts_Status") %>'
                                                                            Width="50px" BackColor='White' ForeColor="Black" OnClick="btnalerts_Click" Font-Bold="true" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="height: auto;">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="background-color: White;" align="center" colspan="2">
                                                                    &nbsp;
                                                                    <asp:Label ID="Label5" runat='server' Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                        Font-Size="Small" Font-Strikeout="False" Text="Tracking Information"></asp:Label>
                                                                </td>
                                                                <%-- <td style="background-color: White" align="left">
                                                                    &nbsp;
                                                                    <asp:Label ID="Label2" runat='server' Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                        Font-Size="Small" Font-Strikeout="False">Information</asp:Label>
                                                                </td>--%>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; width: 140px; color: White">
                                                                    &nbsp;Current Unit Status
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblUnitStat" runat='server' Font-Bold="True" ForeColor="White"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Today&#39;s Availability
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblavailable" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Last Data Recorded
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblLocDT" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Trip&nbsp;
                                                                    <asp:Label ID="lblgeotype" runat="server" ForeColor="Red"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblgeofence" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:LinkButton
                                                                        ID="lbtnview" runat="server" Text="View" Visible="false" Font-Bold="true" OnClientClick="opengeoview()"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;GPS Signal
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblGPSant" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle RTO Number
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblVehNo" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Vehicle Model
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblVehModel" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Latitude &amp; Longitude
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblLocation" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658;">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Ignition
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblEngine" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4;">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle Engine Status
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblEnginestatus" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Speed(in km/h)
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblspeed" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Over Speed&nbsp;
                                                                    <asp:Label ID="lblovs" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lbloverspeed" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Today Total Run&nbsp;
                                                                    <asp:Label ID="Label3" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lbltkm" runat='server' Font-Bold="True" ForeColor="White"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Total Meter Reading
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblTotKmRun" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Fuel Status(in ltr)
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblFuel" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Input Power
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblPower" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;Vehicle Battery
                                                                </td>
                                                                <td>
                                                                    </span> :<asp:Label ID="lblVehBatStat" runat='server' Font-Bold="True" ForeColor="White"></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td>
                                                                    <asp:Label ID="lblcabin" runat="server" Text="Cabin Temperature" Font-Bold="True"></asp:Label>
                                                                    <asp:Label ID="lblreefer" runat="server" Text="Reefer Temperature" Font-Bold="True"
                                                                        Visible="false"></asp:Label>
                                                                </td>
                                                                <%--<td style="font-weight: bold;">
                                                                    &nbsp;Cabin Temperature
                                                                </td>--%>
                                                                <td id="gggg">
                                                                    :<asp:Label ID="lbltemp" runat='server' Font-Bold="True"></asp:Label></span><asp:Label
                                                                        ID="lbltemp2" runat="server" Font-Bold="True" Font-Size="Smaller"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; color: White">
                                                                    &nbsp;AC Status&nbsp;&nbsp;
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblac" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold; color: black">
                                                                    &nbsp;Driver Name&nbsp;&nbsp;
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblDriverName" runat='server' Font-Bold="True" ForeColor="black"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658">
                                                                <td style="font-weight: bold; height: 16px; color: White">
                                                                    &nbsp;Driver Mobile No.
                                                                </td>
                                                                <td style="height: 16px">
                                                                    :<asp:Label ID="lblDriverMobNo" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold; color: black;">
                                                                    &nbsp;Owner/Transporter
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblOwnerName" runat='server' Font-Bold="True" ForeColor="black"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #173658;">
                                                                <td colspan="2" style="font-weight: bold; height: 51px; color: White;">
                                                                    &nbsp;Location :<asp:Label ID="lblPlace" runat='server' Font-Bold="True" ForeColor="White"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <%-- <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ctl00$MainContent$gdVehicles$ctl02$rbtn" EventName="CheckedChanged" />
                <asp:AsyncPostBackTrigger ControlID="ctl00$MainContent$gdVehicles$ctl02$vehicleno"
                    EventName="Click" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </div>
</asp:Content>

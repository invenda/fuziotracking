﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Calc : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Veh"]) && !string.IsNullOrEmpty(Request.QueryString["Date"]))
                {
                    String VehNo = Request.QueryString["Veh"].ToString();
                    String dt = Request.QueryString["Date"].ToString();

                    lblVehno.Text = VehNo;
                    DateTime td = Convert.ToDateTime(dt);


                    lbldate.Text = td.ToString("dd-MM-yyy"); 

                    DataTable fuelop = fuelliter12(VehNo, dt);
                    if (fuelop.Rows.Count > 0)
                        lblopen.Text = fuelop.Rows[0][0].ToString();

                    DataTable fuelclo = fuelliter123(VehNo, dt);
                    if (fuelclo.Rows.Count > 0)
                    lblclose.Text = fuelclo.Rows[0][0].ToString();

                    DataTable milage = db.getmilage(VehNo);

                    DataTable dt2 = binddata1(VehNo, dt);
                    double km = 0;
                    for (int i = 0; i <= 24; i++)
                    {
                        km += Convert.ToDouble(dt2.Rows[i][0].ToString());
                    }
                    string kms = km.ToString();
                    lblkm.Text = kms.ToString();

                     string a=string.Empty;
                     if (milage.Rows.Count > 0 && milage.Rows[0][0].ToString()!="0")
                         a = (Convert.ToDouble(kms) / Convert.ToDouble(milage.Rows[0][0].ToString())).ToString();
                     Double b = (double)Math.Round(Convert.ToDouble(a), 2);


                     lblfuelfill.Text = b.ToString();
                     Double ffuel = 0;
                     if (fuelop.Rows.Count > 0 && fuelclo.Rows.Count > 0)
                     {
                          ffuel = Convert.ToDouble((Convert.ToDouble(fuelop.Rows[0][0].ToString()) - Convert.ToDouble(fuelclo.Rows[0][0].ToString())));
                     }
                    int apfuel = Convert.ToInt32(Convert.ToDouble(a) - ffuel);
                    lblff.Text = apfuel.ToString();

                    DataTable FuFTo = db.Fuel_fill2(VehNo, dt);
                    int fliter = 0;
                    if (FuFTo.Rows.Count != 0)
                    {
                        for (int i = 0; i < FuFTo.Rows.Count; i++)
                        {
                            fliter += Convert.ToInt32(FuFTo.Rows[i][2]);
                        }
                    }

                    fuel.Text = fliter.ToString();
                }

            }
        }
        private DataTable fuelliter(string VehNo, string date)
        {           
            DataTable dttemp = new DataTable();
            dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            string gDate = date;
            DataTable FuFTo = db.Fuel_fill2(VehNo, gDate);
            int fliter = 0;
            DataTable fuelop = fuelliter12(VehNo, date);
            double fuelwe = 0;
            if (fuelop.Rows.Count != 0)
            {
                fuelwe = Convert.ToDouble(fuelop.Rows[0][0].ToString());
            }
            double fuelwe1 = fuelwe;
            double totalfuel = fliter + fuelwe1;
            if (FuFTo.Rows.Count != 0)
            {

                for (int i = 0; i < FuFTo.Rows.Count; i++)
                {
                    fliter += Convert.ToInt32(FuFTo.Rows[i][2]);
                }
                int n = 0;
                for (int m = 1; m < 1440; m++)
                {
                    DataRow dr = dttemp.NewRow();
                    n++;
                    dr["sno"] = n.ToString();
                    dr["fuel"] = totalfuel.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
        private DataTable fuelliter12(string VehNo, string date)
        {
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            DataTable fu = db.prevfuelstaus1(VehNo, date);
            if (fu.Rows.Count != 0)
            {
                DataRow dr = dttemp.NewRow();
                double string_val_fuel = Convert.ToDouble(fu.Rows[0][3].ToString());
                double fuel = 0;
                DataTable dtf = db.GetVehiclesfuel(VehNo, Session["UserId"].ToString());
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                if (Tank_typ == 0)
                {
                    fuel = (string_val_fuel) / (Fuel_Cali);
                    fuel = fuel * tank_capacity;
                    fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = fuel.ToString();
                    dttemp.Rows.Add(dr);
                }
                else
                {
                    fuel = (string_val_fuel) / (Fuel_Cali);
                    fuel = fuel * tank_capacity;
                    fuel = tank_capacity - fuel;
                    fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = fuel.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
        private DataTable fuelliter123(string VehNo, string date)
        {
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            string fdate14 = date + " 12:00 AM";
            string tdate13 = date + " 11:59 PM";
            DataTable fu = db.prevfuelstaus111(VehNo, fdate14, tdate13);
            if (fu.Rows.Count != 0)
            {
                DataRow dr = dttemp.NewRow();
                double string_val_fuel = Convert.ToDouble(fu.Rows[0][3].ToString());
                double fuel = 0;
                DataTable dtf = db.GetVehiclesfuel(VehNo, Session["UserId"].ToString());
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                if (Tank_typ == 0)
                {
                    fuel = (string_val_fuel) / (Fuel_Cali);
                    fuel = fuel * tank_capacity;
                    fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = fuel.ToString();
                    dttemp.Rows.Add(dr);
                }
                else
                {
                    fuel = (string_val_fuel) / (Fuel_Cali);
                    fuel = fuel * tank_capacity;
                    fuel = tank_capacity - fuel;
                    fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = fuel.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
        public DataTable binddata1(string VehNo, string date)
        {
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable DrivDet = db1.GerDriverDetails(VehNo, Session["UserID"].ToString());
            DataTable dt = db1.getRhours();
            DataTable dt1 = new DataTable();

            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Rows.Add(0);
            string fdate12 = date + " 12:00 AM";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt1.NewRow();
                string fdate = date + " " + dt.Rows[i][0];
                string tdate = date + " " + dt.Rows[i][1];
                DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, VehNo);
                double TKMR1 = 0;
                if (dt2.Rows.Count != 0)
                {
                    DataTable dt12 = db1.Select_TotalRunkm(VehNo, fdate12, tdate);
                    for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                    {
                        TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                    }
                    if (dt12.Rows.Count != 0)
                    {
                        fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                    }
                    TKMR1 = (double)Math.Round(TKMR1, 2);
                }
                dr["TKMR"] = TKMR1.ToString();
                dt1.Rows.Add(dr);
            }
            return dt1;
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

    }
}

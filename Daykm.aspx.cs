﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Daykm : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        double count = 0;
        double rcount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable stat = db.GetVehicles(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());           
                        stat.AcceptChanges();
                        GVehicles.DataSource = stat;
                        GVehicles.DataBind();
                    }
                    else
                    {

                        bindvehicle();
                    }
                }
            }
        }
        public void bindvehicle()
        {
            DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());           
            stat.AcceptChanges();
            GVehicles.DataSource = stat;
            GVehicles.DataBind();
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("Country") });
            foreach (GridViewRow row in GVehicles.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("CheckBox1") as CheckBox);
                    if (chkRow.Checked)
                    {
                        //string name = row.Cells[1].Text;
                        string country = (row.Cells[1].FindControl("lblvehicleno") as Label).Text;
                        dt.Rows.Add(country);
                    }
                }
            }

            if (dt.Rows.Count > 0)
            {
                DataTable dtkm = new DataTable();
                dtkm.Columns.Add("Vehicle_number", typeof(string));
                dtkm.Columns.Add("1Hour", typeof(string));
                dtkm.Columns.Add("2Hour", typeof(string));
                dtkm.Columns.Add("3Hour", typeof(string));
                dtkm.Columns.Add("4Hour", typeof(string));
                dtkm.Columns.Add("5Hour", typeof(string));
                dtkm.Columns.Add("6Hour", typeof(string));
                dtkm.Columns.Add("7Hour", typeof(string));
                dtkm.Columns.Add("8Hour", typeof(string));
                dtkm.Columns.Add("9Hour", typeof(string));
                dtkm.Columns.Add("10Hour", typeof(string));
                dtkm.Columns.Add("11Hour", typeof(string));
                dtkm.Columns.Add("12Hour", typeof(string));
                dtkm.Columns.Add("13Hour", typeof(string));
                dtkm.Columns.Add("14Hour", typeof(string));
                dtkm.Columns.Add("15Hour", typeof(string));
                dtkm.Columns.Add("16Hour", typeof(string));
                dtkm.Columns.Add("17Hour", typeof(string));
                dtkm.Columns.Add("18Hour", typeof(string));
                dtkm.Columns.Add("19Hour", typeof(string));
                dtkm.Columns.Add("20Hour", typeof(string));
                dtkm.Columns.Add("21Hour", typeof(string));
                dtkm.Columns.Add("22Hour", typeof(string));
                dtkm.Columns.Add("23Hour", typeof(string));
                dtkm.Columns.Add("24Hour", typeof(string));



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = null;
                    string vno = dt.Rows[i][0].ToString();
                    DateTime StringDT1 = Convert.ToDateTime(txtdate.Text);
                    DateTime dayindiffence1 = StringDT1.AddDays(1);
                    string day, month;
                    if(dayindiffence1.Day < 10)
                    {
                        day = "0"+dayindiffence1.Day;
                    }
                    else
                    {
                        day = dayindiffence1.Day.ToString();
                    }

                    if(dayindiffence1.Month < 10)
                    {
                        month = "0"+dayindiffence1.Month;
                    }
                    else
                    {
                        month = dayindiffence1.Month.ToString();
                    }
                    
                    int year = dayindiffence1.Year;


                    DataTable fuziokmh = db.Fuzio_hourlykm(dt.Rows[i][0].ToString(), txtdate.Text, month +"/"+day+"/"+dayindiffence1.Year);
                    
                    
                    //create new row
                    dr = dtkm.NewRow();
                    dr[0] = dt.Rows[i][0].ToString();
                    if(fuziokmh.Rows.Count > 0)
                    {
                        for (int j = 0; j < fuziokmh.Rows.Count; j++)
                        {
                            DateTime moment;
                            moment = Convert.ToDateTime(fuziokmh.Rows[j][3]);
                            int hour = moment.Hour;

                            decimal m = Convert.ToDecimal(fuziokmh.Rows[j][4]);
                            decimal d = Math.Round(m, 2);

                            dr[hour + 1] = d;
                        }
                    }
                    else
                    {
                        for (int j = 1; j <= 24; j++)
                        {
                            dr[j] = "0";
                        }
                    }
                   
                   

                    //add the row to DataTable
                    dtkm.Rows.Add(dr);

                }


                DataTable data1 = dtkm;
                gvreport.DataSource = data1;
                gvreport.DataBind();
            }
            else
            {
                string message = "alert('Please select Vehicles ')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

            }
            //btndownload.Visible = true;
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                count = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rcount = 0;
                Label lbl1 = e.Row.FindControl("lblH1") as Label;
                rcount += Convert.ToDouble(lbl1.Text);
                if (Convert.ToDouble(lbl1.Text) == 0)
                {
                    lbl1.Text = " ";
                }
                else if (Convert.ToDouble(lbl1.Text) > 0)
                {
                    e.Row.Cells[1].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl2 = e.Row.FindControl("lblH2") as Label;
                rcount += Convert.ToDouble(lbl2.Text);
                if (Convert.ToDouble(lbl2.Text) == 0)
                {
                    lbl2.Text = " ";
                }
                else if (Convert.ToDouble(lbl2.Text) > 0)
                {
                    e.Row.Cells[2].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl3 = e.Row.FindControl("lblH3") as Label;
                rcount += Convert.ToDouble(lbl3.Text);
                if (Convert.ToDouble(lbl3.Text) == 0)
                {
                    lbl3.Text = " ";
                }
                else if (Convert.ToDouble(lbl3.Text) > 0)
                {
                    e.Row.Cells[3].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl4 = e.Row.FindControl("lblH4") as Label;
                rcount += Convert.ToDouble(lbl4.Text);
                if (Convert.ToDouble(lbl4.Text) == 0)
                {
                    lbl4.Text = " ";
                }
                else if (Convert.ToDouble(lbl4.Text) > 0)
                {
                    e.Row.Cells[4].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl5 = e.Row.FindControl("lblH5") as Label;
                rcount += Convert.ToDouble(lbl5.Text);
                if (Convert.ToDouble(lbl5.Text) == 0)
                {
                    lbl5.Text = " ";
                }
                else if (Convert.ToDouble(lbl5.Text) > 0)
                {
                    e.Row.Cells[5].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl6 = e.Row.FindControl("lblH6") as Label;
                rcount += Convert.ToDouble(lbl6.Text);
                if (Convert.ToDouble(lbl6.Text) == 0)
                {
                    lbl6.Text = " ";
                }
                else if (Convert.ToDouble(lbl6.Text) > 0)
                {
                    e.Row.Cells[6].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl7 = e.Row.FindControl("lblH7") as Label;
                rcount += Convert.ToDouble(lbl7.Text);
                if (Convert.ToDouble(lbl7.Text) == 0)
                {
                    lbl7.Text = " ";
                }
                else if (Convert.ToDouble(lbl7.Text) > 0)
                {
                    e.Row.Cells[7].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl8 = e.Row.FindControl("lblH8") as Label;
                rcount += Convert.ToDouble(lbl8.Text);
                if (Convert.ToDouble(lbl8.Text) == 0)
                {
                    lbl8.Text = " ";
                }
                else if (Convert.ToDouble(lbl8.Text) > 0)
                {
                    e.Row.Cells[8].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl9 = e.Row.FindControl("lblH9") as Label;
                rcount += Convert.ToDouble(lbl9.Text);
                if (Convert.ToDouble(lbl9.Text) == 0)
                {
                    lbl9.Text = " ";
                }
                else if (Convert.ToDouble(lbl9.Text) > 0)
                {
                    e.Row.Cells[9].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl10 = e.Row.FindControl("lblH10") as Label;
                rcount += Convert.ToDouble(lbl10.Text);
                if (Convert.ToDouble(lbl10.Text) == 0)
                {
                    lbl10.Text = " ";
                }
                else if (Convert.ToDouble(lbl10.Text) > 0)
                {
                    e.Row.Cells[10].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl11 = e.Row.FindControl("lblH11") as Label;
                rcount += Convert.ToDouble(lbl11.Text);
                if (Convert.ToDouble(lbl11.Text) == 0)
                {
                    lbl11.Text = " ";
                }
                else if (Convert.ToDouble(lbl11.Text) > 0)
                {
                    e.Row.Cells[11].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl12 = e.Row.FindControl("lblH12") as Label;
                rcount += Convert.ToDouble(lbl12.Text);
                if (Convert.ToDouble(lbl12.Text) == 0)
                {
                    lbl12.Text = " ";
                }
                else if (Convert.ToDouble(lbl12.Text) > 0)
                {
                    e.Row.Cells[12].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl13 = e.Row.FindControl("lblH13") as Label;
                rcount += Convert.ToDouble(lbl13.Text);
                if (Convert.ToDouble(lbl13.Text) == 0)
                {
                    lbl13.Text = " ";
                }
                else if (Convert.ToDouble(lbl13.Text) > 0)
                {
                    e.Row.Cells[13].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl14 = e.Row.FindControl("lblH14") as Label;
                rcount += Convert.ToDouble(lbl14.Text);
                if (Convert.ToDouble(lbl14.Text) == 0)
                {
                    lbl14.Text = " ";
                }
                else if (Convert.ToDouble(lbl14.Text) > 0)
                {
                    e.Row.Cells[14].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl15 = e.Row.FindControl("lblH15") as Label;
                rcount += Convert.ToDouble(lbl15.Text);
                if (Convert.ToDouble(lbl15.Text) == 0)
                {
                    lbl15.Text = " ";
                }
                else if (Convert.ToDouble(lbl15.Text) > 0)
                {
                    e.Row.Cells[15].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl16 = e.Row.FindControl("lblH16") as Label;
                rcount += Convert.ToDouble(lbl16.Text);
                if (Convert.ToDouble(lbl16.Text) == 0)
                {
                    lbl16.Text = " ";
                }
                else if (Convert.ToDouble(lbl16.Text) > 0)
                {
                    e.Row.Cells[16].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl17 = e.Row.FindControl("lblH17") as Label;
                rcount += Convert.ToDouble(lbl17.Text);
                if (Convert.ToDouble(lbl17.Text) == 0)
                {
                    lbl17.Text = " ";
                }
                else if (Convert.ToDouble(lbl17.Text) > 0)
                {
                    e.Row.Cells[17].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl18 = e.Row.FindControl("lblH18") as Label;
                rcount += Convert.ToDouble(lbl18.Text);
                if (Convert.ToDouble(lbl18.Text) == 0)
                {
                    lbl18.Text = " ";
                }
                else if (Convert.ToDouble(lbl18.Text) > 0)
                {
                    e.Row.Cells[18].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl19 = e.Row.FindControl("lblH19") as Label;
                rcount += Convert.ToDouble(lbl19.Text);
                if (Convert.ToDouble(lbl19.Text) == 0)
                {
                    lbl19.Text = " ";
                }
                else if (Convert.ToDouble(lbl19.Text) > 0)
                {
                    e.Row.Cells[19].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl20 = e.Row.FindControl("lblH20") as Label;
                rcount += Convert.ToDouble(lbl20.Text);
                if (Convert.ToDouble(lbl20.Text) == 0)
                {
                    lbl20.Text = " ";
                }
                else if (Convert.ToDouble(lbl20.Text) > 0)
                {
                    e.Row.Cells[20].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl21 = e.Row.FindControl("lblH21") as Label;
                rcount += Convert.ToDouble(lbl21.Text);
                if (Convert.ToDouble(lbl21.Text) == 0)
                {
                    lbl21.Text = " ";
                }
                else if (Convert.ToDouble(lbl21.Text) > 0)
                {
                    e.Row.Cells[21].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl22 = e.Row.FindControl("lblH22") as Label;
                rcount += Convert.ToDouble(lbl22.Text);
                if (Convert.ToDouble(lbl22.Text) == 0)
                {
                    lbl22.Text = " ";
                }
                else if (Convert.ToDouble(lbl22.Text) > 0)
                {
                    e.Row.Cells[22].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl23 = e.Row.FindControl("lblH23") as Label;
                rcount += Convert.ToDouble(lbl23.Text);
                if (Convert.ToDouble(lbl23.Text) == 0)
                {
                    lbl23.Text = " ";
                }
                else if (Convert.ToDouble(lbl23.Text) > 0)
                {
                    e.Row.Cells[23].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl24 = e.Row.FindControl("lblH24") as Label;
                rcount += Convert.ToDouble(lbl24.Text);
                if (Convert.ToDouble(lbl24.Text) == 0)
                {
                    lbl24.Text = " ";
                }
                else if (Convert.ToDouble(lbl24.Text) > 0)
                {
                    e.Row.Cells[24].BackColor = System.Drawing.Color.LightPink;
                }
                Label total = e.Row.FindControl("lbltotal") as Label;
                total.Text = rcount.ToString() + "";
                count += rcount;

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label;
                ftotal.Text = count.ToString() + "";
            }
        }




    }
}

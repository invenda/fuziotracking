﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Corrections.aspx.cs" Inherits="Tracking.Corrections"
    EnableEventValidation="false" MasterPageFile="~/ESLMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">

    </script>

    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0; text-align: center;
        color: Blue;">
        <h1>
            Corrections for Meter Reading
        </h1>
    </div>
    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
        <table width="100%">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ValidationGroup="Group1" ShowSummary="False" />
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Number :<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                        ControlToValidate="ddlVeh" ValidationGroup="Group1" ErrorMessage="Select  Vehicle Number">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:DropDownList ID="ddlVeh" runat="server" DataValueField="Vehicalnumber" Width="184px"
                        DataTextField="Vehicalnumber" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td>
                    Date & Time of Correction :
                </td>
                <td>
                    <asp:Label ID="lblCorrectDT" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Type of Correction:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="rbtnTypeOfCor" ValidationGroup="Group1" ErrorMessage="Select Type of Correction">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:RadioButtonList ID="rbtnTypeOfCor" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="+" Value="1"></asp:ListItem>
                        <asp:ListItem Text="-" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    Correction Value:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCorrection"
                        ValidationGroup="Group1" Text="*" ErrorMessage="Enter a valid Correction Value"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter a valid Correction Value"
                        ValidationGroup="Group1" ValidationExpression="^\d*\.?(\d|\d\d|\d\d\d)?$" ControlToValidate="txtCorrection">*</asp:RegularExpressionValidator>
                    <asp:TextBox ID="txtCorrection" runat="server" Height="15px" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnSub" runat="server" Text="Submit" OnClick="btnSub_Click" ValidationGroup="Group1" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="height: 20px;">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4">
                    <asp:DataGrid ID="dgCorrect" runat="server" Width="100%" AllowPaging="false" AutoGenerateColumns="False"
                        CellPadding="3" Visible="true">
                        <AlternatingItemStyle></AlternatingItemStyle>
                        <ItemStyle></ItemStyle>
                        <HeaderStyle></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="CORRECTION_VEH" HeaderText="Vehicle Number" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CORRECTION_DT" HeaderText="Correction Date & Time">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CORRECTION_Value" HeaderText="Correction Value">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CORRECTION_TYPE" HeaderText="Type of Correction">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CORRECTION_CUMU" HeaderText="Cumulative Correction">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

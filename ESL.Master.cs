﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

namespace Tracking
{
    public partial class ESL : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                // Response.Redirect("~/Login.aspx");
                Response.Redirect("~/sessiontimeout.aspx");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
 
 
                        DBClass db = new DBClass();
 
 
                        lblpage.Text = Session["page"].ToString();
                        lblpage2.Text = Session["page1"].ToString();
                        lblusername.Text = Session["UserName"].ToString();
                        DataTable dt = db.Set_Image_Logo(Session["UserID"].ToString());
                        if (dt.Rows[0][0].ToString() != "" && dt.Rows[0][1].ToString() != "")
                        {
                            Image1.ImageUrl = "Handler.ashx?EmpID=" + Session["UserID"].ToString();
                            Image1.Width = Convert.ToInt32(dt.Rows[0][1].ToString());
                        }
                        else
                        {
                            Image1.ImageUrl = "~/Images/ESL1.JPG";
                            Image1.Width = 50;
                        }
                        if (Session["Logotext"].ToString() != "")
                        {
                            lblhead.Text = Session["Logotext"].ToString();
                        }
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                }
                else 
                {
                    Session.Clear();
                    Response.Redirect("Login.aspx");
                
                }
            }  
        }

        protected void btnRaiseComplaint_Click(object sender, EventArgs e)
        {
            string pageurl = "RaiseComplaint.aspx";
            string s = "window.open('" + pageurl + "', 'popup_window','width=900,height=400,left=300,top=200,resizable=no');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "script", s, true);
        }

        protected void btnhepdesk_Click(object sender, EventArgs e)
        {
            string pageurl = "Escalation_matrix .aspx";
            string s = "window.open('" + pageurl + "', 'popup_window','width=900,height=400,left=300,top=200,resizable=no');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "script", s, true);
        }

        //protected void btnhistory_Click(object sender, EventArgs e)
        //{
        //   // Response.Redirect("Complaints.aspx"); 
        //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('Complaints.aspx','_newtab');", true);
        //    string url = "Complaints.aspx";
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<script type = 'text/javascript'>");
        //    sb.Append("window.open('");
        //    sb.Append(url);
        //    sb.Append("');");
        //    sb.Append("</script>");
        //   Page.ClientScript.RegisterStartupScript(this.GetType(),"script", sb.ToString());
        //}

        
    }
}

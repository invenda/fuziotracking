﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;


namespace Tracking
{
    public partial class GenerateReport : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            // Response.AppendHeader("Refresh", "10; URL=" + Request.Url.AbsoluteUri);
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                //Timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"].ToString());
                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["ParentUser"].ToString());
                        ddlMapTOVehicle.DataBind();

                        ddlFromHOURS.DataSource = db.GetFromHours(Session["ParentUser"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["ParentUser"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlToMINUTES.DataBind();

                        //ddlFromDT.DataSource = db.GetDT(Session["ParentUser"].ToString());
                        //ddlFromDT.DataBind();

                        //ddlToDT.DataSource = db.GetDT(Session["ParentUser"].ToString());
                        //ddlToDT.DataBind();

                        dgSearch.Visible = true;
                        dgSave.Visible = true;

                    }
                }
            }

        }

        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();

        //public void imgBtnFind_Click(object sender, ImageClickEventArgs e)
        //{

        //    DBClass db = new DBClass();

        //    string AMPM, DT;
        //    string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
        //    string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
        //    string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

        //    //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

        //    AMPM = rbtnAMPM.SelectedValue.ToString();
        //    DT = rbtnDT.SelectedValue.ToString();

        //    //if (rbtnDT.Checked)
        //    //{
        //    //    AMPM = "AM";
        //    //}
        //    //else
        //    //{
        //    //    AMPM = "PM";
        //    //}

        //    //if (rbtnAMPM.Checked)
        //    //{
        //    //    DT = "AM";
        //    //}
        //    //else
        //    //{
        //    //    DT = "PM";
        //    //}

        //    TextBox1.Text = TextBox1.Text + " " + fromtime + " " + AMPM;
        //    TextBox3.Text = TextBox3.Text + " " + totime + " " + DT;

        //    //TextBox1.Text = TextBox1.Text + " " + AMPM;
        //    //TextBox3.Text = TextBox3.Text + " " + DT;
        //    //TextBox1.Text = TextBox1.Text + " " + ddlFromDT.SelectedValue;
        //    //TextBox3.Text = TextBox3.Text + " " + ddlToDT.SelectedValue;

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("VehicalNumber", typeof(string));
        //    dt.Columns.Add("Start Date & Time", typeof(string));
        //    dt.Columns.Add("End Date & Time", typeof(string));
        //    DataRow DR = dt.NewRow();
        //    DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
        //    DR["Start Date & Time"] = TextBox1.Text;
        //    DR["End Date & Time"] = TextBox3.Text;

        //    dt.Rows.Add(DR);
        //    dgSave.DataSource = dt;
        //    dgSave.DataBind();

        //    DataTable tab = db.search(Session["ParentUser"].ToString(), TextBox1.Text, TextBox3.Text, ddlMapTOVehicle.SelectedValue.ToString());

        //    if (tab.Columns.Count > 0)
        //    {
        //        tab.Columns.Add("LOC", typeof(string));
        //        //foreach (DataRow dr in tab.Rows)
        //        //{
        //        //tab.Rows[0]["Loc"] = getLatLng(dr["LAMI"].ToString(), dr["LGMI"].ToString());  
        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            GeoCode objAddress = new GeoCode();

        //            objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

        //            StringBuilder sb = new StringBuilder();
        //            if (objAddress.valid)
        //            {
        //                sb.Append(objAddress.Placemark.address.ToString());
        //                string address = sb.ToString();
        //                tab.Rows[i]["LOC"] = address;
        //            }
        //        }

        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            double speed;
        //            string sped;
        //            speed = Convert.ToDouble(tab.Rows[i][3].ToString());
        //            if (speed > 3)
        //            {
        //                sped = Convert.ToString(speed * 1.85);
        //                tab.Rows[i]["SPED"] = sped;
        //            }
        //            else if (speed <= 3)
        //            {
        //                sped = "0";
        //                tab.Rows[i]["SPED"] = sped;
        //            }

        //        }

        //        //for (int i = 0; i < tab.Rows.Count; i++) Khizar 5/7/12
        //        //{
        //        //    double speed;
        //        //    string sped;
        //        //    speed = Convert.ToDouble(tab.Rows[i][3].ToString());
        //        //    sped = Convert.ToString(speed * 1.85);
        //        //    tab.Rows[i]["SPED"] = sped;
        //        //    //double speed = Convert.ToDouble(tab.Rows[0][3].ToString());
        //        //    //string sped = Convert.ToString(speed * 1.85);
        //        //}
        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            if (tab.Rows[i][4].ToString() == "0")
        //            {
        //                tab.Rows[i]["IOST"] = "Off";
        //            }
        //            else if (tab.Rows[i][4].ToString() == "1")
        //            {
        //                tab.Rows[i]["IOST"] = "On";
        //            }
        //        }
        //        //}
        //    }
        //    tab.AcceptChanges();
        //    dgSearch.DataSource = tab;
        //    dgSearch.DataBind();
        //    TextBox1.Enabled = false;
        //    TextBox3.Enabled = false;
        //    ddlMapTOVehicle.Enabled = false;
        //    ddlFromHOURS.Enabled = false;
        //    ddlFromMINUTES.Enabled = false;
        //    ddlToHOURS.Enabled = false;
        //    ddlToMINUTES.Enabled = false;
        //    rbtnAMPM.Enabled = false;
        //    rbtnDT.Enabled = false;
        //    imgBtnFind.Enabled = false;
        //}

        //public void btnFindRecords_Click(object sender, EventArgs e)lums
        //{
        //    string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("VehicalNumber", typeof(string));
        //    dt.Columns.Add("Selected Date", typeof(string));
        //    DataRow DR = dt.NewRow();
        //    DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
        //    DR["Selected Date"] = txtDate.Text;
        //    dt.Rows.Add(DR);
        //    dgSave.DataSource = dt;
        //    dgSave.DataBind();

        //    DataTable tab = Find(Session["ParentUser"].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), txtDate.Text, Calendar1.SelectedDate.AddDays(1).ToString("MM/dd/yyyy")); //Convert.ToDateTime(ddlFromTime.SelectedValue)); //, Convert.ToDateTime(ddlToTime.SelectedValue));

        //    if (tab.Columns.Count > 0)
        //    {
        //        tab.Columns.Add("LOC", typeof(string));
        //        //foreach (DataRow dr in tab.Rows)
        //        //{
        //        //tab.Rows[0]["Loc"] = getLatLng(dr["LAMI"].ToString(), dr["LGMI"].ToString());  
        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            GeoCode objAddress = new GeoCode();

        //            objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

        //            StringBuilder sb = new StringBuilder();
        //            if (objAddress.valid)
        //            {
        //                sb.Append(objAddress.Placemark.address.ToString());
        //                string address = sb.ToString();
        //                tab.Rows[i]["LOC"] = address;
        //            } 
        //        }

        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            double speed;
        //            string sped;
        //            speed = Convert.ToDouble(tab.Rows[i][3].ToString());
        //            sped = Convert.ToString(speed * 1.85);
        //            tab.Rows[i]["SPED"] = sped;
        //            //double speed = Convert.ToDouble(tab.Rows[0][3].ToString());
        //            //string sped = Convert.ToString(speed * 1.85);
        //        }
        //        for (int i = 0; i < tab.Rows.Count; i++)
        //        {
        //            if (tab.Rows[i][4].ToString() == "0")
        //            {
        //                tab.Rows[i]["IOST"] = "Off";
        //            }
        //            else
        //            {
        //                tab.Rows[i]["IOST"] = "On";
        //            }
        //        }
        //        //}
        //    }
        //    tab.AcceptChanges();
        //    dgSearch.DataSource = tab;
        //    dgSearch.DataBind();
        //}


        //public object getLatLng(string p, string p_2)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("SELECT Distinct [LAMI],[LGMI],[GTIM]  FROM [Tracking] ", sqlconn);
        //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    ds.AcceptChanges();
        //    adp.Fill(ds);
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();
        //    return ds.Tables[0];
        //}new SqlCommand("select LAMI, LGMI, GTIM as datetime, XXXM as deviceID from Tracking inner join Vehicles_Device_Rel on XXXM = Device_ID where VehicalNumber = '" + ddlMapTOVehicle + "' and USER_ID = " + customerID + " and GTIM >= '" + date + "' and GTIM <= '" + nextdate + "'", sqlconn); //


        //--- Logic to Fetch the Search Results ---
        public DataTable Find(string customerID, string ddlMapTOVehicle, string date, string nextdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID where VehicalNumber = '" + ddlMapTOVehicle + "' and USER_ID = " + customerID + "and GTIM >= '" + date + "' and GTIM < '" + nextdate + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        //protected void calender_selected(object sender, EventArgs e)
        //{
        //    this.txtDate.Text = Calendar1.SelectedDate.ToString("MM/dd/yyyy");
        //}

        ////----EXCEL---- 
        //public void imgBtnSavPrint_Click(object sender, ImageClickEventArgs e)
        //{
        //    Response.Clear();
        //    Response.Buffer = true;

        //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);

        //    //PrepareForExport(dgSave);
        //    //PrepareForExport(dgSearch);

        //    Table tb = new Table();
        //    TableRow tr1 = new TableRow();
        //    TableCell cell1 = new TableCell();
        //    cell1.Controls.Add(dgSave);
        //    tr1.Cells.Add(cell1);
        //    TableCell cell3 = new TableCell();
        //    cell3.Controls.Add(dgSearch);
        //    TableCell cell2 = new TableCell();
        //    cell2.Text = "&nbsp;";
        //    //if (rbPreference.SelectedValue == "2")
        //    //{
        //    //    tr1.Cells.Add(cell2);
        //    //    tr1.Cells.Add(cell3);
        //    //    tb.Rows.Add(tr1);
        //    //}
        //    //else
        //    //{
        //    TableRow tr2 = new TableRow();
        //    tr2.Cells.Add(cell2);
        //    TableRow tr3 = new TableRow();
        //    tr3.Cells.Add(cell3);
        //    tb.Rows.Add(tr1);
        //    tb.Rows.Add(tr2);
        //    tb.Rows.Add(tr3);
        //    //}
        //    tb.RenderControl(hw);

        //    //style to format numbers to string
        //    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
        //    Response.Write(style);
        //    Response.Output.Write(sw.ToString());
        //    Response.Flush();
        //    Response.End();

        //    //Response.Clear();
        //    //Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
        //    //Response.Charset = "";
        //    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //Response.ContentType = "application/vnd.xls";

        //    //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //    //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        //    //dgSearch.RenderControl(htmlWrite);
        //    //Response.Write(stringWrite.ToString());
        //    //Response.End();
        //}

        //protected void imgBtnRefresh_Click(object sender, ImageClickEventArgs e)
        //{
        //    Response.Redirect(Request.RawUrl);
        //}

        protected void BtnFind_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 

            string AMPM, DT;
            string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

            //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();

            //if (rbtnDT.Checked)
            //{
            //    AMPM = "AM";
            //}
            //else
            //{
            //    AMPM = "PM";
            //}

            //if (rbtnAMPM.Checked)
            //{
            //    DT = "AM";
            //}
            //else
            //{
            //    DT = "PM";
            //}

            TextBox1.Text = TextBox1.Text + " " + fromtime + " " + AMPM;
            TextBox3.Text = TextBox3.Text + " " + totime + " " + DT;

            //TextBox1.Text = TextBox1.Text + " " + AMPM;
            //TextBox3.Text = TextBox3.Text + " " + DT;
            //TextBox1.Text = TextBox1.Text + " " + ddlFromDT.SelectedValue;
            //TextBox3.Text = TextBox3.Text + " " + ddlToDT.SelectedValue;

            DataTable dt = new DataTable();
            dt.Columns.Add("VehicalNumber", typeof(string));
            dt.Columns.Add("Start Date & Time", typeof(string));
            dt.Columns.Add("End Date & Time", typeof(string));
            DataRow DR = dt.NewRow();
            DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
            DR["Start Date & Time"] = TextBox1.Text;
            DR["End Date & Time"] = TextBox3.Text;

            dt.Rows.Add(DR);
            dgSave.DataSource = dt;
            dgSave.DataBind();

            DataTable tab = db.search(Session["ParentUser"].ToString(), TextBox1.Text, TextBox3.Text, ddlMapTOVehicle.SelectedValue.ToString());

            if (tab.Columns.Count > 0)
            {
                tab.Columns.Add("LOC", typeof(string));
                //foreach (DataRow dr in tab.Rows)
                //{
                //tab.Rows[0]["Loc"] = getLatLng(dr["LAMI"].ToString(), dr["LGMI"].ToString());  
                for (int i = 0; i < tab.Rows.Count; i++)
                {
                    GeoCode objAddress = new GeoCode();

                    objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

                    StringBuilder sb = new StringBuilder();
                    if (objAddress.valid)
                    {
                        sb.Append(objAddress.Placemark.address.ToString());
                        string address = sb.ToString();
                        tab.Rows[i]["LOC"] = address;
                    }
                }

                for (int i = 0; i < tab.Rows.Count; i++)
                {
                    double speed;
                    string sped;
                    speed = Convert.ToDouble(tab.Rows[i][3].ToString());
                    if (speed > 3)
                    {
                        sped = Convert.ToString(speed * 1.85);
                        tab.Rows[i]["SPED"] = sped;
                    }
                    else if (speed <= 3)
                    {
                        sped = "0";
                        tab.Rows[i]["SPED"] = sped;
                    }

                }

                //for (int i = 0; i < tab.Rows.Count; i++) Khizar 5/7/12
                //{
                //    double speed;
                //    string sped;
                //    speed = Convert.ToDouble(tab.Rows[i][3].ToString());
                //    sped = Convert.ToString(speed * 1.85);
                //    tab.Rows[i]["SPED"] = sped;
                //    //double speed = Convert.ToDouble(tab.Rows[0][3].ToString());
                //    //string sped = Convert.ToString(speed * 1.85);
                //}
                for (int i = 0; i < tab.Rows.Count; i++)
                {
                    if (tab.Rows[i][4].ToString() == "0")
                    {
                        tab.Rows[i]["IGST"] = "Off";
                    }
                    else if (tab.Rows[i][4].ToString() == "1")
                    {
                        tab.Rows[i]["IGST"] = "On";
                    }
                }
                //}
            }
            tab.AcceptChanges();
            dgSearch.DataSource = tab;
            dgSearch.DataBind();
            TextBox1.Enabled = false;
            TextBox3.Enabled = false;
            ddlMapTOVehicle.Enabled = false;
            ddlFromHOURS.Enabled = false;
            ddlFromMINUTES.Enabled = false;
            ddlToHOURS.Enabled = false;
            ddlToMINUTES.Enabled = false;
            rbtnAMPM.Enabled = false;
            rbtnDT.Enabled = false;
            BtnFind.Enabled = false;
        }

        protected void BtnSavPrint_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //PrepareForExport(dgSave);
            //PrepareForExport(dgSearch);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(dgSave);
            tr1.Cells.Add(cell1);
            TableCell cell3 = new TableCell();
            cell3.Controls.Add(dgSearch);
            TableCell cell2 = new TableCell();
            cell2.Text = "&nbsp;";
            //if (rbPreference.SelectedValue == "2")
            //{
            //    tr1.Cells.Add(cell2);
            //    tr1.Cells.Add(cell3);
            //    tb.Rows.Add(tr1);
            //}
            //else
            //{
            TableRow tr2 = new TableRow();
            tr2.Cells.Add(cell2);
            TableRow tr3 = new TableRow();
            tr3.Cells.Add(cell3);
            tb.Rows.Add(tr1);
            tb.Rows.Add(tr2);
            tb.Rows.Add(tr3);
            //}
            tb.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            //Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
            //Response.Charset = "";
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "application/vnd.xls";

            //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            //dgSearch.RenderControl(htmlWrite);
            //Response.Write(stringWrite.ToString());
            //Response.End();
        }

        protected void BtnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }


        //protected void imgBtnFind_Click(object sender, EventArgs e)
        //{
        //    string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
        //    string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

        //    TextBox1.Text = TextBox1.Text + " " + fromtime;
        //    TextBox3.Text = TextBox3.Text + " " + totime;
        //    TextBox1.Text = TextBox1.Text + " " + ddlFromDT.SelectedValue;
        //    TextBox3.Text = TextBox3.Text + " " + ddlToDT.SelectedValue;

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("VehicalNumber", typeof(string));
        //    dt.Columns.Add("Selected Date", typeof(string));
        //    DataRow DR = dt.NewRow();
        //    DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
        //    DR["Start Date & Time"] = TextBox1.Text;
        //    DR["End Date & Time"] = TextBox3.Text;

        //    dt.Rows.Add(DR);
        //    dgSave.DataSource = dt;
        //    dgSave.DataBind();

        //}

        //public void PrepareForExport(GridView Gridview)
        //{
        //    //Gridview.AllowPaging = Convert.ToBoolean(rbPaging.SelectedItem.Value);
        //    Gridview.DataBind();

        //    //Change the Header Row back to white color
        //    Gridview.HeaderRow.Style.Add("background-color", "#FFFFFF");

        //    //Apply style to Individual Cells
        //    for (int k = 0; k < Gridview.HeaderRow.Cells.Count; k++)
        //    {
        //        Gridview.HeaderRow.Cells[k].Style.Add("background-color", "green");
        //    }

        //    for (int i = 0; i < Gridview.Rows.Count; i++)
        //    {
        //        GridViewRow row = Gridview.Rows[i];

        //        //Change Color back to white
        //        row.BackColor = System.Drawing.Color.White;

        //        //Apply text style to each Row
        //        row.Attributes.Add("class", "textmode");

        //        //Apply style to Individual Cells of Alternating Row
        //        if (i % 2 != 0)
        //        {
        //            for (int j = 0; j < Gridview.Rows[i].Cells.Count; j++)
        //            {
        //                row.Cells[j].Style.Add("background-color", "#C2D69B");
        //            }
        //        }
        //    }
        //}

        ////--- Logic for the Reports ---
        //protected void imgBtnSavPrint_Click(object sender, ImageClickEventArgs e)
        //{

        //    //    DataTable tabl = new DataTable();

        //    //    tabl = Find(Session["ParentUser"].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), txtDate.Text, Calendar1.SelectedDate.AddDays(1).ToString("MM/dd/yyyy"));//(customerID,ddlMapTOVehicle,date,nextdate);

        //    //    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"; //Response.ContentType ="application/msword";

        //    //    string strFileName = "ESLTrackingHistory"; //+".xls";
        //    //    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

        //    //    StringBuilder strHTMLContent = new StringBuilder();

        //    //    strHTMLContent.Append("<h1 title='Heading' align='Center'style='font-family: verdana; font -size: 80 % ;color: black' <u>Tracking Report</u> </h1>".ToString());
        //    //    strHTMLContent.Append("<br>".ToString());
        //    //    strHTMLContent.Append("<table align='Center' border=\"1\">".ToString());

        //    //    // Row with Column headers
        //    //    strHTMLContent.Append("<tr>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px; background:# 99CC00'><b>Vehicle Device ID</b></td>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px; background:# 99CC00'><b>From Date Time</b></td>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px;background:# 99CC00'><b>To Date Time</b></td>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px; background:# 99CC00'><b>Latitude</b></td>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px;background:# 99CC00'><b> Longitude</b></td>".ToString());
        //    //    strHTMLContent.Append("<td style='width:200px; background:# 99CC00'><b>Location</b></td>".ToString());
        //    //    strHTMLContent.Append("</tr>".ToString());

        //    //    int i;
        //    //    for (i = 0; i < tabl.Rows.Count; i++)
        //    //    {
        //    //        int j = i;
        //    //        // First Row Data 
        //    //        strHTMLContent.Append("<tr>".ToString());
        //    //        strHTMLContent.Append("<td align=left>" + tabl.Rows[i]["deviceID"].ToString() + " </td>");
        //    //        strHTMLContent.Append("<td align=left>" + tabl.Rows[i]["GTIM"].ToString() + "</td>");
        //    //        strHTMLContent.Append("<td align=left>" + tabl.Rows[i]["GTIM"].ToString() + " </td>");
        //    //        strHTMLContent.Append("<td align=left>" + tabl.Rows[i]["LAMI"].ToString() + "</td>");
        //    //        strHTMLContent.Append("<td align=left>" + tabl.Rows[i]["LAMI"].ToString() + "</td>");
        //    //        strHTMLContent.Append("</tr>".ToString());
        //    //    }

        //    //    strHTMLContent.Append("</table>".ToString());

        //    //    strHTMLContent.Append("<br> <br>".ToString());
        //    //    strHTMLContent.Append("<p align='Center'>Powered by: ES LABS</p>".ToString());
        //    //    HttpContext.Current.Response.Write(strHTMLContent);
        //    //    HttpContext.Current.Response.End();
        //    //    HttpContext.Current.Response.Flush();

        //    //}
        //    ////protected void dgSearch_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //    ////{
        //    ////    dgSearch.SelectedIndex = -1;
        //    ////    dgSearch.CurrentPageIndex = e.NewPageIndex;
        //    ////    //RefreshDataGrid();
        //    ////}
        //}
    }
}


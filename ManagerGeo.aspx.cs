﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class ManagerGeo : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt = 0, lng = 0;
        static int p = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            gmapsettings();
            DataTable dt = db.Get_Manager_Geo(Session["UserID"].ToString());
            if(dt.Rows.Count<=0)
            //if(DBNull.Value.Equals(dt.Rows [0][0]))
            {
            gmapsettings();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Already Created circle');", true);
               // Response.Redirect("ManagerHome1.aspx");
                //Response.Write("<script language='javascript'>window.open('','_self');window.close();</script>");
              p = 1;
              txtradius.Enabled = false;
              btnSubmit.Enabled = false;
              btnClear.Enabled = false;
            }
        }

        public void gmapsettings()
        {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
            GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
            GMap1.Add(extMapType);
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
            GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
            GMap1.enableHookMouseWheelToZoom = true;
            if (p > 0)
            {
                p = 0;
            }
            

        }

        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);
            // return default(string);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        { 
            GMap1.enableServerEvents = false;
            if (lt != 0 && lng != 0)
            {
                int uid=Convert.ToInt32 ( Session["UserID"].ToString());
                db.Set_Manager_Geo(uid, lt, lng, Convert.ToInt32(txtradius.Text), 0,txtGname.Text);
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Successfully Point Created ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                //ClientScript.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Successfully Point Created ');</script>",true);
                p = 1;
                txtGname.Text = "";
                txtradius.Text = "";
            }
            else
            {
                //Response.Redirect(Request.RawUrl);
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point On Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                
            }
            GMap1.enableServerEvents =true;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

    }
}
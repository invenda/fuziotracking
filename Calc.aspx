﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calc.aspx.cs" Inherits="Tracking.Calc"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpage1" runat="server" ContentPlaceHolderID="MainContent">
    <div style="width: 600px; height: 500px; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
         <div id="sublogo" style="text-align: center; color: Blue;">
            <h2>
              
            </h2>
        </div>
        <table style="width: 500px; height: 100%; border:2px;" border="2" >
            <tr>
                <td style="width: 350px;"  >
                    <asp:Label ID="Label3" runat="server" Text="Vehicle No: " Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
                <td align ="right">
                    <asp:Label ID="lblVehno" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label6" runat="server" Text="Date:" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
                <td align ="right">
                    <asp:Label ID="lbldate" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
                <tr style ="background-color :Blue ">
                <td  style :="height = auto"></td>
                <td ></td>
                </tr>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label1" runat="server" Text=" Opening Fuel In litres:" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
                <td align ="right">
                    <asp:Label ID="lblopen" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                 <asp:Label ID="Label5"  Text =" Actual fuel filled:" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
            
                    
                </td>
                <td align ="right">
                <asp:Label ID="fuel" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>
                    
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Medium"
                        Text="Approximate fuel filled/Shortage as Per data:"></asp:Label>
                </td >
                <td align ="right">
                
                    <asp:Label ID="lblff" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
            </tr>
         <tr style ="background-color :Blue ">
                <td  style :="height = auto "></td>
                <td ></td>
                </tr>
            <tr>
                <td >
                    <asp:Label ID="Label9" runat="server" Text="Vehicle Run km:" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td >
                <td align ="right">
                    <asp:Label ID="lblkm" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                 <asp:Label ID="Label7" runat="server" Text="Approximate fuel Consumed as Per data:" Font-Bold="True" Font-Size="Medium"></asp:Label>
                    </td>
                <td align ="right">
                    <asp:Label ID="lblfuelfill" runat="server" Text="" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
            </tr>
           <tr style ="background-color :Blue ">
              <td  style :="height= auto "></td>
                <td ></td>
                </tr>
             <tr>
                <td >
                 <asp:Label ID="Label2" runat="server" Text="closing Fuel in litres :" Font-Bold="True" Font-Size="Medium"></asp:Label>
                   </td>
                <td align ="right">
                <asp:Label ID="lblclose" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                    
                </td>
            </tr>
            <tr style ="background-color :Blue ">
               <td  style :="height = auto "></td>
                <td ></td>
                </tr>
                <tr>
                <td >
                <asp:Label ID="Label8" runat="server" Text="Difference in Litre:" Font-Bold="True" Font-Size="Medium"></asp:Label>
                
                </td>
                <td ></td>
                </tr>
        </table>
    </div>
</asp:Content>

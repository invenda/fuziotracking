﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class CustDetails : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        //ddlSalutation.DataSource = db.GetSalutation(Session["ParentUser"].ToString());
                        //ddlSalutation.DataBind();
                        ddlState.DataSource = db.GetState(Session["ParentUser"].ToString());
                        ddlState.DataBind();
                    }
                }
            }

            //lblDept.Visible = false;
            //txtDept.Visible = false;
        }

        //public void imgBtnSubmit_Click(object sender, ImageClickEventArgs e)
        //{
        //    //ddlSalutation.SelectedItem.Value
        //    DBClass db = new DBClass();
        //    string Company;
        //    if (rbtnCompany.Checked)
        //    {
        //        Company = "Company";
        //    }
        //    else
        //    {
        //        Company = "Individual";
        //    }

        //    db.RegCustomer(Company, txtCustName.Text, txtCustAddress1.Text, txtCustAddress2.Text, txtDistrict.Text, ddlState.SelectedItem.Value, txtAreaCode.Text, txtCustConNum.Text, txtCustMobNum.Text, txtDOR.Text, txtEmail.Text, Session["UserID"].ToString());
        //    rbtnIndCust.Text = string.Empty;
        //    rbtnCompany.Text = string.Empty;
        //    txtCustName.Text = string.Empty;
        //    txtCustAddress1.Text = string.Empty;
        //    txtCustAddress2.Text = string.Empty;
        //    txtDistrict.Text = string.Empty;
        //    ddlState.SelectedItem.Value = string.Empty;
        //    txtAreaCode.Text = string.Empty;
        //    txtCustConNum.Text = string.Empty;
        //    txtCustMobNum.Text = string.Empty;
        //    txtDOR.Text = string.Empty;
        //    txtEmail.Text = string.Empty;

        //    string User = Session["UserName"].ToString();
        //    string register = "Yes";
        //    db.Reg(register, User);

        //    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Registration Successfully');</script>");
        //    Response.Redirect("ViewCustDetails.aspx");
        //    //lnkbtnVehicle.Visible = true;
        //}

        protected void imgBtnClose_Click(object sender, ImageClickEventArgs e)
        {
            Response.Write("<script language=javascript> window.close();</script>");
            Response.End();
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            //ddlSalutation.SelectedItem.Value
 
 
            DBClass db = new DBClass();
 
 
            string id = Session["UserID"].ToString();
            string Company;
            if (rbtnCompany.Checked)
            {
                Company = "Company";
            }
            else
            {
                Company = "Individual";
            }

            db.RegCustomer(id, Company, txtCustName.Text, txtCustAddress1.Text, txtCustAddress2.Text, txtDistrict.Text, ddlState.SelectedItem.Value, txtAreaCode.Text, txtCustConNum.Text, txtCustMobNum.Text, txtDOR.Text, txtEmail.Text,txtaemail.Text,Session["UserID"].ToString());
            rbtnIndCust.Text = string.Empty;
            rbtnCompany.Text = string.Empty;
            txtCustName.Text = string.Empty;
            txtCustAddress1.Text = string.Empty;
            txtCustAddress2.Text = string.Empty;
            txtDistrict.Text = string.Empty;
            ddlState.SelectedItem.Value = string.Empty;
            txtAreaCode.Text = string.Empty;
            txtCustConNum.Text = string.Empty;
            txtCustMobNum.Text = string.Empty;
            txtDOR.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtaemail.Text = string.Empty;

            string User = Session["UserName"].ToString();
            string register = "Yes";
            db.Reg(register, User);

            //Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Registration Successfully');</script>");
           //Response.Redirect("ViewCustDetails.aspx"); //?username=" + userdt.Rows[0][1]
            logout();
        }
        public void logout()
        {
            Session["UserID"] = null;
            Session["UserName"] = null;
            Session["UserRole"] = null;
            Session["ParentUser"] = null;
            Response.Redirect("Login.aspx");

        }

        //public void rbtnCompany_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbtnCompany.Checked.ToString() == "Company")
        //    {
        //        lblDept.Visible = true;
        //        txtDept.Visible = true;
        //    }
        //}

        //protected void lnkbtnVehicle_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("VehicleDetails.aspx");
        //}
    }
}

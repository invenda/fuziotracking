﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RaiseComplaint.aspx.cs"
    MasterPageFile="~/ESLMaster.Master" Inherits="Tracking.RaiseComplaint" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        function checkdate(sender, args) {
            var today = new Date();
            var setdate11 = new Date();
            setdate11.setDate(today.getDate() + 2);
            var yesday = new Date();
            yesday.setDate(today.getDate() +1);
            if (sender._selectedDate < yesday) {
                alert("Please select a later date which is atleast 48 Hours from today's date..!");
                sender._selectedDate = setdate11;
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
   
    </script>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ValidationGroup="Group1" ShowSummary="False" />
    <cc1:ToolkitScriptManager runat="server">
    </cc1:ToolkitScriptManager>
     <div style="width: 90%; height: auto; font-size: 30px; text-align: center; font-style:italic;  color: Blue;">
         Submit Complaint
    </div>
    <div style="width: 90%; height:20px; font-size: 30px; text-align: center; font-style:italic;  color: Blue;">
      
    </div>
    <div style="font-family: Verdana; font-size: 15px; width:75%; margin:0 auto;">
        <div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" Text="Select Vehicle" runat="server"></asp:Label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                            ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                            ValueToCompare="--Select One--">*</asp:CompareValidator>
                        <%-- <asp:RequiredFieldValidator ID="rr" runat="server" ValidationGroup="Group1"
                        ControlToValidate="ddlMapTOVehicle" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                        ValueToCompare="--Select One--">*</asp:RequiredFieldValidator>&nbsp;:--%>
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            AutoPostBack="true" DataTextField="Vehicalnumber" Height="26px" Width="134px"
                            OnDataBound="ddlMapTOVehicle_DataBound" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 70px; text-align: left;">
                        <asp:Label ID="lblDevice" runat="server" Text="Unit Serial No::"></asp:Label>
                    </td>
                    <td style="width: 100px; text-align: left;">
                        <asp:Label ID="lblunitno" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" Text="Type of Complaint" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" InitialValue="--Select One--"
                            ControlToValidate="ddlcomplaint" ValidationGroup="Group1" ErrorMessage="Select Compalint type">*</asp:RequiredFieldValidator>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="Group1"
                        ControlToValidate="ddlcomplaint" ErrorMessage="- Select cpmpalint type" Operator="NotEqual"
                        ValueToCompare="--Select One--">*</asp:RequiredFieldValidator>&nbsp;:--%>
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcomplaint" runat="server" Height="26px" Width="130px">
                            <asp:ListItem Selected="True">--Select One-- </asp:ListItem>
                            <asp:ListItem Text="Offline" Value="Offline"></asp:ListItem>
                            <asp:ListItem Text="Relocate" Value="Relocate"></asp:ListItem>
                            <asp:ListItem Text="Damaged" Value="Damaged"></asp:ListItem>
                            <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" Text="Nature of Complaint" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                            ControlToValidate="txtMessage" ErrorMessage="Enter Complaint Msg">*</asp:RequiredFieldValidator>&nbsp;:
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Width="400px" EnableTheming="False"
                            Style="resize: none" Height="50px" Rows="0"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" Text="Vehicle available Location for Service" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="Group1"
                            ControlToValidate="txtLocation" ErrorMessage="Enter available Location">*</asp:RequiredFieldValidator>&nbsp;:
                    </td>
                    <td>
                        <asp:TextBox ID="txtLocation" runat="server" Width="179px" Height="25px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblcity" Text="City" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                            ControlToValidate="txtcity" ErrorMessage="Enter available Location">*</asp:RequiredFieldValidator>&nbsp;:
                    </td>
                    <td>
                        <asp:TextBox ID="txtcity" runat="server" Width="179px" Height="25px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" Text="Vehicle availability Date and time for service" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="Group1"
                            ControlToValidate="txtfdate" ErrorMessage="Select Date for service">*</asp:RequiredFieldValidator>&nbsp;:
                    </td>
                    <td>
                        <asp:TextBox ID="txtfdate" runat="server" Width="112px" Height="25px"></asp:TextBox>
                        <cc1:CalendarExtender ID="Cal" runat="server" Format="MM/dd/yyyy" TargetControlID="txtfdate"
                            OnClientDateSelectionChanged="checkdate">
                        </cc1:CalendarExtender>
                        &nbsp;
                        <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="50px" DataValueField="Hours"
                            DataTextField="Hours" Height="30px" meta:resourcekey="ddlFromHOURSResource1">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                            ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select time">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rbtnAMPM1"
                            ValidationGroup="Group1" ErrorMessage="Select TO Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 200px;">
                        <asp:RadioButtonList ID="rbtnAMPM1" runat="server" RepeatDirection="Horizontal" Height="12px"
                            Width="106px">
                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblmobile" Text="Contact Mobile Number" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                            ControlToValidate="txtmobile" ErrorMessage="Enter Contact number">*</asp:RequiredFieldValidator>&nbsp;:
                    </td>
                    <td>
                        <asp:TextBox ID="txtmobile" runat="server" Width="172px" Height="25px" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 30px;">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Font-Bold="true" ValidationGroup="Group1"
                            Width="68px" OnClick="btnSubmit_Click" />
                    </td>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnCancel" runat="server" Text="Close" Font-Bold="true" OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;

namespace Tracking
{
    public partial class ManagerTrack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GMapUIOptions options = new GMapUIOptions();
            options.maptypes_hybrid = true;
            options.keyboard = false;
            options.maptypes_physical = false;
            options.zoom_scrollwheel = true;
            GMap1.Add(new GMapUI(options));
            Session["page1"] = "Live Tracking";
            Session["page"] = "";
            if (!IsPostBack)
            {
                Timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"].ToString());                
                if (Session["UserID"] != null && Session["selectedVehicle"]!=null)
                {
                    Vehicle_Status(Session["selectedVehicle"].ToString());                  
                }
                Session["timerc"] = 0;
                Timer2.Enabled = true;
            }
        }       

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            if (Session["UserID"] != null && Session["selectedVehicle"] != null && Session["timerc"] != null)
            {
 
 
                DBClass db = new DBClass();
 
 
                GMap1.resetMarkers();
                GMap1.resetPolylines();
                if (Session["timerc"].ToString() == "1")
                {
                    DataTable status = db.GetPStatus(Session["UserID"].ToString(), Session["selectedVehicle"].ToString());
                    List<GLatLng> points = new List<GLatLng>();
                    List<GLatLng> LatLoc = new List<GLatLng>();
                    Subgurim.Controles.GIcon icon1 = new Subgurim.Controles.GIcon();
                    icon1.image = "http://labs.google.com/ridefinder/images/mm_20_black.png";
                    Subgurim.Controles.GIcon Bicon = new Subgurim.Controles.GIcon();
                    //Bicon.image = "http://google.com/mapfiles/ms/micons/blue-dot.png";
                    Bicon.image = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    Subgurim.Controles.GIcon Ricon = new Subgurim.Controles.GIcon();
                    //Ricon.image = "http://www.google.com/mapfiles/ms/micons/red-dot.png";
                    Ricon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    Subgurim.Controles.GIcon Gicon = new Subgurim.Controles.GIcon();
                    //Gicon.image = "http://www.google.com/mapfiles/ms/micons/green-dot.png";
                    Gicon.image = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    Subgurim.Controles.GIcon Ticon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Ticon.image = "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
                    points.Add(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])));
                    for (int i = 1; i < status.Rows.Count; i++)
                    {

                        if (status.Rows[i][2].ToString() != "0")
                        {
                            points.Add(new GLatLng(Convert.ToDouble(status.Rows[i][0]), Convert.ToDouble(status.Rows[i][1])));
                            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[i][0]), Convert.ToDouble(status.Rows[i][1]));
                            double speed = Convert.ToDouble(status.Rows[i][2].ToString());
                            int igst1 = Convert.ToInt32(status.Rows[i][8].ToString());
                            if (igst1 == 1 || (igst1 == 0 && speed < 3))
                            {
                                GMap1.setCenter(gLatLng1, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                                Subgurim.Controles.GMarker LMark = new Subgurim.Controles.GMarker(gLatLng1);
                                Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                                options.icon = icon1;
                                if (igst1 == 1)
                                {
                                    string sped = Convert.ToString(speed * 1.85);
                                    DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                    options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                                    LMark.options = options;
                                }
                                else if (speed <= 3 && igst1 == 0)
                                {
                                    string sped = "0";
                                    DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                    options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                                    LMark.options = options;
                                }
                                GMap1.Add(LMark);
                            }
                            else
                            {
                                Subgurim.Controles.GMarker TMark = new Subgurim.Controles.GMarker(gLatLng1);
                                Subgurim.Controles.GMarkerOptions Toptions = new Subgurim.Controles.GMarkerOptions();
                                Toptions.icon = Ticon;
                                string sped = Convert.ToString(speed * 1.85);
                                DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                Toptions.title = Session["selectedVehicle"].ToString() + "--Towed At :" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                                TMark.options = Toptions;
                                GMap1.Add(TMark);
                            }
                        }
                    }
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
                    Subgurim.Controles.GMarkerOptions options1 = new Subgurim.Controles.GMarkerOptions();
                    GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    double speed1 = Convert.ToDouble(status.Rows[0][2].ToString());
                    int igst = Convert.ToInt32(status.Rows[0][8].ToString());
                    if (igst == 0 && speed1 == 0)
                    {
#pragma warning disable CS0219 // The variable 'sped' is assigned but its value is never used
                        string sped = "0";
#pragma warning restore CS0219 // The variable 'sped' is assigned but its value is never used
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        //options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Ricon;
                        oMarker.options = options1;
                    }
                    else if (speed1 > 3 && igst == 1)
                    {
                        string sped = Convert.ToString(speed1 * 1.85);
                        lblspeed.Text = sped;
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        // options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Gicon;
                        oMarker.options = options1;
                    }
                    else if (speed1 <= 3 && igst == 1)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        //options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Bicon;
                        oMarker.options = options1;
                        lblspeed.Text = sped;
                    }
                    else if (igst == 0 && speed1 != 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options1.title = Session["selectedVehicle"].ToString() + "Towed At:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options1;
                        lblspeed.Text = sped;
                        options1.icon = Ticon;

                    }
                    GMap1.Add(oMarker);
                    GPolyline line = new GPolyline(points, "#008000", 2);
                    GMap1.Add(line);
                }
                else if (Session["timerc"].ToString() == "0")
                {
                    DataTable status = db.GetVehiclePresentlocation(Session["selectedVehicle"].ToString(), Session["UserID"].ToString());
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.GZoom = 11;
                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
                    Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                    Subgurim.Controles.GIcon Iicon = new Subgurim.Controles.GIcon();
                    //Iicon.image = "http://google.com/mapfiles/ms/micons/blue.png";
                    Iicon.image = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();
                    //icon.image = "http://www.google.com/mapfiles/ms/micons/red.png";
                    icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    Subgurim.Controles.GIcon Licon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Licon.image = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    Subgurim.Controles.GIcon Ticon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Ticon.image = "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
                    double speed = Convert.ToDouble(status.Rows[0][2].ToString());
                    int igst = Convert.ToInt32(status.Rows[0][8].ToString());
                    if (igst == 0 && speed == 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        options.icon = icon;
                    }
                    else if (speed > 3 && igst == 1)
                    {
                        string sped = Convert.ToString(speed * 1.85);
                        lblspeed.Text = sped;
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        options.icon = Licon;
                    }
                    else if (speed <= 3 && igst == 1)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        lblspeed.Text = sped;
                        options.icon = Iicon;
                    }
                    else if (igst == 0 && speed != 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "Towed At:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        lblspeed.Text = sped;
                        options.icon = Ticon;
                    }
                    GMap1.Add(oMarker);
                }
            }
            Timer2.Enabled = false;
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["UserID"] != null && Session["selectedVehicle"] != null)
            {
                string vno = Session["selectedVehicle"].ToString();
                tkmh(vno);
 
 
                DBClass db = new DBClass();
 
 
                DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
                DateTime LocDT = Convert.ToDateTime(status.Rows[0][4]);
                DateTime sysDT = DateTime.Now;
                DateTime addDT = LocDT.AddMinutes(4);
                Select_Vehicle(status, vno);
                if (status.Rows[0][8].ToString() == "1" && status.Rows[0][2].ToString() != "0" && (sysDT < addDT))
                {
                    //---Update Location-----
                    try
                    {
                        string sMapKey = "";
                        if (Session["APIKey"].ToString() != "")
                        {
                            sMapKey = Session["APIKey"].ToString();
                        }
                        else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                        GeoCode objAddress = new GeoCode();
                        objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                        StringBuilder sb = new StringBuilder();
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            Session["address"] = sb.ToString();
                        }
                        Timer2.Enabled = true;
                        Session["timerc"] = 1;
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                    //-------Total KM Run-------
                    tkmh(vno);
                    DataTable tktable = db.gettotalkm(vno);
                    double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                    DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
                    tktkm = Math.Truncate(tktkm * 100) / 100;
                    lbltkm.Text = tktkm.ToString() + "&nbsp;&nbsp;KM  at " + dtkmt.ToShortTimeString();
                    Totalrunkm(vno);
                    //------Over SPeed ---------            
                    DataTable vdt = db.vehicledetailsalerts(vno);
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                        lblovs.Text = "(" + speed1.ToString() + " limit)";
                        speed1 = Convert.ToInt32(speed1 / 1.85);
                        string overspeed = speed1.ToString();
                        DataTable dtdevice = db.getdevicesid(vno);
                        string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                        string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                        string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                        DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                        if (dtoverspeed.Rows.Count != 0)
                        {
                            double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[0][0].ToString());
                            overspeed1 = overspeed1 * 1.85;
                            string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[0][1]).ToShortTimeString();
                            lbloverspeed.Text = overspeed1 + " Km/h at " + ovsdate;
                        }
                        else { lbloverspeed.Text = "No Over Speed"; }
                    }
                    else { lbloverspeed.Text = "No Over Speed"; lblovs.Text = ""; }
                }
                else if (status.Rows[0][8].ToString() == "1" && status.Rows[0][2].ToString() == "0" && (sysDT < addDT))
                {
                    //----Fuel Status------
                    Fuel_Status(vno);
                }
                lblPlace.Text = Session["address"].ToString();
                if (Session["timerc"].ToString() == "0")
                {
                    Session["timerc"] = 1;
                    Timer2.Enabled = true;
                }
            }          
           
        }
        public void Vehicle_Status(string vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
            Select_Vehicle(status, vno);
            DataTable DriDet = db.GerDriverDetails(vno, Session["UserID"].ToString());
            lblDriverName.Text = DriDet.Rows[0][0].ToString();
            lblOwnerName.Text = DriDet.Rows[0][1].ToString();
            lblDriverMobNo.Text = DriDet.Rows[0][2].ToString();
            lblVehModel.Text = DriDet.Rows[0][3].ToString();
            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            //-----Geofence------
            Geofencestatus(vno);
            //---Fuel Status------
            Fuel_Status(vno);
            //-----Location-----
            try
            {
                GeoCode objAddress = new GeoCode();
                objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                StringBuilder sb = new StringBuilder();
                if (objAddress.valid)
                {
                    sb.Append(objAddress.Placemark.address.ToString());
                    Session["address"] = sb.ToString();
                    lblPlace.Text = Session["address"].ToString();
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            //-------Total KM Run-------
            tkmh(vno);
            DataTable tktable = db.gettotalkm(vno);
            double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
            DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
            tktkm = Math.Truncate(tktkm * 100) / 100;
            lbltkm.Text = tktkm.ToString() + "&nbsp;&nbsp;KM  at " + dtkmt.ToShortTimeString();
            Totalrunkm(vno);

            //------Over SPeed ---------            
            DataTable vdt = db.vehicledetailsalerts(vno);
            if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
            {
                int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                lblovs.Text = "(" + speed1.ToString() + " limit)";
                speed1 = Convert.ToInt32(speed1 / 1.85);
                string overspeed = speed1.ToString();
                DataTable dtdevice = db.getdevicesid(vno);
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                if (dtoverspeed.Rows.Count != 0)
                {
                    double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[0][0].ToString());
                    overspeed1 = overspeed1 * 1.85;
                    string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[0][1]).ToShortTimeString();
                    lbloverspeed.Text = overspeed1 + " Km/h at " + ovsdate;
                }
                else { lbloverspeed.Text = "No Over Speed"; }
            }
            else { lbloverspeed.Text = "No Over Speed"; lblovs.Text = ""; }           
        }
        public void Geofencestatus(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            string Gv = db.Geo_Validation(Session["UserID"].ToString(), Vno);
            if (Gv == "0" || Gv == "")
            {
                lblgeofence.Text = "Not Assigned";
                lblgeotype.Text = "";
                lblgeofence.ForeColor = System.Drawing.Color.Black;
                lbtnview.Visible = false;
            }
            else
            {
                lblgeofence.ForeColor = System.Drawing.Color.Red;
                lblgeofence.Text = "Assigned";
                string[] parts = Gv.Split(' ');
                lblgeotype.Text = "(" + parts[1].ToString() + ")";
                lbtnview.Visible = true;
            }
        }
        public void Fuel_Status(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable fuelim = db.Getfuelimp(Session["Cusername"].ToString());
            if (fuelim.Rows[0][1].ToString() == "Yes")
            {

                DataTable dt2 = db.prevfuelstaus(Vno);
                if (dt2.Rows.Count != 0)
                {
                    if (Convert.ToInt32(dt2.Rows[0][1].ToString()) == 1)
                    {
#pragma warning disable CS0168 // The variable 'fulestat' is declared but never used
                        double fulestat;
#pragma warning restore CS0168 // The variable 'fulestat' is declared but never used
                        DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                        double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                        double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                        int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                        if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
                        {
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                lblFuel.Text = b.ToString();
                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                lblFuel.Text = f.ToString();
                            }
                        }
                        else
                        {
                            lblFuel.Text = "Fuel parameters are not calibrated..";


                        }
                    }
                }
                else lblFuel.Text = "Waiting...";
            }
            else
            {
                lblFuel.Text = " Service Not Availed";
                // string alertstring = " Fuel thing not implimented";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
        }
        public void Select_Vehicle(DataTable status, string vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            //----LAT&lNG----
            double loc = Convert.ToDouble(status.Rows[0][0]);
            double loca = Convert.ToDouble(status.Rows[0][1]);
            lblLocation.Text = Convert.ToString(loc) + " &nbsp;&nbsp;  " + Convert.ToString(loca);
            //----Check Availability----
            checkavailabe(vno);
            //----Enginr ON/OFF Status----
            if (status.Rows[0][8].ToString() == "0")
                lblEngine.Text = "OFF";
            else
                lblEngine.Text = "ON";
            //----BAT_ADC----
            double batADC = Convert.ToDouble(status.Rows[0][3].ToString());
            batADC = Convert.ToDouble(batADC / 42.50);
            double ten = 10;
            batADC = Math.Truncate(batADC * 100) / 100;
            lblVehBatStat.Text = batADC.ToString();
            if (Convert.ToDouble(status.Rows[0][3]) <= ten)
            {
                lblVehBatStat.Text = "Battery Not Connected";
            }
            else if (status.Rows[0][8].ToString() == "0")
            {
                lblVehBatStat.Text = "Discharging" + "&nbsp;&nbsp;" + batADC.ToString() + "&nbsp;" + " Volts ";
            }
            else if (status.Rows[0][8].ToString() == "1" && status.Rows[0][3].ToString() != "0")
            {
                lblVehBatStat.Text = "Charging" + "&nbsp;&nbsp;" + batADC.ToString() + "&nbsp;" + " Volts ";
            }

            //------LastString Date---------
            DateTime LocDT = Convert.ToDateTime(status.Rows[0][4]);
            lblLocDT.Text = LocDT.ToString("dd-MM-yyyy hh:mm tt");
            lblVehNo.Text = status.Rows[0][12].ToString();
            //----Check No of Satilites----
            DateTime sysDT = DateTime.Now;
            DateTime DT = LocDT;
            DateTime addDT = LocDT.AddMinutes(30);
            if (sysDT > addDT)
            {
                lblUnitStat.Text = "Offline";
                lblUnitStat.Text += " - 0 Satellites locked";
                lblUnitStat.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            }
            else
            {
                lblUnitStat.Text = "Online";
                lblUnitStat.Text += " - " + status.Rows[0][7].ToString() + " Satellites locked";
                lblUnitStat.Attributes.Add("style", "text-decoration:none;color:black;font-weight:bold");
            }
            //------- Power ------
            string Power = status.Rows[0][5].ToString();
            if (Power == "1")
            {
                lblPower.Text = "Main Battery";
                lblPower.Attributes.Add("style", "text-decoration:none;color:black;font-weight:bold");
            }
            else if (Power == "0")
            {
                lblPower.Text = "Backup Battery";
                lblPower.Attributes.Add("style", "text-decoration:none;color:red;font-weight:bold");
            }
            //------GPS Antenna------- updated on 21/6/2013
            string condition = status.Rows[0][9].ToString();
            if (condition == "100" || condition.Length == 5)
            {
                lblGPSant.Text = "Not Available";
                lblGPSant.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            }
            else if (condition == "0" || condition.Length < 5)
            {
                lblGPSant.Text = "Available";
                lblGPSant.Attributes.Add("style", "text-decoration:none;color:black;font-weight:bold");
            }
            //----Tempature----
            if (condition != "0") // && condition != "100")
            {
                Temp(Session["selectedVehicle"].ToString());
              
            }
            else if (condition == "0")
            {
                lbltemp.Text = "0 °C";
            }

            //ac on or off
            ac(Session["selectedVehicle"].ToString());
            //----Check Engine Status----
            Enginestatus(vno);
            //-----Check Speed------
            double speed = Convert.ToDouble(status.Rows[0][2].ToString());
            if (speed > 3)
            {
                string sped = Convert.ToString(speed * 1.85);
                lblspeed.Text = sped;
                DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
            }
            else if (speed < 3)
            {
                string sped = "0";
                DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                lblspeed.Text = sped;
            }
        }
        public void checkavailabe(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DateTime dt1 = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 12:00 AM");
            DateTime dt2 = Convert.ToDateTime(DateTime.Now.ToString());
            if (dt2 < dt1)
            {
                dt1 = Convert.ToDateTime(dt2.ToShortDateString() + " 12:00 AM");
            }
            TimeSpan ts = dt2 - dt1;
            DataTable dt = db.getblackspot(Vno, dt1.ToString(), dt2.ToString());
            double sysdata = Convert.ToDouble(dt.Rows[0][0].ToString());
            double av = ((ts.TotalMinutes - sysdata) / ts.TotalMinutes) * 100;
            av = 100 - av;
            av = (double)Math.Round(av, 1);
            if (av < 90 && av > 0)
            {
                av = av + 5;
            }
            else if (av < 95 && av > 0)
            {
                av = av + 2;
            }
            else if (av < 98 && av > 0)
            {
                av = av + 1;
            }
            else if (av >= 100)
            {
                av = 100;
            }
            lblavailable.Text = " Working at " + av.ToString() + " % Uptime";
        }
        public void Enginestatus(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable VS = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            int Hours = 0;
            int Minutes = 0;
            if (VS.Rows[0][8].ToString() == "0")
            {
                DataTable pdt = db.GetPreviIGST121(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pdt.Rows.Count != 0)
                {
                    DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                    TimeSpan ts = dtst - dtst1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    if (m == 1)
                    {
                        Timer2.Enabled = true;
                    }
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Stationary since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pdt1 = db.GetPreviIGST221(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    if (pdt1.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        Hours = m / 60;
                        Minutes = m % 60;
                        lblEnginestatus.Text = "Stationary since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";

                    }
                }

            }
            else if (VS.Rows[0][8].ToString() == "1" && Convert.ToInt32(VS.Rows[0][2].ToString()) <= 3)
            {
                DataTable pidt = db.GetPreviING1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtid = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pidt.Rows.Count != 0)
                {
                    DateTime dtid1 = Convert.ToDateTime(pidt.Rows[0][0].ToString());
                    TimeSpan ts = dtid - dtid1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    if (m == 1)
                    {
                        Timer2.Enabled = true;
                    }
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Idling since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pidt1 = db.GetPreviIGST33(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtid1 = Convert.ToDateTime(pidt1.Rows[0][0].ToString());
                    TimeSpan ts = dtid - dtid1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Idling since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
            }
            else if (VS.Rows[0][8].ToString() == "1" && Convert.ToInt32(VS.Rows[0][2].ToString()) > 3)
            {
                DataTable pmdt = db.GetPreviMNG2(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pmdt.Rows.Count != 0)
                {
                    DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                    TimeSpan ts = dtmng - dtmng1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Moving since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                    TimeSpan ts = dtmng - dtmng1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Moving since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
            }
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }
        public void Temp(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            string vno = Session["selectedVehicle"].ToString();
            DataTable getreefer = db.Getreeferveh(Vno.ToString());
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
            string condition = status.Rows[0][9].ToString();
            double conditon1 = Convert.ToDouble(condition);
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            if (fre.Rows[0][1].ToString() == "Yes")
            {

                if (conditon1 >= 10000)
                {
                    double temp = 0;
                    double cond1 = conditon1 - 10000;
                    if (cond1 > 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            if (m == 1)
                            {
                                Timer2.Enabled = true;
                            }
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double cond2 = cond1 - 1000;
                            temp = cond2 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                            {
                                lbltemp.ForeColor = System.Drawing.Color.Red;
                                lbltemp2.ForeColor = System.Drawing.Color.Red;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                //lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezer since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                // lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }
                            else
                            {

                                lbltemp.ForeColor = System.Drawing.Color.Red;
                                lbltemp2.ForeColor = System.Drawing.Color.Red;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                //lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezer since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                // lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }

                        }
                    }
                    else if (cond1 < 1000)
                    {

                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = false;
                        }
                        else
                        {

                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = true;
                            lblreefer.Visible = false;
                            lbltemp2.Visible = false;
                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                        }
                    }


                }
                else if (conditon1 >= 1000)
                {
                    DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                    double temp = 0;

                    if (pdt.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        if (m == 1)
                        {
                            Timer2.Enabled = true;
                        }
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        if (temp <= 7)
                        {
                            temp = temp * 1.9;
                        }
                        else if (temp <= 10)
                        {
                            temp = temp * 1.7;
                        }
                        else if (temp <= 14)
                        {
                            temp = temp * 1.4;
                        }
                        else if (temp <= 17)
                        {
                            temp = temp * 1.3;
                        }
                        else if (temp <= 20)
                        {
                            temp = temp * 1.1;
                        }
                        else if (temp > 20)
                        {
                            temp = temp * 1;
                        }
                        temp = (double)Math.Round(temp, 1);
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            //lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");

                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                        }
                        else
                        {
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            //lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");

                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                        }

                    }
                }
                else if (conditon1 < 1000)
                {
                    if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = false;
                        lblreefer.Visible = true;
                        lbltemp2.Visible = false;
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                    }
                    else
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = true;
                        lblreefer.Visible = false;
                        lbltemp2.Visible = false;

                    }
                }

            }
            else
            {
                if (conditon1 >= 10000)
                {
                    double temp = 0;
                    double cond1 = conditon1 - 10000;
                    if (cond1 > 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            if (m == 1)
                            {
                                Timer2.Enabled = true;
                            }
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double cond2 = cond1 - 1000;
                            temp = cond2 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                            {
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                // lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }
                            else
                            {
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                // lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                            }


                        }
                    }

                    else if (cond1 < 1000)
                    {
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = false;
                        }
                        else
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = true;
                            lblreefer.Visible = false;
                            lbltemp2.Visible = false;

                        }
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                    }


                }
                else if (conditon1 >= 1000)
                {
                    DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                    double temp = 0;

                    if (pdt.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        if (m == 1)
                        {
                            Timer2.Enabled = true;
                        }
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Red;
                        lbltemp2.ForeColor = System.Drawing.Color.Red;
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            // lbltemp2.Visible = true;
                            // lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                        }
                        else
                        {
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                        }
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);


                    }
                }
                else if (conditon1 < 1000)
                {
                    if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Attributes.Add("style", "text-decoration:blink");

                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = false;
                        lblreefer.Visible = true;
                        lbltemp2.Visible = false;
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp );
                        //lbltemp.Attributes.Add("style", "text-decoration:blink");
                    }
                    else
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Attributes.Add("style", "text-decoration:blink");

                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = true;
                        lblreefer.Visible = false;
                        lbltemp2.Visible = false;

                    }
                }

            }
        }
        public void ac(string Vno)
        {
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                int Hours = 0;
                int days = 0;
                int Minutes = 0;
                DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
                DataTable acstat = db.Getacveh(Vno);
                DataTable pdt = db.Getprevacstring(acstatus.Rows[0][11].ToString());
                DateTime dtst = Convert.ToDateTime(acstatus.Rows[0][4].ToString());
                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (acstatus.Rows[0][6].ToString() == "0.00")// && pdt.Rows[0][1].ToString() == "0.00")
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        lblac.ForeColor = System.Drawing.Color.Red;
                        lblac.Text = "AC ON since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins";

                    }
                    else
                    {
                        DataTable acoff = db.Getacoff(acstatus.Rows[0][11].ToString());
                        DateTime acof = Convert.ToDateTime(acoff.Rows[0][0].ToString());//"dd-MM-yyyy hh:mm tt");
                        string acofftime = acof.ToString("dd-MM-yyyy hh:mm tt");
                        lblac.Text = "OFF " + "( at " + acofftime + ")";
                        //lblac.Text = "OFF";
                    }
                }
                else
                {
                    lblac.Text = "Service Not Availed";
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

        }
        public void Totalrunkm(string vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.getTotalRunkm(vno);
            if (dt.Rows.Count != 0)
            {
                string fdate = dt.Rows[0][2].ToString();
                double trkm = Convert.ToDouble(dt.Rows[0][3].ToString());
                string tdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                DataTable dt1 = db.Select_TotalRunkm(dt.Rows[0][1].ToString(), fdate, tdate);
                if (dt1.Rows.Count != 0)
                {
                    for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                    {
                        trkm += CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                    }
                    fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    trkm = (double)Math.Round(trkm, 2);

                }
                else
                {
                    fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                }

                db.IU_Totalrunkm(vno, fdate, trkm.ToString());
                DataTable P = db.getPFE(vno);
                DataTable CKMdt = db.Get_Correction_KM(vno);
                double ckm = 0;
                if (CKMdt.Rows[0][0].ToString() != "")
                {
                    ckm = Convert.ToDouble(CKMdt.Rows[0][0].ToString());
                }
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                nw = trkm + nw + ckm;
                lblTotKmRun.Text = nw.ToString() + "&nbsp;&nbsp;KM";
            }
            else
            {
                DataTable P = db.getPFE(vno);
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                string ftime = P.Rows[0][1].ToString();
                DateTime fdate = Convert.ToDateTime(ftime, dtfi);
                db.IU_Totalrunkm(vno, fdate.ToString(), "0");

            }

        }
        public void tkmh(string vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.gettotalkm(vno);
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() != Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                    string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                    db.uptotalkm(vno, fdate, tdate, "0", fdate);
                }
                else if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() == Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    double tkm = Convert.ToDouble(dt.Rows[0][4].ToString());
                    string fdate = dt.Rows[0][2].ToString();
                    string fdate1 = dt.Rows[0][5].ToString();
                    string tdate = dt.Rows[0][3].ToString();
                    DataTable dt1 = db.Select_TotalRunkm(vno, fdate, tdate);
                    if (dt1.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                        {
                            tkm += CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                        }
                        fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();                        
                    }
                    else if (dt1.Rows.Count == 0)
                    {
                        fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");                      
                    }
                    db.uptotalkm(vno, fdate, tdate, tkm.ToString(), fdate);
                }
            }
            else
            {
                string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                db.settotalkm(vno, fdate, tdate, "0");
            }
        }
    }
}

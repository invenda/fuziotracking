﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="fuelfillmap.aspx.cs" Inherits="Tracking.fuelfillmap"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpalceholder1" runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <div style="width: 900px; margin: 0 auto; padding: 0; padding: 0; height: auto;">
            <table border="1" style="width: 100%;">
                <tr align="center">
                    <asp:Label ID="lblsvno" runat="server" Font-Bold="true"></asp:Label>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="600px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tempdatalog.aspx.cs" Inherits="Tracking.Tempdatalog"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report3" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > yesday) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = yesday;
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout ="300"  AsyncPostBackErrorMessage ="TIMED OUT PLAESE TRY AGAIN"> 
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 42px; font-size: 25px; text-align: center; color: Blue;">
        Temperature Data Log Report</div>
    <div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" style="width: 217px">
                        <asp:Label ID="Label1" Text="Select the Vehicle" runat="server" Font-Bold="true"></asp:Label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                            ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                            ValueToCompare="--">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                            ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left" style="width: 132px">
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            DataTextField="Vehicalnumber" Height="27px" Width="167px" Style="margin-left: 1px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 144px">
                        <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                            ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left" style="width: 153px">
                        <asp:TextBox ID="date" runat="server" Width="147px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="date"
                            OnClientDateSelectionChanged="checkDate">
                        </cc1:CalendarExtender>
                    </td>
                    <td style="width: 119px">
                        <asp:RadioButton ID="rbttype" GroupName="vs" runat="server" Text="All Data" AutoPostBack="true"
                            OnCheckedChanged="rbttype_CheckedChanged" />
                    </td>
                    <td style="width: 172px">
                        <asp:RadioButton ID="rbttype1" GroupName="vs" runat="server" Text="Reefer Data" AutoPostBack="true"
                            OnCheckedChanged="rbttype1_CheckedChanged" />
                    </td>
                    <td style="width: 110px">
                        <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                            OnClick="GetRecord_Click1" />
                    </td>
                    <td style="width: 99px">
                        <asp:Button ID="Download" Text="Download Record" runat="server" ValidationGroup="Group1"
                            Width="120px" OnClick="Download_Click" />
                    </td>
                    <td>
                        <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px" OnClick="Refresh_Click" />
                    </td>
                </tr>
            </table>
            <div  style="height: 40px; text-align :center ; ">
                <asp:Label ID="lblerror" runat="server" Font-Bold ="true" ForeColor ="Red" Font-Size ="Larger" ></asp:Label>
               
            </div>
            <div style="height: auto; text-align: center; color: #FE9A2E;">
                <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div align="center">
                            <table>
                                <tr>
                                    <td>
                                        <h1>
                                            <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                        </h1>
                                    </td>
                                    <td>
                                        <h3>
                                            Please wait.....
                                        </h3>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div style="width: 100%; height:auto ; text-align: center; vertical-align: middle">
                <asp:Panel ID="Panel1" runat="server" Visible="false" Width="100%">
                    <table border="4" cellpadding="0" cellspacing="0" style="width: auto; height: auto;
                        border-style:dotted ; margin-right: auto; margin-left: auto;">
                        <tr>
                            <td valign="top">
                                <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HeaderStyle-VerticalAlign="Top"
                                    RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black"
                                    ShowFooter="false" GridLines="None" Font-Size="8pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="85px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport1" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="85px" FooterStyle-BorderColor="#61A6F8">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport2" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="1px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport3" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport4" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport5" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport6" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:GridView ID="gvreport7" runat="server" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" ShowFooter="false"
                                    GridLines="None" Font-Size="6pt">
                                    <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
                                    <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                        BorderWidth="0" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                            ItemStyle-Font-Size="2px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemStyle Font-Size="8px"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <%-- <td valign ="top" >
    <asp:GridView ID="gvreport8" runat="server" AutoGenerateColumns="false" 
        RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black"
        ShowFooter="false" GridLines="None" Font-Size="6pt" >
        <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
            BorderWidth="0" />
        <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size ="3px" />
        <Columns>
            <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="95px" FooterStyle-BorderColor="#61A6F8"
                ItemStyle-Font-Size="2px">
                <ItemTemplate>
                    <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                </ItemTemplate>
                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle Font-Size="8px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="°C" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                ItemStyle-Font-Size="2px">
                <ItemTemplate>
                    <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                </ItemTemplate>
                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                <HeaderStyle Width="20px"></HeaderStyle>
                <ItemStyle Font-Size="8px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </td>
   <td valign ="top" >
    <asp:GridView ID="gvreport9" runat="server" AutoGenerateColumns="false" HeaderStyle-VerticalAlign ="Top"
        RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black"
        ShowFooter="false" GridLines="None" Font-Size="6pt" >
        <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" />
        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
            BorderWidth="0" />
        <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size ="3px" />
        <Columns>
            <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="85px" FooterStyle-BorderColor="#61A6F8"
                ItemStyle-Font-Size="2px">
                <ItemTemplate>
                    <asp:Label ID="lblVehicle" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                </ItemTemplate>
                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle Font-Size="8px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="°C" HeaderStyle-Width="40px" FooterStyle-BorderColor="#61A6F8"
                ItemStyle-Font-Size="2px">
                <ItemTemplate>
                    <asp:Label ID="lblH1" runat="server" Text='<% #Bind("temp") %>'></asp:Label>
                </ItemTemplate>
                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                <HeaderStyle Width="20px"></HeaderStyle>
                <ItemStyle Font-Size="8px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                </div> 
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Download" />
        </Triggers>
    </asp:UpdatePanel>
    
    <div style="height: 30px">
    </div>
   
</asp:Content>

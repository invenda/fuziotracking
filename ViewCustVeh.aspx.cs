﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Tracking
{
    public partial class ViewCustVeh : System.Web.UI.Page
    {
        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();
        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDetails();
            }

        }

        protected void vehicleno_Click(object sender, EventArgs e)
        {
            //Response.Redirect("ViewVehDetails.aspx");

            if (Session["UserID"] != null)
            {
                //---------Very Important-Do not Delete-----------------------------------------------------
                LinkButton bt = (LinkButton)sender;
 
 
                DBClass db = new DBClass();
 
 
                DataTable VehV = db.ViewVehicle(bt.Text, Session["UserID"].ToString());
                Session["RegNo"] = bt.Text;
                Response.Redirect("JustVehView.aspx?RegNo=" + bt);
                //---------Very Important-Do not Delete-----------------------------------------------------

                //LinkButton lnkbtn = sender as LinkButton;
                ////getting particular row linkbutton
                //GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                ////getting userid of particular row
                //string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
                //string username = gvrow.Cells[0].Text;
                //DBClass db = new DBClass();
                //DataTable VehV = db.ViewVehicle(username, Session["ParentUser"].ToString());
                //Session["RegNo"] = username;
                //Response.Redirect("JustVehView.aspx?RegNo=" + username);
            }
        }

        protected void gvrecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (e.Row.Cells[1].Text == "Mapped")
                {
                    e.Row.Visible = false;

                }
                //getting username from particular row
                string username = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Vehicle_number"));
                //identifying the control in gridview
                LinkButton lnkbtnresult = (LinkButton)e.Row.FindControl("lnkdelete");
                //raising javascript confirmationbox whenver user clicks on link button
                lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + username + "')");
            }
        }
        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            LinkButton lnkbtn = sender as LinkButton;
            //getting particular row linkbutton
            GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
            //getting userid of particular row
            string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
            string username = gvrow.Cells[0].Text;

            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete from Veh where Vehicle_number='" + userid + "'", sqlconn);
            cmd.ExecuteNonQuery().ToString();
            sqlconn.Close();

            SqlConnection sqlconn1 = new SqlConnection(connectionstring);
            sqlconn1.Open();
            SqlCommand cmd1 = new SqlCommand("delete from Vehicles where Vehicle_number='" + userid + "'", sqlconn1);
            cmd1.ExecuteNonQuery();
            sqlconn1.Close();

            BindDetails();

            //Displaying alert message after successfully deletion of user
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('" + username + " details deleted successfully')", true);

        }

        private void BindDetails()
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable VV = db.ViewVeh(Session["UserID"].ToString());
            DataTable VVV = db.ViewVeh(Session["UserID"].ToString());
            VV.Columns.Add("Status", typeof(string));
            VVV.Columns.Add("Status", typeof(string));
            VVV.Columns.Add("Action", typeof(string));
            if (VV.Columns.Count > 0)
            {

                for (int i = 0; i < VV.Rows.Count; i++)
                {
                    string venu = VV.Rows[i][0].ToString();
                    DataTable VeNu = db.chkV(venu);
                    if (VeNu.Rows.Count > 0)
                    {
                        if (VeNu.Rows[0][2].ToString() != "") //(VeNu.Columns.Count > 0)
                        {
                            VV.Rows[i]["Status"] = "Mapped";
                        }

                    }
                    else
                    {

                        for (int j = 0; j < VVV.Rows.Count; j++)
                        {
                            string venuu = VVV.Rows[j][0].ToString();
                            DataTable VeNuu = db.chkV(venuu);
                            if (VeNuu.Rows.Count == 0)
                            {

                                VVV.Rows[j]["Status"] = "UnMapped";

                            }

                        }
                        if (VVV.Columns.Count > 0)
                        {

                            for (int j = 0; j < VVV.Rows.Count; j++)
                            {
                                if (VVV.Rows[j][1].ToString() == "")
                                {
                                    VVV.Rows[j].Delete();
                                }
                            }
                        }

                        VVV.AcceptChanges();
                        gvrecords.DataSource = VVV;
                        gvrecords.DataBind();

                    }
                }
            }



            if (VV.Columns.Count > 0)
            {

                for (int i = 0; i < VV.Rows.Count; i++)
                {
                    if (VV.Rows[i][1].ToString() == "")
                    {
                        VV.Rows[i].Delete();
                    }
                }
            }
            VV.AcceptChanges();
            dgViewVeh.DataSource = VV;
            dgViewVeh.DataBind();


        }
    }
}

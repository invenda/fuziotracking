﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class DayKMRunReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        double count = 0;
        double rcount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //DataTable vehicles = db.Get_AssingedVehicles("UserID");
            //gvreport.DataSource = vehicles;
            //gvreport.DataBind();
        }
        protected void btnsubmit_Click1(object sender, EventArgs e)
        {
            DateTime fdateday = Convert.ToDateTime(txtdate.Text);
            DateTime dhdh = Convert.ToDateTime(fdateday);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                Databind();
            }
            else
            {
                Databindold();
            }         
                  
        }
        public void Databind()
        {
            DataTable vehicles = new DataTable();
            if (Session["UserRole"].ToString() == "3")
            {
                DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                vehicles = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
            }
            else if (Session["UserRole"].ToString() == "2")
            {
                vehicles = db.GetAssingedVehicles(Session["UserID"].ToString());
            }
            for (int i = 0; i < vehicles.Rows.Count; i++)
            {
                string vno = vehicles.Rows[i][1].ToString();
                DateTime day = Convert.ToDateTime(txtdate.Text);
                DateTime fdate = Convert.ToDateTime(txtdate.Text);

                DataTable check = db.Check_vehicle_Date(day, vno);
                if (check.Rows.Count == 0)
                {
                    db.Insert_MonthKM(vno, day.ToString("dd/MM/yyyy"), day.Month.ToString(), day.ToShortDateString());
                    DataTable dthours = db.getRhours();
                    for (int j = 0; j < dthours.Rows.Count; j++)
                    {
                        //DataTable dt12 = db.Select_TotalRunkm(vno, fdate.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                        DataTable dt12 = db.Select_TotalRunkm(vno, fdate.ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString());

                        double TKMR = 0;
                        for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                        {
                            TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                        }
                        TKMR = (double)Math.Round(TKMR, 2);

                        db.Update_KMMonth(vno, day.Month.ToString(), TKMR, j, day.ToString("dd/MM/yyyy"));
                        if (dt12.Rows.Count != 0)
                        {
                            fdate = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                        }


                    }
                }
            }
            DataTable data = db.Get_AssingedVehicles(Session["UserID"].ToString(), txtdate.Text);
            gvreport.DataSource = data;
            gvreport.DataBind();
        
        }
        public void Databindold()
        {
            DataTable vehicles = new DataTable();
            if (Session["UserRole"].ToString() == "3")
            {
                DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                vehicles = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
            }
            else if (Session["UserRole"].ToString() == "2")
            {
                vehicles = db.GetAssingedVehicles(Session["UserID"].ToString());
            }
            for (int i = 0; i < vehicles.Rows.Count; i++)
            {
                string vno = vehicles.Rows[i][1].ToString();
                DateTime day = Convert.ToDateTime(txtdate.Text);
                DateTime fdate = Convert.ToDateTime(txtdate.Text);

                DataTable check = db.Check_vehicle_Date(day, vno);
                if (check.Rows.Count == 0)
                {
                    db.Insert_MonthKM(vno, day.ToString("dd/MM/yyyy"), day.Month.ToString(), day.ToShortDateString());
                    DataTable dthours = db.getRhours();
                    for (int j = 0; j < dthours.Rows.Count; j++)
                    {
                        //DataTable dt12 = db.Select_TotalRunkm(vno, fdate.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString());
                        DataTable dt12 = db.Select_TotalRunkmold(vno, fdate.ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString());

                        double TKMR = 0;
                        for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                        {
                            TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                        }
                        TKMR = (double)Math.Round(TKMR, 2);

                        db.Update_KMMonth(vno, day.Month.ToString(), TKMR, j, day.ToString("dd/MM/yyyy"));
                        if (dt12.Rows.Count != 0)
                        {
                            fdate = Convert.ToDateTime(dt12.Rows[dt12.Rows.Count - 1][3].ToString());
                        }


                    }
                }
            }
            DataTable data = db.Get_AssingedVehicles(Session["UserID"].ToString(), txtdate.Text);
            gvreport.DataSource = data;
            gvreport.DataBind();

        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable dt = db.Owner(Session["UserID"].ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 7;
                HeaderCell.BackColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell.BorderColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);


                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + dt.Rows[0][0].ToString();
                HeaderCell4.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell4.ColumnSpan = 11;
                HeaderCell4.BackColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell4.BorderColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell4.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                TableCell HeaderCell22 = new TableCell();
                HeaderCell22.Text = "Time of the Day in Hrs (24 Hours)";
                HeaderCell22.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell22.ColumnSpan = 8;
                HeaderCell22.BackColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell22.BorderColor = System.Drawing.Color.LightSkyBlue;
                HeaderCell22.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell22);               
            }
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                count = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rcount = 0;
                Label lbl1=e.Row .FindControl ("lblH1") as Label ;
                rcount += Convert.ToDouble(lbl1.Text);
                if (Convert.ToDouble(lbl1.Text) == 0)
                {
                    lbl1.Text = " ";
                }
                else if (Convert.ToDouble(lbl1.Text) > 0)
                {
                    e.Row.Cells[1].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl2 = e.Row.FindControl("lblH2") as Label;
                rcount += Convert.ToDouble(lbl2.Text);
                if (Convert.ToDouble(lbl2.Text) == 0)
                {
                    lbl2.Text = " ";
                }
                else if (Convert.ToDouble(lbl2.Text) > 0)
                {
                    e.Row.Cells[2].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl3 = e.Row.FindControl("lblH3") as Label;
                rcount += Convert.ToDouble(lbl3.Text);
                if (Convert.ToDouble(lbl3.Text) == 0)
                {
                    lbl3.Text = " ";
                }
                else if (Convert.ToDouble(lbl3.Text) > 0)
                {
                    e.Row.Cells[3].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl4 = e.Row.FindControl("lblH4") as Label;
                rcount += Convert.ToDouble(lbl4.Text);
                if (Convert.ToDouble(lbl4.Text) == 0)
                {
                    lbl4.Text = " ";
                }
                else if (Convert.ToDouble(lbl4.Text) > 0)
                {
                    e.Row.Cells[4].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl5 = e.Row.FindControl("lblH5") as Label;
                rcount += Convert.ToDouble(lbl5.Text);
                if (Convert.ToDouble(lbl5.Text) == 0)
                {
                    lbl5.Text = " ";
                }
                else if (Convert.ToDouble(lbl5.Text) > 0)
                {
                    e.Row.Cells[5].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl6 = e.Row.FindControl("lblH6") as Label;
                rcount += Convert.ToDouble(lbl6.Text);
                if (Convert.ToDouble(lbl6.Text) == 0)
                {
                    lbl6.Text = " ";
                }
                else if (Convert.ToDouble(lbl6.Text) > 0)
                {
                    e.Row.Cells[6].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl7 = e.Row.FindControl("lblH7") as Label;
                rcount += Convert.ToDouble(lbl7.Text);
                if (Convert.ToDouble(lbl7.Text) == 0)
                {
                    lbl7.Text = " ";
                }
                else if (Convert.ToDouble(lbl7.Text) > 0)
                {
                    e.Row.Cells[7].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl8 = e.Row.FindControl("lblH8") as Label;
                rcount += Convert.ToDouble(lbl8.Text);
                if (Convert.ToDouble(lbl8.Text) == 0)
                {
                    lbl8.Text = " ";
                }
                else if (Convert.ToDouble(lbl8.Text) > 0)
                {
                    e.Row.Cells[8].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl9 = e.Row.FindControl("lblH9") as Label;
                rcount += Convert.ToDouble(lbl9.Text);
                if (Convert.ToDouble(lbl9.Text) == 0)
                {
                    lbl9.Text = " ";
                }
                else if (Convert.ToDouble(lbl9.Text) > 0)
                {
                    e.Row.Cells[9].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl10 = e.Row.FindControl("lblH10") as Label;
                rcount += Convert.ToDouble(lbl10.Text);
                if (Convert.ToDouble(lbl10.Text) == 0)
                {
                    lbl10.Text = " ";
                }
                else if (Convert.ToDouble(lbl10.Text) > 0)
                {
                    e.Row.Cells[10].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl11 = e.Row.FindControl("lblH11") as Label;
                rcount += Convert.ToDouble(lbl11.Text);
                if (Convert.ToDouble(lbl11.Text) == 0)
                {
                    lbl11.Text = " ";
                }
                else if (Convert.ToDouble(lbl11.Text) > 0)
                {
                    e.Row.Cells[11].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl12 = e.Row.FindControl("lblH12") as Label;
                rcount += Convert.ToDouble(lbl12.Text);
                if (Convert.ToDouble(lbl12.Text) == 0)
                {
                    lbl12.Text = " ";
                }
                else if (Convert.ToDouble(lbl12.Text) > 0)
                {
                    e.Row.Cells[12].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl13 = e.Row.FindControl("lblH13") as Label;
                rcount += Convert.ToDouble(lbl13.Text);
                if (Convert.ToDouble(lbl13.Text) == 0)
                {
                    lbl13.Text = " ";
                }
                else if (Convert.ToDouble(lbl13.Text) > 0)
                {
                    e.Row.Cells[13].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl14 = e.Row.FindControl("lblH14") as Label;
                rcount += Convert.ToDouble(lbl14.Text);
                if (Convert.ToDouble(lbl14.Text) == 0)
                {
                    lbl14.Text = " ";
                }
                else if (Convert.ToDouble(lbl14.Text) > 0)
                {
                    e.Row.Cells[14].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl15 = e.Row.FindControl("lblH15") as Label;
                rcount += Convert.ToDouble(lbl15.Text);
                if (Convert.ToDouble(lbl15.Text) == 0)
                {
                    lbl15.Text = " ";
                }
                else if (Convert.ToDouble(lbl15.Text) > 0)
                {
                    e.Row.Cells[15].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl16 = e.Row.FindControl("lblH16") as Label;
                rcount += Convert.ToDouble(lbl16.Text);
                if (Convert.ToDouble(lbl16.Text) == 0)
                {
                    lbl16.Text = " ";
                }
                else if (Convert.ToDouble(lbl16.Text) > 0)
                {
                    e.Row.Cells[16].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl17 = e.Row.FindControl("lblH17") as Label;
                rcount += Convert.ToDouble(lbl17.Text);
                if (Convert.ToDouble(lbl17.Text) == 0)
                {
                    lbl17.Text = " ";
                }
                else if (Convert.ToDouble(lbl17.Text) > 0)
                {
                    e.Row.Cells[17].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl18 = e.Row.FindControl("lblH18") as Label;
                rcount += Convert.ToDouble(lbl18.Text);
                if (Convert.ToDouble(lbl18.Text) == 0)
                {
                    lbl18.Text = " ";
                }
                else if (Convert.ToDouble(lbl18.Text) > 0)
                {
                    e.Row.Cells[18].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl19 = e.Row.FindControl("lblH19") as Label;
                rcount += Convert.ToDouble(lbl19.Text);
                if (Convert.ToDouble(lbl19.Text) == 0)
                {
                    lbl19.Text = " ";
                }
                else if (Convert.ToDouble(lbl19.Text) > 0)
                {
                    e.Row.Cells[19].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl20 = e.Row.FindControl("lblH20") as Label;
                rcount += Convert.ToDouble(lbl20.Text);
                if (Convert.ToDouble(lbl20.Text) == 0)
                {
                    lbl20.Text = " ";
                }
                else if (Convert.ToDouble(lbl20.Text) > 0)
                {
                    e.Row.Cells[20].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl21 = e.Row.FindControl("lblH21") as Label;
                rcount += Convert.ToDouble(lbl21.Text);
                if (Convert.ToDouble(lbl21.Text) == 0)
                {
                    lbl21.Text = " ";
                }
                else if (Convert.ToDouble(lbl21.Text) > 0)
                {
                    e.Row.Cells[21].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl22 = e.Row.FindControl("lblH22") as Label;
                rcount += Convert.ToDouble(lbl22.Text);
                if (Convert.ToDouble(lbl22.Text) == 0)
                {
                    lbl22.Text = " ";
                }
                else if (Convert.ToDouble(lbl22.Text) > 0)
                {
                    e.Row.Cells[22].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl23 = e.Row.FindControl("lblH23") as Label;
                rcount += Convert.ToDouble(lbl23.Text);
                if (Convert.ToDouble(lbl23.Text) == 0)
                {
                    lbl23.Text = " ";
                }
                else if (Convert.ToDouble(lbl23.Text) > 0)
                {
                    e.Row.Cells[23].BackColor = System.Drawing.Color.LightPink;
                }
                Label lbl24 = e.Row.FindControl("lblH24") as Label;
                rcount += Convert.ToDouble(lbl24.Text);
                if (Convert.ToDouble(lbl24.Text) == 0)
                {
                    lbl24.Text = " ";
                }
                else if (Convert.ToDouble(lbl24.Text) > 0)
                {
                    e.Row.Cells[24].BackColor = System.Drawing.Color.LightPink;
                }
                Label total = e.Row.FindControl("lbltotal") as Label;
                total.Text = rcount.ToString()+"";
                count += rcount;

            }
            else if (e.Row.RowType == DataControlRowType.Footer )
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label ;
                    ftotal .Text =count .ToString ()+"";
            }
        }
        
                
     }


}


﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Tracking.report2" EnableEventValidation="false"
    MasterPageFile="~/ESLMaster.Master" CodeBehind="report2.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report3" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout ="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 30px; font-size: 25px; text-align: center; color: Blue;">
        Vehicle Day summary Report
    </div>
    <div style ="height :20px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" Text="Select the Vehicle" runat="server" Font-Bold="true"></asp:Label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                            ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                            ValueToCompare="--">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                            ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                        :
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            DataTextField="Vehicalnumber" Height="23px" Width="188px" Style="margin-left: 1px">
                        </asp:DropDownList>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                            ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="date" runat="server" Width="185px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="date"
                            OnClientDateSelectionChanged="checkDate">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                        <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                            OnClick="GetRecord_Click" />
                    </td>
                    <td>
                        <asp:Button ID="Download" Text="Download Record" runat="server" ValidationGroup="Group1"
                            Width="120px" OnClick="Download_Click" />
                    </td>
                    <td>
                        <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px" OnClick="Refresh_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <div style="height: 20px;">
            </div>
            <div style="height: auto; text-align: center; color: #FE9A2E;">
                <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div align="center">
                            <table>
                                <tr>
                                    <td>
                                        <h1>
                                            <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                        </h1>
                                    </td>
                                    <td>
                                        <h3>
                                            Please wait.....
                                        </h3>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                Width="85%" OnRowDataBound="gv_RowDataBound" ShowFooter="true" RowStyle-ForeColor="Black"
                OnRowCreated="gv_RowCreated" ShowHeaderWhenEmpty="True" EmptyDataText="No records Found">
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                <FooterStyle Font-Bold="true" BackColor="#61A6F8" ForeColor="black" />
                <Columns>
                    <asp:BoundField HeaderText="FromHours" DataField="Fhours" />
                    <asp:BoundField HeaderText="ToHours" DataField="Thours" FooterText="Total" FooterStyle-Font-Bold="true"
                        FooterStyle-HorizontalAlign="Center" />
                    <%--<asp:BoundField HeaderText="Ignition" DataField="igst"  />--%>
                    <asp:TemplateField HeaderText="Vehicle Moving">
                        <ItemTemplate>
                            <asp:Label ID="igst9" runat="server" Text='<%# Bind("igst") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftigst" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField HeaderText="Idle" DataField="idle"  />--%>
                    <asp:TemplateField HeaderText="Vehicle Idle">
                        <ItemTemplate>
                            <asp:Label ID="idle9" runat="server" Text='<%# Bind("idle") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftidle" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField HeaderText="Stationary" DataField="Stat" />--%>
                    <asp:TemplateField HeaderText="Vehicle Stationary">
                        <ItemTemplate>
                            <asp:Label ID="Stat9" runat="server" Text='<%# Bind("Stat") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftstat" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField HeaderText="Total K/m run" DataField="TKMR"  />--%>
                    <asp:TemplateField HeaderText="Total KM run">
                        <ItemTemplate>
                            <asp:Label ID="TKMR9" runat="server" Text='<%# Bind("TKMR") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftTKMR" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField HeaderText="Fuel Status" DataField="FuelS"  />--%>
                    <asp:TemplateField HeaderText="A/C ON Status">
                        <ItemTemplate>
                            <asp:Label ID="ac" runat="server" Text='<%# Bind("AC") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftFuelS" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle HorizontalAlign="Center" />
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Download" />
        </Triggers>
    </asp:UpdatePanel>
    <div style="height: 20px;">
    </div>
    
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.IO;
using System.Threading;
namespace Tracking
{
    public partial class VehicalDailyReport : System.Web.UI.Page
    {

        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";
        DBClass db = new DBClass();
        BLClass bl12 = new BLClass();
        TimeSpan t1 = new TimeSpan();
        string firsttime = "", lasttime = "";
        double tkmh = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                    ddlFromHOURS.DataBind();

                    ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
                    ddlFromMINUTES.DataBind();

                    ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                    ddlToHOURS.DataBind();

                    ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                    ddlToMINUTES.DataBind();
                }
            }
        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false; txtdate.Enabled = false; rbtType.Enabled = false;
            txtfdate.Enabled = false; txttdate.Enabled = false;
        }
        public void enable()
        {
            btndownload.Visible = true;
            btnclear.Visible = true;
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            gvreport.DataSource = null;
            gvreport.DataBind();
            if (rbtType.SelectedValue == "1")
            {
                disable();
                string fdate = txtdate.Text + " " + "12:00 AM";
                string tdate = txtdate.Text + " " + "11:59 PM";

                DataTable dt = db.getdevicesid(ddlMapTOVehicle.SelectedValue.ToString());
                DateTime dhdh = Convert.ToDateTime(fdate);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    //Binddata(fdate, tdate, dt.Rows[0][0].ToString());
                    Fuzio_Binddata(fdate, tdate, dt.Rows[0][0].ToString(),"Tracking");
                }
                else
                {
                    //Binddataold(fdate, tdate, dt.Rows[0][0].ToString());
                    Fuzio_Binddata(fdate, tdate, dt.Rows[0][0].ToString(), "OldTrack");

                }
                enable();
            }
            else if (rbtType.SelectedValue == "2")
            {
                disable();
                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                string fdate = txtfdate.Text + " " + fromtime + " " + AMPM;
                string tdate = txttdate.Text + " " + totime + " " + DT;

                DateTime ffdate = Convert.ToDateTime(fdate);
                DateTime ttdate = Convert.ToDateTime(tdate);
                DateTime chkdate = ffdate.AddDays(5);

                if (ffdate < ttdate)
                {
                    if (ttdate <= chkdate)
                    {
                        DataTable dt = db.getdevicesid(ddlMapTOVehicle.SelectedValue.ToString());
                        DateTime dhdh = Convert.ToDateTime(fdate);
                        int emonth = dhdh.Month;
                        DateTime curretmonth = DateTime.Now;
                        int cmonth = curretmonth.Month;
                        if (emonth == cmonth)
                        {
                            //Binddata(fdate, tdate, dt.Rows[0][0].ToString());
                            Fuzio_Binddata(fdate, tdate, dt.Rows[0][0].ToString(), "Tracking");
                        }
                        else
                        {
                           // Binddataold(fdate, tdate, dt.Rows[0][0].ToString());
                            Fuzio_Binddata(fdate, tdate, dt.Rows[0][0].ToString(), "OldTrack");

                        }
                        enable();
                    }
                    else
                    {
                        string message = "alert('You can not select more than five days from From date..')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                }
                else
                {
                    string message = "alert('To date is not less than to From date.... ')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                }

            }


        }


        public void Fuzio_Binddata(string fdate, string tdate, string did,string tablename)
        {
            DBClass db1 = new DBClass();
            DataTable dt = new DataTable();
            string key;
            if (Session["APIKey"].ToString() != "")
            {
                key = Session["APIKey"].ToString();
            }
            else
            {
                key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            }

            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("slno", typeof(string));
            dt.Columns.Add("ingon", typeof(string));
            dt.Columns.Add("dep", typeof(string));
            dt.Columns.Add("arrival", typeof(string));
            dt.Columns.Add("ingoff", typeof(string));
            dt.Columns.Add("duration", typeof(string));
            dt.Columns.Add("stop", typeof(string));
            dt.Columns.Add("km", typeof(string));

           // DataTable bygtim = db1.fuzio_getbygtim(ddlMapTOVehicle.SelectedValue.ToString(), txtdate.Text, tablename);
            DataTable bygtim = db1.fuzio_getbygtim2(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate, tablename);


            string gtim = "";
            int previgst = 0;
            string igston = "";
            string igstoff = "";
            double totalkm = 0;
            string depart = "";
            string arrival = "";
            int igstat = 0;
            int counter = 0;
         
            string diff = string.Empty;
            DateTime dateFirst, dateSecond;


            for (int i = 0, j = 1; i < bygtim.Rows.Count - 1; i++, j++)
            {

                if(bygtim.Rows[i][4].ToString() == "1" && Convert.ToInt32(bygtim.Rows[i][3].ToString()) > 0)
                {
                    if (previgst == 0)
                    {
                        if (depart != "")
                        {
                            DataRow dr1 = dt.NewRow();
                            dr1["slno"] = counter + 1;
                            dr1["dep"] = depart;
                            dr1["arrival"] = arrival;
                            dr1["ingon"] = igston;
                            dr1["ingoff"] = igstoff;

                         
                            string sDateFrom = igston;
                            string sDateTo = igstoff;
                            if (DateTime.TryParse(sDateFrom, out dateFirst) && DateTime.TryParse(sDateTo, out dateSecond))
                            {
                                TimeSpan timespan = dateSecond - dateFirst;
                                int hour = timespan.Hours;
                                int mins = timespan.Minutes;
                                int secs = timespan.Seconds;
                                diff = hour.ToString("00") + ":" + mins.ToString("00");
                            }



                            dr1["duration"] = diff;
                            dr1["stop"] = "0";
                            dr1["km"] = Math.Round(totalkm,2);
                            dt.Rows.Add(dr1);
                            depart = "";
                        }

                        totalkm = 0;
                        counter++;
                    }
                    if(depart == "")
                    {
                        if(arrival !="")
                        {
                            depart = arrival;
                        }
                        else
                        {
                            string latitude = bygtim.Rows[i][0].ToString();
                            string longitude = bygtim.Rows[i][1].ToString();

                            System.Net.WebClient client = new System.Net.WebClient();
                            string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                            string response = client.DownloadString(urlstring);
                            dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                            if (jsonresponse.results.Count > 0)
                            {
                                string formatted_address = jsonresponse.results[0].formatted_address;
                                depart = formatted_address;
                            }

                            //depart = bygtim.Rows[i][0].ToString() + "," + bygtim.Rows[i][1].ToString();
                        }
                        igston = bygtim.Rows[i][2].ToString();
                        
                    }
                    totalkm += CalcDistance(Convert.ToDouble(bygtim.Rows[i][0].ToString()), Convert.ToDouble(bygtim.Rows[i][1].ToString()), Convert.ToDouble(bygtim.Rows[i+1][0].ToString()), Convert.ToDouble(bygtim.Rows[i+1][1].ToString()));
                    igstat = 1;
                }
                else if(bygtim.Rows[i][4].ToString() == "0")
                {
                    if (previgst == 1)
                    {
                        string latitude = bygtim.Rows[i][0].ToString();
                        string longitude = bygtim.Rows[i][1].ToString();

                        System.Net.WebClient client = new System.Net.WebClient();
                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            arrival = formatted_address;
                        }

                        //arrival = bygtim.Rows[i][0].ToString() + "," + bygtim.Rows[i][1].ToString();
                        igstoff = bygtim.Rows[i][2].ToString();

                    }
                    igstat = 0;
                }

                previgst = igstat;
            }

            for (int i=0;i < dt.Rows.Count-1;i++)
            {
               string row1 =  dt.Rows[i][4].ToString();
               string row2 = dt.Rows[i+1][1].ToString();

                string sDateFrom = row1;
                string sDateTo = row2;
                if (DateTime.TryParse(sDateFrom, out dateFirst) && DateTime.TryParse(sDateTo, out dateSecond))
                {
                    TimeSpan timespan = dateSecond - dateFirst;
                    int hour = timespan.Hours;
                    int mins = timespan.Minutes;
                    int secs = timespan.Seconds;
                    diff = hour.ToString("00") + ":" + mins.ToString("00");
                }

                dt.Rows[i].SetField("stop", diff);

            }
            dt.AcceptChanges();
            gvreport.DataSource = dt;
            gvreport.DataBind();

        }


























        public void Binddataold(string fdate, string tdate, string did)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = db.firstigstonold(fdate, tdate, did);
            string key;
            if (Session["APIKey"].ToString() != "")
            {
                key = Session["APIKey"].ToString();
            }
            else key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

            //    string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("ingon", typeof(string));
            dt.Columns.Add("dep", typeof(string));
            dt.Columns.Add("arrival", typeof(string));
            dt.Columns.Add("ingoff", typeof(string));
            dt.Columns.Add("duration", typeof(string));
            dt.Columns.Add("stop", typeof(string));
            dt.Columns.Add("km", typeof(string));
            int first = 0, last = 0;
            DateTime ontime = DateTime.Now;
            DateTime offtime;
            string onadd = "";
            string offadd = "";
            double lt1 = 0, lg1 = 0;
            int secloc = 0;
            if (dt1.Rows.Count != 0)
            {

                DataTable dt2 = db.igstondetailsold(dt1.Rows[0][3].ToString(), tdate, did);
                if (dt2.Rows[dt2.Rows.Count - 1][4].ToString() == "1")
                {
                    DataRow dr1 = dt2.NewRow();
                    dr1["LAMI"] = dt2.Rows[dt2.Rows.Count - 1][0].ToString();
                    dr1["LGMI"] = dt2.Rows[dt2.Rows.Count - 1][1].ToString();
                    dr1["SPED"] = "0";
                    dr1["GTIM"] = dt2.Rows[dt2.Rows.Count - 1][3].ToString();
                    dr1["IGST"] = "0";
                    dt2.Rows.Add(dr1);
                }
                DateTime dtlastt = Convert.ToDateTime(dt2.Rows[dt2.Rows.Count - 1][3].ToString());
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 1)
                    {
                        if (first == 0)
                        {

                            string latitude = dt2.Rows[i][0].ToString();
                            string longitude = dt2.Rows[i][1].ToString();

                            if (onadd == "")
                            {
                                System.Net.WebClient client = new System.Net.WebClient();

                                string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                string response = client.DownloadString(urlstring);
                                dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);

                                if (jsonresponse.results.Count > 0)
                                {
                                    string formatted_address = jsonresponse.results[0].formatted_address;
                                    onadd = formatted_address;
                                }
                            }
                            dr["ingon"] = ontime.ToShortTimeString();
                            dr["dep"] = onadd;
                            first++; last = 0;
                            ontime = Convert.ToDateTime(dt2.Rows[i][3].ToString());

                        }
                        else if (dtlastt == Convert.ToDateTime(dt2.Rows[i][3].ToString()) && Convert.ToDateTime(tdate) < DateTime.Now)
                        {
                            if (last == 0)
                            {
                                string ps = offadd;
                                string latitude = dt2.Rows[i][0].ToString();
                                string longitude = dt2.Rows[i][1].ToString();

                                System.Net.WebClient client = new System.Net.WebClient();

                                string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                string response = client.DownloadString(urlstring);
                                dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);

                                if (jsonresponse.results.Count > 0)
                                {
                                    string formatted_address = jsonresponse.results[0].formatted_address;
                                    offadd = formatted_address;
                                }

                                offtime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                                dr["ingon"] = ontime.ToString();
                                dr["dep"] = ps;
                                dr["arrival"] = offadd;
                                dr["ingoff"] = offtime.ToString();
                                DataTable dtigst = db.totaligstonold(ontime.ToString(), offtime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                                TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                                string str = ts.ToString();
                                if (ts.Hours == 0 && ts.Minutes == 0)
                                {
                                    str = "        ";
                                }
                                dr["duration"] = str.Substring(0, 5);
                                last++; first = 0;
                                string str1 = "00:00";
                                DataTable dt3 = db.firstigstonold(Convert.ToString(offtime), tdate, did);
                                if (dt3.Rows.Count != 0)
                                {
                                    ts = Convert.ToDateTime(dt3.Rows[0][3].ToString()) - offtime;
                                    str1 = ts.ToString();
                                }
                                dr["stop"] = str1.Substring(0, 5);
                                double skm = 0;
                                DataTable dtdis = db.igstondetailsold(ontime.ToString(), offtime.ToString(), did);
                                for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                                {
                                    skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                                }

                                string st1 = string.Format("{0:00.00}", skm);
                                dr["km"] = st1;
                                dt.Rows.Add(dr); secloc++;
                            }

                        }
                    }
                    else if (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) == 0)
                    {
                        if (last == 0)
                        {
                            if (secloc != 0)
                            {
                                onadd = offadd;
                            }

                            string latitude = dt2.Rows[i][0].ToString();
                            string longitude = dt2.Rows[i][1].ToString();

                            System.Net.WebClient client = new System.Net.WebClient();
                            string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                            string response = client.DownloadString(urlstring);
                            dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                            if (jsonresponse.results.Count > 0)
                            {
                                string formatted_address = jsonresponse.results[0].formatted_address;
                                offadd = formatted_address;
                            }

                            offtime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            dr["ingon"] = ontime.ToString();
                            if (onadd == "")
                            {
                                onadd = "Location Not Available";
                            }
                            if (offadd == "")
                            {
                                offadd = "Location Not Available";
                            }
                            dr["dep"] = onadd;
                            dr["arrival"] = offadd;
                            dr["ingoff"] = offtime.ToString();
                            DataTable dtigst = db.totaligstonold(ontime.ToString(), offtime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            if (ts.Hours == 0 && ts.Minutes == 0)
                            {
                                str = "        ";
                            }
                            dr["duration"] = str.Substring(0, 5);
                            last++; first = 0;
                            string str1 = "00:00";
                            DataTable dt3 = db.firstigstonold(Convert.ToString(offtime), tdate, did);
                            if (dt3.Rows.Count != 0)
                            {
                                ts = Convert.ToDateTime(dt3.Rows[0][3].ToString()) - offtime;
                                str1 = ts.ToString();
                            }
                            dr["stop"] = str1.Substring(0, 5);

                            double skm = 0;
                            DataTable dtdis = db.igstondetailsold(ontime.ToString(), offtime.ToString(), did);
                            for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                            {
                                skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                            }

                            string st1 = string.Format("{0:00.00}", skm);
                            dr["km"] = st1;
                            dt.Rows.Add(dr); secloc++;
                        }
                    }

                }
            }
            if (dt.Rows.Count != 0)
            {
                enable();
                lblnoact.Text = "";
            }
            else if (dt.Rows.Count == 0)
            {
                DateTime st1;
                DateTime st2;
                string tt = "";
                if (rbtType.SelectedValue == "1")
                {
                    st1 = Convert.ToDateTime(txtdate.Text);
                    st2 = DateTime.Now;
                    TimeSpan ts1;
                    TimeSpan ts2;

                    if (st1.ToShortDateString() == st2.ToShortDateString())
                    {
                        ts1 = new TimeSpan(st2.Hour, st2.Minute, st2.Second);
                        ts2 = new TimeSpan(st1.Hour, st1.Minute, st1.Second);
                        tt = (ts1 - ts2).ToString();
                    }
                    else if (st1 < st2)
                    {
                        tt = "24:00";
                    }
                    lblnoact.Text = "No Activity Vehicle Stoped" + ":  " + tt.Substring(0, 2) + " Hr " + tt.Substring(3, 2) + " Min ";
                }
                else if (rbtType.SelectedValue == "2")
                {
                    st1 = Convert.ToDateTime(txtfdate.Text);
                    st2 = Convert.ToDateTime(txtfdate.Text);
                    lblnoact.Text = "No Activity Vehicle Stoped";
                }


            }
            gvreport.DataSource = dt;
            gvreport.DataBind();

        }
        public void Binddata(string fdate, string tdate, string did)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = db.firstigston(fdate, tdate, did);
            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            dt.Columns.Add("ingon", typeof(string));
            dt.Columns.Add("dep", typeof(string));
            dt.Columns.Add("arrival", typeof(string));
            dt.Columns.Add("ingoff", typeof(string));
            dt.Columns.Add("duration", typeof(string));
            dt.Columns.Add("stop", typeof(string));
            dt.Columns.Add("km", typeof(string));
            int first = 0, last = 0;
            DateTime ontime = DateTime.Now;
            DateTime offtime;
            string onadd = "";
            string offadd = "";
            double lt1 = 0, lg1 = 0;
            int secloc = 0;
            if (dt1.Rows.Count != 0)
            {

                DataTable dt2 = db.igstondetails(dt1.Rows[0][3].ToString(), tdate, did);
                if (dt2.Rows[dt2.Rows.Count - 1][4].ToString() == "1")
                {
                    DataRow dr1 = dt2.NewRow();
                    dr1["LAMI"] = dt2.Rows[dt2.Rows.Count - 1][0].ToString();
                    dr1["LGMI"] = dt2.Rows[dt2.Rows.Count - 1][1].ToString();
                    dr1["SPED"] = "0";
                    dr1["GTIM"] = dt2.Rows[dt2.Rows.Count - 1][3].ToString();
                    dr1["IGST"] = "0";
                    dt2.Rows.Add(dr1);
                }
                DateTime dtlastt = Convert.ToDateTime(dt2.Rows[dt2.Rows.Count - 1][3].ToString());
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    if (dt2.Rows[i][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[i][2].ToString()) >= 1)
                    {
                        if (first == 0)
                        {
                            // gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt2.Rows[i][0]), Convert.ToDouble(dt2.Rows[i][1]));

                            string latitude = dt2.Rows[i][0].ToString();
                            string longitude = dt2.Rows[i][1].ToString();
                            if (onadd == "")
                            {
                                System.Net.WebClient client = new System.Net.WebClient();

                                string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                string response = client.DownloadString(urlstring);
                                dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);

                                if (jsonresponse.results.Count > 0)
                                {
                                    string formatted_address = jsonresponse.results[0].formatted_address;
                                    onadd = formatted_address;
                                }

                                // GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                //GMap gMap1 = new GMap();
                                //gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                //StringBuilder sb = new StringBuilder();
                                //if ((null != geocode) && geocode.valid)
                                //{
                                //  sb.AppendFormat(geocode.Placemark.address);
                                //}
                                //onadd = sb.ToString();

                            }
                            dr["ingon"] = ontime.ToShortTimeString();
                            dr["dep"] = onadd;
                            first++; last = 0;
                            ontime = Convert.ToDateTime(dt2.Rows[i][3].ToString());

                        }
                        else if (dtlastt == Convert.ToDateTime(dt2.Rows[i][3].ToString()) && Convert.ToDateTime(tdate) < DateTime.Now)
                        {
                            if (last == 0)
                            {
                                string ps = offadd;

                                string latitude = dt2.Rows[i][0].ToString();
                                string longitude = dt2.Rows[i][1].ToString();

                                System.Net.WebClient client = new System.Net.WebClient();

                                string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                string response = client.DownloadString(urlstring);
                                dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);

                                if (jsonresponse.results.Count > 0)
                                {
                                    string formatted_address = jsonresponse.results[0].formatted_address;
                                    offadd = formatted_address;
                                }
                                //Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt2.Rows[i][0]), Convert.ToDouble(dt2.Rows[i][1]));
                                //GeoCode geocode = GMap.geoCodeRequest(gLatLng1, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                //GMap gMap1 = new GMap();
                                //gMap1.getGeoCodeRequest(gLatLng1, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                //StringBuilder sb1 = new StringBuilder();
                                //if ((null != geocode) && geocode.valid)
                                //{
                                //  sb1.AppendFormat(geocode.Placemark.address);
                                //}
                                //offadd = sb1.ToString();
                                offtime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                                dr["ingon"] = ontime.ToString();
                                dr["dep"] = ps;
                                dr["arrival"] = offadd;
                                dr["ingoff"] = offtime.ToString();
                                DataTable dtigst = db.totaligston(ontime.ToString(), offtime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                                TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                                string str = ts.ToString();
                                if (ts.Hours == 0 && ts.Minutes == 0)
                                {
                                    str = "        ";
                                }
                                dr["duration"] = str.Substring(0, 5);
                                last++; first = 0;
                                string str1 = "00:00";
                                DataTable dt3 = db.firstigston(Convert.ToString(offtime), tdate, did);
                                if (dt3.Rows.Count != 0)
                                {
                                    ts = Convert.ToDateTime(dt3.Rows[0][3].ToString()) - offtime;
                                    str1 = ts.ToString();
                                }
                                // Convert.ToDateTime(dt2.Rows[i][3].ToString());
                                dr["stop"] = str1.Substring(0, 5);
                                double skm = 0;
                                DataTable dtdis = db.igstondetails(ontime.ToString(), offtime.ToString(), did);
                                for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                                {
                                    skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                                }
                                //string tdkm = db.getdistance(Convert.ToString(gLatLng0), Convert.ToString(gLatLng1));
                                //string[] kmstr = tdkm.Split(' ');
                                //double skm = 0;
                                //if (kmstr[1].ToString() == "m")
                                //{
                                //    skm = Convert.ToDouble(kmstr[0].ToString());
                                //    skm = skm / 1000;

                                //}
                                //else
                                //{
                                //    skm = Convert.ToDouble(kmstr[0].ToString());
                                //}
                                string st1 = string.Format("{0:00.00}", skm);
                                dr["km"] = st1;
                                dt.Rows.Add(dr); secloc++;
                            }

                        }
                    }
                    else if (dt2.Rows[i][4].ToString() == "0" && Convert.ToInt32(dt2.Rows[i][2].ToString()) == 0)
                    {
                        if (last == 0)
                        {
                            if (secloc != 0)
                            {
                                onadd = offadd;
                            }
                            string latitude = dt2.Rows[i][0].ToString();
                            string longitude = dt2.Rows[i][1].ToString();

                            System.Net.WebClient client = new System.Net.WebClient();

                            string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                            string response = client.DownloadString(urlstring);
                            dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);


                            if (jsonresponse.results.Count > 0)
                            {
                                string formatted_address = jsonresponse.results[0].formatted_address;
                                offadd = formatted_address;
                            }
                            //Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt2.Rows[i][0]), Convert.ToDouble(dt2.Rows[i][1]));
                            //GeoCode geocode = GMap.geoCodeRequest(gLatLng1, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                            //GMap gMap1 = new GMap();
                            //gMap1.getGeoCodeRequest(gLatLng1, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                            //StringBuilder sb1 = new StringBuilder();
                            //if ((null != geocode) && geocode.valid)
                            //{
                            //  sb1.AppendFormat(geocode.Placemark.address);
                            //}
                            //offadd = sb1.ToString();
                            offtime = Convert.ToDateTime(dt2.Rows[i][3].ToString());
                            dr["ingon"] = ontime.ToString();
                            if (onadd == "")
                            {
                                onadd = "Location Not Available";
                            }
                            if (offadd == "")
                            {
                                offadd = "Location Not Available";
                            }
                            dr["dep"] = onadd;
                            dr["arrival"] = offadd;
                            dr["ingoff"] = offtime.ToString();
                            DataTable dtigst = db.totaligston(ontime.ToString(), offtime.ToString(), ddlMapTOVehicle.SelectedValue.ToString());
                            TimeSpan ts = new TimeSpan(0, Convert.ToInt32(dtigst.Rows[0][0].ToString()), 0);
                            string str = ts.ToString();
                            if (ts.Hours == 0 && ts.Minutes == 0)
                            {
                                str = "        ";
                            }
                            dr["duration"] = str.Substring(0, 5);
                            last++; first = 0;
                            string str1 = "00:00";
                            DataTable dt3 = db.firstigston(Convert.ToString(offtime), tdate, did);
                            if (dt3.Rows.Count != 0)
                            {
                                ts = Convert.ToDateTime(dt3.Rows[0][3].ToString()) - offtime;
                                str1 = ts.ToString();
                            }
                            dr["stop"] = str1.Substring(0, 5);

                            double skm = 0;
                            DataTable dtdis = db.igstondetails(ontime.ToString(), offtime.ToString(), did);
                            for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                            {
                                skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                            }

                            //string tdkm = db.getdistance(Convert.ToString(gLatLng0), Convert.ToString(gLatLng1));
                            //string[] kmstr = tdkm.Split(' ');
                            //double skm = 0;
                            //if (kmstr[1].ToString() == "m")
                            //{
                            //    skm = Convert.ToDouble(kmstr[0].ToString());
                            //    skm = skm / 1000;

                            //}
                            //else
                            //{
                            //    skm = Convert.ToDouble(kmstr[0].ToString());
                            //}
                            string st1 = string.Format("{0:00.00}", skm);
                            dr["km"] = st1;
                            dt.Rows.Add(dr); secloc++;
                        }
                    }

                }
            }
            if (dt.Rows.Count != 0)
            {
                enable();
                lblnoact.Text = "";
            }
            else if (dt.Rows.Count == 0)
            {
                DateTime st1;
                DateTime st2;
                string tt = "";
                if (rbtType.SelectedValue == "1")
                {
                    st1 = Convert.ToDateTime(txtdate.Text);
                    st2 = DateTime.Now;
                    TimeSpan ts1;
                    TimeSpan ts2;

                    if (st1.ToShortDateString() == st2.ToShortDateString())
                    {
                        ts1 = new TimeSpan(st2.Hour, st2.Minute, st2.Second);
                        ts2 = new TimeSpan(st1.Hour, st1.Minute, st1.Second);
                        tt = (ts1 - ts2).ToString();
                    }
                    else if (st1 < st2)
                    {
                        tt = "24:00";
                    }
                    lblnoact.Text = "No Activity Vehicle Stoped" + ":  " + tt.Substring(0, 2) + " Hr " + tt.Substring(3, 2) + " Min ";
                }
                else if (rbtType.SelectedValue == "2")
                {
                    st1 = Convert.ToDateTime(txtfdate.Text);
                    st2 = Convert.ToDateTime(txtfdate.Text);
                    lblnoact.Text = "No Activity Vehicle Stoped";
                }

            }
            gvreport.DataSource = dt;
            gvreport.DataBind();

        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        public double dis3(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371;
            double dLat = this.toRadian(lat2 - lat1);
            double dLon = this.toRadian(lon2 - lon1);
            //double R = 6371; // km
            //double dLat = (lat2 - lat1).toRad();
            //double dLon = (lon2 - lon1).toRad();
            //double lat1 = lat1.toRad();
            //double lat2 = lat2.toRad();

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d;

        }


        public double distance2(double X1, double Y1, double X2, double Y2)
        {
            double R = 6371;
            double dLat = this.toRadian(Y2 - Y1);
            double dLon = this.toRadian(X2 - X1);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(this.toRadian(Y1)) * Math.Cos(this.toRadian(Y2)) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
            double d = R * c;
            return d;

        }

        private double toRadian(double val)
        {
            return (Math.PI / 180) * val;
        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (rbtType.SelectedValue == "1")
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

                    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Vehical No.:" + ddlMapTOVehicle.SelectedValue.ToString();
                    HeaderCell.ColumnSpan = 1;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    TableCell HeaderCell1 = new TableCell();
                    HeaderCell1.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
                    HeaderCell1.ColumnSpan = 1;
                    HeaderCell1.Font.Bold = true;
                    HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell1);
                    TableCell HeaderCell2 = new TableCell();
                    HeaderCell2.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                    HeaderCell2.ColumnSpan = 1;
                    HeaderCell2.Font.Bold = true;
                    HeaderCell2.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell2.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell2);
                    TableCell HeaderCell3 = new TableCell();
                    HeaderCell3.Text = "Requested Date and Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm ").ToString();
                    HeaderCell3.ColumnSpan = 4;
                    HeaderCell3.Font.Bold = true;
                    HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell3);
                    gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);
                }
            }
            else if (rbtType.SelectedValue == "2")
            {
                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                string fdate = txtfdate.Text + " " + fromtime + " " + AMPM;
                string tdate = txttdate.Text + " " + totime + " " + DT;

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

                    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Vehical No.:" + ddlMapTOVehicle.SelectedValue.ToString();
                    HeaderCell.ColumnSpan = 1;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    TableCell HeaderCell1 = new TableCell();
                    HeaderCell1.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
                    HeaderCell1.ColumnSpan = 1;
                    HeaderCell1.Font.Bold = true;
                    HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell1);
                    TableCell HeaderCell2 = new TableCell();
                    HeaderCell2.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                    HeaderCell2.ColumnSpan = 1;
                    HeaderCell2.Font.Bold = true;
                    HeaderCell2.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell2.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell2);
                    TableCell HeaderCell3 = new TableCell();
                    HeaderCell3.Text = "Requested from  Date and Time:" + fdate + "," + "Requested To Date and Time:" + tdate;
                    HeaderCell3.ColumnSpan = 4;
                    HeaderCell3.Font.Bold = true;
                    HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell3);
                    gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);
                }
            }
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Enabled = true;
            Response.Redirect(Request.RawUrl);

        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (rbtType.SelectedValue == "1")
            {
                string fdt = txtdate.Text + " " + "12:00 AM";
                DateTime dhdh = Convert.ToDateTime(fdt);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lb = (Label)e.Row.FindControl("lblstop");
                        Label lbkm = (Label)e.Row.FindControl("lblkm");
                        tkmh += Convert.ToDouble(lbkm.Text);
                        if (e.Row.RowIndex == 0)
                        {
                            Label lbon = (Label)e.Row.FindControl("lblon");
                            firsttime = txtdate.Text + " " + lbon.Text;
                        }
                        Label lboff = (Label)e.Row.FindControl("lbloff");
                        lasttime = lboff.Text;
                    }
                    else if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        Label flbm = (Label)e.Row.FindControl("lblmov");
                        Label flb1 = (Label)e.Row.FindControl("lbltigson");
                        Label flb2 = (Label)e.Row.FindControl("lbltidl");
                        // string fdt = txtdate.Text + " " + "12:00 AM";
                        string ldt = txtdate.Text + " " + "11:59 PM";
                        DataTable dt1 = db.totaligston(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt2 = db.totalidealton(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt3 = db.totaligstoff(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        TimeSpan t3 = new TimeSpan(0, Convert.ToInt32(dt1.Rows[0][0].ToString()), 00);
                        string str = t3.ToString();
                        flbm.Text = "Moving(hh:";
                        flb1.Text = "mm):" + str.Substring(0, 5);
                        TimeSpan t4 = new TimeSpan(0, Convert.ToInt32(dt2.Rows[0][0].ToString()), 00);
                        string str1 = t4.ToString();
                        flb1.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Idling(hh:mm):" + str1.Substring(0, 5);
                        //flb2.Text = 
                        TimeSpan t5 = new TimeSpan(0, Convert.ToInt32(dt3.Rows[0][0].ToString()), 00);
                        string str2 = t5.ToString();
                        flb2.Text = "Stoped(hh:mm):" + str2.Substring(0, 5);

                        Label ff = (Label)e.Row.FindControl("lblffuel");
                        Label ff1 = (Label)e.Row.FindControl("lblfuel");
                        double fs = fuelstatus();
                        double Efs = endfuelstatus();

                        ff.Text = "Total Distance :";
                        ff1.Text = tkmh.ToString() + " Km";

                        int index = gvreport.Rows.Count;
                        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                        TableCell cell = new TableCell();

                        cell.Text = "Opening Fuel (ltr) :" + fs.ToString();
                        row.Cells.Add(cell);
                        TableCell cell1 = new TableCell();
                        cell1.Text = "Closing Fuel (ltr) :" + Efs.ToString();
                        row.Cells.Add(cell1);
                        double fuelfill12 = fuelfill();
                        TableCell cell2 = new TableCell();
                        cell2.Text = "Fuel fill (ltr) :" + fuelfill12.ToString();
                        row.Cells.Add(cell2);

                        double fuelconsume = 0;
                        if (fuelfill12 != 0)
                        {
                            fuelconsume = (fs + fuelfill12) - Efs;
                        }
                        else if (fs > Efs)
                        {
                            fuelconsume = fs - Efs;
                        }
                        else
                        {
                            fuelconsume = 0;
                        }
                        TableCell cell3 = new TableCell();
                        cell3.Text = "Fuel Consume (ltr) :" + fuelconsume.ToString();
                        row.Cells.Add(cell3);
                        TableCell cell4 = new TableCell();
                        cell4.Text = "";
                        row.Cells.Add(cell4);
                        TableCell cell5 = new TableCell();
                        cell5.Text = "";
                        row.Cells.Add(cell5);
                        TableCell cell6 = new TableCell();
                        cell6.Text = "";
                        row.Cells.Add(cell6);
                        gvreport.Controls[0].Controls.Add(row);
                    }
                }
                else
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lb = (Label)e.Row.FindControl("lblstop");
                        Label lbkm = (Label)e.Row.FindControl("lblkm");
                        tkmh += Convert.ToDouble(lbkm.Text);

                        if (e.Row.RowIndex == 0)
                        {
                            Label lbon = (Label)e.Row.FindControl("lblon");
                            firsttime = txtdate.Text + " " + lbon.Text;
                        }
                        Label lboff = (Label)e.Row.FindControl("lbloff");
                        lasttime = txtdate.Text + " " + lboff.Text;
                    }
                    else if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        Label flbm = (Label)e.Row.FindControl("lblmov");
                        Label flb1 = (Label)e.Row.FindControl("lbltigson");
                        Label flb2 = (Label)e.Row.FindControl("lbltidl");
                        //string fdt = txtdate.Text + " " + "12:00 AM";
                        string ldt = txtdate.Text + " " + "11:59 PM";
                        DataTable dt1 = db.totaligstonold(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt2 = db.totalidealtonold(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt3 = db.totaligstoffold(fdt, ldt, ddlMapTOVehicle.SelectedValue.ToString());
                        TimeSpan t3 = new TimeSpan(0, Convert.ToInt32(dt1.Rows[0][0].ToString()), 00);
                        string str = t3.ToString();
                        flbm.Text = "Moving(hh:";
                        flb1.Text = "mm):" + str.Substring(0, 5);
                        TimeSpan t4 = new TimeSpan(0, Convert.ToInt32(dt2.Rows[0][0].ToString()), 00);
                        string str1 = t4.ToString();
                        flb1.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Idling(hh:mm):" + str1.Substring(0, 5);
                        //flb2.Text = 
                        TimeSpan t5 = new TimeSpan(0, Convert.ToInt32(dt3.Rows[0][0].ToString()), 00);
                        string str2 = t5.ToString();
                        flb2.Text = "Stoped(hh:mm):" + str2.Substring(0, 5);
                        Label ff = (Label)e.Row.FindControl("lblffuel");
                        Label ff1 = (Label)e.Row.FindControl("lblfuel");
                        double fs = fuelstatus();
                        double Efs = endfuelstatus();
                        ff.Text = "Total Distance :";
                        ff1.Text = tkmh.ToString() + " Km";

                        int index = gvreport.Rows.Count;
                        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                        TableCell cell = new TableCell();
                        cell.Text = "Opening Fuel (ltr) :" + fs.ToString();
                        row.Cells.Add(cell);
                        TableCell cell1 = new TableCell();
                        cell1.Text = "Closing Fuel (ltr) :" + Efs.ToString();
                        row.Cells.Add(cell1);
                        double fuelfill12 = fuelfill();
                        TableCell cell2 = new TableCell();
                        cell2.Text = "Fuel fill (ltr) :" + fuelfill12.ToString();
                        row.Cells.Add(cell2);

                        double fuelconsume = 0;
                        if (fuelfill12 != 0)
                        {
                            fuelconsume = (fs + fuelfill12) - Efs;
                        }
                        else if (fs > Efs)
                        {
                            fuelconsume = fs - Efs;

                        }
                        else
                        {
                            fuelconsume = 0;
                        }
                        TableCell cell3 = new TableCell();
                        cell3.Text = "Fuel Consume (ltr) :" + fuelconsume.ToString();
                        row.Cells.Add(cell3);
                        TableCell cell4 = new TableCell();
                        cell4.Text = "";
                        row.Cells.Add(cell4);
                        TableCell cell5 = new TableCell();
                        cell5.Text = "";
                        row.Cells.Add(cell5);
                        TableCell cell6 = new TableCell();
                        cell6.Text = "";
                        row.Cells.Add(cell6);
                        gvreport.Controls[0].Controls.Add(row);
                    }
                }
            }
            else if (rbtType.SelectedValue == "2")
            {
                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                string fdate = txtfdate.Text + " " + fromtime + " " + AMPM;
                string tdate = txttdate.Text + " " + totime + " " + DT;
                string fdt = txtfdate.Text + " " + "12:00 AM";
                DateTime dhdh = Convert.ToDateTime(fdt);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lb = (Label)e.Row.FindControl("lblstop");
                        Label lbkm = (Label)e.Row.FindControl("lblkm");
                        tkmh += Convert.ToDouble(lbkm.Text);
                        if (e.Row.RowIndex == 0)
                        {
                            Label lbon = (Label)e.Row.FindControl("lblon");
                            firsttime = lbon.Text;
                        }
                        Label lboff = (Label)e.Row.FindControl("lbloff");
                        lasttime = lboff.Text;
                    }
                    else if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        Label flbm = (Label)e.Row.FindControl("lblmov");
                        Label flb1 = (Label)e.Row.FindControl("lbltigson");
                        Label flb2 = (Label)e.Row.FindControl("lbltidl");

                        string ldt = txttdate.Text + " " + "11:59 PM";
                        DataTable dt1 = db.totaligston(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt2 = db.totalidealton(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt3 = db.totaligstoff(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());

                        TimeSpan span = TimeSpan.FromMinutes(Convert.ToInt32(dt1.Rows[0][0].ToString()));
                        string str = span.ToString(@"dd\:hh\:mm");
                        flbm.Text = "Moving(dd:hh:";
                        flb1.Text = "mm):" + str;//.Substring(0, 7);
                        TimeSpan span1 = TimeSpan.FromMinutes(Convert.ToInt32(dt2.Rows[0][0].ToString()));

                        string str1 = span1.ToString(@"dd\:hh\:mm");//t4.ToString();//.ToString();
                        flb1.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Idling(dd:hh:mm):" + str1;//.Substring(0, 7);                      

                        TimeSpan span2 = TimeSpan.FromMinutes(Convert.ToInt32(dt3.Rows[0][0].ToString()));
                        string str2 = span2.ToString(@"dd\:hh\:mm");
                        flb2.Text = "Stoped(dd:hh:mm):" + str2;//.Substring(0,7);

                        Label ff = (Label)e.Row.FindControl("lblffuel");
                        Label ff1 = (Label)e.Row.FindControl("lblfuel");
                        double fs = fuelstatus();
                        double Efs = endfuelstatus();
                        ff.Text = "Total Distance :";
                        ff1.Text = tkmh.ToString() + " Km";
                        int index = gvreport.Rows.Count;
                        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                        TableCell cell = new TableCell();

                        cell.Text = "Opening Fuel (ltr) :" + fs.ToString();
                        row.Cells.Add(cell);
                        TableCell cell1 = new TableCell();
                        cell1.Text = "Closing Fuel (ltr) :" + Efs.ToString();
                        row.Cells.Add(cell1);
                        double fuelfill12 = fuelfill();
                        TableCell cell2 = new TableCell();
                        cell2.Text = "Fuel fill (ltr) :" + fuelfill12.ToString();
                        row.Cells.Add(cell2);

                        double fuelconsume = 0;
                        if (fuelfill12 != 0)
                        {
                            fuelconsume = (fs + fuelfill12) - Efs;
                        }
                        else if (fs > Efs)
                        {
                            fuelconsume = fs - Efs;

                        }
                        else
                        {
                            fuelconsume = 0;
                        }
                        TableCell cell3 = new TableCell();
                        cell3.Text = "Fuel Consume (ltr) :" + fuelconsume.ToString();
                        row.Cells.Add(cell3);
                        TableCell cell4 = new TableCell();
                        cell4.Text = "";
                        row.Cells.Add(cell4);
                        TableCell cell5 = new TableCell();
                        cell5.Text = "";
                        row.Cells.Add(cell5);
                        TableCell cell6 = new TableCell();
                        cell6.Text = "";
                        row.Cells.Add(cell6);
                        gvreport.Controls[0].Controls.Add(row);
                    }
                }
                else
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lb = (Label)e.Row.FindControl("lblstop");
                        Label lbkm = (Label)e.Row.FindControl("lblkm");
                        tkmh += Convert.ToDouble(lbkm.Text);
                        if (e.Row.RowIndex == 0)
                        {
                            Label lbon = (Label)e.Row.FindControl("lblon");
                            firsttime = lbon.Text;
                        }
                        Label lboff = (Label)e.Row.FindControl("lbloff");
                        lasttime = lboff.Text;
                    }
                    else if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        Label flbm = (Label)e.Row.FindControl("lblmov");
                        Label flb1 = (Label)e.Row.FindControl("lbltigson");
                        Label flb2 = (Label)e.Row.FindControl("lbltidl");
                        //string fdt = txtdate.Text + " " + "12:00 AM";
                        string ldt = txttdate.Text + " " + "11:59 PM";
                        DataTable dt1 = db.totaligstonold(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt2 = db.totalidealtonold(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                        DataTable dt3 = db.totaligstoffold(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                        TimeSpan span = TimeSpan.FromMinutes(Convert.ToInt32(dt1.Rows[0][0].ToString()));
                        string str = span.ToString(@"dd\:hh\:mm");
                        flbm.Text = "Moving(dd:hh:";
                        flb1.Text = "mm):" + str;//.Substring(0, 7);
                        TimeSpan span1 = TimeSpan.FromMinutes(Convert.ToInt32(dt2.Rows[0][0].ToString()));

                        string str1 = span1.ToString(@"dd\:hh\:mm");//t4.ToString();//.ToString();
                        flb1.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Idling(dd:hh:mm):" + str1;//.Substring(0, 7);                      

                        TimeSpan span2 = TimeSpan.FromMinutes(Convert.ToInt32(dt3.Rows[0][0].ToString()));
                        string str2 = span2.ToString(@"dd\:hh\:mm");
                        flb2.Text = "Stoped(dd:hh:mm):" + str2;//.Substring(0,7);                                       
                        Label ff = (Label)e.Row.FindControl("lblffuel");
                        Label ff1 = (Label)e.Row.FindControl("lblfuel");
                        double fs = fuelstatus();
                        double Efs = endfuelstatus();

                        ff.Text = "Total Distance :";
                        ff1.Text = tkmh.ToString() + " Km";

                        int index = gvreport.Rows.Count;
                        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                        TableCell cell = new TableCell();

                        cell.Text = "Opening Fuel :(ltr) :" + fs.ToString();
                        row.Cells.Add(cell);
                        TableCell cell1 = new TableCell();
                        cell1.Text = "Closing Fuel (ltr) :" + Efs.ToString();
                        row.Cells.Add(cell1);
                        double fuelfill12 = fuelfill();
                        TableCell cell2 = new TableCell();
                        cell2.Text = "Fuel fill (ltr) :" + fuelfill12.ToString();
                        row.Cells.Add(cell2);
                        double fuelconsume = 0;
                        if (fuelfill12 != 0)
                        {
                            fuelconsume = (fs + fuelfill12) - Efs;
                        }
                        else if (fs > Efs)
                        {
                            fuelconsume = fs - Efs;

                        }
                        else
                        {
                            fuelconsume = 0;
                        }
                        TableCell cell3 = new TableCell();
                        cell3.Text = "Fuel Consume (ltr) :" + fuelconsume.ToString();
                        row.Cells.Add(cell3);
                        TableCell cell4 = new TableCell();
                        cell4.Text = "";
                        row.Cells.Add(cell4);
                        TableCell cell5 = new TableCell();
                        cell5.Text = "";
                        row.Cells.Add(cell5);
                        TableCell cell6 = new TableCell();
                        cell6.Text = "";
                        row.Cells.Add(cell6);
                        gvreport.Controls[0].Controls.Add(row);
                    }

                }
            }
        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            DateTime dt = DateTime.Now;
            string str = dt.ToString("dd-MM-yyyy");
            //string str1 = str.Replace("-", "");
            //str1 = str1.Replace(" ", "");
            //str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VAR_" + vstr.Replace(" ", "") + "_" + str + ".xls";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gvreport);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);

            tb.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        public double fuelstatus()
        {
            double fulestat = 0;
            if (rbtType.SelectedValue == "1")
            {
                string fdate = Convert.ToDateTime(txtdate.Text).ToString();
                string tdate = Convert.ToDateTime(txtdate.Text + " " + "11:59 PM").ToString();

                DateTime dhdh = Convert.ToDateTime(fdate);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    // DataTable dt2 = db.search(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    DataTable dt2 = db.startfuelstaus(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                    //for (int m = dt2.Rows.Count - 1; m >= 0; m--)
                    //{
                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                //break;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                // break;
                            }
                        }

                        else
                        {
                            fulestat = 0.0;
                        }

                    }
                    // }
                }
                else
                {
                    //DataTable dt2 = db.searchold(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    //for (int m = dt2.Rows.Count - 1; m >= 0; m--)
                    //{
                    DataTable dt2 = db.startfuelstausold(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                    // if (Convert.ToInt32(dt2.Rows[m][3].ToString()) <= 3 && dt2.Rows[m][4].ToString() == "1")
                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                // break;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                //  break;
                            }
                        }
                        else
                        {
                            fulestat = 0.0;
                        }
                    }
                    // }

                }
            }
            else if (rbtType.SelectedValue == "2")
            {
                string fdate = Convert.ToDateTime(txtfdate.Text).ToString();
                string tdate = Convert.ToDateTime(txttdate.Text + " " + "11:59 PM").ToString();

                DateTime dhdh = Convert.ToDateTime(fdate);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    //DataTable dt2 = db.search(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    //for (int m = dt2.Rows.Count - 1; m >= 0; m--)
                    //{
                    //    if (Convert.ToInt32(dt2.Rows[m][3].ToString()) <= 3 && dt2.Rows[m][4].ToString() == "1")
                    //    {
                    DataTable dt2 = db.startfuelstaus(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                    // if (Convert.ToInt32(dt2.Rows[m][3].ToString()) <= 3 && dt2.Rows[m][4].ToString() == "1")
                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                //break;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                // break;
                            }
                        }
                        else
                        {
                            fulestat = 0.0;
                        }

                    }
                    // }
                }
                else
                {
                    //DataTable dt2 = db.searchold(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    //for (int m = dt2.Rows.Count - 1; m >= 0; m--)
                    //{
                    //    if (Convert.ToInt32(dt2.Rows[m][3].ToString()) <= 3 && dt2.Rows[m][4].ToString() == "1")
                    //    {
                    DataTable dt2 = db.startfuelstausold(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);
                    // if (Convert.ToInt32(dt2.Rows[m][3].ToString()) <= 3 && dt2.Rows[m][4].ToString() == "1")
                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {
                            //string vid = ddlMapTOVehicle.SelectedValue.ToString();
                            //DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                // break;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                // break;
                            }

                        }
                        else
                        {
                            fulestat = 0.0;
                        }
                    }

                }

            }

            return fulestat;
        }

        protected void rbtType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtType.SelectedValue == "1")
            {
                TdSearch.Visible = true;
                TdSearch1.Visible = false;
            }
            else if
               (rbtType.SelectedValue == "2")
            {
                TdSearch1.Visible = true;
                TdSearch.Visible = false;
            }
        }
        public double endfuelstatus()
        {
            double fulestat = 0;
            if (rbtType.SelectedValue == "1")
            {
                string fdate = Convert.ToDateTime(txtdate.Text).ToString();
                string tdate = Convert.ToDateTime(txtdate.Text + " " + "11:59 PM").ToString();

                DateTime dhdh = Convert.ToDateTime(fdate);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {

                    DataTable dt2 = db.Endfuelstaus(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);

                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;
                            }
                        }

                        else
                        {
                            fulestat = 0.0;
                        }

                    }

                }
                else
                {

                    DataTable dt2 = db.startfuelstausold(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);

                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;

                            }
                        }
                        else
                        {
                            fulestat = 0.0;
                        }
                    }


                }
            }
            else if (rbtType.SelectedValue == "2")
            {
                string fdate = Convert.ToDateTime(txtfdate.Text).ToString();
                string tdate = Convert.ToDateTime(txttdate.Text + " " + "11:59 PM").ToString();

                DateTime dhdh = Convert.ToDateTime(fdate);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {

                    DataTable dt2 = db.Endfuelstaus(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);

                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;

                            }
                        }
                        else
                        {
                            fulestat = 0.0;
                        }

                    }

                }
                else
                {

                    DataTable dt2 = db.Endfuelstausold(ddlMapTOVehicle.SelectedValue.ToString(), fdate, tdate);

                    if (dt2.Rows.Count > 0)
                    {
                        string vid = ddlMapTOVehicle.SelectedValue.ToString();
                        DataTable dtf = db.GetVehiclesfuel(vid, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vid);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                fulestat = newY;
                            }
                            else if (newY > tankcapacity)
                            {

                                fulestat = tankcapacity;
                            }
                            else
                            {

                                fulestat = 0.0;
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fulestat = b;

                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fulestat = f;

                            }

                        }
                        else
                        {
                            fulestat = 0.0;
                        }
                    }

                }

            }

            return fulestat;
        }

        public double fuelfill()
        {
            double fill = 0.0;
            if (rbtType.SelectedValue == "1")
            {
                string fdate = Convert.ToDateTime(txtdate.Text).ToString();
                string tdate = Convert.ToDateTime(txtdate.Text + " " + "11:59 PM").ToString();
                string vno = ddlMapTOVehicle.SelectedValue.ToString();
                DateTime dhdh = Convert.ToDateTime(fdate);
                double fliter = 0;
                DataTable Actualfuelfill = db.Fuel_fill2(vno, dhdh.ToShortDateString());
                if (Actualfuelfill.Rows.Count != 0)
                {
                    for (int k = 0; k < Actualfuelfill.Rows.Count; k++)
                    {
                        fliter += Convert.ToInt32(Actualfuelfill.Rows[k][2]);
                    }
                    fill = fliter;
                }
            }
            else if (rbtType.SelectedValue == "2")
            {
                string fdate = Convert.ToDateTime(txtfdate.Text).ToString();
                string tdate = Convert.ToDateTime(txttdate.Text + " " + "11:59 PM").ToString();
                string vno = ddlMapTOVehicle.SelectedValue.ToString();
                DateTime fdate12 = Convert.ToDateTime(fdate);
                DateTime tdate12 = Convert.ToDateTime(tdate);
                double fliter = 0;
                for (int i = 0; i < 6; i++)
                {
                    if (fdate12 <= tdate12)
                    {

                        DataTable Actualfuelfill = db.Fuel_fill2(vno, fdate12.ToShortDateString());
                        if (Actualfuelfill.Rows.Count != 0)
                        {
                            for (int k = 0; k < Actualfuelfill.Rows.Count; k++)
                            {
                                fliter += Convert.ToInt32(Actualfuelfill.Rows[k][2]);
                            }

                        }
                    }
                    fdate12 = fdate12.AddDays(1);
                }
                fill = fliter;
            }

            return fill;
        }
    }
}
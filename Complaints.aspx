﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Complaints.aspx.cs" Inherits="Tracking.Complaints" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 1250px; height: auto; margin: 0 auto; padding: 0; padding: 0; color: Black;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" />
        <div style="height: 30px; text-align: center; color: Blue; font-style: italic;">
            <h2>
                <a>Complaint History</a>
            </h2>
        </div>
       
        <div style="height: 30px; text-align: center">
            <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Italic="true" Font-Size="Large"></asp:Label>
        </div>
        <div>
            <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                ShowFooter="true" Width="1300px" RowStyle-ForeColor="Black">
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px" />
                <FooterStyle BackColor="#61A6F8" ForeColor="Black" Height="20px" Font-Size="14px"
                    BorderWidth="0" />
                <RowStyle ForeColor="Black"></RowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="Complaint ID" HeaderStyle-Width="50px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblcid" runat="server" Text='<%# Bind("cid") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Complaint Date" HeaderStyle-Width="80px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblcdate" runat="server" Text='<%# Bind("cdate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Vehicle NO" HeaderStyle-Width="70px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblvno" runat="server" Text='<%# Bind("vno") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Type" HeaderStyle-Width="40px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblctype" runat="server" Text='<%# Bind("ctype") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Nature of Complaint" HeaderStyle-Width="200px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblcmag" runat="server" Text='<%# Bind("ctypemsg") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Avalilable Date" HeaderStyle-Width="80px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblavaildate" runat="server" Text='<%# Bind("availbledate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Avalilable Location" HeaderStyle-Width="200px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblavaollocaion" runat="server" Text='<%# Bind("availblelocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Complaint Status" HeaderStyle-Width="60px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblcstatus" runat="server" Text='<%# Bind("cstatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Complaint closed Date" HeaderStyle-Width="60px" FooterStyle-BorderColor="#61A6F8"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="13px">
                        <ItemTemplate>
                            <asp:Label ID="lblccloseddate" runat="server" Text='<%# Bind("ccloseddate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace Tracking
{
    public partial class FuelFill : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
             
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null)
                {
 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserID"] != null)
                    {
                        if (Session["UserRole"].ToString() == "3")
                        {
                            ddlVeh.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                            ddlVeh.DataBind();
                            //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                            //ddlVeh.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                            //ddlVeh.DataBind();
                        }
                        else
                        {
                            ddlVeh.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                            ddlVeh.DataBind();
                        }
                    }
                }
            }            
        }

        protected void btnFuelCapEnter_Click(object sender, EventArgs e)
        {
            //-----Important Code: Do not DELETE-----
            string RegNUM = ddlVeh.SelectedValue.ToString();//Session["RegNo"].ToString();

 
 
            DBClass db = new DBClass();
 
 
            DateTime FDT = Convert.ToDateTime(txtFuelCapDT.Text);
            string RefChaNo = txtRefChaNo.Text;
            string FC = txtFuelCapacity.Text;
            string StNa = txtNamOfFilSt.Text;
            //db.FuelF(RegNUM, FDT, FC, RefChaNo, StNa);
            ////txtVehFuRec.Visible = false;
            ////btnGetFuRec.Visible = false;
            ////lblSelDT.Visible = false;
            //txtFuelCapacity.Text = "";
            //txtFuelCapDT.Text = "";
            //txtNamOfFilSt.Text = "";
            //txtRefChaNo.Text = "";
            //---------------------------------------

            if (FDT <= DateTime.Now)
            {
                db.FuelF(RegNUM, FDT, FC, RefChaNo, StNa);
                //txtVehFuRec.Visible = false;
                //btnGetFuRec.Visible = false;
                //lblSelDT.Visible = false;
                txtFuelCapacity.Text = "";
                txtFuelCapDT.Text = "";
                txtNamOfFilSt.Text = "";
                txtRefChaNo.Text = "";
                lblErrorMsg.Visible = false;
            }
            else if (FDT > DateTime.Now)
            {
                lblErrorMsg.Visible = true;
            }

        }
        protected void CheckAppts(object sender, DayRenderEventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            string RegNUM = ddlVeh.SelectedValue.ToString();//Session["RegNo"].ToString();
            //DateTime FDT = Convert.ToDateTime(txtFuelCapDT.Text);
            //string FC = txtFuelCapacity.Text;
            DataSet FuF = db.SelectFF(RegNUM);//, FDT, FC);
            //foreach (DataRow drFuF in FuF.Tables[0].Rows)
            //{
            //    //if ((e.Day.Date >= date1) && (e.Day.Date <= date2))
            //    //{

            //    //}
            //    string hyl = "<br />" + drFuF["Fuel_DT"].ToString() + "<br />";
            //    e.Cell.Controls.Add(new System.Web.UI.LiteralControl(hyl));
            //    e.Cell.Controls.Add
            //}
            //If the month is CurrentMonth
            if (!e.Day.IsOtherMonth)
            {
                foreach (DataRow dr in FuF.Tables[0].Rows)
                {
                    if ((dr["Fuel_DT"].ToString() != DBNull.Value.ToString()))
                    {
                        DateTime dtEvent = (DateTime)dr["Fuel_DT"];
                        if (dtEvent.Equals(e.Day.Date))
                        {
                            //string TOTFuel = FuF.Tables("Fuel_Entry").Compute("SUM(Fuel_Capacity)", string.Empty);
                            DataTable FuFTo = db.SelectFFTOT(RegNUM, e.Day.Date);
                            string FT = FuFTo.Rows[0][0].ToString();
                            string hyl = "<br /> Fuel consumption per day - <br />" + FT + " liters <br />";
                            e.Cell.Controls.Add(new System.Web.UI.LiteralControl(hyl));
                            if (e.Cell != null)
                            {
                                e.Cell.Text = ""; // clear the existing and write the cell afresh
                                string FT2 = e.Day.DayNumberText;
                                dtEvent.Equals(e.Day.Date);
                                DataTable FuFTo1 = db.SelectFFTOT(RegNUM, e.Day.Date);
                                string FT1 = FuFTo.Rows[0][0].ToString();
                                //string hyl1 = "<br/>-------------";
                                string hyl1 = FT2 + " "+"---->"+" "+ FT + " liters";//" <br /> Number of liters consumed per day - <br />" + FT + " liters <br />";
                                e.Cell.Controls.Add(new System.Web.UI.LiteralControl(hyl1));

                                DateTime dat = Convert.ToDateTime(dtEvent.ToString());
                                DateTime firstDate = dat.Subtract(TimeSpan.FromDays(dat.Day - 1));
                                DateTime firstDate_ofNextMonth = firstDate.AddMonths(1);
                                DateTime lastDate = firstDate_ofNextMonth.Subtract(TimeSpan.FromDays(1));

                                DataTable FperM = db.getFperM(firstDate, lastDate, RegNUM);
                                lblLine.Visible = true;
                                lblFperM.Visible = true;
                                lblFperM.Text = FperM.Rows[0][0].ToString() + " &nbsp; liters";

                            }
                        }
                    }
                }
            }

            //If the month is not CurrentMonth then hide the Dates
            else
            {
                e.Cell.Text = "";
            }
            //txtVehFuRec.Visible = true;
            //btnGetFuRec.Visible = true;
            //lblSelDT.Visible = true;
        }

        protected void bntOk_Click(object sender, EventArgs e)
        {
           
            string RegNUM = ddlVeh.SelectedValue.ToString();
            DateTime FDT = Convert.ToDateTime(txtFDate.Text);
            string date = FDT.ToString("yyyy-MM-dd HH:mm:ss");
            db.delete_fuelentry(RegNUM, date);
            txtFDate.Text = "";
            TdSearch.Visible = false;
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Deleted Sucessfully...');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }
        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            TdSearch.Visible = true;
        }
        //protected void GetRefNo(object sender, EventArgs e)
        //{
        //    string RegNUM = ddlVeh.SelectedValue.ToString();//Session["RegNo"].ToString();
        //    DateTime FuRecDT = Convert.ToDateTime(txtFuelCapDT.Text);
        //    DBClass db = new DBClass();
        //    DataTable FuTab = db.GetFuRec(RegNUM, FuRecDT);
        //    dgGetRec.DataSource = FuTab;
        //    dgGetRec.DataBind();
        //}
        //protected void BtnSavPrint_Click(object sender, EventArgs e)
        //{
        //    Response.Clear();
        //    Response.Buffer = true;

        //    Response.AddHeader("content-disposition", "attachment;filename=FuelEntryReport.xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);

        //    //PrepareForExport(dgSave);
        //    //PrepareForExport(dgSearch);

        //    Table tb = new Table();
        //    TableRow tr1 = new TableRow();
        //    TableCell cell1 = new TableCell();
        //    cell1.Controls.Add(dgGetRec);
        //    tr1.Cells.Add(cell1);
        //    TableCell cell3 = new TableCell();
        //    //cell3.Controls.Add(dgGetRec);
        //    TableCell cell2 = new TableCell();
        //    cell2.Text = "&nbsp;";
        //    //if (rbPreference.SelectedValue == "2")
        //    //{
        //    //    tr1.Cells.Add(cell2);
        //    //    tr1.Cells.Add(cell3);
        //    //    tb.Rows.Add(tr1);
        //    //}
        //    //else
        //    //{
        //    TableRow tr2 = new TableRow();
        //    tr2.Cells.Add(cell2);
        //    TableRow tr3 = new TableRow();
        //    tr3.Cells.Add(cell3);
        //    tb.Rows.Add(tr1);
        //    tb.Rows.Add(tr2);
        //    tb.Rows.Add(tr3);
        //    //}
        //    tb.RenderControl(hw);

        //    //style to format numbers to string
        //    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
        //    Response.Write(style);
        //    Response.Output.Write(sw.ToString());
        //    Response.Flush();
        //    Response.End();

        //    //Response.Clear();
        //    //Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
        //    //Response.Charset = "";
        //    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //Response.ContentType = "application/vnd.xls";

        //    //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //    //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        //    //dgSearch.RenderControl(htmlWrite);
        //    //Response.Write(stringWrite.ToString());
        //    //Response.End();
        //}
    }
}

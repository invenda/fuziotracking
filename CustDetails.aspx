﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustDetails.aspx.cs" Inherits="Tracking.CustDetails"
    MasterPageFile="~/ESLMaster.Master" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function ConfirmIt() {
            alert('Registration Successfully');
        }               
    </script>

    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0; color: Black;">
        <div style="color: Blue; text-align: center;">
            <h1>
                <a>Customer Registration Details</a>
            </h1>
        </div>
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <table width="100%">
                <tr>
                    <td colspan="4">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                            ShowSummary="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Customer Type:
                    </td>
                    <td>
                        <asp:RadioButton ID="rbtnIndCust" runat="server" Text="Individual Customer" GroupName="Group1">
                        </asp:RadioButton>
                        <asp:RadioButton ID="rbtnCompany" runat="server" Text="Company" GroupName="Group1">
                        </asp:RadioButton>
                    </td>
                    <td>
                        Name of the Customer/Company:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustName" runat="server" Height="15px" Width="192px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCustName"
                            Text="*" ErrorMessage="Enter Customer Name"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Billing Address Line 1:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustAddress1" runat="server" Height="15px" Width="194px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCustAddress1"
                            Text="*" ErrorMessage="Enter Customer Billing Address Line 1"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Billing Address Line 2:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustAddress2" runat="server" Height="15px" Width="194px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCustAddress2"
                            Text="*" ErrorMessage="Enter Customer Billing Address Line 2"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        City:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDistrict" runat="server" Height="15px" Width="192px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDistrict"
                            Text="*" ErrorMessage="Enter District"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        State:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server" Height="23px" Width="194px" DataValueField="States"
                            DataTextField="States">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlState"
                            Text="*" ErrorMessage="Select State" Operator="NotEqual" ValueToCompare="Select One"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contact Number: (Area Code)
                    </td>
                    <td>
                        <asp:TextBox ID="txtAreaCode" runat="server" Height="15px" Width="50px"></asp:TextBox>
                        <asp:TextBox ID="txtCustConNum" runat="server" Height="15px" Width="142px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCustConNum"
                            Text="*" ErrorMessage="Enter Customer Contact Number"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Default Mobile Number for SMS Alert:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustMobNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCustMobNum"
                            Text="*" ErrorMessage="Enter Customer Mobile Number"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date of Registration:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDOR" runat="server" Height="15px" Width="192px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDOR">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDOR"
                            Text="*" ErrorMessage="Enter Date Of Registration"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Email ID:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Height="15px" Width="192px"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmail" Text="*"
                            ErrorMessage="Enter Email ID"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                     Alternate Email ID:
                        </td>
                    <td>
                     <asp:TextBox ID="txtaemail" runat="server" Height="15px" Width="192px"></asp:TextBox><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtaemail" Text="*"
                            ErrorMessage="Enter Email ID"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnRegister" runat="server" Text="Submit" OnClick="btnRegister_Click"
                            OnClientClick="if(Page_ClientValidate()) ConfirmIt()" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

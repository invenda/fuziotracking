﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Createroutebasis.aspx.cs"
    Inherits="Tracking.Createroutebasis" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="ContentPlaceHolder1" runat="server">
    <br />
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 1132px; height: auto; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
        <div style="width: 1039px">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" Style="margin-bottom: 20px" />
        </div>
        <div id="sublogo" style="text-align: center; color: Blue;">
            <h3>
                <a>Create Geo Route Basis</a>
            </h3>
        </div>
        <div style="height: 200px; color: Blue; width: 1250px;">
        
          
            <div style ="text-align :center ; color: Red;" >
                <asp:Label ID="lblalert" runat="server" Font-Bold ="true" ></asp:Label>
            </div>
              <table>
                <table border="1" style="width: 1100px; height: 148px;">
                    <tr>
                        <td style="width: 130px;">
                            Select Vehicle :
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlMapTOVehicle"
                                ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                                ValueToCompare="--">*</asp:CompareValidator>
                        </td>
                        <td style="width: 170px;">
                            <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                DataTextField="Vehicalnumber" Height="28px" Width="135px" Style="margin-left: 1px">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 127px">
                            Select Route Type
                            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlrtype"
                                ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                                ValueToCompare="--">*</asp:CompareValidator>
                        </td>
                        <td style="width: 157px">
                            <asp:DropDownList ID="ddlrtype" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlrtype_SelectedIndexChanged">
                               
                                <asp:ListItem Text="ROUTE BASIS" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 170px">
                            Route No:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtrno"
                                ValidationGroup="Group1" ErrorMessage="Enter Route No">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="text-align: right; width: 178px;">
                            <%-- <asp:Label ID="Label1" Text="Select Vehicle :" runat="server" ></asp:Label>--%>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtid" runat="server" Width="40px" Text ="ROU" ReadOnly ="true"  ></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtrno" runat="server" Width="95px" ReadOnly ="true" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width: 130px; height: 25px;">
                            Select Start Date:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtstartdate"
                                ValidationGroup="Group1" ErrorMessage="Select Start date">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 170px; height: 25px;">
                            <asp:TextBox ID="txtstartdate" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="txtstartdate">
                            </cc1:CalendarExtender>
                        </td>
                        <td style="width: 127px; height: 25px;">
                            Select End Date:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtenddate"
                                ValidationGroup="Group1" ErrorMessage="Select end date">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 25px; width: 157px;">
                            <asp:TextBox ID="txtenddate" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtenddate">
                            </cc1:CalendarExtender>
                        </td>
                        <td style="width: 170px; height: 25px;">
                         <asp:DropDownList ID="ddlpointstype" runat="server"
                                Height="26px" Width="172px" ondatabound="ddlpointstype_DataBound" >                               
                                <asp:ListItem Text=" Vehicle Moving points" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Vehicle Stoped points" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 178px; height: 25px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; height: 28px;">
                            Select From Time:
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttname"
                                    ValidationGroup="Group1" ErrorMessage="Select from time">*</asp:RequiredFieldValidator>--%>
                        </td>
                        <td style="width: 170px; height: 28px;">
                            <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="59px" DataValueField="Hours"
                                DataTextField="Hours">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                            <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="70px" DataValueField="Minutes"
                                DataTextField="Minutes">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                        </td>
                        <td style="width: 127px; height: 28px;">
                            <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal" Height="23px"
                                Width="112px">
                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td style="width: 157px; height: 28px;">
                            Select To Time:
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txttname"
                                    ValidationGroup="Group1" ErrorMessage="Select to time">*</asp:RequiredFieldValidator>--%>
                        </td>
                        <td style="width: 170px; height: 28px;">
                            <asp:DropDownList ID="ddlToHOURS" runat="server" Width="66px" DataValueField="Hours"
                                DataTextField="Hours">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                            <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="65px" DataValueField="Minutes"
                                DataTextField="Minutes">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes ">*</asp:CompareValidator>
                        </td>
                        <td style="width: 178px; height: 28px;">
                            <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal" Width="102px">
                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; height: 23px;">
                        </td>
                        <td style="width: 170px; height: 23px;">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td style="width: 127px; text-align: center; height: 23px;">
                            <asp:Button ID="btndisplay" runat="server" Text="Display" ValidationGroup="Group1"
                                Height="23px" OnClick="btndisplay_Click" />
                        </td>
                        <td style="width: 157px; text-align: center; height: 23px;">
                            <asp:Button ID="Btn_clr" runat="server" Text="Clear" Width="103px" Height="25px"
                                OnClick="Btn_clr_Click" />
                        </td>
                        <td style="width: 170px; text-align: center; height: 23px;">
                            <asp:Button ID="Btn_submit" runat="server" Text="Submit" ValidationGroup="Group1"
                                Width="105px" Height="25px" OnClick="Btn_submit_Click" />
                        </td>
                        <td style="width: 178px; height: 23px;">
                        </td>
                    </tr>
                    
                </table>
                </tr>
                <tr id="tdmap" visible="false">
                    <td id="maptd" align="center">
                        <cc1:GMap ID="GMap1" runat="server" Width="95%" Height="600px" Visible="true" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

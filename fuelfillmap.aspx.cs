﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class fuelfillmap : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;
                if (!string.IsNullOrEmpty(Request.QueryString["RegNo"]) && !string.IsNullOrEmpty(Request.QueryString["Date"]))
                {
                    String VehNo = Request.QueryString["RegNo"].ToString();
                    string dt = Request.QueryString["Date"].ToString();
                    DateTime date2 = Convert.ToDateTime(dt, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    //DateTime  dt = Convert .ToDateTime( Request.QueryString["Date"].ToString());
                    binddata(VehNo, date2.ToString ());
                }

            }
        }
        public void binddata(string vno, string data)
        {

            if (Session["UserID"] != null)
            {
                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                DataTable ggg = new DataTable();
                string dateString = data ;
                DateTime date2 = Convert.ToDateTime(dateString);//, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                string date = date2.ToString();
                DateTime dhdh = Convert.ToDateTime(date2);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    ggg = db.fuelfillmap(vno, date);
                }
                else
                {
                    ggg = db.fuelfillmap1old(vno, date);
                }
                GLatLng latlng = new GLatLng(Convert.ToDouble(ggg.Rows[0][0]), Convert.ToDouble(ggg.Rows[0][1]));
                GMap1.setCenter(latlng, 15);

                GIcon icon = new GIcon();
                icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                //icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
                icon.iconSize = new GSize(12, 20);
                // icon.shadowSize = new GSize(22, 20);
                icon.iconAnchor = new GPoint(6, 20);
                //icon.infoWindowAnchor = new GPoint(5, 1);
                GMarkerOptions mOpts = new GMarkerOptions();
                mOpts.clickable = false;
                mOpts.icon = icon;

                GMarker marker = new GMarker(latlng, mOpts);
                GMap1.Add(marker);

            }
        }

    }

}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;

namespace Tracking
{
    public partial class TowedReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        double tkm = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                    ddlMapTOVehicle.DataBind();
                    //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                    //ddlMapTOVehicle.DataBind();
                }
                else
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                    ddlMapTOVehicle.DataBind();
                }
            }
        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false;
            txtdate.Enabled = false;
            btndownload.Visible = true;
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {

            //System.Threading.Thread.Sleep(120000);

            DateTime fdate = Convert.ToDateTime(txtdate.Text + " 12:00 AM");
            DateTime tdate = Convert.ToDateTime(txtdate.Text + " 11:59 PM");

            DateTime dhdh = Convert.ToDateTime(fdate);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                binddata(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
            }
            else
            {
                binddataold(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
            }
            disable();
        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            DateTime dt = Convert.ToDateTime(txtdate.Text);
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VTR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gvreport);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
            tb.RenderControl(hw);
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        public void binddata(DateTime fdate, DateTime tdate, string Vno)
        {

            DataTable dt = db.Select_All_Towedpoints(Vno, fdate.ToString(), tdate.ToString());
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng();
            string key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            DateTime tstime = new DateTime();
            DateTime tetime = new DateTime();
            TimeSpan difftime = new TimeSpan();
            int count = 0, count1 = 0, fno = 0;
            string tsloc = "", teloc = "";
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Tstime", typeof(string));
            dt1.Columns.Add("TSLoc", typeof(string));
            dt1.Columns.Add("TELoc", typeof(string));
            dt1.Columns.Add("Tetime", typeof(string));
            dt1.Columns.Add("duration", typeof(string));
            dt1.Columns.Add("km", typeof(string));
            for (int i = 0, j = 1; i < dt.Rows.Count - 1; i++, j++)
            {
                DateTime ftime = Convert.ToDateTime(dt.Rows[i][3].ToString());
                DateTime stime = Convert.ToDateTime(dt.Rows[j][3].ToString());
                TimeSpan ts = stime - ftime;
                if (ts.Minutes == 1)
                {
                    count = count + 1;
                    count1 = 0;
                    if (count == 1)
                    {
                        fno = i;
                    }
                    if (j == dt.Rows.Count - 1)
                    {
                        count1 = count1 + 1;
                    }
                }
                else
                {
                    count1 = count1 + 1;
                    if (count < 9)
                    {
                        count = 0;

                    }
                }
                if (count >= 9 && count1 != 0)
                {
                    try
                    {
                        DataRow dr = dt1.NewRow();
                        tstime = Convert.ToDateTime(dt.Rows[fno][3].ToString());
                        tetime = Convert.ToDateTime(dt.Rows[i][3].ToString());
                        difftime = tetime - tstime;
                        gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[fno][0]), Convert.ToDouble(dt.Rows[fno][1]));
                        GeoCode geocode = GMap.geoCodeRequest(gLatLng, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        StringBuilder sb = new StringBuilder();
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }
                        tsloc = sb.ToString();
                        gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1]));
                        geocode = GMap.geoCodeRequest(gLatLng, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        sb = new StringBuilder();
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }
                        teloc = sb.ToString();
                        double skm = 0;
                        DataTable dtdis = db.Select_All_Towedpoints(Vno, tstime.ToString(), tetime.ToString());
                        for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                        {
                            skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                        }
                        dr["Tstime"] = tstime.ToShortTimeString();
                        dr["TSLoc"] = tsloc;
                        dr["TELoc"] = teloc;
                        dr["Tetime"] = tetime.ToShortTimeString();
                        dr["duration"] = difftime.ToString();
                        string st1 = string.Format("{0:00.00}", skm);
                        dr["km"] = st1.ToString();
                        dt1.Rows.Add(dr);
                        btndownload.Visible = true;
                        count = 0; count1 = 0; fno = 0;
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                }
            }
            if (dt1.Rows.Count <= 0)
            {
                lblinfo.Text = "No Towed report fot this vehicle";
            }
            gvreport.DataSource = dt1;
            gvreport.DataBind();
        }
        public void binddataold(DateTime fdate, DateTime tdate, string Vno)
        {

            DataTable dt = db.Select_All_Towedpointsold(Vno, fdate.ToString(), tdate.ToString());
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng();
            string key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            DateTime tstime = new DateTime();
            DateTime tetime = new DateTime();
            TimeSpan difftime = new TimeSpan();
            int count = 0, count1 = 0, fno = 0;
            string tsloc = "", teloc = "";
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Tstime", typeof(string));
            dt1.Columns.Add("TSLoc", typeof(string));
            dt1.Columns.Add("TELoc", typeof(string));
            dt1.Columns.Add("Tetime", typeof(string));
            dt1.Columns.Add("duration", typeof(string));
            dt1.Columns.Add("km", typeof(string));
            for (int i = 0, j = 1; i < dt.Rows.Count - 1; i++, j++)
            {
                DateTime ftime = Convert.ToDateTime(dt.Rows[i][3].ToString());
                DateTime stime = Convert.ToDateTime(dt.Rows[j][3].ToString());
                TimeSpan ts = stime - ftime;
                if (ts.Minutes == 1)
                {
                    count = count + 1;
                    count1 = 0;
                    if (count == 1)
                    {
                        fno = i;
                    }
                    if (j == dt.Rows.Count - 1)
                    {
                        count1 = count1 + 1;
                    }
                }
                else
                {
                    count1 = count1 + 1;
                    if (count < 9)
                    {
                        count = 0;

                    }
                }
                if (count >= 9 && count1 != 0)
                {
                    try
                    {
                        DataRow dr = dt1.NewRow();
                        tstime = Convert.ToDateTime(dt.Rows[fno][3].ToString());
                        tetime = Convert.ToDateTime(dt.Rows[i][3].ToString());
                        difftime = tetime - tstime;
                        gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[fno][0]), Convert.ToDouble(dt.Rows[fno][1]));
                        GeoCode geocode = GMap.geoCodeRequest(gLatLng, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        StringBuilder sb = new StringBuilder();
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }
                        tsloc = sb.ToString();
                        gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1]));
                        geocode = GMap.geoCodeRequest(gLatLng, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        sb = new StringBuilder();
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }
                        teloc = sb.ToString();
                        double skm = 0;
                        DataTable dtdis = db.Select_All_Towedpointsold(Vno, tstime.ToString(), tetime.ToString());
                        for (int m = 0, n = 1; m < dtdis.Rows.Count - 1; m++, n++)
                        {
                            skm += CalcDistance(Convert.ToDouble(dtdis.Rows[m][0].ToString()), Convert.ToDouble(dtdis.Rows[m][1].ToString()), Convert.ToDouble(dtdis.Rows[n][0].ToString()), Convert.ToDouble(dtdis.Rows[n][1].ToString()));
                        }
                        dr["Tstime"] = tstime.ToShortTimeString();
                        dr["TSLoc"] = tsloc;
                        dr["TELoc"] = teloc;
                        dr["Tetime"] = tetime.ToShortTimeString();
                        dr["duration"] = difftime.ToString();
                        string st1 = string.Format("{0:00.00}", skm);
                        dr["km"] = st1.ToString();
                        dt1.Rows.Add(dr);
                        btndownload.Visible = true;
                        count = 0; count1 = 0; fno = 0;
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                }
            }
             if (dt1.Rows.Count <= 0)
            {
                lblinfo.Text = "No Towed report fot this vehicle";
            }
            gvreport.DataSource = dt1;
            gvreport.DataBind();
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.ColumnSpan = 2;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.ColumnSpan = 4;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                DateTime hdt = Convert.ToDateTime(txtdate.Text);
                HeaderCell1.Text = "Report Date  :" + hdt.ToString("dd-MM-yyyy");
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.Font.Bold = true;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                TableCell HeaderCell3 = new TableCell();
                HeaderCell3.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
                HeaderCell3.ColumnSpan = 1;
                HeaderCell3.Font.Bold = true;
                HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell3);
                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                HeaderCell4.ColumnSpan = 3;
                HeaderCell4.Font.Bold = true;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);
            }
        }
        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                tkm = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lbkm = (Label)e.Row.FindControl("lblkm");
                tkm += Convert.ToDouble(lbkm.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbtkm = (Label)e.Row.FindControl("lbltkm");
                lbtkm.Text = tkm.ToString();
            }
        }

    }
}

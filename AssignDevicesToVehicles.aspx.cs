﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Tracking
{
    public partial class AssignDevicesToVehicles : System.Web.UI.Page
    {
        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
                    //TrackingMaster tm = (TrackingMaster)Page.Master;
                    //tm.MenuVisible = true;

 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserID"] != null)
                    {
                        ddlDevices.DataSource = db.GetAllDevicesForCustomer(Session["UserID"].ToString());
                        ddlDevices.DataBind();

                        ddlMapTOVehicle.DataSource = db.GetUnAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();

                        //gdVehicles.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        //gdVehicles.DataBind();
                        gvrecords.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        gvrecords.DataBind();
                    }
                }
            }
        }

        //--------------Very Important Code: Do Not Delete---------------------------------
        //protected void lbtndelete_click(object sender, EventArgs e)
        //{
        //    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('You are about to delete the mapped vehicle');</script>");

        //    DBClass dbclass = new DBClass();
        //    Button ltb = (Button)sender;
        //    string stat = "1";
        //    DataTable V = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
        //    string VN = V.Rows[0][1].ToString();
        //    dbclass.statUpdate(VN, stat);

        //    dbclass.DeleteVehicleDeviceRel(ltb.ToolTip);

        //    ddlDevices.DataSource = dbclass.GetAllDevicesForCustomer(Session["UserID"].ToString());
        //    ddlDevices.DataBind();

        //    ddlMapTOVehicle.DataSource = dbclass.GetUnAssingedVehicles(Session["UserID"].ToString());
        //    ddlMapTOVehicle.DataBind();

        //    //gdVehicles.DataSource = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
        //    //gdVehicles.DataBind();
        //    gvrecords.DataSource = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
        //    gvrecords.DataBind();

        //}
        //--------------Very Important Code: Do Not Delete---------------------------------

        protected void gvrecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //getting username from particular row
                string username = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Device_ID"));
                //identifying the control in gridview
                LinkButton lnkbtnresult = (LinkButton)e.Row.FindControl("lnkdelete");
                //raising javascript confirmationbox whenver user clicks on link button
                lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + username + "')");
            }
        }

        protected void lnkdelete_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            LinkButton lnkbtn = sender as LinkButton;
            //getting particular row linkbutton
            GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
            //getting userid of particular row
            string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
            string username = gvrow.Cells[0].Text;
            db.CreateDeviceHistory(userid, "Device DeAssigned to" + " " + username, DateTime.Now.ToString());
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete from Vehicles_Device_Rel where Device_ID='" + userid + "'", sqlconn);
            cmd.ExecuteNonQuery().ToString();
            sqlconn.Close();

            BindDetails();

            //Displaying alert message after successfully deletion of user
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('" + username + " details deleted successfully')", true);

 
 
            DBClass dbclass = new DBClass();
 
 

            ddlDevices.DataSource = dbclass.GetAllDevicesForCustomer(Session["UserID"].ToString());
            ddlDevices.DataBind();

            ddlMapTOVehicle.DataSource = dbclass.GetUnAssingedVehicles(Session["UserID"].ToString());
            ddlMapTOVehicle.DataBind();

            //gdVehicles.DataSource = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
            //gdVehicles.DataBind();
            gvrecords.DataSource = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
            gvrecords.DataBind();
            dbclass.delmultitrack(username, Session["UserID"].ToString()); 

        }

        private void BindDetails()
        {
            //connection open
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //sql command to execute query from database
            SqlCommand cmd = new SqlCommand("Select * from Vehicles_Device_Rel", sqlconn);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            //Binding data to gridview
            gvrecords.DataSource = ds;
            gvrecords.DataBind();
            sqlconn.Close();

        }


        protected void onEditCommand_Click(object sender, DataGridCommandEventArgs e)
        {

        }

        protected void btMap_Click(object sender, EventArgs e)
        {
            if (ddlDevices.SelectedValue.ToString() == "")
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Cannot map Device to Vehicle');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }
            else
            {
 
 
                DBClass db = new DBClass();
 
 
                                
                string UI = Session["UserID"].ToString();
                string sms = "35";

                DataTable FFF = db.GetVehicleIMEI(ddlDevices.SelectedItem.Value);
                string imei = FFF.Rows[0][3].ToString();
                string model = FFF.Rows[0][4].ToString();
                string instaldate = " ";
                if (FFF.Rows[0][6].ToString() != null)
                {
                    instaldate = FFF.Rows[0][6].ToString();
                }
                db.CreateDeviceHistory(ddlDevices.SelectedItem.Value, "Assigned Device to" + " " + ddlMapTOVehicle.SelectedItem.Value, DateTime.Now.ToString());
                db.AssignVehicle(ddlDevices.SelectedItem.Value, ddlMapTOVehicle.SelectedItem.Value, UI, sms,imei ,model ,instaldate);

                gvrecords.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                gvrecords.DataBind();

                ddlDevices.DataSource = db.GetAllDevicesForCustomer(Session["UserID"].ToString());
                ddlDevices.DataBind();

                ddlMapTOVehicle.DataSource = db.GetUnAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
            }
        }
    }
}

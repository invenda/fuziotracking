﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Tracking.Home"
    MasterPageFile="~/ESL.Master" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content1" runat="server">
    <link href="CSS/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }

        }
        function myclick(a, b) {
            map.panTo(new GLatLng(a, b));
            //GEvent.trigger(gmarkers[i],"click");
        }
        function openTrackHistory() {
            window.open("TrackingHistory.aspx");
            return false;
        }
        function Daykm() {
            window.open("DayKMRunReport.aspx");
            return false;
        }
        function openRouteHistory() {
            window.open("RouteHistory.aspx");
            return false;
        }
        function openDownloadReport() {
            window.open("GenerateReport.aspx");
            return false
        }
        function openCustomerMgnmt() {
            window.open("CustDetails.aspx");
            return false
        }
        function openViewDetails() {

            window.open("JustView.aspx");
            return false
        }
        function openVehMgmt() {
            window.open("CreateVehicles.aspx");
            return false
        }
        function openVehView() {
            window.open("ViewCustVeh.aspx");
            return false
        }
        function openEngineStatusReport() {
            window.open("EngineStatusReport.aspx");
            return false
        }
        function openFuelEntry() {
            window.open("FuelFill.aspx");
            return false
        }
        function openFuelEntryRpt() {
            window.open("FuelRpt.aspx");
            return false
        }
        function openKMcor() {
            window.open("Corrections.aspx");
            return false
        }
        function openTotalKMRpt() {
            window.open("report2.aspx");
            return false;
        }
        function opengeo() {
            window.open("Geo.aspx");
            return false;
        }
        function openoverspeed() {
            window.open("OverSpeedreport.aspx");
        }
        function openalertreport() {
            window.open("VehicleAlertReport.aspx");
        }
        function opengeotrip() {
            window.open("GeoTrip.aspx");
            return false;
        }
        function alertsdetails(x) {
            alert(x.join('\n'));
        }
        function opendategeo() {
            window.open("dategeofence.aspx");
            return false;
        }
        function opendailyreport() {
            window.open("VehicalDailyReport.aspx");
            return false;
        }
        function stopreport() {
            window.open("Vehiclestoppagereport.aspx");
            return false;
        }
        function BSreport() {
            window.open("BlackSpotreport.aspx");
            return false;
        }
        function openAlerts() {
            window.open("SetAlerts.aspx");
            return false;
        }
        function opengeoview() {
            window.open("ViewGeoRoutePoints.aspx");
            return false;
        }
        function Towereport() {
            window.open("TowedReport.aspx");
            return false;
        }
        function openDaygraph() {
            window.open("DayGraph.aspx");
            return false
        }
        function openFuelgraph() {
            window.open("Fuel.aspx");
            return false
        }
        function openpieDaygraph() {
            window.open("Pieday.aspx");
            return false
        }
        function Templog() {
            window.open("Tempdatalog.aspx");
            return false
        }
        function Fuellog() {
            window.open("Fuellog.aspx");
            return false
        }
        function Daykmff() {
            window.open("Daykm.aspx");
            return false
        }
        function Entertyremailage() {
            window.open("VehicleTyresDetails.aspx");
            return false
        }
        function GettyremailageDetails() {
            window.open("GetTyreDetails.aspx");
            return false
        }
        function monthkmff() {
            window.open("Monthkmreport.aspx");
            return false
        }
    </script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript">
        $(function () {
            blinkeffect('');
        })
        function blinkeffect(selector) {
            $(selector).fadeOut('slow', function () {
                $(this).fadeIn('slow', function () {
                    blinkeffect(this);
                });
            });
        }
    </script>
    <script type="text/javascript">
        // SCRIPT FOR THE MOUSE EVENT.
        function MouseEvents(objRef, evt) {
            if (evt.type == "mouseover") { objRef.style.cursor = 'pointer'; objRef.style.backgroundColor = "Skyblue"; }
            else { if (evt.type == "mouseout") objRef.style.backgroundColor = "#CCFF99"; }
        }
    </script>


    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="True">
    </asp:ScriptManager>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
    <div style="width: auto; height: 90%; background-color:#337ab7;">
        <table style="width: 100%;">
            <tr>
                <td id="desktopNavbar" style="" colspan="2">
                    <ul class="nav nav-tabs ">
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="lnkCxMgnt" runat="server"
                                Text="My Account" meta:resourcekey="lnkCxMgntResource1" /></a>
                            <ul id="Ul1">
                                <li><a>
                                    <asp:LinkButton ID="lnkCxView" Text="View Details" Font-Bold="True" runat="server"
                                        PostBackUrl="~/JustView.aspx" meta:resourcekey="lnkCxViewResource1"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton23" Text="Change Password" Font-Bold="True" runat="server"
                                        PostBackUrl="~/Changepassword.aspx" meta:resourcekey="LinkButton23Resource1"></asp:LinkButton></a></li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton1" runat="server"
                                Text="Vehicle Management" meta:resourcekey="LinkButton1Resource1" /></a><ul id="Ul2">
                                    <li><a>
                                        <asp:LinkButton ID="lnkbtnVehMgmt" Font-Bold="True" Text="Create Vehicle" runat="server"
                                            PostBackUrl="~/CreateVehicles.aspx" meta:resourcekey="lnkbtnVehMgmtResource1"></asp:LinkButton></a></li>
                                    <li><a>
                                        <asp:LinkButton ID="lnjbtnViewVeh" Font-Bold="True" Text="View Vehicles" runat="server"
                                            PostBackUrl="~/ViewCustVeh.aspx" meta:resourcekey="lnjbtnViewVehResource1"></asp:LinkButton></a>
                                    </li>
                                   
                                    <li><a>
                                        <asp:LinkButton ID="LinkButton45" Font-Bold="True" Text="Set Alerts" runat="server"
                                            meta:resourcekey="LinkButton45Resource1"></asp:LinkButton></a>
                                        <ul id="Ul10">
                                            <li><a>
                                                <asp:LinkButton ID="LinkButton15" Font-Bold="True" Text="Daily Alert" runat="server"
                                                    PostBackUrl="~/SetDailyAlerts.aspx" meta:resourcekey="LinkButton15Resource1"></asp:LinkButton></a></li>
                                            <li><a>
                                                <asp:LinkButton ID="LinkButton35" Font-Bold="True" Text="General Alert" runat="server"
                                                    PostBackUrl="~/SetGeneralAlert.aspx" meta:resourcekey="LinkButton35Resource1"></asp:LinkButton></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  ID="lbtnAdmin" Text="Configure" Font-Size="13px"
                                runat="server" PostBackUrl="AssignDevicesToVehicles.aspx" meta:resourcekey="lbtnAdminResource1"></asp:LinkButton></a>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  ID="lnkbtnRouteReplay" Text="Replay" Font-Size="13px"
                                runat="server" meta:resourcekey="lnkbtnRouteReplayResource1"></asp:LinkButton></a>
                            <ul id="Ul9">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton26" Font-Bold="True" Text="Route Replay" runat="server"
                                        PostBackUrl="~/TrackingHistory.aspx" meta:resourcekey="LinkButton26Resource1"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton30" Font-Bold="True" Text="Towed Replay" runat="server"
                                        PostBackUrl="~/TowedRouteReplay.aspx" meta:resourcekey="LinkButton30Resource1"></asp:LinkButton></a>
                                </li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="lblReport" runat="server"
                                Text="Report" meta:resourcekey="lblReportResource1" /></a>
                            <ul id="MenueReports">
                                <li><a>
                                    <asp:LinkButton ID="lbtvr" Text="Daily Report" runat="server" Font-Bold="True" meta:resourcekey="lbtvrResource1"></asp:LinkButton></a>
                                    <ul id="Ul5">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton44" Text="Day Hourly KM Report" runat="server" Font-Bold="True"
                                                OnClientClick="Daykmff()" meta:resourcekey="LinkButton44Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton10" Text="Day Activity" runat="server" Font-Bold="True"
                                                OnClick="LinkButton10_Click" meta:resourcekey="LinkButton10Resource1"></asp:LinkButton></a></li>
                                        <%-- <li><a>
                                            <asp:LinkButton ID="LinkButton11" Text="Stoppage/Idling summary" runat="server" Font-Bold="True"
                                                OnClientClick="stopreport()" meta:resourcekey="LinkButton11Resource1"></asp:LinkButton></a></li>--%>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton42" Text="Stop/Idling summary" runat="server" Font-Bold="True"
                                                OnClientClick="stopreport()" meta:resourcekey="LinkButton42Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton31" Text="Towed summary" runat="server" Font-Bold="True"
                                                OnClientClick="Towereport()" meta:resourcekey="LinkButton31Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton38" Text="Day Temp Log" runat="server" Font-Bold="True"
                                                OnClientClick="Templog()" meta:resourcekey="LinkButton38Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton39" Text="Fuel Report" runat="server" Font-Bold="True"
                                                OnClientClick="Fuellog()" meta:resourcekey="LinkButton39Resource1"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton18" Text="Monthly Report" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton18Resource1"></asp:LinkButton></a>
                                    <ul>
                                        <%-- <li><a>
                                            <asp:LinkButton ID="LinkButton17" Text="KM Report" runat="server" Font-Bold="true"
                                                PostBackUrl="~/MonthlyKMReport.aspx"></asp:LinkButton></a></li>--%>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton16" Text="Temperature Report" runat="server" Font-Bold="True"
                                                PostBackUrl="~/Tempreport1.aspx" meta:resourcekey="LinkButton16Resource1"></asp:LinkButton></a></li>
                                        <%--   <li><a>
                                            <asp:LinkButton ID="LinkButton11" Text="Fuel fill Report" runat="server" Font-Bold="True"
                                                PostBackUrl="~/Fulefillingreport.aspx" meta:resourcekey="LinkButton11Resource1"></asp:LinkButton></a></li>--%>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton43" Text=" MonthKM Report" runat="server" Font-Bold="True"
                                                PostBackUrl="~/monthkm.aspx" meta:resourcekey="LinkButton43Resource1"></asp:LinkButton></a></li>
                                        <%-- <li><a>
                                           <asp:LinkButton ID="LinkButton17" Text="Temp Report" runat="server" Font-Bold="true"
                                                PostBackUrl="~/Tempreport.aspx"></asp:LinkButton></a></li>--%>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="lbtnother" Text="Other Report" runat="server" Font-Bold="True"
                                        meta:resourcekey="lbtnotherResource1"></asp:LinkButton></a>
                                    <ul id="Ul6">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton2" Text="Day Summary" runat="server" Font-Bold="True"
                                                OnClientClick="openTotalKMRpt()" meta:resourcekey="LinkButton2Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton5" Text="Over Speed Summary" runat="server" Font-Bold="True"
                                                OnClientClick="openoverspeed()" meta:resourcekey="LinkButton5Resource1"></asp:LinkButton></a>
                                        </li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton4" Text="Alert Summary" runat="server" Font-Bold="True"
                                                OnClientClick="openalertreport()" meta:resourcekey="LinkButton4Resource1"></asp:LinkButton></a>
                                        </li>
                                        <li><a>
                                            <asp:LinkButton ID="btnFuelEntryRpt" Text="Fuel Entry" runat="server" Font-Bold="True"
                                                OnClientClick="openFuelEntryRpt()" meta:resourcekey="btnFuelEntryRptResource1"></asp:LinkButton></a>
                                        </li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton12" Text="Black Spot" runat="server" Font-Bold="True"
                                                OnClientClick="BSreport()" meta:resourcekey="LinkButton12Resource1"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="lnkbtnFuelMgmt" runat="server"
                                Text="Fuel Management" meta:resourcekey="lnkbtnFuelMgmtResource1" /></a>
                            <ul id="Ul3">
                                <li><a>
                                    <asp:LinkButton ID="lnkbtnFuelEntry" Text="Fuel Voucher Entry" Font-Bold="True" runat="server"
                                        OnClientClick="openFuelEntry()" meta:resourcekey="lnkbtnFuelEntryResource1"></asp:LinkButton></a></li>
                                <%-- <li><a>
                                        <asp:LinkButton ID="lnkbtnKMCorrection" Text=" Cuml. KM Corrections" runat="server"
                                            Font-Bold="True" OnClientClick="openKMcor()" meta:resourcekey="lnkbtnKMCorrectionResource1"></asp:LinkButton></a></li> --%>
                                <li><a>
                                    <asp:LinkButton ID="lnkbtnKMCorrection" Text="Cuml. KM Corrections" runat="server"
                                        Font-Bold="True" OnClientClick="openKMcor()"></asp:LinkButton>
                                </a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton47" Text="Fuelfilling report" runat="server" Font-Bold="True"
                                        PostBackUrl="~/Fulefillingreport.aspx" meta:resourcekey="LinkButton47Resource1"></asp:LinkButton></a></li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton11" runat="server"
                                Text="Tyre Monitor"  /></a>
                            <ul id="Ul12">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton48" Font-Bold="True" Text="Set Tyre Mileage" runat="server"
                                        OnClientClick="Entertyremailage()"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton49" Font-Bold="True" Text="View Tyre Mileage"
                                        runat="server" OnClientClick="GettyremailageDetails()"></asp:LinkButton></a>
                                </li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton3" runat="server"
                                Text="GeoFencing" meta:resourcekey="LinkButton3Resource1"></asp:LinkButton></a>
                            <ul id="Ul4">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton24" Text="Create Route" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton24Resource1"></asp:LinkButton></a>
                                    <ul>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton25" Text="Create Route" runat="server" Font-Bold="True"
                                                PostBackUrl="~/CreateRoute.aspx" meta:resourcekey="LinkButton25Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton28" Text="Delete Route" runat="server" Font-Bold="True"
                                                PostBackUrl="~/DeleteRoute.aspx" meta:resourcekey="LinkButton28Resource1"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton29" Text="Plot/Edit Route" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton29Resource1"></asp:LinkButton></a>
                                    <ul>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton6" Text="Plot Geo Points" runat="server" Font-Bold="True"
                                                PostBackUrl="~/plotGeoPoints.aspx" meta:resourcekey="LinkButton6Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton7" Text="Edit/Delete Points" runat="server" Font-Bold="True"
                                                PostBackUrl="~/CreateGeoRoute.aspx" meta:resourcekey="LinkButton7Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton17" Text="Plot/Edit Route Basis" runat="server" Font-Bold="True"
                                                PostBackUrl="~/Editplotgeopointsnew.aspx" meta:resourcekey="LinkButton17Resource1"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton14" runat="server"
                                Text="MIS" meta:resourcekey="LinkButton14Resource1"></asp:LinkButton></a>
                            <ul id="Ul8">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton19" Text="Graphs" runat="server" Font-Bold="True" meta:resourcekey="LinkButton19Resource1"></asp:LinkButton></a>
                                    <ul id="Ul7">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton13" Text="Temperature Graph" runat="server" Font-Bold="True"
                                                OnClick="LinkButton13_Click" meta:resourcekey="LinkButton13Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton33" Text="Day summury Graph" runat="server" Font-Bold="True"
                                                OnClientClick="openDaygraph()" meta:resourcekey="LinkButton33Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton36" Text="Fuel v/s Km Graph" runat="server" Font-Bold="True"
                                                OnClick="LinkButton36_Click" meta:resourcekey="LinkButton36Resource1"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton37" Text="Day summury Pie Chart" runat="server" Font-Bold="True"
                                                OnClientClick="openpieDaygraph()" meta:resourcekey="LinkButton37Resource1"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton20" Text="ROI Calculation" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton20Resource1"></asp:LinkButton></a> </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton21" Text="Vehicle Audit" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton21Resource1"></asp:LinkButton></a> </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton22" Text="Cost Analysis" runat="server" Font-Bold="True"
                                        meta:resourcekey="LinkButton22Resource1"></asp:LinkButton></a> </li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton34" runat="server"
                                Text="Trip Management" meta:resourcekey="LinkButton34Resource1"></asp:LinkButton></a>
                            <ul>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton8" Text="Trip Assigning" runat="server" Font-Bold="True"
                                        PostBackUrl="~/RouteAssigntoVehicle.aspx" meta:resourcekey="LinkButton8Resource1"></asp:LinkButton></a>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton27" Text="Trip Report" runat="server" Font-Bold="True"
                                        PostBackUrl="~/VehiclebasedGeoReport.aspx" meta:resourcekey="LinkButton27Resource1"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton41" Text="Route Basis Report" runat="server" Font-Bold="True"
                                        PostBackUrl="~/Routebasisreport.aspx" meta:resourcekey="LinkButton41Resource1"></asp:LinkButton></a></li>
                            </ul>
                        </li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="LinkButton9" runat="server"
                                Text="Multi View" OnClick="LinkButton9_Click" meta:resourcekey="LinkButton9Resource1"></asp:LinkButton></a></li>
                        <li class="btn btn-primary">
                            <asp:LinkButton  Font-Size="13px" ID="lbtnLogOut" runat="server"
                                Text="Logout" OnClick="lbtnLogOut_Click" meta:resourcekey="lbtnLogOutResource1"></asp:LinkButton></a>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%; height: 40px;">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table id="pnlToolbar" class="submenu-control" style="vertical-align: middle;" cellpadding="0"
                                cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: left; border-bottom-style: solid;
                                            height: 40px; width: 200px; border-bottom-width: thin;">
                                            <div style=" text-align: center;margin:5px;">
                                                <asp:Label ID="Label1" runat="server" Text="General Alerts"  Font-Bold="True" CssClass="label label-success" style="font-size:15px;padding: 2px;"
                                                    meta:resourcekey="Label1Resource1"></asp:Label>
                                            </div>
                                            <div id="ddd" style=" width: 100px;margin:5px;">
                                                <asp:Button ID="Btndalerts" runat="server" Text="Daily  Alerts :" Font-Bold="True"
                                                      Width="98%" OnClick="Btndalerts_Click" CssClass="btn btn-danger"
                                                    meta:resourcekey="BtndalertsResource1" style="font-size:9px;"/>
                                            </div>
                                        </td>
                                        <td style="width: 100%; border-bottom-style: solid; border-bottom-width: thin;">
                                            <div style="height: 25px; border-bottom-style: solid; border-bottom-width: thin;">
                                                <table style="width: 100%; height: 25px;">
                                                    <tr>
                                                        <td style="width: 25%; height: 25px; vertical-align: middle; font-weight: bold;"
                                                            align="right">
                                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Black" Text="Engine Status :"
                                                                meta:resourcekey="Label2Resource1"></asp:Label>
                                                            &nbsp;&nbsp;
                                                            <asp:Image ID="imgst" runat="server" Width="14px" Height="14px" meta:resourcekey="imgstResource1" />
                                                            <asp:Label Text="Stopped" runat="server" ID="Lbl1" meta:resourcekey="Lbl1Resource1"></asp:Label>&nbsp;&nbsp;
                                                            <asp:Image ID="imgidl" runat="server" Width="14px" Height="14px" meta:resourcekey="imgidlResource1" />
                                                            <asp:Label Text="Idle" runat="server" ID="lbl2" meta:resourcekey="lbl2Resource1"></asp:Label>&nbsp;&nbsp;
                                                            <asp:Image ID="imgmv" runat="server" Width="14px" Height="14px" meta:resourcekey="imgmvResource1" />
                                                            <asp:Label ID="lbl3" runat="server" Text="Moving" meta:resourcekey="lbl3Resource1"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%; height: 100%; vertical-align: middle;" align="center">
                                                            <asp:Label ID="lblmalerts" runat="server" Font-Bold="True" Visible="False" meta:resourcekey="lblmalertsResource1"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; height: 25px; vertical-align: middle; font-weight: bold;"
                                                            align="center">
                                                            <asp:Label ID="Nofodevices" runat="server" Font-Bold="True" Font-Italic="False" meta:resourcekey="NofodevicesResource1"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; height: 25px; vertical-align: middle; font-weight: bold;"
                                                            align="center">
                                                            <asp:Label ID="noofdesabled" runat="server" Font-Bold="True" Font-Italic="False"
                                                                meta:resourcekey="NofodevicesResource1"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%; height: 25px; vertical-align: middle; font-weight: bold;"
                                                            align="right">
                                                            Current time&nbsp;&nbsp;:
                                                            <asp:Label ID="LblSCT" runat="server" Font-Bold="True" Font-Italic="False" meta:resourcekey="LblSCTResource1"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="height: 30px; width: 100%; border-top-width: medium; vertical-align: middle;
                                                text-align: right; margin: 0 auto;">
                                                <table style="width: 90%; height: 100%; text-align: right">
                                                    <tr align="right">
                                                        <td style="width: 10%; height: 30px; vertical-align: middle; font-weight: bold;"
                                                            align="right">
                                                        </td>
                                                        <td style="width: 750%; text-align: right; height: 30px;">
                                                            <marquee onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 6, 0);">
                                                            <asp:Label ID="lblalerts2" runat="server" ForeColor="Black" Font-Bold="True" >
                                                              </asp:Label>
                                                            <asp:Label ID="lbloffalerts" runat="server" ForeColor="Black" Font-Bold="True" >
                                                            </asp:Label>
                                                            </marquee>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <div style="position: relative; vertical-align: top; height: 480px; overflow: hidden;
        overflow: auto; background-color: White; top: 0px; left: 0px;">
        <asp:UpdatePanel ID="updatepannel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" BackColor="#CCFF99"
                    HorizontalAlign="Center" Width="100%" BorderColor="#FF33CC" ForeColor="Black"
                     meta:resourcekey="GVResource1">
                    <RowStyle Height="5px" />
                    <AlternatingRowStyle Height="5px" />
                    <Columns>
                        <asp:TemplateField HeaderText="Select" meta:resourcekey="TemplateFieldResource1">
                            <ItemTemplate>
                                <asp:RadioButton ID="rbtn" runat="server" OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                    Width="20px" Height="20px" AutoPostBack="True" OnCheckedChanged="rbtn_Click"
                                    CssClass="rbtc" meta:resourcekey="rbtnResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Vehicle RTO no" meta:resourcekey="TemplateFieldResource2">
                            <ItemTemplate>
                                <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("VehicalNumber") %>' 
                                    Font-Bold="True"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Font-Bold="False" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Engine status" meta:resourcekey="TemplateFieldResource3">
                            <ItemTemplate>
                                <asp:Image ID="Simg" ImageUrl='<%# Eval("ImageUrl") %>' runat="server" Height="16px" Width="16px" BackColor="Transparent"
                                    meta:resourcekey="SimgResource1"/>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Check">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>' Font-Bold="True"
                                     ToolTip='<%# Eval("ItemDescription") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit serial no" meta:resourcekey="TemplateFieldResource5">
                            <ItemTemplate>
                                <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblserialResource1" ToolTip="sdasdasd"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="70px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Data" meta:resourcekey="TemplateFieldResource6">
                            <ItemTemplate>
                                <asp:Label ID="lbllastdata" Text='<%# Bind("Lastdata") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbllastdataResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="130px" />
                            <ItemStyle Width="130px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GPS" meta:resourcekey="TemplateFieldResource7">
                            <ItemTemplate>
                                <asp:Label ID="lblgps" Text='<%# Bind("Gps") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblgpsResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ignition" meta:resourcekey="TemplateFieldResource8">
                            <ItemTemplate>
                                <asp:Label ID="lbling" Text='<%# Bind("ingnition") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblingResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="45px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Battery voltage" meta:resourcekey="TemplateFieldResource9">
                            <ItemTemplate>
                                <asp:Label ID="lblbvolt" Text='<%# Bind("bvolt") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblbvoltResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver Name" meta:resourcekey="TemplateFieldResource10">
                            <ItemTemplate>
                                <asp:Label ID="lbldname" Text='<%# Bind("Dname") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbldnameResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver mobile" meta:resourcekey="TemplateFieldResource11">
                            <ItemTemplate>
                                <asp:Label ID="lbldmobile" Text='<%# Bind("dmobile") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbldmobileResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Over Speed " meta:resourcekey="TemplateFieldResource12">
                            <ItemTemplate>
                                <asp:Button ID="btnlovs" runat="server" OnClick="btnlovs_Click" CssClass="button "
                                    meta:resourcekey="btnlovsResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" meta:resourcekey="TemplateFieldResource13">
                            <ItemTemplate>
                                <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" CssClass="button "
                                    meta:resourcekey="btnlocationResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Speed" meta:resourcekey="TemplateFieldResource14">
                            <ItemTemplate>
                                <asp:Button ID="btnspeed" runat="server" OnClick="btnspeed_Click" CssClass="button  "
                                    meta:resourcekey="btnspeedResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fuel" meta:resourcekey="TemplateFieldResource15">
                            <ItemTemplate>
                                <asp:Button ID="btnfuel" runat="server" OnClick="btnfuel_Click" CssClass="button "
                                    meta:resourcekey="btnfuelResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today KM run" meta:resourcekey="TemplateFieldResource16">
                            <ItemTemplate>
                                <asp:Button ID="btnKM" runat="server" OnClick="btnKM_Click" CssClass="button " meta:resourcekey="btnKMResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Temp" meta:resourcekey="TemplateFieldResource17">
                            <ItemTemplate>
                                <asp:Button ID="btntemp" runat="server" OnClick="btntemp_Click" CssClass="button "
                                    meta:resourcekey="btntempResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Availability" meta:resourcekey="TemplateFieldResource18">
                            <ItemTemplate>
                                <asp:Button ID="btnavail" runat="server" OnClick="btnavail_Click" CssClass="button"
                                    meta:resourcekey="btnavailResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Alert" meta:resourcekey="TemplateFieldResource19">
                            <ItemTemplate>
                                <asp:Button ID="btnalert" runat="server" OnClick="btnalert_Click" CssClass="button "
                                    meta:resourcekey="btnalertResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#006600"  Height="3px" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
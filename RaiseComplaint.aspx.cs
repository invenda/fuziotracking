﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Net.Mail;


namespace Tracking
{
    public partial class RaiseComplaint : Page
    {
 
 
        DBClass db = new DBClass();
 
 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                    ddlMapTOVehicle.DataBind();
                    ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                    ddlFromHOURS.DataBind();
                }
            }
        }

        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select One--");
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable tt = db.getdevicesid(Vno);
            if (tt.Rows.Count > 0)
            {
                lblunitno.Text = tt.Rows[0][0].ToString();

            }
             DataTable checkcomplaitid = db.CheckCompalintsidstatus(Vno, tt.Rows[0][0].ToString());
             if (checkcomplaitid.Rows.Count > 0)
             {
                 if (Convert.ToInt32(checkcomplaitid.Rows[0][10].ToString()) == 0)
                 {
                     DateTime tdate = Convert.ToDateTime(checkcomplaitid.Rows[0][0].ToString());
                     string ttid = tdate.ToString("yyyyMMdd");
                     string id = checkcomplaitid.Rows[0][1].ToString();
                     ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Alraedy Complaint Raised for this vehicle compalint id:" + ttid + "" + id + ".');", true);
                     //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
                     clear12();
                     btnSubmit.Enabled = false;
                 }
             }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string ticketdate = DateTime.Now.ToString();
            //DateTime dateticket=Convert.ToDateTime(ticketdate);
            //string tdate=dateticket.ToString("yyyyMMdd");
            //lblDevice.Text = tdate;
            DataTable getticketid = db.GetCompalintsid(ticketdate);
            string deviceid = "";
            int ticketid;
            string vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable tt = db.getdevicesid(vno);
            if (tt.Rows.Count > 0)
            {
                deviceid = tt.Rows[0][0].ToString();
            }
            string AMPM;
            string fromtime = ddlFromHOURS.SelectedValue;

            AMPM = rbtnAMPM1.SelectedValue.ToString();
            string time = txtfdate.Text + " " + fromtime + ":00" + " " + AMPM;

            //DataTable checkcomplaitid = db.CheckCompalintsidstatus(vno, deviceid);
            //if (checkcomplaitid.Rows.Count > 0)
            //{
            //    if (Convert.ToInt32(checkcomplaitid.Rows[0][10].ToString()) == 1)
            //    {
                    if (getticketid.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(getticketid.Rows[0][1].ToString()))
                        {
                            int tktid = Convert.ToInt32(getticketid.Rows[0][1].ToString());
                            ticketid = tktid + 1;
                            DateTime dateticket = Convert.ToDateTime(ticketdate);
                            string tdate = dateticket.ToString("yyyyMMdd");
                            string ttid = ticketid.ToString();
                            string complaintid = tdate + "" + ttid;
                            db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text, complaintid, ticketdate);
                           

                           
                            sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
                            clear();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + tdate + "" + ttid + " Raised sucessfully for " + vno + " ...');", true);

                        }
                        else
                        {
                            ticketid = 1;
                            DateTime dateticket = Convert.ToDateTime(ticketdate);

                            string tdate = dateticket.ToString("yyyyMMdd");
                            string ttid = ticketid.ToString();
                            string complaintid = tdate + "" + ttid;
                            db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text, complaintid, ticketdate);
                            
                            sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
                            clear();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully for " + vno + " ....');", true);

                        }
                    }
                    else
                    {
                        ticketid = 1;
                        DateTime dateticket = Convert.ToDateTime(ticketdate);

                        string tdate = dateticket.ToString("yyyyMMdd");
                        string ttid = ticketid.ToString();
                        string complaintid = tdate + "" + ttid;
                        db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text, complaintid, ticketdate);
                     

                        sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
                        clear();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully for " + vno + " ...');", true);

                    }
                //}
                //else
                //{

                //    DateTime tdate = Convert.ToDateTime(checkcomplaitid.Rows[0][0].ToString());
                //    string ttid = tdate.ToString("yyyyMMdd");
                //    string id = checkcomplaitid.Rows[0][1].ToString();
                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Alraedy Complaint Raised for this vehicle compalint id:" + ttid + "" + id + ".');", true);
                //}
           // }
            //else
            //{

            //    if (getticketid.Rows.Count > 0)
            //    {
            //        if (!string.IsNullOrEmpty(getticketid.Rows[0][1].ToString()))
            //        {
            //            int tktid = Convert.ToInt32(getticketid.Rows[0][1].ToString());
            //            ticketid = tktid + 1;
            //            db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text);
            //            // db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, txtfdate.Text, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0);
            //            DateTime dateticket = Convert.ToDateTime(ticketdate);

            //            string tdate = dateticket.ToString("yyyyMMdd");
            //            string ttid = ticketid.ToString();
            //            sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
            //            // sendemailonly(vno, tdate + "" + ttid, txtMessage.Text);
            //            clear();
            //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully for " + vno + " ...');", true);
            //            // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + tdate + "" + ttid + "Complaint Raised sucessfully...');", true);

            //        }
            //        else
            //        {
            //            ticketid = 1;
            //            db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text);
            //            // db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, txtfdate.Text, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0);
            //            DateTime dateticket = Convert.ToDateTime(ticketdate);

            //            string tdate = dateticket.ToString("yyyyMMdd");
            //            string ttid = ticketid.ToString();
            //            sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
            //            //sendemailonly(vno, tdate + "" + ttid, txtMessage.Text);
            //            clear();
            //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully for " + vno + " ...');", true);
            //            // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully....');", true);
            //            // this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
            //        }
            //    }
            //    else
            //    {
            //        ticketid = 1;
            //        db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, time, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0, txtcity.Text, txtmobile.Text);
            //        //db.Insrtcomplaints(ticketdate, ticketid.ToString(), vno, deviceid, ddlcomplaint.SelectedValue.ToString(), txtMessage.Text, txtfdate.Text, txtLocation.Text, Session["UserID"].ToString(), Session["UserName"].ToString(), 0);
            //        DateTime dateticket = Convert.ToDateTime(ticketdate);

            //        string tdate = dateticket.ToString("yyyyMMdd");
            //        string ttid = ticketid.ToString();
            //        sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), time, txtLocation.Text, txtcity.Text, txtmobile.Text, txtMessage.Text);
            //        // sendemailonly(vno, tdate + "" + ttid, txtMessage.Text);
            //        clear();
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully for " + vno + " ...');", true);
            //        // ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Complaint ID" + " " + tdate + "" + ttid + " " + " Raised sucessfully...');", true);

            //    }

            //}

        }
        public void clear12()
        {           
            ddlcomplaint.Enabled=false;          
            lblunitno.Enabled = false;
            txtfdate.Enabled = false;
            txtLocation.Enabled = false;
            txtMessage.Enabled = false;
            txtmobile.Enabled = false;
            ddlFromHOURS.Enabled = false;
            rbtnAMPM1.Enabled = false;
            txtcity.Enabled = false;
        }//  
        public void clear()
        {
            ddlcomplaint.SelectedIndex = -1;
            ddlMapTOVehicle.SelectedIndex = -1;
            lblunitno.Text = string.Empty;
            txtfdate.Text = string.Empty;
            txtLocation.Text = string.Empty;
            txtMessage.Text = string.Empty;
            txtmobile.Text = string.Empty;
            ddlFromHOURS.SelectedIndex = -1;
            rbtnAMPM1.SelectedIndex = -1;
            txtcity.Text = string.Empty;
        }//  sendemailonly(vno, tdate + "" + ttid, ticketdate, deviceid, ddlcomplaint.SelectedValue.ToString(), txtfdate.Text, txtLocation.Text, txtcity.Text, txtmobile.Text);
        public void sendemailonly(string Vno, string Complaintid, string cdate, string devide, string ctype, string avildate, string availpalace, string city, string mobile, string msg)
        {
            try
            {
 
 
                DBClass db = new DBClass();
 
 

                string frommail = "support@eslabs.co.in";
                string password = "9880470756";

                DataTable CxMobNo = db.getCxMoNo(Vno);
                string cxUID = CxMobNo.Rows[0][0].ToString();
                string cxName = CxMobNo.Rows[0][1].ToString();
                string cxMn = CxMobNo.Rows[0][2].ToString();
                string xcemail = CxMobNo.Rows[0][3].ToString();
                string username = Session["UserName"].ToString();
                DateTime date2 = Convert.ToDateTime(cdate);
                string instaldate = date2.ToString("dd/MM/yyyy hh:mm tt ");
                DateTime date222 = Convert.ToDateTime(avildate);
                string avildate12 = date222.ToString("dd/MM/yyyy hh:mm tt");


                string message = " Dear" + " " + username + ",<br />Customer Name: " + cxName + "<br />This is an automated response confirming the receipt of your Complaint vide above Ticket no. " +
                                 "We will try to resolve the issue within 48 hours or at the time mention by you. You will receive an update of the complaint within 4 working hours.<br /><br /><br />" +
                                 "Our support Team will get in touch with you to confirm the availability of the vehicle as mention by you." +
                                  "For your records, the details of the Complaint Ticket are listed below. Pls mention the Ticket ID to ensure your replies are tracked." +
                                  "<br />" +
                                  "<br />" +
                                  "Ticket ID:  " + Complaintid + "<br /> Date of Complaint:" + instaldate + "<br />Vehicle No: " + Vno + "<br />Device No:" + devide + "<br />" +
                                  "Type of Complaint:" + ctype + "<br /> Nature of Complaint:" + msg + "<br />Date & Time of Vehicle Avalibility: " + avildate12 + "<br />Place of Vehcile Avalibility:" + availpalace + "<br />" +
                                  "City: " + city + "<br /> Contact Mobile/ Driver  No:" + mobile + "<br /><br />You can check the status of the ticket online on your Tracking Page – Complaint History<br />" +
                                  "<br />" +
                                  "<br />" +
                                  "Warm regards,<br />Support Team<br />Empowered Security Labs Pvt  Ltd";


                SmtpClient smtpclint = new SmtpClient();
                MailMessage mailmsg = new MailMessage();

                mailmsg.From = new MailAddress(frommail);
                mailmsg.Subject = "Auto Reply: Ticket No-" + Complaintid + "  Vehicle No : " + Vno + "";// "REG:'" + Complaintid + "Complaintid from ESLABS";
                mailmsg.Body = message;
                mailmsg.IsBodyHtml = true;
                mailmsg.To.Add(new MailAddress(xcemail));
               // mailmsg.To.Add(new MailAddress("vamsi@eslabs.co.in"));
                mailmsg.Bcc.Add(new MailAddress("vamsi@eslabs.co.in"));

                smtpclint.Host = "mail.eslabs.co.in";
                smtpclint.EnableSsl = false;

                NetworkCredential credi = new NetworkCredential();
                credi.UserName = mailmsg.From.Address;
                credi.Password = password;
                smtpclint.UseDefaultCredentials = true;
                smtpclint.Credentials = credi;
                smtpclint.Port = 25;
                smtpclint.Send(mailmsg);
                //Response.Write("Mail sent sucessfully...");

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //Response.Write("Could not send the e-mail - error: " + ex.Message);
            }

        }


    }
}
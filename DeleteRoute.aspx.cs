﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class DeleteRoute : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                binddata();

            }
        }
        public void binddata()
        {
            if (Session["UserRole"].ToString() == "3")
            {
                ddlrno.DataSource = db.get_Geo_ALLRouteNo(Session["cUser_id"].ToString());
                ddlrno.DataBind();
                ddlrttype.Enabled = true;
                txtrname.Enabled = true;
                ddlrttype.SelectedValue = null;
                txtrname.Text = string.Empty;
            }
            else
            {
                ddlrno.DataSource = db.get_Geo_ALLRouteNo(Session["userID"].ToString());
                ddlrno.DataBind();
                ddlrttype.Enabled = true;
                txtrname.Enabled = true;
                ddlrttype.SelectedValue = null;
                txtrname.Text = string.Empty;
            }
        }
        protected void Btn_submit_Click(object sender, EventArgs e)
        {
            if (Session["UserRole"].ToString() == "3")
            {
                DataTable dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                if (dt1.Rows.Count == 0)
                {
                    int DiagResult = int.Parse(inpHide.Value);
                    if (DiagResult == 1)
                    {
                        db.delete_Geo_Route(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                        db.delete_ALL_Routepoints(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                        db.delete_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                        db.delete_ALL_Routepointsnew11(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                        // db.delete_ALL_Routepointsnew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        //db.DeleteAssingedRouteVehicles(Session["UserID"].ToString(), ddlrno.SelectedValue.ToString());
                        binddata();
                    }
                }
                else
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please DeAssign Route|');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }
            else
            {

                DataTable dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                if (dt1.Rows.Count == 0)
                {
                    int DiagResult = int.Parse(inpHide.Value);
                    if (DiagResult == 1)
                    {
                        db.delete_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        db.delete_ALL_Routepoints(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        db.delete_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        db.delete_ALL_Routepointsnew11(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        // db.delete_ALL_Routepointsnew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                        //db.DeleteAssingedRouteVehicles(Session["UserID"].ToString(), ddlrno.SelectedValue.ToString());
                        binddata();
                    }
                }
                else
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please DeAssign Route|');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }

        }

        protected void Btn_clr_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void ddlrno_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Route--");
        }

        protected void ddlrno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrno.SelectedValue != "--Select Route--")
            {
                DataTable dt;
                if (Session["UserRole"].ToString() == "3")
                {
                    dt = db.get_Geo_Route(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
                }
                else
                {
                    dt = db.get_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                }
                //DataTable dt = db.get_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                txtrname.Text = dt.Rows[0][0].ToString();
                ddlrttype.SelectedValue = dt.Rows[0][1].ToString();
                txtrname.Enabled = false;
                ddlrttype.Enabled = false;
                DataTable dt1;
                if (Session["UserRole"].ToString() == "3")
                {
                    dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());                       
                }
                else
                {
                    dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                }
               // DataTable dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                gvrecords.DataSource = dt1;
                gvrecords.DataBind();
                if (dt1.Rows.Count == 0)
                {
                    Btn_submit.Visible = true;
                    Btn_del.Visible = false;
                }
                else
                {
                    Btn_submit.Visible = false;
                    Btn_del.Visible = true;
                }
            }
        }

        protected void Btn_del_Click(object sender, EventArgs e)
        {
            DataTable dt1;
            if (Session["UserRole"].ToString() == "3")
            {
                dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["cUser_id"].ToString());
            }
            else
            {
                 dt1 = db.get_Route_Vehicles(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
            }
            if (dt1.Rows.Count != 0)
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please DeAssign Route');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'

            }
        }
    }
}

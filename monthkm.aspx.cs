﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace Tracking
{
    public partial class monthkm : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        double rcount = 0;
        double rcount1 = 0;
        double rcount2 = 0;

        int fcount = 0;
        int fcount1 = 0;
        int fcount2 = 0;

        double count = 0;
        int fcounttt = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {

                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    ddlmonthbind();
                }
            }

        }
        public void ddlmonthbind()
        {
            DateTime dt = DateTime.Now;
            int m = Convert.ToInt32(dt.Month);
            if (m == 1)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));

            }
            else if (m == 2)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));

            }
            else if (m == 3)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));

            }
            else if (m == 4)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
            }
            else if (m == 5)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
            }
            else if (m == 6)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
            }
            else if (m == 7)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));

            }
            else if (m == 8)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));

            }
            else if (m == 9)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));

            }
            else if (m == 10)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));

            }
            else if (m == 11)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));

            }
            else if (m == 12)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));

            }
        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;

            Panel1.Visible = true;
            bindmonthkm12();
            Download.Enabled = true;
            ddlMapTOVehicle.Enabled = false;
            ddlmonth.Enabled = false;
            GetRecord.Enabled = false;
        }
        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime, int Month)
        {
            return new DateTime(dateTime.Year, Month, 1);
        }
        public DateTime LastDayOfMontthFromDateTime(DateTime dateTime, int Month)
        {

            DateTime createDate = new DateTime(dateTime.Year, Month, DateTime.DaysInMonth(dateTime.Year, Month));
            return createDate;
        }
        public DataTable bindmonthkm()
        {
            DataTable dt11 = new DataTable();
            dt11.Columns.Add("Date", typeof(string));
            dt11.Columns.Add("KM", typeof(string));
            dt11.Columns.Add("Fuel", typeof(string));

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());

            DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
            String currentmonth = dt.Month.ToString();
            int currentyear = dt.Year;
            int newyear;
            if(currentmonth == "1" && month == 12)
            {
                newyear = currentyear - 1;
            }
            else
            {
                newyear = currentyear;
            }
            DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
            DateTime fdt12 = LastDayOfMontthFromDateTime(dt, month);

            String newmonth;
            String newday;
            String newmonth12;
            String newday12;

            if(fdt.Month < 10)
            {
                newmonth = "0"+fdt.Month.ToString();
            }
            else
            {
                newmonth = fdt.Month.ToString();
            }

            if(fdt.Day < 10)
            {
                newday = "0"+fdt.Day.ToString();
            }
            else
            {
                newday = fdt.Day.ToString();
            }
            if(fdt12.Month < 10)
            {
                newmonth12 = "0"+fdt12.Month.ToString();
            }
            else
            {
                newmonth12 = fdt12.Month.ToString();
            }

            if(fdt12.Day < 10)
            {
                newday12 = "0"+fdt12.Day.ToString();
            }
            else
            {
                newday12 = fdt12.Day.ToString();
            }



            DataTable dt12 = db.Select_TotalRunkm_Fuzio(ddlMapTOVehicle.SelectedValue.ToString(), newmonth + "/" + newday + "/" + newyear, newmonth12 + "/" + newday12 + "/" + newyear);
            for (int i = 0; i < dt12.Rows.Count; i++)
            {
                DataRow dr = dt11.NewRow();
                dr["Fuel"] = dt12.Rows[i][4].ToString();
                dr["Date"] = dt12.Rows[i][2].ToString();
                dr["KM"] = Math.Round(Convert.ToDouble(dt12.Rows[i][3].ToString()), 2); 
                dt11.Rows.Add(dr);
            }

                return dt11;

        }
        public DataTable bindmonthkmold()
        {
            DataTable dt11 = new DataTable();
            dt11.Columns.Add("Date", typeof(string));
            dt11.Columns.Add("KM", typeof(string));
            dt11.Columns.Add("Fuel", typeof(string));


            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());

            DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
            DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
            DateTime fdt12 = LastDayOfMontthFromDateTime(dt, month);
            for (int i = 0; i < fdt12.Day; i++)
            {
                if ((fdt.Month <= month && fdt < dt))
                {
                    DataRow dr = dt11.NewRow();
                    DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                    DateTime fdt11 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "11:59 PM");

                    DataTable dt12 = db.Select_TotalRunkmold(ddlMapTOVehicle.SelectedValue.ToString(), fdt1.ToString(), fdt11.ToString());
                    double TKMR = 0;
                    if (dt12.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                        {

                            TKMR += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                            TKMR = (double)Math.Round(TKMR, 2);

                        }

                    }
                    DataTable FuFTo = db.Fuel_fill2(Vno, fdt.ToShortDateString());
                    int fliter = 0;
                    if (FuFTo.Rows.Count != 0)
                    {

                        for (int j = 0; j < FuFTo.Rows.Count; j++)
                        {
                            fliter += Convert.ToInt32(FuFTo.Rows[j][2]);
                        }
                    }
                    dr["Fuel"] = fliter.ToString();
                    dr["Date"] = fdt1.ToShortDateString();
                    dr["KM"] = TKMR.ToString();
                    dt11.Rows.Add(dr);
                    fdt = fdt.AddDays(1);

                }
                else break;
            }
            return dt11;
            //gvreport.DataSource = dt11;
            //gvreport.DataBind();

        }

        public void bindmonthkm12()
        {
            DataTable log = bindmonthkm();
            DataTable first = new DataTable();
            first.Columns.Add("Date", typeof(string));
            first.Columns.Add("km", typeof(string));
            first.Columns.Add("Fuel", typeof(string));
            int count = log.Rows.Count;
            Session["countlog"] = count;
            int N = (int)(count / 3);
            int n = N;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = first.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    first.Rows.Add(dr);
                }
            }
            gvreport.DataSource = first;
            gvreport.DataBind();
            ////gvreport.Attributes.CssStyle .Add("font-size", "3px");
            n = N * 2;
            DataTable second = new DataTable();
            second.Columns.Add("Date", typeof(string));
            second.Columns.Add("km", typeof(string));
            second.Columns.Add("Fuel", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = second.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    //string ts = log.Rows[i][0].ToString();
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    second.Rows.Add(dr);
                }
            }
            gvreport1.DataSource = second;
            gvreport1.DataBind();
            n = N * 3;
            n = n + 1;
            DataTable third = new DataTable();
            third.Columns.Add("Date", typeof(string));
            third.Columns.Add("km", typeof(string));
            third.Columns.Add("Fuel", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = third.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    // string ts = log.Rows[i][0].ToString();
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    third.Rows.Add(dr);
                }
            }
            gvreport2.DataSource = third;
            gvreport2.DataBind();
        }
        public void bindmonthkmold12()
        {
            DataTable log = bindmonthkmold();
            DataTable first = new DataTable();
            first.Columns.Add("Date", typeof(string));
            first.Columns.Add("km", typeof(string));
            first.Columns.Add("Fuel", typeof(string));
            int count = log.Rows.Count;
            Session["countlog"] = count;
            int N = (int)(count / 3);
            int n = N;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                if (i < count)
                {
                    DataRow dr = first.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    first.Rows.Add(dr);
                }
            }
            gvreport.DataSource = first;
            gvreport.DataBind();
            n = N * 2;
            DataTable second = new DataTable();
            second.Columns.Add("Date", typeof(string));
            second.Columns.Add("km", typeof(string));
            second.Columns.Add("Fuel", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = second.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    // string ts = log.Rows[i][0].ToString();
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    second.Rows.Add(dr);
                }
            }
            gvreport1.DataSource = second;
            gvreport1.DataBind();
            n = N * 3;
            n = n + 1;
            DataTable third = new DataTable();
            third.Columns.Add("Date", typeof(string));
            third.Columns.Add("km", typeof(string));
            third.Columns.Add("Fuel", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = third.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    //string ts = log.Rows[i][0].ToString();
                    string ets = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["km"] = ets.ToString();
                    dr["Fuel"] = log.Rows[i][2].ToString();
                    third.Rows.Add(dr);
                }
            }
            gvreport2.DataSource = third;
            gvreport2.DataBind();
        }


        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "" && ddlmonth.SelectedItem.Text != "--select Month--")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                string str1 = ddlmonth.SelectedItem.Text + dt.Year;//+ "_" + fdt.Year;
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VKM_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);

                TableCell cell2 = new TableCell();
                cell2.Controls.Add(gvreport1);
                tr1.Cells.Add(cell2);

                TableCell cell3 = new TableCell();
                cell3.Controls.Add(gvreport2);
                tr1.Cells.Add(cell3);


                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 3;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);




            }
        }
        protected void gvreport1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 3;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport1.Controls[0].Controls.AddAt(0, HeaderGridRow);



            }
        }
        protected void gvreport2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DateTime dt = DateTime.Now;
                int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                string str1 = ddlmonth.SelectedItem.Text + "_" + fdt.Year;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Report of month:   " + str1;
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 3;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport2.Controls[0].Controls.AddAt(0, HeaderGridRow);


            }
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                rcount = 0; fcount = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl1 = e.Row.FindControl("lbltotal") as Label;
                if (lbl1.Text != "&nbsp;" && lbl1.Text != "")
                {
                   
                    rcount += Convert.ToDouble(lbl1.Text);
                }
                Label lbl12 = e.Row.FindControl("lblfuel3") as Label;
                if (lbl12.Text != "&nbsp;" && lbl12.Text != "")
                {
                    fcount += Convert.ToInt32(lbl12.Text);
                }

            }           
          
        }
        protected void gvreport1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                rcount1 = 0; fcount1 = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl1 = e.Row.FindControl("lbltotal") as Label;
                rcount1 += Convert.ToDouble(lbl1.Text);

                Label lbl12 = e.Row.FindControl("lblfuel1") as Label;
                fcount1 += Convert.ToInt32(lbl12.Text);

            }         

        }
        protected void gvreport2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                rcount2 = 0; count = 0; fcount2 = 0; fcounttt = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl1 = e.Row.FindControl("lbltotal") as Label;
                rcount2 += Convert.ToDouble(lbl1.Text);

                Label lbl12 = e.Row.FindControl("lblfuel2") as Label;
                fcount2 += Convert.ToInt32(lbl12.Text);

            }
           
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label;
                count = rcount + rcount1 + rcount2;
                ftotal.Text = count.ToString();

                Label lbltotallFuel = e.Row.FindControl("lbltotallFuel") as Label;
                fcounttt = fcount + fcount1 + fcount2;
                lbltotallFuel.Text = fcounttt.ToString();
            }
          
        }


    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;


namespace Tracking
{
    public partial class GeoTrip : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt, lng;
        static int p = 0;
        static int svalid = 0, evalid = 0;

        static List<string> startpoint = new List<string>();
        static List<string> endpoint = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
            }
            gmapsettings();

        }
        public void gmapsettings()
        {
            //GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
            //GMap1.addControl(extMapType);
            //string sStreetAddress = null;
            //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            //Subgurim.Controles.GeoCode GeoCode = default(Subgurim.Controles.GeoCode);
            //sStreetAddress = "Jaynagar, Bangalore";
            //GeoCode = GMap1.getGeoCodeRequest(sStreetAddress, sMapKey);
            ////double lat = GeoCode.Placemark.coordinates.lat;
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(12.9274529, 77.590597);//GeoCode.Placemark.coordinates.lat, GeoCode.Placemark.coordinates.lng);
            GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
#pragma warning disable CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'
            GMap1.enableScrollWheelZoom = true;
#pragma warning restore CS0618 // 'GMap.enableScrollWheelZoom' is obsolete: 'Use enableHookMouseWheelToZoom instead'

        }

        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Vehicle--");
        }


        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            DataTable dtn = db.getgeotripnof(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
            if (Convert.ToInt32(dtn.Rows[0][0]) >= 2)
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Maximum 2 Circles ONLY');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'

            }
            else
            {
                double lt1 = lt;
                double lng1 = lng;
                if (lt1 == 0 && lng1 == 0)
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point on Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'

                }
                else
                {
                    int gnp = Convert.ToInt32(dtn.Rows[0][0]);
                    int np = 1 + gnp;
                    string Vno = ddlMapTOVehicle.SelectedValue.ToString();
                    if (Vno != "--Select Vehicle--")
                    {
                        DataTable dt1 = db.getdevicesid(Vno);
                        double radius = Convert.ToDouble(txtradius.Text) / 1609.3;
                        string pointname=txtpoint.Text;
                        int did = Convert.ToInt32(dt1.Rows[0][0].ToString());
                        int uid = Convert.ToInt32(Session["UserID"].ToString());
                        db.setgeotrip(lt1, lng1, radius, Vno, uid, did,pointname);
                        binddata();
                        //drawcircle(lt1, lng1, radius, np);
                        //refresh();
                    }
                }
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString()!= "--Select Vehicle--")
            {
                DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
                if (dt.Rows.Count == 2)
                {
                    binddata();
                }
                else
                {
                 
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('Please Set Two Geofences');", true);

                }
            }
        }
        public void refresh()
        {
            p = 0;
            lt = 0; lng = 0;
            svalid = 0; evalid = 0;
            geotripgv.DataBind();
            txtradius.Text = string.Empty;
            txtpoint.Text = string.Empty;

        }

        public void drawcircle(double lt, double lg, double r, int gp)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //GPolygon line = new GPolygon(extp);

#pragma warning disable CS0618 // 'GMap.addPolygon(GPolygon)' is obsolete: 'Use Add(GPolygon) instead.'
            this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
#pragma warning restore CS0618 // 'GMap.addPolygon(GPolygon)' is obsolete: 'Use Add(GPolygon) instead.'
            //Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            //GMap1.setCenter(gLatLng1, 5, Subgurim.Controles.GMapType.GTypes.Normal);
            //PinLetter pinLetter = new PinLetter(gp.ToString(), Color.Red, Color.Black);
            //GMap1.Add(new GMarker(gLatLng1, new GMarkerOptions(new GIcon(pinLetter.ToString(), pinLetter.Shadow()))));

            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 14, Subgurim.Controles.GMapType.GTypes.Normal);
            GMap1.GZoom = 14;
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            //options.title = vn + " Time:" + ct;
            oMarker.options = options;

        }
        
        public void binddata()
        {
           
            startpoint.Clear();
            endpoint.Clear();

            drawcircles();
            DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());

                DataTable dt1 = new DataTable();
                //List<GLatLng> ex = points(Convert.ToDouble(dt.Rows[0][0]), Convert.ToDouble(dt.Rows[0][1]), Convert.ToDouble(dt.Rows[0][2]));
                //List<GLatLng> ex1 = points(Convert.ToDouble(dt.Rows[1][0]), Convert.ToDouble(dt.Rows[1][1]), Convert.ToDouble(dt.Rows[1][2]));

                dt1.Columns.Add("Sno", typeof(string));
                dt1.Columns.Add("Location", typeof(string));
                dt1.Columns.Add("Radius", typeof(string));
                dt1.Columns.Add("Times", typeof(string));
                dt1.Columns.Add("Validate", typeof(string));

                if (dt.Rows.Count != 0 && dt.Rows.Count != 1)
                {
                    DataRow dr = dt1.NewRow();
                    dr["Sno"] = "Start Point";

                    string valid1 = "";
                    string address = "";
                    address = dt.Rows[0][8].ToString();
                    //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                    //GeoCode objAddress = new GeoCode();
                    //objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dt.Rows[0][0]), Convert.ToDouble(dt.Rows[0][1])), sMapKey);

                    //StringBuilder sb = new StringBuilder();
                    //string address = "";
                    //if (objAddress.valid)
                    //{
                    //    sb.Append(objAddress.Placemark.address.ToString());
                    //    address = sb.ToString();
                    //}
                    dr["Location"] = address;

                    double rad = Convert.ToDouble(dt.Rows[0][2]) * 1609.3;
                    double rad12 = Math.Truncate(rad * 100) / 100;
                    double rad1 = Math.Ceiling(rad12);

                    dr["Radius"] = rad1.ToString();

                    DateTime pdate = DateTime.Now;
                    string sdate = pdate.ToShortDateString();
                    string fdate = sdate + " 12:00 AM";
                    string tdate = sdate + " 11:59 PM";
                    //string fdate1 = "";
                    //string valid1;
                    //List<string> startpoint =new List<string>();
                    //List<string> endpoint = new List<string>(); 
                    trip(fdate, tdate);
                    string time1 = "";

                    for (int m = 0; m < startpoint.Count; m++)
                    {
                        time1 += startpoint[m].ToString() + " ";
                    }

                    dr["Times"] = startpoint.Count.ToString();

                    if (svalid != 0)
                    {
                        valid1 = "valid ";// +svalid.ToString();
                    }
                    else { valid1 = "invalid"; }

                    dr["Validate"] = valid1;
                    dt1.Rows.Add(dr);

                    DataRow dr1 = dt1.NewRow();

                    dr1["Sno"] = "End Point";

                    //GeoCode objAddress1 = new GeoCode();
                    //objAddress1 = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dt.Rows[1][0]), Convert.ToDouble(dt.Rows[1][1])), sMapKey);
                    //StringBuilder sb1 = new StringBuilder();
                    //address = "";
                    //if (objAddress1.valid)
                    //{
                    //    sb1.Append(objAddress1.Placemark.address.ToString());
                    //    address = sb1.ToString();
                    //}
                    address = "";
                    address = dt.Rows[1][8].ToString();
                    dr1["Location"] = address;

                    double rad3 = Convert.ToDouble(dt.Rows[1][2]) * 1609.3;
                    double rad31 = Math.Truncate(rad3 * 100) / 100;
                    double rad312 = Math.Ceiling(rad31);

                    dr1["Radius"] = rad312.ToString();
                    valid1 = "";

                    time1 = "";
                    for (int n = 0; n < endpoint.Count; n++)
                    {
                        time1 += endpoint[n].ToString() + " ";
                    }

                    dr1["Times"] = endpoint.Count.ToString();
                    if (evalid != 0)
                    {
                        valid1 = "Valid ";// +evalid.ToString();
                    }
                    else { valid1 = "Invalid"; }
                    dr1["Validate"] = valid1;
                    dt1.Rows.Add(dr1);

                    int nooftrips = 0;
                    if (endpoint.Count != 0 && startpoint.Count != 0)
                    {
                        int x = Convert.ToInt32(startpoint.Count);
                        int y = Convert.ToInt32(endpoint.Count);
                        int min = Math.Min(x, y);
                        nooftrips = min;
                    }


                    lbltrips.Text = nooftrips.ToString();
                    btndelete.Visible = true;
                    btnRefresh.Visible = true;
                   // Btndetails.Visible = true;
                    // refresh();
                }
                else { btndelete.Visible = false; btnRefresh.Visible = false;  }
                geotripgv.DataSource = dt1;
                geotripgv.DataBind();
                refresh();

        }
        public void trip(string fdate, string tdate)
        {
            DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
            List<GLatLng> ex = points(Convert.ToDouble(dt.Rows[0][0]), Convert.ToDouble(dt.Rows[0][1]), Convert.ToDouble(dt.Rows[0][2]));
            List<GLatLng> ex1 = points(Convert.ToDouble(dt.Rows[1][0]), Convert.ToDouble(dt.Rows[1][1]), Convert.ToDouble(dt.Rows[1][2]));

            DataTable dt2 = db.getgeotrack1(Convert.ToString(dt.Rows[0][5]), fdate, tdate);
            string fdate1 = "";
            string fdate2 = "";
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                if (valid == true)
                {
                    fdate1 = Convert.ToString(dt2.Rows[j][2]);
                    fdate = fdate1;
                }
                else
                {
                    if (fdate1 != "")
                    {
                        if (fdate2 == "" || Convert.ToDateTime(fdate1) > Convert.ToDateTime(fdate2))
                        {
                            DateTime dtime = Convert.ToDateTime(fdate1);
                            startpoint.Add(dtime.ToShortTimeString());
                            svalid = svalid + 1;
                            DataTable dt3 = db.getgeotrack1(Convert.ToString(dt.Rows[0][5]), fdate, tdate);
                            fdate1 = "";
                            for (int k = 0; k < dt3.Rows.Count; k++)
                            {
                                bool val = IsPointInPolygon(ex1, Convert.ToDouble(dt3.Rows[k][0]), Convert.ToDouble(dt3.Rows[k][1]));
                                if (val == true)
                                {
                                    fdate1 = Convert.ToString(dt3.Rows[k][2]);
                                    fdate = fdate1;
                                }
                                else
                                {
                                    if (fdate1 != "")
                                    {
                                        DateTime dtime1 = Convert.ToDateTime(fdate1);
                                        evalid = evalid + 1;
                                        endpoint.Add(dtime1.ToShortTimeString());
                                        fdate2 = fdate1;
                                        //trip(fdate, tdate);
                                        fdate1 = "";
                                        break;
                                    }
                                }
                            }
                            if (fdate1 != "")
                            {
                                DateTime dtime1 = Convert.ToDateTime(fdate1);
                                evalid = evalid + 1;
                                endpoint.Add(dtime1.ToShortTimeString());
                                fdate2 = fdate1;
                                fdate1 = "";
                            }
                        }
                    }
                }

            }
            //if (fdate1 != "")
            //{
            //    DateTime dtime = Convert.ToDateTime(fdate1);
            //    startpoint.Add(dtime.ToShortTimeString());
            //    svalid = svalid + 1;
            //    //trip(fdate, tdate);
            //    fdate1 = "";
            //}

        }


        public List<GLatLng> points(double Cx, double Cy, double radius)
        {
            double d2r = Math.PI / 180;   // degrees to radians
            double r2d = 180 / Math.PI;   // radians to degrees
            double earthsradius = 3963; // 3963 is the radius of the earth in miles
            double points = 30;
            double rlat = ((double)radius / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(Cx * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = Cy + (rlng * Math.Cos(theta));
                double ey = Cx + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            return extp;
        }
        private static bool IsPointInPolygon(List<GLatLng> poly, double Lt, double Lg)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].lat <= Lt) && (Lt < poly[j].lat)) |
                    ((poly[j].lat <= Lt) && (Lt < poly[i].lat))) &&
                    (Lg < (poly[j].lng - poly[i].lng) * (Lt - poly[i].lat) / (poly[j].lat - poly[i].lat) + poly[i].lng))
                    c = !c;
            }
            return c;
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "--Select Vehicle--")
            {
                DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
                if (dt.Rows.Count == 2)
                {
                    binddata();
                }
                else
                {
                    binddata();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('Please Set Two Geofences');", true);

                }
            }
            else
            {
                binddata();

            }
        }
        public void drawcircles()
        {
            GMap1.resetPolygon();
            GMap1.resetMarkers();
            DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int j = 1 + i;
                drawcircle(Convert.ToDouble(dt.Rows[i][0].ToString()), Convert.ToDouble(dt.Rows[i][1].ToString()), Convert.ToDouble(dt.Rows[i][2].ToString()), j);
            }
        
        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "--Select Vehicle--")
            {
                db.Deletetrip(ddlMapTOVehicle.SelectedValue.ToString());
                binddata();
            }
        }
        public void Triptimes(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            GridViewRow gvrow = btn.NamingContainer as GridViewRow;
            string userid = geotripgv.DataKeys[gvrow.RowIndex].Value.ToString();
            if (userid == "Start Point")
            {
                string [] triptimes= new string[startpoint.Count];
                for (int i = 0; i < startpoint.Count; i++)
                {
                    triptimes[i] = "Trip Time :" + startpoint[i].ToString();
                }
                if (startpoint.Count== 0)
                {
                    triptimes = new string[1];
                    triptimes[0] = "No Trip";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", triptimes) + "'.split(','));", true);
                drawcircles();
            }
            if (userid == "End Point")
            {
                string[] triptimes = new string[endpoint.Count];
                for (int i = 0; i < endpoint.Count; i++)
                {
                    triptimes[i] = "Trip Time :" + endpoint[i].ToString();
                }
                if (endpoint.Count == 0)
                {
                    triptimes = new string[1];
                    triptimes[0] = "No Trip";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", triptimes) + "'.split(','));", true);
                drawcircles();
            }
            
        }
        protected void Btndetails_Click(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(startpoint.Count);
            int y = Convert.ToInt32(endpoint.Count);
            int min = Math.Min(x, y);

            DataTable dttrip = new DataTable();
            dttrip.Columns.Add("Trip", typeof(string));
            dttrip.Columns.Add("time1", typeof(string));
            dttrip.Columns.Add("time2", typeof(string));
            dttrip.Columns.Add("totaltime", typeof(string));
            for (int i = 0, j = 1; i < min; i++)
            {
                DataRow dr = dttrip.NewRow();
                dr["Trip"] = j.ToString();
                dr["time1"] = startpoint[i].ToString();
                dr["time2"] = endpoint[i].ToString();
                TimeSpan ts = Convert.ToDateTime(endpoint[i].ToString()) - Convert.ToDateTime(startpoint[i].ToString());
                string str = ts.ToString();
                string retString = str.Substring(0, 5);
                dr["totaltime"] = retString;
                dttrip.Rows.Add(dr);
                j++;

            }

            Session["dttable"] = dttrip;
            Session["vehicleno"] = ddlMapTOVehicle.SelectedValue.ToString();
            Response.Redirect("GeoTripdetails.aspx");

        }

        
    }
}
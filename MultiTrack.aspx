﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiTrack.aspx.cs" Inherits="Tracking.MultiTrack"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content1" runat="server">
    <script language="javascript" type="text/javascript">
        function alertsdetails(x) {
            alert(x.join('\n'));
        }
    </script>
    <script type="text/javascript">
        var TotalChkBx;
        var Counter;

        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= this.GVehicles.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.GVehicles.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
    </script>
    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="120000">
    </asp:Timer>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <div id="desktopTitlebar">
                <table id="pnlToolbar" style="height: 20px; width: 100%; background-color: #dcdcdc;"
                    border="1">
                    <tbody>
                        <tr>
                            <td style="padding-right: 10px; vertical-align: middle; text-align: center;">
                                <asp:Label ID="lblview" runat="server" Text="Multiple Vehicles Locations View" Font-Size="18px"
                                    ForeColor="Black"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px;">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Image ID="imggreen" runat="server" ImageUrl="http://maps.google.com/mapfiles/ms/micons/green.png"
                                    Height="16px" />Engine Status On&nbsp;&nbsp;
                                <asp:Image ID="imgred" runat="server" ImageUrl="http://maps.google.com/mapfiles/ms/micons/red.png"
                                    Height="16px" />Engine Status Off&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Image ID="Image1" runat="server" ImageUrl="http://maps.google.com/mapfiles/ms/micons/blue.png"
                                    Height="16px" />Engine Status Idle
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table style="border-collapse: collapse; height: 700px;" border="0" bordercolor="#111111"
                cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <cc1:GMap ID="GMap1" runat="server" Height="750px" Width="100%" />
                        </td>
                        <td width="30%">
                            <table style="border-collapse: collapse; height: 750px;" border="0" cellpadding="0"
                                cellspacing="0" width="100%">
                                <tbody>
                                    <tr valign="top" style="background-color: Gray; height: 100%;">
                                        <td width="100%" valign="top">
                                            <table width="100%" border="2">
                                                <tr>
                                                    <td style="height: 25px;">
                                                        &nbsp;
                                                        <asp:CheckBox ID="chkall" runat="server" Text="Select ALL" Font-Bold="true" AutoPostBack="true"
                                                            OnCheckedChanged="chkall_CheckedChanged" />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Button ID="btnLocation" runat="server" Text="Locations Details" OnClick="btnLocation_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GVehicles" runat="server" Width="100%" AutoGenerateColumns="false"
                                                            DataKeyNames="VehicalNumber" BorderWidth="2" BorderColor="Black" HeaderStyle-BackColor="Green"
                                                            HeaderStyle-Font-Bold="true" HeaderStyle-BorderWidth="2" HeaderStyle-BorderColor="Black"
                                                            OnRowDataBound="GVehicles_RowDataBound" 
                                                            onrowcreated="GVehicles_RowCreated">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="change" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Vehicle RTO No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblvehicleno" runat="server" Text='<%# Bind("VehicalNumber") %>' Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>' Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Unit_Serial No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblserial" runat="server" Text='<%# Bind("serialno") %>' Font-Bold="true"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

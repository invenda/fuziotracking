﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Admin
{
    public partial class CreateVehicles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
                    TrackingMaster tm = (TrackingMaster)Page.Master;
                    tm.MenuVisible = true;
                }
                else
                {
                    TrackingMaster tm = (TrackingMaster)Page.Master;
                    tm.MenuVisible = false;
                }
            }
        }
        protected void btSubmit_Click(object sender, EventArgs e)
        {
            DBClass db = new DBClass();
            db.CreateVehicle(txtVehicleNo.Text, Session["UserID"].ToString());
            txtVehicleNo.Text = string.Empty;           
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Vehicle Created Successfully');</script>");
        }
        protected void btClear_click(object sender, EventArgs e)
        {
            txtVehicleNo.Text = string.Empty;
                     
        }

        protected void submit_Click(object sender, EventArgs e)
        {

        }
    }
}

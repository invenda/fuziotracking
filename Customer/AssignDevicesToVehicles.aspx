﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignDevicesToVehicles.aspx.cs"
    Inherits="Tracking.Customer.AssignDevicesToVehicles" MasterPageFile="~/TrackingMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="sublogo">
            <h1>
                <a href="#">Assign Devices To Vehicles</a>
            </h1>
        </div>
    </div>
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <div>
        <%--<asp:UpdatePanel ID="updatepanel1" runat="server">
            <ContentTemplate>--%>
                <div style="padding-left: 75px;">
                    <table>
                        <tr>
                            <td colspan="2">
                                <table border="2">
                                    <tr>
                                        <td style="color: White; font-size: 16px; font-family: Cambria;">
                                            <asp:Label ID="lbldevices" Text="SelectDevices" runat="server"></asp:Label>
                                        </td>
                                        <td style="color: White; font-size: 16px; font-family: Cambria;">
                                            <asp:Label ID="Label1" Text="Map To Vehicle" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Width="75px" ID="ddlDevices" runat="server" DataValueField="Device_ID"
                                                DataTextField="Device_ID">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList Width="75px" ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicle_number"
                                                DataTextField="Vehicle_number">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btMap" runat="server" Text="Map" OnClick="btMap_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btReset" runat="server" Text="Reset" />
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top:50px;">
                        <table>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="gdVehicles" runat="server" Width="100%" AutoGenerateColumns="False"
                                        DataKeyNames="ID" OnEditCommand="onEditCommand_Click" EnableViewState="true"   >
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="125px"  HeaderText="Vehical No." HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Medium">
                                                <ItemTemplate>
                                                    <asp:Label ID="vehical" Text='<%# Bind("VehicalNumber") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn  HeaderStyle-Width="125px" HeaderText="Device ID" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Medium">
                                                <ItemTemplate>
                                                    <asp:Label ID="device" Text='<%# Bind("Device_ID") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>                                            
                                            
                                            <asp:TemplateColumn>
                                            <ItemTemplate>
                                            <asp:Button ID="lbtndelete" Text="Delete" ToolTip='<%# Bind("ID") %>' runat="server" OnClick="lbtndelete_click"></asp:Button>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                                
                            </tr>
                        </table>
                    </div>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>

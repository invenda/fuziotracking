﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateVehicles.aspx.cs" Inherits="Tracking.Admin.CreateVehicles" 
MasterPageFile="~/TrackingMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <div >
        <div id="sublogo">
            <h1>
                <a href="#">Create Vehicles</a>
            </h1>               
        </div>
    </div>
    <div>
     <table>
            <tr>
                <td>
                    <asp:Label ID="lblDevice" Text="Vehicle Number" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtVehicleNo" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfusername" runat="server" 
                        ControlToValidate="txtVehicleNo" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>               
                   <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" />
                </td>
                <td>
                   <asp:Button ID="btClear" runat="server" Text="Clear" OnClick="btClear_click" />
                </td>
            </tr>
            </table>
    </div>
   
</asp:Content>

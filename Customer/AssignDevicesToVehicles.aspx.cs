﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Customer
{
    public partial class AssignDevicesToVehicles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
                    TrackingMaster tm = (TrackingMaster)Page.Master;
                    tm.MenuVisible = true;

                    DBClass db = new DBClass();
                    if (Session["UserID"] != null)
                    {
                        ddlDevices.DataSource = db.GetAllDevicesForCustomer(Session["UserID"].ToString());
                        ddlDevices.DataBind();

                        ddlMapTOVehicle.DataSource = db.GetUnAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();

                        gdVehicles.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        gdVehicles.DataBind();
                    }
                }
            }
        }
        protected void lbtndelete_click(object sender, EventArgs e)
        {
            DBClass dbclass = new DBClass();
            Button ltb = (Button)sender;
            dbclass.DeleteVehicleDeviceRel(ltb.ToolTip);

            ddlDevices.DataSource = dbclass.GetAllDevicesForCustomer(Session["UserID"].ToString());
            ddlDevices.DataBind();

            ddlMapTOVehicle.DataSource = dbclass.GetUnAssingedVehicles(Session["UserID"].ToString());
            ddlMapTOVehicle.DataBind();

            gdVehicles.DataSource = dbclass.GetAssingedVehicles(Session["UserID"].ToString());
            gdVehicles.DataBind();
        }
        protected void onEditCommand_Click(object sender, DataGridCommandEventArgs e)
        {

        }
        protected void btMap_Click(object sender, EventArgs e)
        {
            DBClass db = new DBClass();
            db.AssignVehicle(ddlDevices.SelectedItem.Value, ddlMapTOVehicle.SelectedItem.Value, Session["UserID"].ToString());

            gdVehicles.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
            gdVehicles.DataBind();

            ddlDevices.DataSource = db.GetAllDevicesForCustomer(Session["UserID"].ToString());
            ddlDevices.DataBind();

            ddlMapTOVehicle.DataSource = db.GetUnAssingedVehicles(Session["UserID"].ToString());
            ddlMapTOVehicle.DataBind();
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Corrections : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"].ToString());
                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
 
 
                        DBClass db = new DBClass();
 
 
                        //string RegNUM = Session["RegNo"].ToString();
                        //lblVehNumber.Text = RegNUM;
                        if (Session["UserRole"].ToString() == "3")
                        {
                            ddlVeh.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                            ddlVeh.DataBind();
                            //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                            //ddlVeh.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                            //ddlVeh.DataBind();
                        }
                        else
                        {
                            ddlVeh.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                            ddlVeh.DataBind();
                        }  
                        lblCorrectDT.Text = Convert.ToString(DateTime.Now);
                        dgCorrect.Visible = true;

                        DataTable Co = db.Cor(ddlVeh.SelectedValue.ToString());//(Session["RegNo"].ToString());
                        Co.AcceptChanges();
                        dgCorrect.DataSource = Co;
                        dgCorrect.DataBind();                     

                    }
                }
            }
            if (IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                //ddlVeh.DataSource = db.GetAssingedVehicles(Session["ParentUser"].ToString());
                //ddlVeh.DataBind();
                lblCorrectDT.Text = Convert.ToString(DateTime.Now);
                dgCorrect.Visible = true;


                DataTable Co = db.Cor(ddlVeh.SelectedValue.ToString());//(Session["RegNo"].ToString());
                Co.AcceptChanges();
                dgCorrect.DataSource = Co;
                dgCorrect.DataBind();
            }
        }

        public void btnSub_Click(object sender, EventArgs e)
        {

 
 
            DBClass db = new DBClass();
 
 

            string VehNo = ddlVeh.SelectedValue.ToString();//lblVehNumber.Text;
            string CDT = lblCorrectDT.Text;
            string CV = txtCorrection.Text;
            int cfv = Convert.ToInt32(CV);
            string t;
            //if (rbtnPOS.Checked)
            if (rbtnTypeOfCor.SelectedValue == "1")
            {
                t = "Positive Correction";
            }
            else
            {
                t = "Negative Correction";
                cfv = cfv / -1;
            }
            DataTable CC = db.CCor(ddlVeh.SelectedValue.ToString());
            if (CC.Rows.Count <= 0)
            {
                db.Correct(Session["UserID"].ToString(), VehNo, CDT, t, CV, cfv.ToString());
            }
            else if (CC.Rows.Count != 0 && rbtnTypeOfCor.SelectedValue == "1")//rbtnPOS.Checked)
            {
                DataTable APC = db.APrevCor(ddlVeh.SelectedValue.ToString());
                double PrevCor = Convert.ToDouble(APC.Rows[0][2].ToString());
                double CurCor = Convert.ToDouble(CV);
                double CumCor = PrevCor + CurCor;
                db.CorrectCumu(Session["UserID"].ToString(), VehNo, CDT, t, CV, CumCor, cfv.ToString());
            }
            else if (CC.Rows.Count != 0 && rbtnTypeOfCor.SelectedValue == "2")//rbtnNEG.Checked)
            {
                DataTable APC = db.APrevCor(ddlVeh.SelectedValue.ToString());
                double PrevCor = Convert.ToDouble(APC.Rows[0][2].ToString());
                double CurCor = Convert.ToDouble(CV);
                double CumCor = PrevCor - CurCor;
                db.CorrectCumu(Session["UserID"].ToString(), VehNo, CDT, t, CV, CumCor, cfv.ToString());
            }
            txtCorrection.Text = "";
            rbtnTypeOfCor.ClearSelection();
            //rbtnNEG.Checked = false;
            //rbtnPOS.Checked = false;
            //DataTable dt = new DataTable();
            //dt.Columns.Add("CORRECTION_VEH", typeof(string));
            //dt.Columns.Add("CORRECTION_DT", typeof(string));
            //dt.Columns.Add("CORRECTION_Value", typeof(string));
            //dt.Columns.Add("CORRECTION_TYPE", typeof(string));
            //DataRow DR = dt.NewRow();
            //DR["CORRECTION_VEH"] = VehNo;
            //DR["CORRECTION_DT"] = CDT;
            //DR["CORRECTION_Value"] = CV;
            //DR["CORRECTION_TYPE"] = Type;

            //dt.Rows.Add(DR);
            //dgCorrect.DataSource = dt;
            //dgCorrect.DataBind();
            //DBClass db = new DBClass();
            DataTable Co = db.Cor(ddlVeh.SelectedValue.ToString());//(Session["RegNo"].ToString());
            Co.AcceptChanges();
            dgCorrect.DataSource = Co;
            dgCorrect.DataBind();
            //btnSub.Enabled = false;

            double correction = Convert.ToDouble(CV.ToString());
            DataTable existingMR = db.EMR(ddlVeh.SelectedValue.ToString());//(Session["RegNo"].ToString());
            double EMR = Convert.ToDouble(existingMR.Rows[0][0].ToString());
#pragma warning disable CS0168 // The variable 'sign' is declared but never used
            string sign;
#pragma warning restore CS0168 // The variable 'sign' is declared but never used
            if (rbtnTypeOfCor.SelectedValue == "1")//(rbtnPOS.Checked)
            {
                double NowEMR = EMR + correction;
                string N = NowEMR.ToString();
                db.UpdateNowEMR(Session["UserID"].ToString(), VehNo, CDT, N);
            }
            else if (rbtnTypeOfCor.SelectedValue == "2")//(rbtnNEG.Checked)
            {
                DataTable NwEMR = db.SelectNowEMR(Session["UserID"].ToString(), VehNo);
                if (NwEMR.Rows[0][0].ToString() == "")
                {
                    DataTable MTR = db.MTR(ddlVeh.SelectedValue.ToString());

                    double NEMR = Convert.ToDouble(MTR.Rows[0][0].ToString()) - correction;
                    string N = NEMR.ToString();
                    db.UpdateNowEMR(Session["UserID"].ToString(), VehNo, CDT, N);
                }

                else
                {
                    double nw = Convert.ToDouble(NwEMR.Rows[0][0].ToString());
                    double NEMR = nw - correction;
                    string N = NEMR.ToString();
                    db.UpdateNowEMR(Session["UserID"].ToString(), VehNo, CDT, N);
                }


            }
        }
        //protected void BtnRefresh_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect(Request.RawUrl);
        //}
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
namespace Tracking
{
    public partial class Plotroutebasispoints : System.Web.UI.Page
    {
          
        static double lt = 0, lng = 0;
        static int p = 0;
        static int sno = 0;
        static string rname = "";       
           
        protected void Page_Load(object sender, EventArgs e)
        {
            DBClass db = new DBClass();
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
            if (!IsPostBack)
            {
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;

                ddlrno.DataSource = db.get_Geo_RouteNo2(Session["userID"].ToString());
                ddlrno.DataBind();
            }

        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


namespace Tracking
{
    
    public class Handler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["Connectionstring"].ToString());
                SqlCommand cmd = new SqlCommand("select UserImage from Users where ID=@EmpID", con);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@EmpID", context.Request.QueryString["EmpID"].ToString());
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows && dr.Read())
                {
                    context.Response.BinaryWrite((byte[])(dr["UserImage"]));
                }

                con.Close();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

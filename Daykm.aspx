<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Daykm.aspx.cs" Inherits="Tracking.Daykm"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" language="javascript">
        function CheckCheck() {
            var chkBoxList = document.getElementById('<%=GVehicles.ClientID %>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");

            var i = 0;
            var tot = 0;
            for (i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    tot = tot + 1;
                }
            }

            if (tot > 3) {

                alert('Cannot select  more than 3 Vehicles');
                chkBoxCount[0].checked = false;
                chkBoxCount[1].checked = false;
                chkBoxCount[2].checked = false;
                chkBoxCount[3].checked = false;
                chkBoxCount[4].checked = false;
                chkBoxCount[5].checked = false;
                chkBoxCount[6].checked = false;
                chkBoxCount[7].checked = false;
                chkBoxCount[8].checked = false;
                chkBoxCount[9].checked = false;
                chkBoxCount[10].checked = false;
                chkBoxCount[11].checked = false;
                chkBoxCount[12].checked = false;
                chkBoxCount[13].checked = false;
                chkBoxCount[14].checked = false;
                chkBoxCount[15].checked = false;
                chkBoxCount[16].checked = false;
                chkBoxCount[17].checked = false;
                chkBoxCount[18].checked = false;
                chkBoxCount[19].checked = false;

            }
        }
    </script>

    <script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > yesday) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = yesday;
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 90%; height: auto; font-size: 20px; text-align: center; color: Blue;">
        Vehicle's Day KM Report
    </div>
    <div style="width: 90%; margin: 0 auto; padding: 0; padding: 0;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="width: 90%; margin: 0 auto; padding: 0; padding: 0;">
                    <table align="center" style="width: 120%;">
                        <tr align="right">
                            <td width="14%">
                                <asp:GridView ID="GVehicles" runat="server" Width="100%" AutoGenerateColumns="false"
                                    DataKeyNames="VehicalNumber" BorderWidth="2" BorderColor="Black" HeaderStyle-BackColor="Gray"
                                    HeaderStyle-Font-Bold="true" HeaderStyle-BorderWidth="2" HeaderStyle-BorderColor="Black"
                                    ValidationGroup="Group1">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" AutoPostBack="true" runat="server" onclick="javascript:CheckCheck();" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Select Max 3 vehicles">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvehicleno" runat="server" Text='<%# Bind("VehicalNumber") %>' Font-Bold="true"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="Gray" BorderColor="Black" BorderWidth="2px" Font-Bold="True">
                                    </HeaderStyle>
                                </asp:GridView>
                            </td>
                            <td align="right" style="width: 110px;">
                                <asp:Label ID="Label2" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                                &nbsp;:
                            </td>
                            <td align="left" style="width: 300px;">
                                <asp:TextBox ID="txtdate" runat="server" Width="175px"></asp:TextBox>
                                <cc1:CalendarExtender ID="ce1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtdate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                    ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 120px;">
                                <asp:Button ID="btnsubmit" runat="server" Text="Get Report" ValidationGroup="Group1"
                                    OnClick="btnsubmit_Click" />
                            </td>
                            <td style="width: 120px;">
                                <asp:Button ID="btndownload" runat="server" Text="DownLoad" Visible="false" />
                            </td>
                            <td align="left">
                                <asp:Button ID="btnclear" runat="server" Text="Clear" Width="60px" OnClick="btnclear_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 20px;">
                </div>
                <div style="height: auto; width: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <h1>
                                            <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                        </h1>
                                    </td>
                                    <td>
                                        <h3>
                                            Please wait.....
                                        </h3>
                                    </td>
                                </tr>
                            </table>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div>
                    <table style="border-collapse: collapse; height: auto;" border="0" cellpadding="0"
                        cellspacing="0" width="90%">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                        RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black"
                                        ShowFooter="true" OnRowDataBound="gvreport_RowDataBound" Width="100%">
                                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                            BorderWidth="0" />
                                        <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="VehicalNumber" HeaderStyle-Width="70px" FooterStyle-BorderColor="#61A6F8"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Vehicle_number") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="1" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH1" runat="server" Text='<% #Bind("1Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="2" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH2" runat="server" Text='<% #Bind("2Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="3" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH3" runat="server" Text='<% #Bind("3Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="4" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH4" runat="server" Text='<% #Bind("4Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="5" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH5" runat="server" Text='<% #Bind("5Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="6" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH6" runat="server" Text='<% #Bind("6Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="7" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH7" runat="server" Text='<% #Bind("7Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="8" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH8" runat="server" Text='<% #Bind("8Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="9" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH9" runat="server" Text='<% #Bind("9Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="10" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH10" runat="server" Text='<% #Bind("10Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="11" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH11" runat="server" Text='<% #Bind("11Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="12" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH12" runat="server" Text='<% #Bind("12Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="13" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH13" runat="server" Text='<% #Bind("13Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="14" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH14" runat="server" Text='<% #Bind("14Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="15" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH15" runat="server" Text='<% #Bind("15Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="16" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH16" runat="server" Text='<% #Bind("16Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="17" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH17" runat="server" Text='<% #Bind("17Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="18" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH18" runat="server" Text='<% #Bind("18Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="19" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH19" runat="server" Text='<% #Bind("19Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="20" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH20" runat="server" Text='<% #Bind("20Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="21" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH21" runat="server" Text='<% #Bind("21Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="22" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH22" runat="server" Text='<% #Bind("22Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="23" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                                                FooterStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH23" runat="server" Text='<% #Bind("23Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblf1" runat="server" Text=""></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="24" HeaderStyle-Width="35px" FooterStyle-HorizontalAlign="Left"
                                                FooterStyle-BorderColor="#61A6F8">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblH24" runat="server" Text='<% #Bind("24Hour") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblf2" runat="server" Text="Total"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Run KM" HeaderStyle-Width="120px" HeaderStyle-BackColor="#8FBC8F"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-BackColor="#8FBC8F" FooterStyle-BackColor="#8FBC8F"
                                                ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltotal" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ValidationGroup="Group1" ShowSummary="False" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btndownload" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div style="height: 20px;">
    </div>
    <div>
    </div>
    <div style="height: 20px;">
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCustVeh.aspx.cs" Inherits="Tracking.ViewCustVeh"
    MasterPageFile="~/ESMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
function ConfirmationBox(username) {

var result = confirm('Are you sure you want to Unmap '+username+' Details' );
if (result) {

return true;
}
else {
return false;
}
}
    </script>

    <div style="width: 100%; height: 100%;">
        <div id="sublogo" style="text-align: center;">
            <h1>
                View/Edit Vehicles
            </h1>
        </div>
        <%--</div>
    <div>--%>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
                <td style="font-size: large; color: Black;">
                    Mapped Vehicles
                </td>
                <td colspan="3">
                    &nbsp;
                </td>
                <td style="font-size: large; color: Black;" align="left">
                    Unmapped Vehicles
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
                <td align="left" colspan="4">
                    <asp:DataGrid ID="dgViewVeh" runat="server" Width="50%" AllowPaging="false" AutoGenerateColumns="False"
                        CellPadding="3" Visible="true">
                        <Columns>
                            <asp:TemplateColumn HeaderText="Vehical Number" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#0099ff"
                                HeaderStyle-ForeColor="Black">
                                <ItemTemplate>
                                    <asp:LinkButton ID="vehicleno" Text='<%# Bind("Vehicle_number") %>' runat="server"
                                        OnClick="vehicleno_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#0099ff"
                                HeaderStyle-ForeColor="Black">
                                <ItemTemplate>
                                    <asp:Label ID="label1" Text='<%# Bind("Status") %>' runat="server" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
                <td>
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" Width="50%"
                        DataKeyNames="Vehicle_number" OnRowDataBound="gvrecords_RowDataBound" HeaderStyle-BackColor="#0099ff"
                        HeaderStyle-ForeColor="Black" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="Vehicle_number" HeaderText="Vehical Number" />
                            <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-ForeColor="Red" />
                            <asp:TemplateField HeaderText="Delete Entries">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <div style="width: 100%; height: 50px;">
        </div>
    </div>
</asp:Content>

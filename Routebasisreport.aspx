﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Routebasisreport.aspx.cs"
    Inherits="Tracking.Routebasisreport" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpalceholder1" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
        var TotalChkBx;
        var Counter;

        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= this.gvrecords.Rows.Count %>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= this.gvrecords.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target base & child control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter;            
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }

        function validateCheckBoxes() 
        {
            var isValid = false;
            var gridView = document.getElementById('<%= gvrecords.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++){
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null){
                    if (inputs[0].type == "checkbox"){
                         if (inputs[0].checked){
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600"
        AsyncPostBackErrorMessage="Please Ttry agaian">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <div style="text-align: center; color: Blue;">
            <h2>
                Route Basis Reports
            </h2>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 950px; margin: 0 auto; padding: 0; padding: 0;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label1" Text="Select Route Type :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlrtype"
                                    ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                            </td>
                            <td style="width: 150px; text-align: left;">
                                <asp:DropDownList ID="ddlrtype" runat="server" AutoPostBack="true">
                                    <%-- <asp:ListItem Text="--" Value="--"></asp:ListItem>--%>
                                    <asp:ListItem Text="ROUTE BASIS" Value="4"></asp:ListItem>
                                    <%-- <asp:ListItem Text="GEO FENCE" Value="4"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Label ID="Label4" Text="Select Date :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                    ValidationGroup="Group1" ErrorMessage="Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtdate" runat="server" AutoPostBack="true" OnTextChanged="txtdate_TextChanged"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtdate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="center">
                                <asp:Button ID="btMap" runat="server" Text="Submit" ValidationGroup="Group1" OnClick="btMap_Click" OnClientClick ="javascript:validateCheckBoxes()" />
                                  <%--  Width="100px" />--%>

                                      <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Validate"
                                    ValidationGroup="Group1" ErrorMessage="Select one">*</asp:RequiredFieldValidator>--%>
                            </td>
                            <td align="center">
                                <asp:Button ID="Btn_Download" runat="server" Text="Download" ValidationGroup="Group1" Enabled ="false"
                                    Width="100px" OnClick="Btn_Download_Click" />
                            </td>
                            <td align="center">
                                <asp:Button ID="Btn_Clear" runat="server" Text="Clear" OnClick="Btn_Clear_Click"
                                    Width="100px" />
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="height: 20px; text-align: center;">
                        <asp:Label ID="lblnorec" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </div>
                    <div>
                        <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" HorizontalAlign="Center"
                            DataKeyNames="Vehicle_number" HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black"
                            RowStyle-ForeColor="Black" OnRowCreated="gvrecords_RowCreated">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkBxSelect" runat="server" Enabled="false" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkBxHeader" onclick="javascript:HeaderClick(this);" runat="server"
                                            Enabled="false" Text="Select All" />
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vehical Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSvno" runat="server" Text='<% #Bind("Vehicle_number")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Route_No" HeaderText="Route ID" />
                                <asp:BoundField DataField="Route_Name" HeaderText="Route Name" />
                                <asp:BoundField DataField="Route_Assigned_Date" HeaderText="Assigned Date" />
                            </Columns>
                        </asp:GridView>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one record." ClientValidationFunction="Validate" ForeColor="Red"></asp:CustomValidator>
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: 20px;">
                    </div>
                    <div>
                        <asp:Panel ID="Panel1" runat="server" Visible="false" Width="100%">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black" RowStyle-ForeColor="Black"
                                OnRowDataBound="GridView1_RowDataBound" OnRowCreated="GridView1_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="SNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsno" runat="server" Text='<% #Bind("Sno") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vehicle No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVno" runat="server" Text='<% #Bind("Vno") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Route No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRno" runat="server" Text='<% #Bind("RouteNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No Trips" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbnt" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Geo Points">
                                        <ItemTemplate>
                                            <asp:GridView ID="gvchild" runat="server" AutoGenerateColumns="false" RowStyle-ForeColor="Black"
                                                OnRowDataBound="gvchild_RowDataBound" Width="100%" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderStyle Font-Bold="true" ForeColor="Black" Font-Size="12px" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsn" runat="server" Text='<% #Bind("Sno") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblpname" runat="server" Text='<% #Bind("PointName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="In Time" HeaderStyle-Width="70px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbltime" runat="server" Text='<% #Bind("Time") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Out Time" HeaderStyle-Width="70px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbloutt" runat="server" Text='<% #Bind("OTime") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="No's Times" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblnot" runat="server" Text='<% #Bind("notimes") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="80px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblstatus" runat="server" Text='<% #Bind("Status") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Btn_Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

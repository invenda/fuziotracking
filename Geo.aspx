﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Tracking.Admin.Geo" MasterPageFile="~/ESMaster.Master"
    CodeBehind="Geo.aspx.cs" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Delete The Point ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }         
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <div>
            <input id="inpHide" type="hidden" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="circleinfo" ShowSummary="False" />
        </div>
        <table style="width:100%; height: auto;">
            <tr style="width: 100%; height: auto;">
                <td>
                    <table style="background-color: #F7F7F6; width: 100%; height: auto;" border="1">
                        <tr style="height: auto;">
                            <td>
                                <asp:Label ID="lblVehicle" Text="Select Vehicle:" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblVehEngStat" Text="Circle Radius" runat="server"></asp:Label>
                            </td>
                            <td>
                                Select Date:
                            </td>
                            <td>
                                In Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rbtnAMPM"
                                    ErrorMessage="Select FROM Hours:Minutes and AM/PM" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                Out Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbtnDT"
                                    ErrorMessage="Select TO Hours:Minutes and AM/PM" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Button ID="btrefresh" Text="Refresh" runat="server" Width="100px" OnClick="btrefresh_Click" />
                            </td>
                        </tr>
                        <tr style="height: auto;">
                            <td>
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    AutoPostBack="true" DataTextField="Vehicalnumber" Width="150px" OnDataBound="ddlMapTOVehicle_DataBound"
                                    OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtradius" runat="server" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtradius"
                                    ErrorMessage="Enter radius" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                                (Mtrs)
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" Width="150px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="TextBox1">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                    ErrorMessage="Select the Start Date" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="40px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlToHOURS" runat="server" Width="40px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:Button ID="btView" runat="server" Text="SetGeoFence" OnClick="btView_Click1"
                                    Width="100px" ValidationGroup="circleinfo" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: auto;">
                <td>
                    <table border="2">
                        <tr>
                            <td>
                                <cc1:GMap ID="GMap1" runat="server" Width="1250px" enableServerEvents="true" Height="400px"
                                    OnClick="GMap1_Click1" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="width: 1250px; height: auto;">
                <td>
                    <div style="text-align: center;">
                        <asp:GridView ID="gvrecord" runat="server" AutoGenerateColumns="false" Width="100%"
                            DataKeyNames="ID" OnRowDataBound="gvrecord_RowDataBound" RowStyle-ForeColor="Black">
                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                            <Columns>
                                <asp:TemplateField Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" Text='<% #Eval("ID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SNo" DataField="sno" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Location" DataField="loc" ItemStyle-Font-Size="10pt"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="550px" ItemStyle-Height="10px">
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Radius(mtr)" DataField="radius" ItemStyle-Font-Size="10pt"
                                    ItemStyle-Height="10px"></asp:BoundField>
                                <asp:BoundField HeaderText="Date" DataField="date" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Time" DataField="time" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Image ID="imgval" runat="server" Height="14px" Width="14px" />
                                        <asp:Label ID="lblval" runat="server" Text='<% #Eval("validate")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remove" ItemStyle-Height="10px">
                                    <ItemTemplate>
                                        <asp:Button ID="remove" runat="server" Text="Delete" AutoPostBack="true" OnClientClick="ConfirmIt()"
                                            OnClick="changed" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

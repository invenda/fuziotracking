﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewVehDetails.aspx.cs"
    Inherits="Tracking.ViewVehDetails" MasterPageFile="~/ESMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <%--    <script type="text/javascript">
function closeWindow()
{
 var windowObject = window.self;

 windowObject.opener = window.self;
 windowObject.close();
}
    </script>--%>
    <div>
        <div style="text-align:center;" >
            <h1>
                <a>View Vehicle Registration Details</a>
            </h1>
        </div>
    </div>
    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
        <table width="100%">            
            <tr>
                <td style="color:Black; font-size: large; width:200px;">
                    Vehicle Details
                </td>
                <td style="color:Black; font-size: large; width:300px;">
                </td>
                <td style="color: Black; font-size: large; width:200px;">
                    Driver Details
                </td>
                <td>
                </td>
            </tr>           
            <tr>
                <td>
                    Vehicle Registration Number
                </td>
                <td>
                    :<asp:Label ID="lblVehRegNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driver Name:&nbsp;
                </td>
                <td>
                    :<asp:Label ID="lblDriName" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Model</td>
                <td>
                    :<asp:Label ID="lblVehModel" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driver Mobile Number:
                </td>
                <td>
                    :<asp:Label ID="lblDriMobNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Make</td>
                <td>
                    :<asp:Label ID="lblVehMake" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Address:
                </td>
                <td>
                    :<asp:Label ID="lblDriAddress" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Insurance Number
                </td>
                <td>
                    :<asp:Label ID="lblVehInsNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driving Licence Number:
                </td>
                <td>
                    :<asp:Label ID="lblDriLicNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RTO Devision/Name</td>
                <td>
                    :<asp:Label ID="lblrtoname" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Tank Capacity in Liters&nbsp;
                </td>
                <td>
                    :<asp:Label ID="lblTankCap" runat="server" Text="Label"></asp:Label>
                </td>
                <td style="color: Black; font-size: large">
                    Alerts
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Battery Voltage Capacity
                </td>
                <td>
                    :<asp:Label ID="lblBatVolt" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Vehicle Insurance Expiry Date:
                </td>
                <td>
                    <asp:Label ID="lblVehInsExpDt" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Existing Meter Reading</td>
                <td>
                    :<asp:Label ID="lblExistMetRead" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    RTO Permit Renewal Date:
                </td>
                <td>
                    <asp:Label ID="lblrtodate" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Kilometer Pulse
                </td>
                <td>
                    :<asp:Label ID="lblKiloPulse" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driving Licence Expiry Date:
                </td>
                <td>
                    <asp:Label ID="lblDriLicExpDt" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <%--</table>
        <table width="100%">--%>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    RTO (FC-Renewal Date):
                </td>
                <td>
                    <asp:Label ID="lblfcdate" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    Emission Check up to:
                </td>
                <td>
                    <asp:Label ID="lblemsdate" runat="server" Text="Label"></asp:Label>
                </td>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        Road Tax Expiry Date:
                    </td>
                    <td>
                        <asp:Label ID="lblroadtaxdate" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        OverSpeed Limit KM/H:
                    </td>
                    <td>
                        <asp:Label ID="lbloverspeed" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        Contract Period From Date:
                    </td>
                    <td>
                        <asp:Label ID="lblconfdate" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        Contract Period To Date:
                    </td>
                    <td>
                        <asp:Label ID="lblcontdate" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        Contractor Name:
                    </td>
                    <td>
                        <asp:Label ID="lblcontname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <td style="color: Black; font-size: large">
                    SIM Details
                </td>
                <td>
                </td>
                <td style="color: Black; font-size: large">
                    Device Details
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                                        Mobile Number
                </td>
                <td>
                    :<asp:Label ID="lblMobNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Serial Number</td>
                <td>
                    :<asp:Label ID="lblSerialNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                                        SIM Provider</td>
                <td>
                    :<asp:Label ID="lblSIMProvider" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Date of Purchase
                </td>
                <td>
                    :<asp:Label ID="lblDOP" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                                        GPRS Activated Date
                </td>
                <td>
                    :<asp:Label ID="lblGPRSActivatedDT" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Dealer Name
                </td>
                <td>
                    :<asp:Label ID="lblDealerName" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                                        Recharge Period</td>
                <td>
                    :<asp:Label ID="lblRechargePeriod" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
                <%--<td>
                    Device ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDeviceID" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>--%>
            </tr>
            <%--<tr>
                <td>
                    Transmission Frequency:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtTransFreq" runat="server" Height="15px" Width="192px" Text="1"
                        Enabled="false"></asp:TextBox>&nbsp;&nbsp;in minutes
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>--%>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btClose" runat="server" Text="Home" OnClick="btClose_Click" />
                    <%--OnClick="btSubmit_Click" />--%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

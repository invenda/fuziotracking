﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Xml;

namespace Tracking
{
    public class DBClass
    {
        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();
        public string ID;
        public string Cx_Type;
        public string Cx_Name;
        public string Cx_BillingAddress1;
        public string Cx_BillingAddress2;
        public string Cx_District;
        public string Cx_State;
        public string Cx_AreaCode;
        public string Cx_ContactNumber;
        public string Cx_MobileNumber;
        public string Cx_DateOfRegistration;
        public string Cx_EmailID;
        public string Cx_alteEmailID;

        public string Vehicle_number;
        public string Vehicle_Reg_Name;
        public string User_ID;
        public string Vehicle_Model;
        public string Vehicle_Make;
        public string Vehicle_Insurance_Number;
        public string Vehicle_Insurance_Expiry_Date;
        public string Tank_Capacity;
        public string Battery_Voltage;
        public string Existing_Meter_Reading;
        public string Kilometer_Pulse;
        public string Driver_Name;
        public string Driver_Mobile_Number;
        public string Driver_Address;
        public string Driving_Licence_Number;
        public string Driving_Licence_Expiry_Date;
        public string Mobile_Number;
        public string SIM_Provider;
        public string GPRS_Activated_Date;
        public string Recharge_Period;
        public string Serial_Number;
        public string Date_of_Purchase;
        public string Dealer_Name;
        public string Tank_Type;
        public string Fuel_Calibration;
        public string milage;
        public string Fuel_Type;
        public string Minusstring;
        public string Vehicle_Type;
        // public string ACswitch;


        public DataTable GetVehicles(string customerid)//, string parentuser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select distinct VehicalNumber,Vehicle_Alerts_Status from Vehicles_Device_Rel inner join Users  on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select VehicalNumber,Vehicle_Alerts_Status,Serial_no from Vehicles_Device_Rel inner join Devices on Devices.Device_ID=Vehicles_Device_Rel.Device_ID where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable Fuzio_Home(string v)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.Fuzio_Home T1  WHERE User_Id=" + v + " and GTIM = (SELECT MAX(GTIM) FROM dbo.Fuzio_Home GROUP BY XXXM HAVING XXXM = T1.XXXM)", sqlconn); 
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable Fuzio_Track(string vehiclenumber, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT * FROM dbo.Fuzio_Home T1  WHERE User_Id=" + customerID + " and VehicalNumber='" + vehiclenumber + "' and GTIM = (SELECT MAX(GTIM) FROM dbo.Fuzio_Home GROUP BY XXXM HAVING XXXM = T1.XXXM)", sqlconn); //and USER_ID = " + customerID + " 
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }


        public DataTable GetVehiclescount(string customerid)//, string parentuser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select distinct VehicalNumber,Vehicle_Alerts_Status from Vehicles_Device_Rel inner join Users  on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            //SqlCommand cmd = new SqlCommand("select VehicalNumber,Vehicle_Alerts_Status,Serial_no from Vehicles_Device_Rel inner join Devices on Devices.Device_ID=Vehicles_Device_Rel.Device_ID where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select count (*) from Device_Customer_Rel where user_id=" + customerid + "", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetdesabledVehiclescount(string customerid)//, string parentuser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select distinct VehicalNumber,Vehicle_Alerts_Status from Vehicles_Device_Rel inner join Users  on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            //SqlCommand cmd = new SqlCommand("select VehicalNumber,Vehicle_Alerts_Status,Serial_no from Vehicles_Device_Rel inner join Devices on Devices.Device_ID=Vehicles_Device_Rel.Device_ID where User_ID = " + customerid + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select count (*) from DesabledDevices where user_id=" + customerid + "", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable Getuseralert(string customerid)//, string parentuser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Alert_sms,Alertmessage from Users where ID=" + customerid + "", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_GetVehicles(string customerid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("GetVehicles", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@customerid", customerid));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetVehStat1(string customerID, string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select top 1 GTIM,Vehicles_Device_Rel.VehicalNumber FROM Tracking INNER JOIN Vehicles_Device_Rel ON Tracking.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = '" + customerID + "' and VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);
            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select top 1 GTIM FROM Trackoff INNER JOIN Vehicles_Device_Rel ON Trackoff.XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VN + "')ORDER by GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }

        internal DataTable GetVehStat(string customerID, string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select top 1 GTIM,Vehicles_Device_Rel.VehicalNumber FROM Tracking INNER JOIN Vehicles_Device_Rel ON Tracking.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = '" + customerID + "' and VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);
            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select top 1 GTIM FROM Track INNER JOIN Vehicles_Device_Rel ON Track.XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VN + "')ORDER by GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        internal DataTable GetVehStatoff(string customerID, string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("select top 1 GTIM,Vehicles_Device_Rel.VehicalNumber FROM Tracking INNER JOIN Vehicles_Device_Rel ON Tracking.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = '" + customerID + "' and VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);
            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlCommand cmd = new SqlCommand("select top 1 GTIM FROM Trackoff INNER JOIN Vehicles_Device_Rel ON Trackoff.XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VN + "')ORDER by GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable GetVehiclePresentlocation1(string vehiclenumber, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP (1) Trackoff.LAMI, Trackoff.LGMI, Trackoff.SPED, Trackoff.BAT_ADC, Trackoff.GTIM, Trackoff.MAIN_BACKUP,Trackoff.PULSE_CTR, Trackoff.NOST, Trackoff.IGST, Trackoff.COND_INPUTS, Trackoff.FUEL_ADC, Trackoff.XXXM, Vehicles_Device_Rel.VehicalNumber FROM Trackoff INNER JOIN Vehicles_Device_Rel ON Trackoff.XXXM = Vehicles_Device_Rel.Device_ID WHERE (Vehicles_Device_Rel.VehicalNumber = '" + vehiclenumber + "') order by GTIM desc", sqlconn); //and USER_ID = " + customerID + " 
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetVehiclePresentlocation(string vehiclenumber, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP (1) Track.LAMI, Track.LGMI, Track.SPED, Track.BAT_ADC, Track.GTIM, Track.MAIN_BACKUP,Track.PULSE_CTR, Track.NOST, Track.IGST, Track.COND_INPUTS, Track.FUEL_ADC, Track.XXXM, Vehicles_Device_Rel.VehicalNumber FROM Track INNER JOIN Vehicles_Device_Rel ON Track.XXXM = Vehicles_Device_Rel.Device_ID WHERE (Vehicles_Device_Rel.VehicalNumber = '" + vehiclenumber + "') order by GTIM desc", sqlconn); //and USER_ID = " + customerID + " 
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_GetVehiclePresentlocation(string vehiclenumber, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("GetVehiclePresentlocation", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vehiclenumber", vehiclenumber));
            cmd.Parameters.Add(new SqlParameter("@customerid", customerID));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetALLVehiclePresentlocation()
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select ID,VehicleNumber,lat,Long,Status from VehiclesStatus", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetUserRoles(string userrole)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd;
            if (userrole == "2")
                cmd = new SqlCommand("select UserRole_ID,UserRole_Name from UserRoles where UserRole_ID != 1 ", sqlconn);
            else
                cmd = new SqlCommand("select UserRole_ID,UserRole_Name from UserRoles", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void CreateUser(string userId, string password, string firstName, string lastName, string userRoleID, string parentuser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("insert into Users(UserName,Password,FirstName,LastName,User_Role_ID,Parent_User) values('"
            //    + userId + "','" + password + "','" + firstName + "','" + lastName + "','" + userRoleID + "','" + parentuser + "')", sqlconn);
            SqlCommand cmd = new SqlCommand("CreateUsers", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@username", userId));
            cmd.Parameters.Add(new SqlParameter("@password", password));
            cmd.Parameters.Add(new SqlParameter("@firstname", firstName));
            cmd.Parameters.Add(new SqlParameter("@lastname", lastName));
            cmd.Parameters.Add(new SqlParameter("@userrole", userRoleID));
            cmd.Parameters.Add(new SqlParameter("@parentuser", parentuser));
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void CreateDevice(string deviceID, string manuDate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Devices(Device_ID,Device_Manufacture_Date,Status) values('"
                + deviceID + "','" + manuDate + "',0)", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable GetAllCustomers()
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select ID,UserName from Users where User_Role_ID = 2", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetAllDevices()
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Device_ID from Devices where Status = 0", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void AssignDevices(string deviceID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("AssignDevices", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@DeviceID", deviceID));
            cmd.Parameters.Add(new SqlParameter("@CustomerID", customerID));
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable GetAssingedDevices(string customerid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Device_ID from Device_Customer_Rel where User_id = " + customerid, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetAllDevicesForCustomer(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Device_ID from Device_Customer_Rel where User_Id = " + customerID + "and Device_ID not in(select Device_ID from Vehicles_Device_Rel where user_id = " + customerID + ")", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetUnAssingedVehicles(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_number],[User_ID] FROM [Vehicles] where User_ID = " + customerID + " and Desable ='No' and Vehicle_number not in (SELECT [VehicalNumber] FROM [Vehicles_Device_Rel] where USER_ID = " + customerID + ")", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetAssingedVehicles(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT ID,[VehicalNumber],[Device_ID],[IMEI],[Model],[Instal_date] FROM [Vehicles_Device_Rel] where USER_ID =" + customerID + " order by VehicalNumber asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void AssignVehicle(string deviceID, string vehicleNumber, string customerid, string sms, string imei, string model, string instdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO [Vehicles_Device_Rel]([VehicalNumber],[Device_ID],[User_ID],[SAdmin],[SMS_Status],[IMEI],[Model],[Instal_date]) VALUES ('" + vehicleNumber + "','" + deviceID + "','" + customerid + "','" + sms + "','0','" + imei + "','" + model + "','" + instdate + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable GetVehicleIMEI(string deviceID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand(" SELECT * FROM Devices WHERE Device_ID= '" + deviceID + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetVehicleReport(string customerID, string vehicleNumber, string date, string nextdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [LAMI],[LGMI],[GTIM],[SPED] as datetime,[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID where VehicalNumber = '" + vehicleNumber + "' and USER_ID = " + customerID + "and GTIM >= '" + date + "' and GTIM < '" + nextdate + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable CheckUser(string username, string password)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT ID,[UserName],[Password] ,[FirstName],[LastName] ,[User_Role_ID],[Register],parent_user,UserImage,Logo_Text,Google_API_Key FROM [Users] where UserName = '" + username + "' and Password= '" + password + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void ChangePW(string id, string username, string password)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand(" UPDATE Users Set Password='" + password + "'where UserName = '" + username + "' and ID= " + id + "", sqlconn);
            cmd.ExecuteNonQuery();
            sqlconn.Close();

        }
        public void CreateVehicle(string vehicleNo, string txtVehRegName, string userID, string txtVehModel, string txtVehMake, string txtVehInsNum, string txtTankCapacity, string Voltage, string txtExistMetRead, string txtKiloPulse, string txtDriverName, string txtDriverMobNum, string txtDriverADD, string txtDrivLicNum, string txtMobNum, string txtSIMProvider, string txtGPRSActDt, string Recharge, string txtSNum, string txtDOP, string txtDealerName, string sms, string TankType, string txtFCali, string dname, string smsno, string txtmilage, string ddlft, string Minusstring, string vehicletype)
        {
            //txtVehicleNo.Text, Session["UserID"].ToString(), txtDriverName.Text, txtVehModel.Text, txtDriverMobNum.Text, txtVehMake.Text, txtDriverADD.Text, txtVehInsNum.Text, txtDrivLicNum.Text, txtVehInsExpDt.Text, txtDrivLic.Text, txtTankCapacity.Text, Voltage, txtExistMetRead.Text, txtKiloPulse.Text, txtMobNum.Text, txtSNum.Text, txtSIMProvider.Text, txtDOP.Text, txtGPRSActDt.Text, txtDealerName.Text, Recharge
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Vehicles(Vehicle_number,Vehicle_Reg_Name,User_ID,Vehicle_Model,Vehicle_Make,Vehicle_Insurance_Number,Tank_Capacity,Battery_Voltage,Existing_Meter_Reading,Kilometer_Pulse,Driver_Name,Driver_Mobile_Number,Driver_Address,Driving_Licence_Number,Mobile_Number,SIM_Provider,GPRS_Activated_Date,Recharge_Period,Serial_Number,Date_of_Purchase,Dealer_Name, SAdmin,Tank_Type, Fuel_Calibration,Rto_Devision_name,SMS_No,milage,Fuel_type,Minusstring,Vehicle_Type,Desable)values('" + vehicleNo + "','" + txtVehRegName + "','" + userID + "','" + txtVehModel + "','" + txtVehMake + "','" + txtVehInsNum + "','" + txtTankCapacity + "','" + Voltage + "','" + txtExistMetRead + "','" + txtKiloPulse + "','" + txtDriverName + "','" + txtDriverMobNum + "','" + txtDriverADD + "','" + txtDrivLicNum + "','" + txtMobNum + "','" + txtSIMProvider + "','" + txtGPRSActDt + "','" + Recharge + "','" + txtSNum + "','" + txtDOP + "','" + txtDealerName + "','" + 35 + "','" + TankType + "','" + txtFCali + "','" + dname + "','" + smsno + "','" + txtmilage + "','" + ddlft + "','" + Minusstring + "','" + vehicletype + "','No')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void DeleteVehicleDeviceRel(string id)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete from Vehicles_Device_Rel where ID = '" + id + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }


        //-------------------------------------
        // 
        public DataTable GetFromTime(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select GTIM from Tracking", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        public DataTable GetToTime(string customerID)//string val
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select GTIM from Tracking", sqlconn); //SELECT CONVERT(VARCHAR,GTIM,108) FROM Tracking
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable CheckStatus(string ddlMapTOVehicle, string ddlFromTime, string ddlToTime, string ParentUser)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Vehicles_Device_Rel.VehicalNumber, Tracking.GTIM FROM Vehicles_Device_Rel CROSS JOIN Tracking", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        //public void insertLocation(string address)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("UPDATE Tracking SET Location = '" + address + "'", sqlconn);
        //    cmd.ExecuteNonQuery();
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();
        //}

        //public DataTable bi_search(string ddlMapTOVehicle, string ddlFromTime, string ddlToTime)
        //{
        //    string sqlWHERE = " WHERE";


        //    if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
        //        sqlWHERE = sqlWHERE + " XXXM = '" + ddlMapTOVehicle.Trim() + "' and ";

        //    if (ddlFromTime.CompareTo("") != 0 && ddlFromTime != null && ddlFromTime != "Select One")
        //        sqlWHERE = sqlWHERE + " GTIM = '" + ddlFromTime.Trim() + "' and ";

        //    if (ddlToTime.CompareTo("") != 0 && ddlToTime != null && ddlToTime != "Select One")
        //        sqlWHERE = sqlWHERE + " GTIM = '" + ddlToTime.Trim() + "' and ";

        //    return ds_search(sqlWHERE);
        //}

        //private DataTable ds_search(string sqlWHERE)
        //{
        //    try
        //    {
        //        SqlConnection sqlconn = new SqlConnection(connectionstring);
        //        sqlconn.Open();

        //        string sql = "SELECT * FROM Tracking";
        //        sql = sql + sqlWHERE;
        //        SqlCommand cmd = new SqlCommand(sql, sqlconn);
        //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //        DataSet ds = new DataSet();
        //        adp.Fill(ds);
        //        if (sqlconn.State == ConnectionState.Open)
        //            sqlconn.Close();
        //        return ds.Tables[0];
        //    }
        //    catch { throw new Exception("Your Search returned 0 Results!"); }
        //}


        //public DataTable Find(string ddlMapTOVehicle, string ddlFromTime, string ddlToTime)
        //{
        //    string sqlWHERE = " WHERE";


        //    if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
        //        sqlWHERE = sqlWHERE + " XXXM = '" + ddlMapTOVehicle.Trim() + "' and ";

        //    if (ddlFromTime.CompareTo("") != 0 && ddlFromTime != null && ddlFromTime != "Select One")
        //        sqlWHERE = sqlWHERE + " GTIM = '" + ddlFromTime.Trim() + "' and ";

        //    if (ddlToTime.CompareTo("") != 0 && ddlToTime != null && ddlToTime != "Select One")
        //        sqlWHERE = sqlWHERE + " GTIM = '" + ddlToTime.Trim() + "' and ";

        //    return ds_Find(sqlWHERE);
        //}

        //private DataTable ds_Find(string sqlWHERE)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();

        //    string sql = "SELECT * FROM Tracking";
        //    sql = sql + sqlWHERE;
        //    SqlCommand cmd = new SqlCommand(sql, sqlconn);
        //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    adp.Fill(ds);
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();
        //    return ds.Tables[0];
        //}

        public DataTable GetFromHours(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Hours from LU_Time", sqlconn); //SELECT CONVERT(VARCHAR,GTIM,108) FROM Tracking
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetFromMinutes(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Minutes from LU_Time", sqlconn); //SELECT CONVERT(VARCHAR,GTIM,108) FROM Tracking
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable searchOn(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {

            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 1 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED > '0' order by GTIM desc,NewID desc";

            return ds_Find(sql);
        }

        private DataTable ds_Find(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getMovingData(string Vid, DateTime fdate, DateTime tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  IGST = 1  and  GTIM between '" + fdate + "' and '" + tdate + " ' and SPED > '0' order by GTIM ASC";
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,FUEL_ADC  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  IGST = 1  and  GTIM between '" + fdate + "' and '" + tdate + " ' order by GTIM ASC";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getMovingData12(string Vid, DateTime fdate, DateTime tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  IGST = 1  and  GTIM between '" + fdate + "' and '" + tdate + " ' and SPED > '0' order by GTIM ASC";
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  GTIM between '" + fdate + "' and '" + tdate + " ' order by GTIM ASC";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getMovinggeoData12(string Vid, DateTime fdate, DateTime tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  IGST = 1  and  GTIM between '" + fdate + "' and '" + tdate + " ' and SPED > '0' order by GTIM ASC";
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Geo_route_basis] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and IGST = 1 and SPED > 0 and GTIM between '" + fdate + "' and '" + tdate + " ' order by GTIM ASC";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable searchOn2geoData12(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 0 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED = '0' order by GTIM desc,NewID desc";

            return ds_Find2geoData12(sql);
        }
        private DataTable ds_Find2geoData12(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //top 1
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Geo_route_basis] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable GetDT(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select DT from LU_Time", sqlconn); //SELECT CONVERT(VARCHAR,GTIM,108) FROM Tracking
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }



        //public DataTable GetSalutation(string customerID)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("select Salutation from LU_General", sqlconn);
        //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    adp.Fill(ds);
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();
        //    return ds.Tables[0];
        //}

        public void RegCustomer(string ID, string Company, string txtCustName, string txtCustAddress1, string txtCustAddress2, string txtDistrict, string ddlState, string txtAreaCode, string txtCustConNum, string txtCustMobNum, string txtDOR, string txtEmail, string txtalterEmail, string customerID)
        {
            //string ddlSalutation
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Customer([ID],[Cx_Type],[Cx_Name],[Cx_BillingAddress1],[Cx_BillingAddress2],[Cx_District],[Cx_State],[Cx_AreaCode],[Cx_ContactNumber],[Cx_MobileNumber],[Cx_DateOfRegistration],[Cx_EmailID],[Cx_AlterEmailID]) values('"
                + ID + "','" + Company + "','" + txtCustName + "','" + txtCustAddress1 + "','" + txtCustAddress2 + "','" + txtDistrict + "','" + ddlState + "','" + txtAreaCode + "','" + txtCustConNum + "','" + txtCustMobNum + "','" + txtDOR + "','" + txtEmail + "','" + txtalterEmail + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void UpdateCustomer(string Company, string txtCustName, string txtCustAddress1, string txtCustAddress2, string txtDistrict, string ddlState, string txtAreaCode, string txtCustConNum, string txtCustMobNum, string txtDOR, string txtEmail, string txtaEmail, string lblID)
        {
            //string ddlSalutation
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Customer set [Cx_Type]='" + Company + "',[Cx_Name]='" + txtCustName + "',[Cx_BillingAddress1]='" + txtCustAddress1 + "',[Cx_BillingAddress2]='" + txtCustAddress2 + "',[Cx_District]='" + txtDistrict + "',[Cx_State]='" + ddlState + "',[Cx_AreaCode]='" + txtAreaCode + "',[Cx_ContactNumber]='" + txtCustConNum + "',[Cx_MobileNumber]='" + txtCustMobNum + "',[Cx_DateOfRegistration]='" + txtDOR + "',[Cx_EmailID]='" + txtEmail + "',[Cx_AlterEmailID]='" + txtaEmail + "' where [ID]='" + lblID + "'", sqlconn);//) values('"
            //+ Company + "','" + txtCustName + "','" + txtCustAddress1 + "','" + txtCustAddress2 + "','" + txtDistrict + "','" + ddlState + "','" + txtAreaCode + "','" + txtCustConNum + "','" + txtCustMobNum + "','" + txtDOR + "','" + txtEmail + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }

        public DataTable GetState(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select States from LU_General", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable ViewDetails(string id, string lblID, string lblCustType, string lblCustName, string lblBillAdd1, string lblBillAdd2, string lblCity, string lblState, string lblAreaCode, string lblConNum, string lblMobNumstring, string lblDOR, string lblEmail)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Customer.ID, Customer.Cx_Type, Customer.Cx_Name, Customer.Cx_BillingAddress1, Customer.Cx_BillingAddress2, Customer.Cx_District, Customer.Cx_State, Customer.Cx_AreaCode, Customer.Cx_ContactNumber, Customer.Cx_MobileNumber, Customer.Cx_DateOfRegistration,Customer.Cx_EmailID FROM Users INNER JOIN Customer ON Users.ID = Customer.ID where Users.ID='" + id + "'", sqlconn); //select top (1) [Cx_ID],[Cx_Type],[Cx_Name],[Cx_BillingAddress1],[Cx_BillingAddress2],[Cx_District],[Cx_State],[Cx_AreaCode],[Cx_ContactNumber],[Cx_MobileNumber],[Cx_DateOfRegistration],[Cx_EmailID] from Customer order by Cx_ID desc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.ID = ds.Tables[0].Rows[0]["ID"].ToString();
            this.Cx_Type = ds.Tables[0].Rows[0]["Cx_Type"].ToString();
            this.Cx_Name = ds.Tables[0].Rows[0]["Cx_Name"].ToString();
            this.Cx_BillingAddress1 = ds.Tables[0].Rows[0]["Cx_BillingAddress1"].ToString();
            this.Cx_BillingAddress2 = ds.Tables[0].Rows[0]["Cx_BillingAddress2"].ToString();
            this.Cx_District = ds.Tables[0].Rows[0]["Cx_District"].ToString();
            this.Cx_State = ds.Tables[0].Rows[0]["Cx_State"].ToString();
            this.Cx_AreaCode = ds.Tables[0].Rows[0]["Cx_AreaCode"].ToString();
            this.Cx_ContactNumber = ds.Tables[0].Rows[0]["Cx_ContactNumber"].ToString();
            this.Cx_MobileNumber = ds.Tables[0].Rows[0]["Cx_MobileNumber"].ToString();
            this.Cx_DateOfRegistration = ds.Tables[0].Rows[0]["Cx_DateOfRegistration"].ToString();
            this.Cx_EmailID = ds.Tables[0].Rows[0]["Cx_EmailID"].ToString();

            sqlconn.Close();
            return ds.Tables[0];


        }

        public DataTable Load(string id)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Customer.ID, Customer.Cx_Type, Customer.Cx_Name, Customer.Cx_BillingAddress1, Customer.Cx_BillingAddress2, Customer.Cx_District, Customer.Cx_State, Customer.Cx_AreaCode, Customer.Cx_ContactNumber, Customer.Cx_MobileNumber, Customer.Cx_DateOfRegistration,Customer.Cx_EmailID,Cx_AlterEmailID FROM Users INNER JOIN Customer ON Users.ID = Customer.ID where Users.ID='" + id + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select top (1) [Cx_ID],[Cx_Type],[Cx_Name],[Cx_BillingAddress1],[Cx_BillingAddress2],[Cx_District],[Cx_State],[Cx_AreaCode],[Cx_ContactNumber],[Cx_MobileNumber],[Cx_DateOfRegistration],[Cx_EmailID] from Customer order by Cx_ID desc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.ID = ds.Tables[0].Rows[0]["ID"].ToString();
            this.Cx_Type = ds.Tables[0].Rows[0]["Cx_Type"].ToString();
            this.Cx_Name = ds.Tables[0].Rows[0]["Cx_Name"].ToString();
            this.Cx_BillingAddress1 = ds.Tables[0].Rows[0]["Cx_BillingAddress1"].ToString();
            this.Cx_BillingAddress2 = ds.Tables[0].Rows[0]["Cx_BillingAddress2"].ToString();
            this.Cx_District = ds.Tables[0].Rows[0]["Cx_District"].ToString();
            this.Cx_State = ds.Tables[0].Rows[0]["Cx_State"].ToString();
            this.Cx_AreaCode = ds.Tables[0].Rows[0]["Cx_AreaCode"].ToString();
            this.Cx_ContactNumber = ds.Tables[0].Rows[0]["Cx_ContactNumber"].ToString();
            this.Cx_MobileNumber = ds.Tables[0].Rows[0]["Cx_MobileNumber"].ToString();
            this.Cx_DateOfRegistration = ds.Tables[0].Rows[0]["Cx_DateOfRegistration"].ToString();
            this.Cx_EmailID = ds.Tables[0].Rows[0]["Cx_EmailID"].ToString();
            this.Cx_alteEmailID = ds.Tables[0].Rows[0]["Cx_AlterEmailID"].ToString();

            sqlconn.Close();
            return ds.Tables[0];
        }


        public void Reg(string register, string User)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Users set [Register]='" + register + "' where UserName='" + User + "'", sqlconn);//) values('"
            //+ Company + "','" + txtCustName + "','" + txtCustAddress1 + "','" + txtCustAddress2 + "','" + txtDistrict + "','" + ddlState + "','" + txtAreaCode + "','" + txtCustConNum + "','" + txtCustMobNum + "','" + txtDOR + "','" + txtEmail + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public DataTable ViewExactCust(string p)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Customer.ID, Customer.Cx_Type, Customer.Cx_Name, Customer.Cx_BillingAddress1, Customer.Cx_BillingAddress2, Customer.Cx_District, Customer.Cx_State, Customer.Cx_AreaCode, Customer.Cx_ContactNumber, Customer.Cx_MobileNumber, Customer.Cx_DateOfRegistration,Customer.Cx_EmailID,Cx_AlterEmailID FROM Users INNER JOIN Customer ON Users.ID = Customer.ID where Users.ID='" + p + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            //this.ID = ds.Tables[0].Rows[0]["ID"].ToString();
            //this.Cx_Type = ds.Tables[0].Rows[0]["Cx_Type"].ToString();
            //this.Cx_Name = ds.Tables[0].Rows[0]["Cx_Name"].ToString();
            //this.Cx_BillingAddress1 = ds.Tables[0].Rows[0]["Cx_BillingAddress1"].ToString();
            //this.Cx_BillingAddress2 = ds.Tables[0].Rows[0]["Cx_BillingAddress2"].ToString();
            //this.Cx_District = ds.Tables[0].Rows[0]["Cx_District"].ToString();
            //this.Cx_State = ds.Tables[0].Rows[0]["Cx_State"].ToString();
            //this.Cx_AreaCode = ds.Tables[0].Rows[0]["Cx_AreaCode"].ToString();
            //this.Cx_ContactNumber = ds.Tables[0].Rows[0]["Cx_ContactNumber"].ToString();
            //this.Cx_MobileNumber = ds.Tables[0].Rows[0]["Cx_MobileNumber"].ToString();
            //this.Cx_DateOfRegistration = ds.Tables[0].Rows[0]["Cx_DateOfRegistration"].ToString();
            //this.Cx_EmailID = ds.Tables[0].Rows[0]["Cx_EmailID"].ToString();

            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable CustID(string lblID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select [Cx_ID] from Customer", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable CustRegNum(string CxRegNo)
        {

            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Cx_ID, Cx_Type, Cx_Name, Cx_BillingAddress1, Cx_BillingAddress2, Cx_AreaCode, Cx_ContactNumber, Cx_MobileNumber, Cx_DateOfRegistration, Cx_EmailID FROM Customer where Cx_ID = '" + CxRegNo + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];

        }

        public DataTable ViewVehDetails(string VehRegNUM, string lblVehRegNum, string UserID, string lblDriName, string lblVehModel, string lblDriMobNum, string lblVehMake, string lblDriAddress, string lblVehInsNum, string lblDriLicNum, string lblVehInsExpDt, string lblDriLicExpDt, string lblTankCap, string lblBatVolt, string lblExistMetRead, string lblKiloPulse, string lblMobNum, string lblSerialNum, string lblSIMProvider, string lblDOP, string lblGPRSActivatedDT, string lblDealerName, string lblRechargePeriod)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Veh.Vehicle_number, Veh.User_ID, Veh.Vehicle_Model, Veh.Vehicle_Make, Veh.Vehicle_Insurance_Number, Veh.Vehicle_Insurance_Expiry_Date, Veh.Tank_Capacity, Veh.Battery_Voltage, Veh.Existing_Meter_Reading, Veh.Kilometer_Pulse, Veh.Driver_Name, Veh.Driver_Mobile_Number, Veh.Driver_Address, Veh.Driving_Licence_Number, Veh.Driving_Licence_Expiry_Date, Veh.Mobile_Number, Veh.SIM_Provider, Veh.GPRS_Activated_Date, Veh.Recharge_Period, Veh.Serial_Number, Veh.Date_of_Purchase, Veh.Dealer_Name, Vehicles.Vehicle_number AS Expr2,Veh.ID,Veh.Rto_Renewal_Date,Veh.Rto_Devision_name,Veh.Overspeed,Veh.Contract_Fromdate,Veh.Contract_Todate,Veh.Contract_Name,Veh.Rto_fc_date,Veh.Emission_date,Veh.Road_tax_date FROM  Veh INNER JOIN Vehicles ON Veh.Vehicle_number = Vehicles.Vehicle_number where Vehicles.Vehicle_number='" + VehRegNUM + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.Vehicle_number = ds.Tables[0].Rows[0]["Vehicle_number"].ToString();
            this.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
            this.Driver_Name = ds.Tables[0].Rows[0]["Driver_Name"].ToString();
            this.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model"].ToString();
            this.Driver_Mobile_Number = ds.Tables[0].Rows[0]["Driver_Mobile_Number"].ToString();
            this.Vehicle_Make = ds.Tables[0].Rows[0]["Vehicle_Make"].ToString();
            this.Driver_Address = ds.Tables[0].Rows[0]["Driver_Address"].ToString();
            this.Vehicle_Insurance_Number = ds.Tables[0].Rows[0]["Vehicle_Insurance_Number"].ToString();
            this.Driving_Licence_Number = ds.Tables[0].Rows[0]["Driving_Licence_Number"].ToString();
            this.Vehicle_Insurance_Expiry_Date = ds.Tables[0].Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            this.Driving_Licence_Expiry_Date = ds.Tables[0].Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            this.Tank_Capacity = ds.Tables[0].Rows[0]["Tank_Capacity"].ToString();
            this.Battery_Voltage = ds.Tables[0].Rows[0]["Battery_Voltage"].ToString();
            this.Existing_Meter_Reading = ds.Tables[0].Rows[0]["Existing_Meter_Reading"].ToString();
            this.Kilometer_Pulse = ds.Tables[0].Rows[0]["Kilometer_Pulse"].ToString();
            this.Mobile_Number = ds.Tables[0].Rows[0]["Mobile_Number"].ToString();
            this.Serial_Number = ds.Tables[0].Rows[0]["Serial_Number"].ToString();
            this.SIM_Provider = ds.Tables[0].Rows[0]["SIM_Provider"].ToString();
            this.Date_of_Purchase = ds.Tables[0].Rows[0]["Date_of_Purchase"].ToString();
            this.GPRS_Activated_Date = ds.Tables[0].Rows[0]["GPRS_Activated_Date"].ToString();
            this.Dealer_Name = ds.Tables[0].Rows[0]["Dealer_Name"].ToString();
            this.Recharge_Period = ds.Tables[0].Rows[0]["Recharge_Period"].ToString();

            sqlconn.Close();
            return ds.Tables[0];
        }
        public void CreateVeh(string vehicleNo, string txtVehRegName, string userID, string txtVehModel, string txtVehMake, string txtVehInsNum, string txtTankCapacity, string Voltage, string txtExistMetRead, string txtKiloPulse, string txtDriverName, string txtDriverMobNum, string txtDriverADD, string txtDrivLicNum, string txtMobNum, string txtSIMProvider, string txtGPRSActDt, string Recharge, string txtSNum, string txtDOP, string txtDealerName, string sms, string TankType, string txtFCali, string dname, string smsno, string txtmilage, string ddlft, string Minusstring, string vehicletype)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Veh(Vehicle_number,Vehicle_Reg_Name,User_ID,Vehicle_Model,Vehicle_Make,Vehicle_Insurance_Number,Tank_Capacity,Battery_Voltage,Existing_Meter_Reading,Kilometer_Pulse,Driver_Name,Driver_Mobile_Number,Driver_Address,Driving_Licence_Number,Mobile_Number,SIM_Provider,GPRS_Activated_Date,Recharge_Period,Serial_Number,Date_of_Purchase,Dealer_Name, SAdmin,Tank_Type, Fuel_Calibration,Rto_Devision_name,SMS_No,milage,Fuel_type,Minusstring,Vehicle_Type,Desable) values('"
                + vehicleNo + "','" + txtVehRegName + "','" + userID + "','" + txtVehModel + "','" + txtVehMake + "','" + txtVehInsNum + "','" + txtTankCapacity + "','" + Voltage + "','" + txtExistMetRead +
                "','" + txtKiloPulse + "','" + txtDriverName + "','" + txtDriverMobNum + "','" + txtDriverADD + "','" + txtDrivLicNum + "','" + txtMobNum + "','" + txtSIMProvider + "','" + txtGPRSActDt + "','" + Recharge + "','" + txtSNum + "','" + txtDOP + "','" + txtDealerName + "','" + 35 + "','" + TankType + "','" + txtFCali + "','" + dname + "','" + smsno + "','" + txtmilage + "','" + ddlft + "','" + Minusstring + "','" + vehicletype + "','No')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable ViewVeh(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select distinct Vehicle_number from Veh where  USER_ID = '" + customerID + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable ViewVehicle(string vehnum, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Veh.Vehicle_number, Veh.Vehicle_Reg_Name,  Veh.User_ID, Veh.Vehicle_Model, Veh.Vehicle_Make, Veh.Vehicle_Insurance_Number, Veh.Vehicle_Insurance_Expiry_Date, Veh.Tank_Capacity, Veh.Battery_Voltage, Veh.Existing_Meter_Reading, Veh.Kilometer_Pulse, Veh.Driver_Name, Veh.Driver_Mobile_Number, Veh.Driver_Address, Veh.Driving_Licence_Number, Veh.Driving_Licence_Expiry_Date, Veh.Mobile_Number, Veh.SIM_Provider, Veh.GPRS_Activated_Date, Veh.Recharge_Period, Veh.Serial_Number, Veh.Date_of_Purchase, Veh.Dealer_Name FROM Veh WHERE Vehicle_number = '" + vehnum + "' and USER_ID = '" + customerID + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.Vehicle_number = ds.Tables[0].Rows[0]["Vehicle_number"].ToString();
            this.Vehicle_Reg_Name = ds.Tables[0].Rows[0]["Vehicle_Reg_Name"].ToString();
            this.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
            this.Driver_Name = ds.Tables[0].Rows[0]["Driver_Name"].ToString();
            this.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model"].ToString();
            this.Driver_Mobile_Number = ds.Tables[0].Rows[0]["Driver_Mobile_Number"].ToString();
            this.Vehicle_Make = ds.Tables[0].Rows[0]["Vehicle_Make"].ToString();
            this.Driver_Address = ds.Tables[0].Rows[0]["Driver_Address"].ToString();
            this.Vehicle_Insurance_Number = ds.Tables[0].Rows[0]["Vehicle_Insurance_Number"].ToString();
            this.Driving_Licence_Number = ds.Tables[0].Rows[0]["Driving_Licence_Number"].ToString();
            this.Vehicle_Insurance_Expiry_Date = ds.Tables[0].Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            this.Driving_Licence_Expiry_Date = ds.Tables[0].Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            this.Tank_Capacity = ds.Tables[0].Rows[0]["Tank_Capacity"].ToString();
            this.Battery_Voltage = ds.Tables[0].Rows[0]["Battery_Voltage"].ToString();
            this.Existing_Meter_Reading = ds.Tables[0].Rows[0]["Existing_Meter_Reading"].ToString();
            this.Kilometer_Pulse = ds.Tables[0].Rows[0]["Kilometer_Pulse"].ToString();
            this.Mobile_Number = ds.Tables[0].Rows[0]["Mobile_Number"].ToString();
            this.Serial_Number = ds.Tables[0].Rows[0]["Serial_Number"].ToString();
            this.SIM_Provider = ds.Tables[0].Rows[0]["SIM_Provider"].ToString();
            this.Date_of_Purchase = ds.Tables[0].Rows[0]["Date_of_Purchase"].ToString();
            this.GPRS_Activated_Date = ds.Tables[0].Rows[0]["GPRS_Activated_Date"].ToString();
            this.Dealer_Name = ds.Tables[0].Rows[0]["Dealer_Name"].ToString();
            this.Recharge_Period = ds.Tables[0].Rows[0]["Recharge_Period"].ToString();

            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable ViewVD(string RegNUM, string p, string lblVehRegName, string p_3, string p_4, string p_5, string p_6, string p_7, string p_8, string p_9, string p_10, string p_13, string p_14, string p_15, string p_16, string p_17, string p_18, string p_19, string p_20, string p_21, string p_22, string p_23, string p_24, string p_25, string p_26)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Veh.Vehicle_number, Veh.Vehicle_Reg_Name, Veh.User_ID, Veh.Vehicle_Model, Veh.Vehicle_Make, Veh.Vehicle_Insurance_Number, Veh.Vehicle_Insurance_Expiry_Date, Veh.Tank_Capacity, Veh.Battery_Voltage, Veh.Existing_Meter_Reading, Veh.Kilometer_Pulse, Veh.Driver_Name, Veh.Driver_Mobile_Number, Veh.Driver_Address, Veh.Driving_Licence_Number, Veh.Driving_Licence_Expiry_Date, Veh.Mobile_Number, Veh.SIM_Provider, Veh.GPRS_Activated_Date, Veh.Recharge_Period, Veh.Serial_Number, Veh.Date_of_Purchase, Veh.Dealer_Name,Veh.Rto_Renewal_Date,Veh.Rto_Devision_name,Veh.Overspeed,Veh.Contract_Fromdate,Veh.Contract_Todate,Veh.Contract_Name,Veh.Rto_fc_date,Veh.Emission_date,Veh.Road_tax_date,Veh.SMS_No,Veh.Max_Temp,Veh.milage,Veh.Fuel_type,Veh.Vehicle_Type FROM Veh WHERE Vehicle_number = '" + RegNUM + "' and USER_ID = '" + p_3 + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.Vehicle_number = ds.Tables[0].Rows[0]["Vehicle_number"].ToString();
            this.Vehicle_Reg_Name = ds.Tables[0].Rows[0]["Vehicle_Reg_Name"].ToString();
            this.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
            this.Driver_Name = ds.Tables[0].Rows[0]["Driver_Name"].ToString();
            this.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model"].ToString();
            this.Driver_Mobile_Number = ds.Tables[0].Rows[0]["Driver_Mobile_Number"].ToString();
            this.Vehicle_Make = ds.Tables[0].Rows[0]["Vehicle_Make"].ToString();
            this.Driver_Address = ds.Tables[0].Rows[0]["Driver_Address"].ToString();
            this.Vehicle_Insurance_Number = ds.Tables[0].Rows[0]["Vehicle_Insurance_Number"].ToString();
            this.Driving_Licence_Number = ds.Tables[0].Rows[0]["Driving_Licence_Number"].ToString();
            this.Vehicle_Insurance_Expiry_Date = ds.Tables[0].Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            this.Driving_Licence_Expiry_Date = ds.Tables[0].Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            this.Tank_Capacity = ds.Tables[0].Rows[0]["Tank_Capacity"].ToString();
            this.Battery_Voltage = ds.Tables[0].Rows[0]["Battery_Voltage"].ToString();
            this.Existing_Meter_Reading = ds.Tables[0].Rows[0]["Existing_Meter_Reading"].ToString();
            this.Kilometer_Pulse = ds.Tables[0].Rows[0]["Kilometer_Pulse"].ToString();
            this.Mobile_Number = ds.Tables[0].Rows[0]["Mobile_Number"].ToString();
            this.Serial_Number = ds.Tables[0].Rows[0]["Serial_Number"].ToString();
            this.SIM_Provider = ds.Tables[0].Rows[0]["SIM_Provider"].ToString();
            this.Date_of_Purchase = ds.Tables[0].Rows[0]["Date_of_Purchase"].ToString();
            this.GPRS_Activated_Date = ds.Tables[0].Rows[0]["GPRS_Activated_Date"].ToString();
            this.Dealer_Name = ds.Tables[0].Rows[0]["Dealer_Name"].ToString();
            this.Recharge_Period = ds.Tables[0].Rows[0]["Recharge_Period"].ToString();
            //this.Tank_Type = ds.Tables[0].Rows[0]["Tank_Type"].ToString();
            //this.Fuel_Calibration  = ds.Tables[0].Rows[0]["Fuel_Calibration"].ToString();
            this.milage = ds.Tables[0].Rows[0]["milage"].ToString();
            this.Fuel_Type = ds.Tables[0].Rows[0]["Fuel_Type"].ToString();
            //this.Minusstring = ds.Tables[0].Rows[0]["Minusstring"].ToString();
            this.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type"].ToString();


            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable LoadVeh(string VehRegNo)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Veh.Vehicle_number,Veh.Vehicle_Reg_Name, Veh.User_ID, Veh.Vehicle_Model, Veh.Vehicle_Make, Veh.Vehicle_Insurance_Number, Veh.Vehicle_Insurance_Expiry_Date, Veh.Tank_Capacity, Veh.Battery_Voltage, Veh.Existing_Meter_Reading, Veh.Kilometer_Pulse, Veh.Driver_Name, Veh.Driver_Mobile_Number, Veh.Driver_Address, Veh.Driving_Licence_Number, Veh.Driving_Licence_Expiry_Date, Veh.Mobile_Number, Veh.SIM_Provider, Veh.GPRS_Activated_Date, Veh.Recharge_Period, Veh.Serial_Number, Veh.Date_of_Purchase, Veh.Dealer_Name,Veh.Tank_Type, Veh.Fuel_Calibration,Veh.Rto_Renewal_Date,Veh.Rto_Devision_name,Veh.Overspeed,Veh.Contract_Fromdate,Veh.Contract_Todate,Veh.Contract_Name,Veh.Rto_fc_date,Veh.Emission_date,Veh.Road_tax_date,Veh.SMS_No,Veh.Max_Temp,Veh.Alert_stationary,Veh.Alert_idling,Veh.Alert_moving,Veh.Min_Temp,Veh.Delay_Temp,Veh.Stationary_Fromtime,Veh.Stationary_Totime,Veh.Vehicle_Security_Ftime,Veh.Vehicle_Security_Ttime,Veh.Vehicle_Security_Min,Veh.milage,Veh.Fuel_type,Veh.Minusstring,Veh.Vehicle_Type FROM Veh WHERE Vehicle_number = '" + VehRegNo + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            this.Vehicle_number = ds.Tables[0].Rows[0]["Vehicle_number"].ToString();
            this.Vehicle_Reg_Name = ds.Tables[0].Rows[0]["Vehicle_Reg_Name"].ToString();
            this.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
            this.Driver_Name = ds.Tables[0].Rows[0]["Driver_Name"].ToString();
            this.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model"].ToString();
            this.Driver_Mobile_Number = ds.Tables[0].Rows[0]["Driver_Mobile_Number"].ToString();
            this.Vehicle_Make = ds.Tables[0].Rows[0]["Vehicle_Make"].ToString();
            this.Driver_Address = ds.Tables[0].Rows[0]["Driver_Address"].ToString();
            this.Vehicle_Insurance_Number = ds.Tables[0].Rows[0]["Vehicle_Insurance_Number"].ToString();
            this.Driving_Licence_Number = ds.Tables[0].Rows[0]["Driving_Licence_Number"].ToString();
            this.Vehicle_Insurance_Expiry_Date = ds.Tables[0].Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            this.Driving_Licence_Expiry_Date = ds.Tables[0].Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            this.Tank_Capacity = ds.Tables[0].Rows[0]["Tank_Capacity"].ToString();
            this.Battery_Voltage = ds.Tables[0].Rows[0]["Battery_Voltage"].ToString();
            this.Existing_Meter_Reading = ds.Tables[0].Rows[0]["Existing_Meter_Reading"].ToString();
            this.Kilometer_Pulse = ds.Tables[0].Rows[0]["Kilometer_Pulse"].ToString();
            this.Mobile_Number = ds.Tables[0].Rows[0]["Mobile_Number"].ToString();
            this.Serial_Number = ds.Tables[0].Rows[0]["Serial_Number"].ToString();
            this.SIM_Provider = ds.Tables[0].Rows[0]["SIM_Provider"].ToString();
            this.Date_of_Purchase = ds.Tables[0].Rows[0]["Date_of_Purchase"].ToString();
            this.GPRS_Activated_Date = ds.Tables[0].Rows[0]["GPRS_Activated_Date"].ToString();
            this.Dealer_Name = ds.Tables[0].Rows[0]["Dealer_Name"].ToString();
            this.Recharge_Period = ds.Tables[0].Rows[0]["Recharge_Period"].ToString();
            this.Tank_Type = ds.Tables[0].Rows[0]["Tank_Type"].ToString();
            this.Fuel_Calibration = ds.Tables[0].Rows[0]["Fuel_Calibration"].ToString();
            this.Minusstring = ds.Tables[0].Rows[0]["Minusstring"].ToString();
            this.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type"].ToString();
            //this.ACswitch = ds.Tables[0].Rows[0]["ACswitch"].ToString();




            sqlconn.Close();
            return ds.Tables[0];
        }

        public void UpdateVehicle(string VN, string VRegNo, string VehNo, string txtVehRegName, string UID, string txtVehModel, string txtVehMake, string txtVehInsNum, string txtTankCapacity, string Voltage, string txtExistMetRead, string txtKiloPulse, string txtDriverName, string txtDriverMobNum, string txtDriverADD, string txtDrivLicNum, string txtMobNum, string txtSIMProvider, string Recharge, string txtSNum, string txtDOP, string txtDealerName, string rtoname, string smsno, string milage, string Fuel_type, string Vehicle_Type)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Veh set [Vehicle_number]='" + VehNo + "',[Vehicle_Reg_Name]='" + txtVehRegName + "',[Vehicle_Model]='" + txtVehModel + "',[Vehicle_Make]='" + txtVehMake + "',[Vehicle_Insurance_Number]='" + txtVehInsNum + "',[Tank_Capacity]='" + txtTankCapacity + "',[Battery_Voltage]='" + Voltage + "',[Existing_Meter_Reading]='" + txtExistMetRead + "',[Kilometer_Pulse]='" + txtKiloPulse + "',[Driver_Name]='" + txtDriverName + "',[Driver_Mobile_Number]='" + txtDriverMobNum + "',[Driver_Address]='" + txtDriverADD + "',[Driving_Licence_Number]='" + txtDrivLicNum + "',[Mobile_Number]='" + txtMobNum + "',[SIM_Provider]='" + txtSIMProvider + "',[Recharge_Period]='" + Recharge + "',[Serial_Number]='" + txtSNum + "',[Date_of_Purchase]='" + txtDOP + "',[Dealer_Name]='" + txtDealerName + "',[Rto_Devision_name]='" + rtoname + "',[SMS_No]='" + smsno + "',[milage]='" + milage + "',[Fuel_type]='" + Fuel_type + "',[Vehicle_Type]='" + Vehicle_Type + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }

        public void UpdateVeh(string VN, string VRegNo, string VehNo, string txtVehRegName, string UID, string txtVehModel, string txtVehMake, string txtVehInsNum, string txtTankCapacity, string Voltage, string txtExistMetRead, string txtKiloPulse, string txtDriverName, string txtDriverMobNum, string txtDriverADD, string txtDrivLicNum, string txtMobNum, string txtSIMProvider, string Recharge, string txtSNum, string txtDOP, string txtDealerName, string rtoname, string smsno, string milage, string Fuel_type, string Vehicle_Type)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Vehicles set [Vehicle_number]='" + VehNo + "',[Vehicle_Reg_Name]='" + txtVehRegName + "',[Vehicle_Model]='" + txtVehModel + "',[Vehicle_Make]='" + txtVehMake + "',[Vehicle_Insurance_Number]='" + txtVehInsNum + "',[Tank_Capacity]='" + txtTankCapacity + "',[Battery_Voltage]='" + Voltage + "',[Existing_Meter_Reading]='" + txtExistMetRead + "',[Kilometer_Pulse]='" + txtKiloPulse + "',[Driver_Name]='" + txtDriverName + "',[Driver_Mobile_Number]='" + txtDriverMobNum + "',[Driver_Address]='" + txtDriverADD + "',[Driving_Licence_Number]='" + txtDrivLicNum + "',[Mobile_Number]='" + txtMobNum + "',[SIM_Provider]='" + txtSIMProvider + "',[Recharge_Period]='" + Recharge + "',[Serial_Number]='" + txtSNum + "',[Date_of_Purchase]='" + txtDOP + "',[Dealer_Name]='" + txtDealerName + "',[Rto_Devision_name]='" + rtoname + "',[SMS_No]='" + smsno + "',[milage]='" + milage + "',[Fuel_type]='" + Fuel_type + "',[Vehicle_Type]='" + Vehicle_Type + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public DataTable GerDriverDetails(string p, string p_2)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT   Vehicles.Driver_Name, Vehicles.Vehicle_Reg_Name, Vehicles.Driver_Mobile_Number, Vehicles.Vehicle_Model, Vehicles.Kilometer_Pulse, Vehicles.Tank_Capacity, Vehicles.Battery_Voltage, Vehicles.Tank_Type, Vehicles.Fuel_Calibration,Vehicles.Max_Temp,Vehicles.Min_Temp FROM Vehicles WHERE Vehicle_number = '" + p + "'", sqlconn);//"' and USER_ID = '" + p_2 +
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getblackspot1(string vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select count(*) from track where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and gtim between '" + fdate + "' and '" + tdate + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable getblackspot(string vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and gtim between '" + fdate + "' and '" + tdate + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }

        public DataTable search(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {

            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            //sql = sql + " IGST = 1 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' ";

            return ds_FindR(sql);
        }
        public DataTable startfuelstaus(string vid, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from tracking where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and sped <=6 and igst!=0 and fuel_adc!=0 order by gtim asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Endfuelstaus(string vid, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from tracking where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and sped <=6 and igst!=0 and fuel_adc!=0 order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable startfuelstausold(string vid, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from OldTrack where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and sped <=6 and igst!=0 and fuel_adc!=0 order by gtim asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Endfuelstausold(string vid, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from OldTrack where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and sped <=6 and igst!=0 and fuel_adc!=0 order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }


        private DataTable ds_FindR(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,[PULSE_CTR],[FUEL_ADC]  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        


        public DataTable fuzio_getbygtim(string vno,string date, string tablename)
        {
            
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,[PULSE_CTR],[FUEL_ADC]  FROM ["+tablename+"] inner join Vehicles_Device_Rel on XXXM = Device_ID where VehicalNumber='"+vno+"' and GTIM between '"+date+ " 12:00 AM' and  '" + date + " 11:59 PM' order by GTIM";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        


        public DataTable fuzio_daysummary(string vno,string date, string tablename)
        {
            
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            DateTime date1 = Convert.ToDateTime(date);
            DateTime date2 = date1.AddDays(1);
            String date3 = Convert.ToDateTime(date1).ToString("MM/dd/yyyy"); 
            String date4 = Convert.ToDateTime(date2).ToString("MM/dd/yyyy");


            string sql = "SELECT * from "+tablename+ " where VehicalNumber='" + vno+ "' and Date between '" + date3+ "' and  '" + date4 + "' order by DateNew asc";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        public DataTable fuzio_getbygtim2(string vno, string fdate, string tdate, string tablename)
        {

            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,[PULSE_CTR],[FUEL_ADC]  FROM [" + tablename + "] inner join Vehicles_Device_Rel on XXXM = Device_ID where VehicalNumber='" + vno + "' and GTIM between '" + fdate + "' and  '" + tdate + "' order by GTIM";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }



        public DataTable getTOTKMR(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select coalesce(SUM(PULSE_CTR),0) as PULSE_CTR from Tracking where PULSE_CTR >= 100000 and XXXM = '" + VID + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable getOlPrev(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select  top 1 PULSE_CTR, GTIM from Tracking where PULSE_CTR >= 0 and XXXM = '" + VID + "'order by GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        //public DataTable getOlTo(string VID)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("select  top 1 PULSE_CTR, GTIM from Tracking where PULSE_CTR >= 0 and XXXM = '" + VID + "'order by GTIM desc", sqlconn);
        //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    adp.Fill(ds);
        //    sqlconn.Close();
        //    return ds.Tables[0];
        //}
        public void Correct(string customerID, string VehNo, string CDT, string Type, string CV, string cfv)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Correction_Log([CORRECTION_TYPE],[CORRECTION_Value],[CORRECTION_DT],[CORRECTION_VEH],[CORRECTION_VALNOW],[CORRECTION_CUMU]) values('"
                + Type + "','" + CV + "','" + CDT + "','" + VehNo + "','" + cfv + "','" + CV + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public DataTable Cor(string VehNo)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from Correction_Log where  CORRECTION_VEH = '" + VehNo + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable EMR(string VehNo)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Existing_Meter_Reading from Vehicles where  Vehicle_number = '" + VehNo + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public void UpdateNowEMR(string customerID, string VehNo, string CDT, string N)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Correction_Log set [CORRECTION_VALNOW]='" + N + "' where [CORRECTION_VEH]='" + VehNo + "' and [CORRECTION_DT]='" + CDT + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public DataTable SelectNowEMR(string customerID, string VehNo)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 CORRECTION_VALNOW,CORRECTION_DT From (select Top 2 CORRECTION_VALNOW,CORRECTION_DT from Correction_Log where CORRECTION_VEH='" + VehNo + "' ORDER BY CORRECTION_DT DESC) x ORDER BY CORRECTION_DT", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable SelectNEMR(string p, string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 CASE WHEN CORRECTION_VALNOW IS NULL THEN 0 ELSE CORRECTION_VALNOW END from Correction_Log where  CORRECTION_VEH = '" + VID + "' order by CORRECTION_DT desc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        //-------------------------------For Engine Moving, Stationary and Idle-----------------------------------------------------------------------


        internal DataTable GetTwoDT3(string VehID, DateTime SDT, DateTime PrDT, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select DATEDIFF(hh, '" + PrDT + "', '" + SDT + "') as Hours,DATEDIFF(mi,DATEADD(hh,DATEDIFF(hh, '" + PrDT + "', '" + SDT + "'),'" + PrDT + "'),'" + SDT + "') as Minutes from Tracking where XXXM='" + VehID + "'", sqlconn);//and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPrevIGST(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST From (select Top 2 GTIM,IGST from Tracking where XXXM='" + VehID + "' ORDER BY GTIM DESC) x ORDER BY GTIM ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviIGST(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM ASC", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];

            //select top 1 GTIM,IGST from Tracking where IGST='1' order by GTIM desc
        }

        internal DataTable GetPreviIGST1(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];

        }

        internal DataTable GetPS(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select top 1 SPED,GTIM,IGST from Tracking where IGST='0' and XXXM='" + VehID + "' and SPED = '0' Order by GTIM desc", sqlconn);
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 SPED,GTIM,IGST From (select Top 2 SPED,GTIM,IGST from Tracking where XXXM='" + VehID + "' ORDER BY GTIM DESC) x ORDER BY GTIM", sqlconn);
            //SqlCommand cmd = new SqlCommand("select top 1 SPED,GTIM,IGST from Tracking where IGST = '1' and XXXM='" + VehID + "' and SPED = '0' ORDER BY GTIM Desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST2(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='0' ORDER BY GTIM DESC", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST11(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM Desc", sqlconn);//DESC
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' and SPED!=0 ORDER BY GTIM Desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviIGST121(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM Desc", sqlconn);//DESC
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM Desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        internal DataTable GetPreviIGST12(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' and SPED != '0' ORDER BY GTIM desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST22(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            // SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='0' ORDER BY GTIM asc", sqlconn);//DESC
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "'  and (igst='0' or sped!='0' and igst='1') ORDER BY GTIM desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviIGST221(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='0' ORDER BY GTIM asc", sqlconn);//DESC
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "'  and (igst='0' or sped!='0' and igst='1') ORDER BY GTIM desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviING1(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Top 1 GTIM,IGST from tracking where XXXM=" + VehID + " and ((igst='0') or (sped>3 and igst='1')) order by gtim desc ", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST33(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM='" + VehID + "'and IGST ='1' and SPED = '0'  ORDER BY GTIM asc", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPSA(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select SPED,GTIM,IGST from Tracking where IGST='1' and XXXM='" + VehID + "' and SPED = '0' Order by GTIM asc", sqlconn);
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 SPED,GTIM,IGST From (select Top 2 SPED,GTIM,IGST from Tracking where XXXM='" + VehID + "' ORDER BY GTIM DESC) x ORDER BY GTIM", sqlconn);
            //SqlCommand cmd = new SqlCommand("select top 1 SPED,GTIM,IGST from Tracking where IGST = '1' and XXXM='" + VehID + "' and SPED = '0' ORDER BY GTIM Desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST32(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST ='1'and SPED = '0' ORDER BY GTIM desc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST45(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM='" + VehID + "'ORDER BY GTIM asc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetPreviIGST43(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST ='1'and SPED != '0' ORDER BY GTIM asc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        //--------------------------------------------------------------VES RPT--------------------------
        internal DataTable searchOn2(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 0 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED = '0' order by GTIM desc,NewID desc";

            return ds_Find2(sql);
        }
        private DataTable ds_Find2(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //top 1
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable searchOn3(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 1 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED = '0' order by GTIM desc,NewID desc";

            return ds_Find3(sql);
        }
        private DataTable ds_Find3(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        internal DataTable GetPreviIGST121(string VehID, DateTime IMP, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' and SPED != '0' and GTIM < '" + IMP + "' ", sqlconn);//ORDER BY GTIM desc
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        //-----------------------------------------------


        internal DataTable VehEngStat(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 1 " + " and SPED > 0 and";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' ";

            return ds_FindS(sql);
        }

        private DataTable ds_FindS(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable VehEngStat2(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 0 " + " and SPED = 0 and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' ";

            return ds_FindS2(sql);
        }

        private DataTable ds_FindS2(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable VehEngStat3(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 1 " + " and SPED = 0 and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' ";

            return ds_FindS3(sql);
        }

        private DataTable ds_FindS3(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [Tracking] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public void FuelF(string RegNUM, DateTime FDT, string FC, string RefChaNo, string StNa)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Fuel_Entry([Fuel_Vehicle],[Fuel_DT],[Fuel_Capacity],[Fuel_Challan_No],[Fuel_Station_Name]) values('"
                + RegNUM + "','" + FDT + "','" + FC + "','" + RefChaNo + "','" + StNa + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delete_fuelentry(string RegNUM, string FDT)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Fuel_Entry where Fuel_Vehicle= '" + RegNUM + "' AND Fuel_DT ='" + FDT + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }

        internal DataSet SelectFF(string RegNUM)//, DateTime FDT, string FC)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Fuel_Vehicle],[Fuel_DT],[Fuel_Capacity]  FROM [Fuel_Entry] where Fuel_Vehicle = '" + RegNUM + "'", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds;
        }

        internal DataTable SelectFFTOT(string RegNUM, DateTime FT2)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT SUM(Fuel_Capacity)  FROM [Fuel_Entry] where Fuel_Vehicle = '" + RegNUM + "' and Fuel_DT='" + FT2 + "'", sqlconn);//DESC
            //SqlCommand cmd = new SqlCommand("SELECT [Fuel_Vehicle],[Fuel_DT],[Fuel_Capacity]  FROM [Fuel_Entry] where Fuel_Vehicle = '" + RegNUM + "'", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable GetFuRec(string RegNUM, DateTime FuRecDT)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Convert(varchar, Fuel_DT ,110) as Date,Fuel_Challan_No,Fuel_Capacity,Fuel_Station_Name  FROM [Fuel_Entry] where Fuel_Vehicle = '" + RegNUM + "' and Fuel_DT='" + FuRecDT + "'", sqlconn);//DESC
            //SqlCommand cmd = new SqlCommand("SELECT [Fuel_Vehicle],[Fuel_DT],[Fuel_Capacity]  FROM [Fuel_Entry] where Fuel_Vehicle = '" + RegNUM + "'", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        public DataTable getPFE(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Existing_Meter_Reading,Date_of_Purchase from Vehicles where  Vehicle_number = '" + VID + "'", sqlconn);//DESC

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable MTR(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Existing_Meter_Reading from Vehicles where  Vehicle_number = '" + VID + "'", sqlconn);//DESC

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable CCor(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from Correction_Log where  CORRECTION_VEH = '" + VID + "'", sqlconn);//DESC

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable APrevCor(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 CORRECTION_VALNOW,CORRECTION_DT,CORRECTION_CUMU From (select Top 2 CORRECTION_VALNOW,CORRECTION_DT,CORRECTION_CUMU from Correction_Log where CORRECTION_VEH='" + VID + "' ORDER BY CORRECTION_DT DESC) x ORDER BY CORRECTION_DT DESC", sqlconn);//DESC

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public void CorrectCumu(string customerID, string VehNo, string CDT, string Type, string CV, double CumCor, string cfv)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Correction_Log([CORRECTION_TYPE],[CORRECTION_Value],[CORRECTION_DT],[CORRECTION_VEH],[CORRECTION_VALNOW],[CORRECTION_CUMU]) values('"
                + Type + "','" + CV + "','" + CDT + "','" + VehNo + "','" + cfv + "','" + CumCor + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable Get_Correction_KM(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select sum(CORRECTION_VALNOW) as CORRECTION_VALNOW from Correction_Log where CORRECTION_VEH ='" + vno + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable chkV(string venu)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT ID,[VehicalNumber],[Device_ID] FROM [Vehicles_Device_Rel] where VehicalNumber ='" + venu + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }


        public DataTable getFperM(DateTime firstDate, DateTime lastDate, string RegNUM)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select sum(Fuel_Capacity) Total_Fuel from Fuel_Entry where Fuel_DT >='" + firstDate + "' and Fuel_DT <='" + lastDate + "' and Fuel_Vehicle = '" + RegNUM + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable DriverDetails(string VID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT   Vehicles.Driver_Name, Vehicles.Vehicle_Reg_Name, Vehicles.Driver_Mobile_Number, Vehicles.Vehicle_Make, Vehicles.Kilometer_Pulse FROM Vehicles WHERE Vehicle_number = '" + VID + "'", sqlconn);//"' and USER_ID = '" + p_2 +
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable getRhours()
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Fhours,Thours from Rhours", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }

        public DataTable getMaxsped(string p, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select SPED,GTIM from Tracking where SPED=(select max(SPED) from Tracking where XXXM=" + p + " and GTIM between '" + fdate + "' and '" + tdate + "')and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            //SqlCommand cmd = new SqlCommand("select gtim,sped from tracking where sped=(select max(sped) from tracking where xxxm='" + p + "') and  gtim between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }

        public DataTable getTOTKMR1(string vno, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select coalesce(SUM(PULSE_CTR),0) as PULSE_CTR from Tracking where PULSE_CTR >= 100000 and XXXM = '" + vno + "'and GTIM<='" + date + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getprevfuelstaus(string vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and sped<=6 and igst!=0 and fuel_adc!=0 and GTIM between '" + fdate + "' and '" + tdate + "' order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetVehiclesfuel(string vid, string p)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Tank_Capacity,Battery_Voltage,Fuel_Calibration,Tank_Type,Minusstring from Vehicles where Vehicle_number='" + vid + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }



        public DataTable getMaxspedandtime(string p, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top 1 gtim, sped from tracking where xxxm = '" + p + "' and  gtim between '" + fdate + "' and '" + tdate + "' order by sped desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        

        public DataTable getMaxsped1(string p, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select max(SPED) as sped from Tracking where XXXM=" + p + " and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            //SqlCommand cmd = new SqlCommand("select gtim,sped from tracking where sped=(select max(sped) from tracking where xxxm='" + p + "') and  gtim between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }



        public DataTable getMaxspedtime(string p, string fdate, string tdate, string sped)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select gtim from tracking where xxxm=" + p + " and sped=" + sped + " and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            //SqlCommand cmd = new SqlCommand("select gtim,sped from tracking where sped=(select max(sped) from tracking where xxxm='" + p + "') and  gtim between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable getgeo(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top(10) LAMI,LGMI,Radius,Date,Ftime,Ttime,User_ID,Device_ID,ID,VehicleNumber from Geo where VehicleNumber='" + vno + "'  and Status=" + 1 + " order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public void setgeo(double lami, double lgmi, double radius, string date, string fdate, string tdate, string vno, int uid, int did)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Geo([LAMI],[LGMI],[Radius],[Date],[Ftime],[Ttime],[VehicleNumber],[User_ID],[Device_ID],[Status]) values('" + lami + "','" + lgmi + "','" + radius + "','" + date + "','" + fdate + "','" + tdate + "','" + vno + "'," + uid + "," + did + "," + 1 + ")", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable getgeonof(string vno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select count(*) as Noof from Geo where VehicleNumber='" + vno + "' and User_ID=" + uid + " and Status=" + 1 + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public void deletegeo(string id)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Update Geo set Status=" + 0 + " where ID=" + id + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        internal DataTable GetStatus(string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Track INNER JOIN Vehicles_Device_Rel ON Track.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Offline INNER JOIN Vehicles_Device_Rel ON Offline.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetStatusoffff(string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Trackoff INNER JOIN Vehicles_Device_Rel ON Trackoff.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Offline INNER JOIN Vehicles_Device_Rel ON Offline.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetStatus12(string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Tracking INNER JOIN Vehicles_Device_Rel ON Tracking.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,Vehicles_Device_Rel.VehicalNumber FROM Offline INNER JOIN Vehicles_Device_Rel ON Offline.XXXM = Vehicles_Device_Rel.Device_ID inner join Users on users.id = Vehicles_Device_Rel.user_id where VehicalNumber='" + VN + "' order by GTIM desc", sqlconn);

            //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getserialno(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select distinct Serial_no from Devices inner join Vehicles_Device_Rel  on Vehicles_Device_Rel.Device_ID =Devices.Device_ID where VehicalNumber='" + vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getgeotrack1(string Did, string ftime, string ttime)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,GTIM from tracking where xxxm=" + Did + " and gtim between '" + ftime + "' and'" + ttime + "' order by gtim asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable getdevicesid(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable geocodestatus(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Date from Geo where VehicleNumber ='" + vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable geotripcodestatus(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from GeoTrip where VehicleNumber ='" + vno + "' and Status=" + 1 + " ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable prevfuelstaus(string vid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from track where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and sped <=6 and igst!=0 and fuel_adc!=0 order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable vehicledetailsalerts(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Vehicle_Insurance_Expiry_Date,Rto_Renewal_Date,Driving_Licence_Expiry_Date,Overspeed,Contract_Todate,GPRS_Activated_Date,Rto_fc_date,Emission_date,Road_tax_date,Max_Temp,Alert_stationary,Alert_idling,Alert_moving,Min_Temp,Delay_Temp,Stationary_Fromtime,Stationary_Totime,Vehicle_Security_Ftime,Vehicle_Security_Ttime,Vehicle_Security_Min from Vehicles where Vehicle_number='" + vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public void Delete_Monthtemp(string vid, string month)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Delete from Month_Temp where Vehicle_number='" + vid + "' AND Month='" + month + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public int Temp_alert2(string Vno, string min, string max)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Alert_temp", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@mintemp", min));
            cmd.Parameters.Add(new SqlParameter("@maxtemp", max));
            cmd.Parameters.Add("@totalmin", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            if (cmd.Parameters["@totalmin"].Value != DBNull.Value)
            {
                return Convert.ToInt32(cmd.Parameters["@totalmin"].Value); ;
            }
            else
            {
                return 0;
            }

        }
        public int Temp_alert3(string Vno, string min, string max)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Alert_temp1", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@mintemp", min));
            cmd.Parameters.Add(new SqlParameter("@maxtemp", max));
            cmd.Parameters.Add("@totalmin", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            if (cmd.Parameters["@totalmin"].Value != DBNull.Value)
            {
                return Convert.ToInt32(cmd.Parameters["@totalmin"].Value);
            }
            else
            {
                return 0;
            }

        }
        public DataTable getoverspeeddetails11(string dno, string fdate, string tdate, string ospeed)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select SPED,GTIM from Track where XXXM=" + dno + " and SPED > " + ospeed + " and GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable getoverspeeddetails(string dno, string fdate, string tdate, string ospeed)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top(1) SPED,GTIM from Track where XXXM=" + dno + " and SPED > " + ospeed + " and GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable getalertsforspeed(string vno, string fdate, string tdate, string speed)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and sped >" + speed + " and gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable getgeotripnof(string vno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select count(*) as Noof from GeoTrip where VehicleNumber='" + vno + "' and User_ID=" + uid + " and Status=" + 1 + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable getgeotrip(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top(2) LAMI,LGMI,Radius,VehicleNumber,User_ID,Device_ID,Status,Device_ID,Location from GeoTrip where VehicleNumber='" + vno + "' and Status=" + 1 + " order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public void setgeotrip(double lami, double lgmi, double radius, string vno, int uid, int did, string loc)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into GeoTrip([LAMI],[LGMI],[Radius],[VehicleNumber],[User_ID],[Device_ID],[Status],[Location]) values('" + lami + "','" + lgmi + "','" + radius + "','" + vno + "'," + uid + "," + did + "," + 1 + ",'" + loc + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void Deletetrip(string vid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Update GeoTrip set Status=" + 0 + " where VehicleNumber='" + vid + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void setmultitrack(string vno, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into MultiTrack([VehicalNumber],[User_ID]) values('" + vno + "'," + uid + ")", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable getmultitrack(string uid, string Urole)
        {
            string query = "";
            query = "User_ID=" + uid + "";
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select  distinct VehicalNumber from MultiTrack where " + query + "", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable multitrackdetails(string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select  top(1) LAMI,LGMI,SPED,GTIM,IGST from tracking where XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') order by GTIM desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable multitrackdetailsoff(string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select  top(1) LAMI,LGMI,SPED,GTIM,IGST from Trackoff where XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') order by GTIM desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public void delmultitrack(string vno, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Delete from MultiTrack where VehicalNumber='" + vno + "' and User_ID=" + uid + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delmultitrack1(string uid, string Urole)
        {
            string query = "";
            //if (Convert.ToInt32(Urole)==3)
            //{               
            //    DataTable CUsers =Get_CAdminUsers(uid, 0);
            //    for (int i = 0; i < CUsers.Rows.Count; i++)
            //    {
            //        if (i == 0)
            //        {
            //            query = "User_ID = " + CUsers.Rows[i][0].ToString() + "";
            //        }
            //        else { query += " OR User_ID = " + CUsers.Rows[i][0].ToString() + ""; ; }
            //    }

            //}else 
            query = "User_ID=" + uid + "";
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Delete from MultiTrack where " + query + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable getgeodatenof(string vno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select count(*) as Noof from GeoDate where VehicleNumber='" + vno + "' and User_ID=" + uid + " and Status=" + 1 + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public void setgeodate(double lami, double lgmi, double radius, string date, string vno, int uid, int did)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into GeoDate([LAMI],[LGMI],[Radius],[Date],[VehicleNumber],[User_ID],[Device_ID],[Status]) values('" + lami + "','" + lgmi + "','" + radius + "','" + date + "','" + vno + "'," + uid + "," + did + "," + 1 + ")", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void deletegeodate(string id)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Update GeoDate set Status=" + 0 + " where ID=" + id + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public DataTable getgeodate(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top(10) LAMI,LGMI,Radius,Date,User_ID,Device_ID,ID,VehicleNumber from GeoDate where VehicleNumber='" + vno + "'  and Status=" + 1 + " order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable firstigston(string fdate, string tdate, string did)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(1) LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=" + did + " and IGST=1 and SPED>0 and  gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable igstondetails(string fdate, string tdate, string did)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=" + did + " and  GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable totaligston(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from tracking where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=1 and SPED>1 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable totalidealton(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from tracking where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=1 and SPED=0 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }

        public DataTable totaligstoff(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from tracking where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=0  and SPED=0 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }

        public string getgeoaddress(double lt, double lng)
        {
            string geocoderUri1 = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=true", lt, lng);
            XmlDocument geocoderXmlDoc1 = new XmlDocument();
            geocoderXmlDoc1.Load(geocoderUri1);

            string status1 = geocoderXmlDoc1.DocumentElement
                .SelectSingleNode(@"//status").InnerText;
            string st = "";
            if (status1 == "OK")
            {
                st = geocoderXmlDoc1.DocumentElement
                    .SelectSingleNode(@"//formatted_address").InnerText;
            }
            return st;

        }
        public string getdistance(string l1, string l2)
        {
            string geocoderUri1 = string.Format("http://maps.googleapis.com/maps/api/directions/xml?origin={0}&destination={1}&sensor=true", l1, l2);
            XmlDocument geocoderXmlDoc1 = new XmlDocument();
            geocoderXmlDoc1.Load(geocoderUri1);

            string status1 = geocoderXmlDoc1.DocumentElement
                 .SelectSingleNode(@"//status").InnerText;
            string st = "";
            if (status1 == "OK")
            {
                XmlNodeList xmxlist = geocoderXmlDoc1.SelectNodes(@"//route//leg//distance//text");
                foreach (XmlNode xn in xmxlist)
                {
                    st = xn.InnerText;
                }

            }
            return st;

        }
        public DataTable gettotalkm(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select * from TotalKM where VehicalNumber='" + vno + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void settotalkm(string vno, string fdate, string tdate, string km)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into TotalKM([VehicalNumber],[FDate],[TDate],[KM],[Kmdate]) values('" + vno + "','" + fdate + "','" + tdate + "','" + km + "','" + fdate + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable getTotalRunkm(string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select * from TotalRunkm where VehicalNumber='" + vno + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void uptotalkm(string vno, string fdate, string tdate, string km, string fkm)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update TotalKM set [FDate]='" + fdate + "',[TDate]='" + tdate + "',[KM]='" + km + "',[Kmdate]='" + fkm + "' where [VehicalNumber]='" + vno + "'", sqlconn);//) values('"
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }



        public DataTable Select_TotalRunkm_Fuzio(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select * from Fuzio_TKMR where VehicalNumber='" + Vno + "' and Date BETWEEN '"+ fdate + "' AND '"+ tdate + "' order by Date Asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }

        public DataTable Fuzio_hourlykm(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select * from Fuzio_TKMRH where VehicalNumber='" + Vno + "' and Date BETWEEN '"+ fdate + "' AND '"+ tdate + "' order by ID desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }

        public DataTable fuzio_get_temp(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select * from Fuzio_TEMPDAILY where VehicalNumber='" + Vno + "' and Date BETWEEN '"+ fdate + "' AND '"+ tdate + "' order by ID desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }

        public DataTable Select_TotalRunkm(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_TotalRunkm ", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }



        public DataTable Select_TotalRunkm11(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_TotalRunkm11 ", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_tkm(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select_tkm ", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public void IU_Totalrunkm(string Vno, string fdate, string rkm)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("IU_TotalRunkm", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@FDate", fdate));
            cmd.Parameters.Add(new SqlParameter("@KM ", rkm));
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void uptotalkm1(string vno, string km)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update TotalKM set [KM]='" + km + "' where [VehicalNumber]='" + vno + "'", sqlconn);//) values('"
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable firigon(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(1) LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "')and IGST=1 and  gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable details(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and  GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Select_Temp(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_temparature", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Check_vehicle_Date(DateTime date, string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlCommand cmd = new SqlCommand("select * from Month_KMRun where Vehicle_number='" + vno + "' and Month_Date= '" + date + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }


        //public void UpdateVehicle1(string VN, string txtVehInsExpDt, string txtDrivLic, string rtodate, string ospeed, string conf, string cont, string conname, string fcdate, string ems, string roadtax, string maxtemp, string vs, string vi, string vm, string mintemp, string delaytime, string gpstime, string sft, string stt, string saft, string satt, string samin)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("update Veh set [Vehicle_Insurance_Expiry_Date]='" + txtVehInsExpDt + "',[Rto_Renewal_Date]='" + rtodate + "',[Driving_Licence_Expiry_Date]='" + txtDrivLic + "',[Overspeed]='" + ospeed + "',[Contract_Fromdate]='" + conf + "',[Contract_Todate]='" + cont + "',[Contract_Name]='" + conname + "',[Rto_fc_date]='" + fcdate + "',[Emission_date]='" + ems + "',[Road_tax_date]='" + roadtax + "',[Max_Temp]='" + maxtemp + "',[Alert_stationary]='" + vs + "',[Alert_idling]='" + vi + "',[Alert_moving]='" + vm + "',[Min_Temp]='" + mintemp + "',[Delay_Temp]='" + delaytime + "',[GPRS_Activated_Date]='" + gpstime + "',[Stationary_Fromtime]='" + sft + "',[Stationary_Totime]='" + stt + "',[Vehicle_Security_Ftime]='" + saft + "',[Vehicle_Security_Ttime]='" + satt + "',[Vehicle_Security_Min]='" + samin + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
        //    cmd.ExecuteNonQuery();
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();

        //}
        //public void UpdateVeh1(string VN, string txtVehInsExpDt, string txtDrivLic, string rtodate, string ospeed, string conf, string cont, string conname, string fcdate, string ems, string roadtax, string maxtemp, string vs, string vi, string vm, string mintemp, string delaytime, string gpstime, string sft, string stt, string saft, string satt, string samin)
        //{
        //    SqlConnection sqlconn = new SqlConnection(connectionstring);
        //    sqlconn.Open();
        //    SqlCommand cmd = new SqlCommand("update Vehicles set [Vehicle_Insurance_Expiry_Date]='" + txtVehInsExpDt + "',[Rto_Renewal_Date]='" + rtodate + "',[Driving_Licence_Expiry_Date]='" + txtDrivLic + "',[Overspeed]='" + ospeed + "',[Contract_Fromdate]='" + conf + "',[Contract_Todate]='" + cont + "',[Contract_Name]='" + conname + "',[Rto_fc_date]='" + fcdate + "',[Emission_date]='" + ems + "',[Road_tax_date]='" + roadtax + "',[Max_Temp]='" + maxtemp + "',[Alert_stationary]='" + vs + "',[Alert_idling]='" + vi + "',[Alert_moving]='" + vm + "',[Min_Temp]='" + mintemp + "',[Delay_Temp]='" + delaytime + "',[GPRS_Activated_Date]='" + gpstime + "',[Stationary_Fromtime]='" + sft + "',[Stationary_Totime]='" + stt + "',[Vehicle_Security_Ftime]='" + saft + "',[Vehicle_Security_Ttime]='" + satt + "',[Vehicle_Security_Min]='" + samin + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
        //    cmd.ExecuteNonQuery();
        //    if (sqlconn.State == ConnectionState.Open)
        //        sqlconn.Close();
        //}

        public void UpdateVehicle1(string VN, string txtVehInsExpDt, string txtDrivLic, string rtodate, string conf, string cont, string conname, string fcdate, string ems, string roadtax, string gpstime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Veh set [Vehicle_Insurance_Expiry_Date]='" + txtVehInsExpDt + "',[Rto_Renewal_Date]='" + rtodate + "',[Driving_Licence_Expiry_Date]='" + txtDrivLic + "',[Contract_Fromdate]='" + conf + "',[Contract_Todate]='" + cont + "',[Contract_Name]='" + conname + "',[Rto_fc_date]='" + fcdate + "',[Emission_date]='" + ems + "',[Road_tax_date]='" + roadtax + "',[GPRS_Activated_Date]='" + gpstime + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void UpdateVeh1(string VN, string txtVehInsExpDt, string txtDrivLic, string rtodate, string conf, string cont, string conname, string fcdate, string ems, string roadtax, string gpstime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Vehicles set [Vehicle_Insurance_Expiry_Date]='" + txtVehInsExpDt + "',[Rto_Renewal_Date]='" + rtodate + "',[Driving_Licence_Expiry_Date]='" + txtDrivLic + "',[Contract_Fromdate]='" + conf + "',[Contract_Todate]='" + cont + "',[Contract_Name]='" + conname + "',[Rto_fc_date]='" + fcdate + "',[Emission_date]='" + ems + "',[Road_tax_date]='" + roadtax + "',[GPRS_Activated_Date]='" + gpstime + "' where [Vehicle_number]='" + VN + "'", sqlconn);//VRegNo
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable Temp_alert(string fdate, string tdate, string vid, double temp)
        {

            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top 1 GTIM,COND_INPUTS from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and " + temp + "<(select right(COND_INPUTS,3))and GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];


        }
        public DataTable Temp_alert1(string fdate, string tdate, string vid, double temp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select top 1 GTIM,COND_INPUTS from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and " + temp + ">(select right(COND_INPUTS,3))and GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        internal DataTable GetPreviING(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Top 1 GTIM,IGST from tracking where XXXM=" + VehID + " and (igst='0' or sped!='0' and igst='1') order by gtim desc ", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviMNG(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM=" + VehID + " and (SPED = '0' and IGST ='1' or IGST='0') ORDER BY GTIM desc", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviMNG1(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' and SPED != '0' ORDER BY GTIM asc", sqlconn);//DESC
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviMNG2(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top 1 GTIM,IGST from Tracking where XXXM=" + VehID + " and ((SPED<3  and IGST ='1') or (IGST='0')) ORDER BY GTIM desc", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_TempMonth(string Vno, string month)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SP_Month_temp", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Month", month));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public void Insert_Monthtemp(string vid, string date, string month)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Month_Temp([Vehicle_number],[Month],[Date]) values('" + vid + "'," + month + ",'" + date + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void Update_TempMonth(string Vno, string month, string value, int con, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update__Month_temp", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Month", month));
            cmd.Parameters.Add(new SqlParameter("@Value", value));
            cmd.Parameters.Add(new SqlParameter("@Con", con));
            cmd.Parameters.Add(new SqlParameter("@Date", date));
            cmd.ExecuteNonQuery();
            sqlconn.Close();

        }
        public DataTable get_TempMonth(string vno, int month)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Date,[1Hour],[2Hour],[3Hour],[4Hour],[5Hour],[6Hour],[7Hour],[8Hour],[9Hour],[10Hour],[11Hour],[12Hour],[13Hour],[14Hour],[15Hour],[16Hour],[17Hour],[18Hour],[19Hour],[20Hour],[21Hour],[22Hour],[23Hour],[24Hour] from Month_Temp where Vehicle_number='" + vno + "' and Month=" + month + "order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void Delete_TempMonth(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete from Month_Temp where Vehicle_number = '" + vno + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public DataTable Temp_Noofalert(string fdate, string tdate, string vid, int maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and ((len(COND_INPUTS)=4 and " + 1200 + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) or (len(COND_INPUTS)<=3 and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,3))))))", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Temp_Noofalert1(string fdate, string tdate, string vid, double maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*)from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and (len(COND_INPUTS)=4) and " + maxtemp + "<(select CONVERT(INT,(right(COND_INPUTS,4)))) ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Temp_Noofalert2(string fdate, string tdate, string vid, double maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            //SqlCommand cmd = new SqlCommand("select count(*)from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and (len(COND_INPUTS)=4) and " + maxtemp + "<(select CONVERT(INT,(right(COND_INPUTS,4)))) AND " + mintemp + "< (select CONVERT(INT,(right(COND_INPUTS,4))))", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*)from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        internal DataTable GetPStatus(string customerID, string VN)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("WITH cte AS(SELECT Top (60) Track.LAMI,Track.LGMI,Track.SPED,Track.BAT_ADC,Track.GTIM,Track.MAIN_BACKUP,Track.PULSE_CTR,Track.NOST,Track.IGST,Track.COND_INPUTS,Track.FUEL_ADC,Track.XXXM,Track.NewID from Track WHERE XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VN + "') order by GTIM desc)SELECT TOP (60) * FROM cte ORDER BY GTIM desc,NewID DESC", sqlconn);
            //SqlCommand cmd = new SqlCommand("WITH cte AS(SELECT Top (9) Tracking.LAMI,Tracking.LGMI,Tracking.SPED,Tracking.BAT_ADC,Tracking.GTIM,Tracking.MAIN_BACKUP,Tracking.PULSE_CTR,Tracking.NOST,Tracking.IGST,Tracking.COND_INPUTS,Tracking.FUEL_ADC,Tracking.XXXM,Tracking.NewID from Tracking WHERE XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VN + "') order by GTIM,NewID desc)SELECT TOP (9) * FROM cte ORDER BY GTIM DESC", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable Select_KMMonth(string Vno, string month)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SP_Month_KM", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Month", month));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public void Insert_MonthKM(string vid, string date, string month, string date1)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Month_KMRun([Vehicle_number],[Month],[Date],[Month_Date]) values('" + vid + "'," + month + ",'" + date + "','" + date1 + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void Update_KMMonth(string Vno, string month, double value, int con, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update__Month_KM", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicleno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Month", month));
            cmd.Parameters.Add(new SqlParameter("@Value", value));
            cmd.Parameters.Add(new SqlParameter("@Con", con));
            cmd.Parameters.Add(new SqlParameter("@Date", date));
            cmd.ExecuteNonQuery();
            sqlconn.Close();

        }
        public DataTable get_KMMonth(string vno, int month)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Date,[1Hour],[2Hour],[3Hour],[4Hour],[5Hour],[6Hour],[7Hour],[8Hour],[9Hour],[10Hour],[11Hour],[12Hour],[13Hour],[14Hour],[15Hour],[16Hour],[17Hour],[18Hour],[19Hour],[20Hour],[21Hour],[22Hour],[23Hour],[24Hour] from Month_KMRun where  Vehicle_number='" + vno + "' and Month=" + month + " group by Date,[1Hour],[2Hour],[3Hour],[4Hour],[5Hour],[6Hour],[7Hour],[8Hour],[9Hour],[10Hour],[11Hour],[12Hour],[13Hour],[14Hour],[15Hour],[16Hour],[17Hour],[18Hour],[19Hour],[20Hour],[21Hour],[22Hour],[23Hour],[24Hour] order by min(ID)", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_KMMonthkvf(string vno, string date)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Date,[1Hour],[2Hour],[3Hour],[4Hour],[5Hour],[6Hour],[7Hour],[8Hour],[9Hour],[10Hour],[11Hour],[12Hour],[13Hour],[14Hour],[15Hour],[16Hour],[17Hour],[18Hour],[19Hour],[20Hour],[21Hour],[22Hour],[23Hour],[24Hour] from Month_KMRun where  Vehicle_number='" + vno + "' and Month_Date='" + date + "' group by Date,[1Hour],[2Hour],[3Hour],[4Hour],[5Hour],[6Hour],[7Hour],[8Hour],[9Hour],[10Hour],[11Hour],[12Hour],[13Hour],[14Hour],[15Hour],[16Hour],[17Hour],[18Hour],[19Hour],[20Hour],[21Hour],[22Hour],[23Hour],[24Hour] order by min(ID)", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public int Insert_Geo_Route(string rno, string rname, string uid, string type)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Insert_Geo_Route", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add(new SqlParameter("@Route_Name", rname));
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@Route_Type", type));
            cmd.Parameters.Add("@Nofrec", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Nofrec"].Value); ;

        }
        public int Insert_Geo_Route12(string rno, string uid, string rtype, string rname)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Insert_Geo_Route12", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            //cmd.Parameters.Add(new SqlParameter("@sdate", fdate));
            //cmd.Parameters.Add(new SqlParameter("@edate", tdate));
            cmd.Parameters.Add(new SqlParameter("@rtype", rtype));
            // cmd.Parameters.Add(new SqlParameter("@vno", vno));
            cmd.Parameters.Add(new SqlParameter("@rname", rname));
            cmd.Parameters.Add("@Nofrec", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Nofrec"].Value); ;

        }
        public DataTable get_Geodate_Route12(string rno, string uid)//,DateTime sdate,DateTime edate,string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Satrt_date,End_date,Vehicle_number from Geo_Route_No12 where Route_No='" + rno + "' and User_ID='" + uid + "'", sqlcon);

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void update_Geo_Route123(string rno, string uid, DateTime sdate, DateTime edate, string vno, string pointstype, DateTime pdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update_Geo_Route123", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@rno", rno));
            cmd.Parameters.Add(new SqlParameter("@uid", uid));
            cmd.Parameters.Add(new SqlParameter("@sdate", sdate));
            cmd.Parameters.Add(new SqlParameter("@edate", edate));
            cmd.Parameters.Add(new SqlParameter("@vno", vno));
            cmd.Parameters.Add(new SqlParameter("@pdate", pdate));
            cmd.Parameters.Add(new SqlParameter("@pointstype", pointstype));
            cmd.ExecuteNonQuery();
            sqlconn.Close();

        }
        public DataTable get_Geo_RouteNo(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + "", sqlcon);
            SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + " and Route_No NOT IN(select distinct Route_No from Vehicle_Route_Relation where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_RouteNo123(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + "", sqlcon);
            SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Routenew where User_ID=" + uid + " and Route_No NOT IN(select distinct Route_No from Vehicle_Route_Relation where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Route(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_Name,Route_Type from Geo_Route_No where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Routenew(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Vehicle_number,Route_Type from Geo_Route_No12 where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Route12(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_Type from Geo_Route_No12 where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geornos(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select * from Geo_Route_No12 where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Routepoints(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select ID,Route_No,Route_No,Route_Name,LAMI,LGMI,Radius,Route_Address,Status from Geo_Route where Route_No='" + rno + "' and User_ID=" + uid + " order by ID ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Routepointsnew(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select ID,Route_No,Route_No,LAMI,LGMI,Radius,Route_Address,Status from Geo_Routenew where Route_No='" + rno + "' and User_ID=" + uid + " order by ID ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_Routename(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_No,Route_Name,USER_ID from Geo_Route_No where Route_No ='" + rno + "' and USER_ID =" + uid + " order by ID ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public int create_Geo_Route(string rno, string rname, string uid, string lt, string lng, string radius, string address, string type, string date, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Create_Geo_Route1", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add(new SqlParameter("@Route_Name", rname));
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@LAMI", lt));
            cmd.Parameters.Add(new SqlParameter("@LGMI", lng));
            cmd.Parameters.Add(new SqlParameter("@Radius", radius));
            cmd.Parameters.Add(new SqlParameter("@Route_Address", address));
            cmd.Parameters.Add(new SqlParameter("@Route_Type", type));
            cmd.Parameters.Add(new SqlParameter("@Date", date));
            cmd.Parameters.Add(new SqlParameter("@From_Date", fdate));
            cmd.Parameters.Add(new SqlParameter("@To_Date", tdate));
            cmd.Parameters.Add("@Nofrec", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Nofrec"].Value); ;

        }

        public int create_Geo_Routenew(string rno, string uid, string lt, string lng, string radius, string address, string type, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Create_Geo_Route1new", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@LAMI", lt));
            cmd.Parameters.Add(new SqlParameter("@LGMI", lng));
            cmd.Parameters.Add(new SqlParameter("@Radius", radius));
            cmd.Parameters.Add(new SqlParameter("@Route_Address", address));
            cmd.Parameters.Add(new SqlParameter("@Route_Type", type));
            cmd.Parameters.Add(new SqlParameter("@Date", date));
            cmd.Parameters.Add("@Nofrec", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Nofrec"].Value); ;

        }
        public void delete_Geo_Routepoints(string id, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Geo_Route where ID=" + id + " and User_ID=" + uid + " ", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delete_Geo_Routepointsnew(string id, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Geo_Routenew where ID=" + id + " and User_ID=" + uid + " ", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delete_Geo_Route(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("DELETE Geo_Route_No where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            cmd.ExecuteNonQuery();
            if (sqlcon.State == ConnectionState.Open)
                sqlcon.Close();

        }
        public void delete_Geo_Routenew(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("DELETE Geo_Route_No12 where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlcon);
            cmd.ExecuteNonQuery();
            if (sqlcon.State == ConnectionState.Open)
                sqlcon.Close();

        }
        public void delete_ALL_Routepoints(string rno, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Geo_Route where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delete_ALL_Routepointsnew11(string rno, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Geo_Routenew where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void delete_ALL_Routepointsnew(string rno, string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("delete Geo_Routenew where Route_No='" + rno + "' and User_ID=" + uid + " ", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void DeleteAssingedRouteVehicles(string id, string rno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("DELETE [Vehicle_Route_Relation] where Route_No ='" + rno + "' and  [User_ID]=" + id + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public DataTable get_Geo_RouteNo1(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_No from Geo_Route_No where User_ID=" + uid + " and Route_No IN( select distinct Route_No from Geo_Route where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_RouteNo1new(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_No from Geo_Route_No12 where User_ID=" + uid + " and Route_No IN( select distinct Route_No from Geo_Routenew where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable GetUnAssingedRouteVehicles(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT [Vehicle_number],[User_ID] FROM [Vehicles] where User_ID = " + customerID + " and  Vehicle_number  NOT IN(SELECT [Vehicle_number] FROM [Vehicle_Route_Relation] where USER_ID= " + customerID + ") order by [Vehicle_number] asc", sqlconn);
            SqlCommand cmd = new SqlCommand("SELECT VehicalNumber,[User_ID] FROM Vehicles_Device_Rel where User_ID = " + customerID + " and  VehicalNumber  NOT IN(SELECT [Vehicle_number] FROM [Vehicle_Route_Relation] where USER_ID= " + customerID + ") order by VehicalNumber asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetAssingedRouteVehicles(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].ID,[Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,'"+none+"',[Vehicle_Route_Relation].Route_Assigned_Date FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID where [Vehicle_Route_Relation].USER_ID =" + customerID + " order by [Vehicle_number] asc", sqlconn);

            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].ID,[Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].Route_Assigned_Date FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID where [Vehicle_Route_Relation].USER_ID =" + customerID + " order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetAssingedRouteVehiclesnew(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].ID,[Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,[Vehicle_Route_Relation].Route_Assigned_Date FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID where [Vehicle_Route_Relation].USER_ID =" + customerID + " order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void DeAssingedRouteVehicles(string id, string vid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("DELETE [Vehicle_Route_Relation] where Vehicle_number ='" + vid + "' and  [User_ID]=" + id + "", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public int Check_Route_No(string uid, string rno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Check_Route_No", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Result"].Value);

        }
        public int Check_Route_No12(string uid, string rno, string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Check_Route_No12", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@Route_No", rno));
            cmd.Parameters.Add(new SqlParameter("@vno", vno));
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Result"].Value);

        }
        public string Geo_Validation(string uid, string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("GeoCode_Validate", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@User_ID", uid));
            cmd.Parameters.Add(new SqlParameter("@Vehicle_No", vno));
            cmd.Parameters.Add("@Geo_Type", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToString(cmd.Parameters["@Geo_Type"].Value);
        }
        public DataTable get_Geo_ALLRouteNo(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + "", sqlcon);
            SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + "", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_ALLRouteNonew(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No where User_ID=" + uid + "", sqlcon);
            SqlCommand cmd = new SqlCommand("select distinct Route_No from Geo_Route_No12 where User_ID=" + uid + "", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Route_Vehicles(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],Geo_Route_No.Route_Name FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID where [Vehicle_Route_Relation].Route_No ='" + rno + "' and [Vehicle_Route_Relation].User_ID=" + uid + " order by [Vehicle_number] asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_RouteNo2(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_No from Geo_Route_No where User_ID=" + uid + " and Route_No  NOT IN( select distinct Route_No from Geo_Route where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Geo_RouteNo12(string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Route_No from Geo_Route_No12 where User_ID=" + uid + " and Route_No NOT IN( select distinct Route_No from Geo_Route where User_ID=" + uid + ") order by Route_No asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable get_Routeno(string uid, string vno)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("SELECT Route_No from Vehicle_Route_Relation where User_ID='" + uid + "' and Vehicle_number='" + vno + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public int Check_Vehicle_No(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Check_VeHicle_No", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Vehicle_number", vno));
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sqlconn.Close();
            return Convert.ToInt32(cmd.Parameters["@Result"].Value);

        }
        public DataTable GET_DAY_DATA(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("GET_DAY_DATA", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            adp.Fill(ds);
            sqlconn.Close();
            return ds;
        }
        public DataTable get_routebasis_data(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Get_routebasis_data]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            adp.Fill(ds);
            sqlconn.Close();
            return ds;
        }
        public DataTable get_Routeno_Routetyp(string vno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select Geo_Route_No.Route_no,Geo_Route_No.Route_Type from Geo_Route_No inner join Vehicle_Route_Relation on Vehicle_Route_Relation.User_ID=Geo_Route_No.User_ID and Vehicle_Route_Relation.Route_no=Geo_Route_No.Route_no where Vehicle_Route_Relation.Vehicle_number='" + vno + "' and Vehicle_Route_Relation.User_ID=" + uid + "", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Noof_Points(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(10) LAMI,LGMI,Radius,From_Date,To_Date,Route_Address,Route_Type,ID,Route_No from Geo_Route where Route_No='" + rno + "' and User_ID=" + uid + " order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Noof_Points1122(string rno, string uid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select LAMI,LGMI,Radius,Route_Address,Route_Type,ID,Route_No from Geo_Routenew where Route_No='" + rno + "' and User_ID='" + uid + "' order by ID asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable GetAssingedTripVehicles1(string customerID, string rtype, string dt, DataTable CAdmin)
        {
            string query = "[Vehicle_Route_Relation].USER_ID  = " + customerID + "";
            for (int i = 0; i < CAdmin.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "[Vehicle_Route_Relation].USER_ID  = " + CAdmin.Rows[i][0].ToString() + "";
                }
                else { query += " OR [Vehicle_Route_Relation].USER_ID  = " + CAdmin.Rows[i][0].ToString() + ""; ; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].[Route_Assigned_Date] FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID and Geo_Route_No.Route_Type='" + rtype + "' where " + query + " and [Vehicle_Route_Relation].[Vehicle_number] in(SELECT DISTINCT Vehicle_number FROM Vehicle_Route_Trip where '" + dt + "' between Route_Assigned_Date and Route_Deassigned_Date and User_ID=" + customerID + ") order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable First_TrackString(string vid, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(1) LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Last_TrackString(string vid, string fdate, string tdate)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(1) LAMI,LGMI,SPED,GTIM,IGST from Tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and gtim between '" + fdate + "' and '" + tdate + "' order by GTIM Desc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void AssignRoutetoVehicle(string Routeno, string vehicleNumber, string customerid, string dt)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO [Vehicle_Route_Relation]([Vehicle_number],[Route_No],[User_ID],[Route_Assigned_Date]) VALUES ('" + vehicleNumber + "','" + Routeno + "','" + customerid + "','" + dt + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void Assign_Routes_Vehicle(string Routeno, string vehicleNumber, string customerid, string Sdt, string Edt)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO [Vehicle_Route_Trip]([Vehicle_Number],[Route_No],[User_ID],[Route_Assigned_Date],[Route_Deassigned_Date]) VALUES ('" + vehicleNumber + "','" + Routeno + "','" + customerid + "','" + Sdt + "','" + Edt + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public void DeAssinged_Routes_Vehicles(string id, string vid, string rno, string dt)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE [Vehicle_Route_Trip] Set Route_Deassigned_Date ='" + dt + "' where  [User_ID]=" + id + " and [Route_No]='" + rno + "' and Vehicle_number='" + vid + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public DataTable get_Routeno_BetweenDate(string vno, string uid, string date)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select distinct Route_No from Vehicle_Route_Trip where Vehicle_number='" + vno + "' and '" + date + "' between Route_Assigned_Date and Route_Deassigned_Date and User_ID=" + uid + "", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public void Update_Status_GeoRoute(string rno, string uid, string id)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Update Geo_Route Set Status='" + 1 + "' where Route_No='" + rno + "' and User_ID=" + uid + " and ID='" + id + "'", sqlcon);
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        public DataTable Select_All_Moving(string vid, string fd, string td)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,GTIM from tracking where XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=" + 1 + " and GTIM between '" + fd + "' and '" + td + "' order by GTIM ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable GetAssingedTripVehicles(string customerID, string rtype, string dt)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
           // SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].[Route_Assigned_Date] FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID and Geo_Route_No.Route_Type='" + rtype + "' where [Vehicle_Route_Relation].USER_ID =" + customerID + " and [Vehicle_Route_Relation].[Vehicle_number] in(SELECT DISTINCT Vehicle_number FROM Vehicle_Route_Trip where '" + dt + "' between Route_Assigned_Date and Route_Deassigned_Date and User_ID=" + customerID + ") order by [Vehicle_number] asc", sqlconn);
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].[Route_Assigned_Date] FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID and Geo_Route_No.Route_Type='" + rtype + "' where [Vehicle_Route_Relation].USER_ID =" + customerID + " and [Vehicle_Route_Relation].[Vehicle_number] in(SELECT DISTINCT Vehicle_number FROM Vehicle_Route_Trip where User_ID=" + customerID + ") order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void Update_Vehicles_DailyAlerts(string VN, string ospeed, string maxtemp, string vs, string vi, string vm, string mintemp, string delaytime, string sft, string stt, string saft, string satt, string samin)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Vehicles set [Overspeed]='" + ospeed + "',[Max_Temp]='" + maxtemp + "',[Alert_stationary]='" + vs + "',[Alert_idling]='" + vi + "',[Alert_moving]='" + vm + "',[Min_Temp]='" + mintemp + "',[Delay_Temp]='" + delaytime + "',[Stationary_Fromtime]='" + sft + "',[Stationary_Totime]='" + stt + "',[Vehicle_Security_Ftime]='" + saft + "',[Vehicle_Security_Ttime]='" + satt + "',[Vehicle_Security_Min]='" + samin + "' where [Vehicle_number]='" + VN + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        public void Update_Veh_DailyAlerts(string VN, string ospeed, string maxtemp, string vs, string vi, string vm, string mintemp, string delaytime, string sft, string stt, string saft, string satt, string samin)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Veh set [Overspeed]='" + ospeed + "',[Max_Temp]='" + maxtemp + "',[Alert_stationary]='" + vs + "',[Alert_idling]='" + vi + "',[Alert_moving]='" + vm + "',[Min_Temp]='" + mintemp + "',[Delay_Temp]='" + delaytime + "',[Stationary_Fromtime]='" + sft + "',[Stationary_Totime]='" + stt + "',[Vehicle_Security_Ftime]='" + saft + "',[Vehicle_Security_Ttime]='" + satt + "',[Vehicle_Security_Min]='" + samin + "' where [Vehicle_number]='" + VN + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();

        }
        internal DataTable Towed_Alerts(string VehID, string smsbody, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT CONVERT(VARCHAR(10), GTIM, 111) as D1,DATEPART(HOUR,GTIM)as H1,LEFT(DATEPART(MINUTE,GTIM),1)as M1 from TRACKING WHERE XXXM=(SELECT Device_ID FROM Vehicles_Device_Rel WHERE VehicalNumber='" + VehID + "') and IGST=0 and SPED>4 and GTIM between '" + fdate + "' and '" + tdate + "' GROUP BY CONVERT(VARCHAR(10), GTIM, 111),DATEPART(hOUR,GTIM),LEFT(DATEPART(MINUTE,GTIM),1) HAVING COUNT(*)>7", sqlconn);//DESC and IGST='0'
            // SqlCommand cmd = new SqlCommand("Select * From SMS_Log where SMS_Vno='" + VehID + "' and SMS_DateTime BETWEEN '" + fdate + "' AND '" + tdate + "'  and SMS_Body='" + smsbody + "'", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable Fuzio_Towed_Alerts(string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select distinct(VehicalNumber) from(SELECT CONVERT(VARCHAR(10), GTIM, 111) as D1,DATEPART(HOUR,GTIM)as H1,LEFT(DATEPART(MINUTE,GTIM),1) as M1, VehicalNumber from TRACKING t, Vehicles_Device_Rel vd WHERE t.XXXM = vd.Device_ID and IGST = 0 and SPED > 4 and GTIM between '"+ fdate + "' and '"+ tdate + "' GROUP BY CONVERT(VARCHAR(10), GTIM, 111), DATEPART(hOUR, GTIM), LEFT(DATEPART(MINUTE, GTIM), 1), VehicalNumber HAVING COUNT(*) > 7) as ttt", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Security_Alerts(string VehID, string customerID, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select distinct LAMI,LGMI,GTIM from Tracking where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VehID + "' and User_ID=" + customerID + ") and Sped>3 and GTIM between '" + fdate + "' and '" + tdate + "' group by LAMI,LGMI,GTIM  order by GTIM DESC", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void Vehicle_Alert_Status(string vno, string uid, int status)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("update Vehicles_Device_Rel set [Vehicle_Alerts_Status]='" + status + "'where [VehicalNumber]='" + vno + "' and User_ID='" + uid + "'", sqlconn);//) values('"
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable CheckUserDetails(string username)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [UserName],[Password] ,[FirstName],[LastName] ,[User_Role_ID],[Register] FROM [Users] where ID = '" + username + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Get_Towed_Points(string VehID, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT * from TRACKING WHERE XXXM=(SELECT Device_ID FROM Vehicles_Device_Rel WHERE VehicalNumber='" + VehID + "') and IGST=0 and SPED>4 and GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM Desc ", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Check_Genaral_Alerts(string UserID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from Vehicles_Device_Rel where User_ID=" + UserID + " and Vehicle_Alerts_Status=0", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_All_Towedpoints(string vid, string fd, string td)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,SPED,GTIM from tracking where XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=0 and SPED>4 and GTIM between '" + fd + "' and '" + td + "' order by GTIM ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        internal DataTable Set_Image_Logo(string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top(1) UserImage,Image_width from Users where ID='" + customerID + "'", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_CAdminUsers(string CAdminID, int Status)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select User_Name,User_ID from CAdmin_Customer_Rel where User_Name = '" + CAdminID + "' and Status=" + Status + "", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public void Set_CAdminUsers_Status(string uname, string uid, int status)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Update CAdmin_Customer_Rel Set Status=" + status + " where User_Name = '" + uname + "'", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        public DataTable Get_CAminVeh(string customerid, DataTable CAdmin)
        {
            string query = "User_ID = " + customerid + "";
            for (int i = 0; i < CAdmin.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "User_ID = " + CAdmin.Rows[i][0].ToString() + "";
                }
                else { query += " OR User_ID = " + CAdmin.Rows[i][0].ToString() + ""; ; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select VehicalNumber,Vehicle_Alerts_Status,Serial_no,User_ID from Vehicles_Device_Rel inner join Devices on Devices.Device_ID=Vehicles_Device_Rel.Device_ID where " + query + " order by VehicalNumber asc", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Check_Genaral_Alerts1(string UserID, DataTable dt)
        {
            string query = "User_ID = " + UserID + "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "User_ID = " + dt.Rows[i][0].ToString() + "";
                }
                else { query += " OR User_ID = " + dt.Rows[i][0].ToString() + ""; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from Vehicles_Device_Rel where Vehicle_Alerts_Status=0 and " + query + "", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_CAdminVehicles(string customerID, DataTable CAdmin)
        {
            string query = "User_ID = " + customerID + "";
            for (int i = 0; i < CAdmin.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "User_ID = " + CAdmin.Rows[i][0].ToString() + "";
                }
                else { query += " OR User_ID = " + CAdmin.Rows[i][0].ToString() + ""; ; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT ID,[VehicalNumber],[Device_ID] FROM [Vehicles_Device_Rel] where " + query + " order by VehicalNumber asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public string Get_UserID(string Vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT User_ID From Vehicles  where Vehicle_number='" + Vno + "'", sqlconn);
            string uid = Convert.ToString(cmd.ExecuteScalar());
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return uid;

        }
        public DataTable GetAssingedTripVehicles12(string customerID, string rtype, DataTable CAdmin)
        {
            string query = "[Vehicle_Route_Relation].USER_ID = " + customerID + "";
            for (int i = 0; i < CAdmin.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "[Vehicle_Route_Relation].USER_ID = " + CAdmin.Rows[i][0].ToString() + "";
                }
                else { query += " OR [Vehicle_Route_Relation].USER_ID = " + CAdmin.Rows[i][0].ToString() + ""; ; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].[Route_Assigned_Date] FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID and Geo_Route_No.Route_Type='" + rtype + "'  where " + query + " order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable GetAssingedTripVehicles13(string customerID, string rtype)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].[Route_Assigned_Date] FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID and Geo_Route_No.Route_Type='" + rtype + "'  where [Vehicle_Route_Relation].USER_ID =" + customerID + " order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_AssingedRoute_Vehicles(string customerID, DataTable CAdmin)
        {
            string query = "[Vehicle_Route_Relation].USER_ID= " + customerID + "";
            for (int i = 0; i < CAdmin.Rows.Count; i++)
            {
                if (i == 0)
                {
                    query = "[Vehicle_Route_Relation].USER_ID= " + CAdmin.Rows[i][0].ToString() + "";
                }
                else { query += " OR [Vehicle_Route_Relation].USER_ID= " + CAdmin.Rows[i][0].ToString() + ""; ; }
            }
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Vehicle_Route_Relation].ID,[Vehicle_Route_Relation].[Vehicle_number],[Vehicle_Route_Relation].Route_No,Geo_Route_No.Route_Name,[Vehicle_Route_Relation].Route_Assigned_Date FROM [Vehicle_Route_Relation] INNER JOIN Geo_Route_No ON [Vehicle_Route_Relation].Route_No=Geo_Route_No.Route_No and [Vehicle_Route_Relation].User_ID=Geo_Route_No.User_ID where " + query + " order by [Vehicle_number] asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_Details(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Users.UserName,Vehicles.Driver_Name, Vehicles.Vehicle_Reg_Name, Vehicles.Driver_Mobile_Number from Users inner join Vehicles on Users.ID=Vehicles.User_ID where Vehicles.Vehicle_number='" + vno + "'", sqlconn); //("select distinct [VehicalNumber], case IOST when 0 then 'OFF' else 'ON' end as Status from Vehicles_Device_Rel inner join dbo.Tracking_current on XXXM = Device_ID  inner join Users on users.id = Vehicles_Device_Rel.user_id where User_ID = " + customerid, sqlconn);//+ " or Parent_User = " + parentuser
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Owner(string suserid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Vehicles.Vehicle_Reg_Name FROM Vehicles WHERE User_ID = '" + suserid + "'", sqlconn);//"' and USER_ID = '" + p_2 +
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_AssingedVehicles(string customerID, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT distinct [Vehicles_Device_Rel].[VehicalNumber], Month_KMRun.[1Hour],Month_KMRun.[2Hour],Month_KMRun.[3Hour],Month_KMRun.[4Hour],Month_KMRun.[5Hour],Month_KMRun.[6Hour],Month_KMRun.[7Hour],Month_KMRun.[8Hour],Month_KMRun.[9Hour],Month_KMRun.[10Hour],Month_KMRun.[11Hour],Month_KMRun.[12Hour],Month_KMRun.[13Hour],Month_KMRun.[14Hour],Month_KMRun.[15Hour],Month_KMRun.[16Hour],Month_KMRun.[17Hour],Month_KMRun.[18Hour],Month_KMRun.[19Hour],Month_KMRun.[20Hour],Month_KMRun.[21Hour],Month_KMRun.[22Hour],Month_KMRun.[23Hour],Month_KMRun.[24Hour] FROM [Vehicles_Device_Rel]INNER JOIN Month_KMRun ON [Vehicles_Device_Rel].[VehicalNumber] = Month_KMRun.Vehicle_number where USER_ID ='" + customerID + "' and Month_Date='" + date + "'order by VehicalNumber asc", sqlconn);

            // SqlCommand cmd = new SqlCommand("select * from Month_KMRun where Vehicle_number='"+customerID +"' and Month_Date='" + date + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("SELECT [VehicalNumber] FROM [Vehicles_Device_Rel] where USER_ID =" + customerID + " order by VehicalNumber asc", sqlconn);
            //SELECT [Vehicles_Device_Rel].[VehicalNumber], Month_KMRun.*FROM [Vehicles_Device_Rel]INNER JOIN Month_KMRun ON [Vehicles_Device_Rel].[VehicalNumber] = Month_KMRun.Vehicle_number
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_monthkmvf(string customerID, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Month_KMRun.[Vehicle_number], Month_KMRun.[1Hour],Month_KMRun.[2Hour],Month_KMRun.[3Hour],Month_KMRun.[4Hour],Month_KMRun.[5Hour],Month_KMRun.[6Hour],Month_KMRun.[7Hour],Month_KMRun.[8Hour],Month_KMRun.[9Hour],Month_KMRun.[10Hour],Month_KMRun.[11Hour],Month_KMRun.[12Hour],Month_KMRun.[13Hour],Month_KMRun.[14Hour],Month_KMRun.[15Hour],Month_KMRun.[16Hour],Month_KMRun.[17Hour],Month_KMRun.[18Hour],Month_KMRun.[19Hour],Month_KMRun.[20Hour],Month_KMRun.[21Hour],Month_KMRun.[22Hour],Month_KMRun.[23Hour],Month_KMRun.[24Hour] from  Month_KMRun where Vehicle_number='" + customerID + "' and Month_Date='" + date + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetPreviIGST144(string VehID, string customerID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,IGST from Tracking where XXXM='" + VehID + "' and IGST='1' ORDER BY GTIM Desc", sqlconn);//DESC
            // SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,Cond_inputs from Tracking where XXXM='" + VehID + "' and Cond_inputs>=1000 ORDER BY GTIM asc", sqlconn);//DESC
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 GTIM,Cond_inputs from Tracking where XXXM='" + VehID + "' and Cond_inputs between 0 and 3  ORDER BY GTIM desc", sqlconn);//DESC

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Get_tank(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select tank_capacity from veh where vehicle_number='" + vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_Fuel(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_fuel", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable Select_Fuelold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_fuelold]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_Fueldata(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_fueldata]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_KMreport(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_KMreport]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable prevfuelstaus1(string vid, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from tracking where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and igst!=0 and fuel_adc!=0 and GTIM <'" + date + "' order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable prevfuelstaus1old(string vid, string date)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from OldTrack where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and igst!=0 and fuel_adc!=0 and GTIM <'" + date + "' order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable prevfuelstaus111(string vid, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from tracking where xxxm=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and igst!=0 and fuel_adc!=0 and GTIM  between '" + fdate + "' and '" + tdate + "' order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Fuel_fill2(string Vno, string fdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Fuel_fill2", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getfreez(string customer)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select ID,freez from Users where UserName='" + customer + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable Get_kmhike(string customer)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select ID,KMhike from Users where UserName='" + customer + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getreeferveh(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select User_ID,Vehicle_Type from Veh where Vehicle_number='" + vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getacveh(string vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select device_id,ac_switch from Devices  where device_id=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "')", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getprevacstring(string VehID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top(1) GTIM,PULSE_CTR  from Tracking where XXXM='" + VehID + "' and PULSE_CTR ='100000.00' ORDER BY GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getacoff(string VehID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT top(1) GTIM,PULSE_CTR from Tracking where XXXM='" + VehID + "' and PULSE_CTR ='0.00' ORDER BY GTIM desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getacontime(string VehID, string stime, string ttime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getacontime1(string VehID, string stime, string ttime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT (*) from Tracking where XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VehID + "') and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getacofftime1(string VehID, string stime, string ttime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT (*) from Tracking where XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VehID + "') and PULSE_CTR ='100000.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getacofftime1old(string VehID, string stime, string ttime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT (*) from OldTrack where XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VehID + "') and PULSE_CTR ='100000.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getacontime1old(string VehID, string stime, string ttime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT (*) from OldTrack where XXXM =(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + VehID + "') and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            //SqlCommand cmd = new SqlCommand("select COUNT (*) from Track where XXXM ='" + VehID + "' and PULSE_CTR ='0.00' and GTIM between '" + stime + "' and '" + ttime + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Getfuelimp(string customer)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select ID,fuelimp from Users where UserName='" + customer + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getmilage(string vid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select milage from Vehicles where Vehicle_number='" + vid + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable getmanagerusers(string mid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select User_Name,User_ID,Cx_Name ,CAdmin_ID from CAdmin_Customer_Rel inner join Customer ON CAdmin_Customer_Rel.User_ID=Customer.ID where CAdmin_ID='" + mid + "' order by User_Name asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }

        // FOR REPORTS OldTrack table

        public DataTable Select_Tempold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_temparatureold", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_All_Towedpointsold(string vid, string fd, string td)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,SPED,GTIM from OldTrack where XXXM=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=0 and SPED>4 and GTIM between '" + fd + "' and '" + td + "' order by GTIM ASC", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];
        }
        public DataTable igstondetailsold(string fdate, string tdate, string did)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select LAMI,LGMI,SPED,GTIM,IGST from OldTrack where xxxm=" + did + " and  GTIM between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable firstigstonold(string fdate, string tdate, string did)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select top(1) LAMI,LGMI,SPED,GTIM,IGST from OldTrack where xxxm=" + did + " and IGST=1 and SPED>0 and  gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable totaligstonold(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from OldTrack where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=1 and SPED>1 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable totalidealtonold(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from OldTrack where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=1 and SPED=0 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable totaligstoffold(string fdate, string tdate, string vid)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("Select Count(*) from OldTrack where XXXM=(Select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and IGST=0 and SPED=0 and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable searchold(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {

            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            //sql = sql + " IGST = 1 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' ";

            return ds_FindRold(sql);
        }

        private DataTable ds_FindRold(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,[PULSE_CTR],[FUEL_ADC]  FROM [OldTrack] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_TotalRunkmold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select_TotalRunkmold ", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_KMreportold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_KMreportold]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_Fueldataold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_fueldataold]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        public DataTable fuelfillmap1old(string vno, string date)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,GTIM,FUEL_ADC from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and GTIM ='" + date + "'", sqlcon);
            // SqlCommand cmd = new SqlCommand("select count(*) from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and ((len(COND_INPUTS)=4 and " + 1200 + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) or (len(COND_INPUTS)<=3 and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,3))))))", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable fuelfillmap(string vno, string date)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("select LAMI,LGMI,GTIM,FUEL_ADC from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and GTIM ='" + date + "'", sqlcon);
            // SqlCommand cmd = new SqlCommand("select count(*) from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and ((len(COND_INPUTS)=4 and " + 1200 + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) or (len(COND_INPUTS)<=3 and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,3))))))", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }

        public DataTable Temp_Noofalertold(string fdate, string tdate, string vid, int maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*) from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and ((len(COND_INPUTS)=4 and " + 1200 + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) or (len(COND_INPUTS)<=3 and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,3))))))", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Temp_Noofalert2old(string fdate, string tdate, string vid, double maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            //SqlCommand cmd = new SqlCommand("select count(*)from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and (len(COND_INPUTS)=4) and " + maxtemp + "<(select CONVERT(INT,(right(COND_INPUTS,4)))) AND " + mintemp + "< (select CONVERT(INT,(right(COND_INPUTS,4))))", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*)from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable Temp_Noofalert1old(string fdate, string tdate, string vid, double maxtemp, int mintemp)
        {
            SqlConnection sqlcon = new SqlConnection(connectionstring);
            sqlcon.Open();
            //SqlCommand cmd = new SqlCommand("select count(*) from tracking where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and (len(COND_INPUTS)>3 or " + mintemp + ">(select CONVERT(INT,(right(COND_INPUTS,4))))) and  " + maxtemp + ">(select CONVERT(INT,(right(COND_INPUTS,4)))) and GTIM between '" + fdate + "' and '" + tdate + "'", sqlcon);
            SqlCommand cmd = new SqlCommand("select count(*)from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vid + "') and GTIM between '" + fdate + "' and '" + tdate + "' and (len(COND_INPUTS)=4) and " + maxtemp + "<(select CONVERT(INT,(right(COND_INPUTS,4)))) ", sqlcon);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlcon.Close();
            return ds.Tables[0];

        }
        public DataTable getprevfuelstausold(string vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select top 1 SPED,IGST,GTIM,FUEL_ADC from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and sped<=3 and igst!=0 and fuel_adc!=0 and GTIM between '" + fdate + "' and '" + tdate + "' order by gtim desc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getalertsforspeedold(string vno, string fdate, string tdate, string speed)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select * from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and sped >" + speed + " and gtim between '" + fdate + "' and '" + tdate + "' order by GTIM asc", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable Select_tkmold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select_tkmold ", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VehicleNo", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable getblackspotold(string vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select count(*) from OldTrack where xxxm=(select Device_ID from Vehicles_Device_Rel where VehicalNumber='" + vno + "') and gtim between '" + fdate + "' and '" + tdate + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];

        }
        public DataTable getMovingDataold(string Vid, DateTime fdate, DateTime tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID,FUEL_ADC  FROM [OldTrack] inner join Vehicles_Device_Rel on XXXM = Device_ID WHERE VehicalNumber = '" + Vid + "' and  IGST = 1  and  GTIM between '" + fdate + "' and '" + tdate + " ' and SPED > '0' order by GTIM ASC";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }

        internal DataTable searchOn2old(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 0 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED = '0' order by GTIM desc,NewID desc";

            return ds_Find2old(sql);
        }
        private DataTable ds_Find2old(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //top 1
            string sql = "SELECT  [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [OldTrack] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable searchOn3old(string pID, string TextBox1, string TextBox3, string ddlMapTOVehicle)
        {
            string sql = " WHERE";

            if (ddlMapTOVehicle.CompareTo("") != 0 && ddlMapTOVehicle != null && ddlMapTOVehicle != "Select One")
                sql = sql + " VehicalNumber = '" + ddlMapTOVehicle.Trim() + "' and ";

            sql = sql + " IGST = 1 " + " and ";

            if (TextBox1.CompareTo("") != 0 && TextBox1 != null)
                sql = sql + " GTIM between '" + TextBox1 + "' and";
            if (TextBox3.CompareTo("") != 0 && TextBox3 != null)
                sql = sql + "' " + TextBox3 + "' and SPED = '0' order by GTIM desc,NewID desc";

            return ds_Find3old(sql);
        }
        private DataTable ds_Find3old(string sqlWHERE)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            string sql = "SELECT [LAMI],[LGMI],[GTIM],[SPED], [IGST],[XXXM] as deviceID  FROM [OldTrack] inner join Vehicles_Device_Rel on XXXM = Device_ID";
            sql = sql + sqlWHERE;
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Towed_Alertsold(string VehID, string smsbody, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT CONVERT(VARCHAR(10), GTIM, 111) as D1,DATEPART(HOUR,GTIM)as H1,LEFT(DATEPART(MINUTE,GTIM),1)as M1 from OldTrack WHERE XXXM=(SELECT Device_ID FROM Vehicles_Device_Rel WHERE VehicalNumber='" + VehID + "') and IGST=0 and SPED>4 and GTIM between '" + fdate + "' and '" + tdate + "' GROUP BY CONVERT(VARCHAR(10), GTIM, 111),DATEPART(hOUR,GTIM),LEFT(DATEPART(MINUTE,GTIM),1) HAVING COUNT(*)>7", sqlconn);//DESC and IGST='0'
            // SqlCommand cmd = new SqlCommand("Select * From SMS_Log where SMS_Vno='" + VehID + "' and SMS_DateTime BETWEEN '" + fdate + "' AND '" + tdate + "'  and SMS_Body='" + smsbody + "'", sqlconn);//DESC and IGST='0'
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public void Insert_Geo_Route_basis(string Latitude, string Longitude, string Speed, string sDateTime, string IgnitionStatus, string UnitID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Insert_Geo_Route_basis", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Latitude", Latitude));
            cmd.Parameters.Add(new SqlParameter("@Longitude ", Longitude));
            cmd.Parameters.Add(new SqlParameter("@Speed", Speed));
            cmd.Parameters.Add(new SqlParameter("@sDateTime ", sDateTime));
            cmd.Parameters.Add(new SqlParameter("@IgnitionStatus ", IgnitionStatus));
            cmd.Parameters.Add(new SqlParameter("@UID ", UnitID));
            cmd.ExecuteNonQuery();
            sqlconn.Close();

        }

        public DataTable Select_Georoutenew(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_Georoutebasisnew]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public DataTable Select_Georouteold(string Vno, string fdate, string tdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("[Select_Georoutebasisold]", sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@vno", Vno));
            cmd.Parameters.Add(new SqlParameter("@Fdate", fdate));
            cmd.Parameters.Add(new SqlParameter("@Tdate", tdate));
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        public void Set_Manager_Geo(int uid, double lami, double lgmi, double radius, int status, string name)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Manager_Geofence(User_id,Latitude,Langitude,Radius,Status,Geo_Name) values('" + uid + "','" + lami + "','" + lgmi + "','" + radius + "','" + status + "','" + name + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        internal DataTable Get_Manager_Geo(string UID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT User_id from Manager_Geofence where User_id='" + UID + "' ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable Get_Manager_Geo_latlan(string UID)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Latitude,Langitude,Radius,Geo_Name from Manager_Geofence where User_id='" + UID + "' ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        public double Getdistance_between_latlan(double Lat1, double Long1, double Lat2, double Long2)
        {
            string s = "SELECT dbo.GetDistanceBetween1(@Lat1 ,@Long1 , @Lat2 ,@Long2 )";
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand(s, sqlconn);
            cmd.Parameters.AddWithValue("@Lat1", Lat1);
            cmd.Parameters.AddWithValue("@Long1", Long1);
            cmd.Parameters.AddWithValue("@Lat2", Lat2);
            cmd.Parameters.AddWithValue("@Long2", Long2);
            cmd.CommandType = CommandType.Text;
            sqlconn.Open();
            double sss = (double)cmd.ExecuteScalar();
            sqlconn.Close();
            return sss;
        }

        public void Delete_Manager_Geo(string uid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Delete from Manager_Geofence where User_id='" + uid + "'", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public void CreateDeviceHistory(string deviceID, string Devicehistory, string Datetime)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Device_History(Device_ID,Device_History,Date_time) values('" + deviceID + "','" + Devicehistory + "','" + Datetime + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }

        public void Insrtcomplaints(string ticketdate, string ticketId, string VehicleNo, string DeviceId, string ComplaintType, string ComplaintMsg, string AvailableDate, string AvailableLocation, string UserID, string UserName, int status, string city, string mobile, string compalintid, string ctdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into Complaints(TicketDate,TicketId,VehicleNo,DeviceId,ComplaintType,ComplaintMsg,AvailableDate,AvailableLocation,UserID,UserName,ComplaintStatus,City,ContactNo,ComplaintID,Ctdate) values('" + ticketdate + "','" + ticketId + "','" + VehicleNo + "','" + DeviceId + "','" + ComplaintType + "','" + ComplaintMsg + "','" + AvailableDate + "','" + AvailableLocation + "','" + UserID + "','" + UserName + "','" + status + "','" + city + "','" + mobile + "','" + compalintid + "','" + ctdate + "')", sqlconn);
            cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
        }
        internal DataTable GetCompalints(string userid)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("Select TicketDate,TicketId,VehicleNo,DeviceId,ComplaintType,ComplaintMsg,AvailableDate,AvailableLocation,UserID,UserName,ComplaintStatus,City,ContactNo,ComplaintclosedDate from Complaints where UserID= '" + userid + "'order by ComplaintID desc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable GetCompalintsid(string ticketdate)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Distinct top(1) TicketDate, TicketId,VehicleNo,DeviceId,ComplaintType,ComplaintMsg,AvailableDate,AvailableLocation,UserID,UserName from Complaints where Ctdate= '" + ticketdate + "' order by TicketId desc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable CheckCompalintsidstatus(string vno, string did)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("select Distinct top(1) TicketDate, TicketId,VehicleNo,DeviceId,ComplaintType,ComplaintMsg,AvailableDate,AvailableLocation,UserID,UserName,ComplaintStatus,ComplaintID from Complaints where VehicleNo= '" + vno + "' and DeviceId= '" + did + "' order by ComplaintID desc, TicketId asc ", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.AcceptChanges();
            adp.Fill(ds);
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable getCxMoNo(string Vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Vehicles_Device_Rel.User_ID, Customer.Cx_Name, Customer.Cx_MobileNumber,Customer.Cx_EmailID FROM Vehicles_Device_Rel INNER JOIN Customer ON Vehicles_Device_Rel.User_ID = Customer.ID where VehicalNumber='" + Vno + "'", sqlconn); //(Vehicles_Device_Rel.VehicalNumber = '" + vehiclenumber + "') and USER_ID = " + customerID + "
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
        internal DataTable getfivestagefuel(string Vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT VehicleNo,UserID,Tank_Capacity,[1stFillingValue],[1stStringValue],[2ndFillingValue],[2ndStringValue],[3rdFillingValue],[3rdStringValue],[4thFillingValue],[4thStringValue],[5thFillingValue],[5thStringValue] from TblFiveStageFuel where VehicleNo='" + Vno + "'", sqlconn); //(Vehicles_Device_Rel.VehicalNumber = '" + vehiclenumber + "') and USER_ID = " + customerID + "
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }

        //--------------------------Tyre Details------------------------------

        public int InsertTyreDetails(string Vehicle_number, string Numberoftyres)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("insert into SetTyreMilageEntry(Vehicle_number,Numberoftyres) values('" + Vehicle_number + "','" + Numberoftyres + "')", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal DataTable CheckvehicleTyres(string Vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Vehicle_number,Numberoftyres from SetTyreMilageEntry where Vehicle_number='" + Vno + "'", sqlconn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
       
        internal DataTable Gettyredetails(string Vno)
        {
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Vehicle_number,Numberoftyres,frontlefttyre,frontleftytrelimit,totalleftfront,frontrighttyre,frontrighttytrelimit,totalrightfront,rearleft1tyre,rearleft1tyrelimit,totalleft1,rearleft2tyre,rearleft2tyrelimit,totalleft2,rearleft3tyre,rearleft3tyrelimit,totalleft3,rearleft4tyre,rearleft4tyrelimit,totalleft4,rearright1ytre,rearright1ytrelimit,totalrear1,rearright2tyre,rearright2ytrelimit,totalrear2,rearright3tyre,rearright3ytrelimit,totalrear3,rearright4tyre,rearright4ytrelimit,totalrear4 from SetTyreMilageEntry where Vehicle_number='" + Vno + "'", sqlconn); 
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            sqlconn.Close();
            return ds.Tables[0];
        }
      
        internal int UpdateFrontleftTyredetails(string Vehicle_number, string frontlefttyre, string frontleftytrelimit, string totalleftfront)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set frontlefttyre='" + frontlefttyre + "',frontleftytrelimit='" + frontleftytrelimit + "',totalleftfront='" + totalleftfront + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateFrontRightTyredetails(string Vehicle_number, string frontrighttyre, string frontrighttytrelimit, string totalrightfront)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set frontrighttyre='" + frontrighttyre + "',frontrighttytrelimit='" + frontrighttytrelimit + "',totalrightfront='" + totalrightfront + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }

        internal int UpdateReartleft1Tyredetails(string Vehicle_number, string rearleft1tyre, string rearleft1tyrelimit, string totalleft1)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearleft1tyre='" + rearleft1tyre + "',rearleft1tyrelimit='" + rearleft1tyrelimit + "',totalleft1='" + totalleft1 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateRearRight1Tyredetails(string Vehicle_number, string rearright1ytre, string rearright1ytrelimit, string totalrear1)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearright1ytre='" + rearright1ytre + "',rearright1ytrelimit='" + rearright1ytrelimit + "',totalrear1='" + totalrear1 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateReartleft2Tyredetails(string Vehicle_number, string rearleft2tyre, string rearleft2tyrelimit, string totalleft2)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearleft2tyre='" + rearleft2tyre + "',rearleft2tyrelimit='" + rearleft2tyrelimit + "',totalleft2='" + totalleft2 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateRearRight2Tyredetails(string Vehicle_number, string rearright2tyre, string rearright2ytrelimit, string totalrear2)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearright2tyre='" + rearright2tyre + "',rearright2ytrelimit='" + rearright2ytrelimit + "',totalrear2='" + totalrear2 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateReartleft3Tyredetails(string Vehicle_number, string rearleft3tyre, string rearleft3tyrelimit, string totalleft3)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearleft3tyre='" + rearleft3tyre + "',rearleft3tyrelimit='" + rearleft3tyrelimit + "',totalleft3='" + totalleft3 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateRearRight3Tyredetails(string Vehicle_number, string rearright3tyre, string rearright3ytrelimit, string totalrear3)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearright3tyre='" + rearright3tyre + "',rearright3ytrelimit='" + rearright3ytrelimit + "',totalrear3='" + totalrear3 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateReartleft4Tyredetails(string Vehicle_number, string rearleft4tyre, string rearleft4tyrelimit, string totalleft4)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearleft4tyre='" + rearleft4tyre + "',rearleft4tyrelimit='" + rearleft4tyrelimit + "',totalleft4='" + totalleft4 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
        internal int UpdateRearRight4Tyredetails(string Vehicle_number, string rearright4tyre, string rearright4ytrelimit, string totalrear4)
        {
            int i = 0;
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  SetTyreMilageEntry Set rearright4tyre='" + rearright4tyre + "',rearright4ytrelimit='" + rearright4ytrelimit + "',totalrear4='" + totalrear4 + "' where Vehicle_number='" + Vehicle_number + "'", sqlconn);
            i = cmd.ExecuteNonQuery();
            if (sqlconn.State == ConnectionState.Open)
                sqlconn.Close();
            return i;
        }
    }
}

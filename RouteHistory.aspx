﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RouteHistory.aspx.cs" Inherits="Tracking.RouteHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script src="Scripts/maps.js" type="text/javascript"></script>

</head>
<body style="background-color: Gray;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <%--    <asp:UpdatePanel ID="updatecontrol" runat="server">
        <ContentTemplate>--%>
    <div style="background-color: Gray; width: 100%; height: 100%">
        <table>
            <tr>
                <td valign="top">
                    <table border="2">
                        <tr>
                            <td style="color: White;">
                                <asp:Label ID="lblVehicle" Text="Select Vehicle" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="color: White;">
                                <asp:Label ID="Label1" Text="Date" runat="server"></asp:Label>
                            </td>
                            <td style="color: White;">
                                <asp:UpdatePanel ID="updatepanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                                        <asp:Calendar ID="Calendar1" OnSelectionChanged="calender_selected" runat="server">
                                        </asp:Calendar>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btView" runat="server" Text="View History" OnClick="btView_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:DataGrid ID="gdhistory" runat="server" Width="100%" AutoGenerateColumns="False"
                        DataKeyNames="ID">
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-Width="125px" HeaderText="" HeaderStyle-Font-Bold="true"
                                HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <asp:Label ID="label1" Text='<%# Bind("VehicalNumber") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-Width="125px" HeaderText="Device ID" HeaderStyle-Font-Bold="true"
                                HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <asp:Label ID="label1" Text='<%# Bind("Device_ID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
    <%--      </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
namespace Tracking
{
    public partial class Tempdatalog : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null)
                {
 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["cUser_id"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }
        }
        public DataTable binddatatemp()
        {
            DataTable dt = binddata();
            DataTable log = RemoveDuplicateRows(dt, "Date");
            DataTable dttemp = new DataTable();
            dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("Date", typeof(string));
            dttemp.Columns.Add("temp", typeof(double));
            int n = 0;
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    n++;
                    DataRow dr = dttemp.NewRow();
                    dr["sno"] = n.ToString();
                    dr["Date"] = log.Rows[i][0].ToString();
                    dr["Temp"] = log.Rows[i][1].ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }


        public DataTable binddata()
        {
            DataTable fre = db.Getfreez(Session["Username"].ToString());

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            string fdate = date.Text + " 12:00 AM";
            string tdate = date.Text + " 11:59 PM";


            DateTime dhdh = Convert.ToDateTime(fdate);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;

 
 
            DBClass db1 = new DBClass();
 
 
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("Date", typeof(string));
            dttemp.Columns.Add("temp", typeof(double));
            if (fre.Rows[0][1].ToString() == "Yes")
            {
                if (emonth == cmonth)
                {
                    DataTable dt = db.Select_Temp(Vno, fdate, tdate);

                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            // n++;
                            DataRow dr = dttemp.NewRow();
                            // dr["sno"] = n.ToString();
                            string ts = dt.Rows[i][0].ToString();

                            dr["Date"] = ts;
                            //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                            double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                            double temp = 0;
                            if (conditon1 >= 10000)
                            {
                                double cond1 = conditon1 - 10000;
                                if (cond1 > 1000)
                                {
                                    double cond2 = cond1 - 1000;
                                    temp = cond2 / 3.3;
                                    if (temp <= 7)
                                    {
                                        temp = temp * 1.9;
                                    }
                                    else if (temp <= 10)
                                    {
                                        temp = temp * 1.7;
                                    }
                                    else if (temp <= 14)
                                    {
                                        temp = temp * 1.4;
                                    }
                                    else if (temp <= 17)
                                    {
                                        temp = temp * 1.3;
                                    }
                                    else if (temp <= 20)
                                    {
                                        temp = temp * 1.1;
                                    }
                                    else if (temp > 20)
                                    {
                                        temp = temp * 1;
                                    }

                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = "-" + temp.ToString();

                                }
                                else if (cond1 < 1000)
                                {
                                    temp = cond1 / 3.3;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = temp.ToString();
                                }
                            }
                            else if (conditon1 >= 1000)
                            {
                                double con1 = conditon1 - 1000;
                                temp = con1 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }

                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                            else if (conditon1 < 1000)
                            {
                                temp = conditon1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                            }
                            dttemp.Rows.Add(dr);
                        }
                    }
                }
                else
                {
                    DataTable dt = db.Select_Tempold(Vno, fdate, tdate);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            // n++;
                            DataRow dr = dttemp.NewRow();
                            // dr["sno"] = n.ToString();
                            string ts = dt.Rows[i][0].ToString();

                            dr["Date"] = ts;
                            //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                            double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                            double temp = 0;
                            if (conditon1 >= 10000)
                            {
                                double cond1 = conditon1 - 10000;
                                if (cond1 > 1000)
                                {
                                    double cond2 = cond1 - 1000;
                                    temp = cond2 / 3.3;
                                    if (temp <= 7)
                                    {
                                        temp = temp * 1.9;
                                    }
                                    else if (temp <= 10)
                                    {
                                        temp = temp * 1.7;
                                    }
                                    else if (temp <= 14)
                                    {
                                        temp = temp * 1.4;
                                    }
                                    else if (temp <= 17)
                                    {
                                        temp = temp * 1.3;
                                    }
                                    else if (temp <= 20)
                                    {
                                        temp = temp * 1.1;
                                    }
                                    else if (temp > 20)
                                    {
                                        temp = temp * 1;
                                    }

                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = "-" + temp.ToString();

                                }
                                else if (cond1 < 1000)
                                {
                                    temp = cond1 / 3.3;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = temp.ToString();
                                }
                            }
                            else if (conditon1 >= 1000)
                            {
                                double con1 = conditon1 - 1000;
                                temp = con1 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }

                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                            else if (conditon1 < 1000)
                            {
                                temp = conditon1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                            }
                            dttemp.Rows.Add(dr);
                        }
                    }

                }

            }
            //end to Getfreez condition
            else
            {
                if (emonth == cmonth)
                {
                    DataTable dt = db.Select_Temp(Vno, fdate, tdate);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            // n++;
                            DataRow dr = dttemp.NewRow();
                            // dr["sno"] = n.ToString();
                            string ts = dt.Rows[i][0].ToString();

                            dr["Date"] = ts;
                            //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                            double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                            double temp = 0;
                            if (conditon1 >= 10000)
                            {
                                double cond1 = conditon1 - 10000;
                                if (cond1 > 1000)
                                {
                                    double cond2 = cond1 - 1000;
                                    temp = cond2 / 3.3;
                                    //temp = temp * 1.9;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = "-" + temp.ToString();
                                    //lbltemp.Text = " - " + temp.ToString() + " °C";
                                }
                                else if (cond1 < 1000)
                                {
                                    temp = cond1 / 3.3;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = temp.ToString();
                                }
                            }
                            else if (conditon1 >= 1000)
                            {
                                double con1 = conditon1 - 1000;
                                temp = con1 / 3.3;
                                //temp = temp * 1.9;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                            else if (conditon1 < 1000)
                            {
                                temp = conditon1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                            }
                            dttemp.Rows.Add(dr);
                        }
                    }
                }
                else 
                {
                    DataTable dt = db.Select_Tempold(Vno, fdate, tdate);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            // n++;
                            DataRow dr = dttemp.NewRow();
                            // dr["sno"] = n.ToString();
                            string ts = dt.Rows[i][0].ToString();

                            dr["Date"] = ts;
                            //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                            double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                            double temp = 0;
                            if (conditon1 >= 10000)
                            {
                                double cond1 = conditon1 - 10000;
                                if (cond1 > 1000)
                                {
                                    double cond2 = cond1 - 1000;
                                    temp = cond2 / 3.3;
                                    //temp = temp * 1.9;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = "-" + temp.ToString();
                                    //lbltemp.Text = " - " + temp.ToString() + " °C";
                                }
                                else if (cond1 < 1000)
                                {
                                    temp = cond1 / 3.3;
                                    temp = (double)Math.Round(temp, 1);
                                    dr["Temp"] = temp.ToString();
                                }
                            }
                            else if (conditon1 >= 1000)
                            {
                                double con1 = conditon1 - 1000;
                                temp = con1 / 3.3;
                                //temp = temp * 1.9;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                            else if (conditon1 < 1000)
                            {
                                temp = conditon1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                            }
                            dttemp.Rows.Add(dr);
                        }
                    }
                
                }
            }
            return dttemp;

        }
        public DataTable minusdatabind()
        {
            DataTable fre = db.Getfreez(Session["Username"].ToString());

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            string fdate = date.Text + " 12:00 AM";
            string tdate = date.Text + " 11:59 PM";
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable dttemp = new DataTable();
            dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("Date", typeof(string));
            dttemp.Columns.Add("temp", typeof(string));
            if (fre.Rows[0][1].ToString() == "Yes")
            {

                int n = 0;
                DataTable dt = db.Select_Temp(Vno, fdate, tdate);
                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();

                        n++;

                        dr["sno"] = n.ToString();
                        dr["Date"] = dt.Rows[i][0].ToString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {

                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {

                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                        }

                        dttemp.Rows.Add(dr);
                    }
                }
            }

            else
            {


                int n = 0;
                DataTable dt = db.Select_Temp(Vno, fdate, tdate);
                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();

                        n++;

                        dr["sno"] = n.ToString();
                        dr["Date"] = dt.Rows[i][0].ToString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {

                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;

                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {

                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                        }

                        dttemp.Rows.Add(dr);
                    }
                }
            }
            return dttemp;

        }
        public DataTable minusdata()
        {
            DataTable dt = minusdatabind();
            bool flag = false;
            // int counter = 0;
            //EXIT:
            //for (int i = counter; i < dt.Columns.Count; i++)
            //{
            for (int x = 0; x < dt.Rows.Count; x++)
            {
                if (string.IsNullOrEmpty(dt.Rows[x][2].ToString()))
                {
                    flag = true; //means there is an empty value
                }
                else
                {
                    //means if it found non null or empty in rows of a particular column
                    flag = false;
                    //counter = i + 1;
                    //goto EXIT;
                }
            }


            if (flag == true)
            {
                dt.Columns.Remove(dt.Columns[2]);

            }

            //}
            return dt;
        }
        public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {

            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }


            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            return dTable;
        }

        protected void GetRecord_Click1(object sender, EventArgs e)
        {
            if (Convert.ToDateTime(date.Text) < DateTime.Today)
            {
                if (rbttype.Checked)
                {
                    DataTable log = binddatatemp();
                    if (log.Rows.Count > 0)
                    {
                        DataTable first = new DataTable();
                        first.Columns.Add("Date", typeof(string));
                        first.Columns.Add("temp", typeof(string));
                        int count = log.Rows.Count;
                        int N = (int)(count / 8);
                        int n = N;
                        int i = 0;
                        for (i = 0; i < n; i++)
                        {
                            if (i < count)
                            {
                                DataRow dr = first.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                first.Rows.Add(dr);
                            }
                        }
                        gvreport.DataSource = first;
                        gvreport.DataBind();
                        ////gvreport.Attributes.CssStyle .Add("font-size", "3px");
                        n = N * 2;
                        DataTable second = new DataTable();
                        second.Columns.Add("Date", typeof(string));
                        second.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = second.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                second.Rows.Add(dr);
                            }
                        }
                        gvreport1.DataSource = second;
                        gvreport1.DataBind();
                        n = N * 3;
                        DataTable third = new DataTable();
                        third.Columns.Add("Date", typeof(string));
                        third.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = third.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                third.Rows.Add(dr);
                            }
                        }
                        gvreport2.DataSource = third;
                        gvreport2.DataBind();
                        n = N * 4;
                        DataTable four = new DataTable();
                        four.Columns.Add("Date", typeof(string));
                        four.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = four.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                four.Rows.Add(dr);
                            }
                        }
                        gvreport3.DataSource = four;
                        gvreport3.DataBind();
                        n = N * 5;
                        DataTable five = new DataTable();
                        five.Columns.Add("Date", typeof(string));
                        five.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = five.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                five.Rows.Add(dr);
                            }
                        }
                        gvreport4.DataSource = five;
                        gvreport4.DataBind();
                        n = N * 6;
                        DataTable six = new DataTable();
                        six.Columns.Add("Date", typeof(string));
                        six.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = six.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                six.Rows.Add(dr);
                            }
                        }
                        gvreport5.DataSource = six;
                        gvreport5.DataBind();
                        n = N * 7;
                        DataTable seven = new DataTable();
                        seven.Columns.Add("Date", typeof(string));
                        seven.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = seven.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                seven.Rows.Add(dr);
                            }

                        }
                        gvreport6.DataSource = seven;
                        gvreport6.DataBind();
                        n = N * 8;
                        DataTable eight = new DataTable();
                        eight.Columns.Add("Date", typeof(string));
                        eight.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                        for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                        {
                            if (i < count)
                            {
                                DataRow dr = eight.NewRow();
                                string ts = log.Rows[i][1].ToString();
                                string ets = log.Rows[i][2].ToString();
                                dr["Date"] = ts.ToString();
                                dr["temp"] = ets.ToString();
                                eight.Rows.Add(dr);
                            }
                        }
                        gvreport7.DataSource = eight;
                        gvreport7.DataBind();

                        //DataTable nine = new DataTable();
                        //nine.Columns.Add("Date", typeof(string));
                        //nine.Columns.Add("temp", typeof(string));
                        //for (int i = Convert.ToInt32(log.Rows[1119][0].ToString()); i <= Convert.ToInt32(log.Rows[1258][0].ToString()); i++)
                        //{
                        //    DataRow dr = nine.NewRow();
                        //    string ts = log.Rows[i][1].ToString();
                        //    string ets = log.Rows[i][2].ToString();
                        //    dr["Date"] = ts.ToString();
                        //    dr["temp"] = ets.ToString();
                        //    nine.Rows.Add(dr);

                        //}
                        //gvreport8.DataSource = nine;
                        //gvreport8.DataBind();

                        //DataTable ten = new DataTable();
                        //ten.Columns.Add("Date", typeof(string));
                        //ten.Columns.Add("temp", typeof(string));
                        //for (int i = Convert.ToInt32(log.Rows[1259][0].ToString()); i < Convert.ToInt32(log.Rows.Count); i++)
                        //{
                        //    DataRow dr = ten.NewRow();
                        //    string ts = log.Rows[i][1].ToString();
                        //    string ets = log.Rows[i][2].ToString();
                        //    dr["Date"] = ts.ToString();
                        //    dr["temp"] = ets.ToString();
                        //    ten.Rows.Add(dr);

                        //}
                        //gvreport9.DataSource = ten;
                        //gvreport9.DataBind();
                        Panel1.Visible = true;
                    }
                    else
                    {
                        lblerror.Visible = true;
                        lblerror.Text = "No data avilable...";
                    }
                }
                else
                {
                    DataTable log = binddatatemp();
                    if (log.Rows.Count > 0)
                    {
                        DataTable first = new DataTable();
                        DataView dv = new DataView(log);
                        dv.RowFilter = "temp<0";
                        log = dv.ToTable();
                        if (log.Rows.Count > 0)
                        {
                            first.Columns.Add("Date", typeof(string));
                            first.Columns.Add("temp", typeof(string));
                            int count = log.Rows.Count;
                            int N = (int)(count / 8);
                            int n = N;
                            int i = 0;
                            for (i = 0; i < n; i++)
                            {
                                if (i < count)
                                {
                                    DataRow dr = first.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    first.Rows.Add(dr);
                                }
                            }
                            gvreport.DataSource = first;
                            gvreport.DataBind();
                            ////gvreport.Attributes.CssStyle .Add("font-size", "3px");
                            n = N * 2;
                            DataTable second = new DataTable();
                            second.Columns.Add("Date", typeof(string));
                            second.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = second.NewRow();
                                    string ts = log.Rows[i][1].ToString();  
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    second.Rows.Add(dr);
                                }
                            }
                            gvreport1.DataSource = second;
                            gvreport1.DataBind();
                            n = N * 3;
                            DataTable third = new DataTable();
                            third.Columns.Add("Date", typeof(string));
                            third.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = third.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    third.Rows.Add(dr);
                                }
                            }
                            gvreport2.DataSource = third;
                            gvreport2.DataBind();
                            n = N * 4;
                            DataTable four = new DataTable();
                            four.Columns.Add("Date", typeof(string));
                            four.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = four.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    four.Rows.Add(dr);
                                }
                            }
                            gvreport3.DataSource = four;
                            gvreport3.DataBind();
                            n = N * 5;
                            DataTable five = new DataTable();
                            five.Columns.Add("Date", typeof(string));
                            five.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = five.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    five.Rows.Add(dr);
                                }
                            }
                            gvreport4.DataSource = five;
                            gvreport4.DataBind();
                            n = N * 6;
                            DataTable six = new DataTable();
                            six.Columns.Add("Date", typeof(string));
                            six.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = six.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    six.Rows.Add(dr);
                                }
                            }
                            gvreport5.DataSource = six;
                            gvreport5.DataBind();
                            n = N * 7;
                            DataTable seven = new DataTable();
                            seven.Columns.Add("Date", typeof(string));
                            seven.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = seven.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    seven.Rows.Add(dr);
                                }

                            }
                            gvreport6.DataSource = seven;
                            gvreport6.DataBind();
                            n = N * 8;
                            DataTable eight = new DataTable();
                            eight.Columns.Add("Date", typeof(string));
                            eight.Columns.Add("temp", typeof(string));
#pragma warning disable CS1717 // Assignment made to same variable; did you mean to assign something else?
                            for (i = i; i < n; i++)
#pragma warning restore CS1717 // Assignment made to same variable; did you mean to assign something else?
                            {
                                if (i < count)
                                {
                                    DataRow dr = eight.NewRow();
                                    string ts = log.Rows[i][1].ToString();
                                    string ets = log.Rows[i][2].ToString();
                                    dr["Date"] = ts.ToString();
                                    dr["temp"] = ets.ToString();
                                    eight.Rows.Add(dr);
                                }
                            }
                            gvreport7.DataSource = eight;
                            gvreport7.DataBind();

                            //DataTable nine = new DataTable();
                            //nine.Columns.Add("Date", typeof(string));
                            //nine.Columns.Add("temp", typeof(string));
                            //for (int i = Convert.ToInt32(log.Rows[1119][0].ToString()); i <= Convert.ToInt32(log.Rows[1258][0].ToString()); i++)
                            //{
                            //    DataRow dr = nine.NewRow();
                            //    string ts = log.Rows[i][1].ToString();
                            //    string ets = log.Rows[i][2].ToString();
                            //    dr["Date"] = ts.ToString();
                            //    dr["temp"] = ets.ToString();
                            //    nine.Rows.Add(dr);

                            //}
                            //gvreport8.DataSource = nine;
                            //gvreport8.DataBind();

                            //DataTable ten = new DataTable();
                            //ten.Columns.Add("Date", typeof(string));
                            //ten.Columns.Add("temp", typeof(string));
                            //for (int i = Convert.ToInt32(log.Rows[1259][0].ToString()); i < Convert.ToInt32(log.Rows.Count); i++)
                            //{
                            //    DataRow dr = ten.NewRow();
                            //    string ts = log.Rows[i][1].ToString();
                            //    string ets = log.Rows[i][2].ToString();
                            //    dr["Date"] = ts.ToString();
                            //    dr["temp"] = ets.ToString();
                            //    ten.Rows.Add(dr);

                            //}
                            //gvreport9.DataSource = ten;
                            //gvreport9.DataBind();
                            Panel1.Visible = true;
                        }
                        else
                        {
                            lblerror.Visible = true;
                            lblerror.Text = "No Reefer data avilable...";
                        }
                    }
                    else
                    {
                        lblerror.Visible = true;
                        lblerror.Text = "No Reefer data avilable...";
                    }
                }
            }
           

        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Items.Clear();
            Response.Redirect(Request.RawUrl);
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            if (date.Text != "")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = Convert.ToDateTime(date.Text);
                string str = dt.ToString("yyyy-MM-dd");
                string str1 = str.Replace("-", "");
                str1 = str1.Replace(" ", "");
                str1 = str1.Substring(2);
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VDSR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();

                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);

                TableCell cell2 = new TableCell();
                cell2.Controls.Add(gvreport1);
                tr1.Cells.Add(cell2);

                TableCell cell3 = new TableCell();
                cell3.Controls.Add(gvreport2);
                tr1.Cells.Add(cell3);

                TableCell cell4 = new TableCell();
                cell4.Controls.Add(gvreport3);
                tr1.Cells.Add(cell4);

                TableCell cell5 = new TableCell();
                cell5.Controls.Add(gvreport4);
                tr1.Cells.Add(cell5);

                TableCell cell6 = new TableCell();
                cell6.Controls.Add(gvreport5);
                tr1.Cells.Add(cell6);

                TableCell cell7 = new TableCell();
                cell7.Controls.Add(gvreport6);
                tr1.Cells.Add(cell7);

                TableCell cell8 = new TableCell();
                cell8.Controls.Add(gvreport7);
                tr1.Cells.Add(cell8);

                //TableCell cell9 = new TableCell();
                //cell9.Controls.Add(gvreport8);
                //tr1.Cells.Add(cell9);

                //TableCell cell10 = new TableCell();
                //cell10.Controls.Add(gvreport9);
                //tr1.Cells.Add(cell10);




                tb.Rows.Add(tr1);



                tb.RenderControl(hw);



                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void rbttype_CheckedChanged(object sender, EventArgs e)
        {
            if (rbttype.Checked)
            {
                gvreport.DataSource = null;
                gvreport.DataBind();
                gvreport1.DataSource = null;
                gvreport1.DataBind();
                gvreport2.DataSource = null;
                gvreport2.DataBind();
                gvreport3.DataSource = null;
                gvreport3.DataBind();
                gvreport4.DataSource = null;
                gvreport4.DataBind();
                gvreport5.DataSource = null;
                gvreport5.DataBind();
                gvreport6.DataSource = null;
                gvreport6.DataBind();
                gvreport7.DataSource = null;
                gvreport7.DataBind();
            }
        }

        protected void rbttype1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbttype1.Checked)
            {
                gvreport.DataSource = null;
                gvreport.DataBind();
                gvreport1.DataSource = null;
                gvreport1.DataBind();
                gvreport2.DataSource = null;
                gvreport2.DataBind();
                gvreport3.DataSource = null;
                gvreport3.DataBind();
                gvreport4.DataSource = null;
                gvreport4.DataBind();
                gvreport5.DataSource = null;
                gvreport5.DataBind();
                gvreport6.DataSource = null;
                gvreport6.DataBind();
                gvreport7.DataSource = null;
                gvreport7.DataBind();
            }
        }
    }

}

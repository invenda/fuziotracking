﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Changepassword.aspx.cs"
    Inherits="Tracking.Changepassword" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpage1" runat="server" ContentPlaceHolderID="MainContent">
 <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Change Password ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
                 	alert('Password Updated Successfully');

             }
             else {
                 document.getElementById(control).value = "0";
             }
         }               
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 600px; height: 400px; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
        <div id="sublogo" style="text-align: center; color: Blue;">
            <h2>
                <a>Change Password</a>
            </h2>
        </div>
        <div style="height: 20px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div>
            <asp:Panel ID="panel1" runat="server">
                <table style="width: 600px; height: auto;">
                    <tr>
                        <td style="width: 150px;">
                            User Name
                        </td>
                        <td style="width: 150px;">
                            <asp:Label ID="lbluname" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Old Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtop" runat="server" OnTextChanged="txtop_TextChanged" TextMode="Password"
                                AutoPostBack="true" autocomplete="off"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                ErrorMessage="Enter Current Password" ControlToValidate="txtop">*</asp:RequiredFieldValidator>
                            <asp:Label ID="lblUser" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            New Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtnp" runat="server" TextMode="Password" MaxLength="15" autocomplete="off"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                ErrorMessage="Enter New Password" ControlToValidate="txtnp">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Confirm Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtcp" runat="server" TextMode="Password" autocomplete="off"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcp"
                                ErrorMessage="Enter Confirm Password" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password not match"
                                ControlToCompare="txtnp" ValidationGroup="Group1" ControlToValidate="txtcp">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input id="inpHide" type="hidden" runat="server" />
                            <asp:Button ID="Btn_submit" runat="server" Text="Submit" OnClick="Btn_submit_Click"
                                ValidationGroup="Group1" OnClientClick="if(Page_ClientValidate()) ConfirmIt()"/>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

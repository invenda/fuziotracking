﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fuelgraph.aspx.cs" Inherits="Tracking.Fuelgraph"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report4" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
    function checkDate(sender,args)
    {
        if (sender._selectedDate > new Date())
        {
            alert("You cannot select a day earlier than today!");
            sender._selectedDate = new Date(); 
            // set the date back to the current date
            sender._textbox.set_Value(sender._selectedDate.format(sender._format))
        }
    }    
  
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 930px; background-color: White;">
        <div style="width: 100%; height: 40px; font-size: 25px; text-align: center; color: Blue;">
            <asp:Label ID="lblhead" Text="Vehicle Kilometer Graphs" runat="server"></asp:Label></div>
        <div style="width: 100%; height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div>
            <table width="100%">
                <tr>
                    <td align="right" style="width: 200px;">
                        <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                            ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left" style="width: 150px;">
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" style="width: 100px;">
                        <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>:
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                            ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left" style="width: 130px;">
                        <asp:TextBox ID="date" runat="server" Width="120px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date"
                            OnClientDateSelectionChanged="checkDate">
                        </cc1:CalendarExtender>
                    </td>
                    <td style="width: 100px;" align="right">
                        :
                    </td>
                    <td style="width: 150px;">
                        &nbsp;
                    </td>
                    <td style="width: 100px;">
                        <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" OnClick="GetRecord_Click" />
                    </td>
                    <td style="width: 120px;" align="center">
                        <%-- <input type="button" value="Print 1st Div" onclick="javascript:printDiv('Printdiv')" />
                       <input type="button" value="PrintDiv1" onclick="Print()" />--%>
                        <asp:Button ID="Download" Text="Download" runat="server" Width="70px" />
                    </td>
                    <td>
                        <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <p>
            <asp:Chart ID="Chart1" runat="server" Height="650px" Width="1290px" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)"
                ImageType="Png" BackColor="WhiteSmoke" BorderWidth="2" BackGradientStyle="TopBottom"
                BackSecondaryColor="White" Palette="BrightPastel" BorderlineDashStyle="Solid"
                BorderColor="26, 59, 105">
                 <Titles>                    
                    <asp:Title Docking="Left"  Font="Trebuchet MS, 14pt" BackColor="Transparent" Text="Kilometers"
                        DockingOffset="0" ForeColor="Chocolate">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Enabled="false"  IsTextAutoFit="False" Name="Default" BackColor="Transparent"  
                        LegendStyle="Row"    Docking="Bottom"  Font="Trebuchet MS, 10pt, style=Bold">
                    </asp:Legend>
                </Legends>
               
                <BorderSkin SkinStyle="Emboss"></BorderSkin>
                <Series>
                    <asp:Series Name="kilometers" ChartType="Spline"  ShadowColor="Black" BorderColor="180, 26, 59, 105"
                        YAxisType="Primary"  Color="Red">
                    </asp:Series>                   
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                        BackSecondaryColor="White" BackColor="Gainsboro" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                        <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                            WallWidth="0" IsClustered="False"></Area3DStyle>
                        <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Maximum="500" Minimum="0"
                            Interval="20">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisY>                        
                        <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False" >                            
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="64, 64, 64, 64" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </p>
        <%--  <asp:Chart ID="CrtDayReport" runat="server" Height="650px" Width="1341px" ImageLocation="~/tempImages/ChartPic_#SEQ(300,3)"
            Palette="None" BorderDashStyle="Solid" BorderWidth="2" BackColor="#213258">
            <Titles>
                <asp:Title Docking="Left" Font="Trebuchet MS, 10.25pt" Text="Fuel in liters and Tank Capacity" BackColor="Transparent"
                    DockingOffset="0" ForeColor="White">
                </asp:Title>
                <asp:Title Docking="Bottom" Font="Trebuchet MS, 10.25pt" BackColor="Transparent"
                    DockingOffset="0" ForeColor="White">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend LegendStyle="Row" Docking="Top" Name="Default" BackColor="Transparent"
                    Font="Trebuchet MS, 7.25pt" ForeColor="WhiteSmoke" Alignment="Far">
                </asp:Legend>
            </Legends>
            <BorderSkin SkinStyle="Emboss"></BorderSkin>
            <Series>
                <asp:Series ChartArea="ChartArea1" Name="Series1" ChartType="Line" LegendText="Kilometers" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series2" ChartType="Line" LegendText="Fuel"
                    Color="Red" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series3" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series4" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                     </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Name="Series5" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">                    
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series6" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">               
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series7" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series8" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series9" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series10" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series11" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series12" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series13" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series14" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                     </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Name="Series15" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">                    
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series16" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">               
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series17" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series18" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series19" ChartType="Line" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series20" ChartType="Line" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                  <asp:Series ChartArea="ChartArea1" Name="Series21" ChartType="Spline" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series22" ChartType="Spline" LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series23" ChartType="Spline" LegendText="Kilometers" IsVisibleInLegend ="false" 
                    Color="Green" BorderWidth="1">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" Name="Series24" ChartType="Line"  LegendText="Fuel" IsVisibleInLegend ="false" 
                    Color="Red" BorderWidth="1">
                </asp:Series>
                
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                    BackColor="#FFF0F5" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                    <AxisY LineColor="64, 64, 64, 64" Maximum="60" Minimum="0" IsLabelAutoFit="false"
                        TitleForeColor="White" Crossing="0" Interval="10">
                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                    </AxisY>
                    <AxisY2 Enabled="True " Minimum ="0" Maximum ="800" Interval ="50">
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY2>
                    <AxisX LineColor="64, 64, 64, 64" TitleForeColor="White" IntervalAutoMode="FixedCount"
                        TitleFont="Trebuchet MS, 10.25pt">
                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White" />
                        <MajorGrid LineColor="64, 64, 64, 64" />
                        <CustomLabels>
                            <asp:CustomLabel Text="" FromPosition="0" ToPosition="30"></asp:CustomLabel>
                            <asp:CustomLabel Text="1" FromPosition="50" ToPosition="60"></asp:CustomLabel>
                            <asp:CustomLabel Text="2" FromPosition="110" ToPosition="120"></asp:CustomLabel>
                            <asp:CustomLabel Text="3" FromPosition="170" ToPosition="180"></asp:CustomLabel>
                            <asp:CustomLabel Text="4" FromPosition="230" ToPosition="240"></asp:CustomLabel>
                            <asp:CustomLabel Text="5" FromPosition="290" ToPosition="300"></asp:CustomLabel>
                            <asp:CustomLabel Text="6" FromPosition="350" ToPosition="360"></asp:CustomLabel>
                            <asp:CustomLabel Text="7" FromPosition="410" ToPosition="420"></asp:CustomLabel>
                            <asp:CustomLabel Text="8" FromPosition="470" ToPosition="480"></asp:CustomLabel>
                            <asp:CustomLabel Text="9" FromPosition="530" ToPosition="540"></asp:CustomLabel>
                            <asp:CustomLabel Text="10" FromPosition="580" ToPosition="610"></asp:CustomLabel>
                            <asp:CustomLabel Text="11" FromPosition="640" ToPosition="670"></asp:CustomLabel>
                            <asp:CustomLabel Text="12" FromPosition="700" ToPosition="730"></asp:CustomLabel>
                            <asp:CustomLabel Text="13" FromPosition="770" ToPosition="800"></asp:CustomLabel>
                            <asp:CustomLabel Text="14" FromPosition="830" ToPosition="860"></asp:CustomLabel>
                            <asp:CustomLabel Text="15" FromPosition="890" ToPosition="920"></asp:CustomLabel>
                            <asp:CustomLabel Text="16" FromPosition="950" ToPosition="980"></asp:CustomLabel>
                            <asp:CustomLabel Text="17" FromPosition="1010" ToPosition="1040"></asp:CustomLabel>
                            <asp:CustomLabel Text="18" FromPosition="1070" ToPosition="1100"></asp:CustomLabel>
                            <asp:CustomLabel Text="19" FromPosition="1130" ToPosition="1160"></asp:CustomLabel>
                            <asp:CustomLabel Text="20" FromPosition="1190" ToPosition="1220"></asp:CustomLabel>
                            <asp:CustomLabel Text="21" FromPosition="1250" ToPosition="1280"></asp:CustomLabel>
                            <asp:CustomLabel Text="22" FromPosition="1310" ToPosition="1340"></asp:CustomLabel>
                            <asp:CustomLabel Text="23" FromPosition="1370" ToPosition="1400"></asp:CustomLabel>
                            <asp:CustomLabel Text="24" FromPosition="1410" ToPosition="1450"></asp:CustomLabel>
                        </CustomLabels>
                        
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>--%>
    </div>
</asp:Content>

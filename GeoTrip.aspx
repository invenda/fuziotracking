﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeoTrip.aspx.cs" Inherits="Tracking.GeoTrip"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="content1" runat="server">

    <script language="javascript" type="text/javascript">
 function alertsdetails(x)
        {        
         alert(x.join('\n'));  
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; margin-left: auto; margin-right: auto;">
        <table border="1" width="100%" style="background-color: White; border-color: Black;">
            <tr>
                <td>
                    <asp:Label ID="lblveh" runat="server" Text="Select Vehicle"></asp:Label>
                    :
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        AutoPostBack="true" DataTextField="Vehicalnumber" Width="150px" OnDataBound="ddlMapTOVehicle_DataBound"
                        OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="lblradius" runat="server" Text="Circle Radius"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtradius" ValidationGroup="Group1"></asp:RequiredFieldValidator>
                    :
                    <asp:TextBox ID="txtradius" runat="server" Width="150px"></asp:TextBox>&nbsp;<asp:Label
                        ID="lblmtrs" runat="server" Text="In Meters"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblstrat" runat="server" Text=" Enter Point Name"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                        ControlToValidate="txtpoint" ValidationGroup="Group1"></asp:RequiredFieldValidator>
                    :
                    <asp:TextBox ID="txtpoint" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblnop" runat="server" Text="No Of Geo Positions"></asp:Label>
                    :
                    <asp:Label ID="lblps" runat="server" Text="2" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblt" runat="server" Text="No Of Trips"></asp:Label>
                    :
                    <asp:Label ID="lbltrips" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="5" style="text-align: center;">
                    <asp:Button ID="btnsubmit" Text="Set GeoFence" runat="server" OnClick="btnsubmit_Click"
                        ValidationGroup="Group1" />&nbsp;&nbsp;
                    <asp:Button ID="Btndetails" runat="server" Text="Trip Details" OnClick="Btndetails_Click" />&nbsp;
                    &nbsp;
                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Visible="false" OnClick="btnRefresh_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btndelete" Text="Delete Trip" runat="server" Visible="false" OnClick="btndelete_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table style="width: 100%;" border="1">
            <tr>
                <td style="border-width: 2;">
                    <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="440px" enableServerEvents="true"
                        OnClick="GMap1_Click" />
                </td>
            </tr>
            <tr style="width: 100%;">
                <td>
                    <asp:GridView ID="geotripgv" runat="server" AutoGenerateColumns="false" Width="100%"
                        HeaderStyle-Font-Size="18px" DataKeyNames="Sno" HeaderStyle-Font-Bold="true"
                        HeaderStyle-BackColor="#61A6F8" RowStyle-VerticalAlign="Middle">
                        <Columns>
                            <asp:BoundField HeaderText="Sno" DataField="Sno" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Size="14px" />
                            <asp:BoundField HeaderText="Location" DataField="Location" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Size="14px" />
                            <asp:BoundField HeaderText="Radius(mtr)" DataField="Radius" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Size="14px" />
                            <asp:TemplateField HeaderText="No Of Trips" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtntrips" runat="server" Text='<% #Eval("Times")%>' OnClick="Triptimes"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Validate" DataField="validate" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Size="14px" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

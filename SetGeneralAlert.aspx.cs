﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class SetGeneralAlert : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
            }
        }

        protected void Btn_Submit_Click(object sender, EventArgs e)
        {

            db.UpdateVeh1(ddlMapTOVehicle.SelectedValue.ToString(), txtVehInsExpDt.Text, txtDrivLic.Text, txtrtodate.Text,  txtconfdate.Text, txtcontdate.Text, txtconname.Text, txtfcdate.Text, txtems.Text, txtroadtax.Text, txtGPRSActDt.Text);
            db.UpdateVehicle1(ddlMapTOVehicle.SelectedValue.ToString(), txtVehInsExpDt.Text, txtDrivLic.Text, txtrtodate.Text, txtconfdate.Text, txtcontdate.Text, txtconname.Text, txtfcdate.Text, txtems.Text, txtroadtax.Text,txtGPRSActDt.Text);
            refresh();  
        }
        public void refresh()
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void cbins_CheckedChanged(object sender, EventArgs e)
        {
            if (cbins.Checked == true)
            {
                txtVehInsExpDt.Visible = true;
                cbins.Text = "Disable";
            }
            else
            {
                txtVehInsExpDt.Text = string.Empty;
                txtVehInsExpDt.Visible = false;
                cbins.Text = "Enable";
            }
        }

        protected void cbrtodate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbrtodate.Checked == true)
            {
                txtrtodate.Visible = true;
                cbrtodate.Text = "Disable";
            }
            else
            {
                txtrtodate.Text = string.Empty;
                txtrtodate.Visible = false;
                cbrtodate.Text = "Enable";
            }
        }

        protected void cblicdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cblicdate.Checked == true)
            {
                txtDrivLic.Visible = true;
                cblicdate.Text = "Disable";
            }
            else
            {
                txtDrivLic.Text = string.Empty;
                txtDrivLic.Visible = false;
                cblicdate.Text = "Enable";
            }
        }
        protected void cbfcdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbfcdate.Checked == true)
            {
                txtfcdate.Visible = true;
                cbfcdate.Text = "Disable";
            }
            else
            {
                txtfcdate.Text = string.Empty;
                txtfcdate.Visible = false;
                cbfcdate.Text = "Enable";
            }
        }

        protected void cbems_CheckedChanged(object sender, EventArgs e)
        {
            if (cbems.Checked == true)
            {
                txtems.Visible = true;
                cbems.Text = "Disable";
            }
            else
            {
                txtems.Text = string.Empty;
                txtems.Visible = false;
                cbems.Text = "Enable";
            }
        }

        protected void cbroadtax_CheckedChanged(object sender, EventArgs e)
        {
            if (cbroadtax.Checked == true)
            {
                txtroadtax.Visible = true;
                cbroadtax.Text = "Disable";
            }
            else
            {
                txtroadtax.Text = string.Empty;
                txtroadtax.Visible = false;
                cbroadtax.Text = "Enable";
            }
        }

        protected void cbcontd_CheckedChanged(object sender, EventArgs e)
        {
            if (cbcontd.Checked == true)
            {
                txtconname.Visible = true;
                txtcontdate.Visible = true;
                txtconfdate.Visible = true;
                cbcontd.Text = "Disable";
            }
            else
            {
                txtconname.Text = string.Empty;
                txtcontdate.Text = string.Empty;
                txtconfdate.Text = string.Empty;
                txtconname.Visible = false;
                txtcontdate.Visible = false;
                txtconfdate.Visible = false;
                cbcontd.Text = "Enable";
            }
        }

        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Vehicle--");
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue != "--Select Vehicle--")
            {
                bindvehicledata(ddlMapTOVehicle.SelectedValue.ToString());

            }
        }
        public void bindvehicledata(string Vno)
        {
           
            DataTable EditV = db.LoadVeh(Vno);
            txtGPRSActDt.Text = EditV.Rows[0]["GPRS_Activated_Date"].ToString();
            txtVehInsExpDt.Text = EditV.Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            txtDrivLic.Text = EditV.Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            txtrtodate.Text = EditV.Rows[0]["Rto_Renewal_Date"].ToString();
            txtconfdate.Text = EditV.Rows[0]["Contract_Fromdate"].ToString();
            txtcontdate.Text = EditV.Rows[0]["Contract_Todate"].ToString();
            txtconname.Text = EditV.Rows[0]["Contract_Name"].ToString();
            txtfcdate.Text = EditV.Rows[0]["Rto_fc_date"].ToString();
            txtems.Text = EditV.Rows[0]["Emission_date"].ToString();
            txtroadtax.Text = EditV.Rows[0]["Road_tax_date"].ToString();            
            if (txtVehInsExpDt.Text == "")
            {
                cbins.Checked = false;
                txtVehInsExpDt.Visible = false;
                cbins.Text = "Enable";
            }
            else
            {
                txtVehInsExpDt.Visible = true;
                cbins.Checked = true;
                cbins.Text = "Disable";

            }
            if (txtDrivLic.Text == "")
            {
                txtDrivLic.Visible = false;
                cblicdate.Checked = false;
                cblicdate.Text = "Enable";
            }
            else
            {
                cblicdate.Checked = true;
                cblicdate.Text = "Disable";
            }
            if (txtrtodate.Text == "")
            {
                cbrtodate.Checked = false;
                txtrtodate.Visible = false;
                cbrtodate.Text = "Enable";

            }
            else
            {
                cbrtodate.Checked = true;
                txtrtodate.Visible = true;
                cbrtodate.Text = "Disable";
            }
            if (txtDrivLic.Text == "")
            {
                txtDrivLic.Visible = false;
                cblicdate.Checked = false;
                cblicdate.Text = "Enable";
            }
            else
            {
                txtDrivLic.Visible = true;
                cblicdate.Checked = true;
                cblicdate.Text = "Disable";
            }
            if (txtfcdate.Text == "")
            {
                txtfcdate.Visible = false;
                cbfcdate.Checked = false;
                cbfcdate.Text = "Enable";
            }
            else
            {
                txtfcdate.Visible = true;
                cbfcdate.Checked = true;
                cbfcdate.Text = "Disable";
            }
            if (txtems.Text == "")
            {
                txtems.Visible = false;
                cbems.Checked = false;
                cbems.Text = "Enable";
            }
            else
            {
                txtems.Visible = true;
                cbems.Checked = true;
                cbems.Text = "Disable";
            }
            if (txtroadtax.Text == "")
            {
                txtroadtax.Visible = false;
                cbroadtax.Checked = false;
                cbroadtax.Text = "Enable";
            }
            else
            {
                txtroadtax.Visible = true;
                cbroadtax.Checked = true;
                cbroadtax.Text = "Disable";
            }
            if (txtconname.Text == "")
            {
                txtconname.Visible = false;
                txtconfdate.Visible = false;
                txtcontdate.Visible = false;
                cbcontd.Checked = false;
                cbcontd.Text = "Enable";
            }
            else
            {
                txtconname.Visible = true;
                txtconfdate.Visible = true;
                txtcontdate.Visible = true;
                cbcontd.Checked = true;
                cbcontd.Text = "Disable";
            }

        }



    }
}

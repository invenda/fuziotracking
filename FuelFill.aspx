﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FuelFill.aspx.cs" Inherits="Tracking.FuelFill"
    EnableEventValidation="false" MasterPageFile="~/ESLMaster.Master" %>

<%-- <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <table>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" />
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
            ValidationGroup="Group2" ShowSummary="False" />
        <tr>
            <td align="left" style="width: 115px">
                Vehicle Number<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="ddlVeh" ValidationGroup="Group1" ErrorMessage="Select Vehicle Number">*</asp:RequiredFieldValidator>
            </td>
            <td align="left" style="width: 121px">
                Select Day
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFuelCapDT"
                    ValidationGroup="Group1" ErrorMessage="Select the Date">*</asp:RequiredFieldValidator>
            </td>
            <td align="left" style="width: 154px">
                Enter Reference No/<br />
                Challan Number
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRefChaNo"
                    ValidationGroup="Group1" ErrorMessage="Enter the Ref No./Challan No.">*</asp:RequiredFieldValidator>
            </td>
            <td style="width: 5px">
            </td>
            <td align="left" style="width: 118px">
                Number of Liters
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFuelCapacity"
                    ValidationGroup="Group1" ErrorMessage="Enter the Fuel Capacity">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter number of liters"
                    ValidationGroup="Group1" ValidationExpression="^\d*\.?(\d|\d\d|\d\d\d)?$" ControlToValidate="txtFuelCapacity">*</asp:RegularExpressionValidator>
            </td>
            <td style="width: 1px">
            </td>
            <td align="left" style="width: 210px">
                Name of the Filling Station
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 49px;">
                <asp:DropDownList ID="ddlVeh" runat="server" DataValueField="Vehicalnumber" Width="130px"
                    DataTextField="Vehicalnumber" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td style="width: 121px; height: 49px;">
                <asp:TextBox ID="txtFuelCapDT" runat="server" Height="22px" Width="100px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="txtFuelCapDT"
                    OnClientDateSelectionChanged="checkDate">
                </asp:CalendarExtender>
                <asp:Label ID="lblErrorMsg" runat="server" Text="*You cannot select a day earlier than today!"
                    Visible="false" ForeColor="Red">*</asp:Label>
            </td>
            <td style="width: 154px; height: 49px;">
                <asp:TextBox ID="txtRefChaNo" runat="server" Height="22px" Width="120px"></asp:TextBox>
            </td>
            <td style="width: 5px; height: 49px;">
            </td>
            <td style="width: 118px; height: 49px;">
                <asp:TextBox ID="txtFuelCapacity" runat="server" Height="22px" Width="100px"></asp:TextBox>
            </td>
            <td style="width: 1px; height: 49px;">
            </td>
            <td style="width: 210px; height: 49px;">
                <asp:TextBox ID="txtNamOfFilSt" runat="server" Height="22px" Width="200px"></asp:TextBox>
            </td>
            <td align="center" style="width: 73px; height: 49px;">
                <asp:Button ID="btnFuelCapEnter" runat="server" Font-Bold="true" Text="Enter" OnClick="btnFuelCapEnter_Click"
                    ValidationGroup="Group1" />
            </td>
            <td style="width: 81px; height: 49px;">
                <asp:Button ID="btnDelete" runat="server" Text="Delete" Font-Bold="true" Width="80px"
                    OnClick="btnDelete_Click" />
            </td>
            <td id="TdSearch" runat="server" visible="false" style="width: 400px; height: 49px;">
                <table>
                    <tr>
                        <td style="width: 90px">
                            Select Date
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="Group2"
                                ControlToValidate="txtFDate" ErrorMessage="Select Date">*</asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 110px">
                            <asp:TextBox ID="txtFDate" runat="server" Height="22px" Width="117px"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtFDate">
                            </asp:CalendarExtender>
                        </td>
                        <td style="width: 15px">
                            <asp:Button ID="bntOk" runat="server" Text="Ok" Font-Bold="true" ValidationGroup="Group2"
                                OnClick="bntOk_Click" />
                        </td>
                        <td style="width: 25px">
                            <asp:Button ID="btncancel" runat="server" Text="Cancel" Font-Bold="true" OnClick="btncancel_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <div>
        <asp:Calendar ID="Calendar1" runat="server" BackColor="#DAA520" DayStyle-VerticalAlign="Top"
            OnDayRender="CheckAppts" BorderColor="Black" DayNameFormat="Short" Font-Names="Times New Roman"
            DayStyle-HorizontalAlign="Left" Font-Size="12pt" ForeColor="Black" Height="300px"
            ShowGridLines="true" TitleFormat="MonthYear" Width="100%" BorderStyle="Dotted">
            <SelectedDayStyle BackColor="#9999FF" Font-Bold="True" />
            <SelectorStyle Font-Bold="True" Font-Names="Verdana" Font-Size="8pt" ForeColor="#333333"
                Width="1%" BackColor="#66FF33" BorderColor="Red" BorderStyle="Dotted" HorizontalAlign="Left"
                VerticalAlign="Top" />
            <%--<NextPrevStyle Font-Size="8pt" ForeColor="White"/>--%>
            <NextPrevStyle Font-Bold="True" Font-Size="Medium" />
            <OtherMonthDayStyle BackColor="#FFCC99" />
            <DayStyle BackColor="#FAFAD2" ForeColor="Black" Width="5%" BorderColor="Goldenrod" />
            <DayHeaderStyle BackColor="#F0E68C" Font-Bold="True" Font-Size="13pt" ForeColor="#333333"
                Height="10pt" Width="10%" BorderColor="#DAA520" />
            <TitleStyle BackColor="Khaki" Font-Bold="True" Font-Size="X-Large" ForeColor="Black"
                Height="20pt" BorderColor="#DAA520" BorderWidth="2px" Font-Italic="True" HorizontalAlign="Center"
                VerticalAlign="Middle" />
            <TodayDayStyle BackColor="#DAA520" ForeColor="#663300" Font-Bold="True" Font-Italic="False" />
        </asp:Calendar>
    </div>
    <br />
    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblLine" runat="server" Text="The total amount of Fuel input for this month is"
                    Visible="false"></asp:Label>
                &nbsp;
                <asp:Label ID="lblFperM" runat="server" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

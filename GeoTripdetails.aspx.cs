﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;


namespace Tracking
{
    public partial class GeoTripdetails : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //DataTable dt = (DataTable)Session["dttable"];
                //if (dt.Rows.Count != 0)
                //{
                //    gvgeodetails.DataSource = dt;
                //    gvgeodetails.DataBind();
                //}
                //else { lbltrip.Visible = true; lbltrip.Text = "No Trip Found"; }

                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
            }
        }

        protected void btnexit_Click(object sender, EventArgs e)
        {
            Response.Redirect("GeoTrip.aspx");
        }

        protected void gvgeodetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
                if (dt.Rows.Count != 0)
                {
                    e.Row.Cells[1].Text = dt.Rows[0][8].ToString() + " Exit Time";
                    e.Row.Cells[2].Text = dt.Rows[1][8].ToString() + " Exit Time";
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string fdate = TextBox1.Text + " " + "12:00 AM";
            string tdate = TextBox1.Text + " " + "11:59 PM";
            int svalid = 0, evalid = 0;

            List<string> startpoint = new List<string>();
            List<string> endpoint = new List<string>();

            DataTable dt = db.getgeotrip(ddlMapTOVehicle.SelectedValue.ToString());
            List<GLatLng> ex = points(Convert.ToDouble(dt.Rows[0][0]), Convert.ToDouble(dt.Rows[0][1]), Convert.ToDouble(dt.Rows[0][2]));
            List<GLatLng> ex1 = points(Convert.ToDouble(dt.Rows[1][0]), Convert.ToDouble(dt.Rows[1][1]), Convert.ToDouble(dt.Rows[1][2]));

            DataTable dt2 = db.getgeotrack1(Convert.ToString(dt.Rows[0][5]), fdate, tdate);
            string fdate1 = "";
            string fdate2 = "";
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                if (valid == true)
                {
                    fdate1 = Convert.ToString(dt2.Rows[j][2]);
                    fdate = fdate1;
                }
                else
                {
                    if (fdate1 != "")
                    {
                        if (fdate2 == "" || Convert.ToDateTime(fdate1) > Convert.ToDateTime(fdate2))
                        {
                            DateTime dtime = Convert.ToDateTime(fdate1);
                            startpoint.Add(dtime.ToShortTimeString());
                            svalid = svalid + 1;
                            DataTable dt3 = db.getgeotrack1(Convert.ToString(dt.Rows[0][5]), fdate, tdate);
                            fdate1 = "";
                            for (int k = 0; k < dt3.Rows.Count; k++)
                            {
                                bool val = IsPointInPolygon(ex1, Convert.ToDouble(dt3.Rows[k][0]), Convert.ToDouble(dt3.Rows[k][1]));
                                if (val == true)
                                {
                                    fdate1 = Convert.ToString(dt3.Rows[k][2]);
                                    fdate = fdate1;
                                }
                                else
                                {
                                    if (fdate1 != "")
                                    {
                                        DateTime dtime1 = Convert.ToDateTime(fdate1);
                                        evalid = evalid + 1;
                                        endpoint.Add(dtime1.ToShortTimeString());
                                        fdate2 = fdate1;
                                        //trip(fdate, tdate);
                                        fdate1 = "";
                                        break;
                                    }
                                }
                            }
                            if (fdate1 != "")
                            {
                                DateTime dtime1 = Convert.ToDateTime(fdate1);
                                evalid = evalid + 1;
                                endpoint.Add(dtime1.ToShortTimeString());
                                fdate2 = fdate1;
                                fdate1 = "";
                            }
                        }
                    }
                }

            }
            int x = Convert.ToInt32(startpoint.Count);
            int y = Convert.ToInt32(endpoint.Count);
            int min = Math.Min(x, y);

            DataTable dttrip = new DataTable();
            dttrip.Columns.Add("Trip", typeof(string));
            dttrip.Columns.Add("time1", typeof(string));
            dttrip.Columns.Add("time2", typeof(string));
            dttrip.Columns.Add("totaltime", typeof(string));
            for (int i = 0, j = 1; i < min; i++)
            {
                DataRow dr = dttrip.NewRow();
                dr["Trip"] = j.ToString();
                dr["time1"] = startpoint[i].ToString();
                dr["time2"] = endpoint[i].ToString();
                TimeSpan ts = Convert.ToDateTime(endpoint[i].ToString()) - Convert.ToDateTime(startpoint[i].ToString());
                string str = ts.ToString();
                string retString = str.Substring(0, 5);
                dr["totaltime"] = retString;
                dttrip.Rows.Add(dr);
                j++;

            }
            if (dttrip.Rows.Count == 0)
            {
                gvgeodetails.DataSource = dttrip;
                gvgeodetails.DataBind();
                btnexit.Visible = true;
                lbltrip.Visible = true;
                lbltrip.Text = "No Trip Found";
            }
            else
            {
                lbltrip.Visible = false; lbltrip.Text = " "; btnexit.Visible = true;
                gvgeodetails.DataSource = dttrip;
                gvgeodetails.DataBind();
            }
            ddlMapTOVehicle.Enabled = false; TextBox1.Enabled = false;
        }
        public List<GLatLng> points(double Cx, double Cy, double radius)
        {
            double d2r = Math.PI / 180;   // degrees to radians
            double r2d = 180 / Math.PI;   // radians to degrees
            double earthsradius = 3963; // 3963 is the radius of the earth in miles
            double points = 30;
            double rlat = ((double)radius / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(Cx * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = Cy + (rlng * Math.Cos(theta));
                double ey = Cx + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            return extp;
        }
        private static bool IsPointInPolygon(List<GLatLng> poly, double Lt, double Lg)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].lat <= Lt) && (Lt < poly[j].lat)) |
                    ((poly[j].lat <= Lt) && (Lt < poly[i].lat))) &&
                    (Lg < (poly[j].lng - poly[i].lng) * (Lt - poly[i].lat) / (poly[j].lat - poly[i].lat) + poly[i].lng))
                    c = !c;
            }
            return c;
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Enabled = true;
            TextBox1.Enabled = true;
            lbltrip.Text = "";
            btnexit.Visible = false;
            gvgeodetails.DataSource = null;
            gvgeodetails.DataBind();
        }

    }
}

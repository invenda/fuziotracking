﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class ViewGeoRoutePoints : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;
                binddata();
            }
        }
        public void binddata()
        {
            DataTable dt = db.get_Routeno(Session["UserID"].ToString(), Session["selectedVehicle"].ToString());
            if (dt.Rows.Count > 0)
            {
                string rnos = dt.Rows[0][0].ToString();
                string sub = rnos.Substring(0, 3);
                if (sub == "ROU")
                {
                    vehiclepresentloc(Session["selectedVehicle"].ToString());
                    DataTable rrname = db.get_Geo_Routename(rnos, Session["UserID"].ToString());
                    DataTable dt1 = db.get_Geo_Routepointsnew(dt.Rows[0][0].ToString(), Session["UserID"].ToString());
                    for (int i = 0, j = 1; i < dt1.Rows.Count; i++, j++)
                    {
                        string rname = dt1.Rows[i][6].ToString();
                        drawcircle1(Convert.ToDouble(dt1.Rows[i][3].ToString()), Convert.ToDouble(dt1.Rows[i][4].ToString()), Convert.ToDouble(dt1.Rows[i][5].ToString()) / 1609.3, j, rnos, rname);
                    }
                    lblsrno.Text = "Route No :" + dt.Rows[0][0].ToString();
                    lblsvno.Text = "Vehicle No :" + Session["selectedVehicle"].ToString();
                    lblrname.Text = "Route Name :" + rrname.Rows[0][1].ToString();

                }
                else
                {
                    vehiclepresentloc(Session["selectedVehicle"].ToString());

                    DataTable dt1 = db.get_Geo_Routepoints(dt.Rows[0][0].ToString(), Session["UserID"].ToString());
                    for (int i = 0, j = 1; i < dt1.Rows.Count; i++, j++)
                    {
                        drawcircle(Convert.ToDouble(dt1.Rows[i][4].ToString()), Convert.ToDouble(dt1.Rows[i][5].ToString()), Convert.ToDouble(dt1.Rows[i][6].ToString()) / 1609.3, j, rnos);
                    }
                    lblsrno.Text = "Route No :" + rnos;
                    lblsvno.Text = "Vehicle No :" + Session["selectedVehicle"].ToString();
                    lblrname.Text = "Route Name :" + dt1.Rows[0][3].ToString();

                }
            }
        }
        
        public void drawcircle(double lt, double lg, double r, int gp ,string vn)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 10, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            options.title = "Route No:" + vn;
            oMarker.options = options;

        }
        public void drawcircle1(double lt, double lg, double r, int gp, string vn, string rname)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 10, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            options.title = "Route No:-" + vn + "," + "Location:-" + rname; 
            oMarker.options = options;

        }
        public void vehiclepresentloc(string vno)
        {
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));

            GMap1.setCenter(gLatLng);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter("V", System.Drawing.Color.Green, System.Drawing.Color.Red);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
            GMap1.Add(oMarker);
            options.title = vno;
            oMarker.options = options;

        }
    }
}

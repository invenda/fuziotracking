﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Admin
{
    public partial class CreateVehicles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable View = db.ViewExactCust(Session["UserID"].ToString());
                txtsmsno.Text = View.Rows[0]["Cx_MobileNumber"].ToString();
            }
        }
        protected void btSubmit_Click(object sender, EventArgs e)
        {
            if (lblchkvno.Text != "Vehicle Exist")
            {
 
 
                DBClass db = new DBClass();
 
 
#pragma warning disable CS0168 // The variable 'TankType' is declared but never used
                string TankType;
#pragma warning restore CS0168 // The variable 'TankType' is declared but never used
                //if (rbtnPosTank.Checked)
                //{
                //    TankType = "0";
                //}
                //else
                //{
                //    TankType = "1";
                //}
                string Voltage;
                if (rbtn12V.Checked)
                {
                    Voltage = "12";
                }
                else
                {
                    Voltage = "24";
                }
                string Recharge;
                if (rbtnMonthly.Checked)
                {
                    Recharge = "Monthly";
                }
                else
                {
                    Recharge = "Yearly";
                }

                string sms = "35";
             
                string txtVehicleNo = "";// txtscode.Text.Trim() + txtdcode.Text.Trim() + " " + txtn.Text.Trim() + " " + txtnumber.Text.Trim();
               
                if (txtnumber.Text.Length < 4)
                {
                    txtVehicleNo = txtscode.Text.Trim() + txtdcode.Text.Trim() + " " + txtn.Text.Trim() + " " + "0" +txtnumber.Text;                    
                }
                else
                {
                    txtVehicleNo = txtscode.Text.Trim() + txtdcode.Text.Trim() + " " + txtn.Text.Trim() + " " + txtnumber.Text.Trim();
                }
               
                //string txtVehicleNo = txtscode.Text.Trim() + txtdcode.Text.Trim() + " " + txtn.Text.Trim() + " " + txtnumber.Text.Trim();
              
                db.CreateVehicle(txtVehicleNo, txtVehRegName.Text, Session["UserID"].ToString(), txtVehModel.Text, txtVehMake.Text, txtVehInsNum.Text, txtTankCapacity.Text, Voltage, txtExistMetRead.Text, txtKiloPulse.Text, txtDriverName.Text, txtDriverMobNum.Text, txtDriverADD.Text, txtDrivLicNum.Text, txtMobNum.Text, txtSIMProvider.Text, txtGPRSActDt.Text, Recharge, txtSNum.Text, txtDOP.Text, txtDealerName.Text, sms, "0","0", txtrtoname.Text, txtmsmmno.Text, txtmilage.Text, ddlpt.SelectedValue.ToString(), "0",ddlvc.SelectedValue .ToString());
                //string VehRegNo = txtVehicleNo.Text;
                Session["RegNo"] = txtVehicleNo;
                //VehRegNo = Convert.ToString(Session["VehRegNo"]);
                db.CreateVeh(txtVehicleNo, txtVehRegName.Text, Session["UserID"].ToString(), txtVehModel.Text, txtVehMake.Text, txtVehInsNum.Text, txtTankCapacity.Text, Voltage, txtExistMetRead.Text, txtKiloPulse.Text, txtDriverName.Text, txtDriverMobNum.Text, txtDriverADD.Text, txtDrivLicNum.Text, txtMobNum.Text, txtSIMProvider.Text, txtGPRSActDt.Text, Recharge, txtSNum.Text, txtDOP.Text, txtDealerName.Text, sms, "0", "0", txtrtoname.Text, txtmsmmno.Text, txtmilage.Text, ddlpt.SelectedValue.ToString(), "0", ddlvc.SelectedValue.ToString());
                //txtVehicleNo.Text = string.Empty;
                txtscode.Text = string.Empty;
                txtdcode.Text = string.Empty;
                txtn.Text = string.Empty;
                txtnumber.Text = string.Empty;
                txtVehRegName.Text = string.Empty;
                txtDriverName.Text = string.Empty;
                txtVehModel.Text = string.Empty;
                txtDriverMobNum.Text = string.Empty;
                txtVehMake.Text = string.Empty;
                txtDriverADD.Text = string.Empty;
                txtVehInsNum.Text = string.Empty;
                txtDrivLicNum.Text = string.Empty;
                //txtVehInsExpDt.Text = string.Empty;
                //txtDrivLic.Text = string.Empty;
                txtTankCapacity.Text = string.Empty;
                txtExistMetRead.Text = string.Empty;
                txtKiloPulse.Text = string.Empty;
                txtMobNum.Text = string.Empty;
                txtSNum.Text = string.Empty;
                txtSIMProvider.Text = string.Empty;
                txtDOP.Text = string.Empty;
                txtGPRSActDt.Text = string.Empty;
                txtDealerName.Text = string.Empty;
                
                //txtmis.Text = string.Empty;
                // txtfcdate.Text = string.Empty;
                //txtemdate.Text = string.Empty;
                //txtroadtdate.Text = string.Empty;
                //maxtemp.Text = string.Empty;
                Response.Redirect("JustVehView.aspx?RegNo=" + txtVehicleNo);
               // Response.Redirect("ViewVehDetails.aspx?VehRegNo=" + txtVehicleNo);
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Vehicle Created Successfully');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }
        }
        protected void cbsmsno_CheckedChanged(object sender, EventArgs e)
        {
            if (cbsmsno.Checked == true)
            {
                txtmsmmno.Visible = true;
                lblsmsno.Visible = true;
            }
            else
            {
                txtmsmmno.Visible = false;
                lblsmsno.Visible = false;
            }

        }
        protected void btClear_click(object sender, EventArgs e)
        {
           // txtVehicleNo.Text = string.Empty;
            txtscode.Text = string.Empty;
            txtdcode.Text = string.Empty;
            txtn.Text = string.Empty;
            txtnumber.Text = string.Empty;

        }

        protected void submit_Click(object sender, EventArgs e)
        {

        }

        protected void txtnumber_TextChanged(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            if (lblchkvno.Text == "Vehicle Exist")
            {
                lblchkvno.Text = "";
                lblchkvno.Visible = false;

            }
            string txtVehicleNo = txtscode.Text.Trim() + txtdcode.Text.Trim() + " " + txtn.Text.Trim() + " " + txtnumber.Text.Trim();
            int userdt = db.Check_Vehicle_No(txtVehicleNo);
            if (userdt == 1)
            {
                lblchkvno.Visible = true;
                lblchkvno.Text = "Vehicle Exist";
            }
            else lblchkvno.Text = "";
        }

        protected void txtmsmmno_TextChanged(object sender, EventArgs e)
        {

        }
        //protected void ckbins_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ckbins.Checked == true)
        //    {
        //        txtVehInsExpDt.Visible = true;
        //        ckbins.Text = "Disable";
        //    }
        //    else
        //    {
        //        txtVehInsExpDt.Text = string.Empty;
        //        txtVehInsExpDt.Visible = false;
        //        ckbins.Text = "Enable";
        //    }
        //}

        //protected void cbrtordate_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbrtordate.Checked == true)
        //    {
        //        txtrtodate.Visible = true;
        //        cbrtordate.Text = "Disable";

        //    }
        //    else
        //    {
        //        txtrtodate.Text = string.Empty;
        //        txtrtodate.Visible = false;
        //        cbrtordate.Text = "Enable";
        //    }
        //}

        //protected void cblicdate_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cblicdate.Checked == true)
        //    {

        //        txtDrivLic.Visible = true;
        //        cblicdate.Text = "Disable";

        //    }
        //    else
        //    {
        //        txtDrivLic.Text = string.Empty;
        //        txtDrivLic.Visible = false;
        //        cblicdate.Text = "Enable";
        //    }
        //}

        //protected void cbfcdate_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbfcdate.Checked == true)
        //    {

        //        txtfcdate.Visible = true;
        //        cbfcdate.Text = "Disable";

        //    }
        //    else
        //    {
        //        txtfcdate.Text = string.Empty;
        //        txtfcdate.Visible = false;
        //        cbfcdate.Text = "Enable";
        //    }

        //}

        //protected void cbemsdate_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbemsdate.Checked == true)
        //    {
        //        txtemdate.Visible = true;
        //        cbemsdate.Text = "Disable";
        //    }
        //    else
        //    {
        //        txtemdate.Text = string.Empty;
        //        txtemdate.Visible = false;
        //        cbemsdate.Text = "Enable";
        //    }
        //}

        //protected void cbroattax_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbroattax.Checked == true)
        //    {
        //        txtroadtdate.Visible = true;
        //        cbroattax.Text = "Disable";
        //    }
        //    else
        //    {
        //        txtroadtdate.Text = string.Empty;
        //        txtroadtdate.Visible = false;
        //        cbroattax.Text = "Enable";
        //    }
        //}

        //protected void cbovers_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbovers.Checked == true)
        //    {
        //        txtoverspeed.Visible = true;
        //        cbovers.Text = "Disable";
        //    }
        //    else
        //    {
        //        txtoverspeed.Text = string.Empty;
        //        txtoverspeed.Visible = false;
        //        cbovers.Text = "Enable";
        //    }
        //}

        //protected void cbcontname_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbcontname.Checked == true)
        //    {
        //        contactfdate.Visible = true;
        //        contactTdate.Visible = true;
        //        contname.Visible = true;
        //        lblconttodate.Visible = true;
        //        cbcontname.Text = "Disable";
        //    }
        //    else
        //    {
        //        contactfdate.Text = string.Empty;
        //        contactTdate.Text = string.Empty;
        //        contname.Text = string.Empty;
        //        contactfdate.Visible = false;
        //        contactTdate.Visible = false;
        //        contname.Visible = false;
        //        lblconttodate.Visible = false;
        //        cbcontname.Text = "Enable";
        //    }
        //}

        //protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        //{

        //    if (CheckBox1.Checked == true)
        //    {
        //        maxtemp.Visible = true;
        //        CheckBox1.Text = "Disable";
        //    }
        //    else
        //    {
        //        maxtemp.Text = string.Empty;
        //        maxtemp.Visible = true;
        //        CheckBox1.Text = "Enable";
        //    }
        //}
    }
}

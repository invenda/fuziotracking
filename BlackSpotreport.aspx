﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlackSpotreport.aspx.cs"
    Inherits="Tracking.BlackSpotreport" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report3" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0; text-align: center;
        color: Blue;">
        <h1>
            Vehicle Black Spot Report</h1>
    </div>
    <div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" />
    </div>
    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                DataTextField="Vehicalnumber" Height="23px" Width="188px" Style="margin-left: 1px">
                            </asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                                ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="date" runat="server" Width="185px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date"
                                OnClientDateSelectionChanged="checkDate">
                            </cc1:CalendarExtender>
                            <%--<asp:Label ID="lblErrorMsg" runat="server" Text="*You cannot select a day earlier than today!"
                    Visible="false" ForeColor="Red"></asp:Label>--%>
                        </td>
                        <td>
                            <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                OnClick="GetRecord_Click" />
                        </td>
                        <td>
                            <asp:Button ID="Download" Text="Download Record" runat="server" ValidationGroup="Group1"
                                Width="120px" OnClick="Download_Click" Visible="false" />
                        </td>
                        <td>
                            <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px" OnClick="Refresh_Click" />
                        </td>
                    </tr>
                </table>
                </div>
                <div style="vertical-align: middle; text-align: center; height: 20px;">
                    <asp:Label ID="lblnorec" runat="server" Font-Bold="true" Font-Size="20px" ForeColor="Red"></asp:Label>
                </div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div style="vertical-align: middle; text-align: center; font-family: Times New Roman;">
                    <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                        ShowFooter="true" Width="1000px" OnRowCreated="gvreport_RowCreated" OnRowDataBound="gvreport_RowDataBound">
                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px" />
                        <Columns>
                            <asp:BoundField HeaderText="Sno" DataField="Sno" HeaderStyle-Width="50px" />
                            <asp:BoundField HeaderText="From Time" DataField="ftime" HeaderStyle-Width="80px" />
                            <asp:BoundField HeaderText="Location" DataField="floc" HeaderStyle-Width="300px"
                                ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText="To Time" DataField="ttime" HeaderStyle-Width="80px" />
                            <asp:BoundField HeaderText="End Location" DataField="tloc" HeaderStyle-Width="300px"
                                ItemStyle-HorizontalAlign="Left" FooterText="Black Spot Total" FooterStyle-HorizontalAlign="Right"
                                FooterStyle-BorderColor="#61A6F8" />
                            <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="110px" FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lbldur" runat="server" Text='<%#Bind("duration") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblftotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Download" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="height: 20px;">
        </div>
</asp:Content>

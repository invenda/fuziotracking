﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;


namespace Tracking
{
    public partial class VehicleAlertReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                vehiclebind();
            }
        }
        public void vehiclebind()
        {
            DataTable dt = new DataTable();
            if (Session["UserRole"].ToString() == "3")
            {
                dt = db.GetVehicles(Session["cUser_id"].ToString());
                //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                //dt = db.Get_CAminVeh(Session["UserID"].ToString(), CUsers);
            }
            else
            {
                dt = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
            } 
            DataTable dts = new DataTable();
            dts.Columns.Add("vrtno", typeof(string));
           // dts.Columns.Add("VehicleNumber", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dts.NewRow();
                dr["vrtno"] = "Vehicle RTO Number  :"+dt.Rows[i][0].ToString();
                //dr["VehicleNumber"] = dt.Rows[i][0].ToString();
                dts.Rows.Add(dr);
            }
            gvparent.DataSource = dts;
            gvparent.DataBind();
        }
        public DataTable  binddata(string Vno)
        {

            //string Vno = Vno;
                DataTable vdt = db.vehicledetailsalerts(Vno);
                string vinsEd = vdt.Rows[0][0].ToString();
                string rtorewd = vdt.Rows[0][1].ToString();
                string dlicexd = vdt.Rows[0][2].ToString();
                string overspeed = vdt.Rows[0][3].ToString();
                string conttodate = vdt.Rows[0][4].ToString();
                string gprsad = vdt.Rows[0][5].ToString();
                string fcdate = vdt.Rows[0][6].ToString();
                string emsdate = vdt.Rows[0][7].ToString();
                string roadtax = vdt.Rows[0][8].ToString();
                //string[] stralert = new string[10];
                DataTable dt1 = new DataTable();
                dt1.Columns.Add("Alert Type", typeof(string));
                dt1.Columns.Add("Vehicle Insurance", typeof(string));
                dt1.Columns.Add("RTO Permit", typeof(string));
                dt1.Columns.Add("Driving Licence", typeof(string));
                dt1.Columns.Add("FC Renewal", typeof(string));
                dt1.Columns.Add("Emission Date", typeof(string));
                dt1.Columns.Add("Road Tax Date", typeof(string));
                dt1.Columns.Add("Contract Date", typeof(string));
                dt1.Columns.Add("GPRS Activated", typeof(string)); 
                DateTime spdate = DateTime.Now;
                spdate = spdate.AddDays(30);
                DateTime spdate1 = DateTime.Now.AddDays(3);
                //DataRow dr = alertdt.NewRow();

                DataRow dr = dt1.NewRow();
                dr["Alert Type"] = "Status";
                int j = 0;
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };

                if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["Vehicle Insurance"] = " Expired on " + vinsEd;
                        j++;
                    }
                    else
                        dr["Vehicle Insurance"] = " Valid";

                }
                else { dr["Vehicle Insurance"] = "N/A"; }

                if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["RTO Permit"] = " Expired on " + rtorewd;
                        j++;
                    }
                    else
                        dr["RTO Permit"] = " Valid";


                }
                else { dr["RTO Permit"] = "N/A"; }

                if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
                {

                    TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["Driving Licence"] = " Expired on " + dlicexd;
                        j++;
                    }
                    else
                        dr["Driving Licence"] = " Valid";
                }
                else { dr["Driving Licence"] = "N/A"; }

                if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["FC Renewal"] = " Expired on " + fcdate;
                        j++;
                    }
                    else
                        dr["FC Renewal"] = " Valid";
                }
                else { dr["FC Renewal"] = "N/A"; }

                if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["Emission Date"] = " Expired on " + emsdate;
                        j++;
                    }
                    else
                        dr["Emission Date"] = " Valid";
                }
                else { dr["Emission Date"] = " N/A"; }


                if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["Road Tax Date"] = " Expired on " + roadtax;
                        j++;
                    }
                    else
                        dr["Road Tax Date"] = " Valid";
                }
                else { dr["Road Tax Date"] = "N/A"; }


                if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 30 && s >= 0 || s < 0)
                    {
                        dr["Contract Date"] = " Expired on " + conttodate;
                        j++;
                    }
                    else
                        dr["Contract Date"] = " Valid";
                }
                else { dr["Contract Date"] = "N/A"; }

                if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
                {
                    TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    int s = Convert.ToInt32(span.Days);
                    if (s <= 3 && s >= 0 || s < 0)
                    {
                        dr["GPRS Activated"] = " Expired on " + gprsad;
                        j++;
                    }
                    else
                        dr["GPRS Activated"] = " Valid";
                }
                else { dr["GPRS Activated"] = "N/A"; }

                dt1.Rows.Add(dr);
                //ds.Tables.Add(dt1);

                return dt1;
             }

            //gvalerts.DataSource = ds;
            //gvalerts.DataBind();
            ////for (int j = 0; j <ds.Tables.Count; j++)
            ////{ 
            ////GridView gv=new GridView();
            ////gv.DataSource = ds.Tables[j];
            
            ////}


            //}

        protected void gvparent_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gv = (GridView)e.Row.FindControl("gvchild");
                string v = e.Row.Cells[0].Text;
                string vno = v.Substring(21);
                //string  vno = e.Row.Cells[1].Text;
                DataTable dt = binddata(vno);
                gv.DataSource = dt;
                gv.DataBind();
            }

        }

        protected void btnprint_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            str1 = str1.Substring(2);
            //string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VAR_" + str1 + ".doc";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
          
            //Response.AddHeader("content-disposition", "attachment;filename=AlertReport.doc");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.word";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            // Create a form to contain the grid
            HtmlForm frm = new HtmlForm();
            gvparent.Parent.Controls.Add(frm);
            frm.Attributes["runat"] = "server";
            frm.Controls.Add(gvparent);
            frm.RenderControl(htmlWrite);
            //GridView1.RenderControl(htw);
            Response.Write(stringWrite.ToString());
            Response.End();
            //Response.Clear();
            //Response.Buffer = true;

            //Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.doc");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-word";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //Table tb = new Table();
            //TableRow tr1 = new TableRow();
            //TableCell cell1 = new TableCell();
            //cell1.Controls.Add(gvparent);
            //tr1.Cells.Add(cell1);
            //tb.Rows.Add(tr1);

            //tb.RenderControl(hw);

            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-document-format:\@; } </style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();


        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }
    }
    }


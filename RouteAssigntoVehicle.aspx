﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RouteAssigntoVehicle.aspx.cs"
    Inherits="Tracking.RouteAssigntoVehicle" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpalceholder1" runat="server" ContentPlaceHolderID="MainContent">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to UnMap The Route??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }         
    </script>

    <div>
        <div style="text-align: center; color: Blue;">
            <h2>
                Assign Route To Vehicle
            </h2>
        </div>
        <div style="width: 600px; margin: 0 auto; padding: 0; padding: 0;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbldevices" Text="Select Route No" runat="server" Font-Bold="true"
                            ForeColor="Black"></asp:Label>
                    </td>
                    <td>
                        <input id="inpHide" type="hidden" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList Width="180px" ID="ddlMapTOVehicle" runat="server" DataValueField="VehicalNumber"
                            DataTextField="VehicalNumber">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlMapTOVehicle"
                            ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:DropDownList Width="180px" ID="ddlRoutes" runat="server" DataValueField="Route_No"
                            DataTextField="Route_No">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlRoutes"
                            ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="center">
                        <asp:Button ID="btMap" runat="server" Text="Assign" ValidationGroup="Group1" OnClick="btMap_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 700px; margin: 0 auto; padding: 0; padding: 0; position: relative;
            vertical-align: top; height: 200px; overflow: auto;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" Width="100%"
                            DataKeyNames="Vehicle_number" HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black"
                            CellPadding="3">
                            <Columns>
                                <asp:TemplateField HeaderText="View Route" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="btnView" runat="server" Text="View" Height="24px" OnClick="btnView_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Vehicle_number" HeaderText="Vehical Number" />
                                <asp:BoundField DataField="Route_No" HeaderText="Route ID" />
                                <asp:BoundField DataField="Route_Name" HeaderText="Route Name" />
                                <asp:BoundField DataField="Route_Assigned_Date" HeaderText="Route Assigned On" />
                                <asp:TemplateField HeaderText="Function" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" OnClientClick="ConfirmIt()">DeAssign</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 800px; margin: 0 auto; padding: 0; padding: 0; height: auto;">
            <table border="1" style="width: 100%;">
                <tr>
                    <td>
                        <asp:Label ID="lblsvno" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblsrno" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="600px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

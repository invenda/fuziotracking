﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagerHome.aspx.cs" Inherits="Tracking.ManagerHome"
    MasterPageFile="~/TrackManager.Master" %>

<asp:Content ContentPlaceHolderID="MainContent" ID="CPH1" runat="server">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
        
        
        function alertsdetails(x) {
            alert(x.join('\n'));
        }   
                 
       
    </script>
    <script type="text/javascript">
        // SCRIPT FOR THE MOUSE EVENT.
        function MouseEvents(objRef, evt) {
            if (evt.type == "mouseover") { objRef.style.cursor = 'pointer'; objRef.style.backgroundColor = "Skyblue"; }
            else { if (evt.type == "mouseout") objRef.style.backgroundColor = "#CCFF99"; }
        }
    </script>     
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="120000">
    </asp:Timer>
    <div style="position: relative; vertical-align: top; height: 500px; overflow: hidden;
        background-color: White; top: 0px; left: 0px;">
        <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" BackColor="#CCFF99"
                    HorizontalAlign="Center" Width="100%" BorderColor="#FF33CC" ForeColor="Black"
                    OnRowDataBound="GV_RowDataBound" meta:resourcekey="GVResource1">
                    <RowStyle Height="5px" />
                    <AlternatingRowStyle Height="5px" />
                    <Columns>
                        <asp:TemplateField HeaderText="Select" meta:resourcekey="TemplateFieldResource1">
                            <ItemTemplate>
                                <asp:RadioButton ID="rbtn" runat="server" OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                    Width="20px" Height="20px" AutoPostBack="True" OnCheckedChanged="rbtn_Click"
                                    CssClass="rbtc " />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle RTO no" meta:resourcekey="TemplateFieldResource2">
                            <ItemTemplate>
                                <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("VehicalNumber") %>'
                                    Font-Bold="True" meta:resourcekey="lblvehiclenoResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Right" Font-Bold="False" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Engine status" meta:resourcekey="TemplateFieldResource3">
                            <ItemTemplate>
                                <asp:Image ID="Simg" runat="server" Height="16px" Width="16px" BackColor="Transparent"
                                    meta:resourcekey="SimgResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Check" meta:resourcekey="TemplateFieldResource4">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>' Font-Bold="True"
                                    meta:resourcekey="lblstatusResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit serial no" meta:resourcekey="TemplateFieldResource5">
                            <ItemTemplate>
                                <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblserialResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="70px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Data" meta:resourcekey="TemplateFieldResource6">
                            <ItemTemplate>
                                <asp:Label ID="lbllastdata" Text='<%# Bind("Lastdata") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbllastdataResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="130px" />
                            <ItemStyle Width="130px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GPS" meta:resourcekey="TemplateFieldResource7">
                            <ItemTemplate>
                                <asp:Label ID="lblgps" Text='<%# Bind("Gps") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblgpsResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ignition" meta:resourcekey="TemplateFieldResource8">
                            <ItemTemplate>
                                <asp:Label ID="lbling" Text='<%# Bind("ingnition") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblingResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="45px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Battery voltage" meta:resourcekey="TemplateFieldResource9">
                            <ItemTemplate>
                                <asp:Label ID="lblbvolt" Text='<%# Bind("bvolt") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lblbvoltResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver Name" meta:resourcekey="TemplateFieldResource10">
                            <ItemTemplate>
                                <asp:Label ID="lbldname" Text='<%# Bind("Dname") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbldnameResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver mobile" meta:resourcekey="TemplateFieldResource11">
                            <ItemTemplate>
                                <asp:Label ID="lbldmobile" Text='<%# Bind("dmobile") %>' runat="server" Font-Bold="True"
                                    meta:resourcekey="lbldmobileResource1"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Over Speed " meta:resourcekey="TemplateFieldResource12">
                            <ItemTemplate>
                                <asp:Button ID="btnlovs" runat="server" OnClick="btnlovs_Click" CssClass="button "
                                    meta:resourcekey="btnlovsResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" meta:resourcekey="TemplateFieldResource13">
                            <ItemTemplate>
                                <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" CssClass="button "
                                    meta:resourcekey="btnlocationResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Speed" meta:resourcekey="TemplateFieldResource14">
                            <ItemTemplate>
                                <asp:Button ID="btnspeed" runat="server" OnClick="btnspeed_Click" CssClass="button  "
                                    meta:resourcekey="btnspeedResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fuel" meta:resourcekey="TemplateFieldResource15">
                            <ItemTemplate>
                                <asp:Button ID="btnfuel" runat="server" OnClick="btnfuel_Click" CssClass="button "
                                    meta:resourcekey="btnfuelResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today KM run" meta:resourcekey="TemplateFieldResource16">
                            <ItemTemplate>
                                <asp:Button ID="btnKM" runat="server" OnClick="btnKM_Click" CssClass="button " meta:resourcekey="btnKMResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Temp" meta:resourcekey="TemplateFieldResource17">
                            <ItemTemplate>
                                <asp:Button ID="btntemp" runat="server" OnClick="btntemp_Click" CssClass="button  "
                                    meta:resourcekey="btntempResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Availability" meta:resourcekey="TemplateFieldResource18">
                            <ItemTemplate>
                                <asp:Button ID="btnavail" runat="server" OnClick="btnavail_Click" CssClass="button "
                                    meta:resourcekey="btnavailResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Alert" meta:resourcekey="TemplateFieldResource19">
                            <ItemTemplate>
                                <asp:Button ID="btnalert" runat="server" OnClick="btnalert_Click" CssClass="button "
                                    meta:resourcekey="btnalertResource1" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#006600" ForeColor="White" Height="3px" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--  <div>
        <table style="width: 100%;">
            <tr>
                <td>
                    <div style="position: relative; vertical-align: top; height: 600px; overflow: auto;
                        background-color: Gray;">
                        <asp:GridView ID="gdVehicles" runat="server" Width="100%" AutoGenerateColumns="False"
                            DataKeyNames="VehicalNumber" OnRowDataBound="gdVehicles_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderStyle-BackColor="Green" HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black"
                                    HeaderStyle-BorderWidth="2">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rbtn" runat="server" AutoPostBack="true" OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                            OnCheckedChanged="rbtn_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vehicle RTO No." HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                    ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="vehicleno" Text='<%# Bind("VehicalNumber") %>' Font-Strikeout="false"
                                            OnClick="vehicleno_Click" runat="server" Font-Bold="true"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="Green" HeaderStyle-Width="16px" HeaderStyle-ForeColor="White"
                                    HeaderStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Image ID="Simg" runat="server" Height="16px" Width="16px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-Width="50px" HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Black"
                                    HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2" ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="label1" Text='<%# Bind("Status") %>' runat="server" Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit_Serial No." HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" ItemStyle-ForeColor="Black" HeaderStyle-BorderColor="Black"
                                    HeaderStyle-BorderWidth="2" ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Alerts" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-Width="50px" HeaderStyle-ForeColor="White" ItemStyle-VerticalAlign="Middle"
                                    HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2" ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Button ID="btnalerts" runat="server" Font-Size="9px" Height="17px" Text='<%# Bind("Vehicle_Alerts_Status") %>'
                                            Width="50px" BackColor="White" ForeColor="Black" Font-Bold="true" OnClick="btnalerts_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                    ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lblusername" Text='<%# Bind("UserName") %>' runat="server" Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Owner/Transporter" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                    ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltrans" Text='<%# Bind("Vehicle_Reg_Name") %>' runat="server" Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Driver Name" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                    ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDname" Text='<%# Bind("Driver_Name") %>' runat="server" Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Driver Number" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="Green"
                                    HeaderStyle-ForeColor="White" HeaderStyle-BorderColor="Black" HeaderStyle-BorderWidth="2"
                                    ItemStyle-BorderColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDmno" Text='<%# Bind("Driver_Mobile_Number") %>' runat="server"
                                            Font-Bold="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>--%>
</asp:Content>

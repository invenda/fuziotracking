﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssigUsertoManager.aspx.cs"
    Inherits="Tracking.AssigUsertoManager" MasterPageFile="~/ESMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to UnMap ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
                	alert('User UnMaped Successfully');
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }    
    </script>

    <div>
        <div id="sublogo" style="text-align: center;">
            <h1>
                <a>Assign Users To Manager</a>
            </h1>
        </div>
    </div>
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <div style="width: 500px; margin: 0 auto; padding: 0; padding: 0;">
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:Label ID="lbldevices" Text="Manager Name" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label1" Text="Map To User" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList Width="180px" ID="ddlManagerName" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlManagerName"
                        ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:DropDownList Width="180px" ID="ddlMapTOUsers" runat="server" DataValueField="User_ID" DataTextField="User_Name">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlMapTOUsers"
                        ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                </td>
                <td align="center">
                    <input id="inpHide" type="hidden" runat="server" />
                    <asp:Button ID="btMap" runat="server" Text="Map" ValidationGroup="Group1" OnClick="btMap_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 500px; margin: 0 auto; padding: 0; padding: 0; height: auto;">
        <table style="width: 100%;">
            <tr>
                <td style="height: 20px;">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" Width="100%"
                        DataKeyNames="User_Name" HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black"
                        CellPadding="3">
                        <Columns>
                            <%--<asp:BoundField DataField="User_ID" HeaderText="User ID" />--%>
                            <asp:BoundField DataField="User_Name" HeaderText="User Name" />
                            <asp:TemplateField HeaderText="Function">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" OnClientClick="ConfirmIt()">UnMap</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

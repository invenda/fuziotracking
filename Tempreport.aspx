﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tempreport.aspx.cs" Inherits="Tracking.Tempreport" MasterPageFile="~/ESMaster.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > yesday) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = yesday;
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="text-align: center; color: Blue;">
            <h1>
                Temperature Report
            </h1>
        </div>
        <div style="width: 1100px; height: auto; margin: 0 auto 0 85px; padding: 0; padding: 0; ">
            <table width="110%">
                <tr align ="left" >
                    <td align="right">
                        <asp:Label ID="Label1" Text="Select Vehicle :" runat="server" Font-Bold="true"></asp:Label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle" ValidationGroup="Group1"
                            ErrorMessage="Select the Vehicle Number" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            DataTextField="Vehicalnumber" Height="23px" Width="150px" Style="margin-left: 1px">
                        </asp:DropDownList>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label4" Text="From Date :" runat="server" Font-Bold="true"></asp:Label>
                        <%--<asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlmonth" ValidationGroup="Group1"
                            ErrorMessage="Select the Month" Operator="NotEqual" ValueToCompare="00">*</asp:CompareValidator>--%>
                    </td>
                    <td align="left">
                    <asp:TextBox ID="txtdate" runat="server" Width="150px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ce1" runat ="server"  Format="MM/dd/yyyy" TargetControlID="txtdate"  OnClientDateSelectionChanged="checkDate">
                         </cc1:CalendarExtender>
                        <%--<asp:DropDownList ID="ddlmonth" runat="server" Width="150px">
                        </asp:DropDownList>--%>
                    </td>
                     <td align="right">
                        <asp:Label ID="Label2" Text="To Date :" runat="server" Font-Bold="true"></asp:Label>
                       <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlmonth" ValidationGroup="Group1"
                            ErrorMessage="Select the Month" Operator="NotEqual" ValueToCompare="00">*</asp:CompareValidator>--%>
                    </td>
                    <td align="left">
                    <asp:TextBox ID="txtdate1" runat="server" Width="150px"></asp:TextBox>
                        <cc1:CalendarExtender ID="sdf" runat ="server"  Format="MM/dd/yyyy" TargetControlID="txtdate1" OnClientDateSelectionChanged="checkDate" >
                         </cc1:CalendarExtender>
                        <%--<asp:DropDownList ID="ddlmonth" runat="server" Width="150px">
                        </asp:DropDownList>--%>
                    </td>
                    <td>
                        <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" 
                            ValidationGroup="Group1" onclick="GetRecord_Click"  />
                    </td>
                    <td>
                        <asp:Button ID="Download" Text="Download Record" runat="server" ValidationGroup="Group1"
                            Width="120px" Enabled="False"    />
                    </td>
                    <td>
                        <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px"   />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 20px;">
        </div>
        <div>
            <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black" 
                ShowFooter="true" onrowcreated="gvreport_RowCreated" 
                onrowdatabound="gvreport_RowDataBound"                 >
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                    BorderWidth="0" />
                <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                <Columns>
                    <asp:TemplateField HeaderText="Date" HeaderStyle-Width="70px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="70px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="1" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH1" runat="server" Text='<% #Bind("1Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="2" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH2" runat="server" Text='<% #Bind("2Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="3" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH3" runat="server" Text='<% #Bind("3Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="4" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH4" runat="server" Text='<% #Bind("4Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="5" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH5" runat="server" Text='<% #Bind("5Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="6" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH6" runat="server" Text='<% #Bind("6Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="7" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH7" runat="server" Text='<% #Bind("7Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="8" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH8" runat="server" Text='<% #Bind("8Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="9" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH9" runat="server" Text='<% #Bind("9Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="10" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH10" runat="server" Text='<% #Bind("10Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="11" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH11" runat="server" Text='<% #Bind("11Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="12" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH12" runat="server" Text='<% #Bind("12Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="13" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH13" runat="server" Text='<% #Bind("13Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="14" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH14" runat="server" Text='<% #Bind("14Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="15" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH15" runat="server" Text='<% #Bind("15Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="16" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH16" runat="server" Text='<% #Bind("16Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="17" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH17" runat="server" Text='<% #Bind("17Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="18" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH18" runat="server" Text='<% #Bind("18Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="19" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH19" runat="server" Text='<% #Bind("19Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="20" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH20" runat="server" Text='<% #Bind("20Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="21" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH21" runat="server" Text='<% #Bind("21Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="22" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH22" runat="server" Text='<% #Bind("22Hour") %>'></asp:Label>
                        </ItemTemplate>

<FooterStyle BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="23" HeaderStyle-Width="35px" FooterStyle-BorderColor="#61A6F8"
                        FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblH23" runat="server" Text='<% #Bind("23Hour") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblf1" runat="server" Text=""></asp:Label>
                        </FooterTemplate>

<FooterStyle HorizontalAlign="Right" BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="24" HeaderStyle-Width="35px" FooterStyle-HorizontalAlign="Left"
                        FooterStyle-BorderColor="#61A6F8">
                        <ItemTemplate>
                            <asp:Label ID="lblH24" runat="server" Text='<% #Bind("24Hour") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblf2" runat="server" Text="Total"></asp:Label>
                        </FooterTemplate>

<FooterStyle HorizontalAlign="Left" BorderColor="#61A6F8"></FooterStyle>

<HeaderStyle Width="35px"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Run KM" HeaderStyle-Width="120px" HeaderStyle-BackColor="#8FBC8F" HeaderStyle-HorizontalAlign ="Right" 
                        ItemStyle-BackColor="#8FBC8F" FooterStyle-BackColor="#8FBC8F" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" >
                        <ItemTemplate>
                            <asp:Label ID="lbltotal" runat="server"></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                        </FooterTemplate>

<FooterStyle HorizontalAlign="Right" BackColor="#8FBC8F"></FooterStyle>

<HeaderStyle HorizontalAlign="Right" BackColor="#8FBC8F" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" BackColor="#8FBC8F"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>






﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagerTrack.aspx.cs" Inherits="Tracking.ManagerTrack"
    MasterPageFile="~/TrackManager.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="CPH1" runat="server">
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <table style="width: 100%; height: 800px; background-color: Gray;">
                                <tbody>
                                    <tr>
                                        <td style="width: 70%;">
                                            <asp:Timer ID="Timer2" runat="server" Interval="1" OnTick="Timer2_Tick">
                                            </asp:Timer>
                                            <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="800px" />
                                        </td>
                                        <td>
                                            <asp:UpdatePanel runat="server" ID="Panel1" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="height: 1px;" align="center" colspan="2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: White;" align="center" colspan="2">
                                                                    &nbsp;
                                                                    <asp:Label ID="Label5" runat='server' Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                        Font-Size="Small" Font-Strikeout="False" Text="Tracking Information"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold; width: 140px;">
                                                                    &nbsp;Current Unit Status
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblUnitStat" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Today&#39;s Availability
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblavailable" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Last Data Recorded
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblLocDT" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Trip&nbsp;
                                                                    <asp:Label ID="lblgeotype" runat="server" ForeColor="Red"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblgeofence" runat="server" Font-Bold="true"></asp:Label>
                                                                    &nbsp;&nbsp;<asp:LinkButton ID="lbtnview" runat="server" Text="View" Visible="false"
                                                                        Font-Bold="true" OnClientClick="opengeoview()"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;GPS Signal
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblGPSant" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle RTO Number
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblVehNo" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle Model
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblVehModel" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Latitude &amp; Longitude
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblLocation" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray;">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Ignition
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblEngine" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4;">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle Engine Status
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblEnginestatus" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Speed(in km/h)
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblspeed" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Over Speed&nbsp;
                                                                    <asp:Label ID="lblovs" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lbloverspeed" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Today Total Run&nbsp;
                                                                    <asp:Label ID="Label3" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lbltkm" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Total Meter Reading
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblTotKmRun" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Fuel Status(in ltr)
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblFuel" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Input Power
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblPower" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Vehicle Battery
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblVehBatStat" runat='server' Font-Bold="True"></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblcabin" runat="server" Text="Cabin Temperature" Font-Bold="True"></asp:Label>
                                                                    <asp:Label ID="lblreefer" runat="server" Text="Reefer Temperature" Font-Bold="True"
                                                                        Visible="false"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lbltemp" runat='server' Font-Bold="True"></asp:Label></span><asp:Label
                                                                        ID="lbltemp2" runat="server" Font-Bold="True" Font-Size="Smaller"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;AC Status&nbsp;&nbsp
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblac" runat='server' Font-Bold="True"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Driver Name&nbsp;&nbsp;
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblDriverName" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Driver Mobile No.
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblDriverMobNo" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: #7FFFD4">
                                                                <td style="font-weight: bold;">
                                                                    &nbsp;Owner/Transporter
                                                                </td>
                                                                <td>
                                                                    :<asp:Label ID="lblOwnerName" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: Gray;">
                                                                <td colspan="2" style="font-weight: bold; height: 51px;">
                                                                    &nbsp;Location :<asp:Label ID="lblPlace" runat='server' Font-Bold="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JustVehView.aspx.cs" Inherits="Tracking.JustVehView"
    EnableEventValidation="false" MasterPageFile="~/ESMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="sublogo" style="width: 1100px; margin: 0 auto; padding: 0; text-align: center;">
        <h1>
            View Vehicle Registration Details
        </h1>
    </div>
    <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0;">
        <table width="100%">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="color: Black; font-size: large; width: 200px;">
                    Vehicle Details
                </td>
                <td>
                </td>
                <td style="color: Black; font-size: large; width: 200px;">
                    Driver Details
                </td>
                <td>
                </td>
            </tr>
            </tr>
            <tr>
                <td>
                    Vehicle Registered Name:
                </td>
                <td>
                    <asp:Label ID="lblVehRegName" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driver Name:
                </td>
                <td>
                    <asp:Label ID="lblDriName" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Registration Number:
                </td>
                <td>
                    <asp:Label ID="lblVehRegNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driver Mobile Number:
                </td>
                <td>
                    <asp:Label ID="lblDriMobNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Model:
                </td>
                <td>
                    <asp:Label ID="lblVehModel" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Address:
                </td>
                <td>
                    <asp:Label ID="lblDriAddress" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Make:
                </td>
                <td>
                    <asp:Label ID="lblVehMake" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Driving Licence Number:
                </td>
                <td>
                    <asp:Label ID="lblDriLicNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Insurance Number:
                </td>
                <td>
                    <asp:Label ID="lblVehInsNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RTO Devision/Name:
                </td>
                <td>
                    <asp:Label ID="lblrtoname" runat="server" Text="Label"></asp:Label>
                </td>
                <td style="color: Black; font-size: large">
                    Device Details
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Existing Meter Reading:
                </td>
                <td>
                    <asp:Label ID="lblExistMetRead" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Serial Number:
                </td>
                <td>
                    <asp:Label ID="lblSerialNum" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Kilometer Pulse:
                </td>
                <td>
                    <asp:Label ID="lblKiloPulse" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    Date of Purchase:
                </td>
                <td>
                    <asp:Label ID="lblDOP" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Tank Capacity in Liters:
                    </td>
                <td>
                   <asp:Label ID="lblTankCap" runat="server" Text="Label"></asp:Label>
                    </td>
                <td>
                    Dealer Name:
                </td>
                <td>
                    <asp:Label ID="lblDealerName" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                   Battery Voltage Capacity:
                    </td>
                <td>
                    <asp:Label ID="lblBatVolt" runat="server" Text="Label"></asp:Label>
                    </td>
                <td>
                    SMS Alert No:
                </td>
                <td>
                    <asp:Label ID="lbldsmsno" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;(default)
                </td>
            </tr>
            <tr>
                <td>
                     Milage/liter
                    </td>
                <td>
                    <asp:Label ID="Milage" runat="server" Text="Label"></asp:Label>
                    </td>
                <td>
                    SMS Alert New NO:
                </td>
                <td>
                    <asp:Label ID="lblsmsmno" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td>
                    FuelType
                </td>
                <td>
                    <asp:Label ID="Fueltype" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
               <td>
                    Vehicle Category
                </td>
                
                <td>  
                    <asp:Label ID="vehcate" runat="server" Text="Label"></asp:Label>
                   
                 </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            
             <tr>
                <td>
                    </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="color: Black; font-size: large">
                    SIM Details
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Number:
                </td>
                <td>
                    <asp:Label ID="lblMobNum" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SIM Provider:
                </td>
                <td>
                    <asp:Label ID="lblSIMProvider" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    GPRS Expiry Date:
                </td>
                <td>
                    <asp:Label ID="lblGPRSActivatedDT" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Recharge Period:
                </td>
                <td>
                    <asp:Label ID="lblRechargePeriod" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div style="width: 100%; height: 100px; text-align: center;">
            <div style="height: 50px;">
            </div>
            <asp:Button ID="btnEditVeh" runat="server" Text="Edit" OnClick="btnEditVeh_Click" />
            <asp:Button ID="btnClose" runat="server" Text="Home" OnClick="btnClose_Click" />
        </div>
    </div>
</asp:Content>

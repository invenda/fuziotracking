﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ESLmanager.Master" AutoEventWireup="true"
    CodeBehind="ManagerHome1.aspx.cs" Inherits="Tracking.MasterHome1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        // SCRIPT FOR THE MOUSE EVENT.
        function MouseEvents(objRef, evt) {
            if (evt.type == "mouseover") { objRef.style.cursor = 'pointer'; objRef.style.backgroundColor = "Skyblue"; }
            else { if (evt.type == "mouseout") objRef.style.backgroundColor = "#FBF6D9"; }
        }

        function openManagerGeo() {
            window.open("ManagerGeo.aspx");
            return false;
        }
    </script>
    <div style="height: 20px; text-align: center; font-style: italic; font-size: xx-large;
        color: Blue;">
    </div>
    <div style="height: 40px; text-align: center; font-style: italic; font-size: xx-large;
        color: Blue;">
        Manager Home Page
    </div>
    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="True">
    </asp:ScriptManager>
    <div style="width: 800px; margin: 0 auto; padding: 0; padding: 0; height:auto;
        font-size: large;">
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="False" Width="100%"
                        RowStyle-Font-Bold="true" RowStyle-Font-Size="Larger" HeaderStyle-BackColor="#0099ff"
                        HeaderStyle-ForeColor="Black" ForeColor="Black" BackColor="#FBF6D9" OnRowDataBound="gvrecords_RowDataBound"
                        ShowFooter="True" Font-Size="Small" Height="214px">
                        <AlternatingRowStyle BorderColor="#990099" />
                        <Columns>
                            <asp:TemplateField HeaderText="User Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbluser_name" CommandName="AddToCart" runat="server" Text='<%# Bind("User_Name") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Width="100px" />
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="User_Name" HeaderText="User Name" ControlStyle-Width ="120px" HeaderStyle-Width ="120px" />--%>
                            <%--<asp:BoundField DataField="Cx_Name" HeaderText="Company Name" ControlStyle-Width ="450px" HeaderStyle-Width ="450px"/>--%>
                            <asp:TemplateField HeaderText="Branch Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblcname" CommandName="AddToCart" runat="server" Text='<%# Bind("Cx_Name") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <HeaderStyle Width="100px" />
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal" runat="server" Text="Total Vehicles:" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Width="450px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No of Vehicles">
                                <ItemTemplate>
                                    <asp:Label ID="lbldno" CommandName="AddToCart" runat="server" Text='<%# Bind("No_of_veh") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal12" runat="server" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" />
                                <HeaderStyle Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Font-Bold="False" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Function">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btncustomer" runat="server" ImageUrl="Images/images.jpg" OnClick="btncustomer_Click"
                                        Height="20px" Width="75px" />
                                    <%-- <asp:Button ID="btncustomer" runat="server" BackColor="Orange" Height="25px" Width="75px"
                                        Text="Go" OnClick="btncustomer_Click" />--%>
                                    <%--  <asp:Button ID="btncustomer" runat="server" OnClick="btncustomer_Click" Text ="Go"  /> --%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Font-Bold="False" Width="100px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle Font-Size="Large" />
                        <FooterStyle BackColor="#3399FF" />
                        <HeaderStyle BackColor="#0099FF" ForeColor="Black"></HeaderStyle>
                        <RowStyle Font-Bold="True" Font-Size="Large" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="height:30px;">
            
            </tr>
        </table>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateRoute.aspx.cs" Inherits="Tracking.CreateRoute"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="ContentPlaceHolder1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 600px; height: 400px; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
        <div id="sublogo" style="text-align: center; color: Blue;">
            <h2>
                <a>Create Geo Route</a>
            </h2>
        </div>
        <div style="height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
          <div style ="text-align :center ; color: Red;" >
                <asp:Label ID="lblalert" runat="server"></asp:Label>
        
        </div>
        <div>
            <table style="width: 600px; height: auto;">
                <tr>
                    <td style="width: 150px;">
                        Select Route Type
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlrtype"
                            ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                            ValueToCompare="--">*</asp:CompareValidator>
                    </td>
                    <td style="width: 150px;">
                        <asp:DropDownList ID="ddlrtype" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlrtype_SelectedIndexChanged">
                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                            <asp:ListItem Text="TRIP BASIS" Value="0"></asp:ListItem>
                            <asp:ListItem Text="DAILY BASIS" Value="1"></asp:ListItem>
                            <asp:ListItem Text="DATE BASIS" Value="2"></asp:ListItem>
                            <asp:ListItem Text="DATE/TIME BASIS" Value="3"></asp:ListItem>
                             <asp:ListItem Text="ROUTE BASIS" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Route No:
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtrno"
                            ValidationGroup="Group1" ErrorMessage="Enter Route No">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtid" runat="server" Width="40px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtrno" runat="server" Width="95px" AutoPostBack="true" 
                                        ontextchanged="txtrno_TextChanged"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    <asp:Label ID="lblckeckro" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Start Destination:
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfname"
                            ValidationGroup="Group1" ErrorMessage="Enter Route Name">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtfname" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        End Destination:
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttname"
                            ValidationGroup="Group1" ErrorMessage="Enter Route Name">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txttname" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="Btn_submit" runat="server" Text="Submit" ValidationGroup="Group1"
                            OnClick="Btn_submit_Click" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Btn_clr" runat="server" Text="Clear" OnClick="Btn_clr_Click" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

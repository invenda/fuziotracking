﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditVehDetails.aspx.cs"
    Inherits="Tracking.EditVehDetails" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0; text-align: center;">
        <div id="Div1">
            <h1>
                Edit Vehicle Registration Details
            </h1>
        </div>
    </div>
    <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                </td>
            </tr>
            <tr>
                <td style="color: black; font-size: large">
                    Vehicle Details
                </td>
                <td>
                </td>
                <td style="color: black; font-size: large">
                    Driver Details
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Registered Name:
                </td>
                <td>
                    <asp:TextBox ID="txtVehRegName" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtVehRegName"
                        runat="server" ErrorMessage="Enter Vehicle Registered Name">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    Driver Name:
                </td>
                <td>
                    <asp:TextBox ID="txtDriverName" runat="server" Height="15px" Width="150px" 
                        MaxLength="13" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDriverName"
                        runat="server" ErrorMessage="Enter Driver Name">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Registration Number:
                </td>
                <td>
                    <asp:TextBox ID="txtVehicleNo" runat="server" Height="15px" Width="150px"></asp:TextBox>
                </td>
                <td>
                    Driver Mobile Number:
                </td>
                <td>
                    <asp:TextBox ID="txtDriverMobNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtDriverMobNum"
                        runat="server" ErrorMessage="Enter Driver Mobile Number">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDriverMobNum"
                        ErrorMessage="Please enter valid Mobile number!" ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Model:
                </td>
                <td>
                    <asp:TextBox ID="txtVehModel" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtVehModel"
                        runat="server" ErrorMessage="Enter Vehicle Model">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    Address:
                </td>
                <td>
                    <asp:TextBox ID="txtDriverADD" runat="server" Height="50px" Width="150px" TextMode="MultiLine"
                        MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtDriverADD"
                        runat="server" ErrorMessage="Enter Driver Address">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Make:
                </td>
                <td>
                    <asp:TextBox ID="txtVehMake" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtVehMake"
                        runat="server" ErrorMessage="Enter Vehicle Make">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    Driving Licence Number:
                </td>
                <td>
                    <asp:TextBox ID="txtDrivLicNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtDrivLicNum"
                        runat="server" ErrorMessage="Enter Driving Licence Number">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Insurance Number:
                </td>
                <td>
                    <asp:TextBox ID="txtVehInsNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtVehInsNum"
                        runat="server" ErrorMessage="Enter Vehicle Insurance Number">*</asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RTO Devision/Name:
                </td>
                <td>
                    <asp:TextBox ID="txtrtoname" runat="server" Height="15px" Width="150px"></asp:TextBox>
                </td>
                <td style="color: black; font-size: large">
                    Device Details
                </td>
            </tr>
            <tr>
                <td>
                    Existing Meter Reading:
                </td>
                <td>
                    <asp:TextBox ID="txtExistMetRead" runat="server" Height="15px" Width="150px" MaxLength="6"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="txtExistMetRead"
                        runat="server" ErrorMessage="Enter Existing Meter Reading">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtExistMetRead"
                        runat="server" ErrorMessage="Please enter valid Existing Meter Reading" ValidationExpression="[0-9]*\.?[0-9]*">*</asp:RegularExpressionValidator>
                </td>
                <td>
                    Serial Number:
                </td>
                <td>
                    <asp:TextBox ID="txtSNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="txtSNum"
                        runat="server" ErrorMessage="Enter Serial Number">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Kilometer Pulse:
                </td>
                <td>
                    <asp:TextBox ID="txtKiloPulse" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="txtKiloPulse"
                        runat="server" ErrorMessage="Enter Kilometer Pulse">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    Date of Purchase:
                </td>
                <td>
                    <asp:TextBox ID="txtDOP" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ControlToValidate="txtDOP"
                        runat="server" ErrorMessage="Enter Date of Purchase">*</asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDOP">
                    </cc1:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                Tank Capacity in Liters:
                    </td>
                <td>
                <asp:TextBox ID="txtTankCapacity" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtTankCapacity"
                        runat="server" ErrorMessage="Enter Tank Capacity">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtTankCapacity"
                        runat="server" ErrorMessage="Please enter valid liters." ValidationExpression="^(-)?\d+(\.\d\d)?$">*
                    </asp:RegularExpressionValidator>
                    
                    </td>
                <td>
                    Dealer Name:
                </td>
                <td>
                    <asp:TextBox ID="txtDealerName" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ControlToValidate="txtDealerName"
                        runat="server" ErrorMessage="Enter Dealer Name">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
            <td>
                    Battery Voltage Capacity:
                </td>
                <td>
                    <asp:RadioButton ID="rbtn12V" runat="server" Text="12V" Value="12" GroupName="Group1">
                    </asp:RadioButton>
                    <asp:RadioButton ID="rbtn24V" runat="server" Text="24V" Value="24" GroupName="Group1">
                    </asp:RadioButton>
                </td>
                
                    <td>
                        SMS Alert No:
                    </td>
                    <td>
                        <asp:TextBox ID="txtsmsno" runat="server" Height="15px" Width="150px"></asp:TextBox>
                        &nbsp;(Default)&nbsp;&nbsp;
                    </td>
            </tr>
            <tr>
                <td>
                Vehicle milage/liter
                    
                    &nbsp;</td>
                <td>
                     <asp:TextBox ID="txtmilage" runat="server"></asp:TextBox>
                    </td>
                <td>
                    <asp:Label ID="lblsmsno" runat="server" Text="SMS Alert New NO:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtmsmmno" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                        ControlToValidate="txtmsmmno" ErrorMessage="Please enter valid Mobile number!"
                        ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                Fuel Type
                    
                    </td>
                <td>
                   <asp:DropDownList ID="ddlpt" runat="server">
                        <asp:ListItem>Petrol</asp:ListItem>
                        <asp:ListItem>Diesel</asp:ListItem>
                        <asp:ListItem>LPG</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                <td>
                </td>
                <td>
                </td>
            </tr>          
           
            <tr>
                 <td style="width: 178px">
                    Vehicle Category
                </td>
                <td style="width: 332px">
                    <asp:DropDownList ID="ddlvc" runat="server">
                        <asp:ListItem Text="Truck & Vans" Value="Truck & Vans"></asp:ListItem>
                        <asp:ListItem Text="Rented/Hired Vehicle" Value="Rented/Hired Vehicle"></asp:ListItem>
                        <asp:ListItem Text="Public Passenger" Value="Public Passenger"></asp:ListItem>
                        <asp:ListItem Text="Tourist Vehicles & Taxis" Value="Tourist Vehicles & Taxis"></asp:ListItem>
                        <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                        <asp:ListItem Text="Three Wheeler" Value="Three Wheeler"></asp:ListItem>
                         <asp:ListItem Text="Reefer Vehicles" Value="Reefer Vehicles"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
             
             </tr>
              <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="color: black; font-size: large">
                    SIM Details
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Number:
                </td>
                <td>
                    <asp:TextBox ID="txtMobNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ControlToValidate="txtMobNum"
                        runat="server" ErrorMessage="Enter Mobile Number">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtMobNum"
                        ErrorMessage="Please enter valid Mobile number!" ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SIM Provider:
                </td>
                <td>
                    <asp:TextBox ID="txtSIMProvider" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ControlToValidate="txtSIMProvider"
                        runat="server" ErrorMessage="Enter SIM Provider">*</asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
             <td>
                   GPRS Expiry Date:
                </td>
              
                   <td style="width: 332px">
                    <asp:TextBox ID="txtGPRSActDt" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ControlToValidate="txtGPRSActDt"
                        runat="server" ErrorMessage="Enter GPRS Activated Date">*</asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtGPRSActDt">
                    </cc1:CalendarExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                        ControlToValidate="txtGPRSActDt" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$">*</asp:RegularExpressionValidator>
                        </td> 
             
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <%-- GPRS Expiry Date:--%>
                    Recharge Period:
                </td>
                <td>
                    <asp:RadioButton ID="rbtnMonthly" runat="server" Text="Monthly" GroupName="Group2">
                    </asp:RadioButton>
                    <asp:RadioButton ID="rbtnYearly" runat="server" Text="Yearly" GroupName="Group2">
                    </asp:RadioButton>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div style="height: 20px;">
        </div>
        <div style="text-align: center;">
            <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" />
        </div>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TowedReport.aspx.cs" Inherits="Tracking.TowedReport"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" AsyncPostBackErrorMessage ="TIMED OUT PLAESE TRY AGAIN">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="width: 100%; height: 50px; font-size: 25px; text-align: center; color: Blue;">
            Vehicle Towed Report
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label1" Text="Select the Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                        ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>&nbsp;:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                        DataTextField="Vehicalnumber" Height="23px" Width="188px">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Label ID="Label2" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                        ControlToValidate="txtdate" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                                    &nbsp;:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtdate" runat="server" Width="175px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtdate"
                                        OnClientDateSelectionChanged="checkDate">
                                    </cc1:CalendarExtender>
                                </td>
                                <td>
                                    <asp:Button ID="btnsubmit" runat="server" Text="Get Report" ValidationGroup="Group1"
                                        OnClick="btnsubmit_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btndownload" runat="server" Text="DownLoad" Visible="false" OnClick="btndownload_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnclear" runat="server" Text="Clear" Width="60px" OnClick="btnclear_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: auto;">
                        <div style="height: 20px; text-align :center">
                            <asp:Label ID="lblinfo" runat="server"  ForeColor ="Red" Font-Bold ="true"  ></asp:Label>
                        </div>
                        <div style="vertical-align: middle; text-align: center;">
                            <asp:Label ID="lblnoact" runat="server" Font-Bold="true" Font-Size="20px" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                            ShowFooter="true" Width="1000px" RowStyle-ForeColor="Black" OnRowCreated="gvreport_RowCreated"
                            OnRowDataBound="gvreport_RowDataBound">
                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px" />
                            <FooterStyle BackColor="#61A6F8" ForeColor="Black" Height="20px" Font-Size="14px"
                                BorderWidth="0" />
                            <Columns>
                                <asp:TemplateField HeaderText="Started " HeaderStyle-Width="75px" FooterStyle-BorderColor="#61A6F8"
                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblon" runat="server" Text='<%# Bind("Tstime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblmov" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Towe Start Location" HeaderStyle-Width="280px" FooterStyle-BorderColor="#61A6F8"
                                    ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldep" runat="server" Text='<%# Bind("TSLoc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltigson" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Towe End Location" HeaderStyle-Width="280px" FooterStyle-BorderColor="#61A6F8"
                                    FooterStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblar" runat="server" Text='<%# Bind("TELoc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltidl" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ended" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right"
                                    FooterStyle-BorderColor="#61A6F8" HeaderStyle-Width="75px" ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lbloff" runat="server" Text='<%# Bind("Tetime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblts" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Running Duration" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="80px" ItemStyle-Font-Size="13px" FooterStyle-BorderColor="#61A6F8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblduration" runat="server" Text='<%# Bind("duration") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltsv" runat="server" Text="Total Km"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Travel Distance" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="13px" FooterStyle-BorderColor="#61A6F8" HeaderStyle-Width="90px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblkm" runat="server" Text='<%# Bind("km") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltkm" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btndownload" />
                </Triggers>
            </asp:UpdatePanel>
            <div style="height: 20px;">
            </div>
        </div>
</asp:Content>

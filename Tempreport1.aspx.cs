﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace Tracking
{
    public partial class Tempreport1 : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        int rcount = 0;
        int rcount1 = 0;
        int rcount2 = 0;
        int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    ddlmonthbind();
                }
            }

        }
        public void ddlmonthbind()
        {
            DateTime dt = DateTime.Now;
            int m = Convert.ToInt32(dt.Month);
            if (m == 1)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));

            }
            else if (m == 2)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));

            }
            else if (m == 3)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));

            }
            else if (m == 4)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
            }
            else if (m == 5)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
            }
            else if (m == 6)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
            }
            else if (m == 7)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));

            }
            else if (m == 8)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));

            }
            else if (m == 9)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));

            }
            else if (m == 10)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));

            }
            else if (m == 11)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));

            }
            else if (m == 12)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));

            }
        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {

            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            Panel1.Visible = true;
            bindmonthtemp12();
            Download.Enabled = true;
            ddlMapTOVehicle.Enabled = false;
            ddlmonth.Enabled = false;
            GetRecord.Enabled = false;
        }
        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime, int Month)
        {
            return new DateTime(dateTime.Year, Month, 1);
        }
        public DateTime LastDayOfMontthFromDateTime(DateTime dateTime, int Month)
        {

            DateTime createDate = new DateTime(dateTime.Year, Month, DateTime.DaysInMonth(dateTime.Year, Month));
            return createDate;
        }
        public DataTable bindmonthtemp()
        {
            DataTable dt11 = new DataTable();
            dt11.Columns.Add("Date", typeof(string));
            dt11.Columns.Add("Temp", typeof(string));

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());

            DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
            DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
            DateTime fdt12 = LastDayOfMontthFromDateTime(dt, month);


            DataTable dt12 = db.fuzio_get_temp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.Month.ToString() + "/0" + fdt.Day.ToString() + "/" + fdt.Year.ToString(), fdt12.ToShortDateString());

            for (int i = 0; i < dt12.Rows.Count; i++)
            {
                DataRow dr = dt11.NewRow();
                dr["Date"] = dt12.Rows[i][2].ToString();
                dr["Temp"] = dt12.Rows[i][3].ToString();
                dt11.Rows.Add(dr);
            }
            return dt11;

        }
        public DataTable bindmonthtempold()
        {
            DataTable dt11 = new DataTable();
            dt11.Columns.Add("Date", typeof(string));
            dt11.Columns.Add("Temp", typeof(string));

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());

            DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "12:00 AM");
            DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
            DateTime fdt12 = LastDayOfMontthFromDateTime(dt, month);
            if (gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
            {
                int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                int maxtemp1 = maxtemp;
                double maxxtemp;
                double matemp;

                int Hours = 0;
                int days = 0;
                int Minutes = 0;
                for (int i = 0; i < fdt12.Day; i++)
                {
                    if ((fdt.Month <= month && fdt < dt))
                    {
                        DataRow dr = dt11.NewRow();
                        DateTime fdt1 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "12:00 AM");
                        DateTime fdt11 = Convert.ToDateTime(fdt.ToShortDateString() + " " + "11:59 PM");
                        if (maxtemp1 > 0)
                        {
                            DataTable dt3 = db.Temp_Noofalertold(fdt1.ToString(), fdt11.ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);

                            int m = Convert.ToInt32(dt3.Rows[0][0].ToString());

                            //Hours = m / 60;
                            // Minutes = m % 60;
                            dr["Temp"] = m.ToString();//Hours + "hrs ," + Minutes + "min";
                        }
                        else if (maxtemp1 < 0)
                        {
                            maxtemp = maxtemp1 * -1 + 1000;

                            DataTable dt3 = db.Temp_Noofalert1old(fdt1.ToString(), fdt11.ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                            int m = Convert.ToInt32(dt3.Rows[0][0].ToString());
                            //days = m / 1440;
                            //Hours = m / 60;
                            //Minutes = m % 60;
                            //dr["Temp"] = Hours + "hrs ," + Minutes + "min";
                            dr["Temp"] = m.ToString();
                        }

                        dr["Date"] = fdt1.ToShortDateString();
                        //dr["KM"] = "";
                        dt11.Rows.Add(dr);
                        fdt = fdt.AddDays(1);

                    }
                    else break;
                }
            }
            return dt11;

        }
        public void bindmonthtemp12()
        {
            DataTable log = bindmonthtemp();
            DataTable first = new DataTable();
            first.Columns.Add("Date", typeof(string));
            first.Columns.Add("Temp", typeof(string));
            int count = log.Rows.Count;
            int N = (int)(count / 3);
            int n = N;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = first.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    string[] words = log.Rows[i][1].ToString().Split(':');
                    string newhour = words[0].Remove(words[0].Length - 4, 4);
                    string newmin = words[1].Remove(words[1].Length - 3, 3);
                    rcount += Convert.ToInt32(newhour.ToString()) * 60 + Convert.ToInt32(newmin); 
                    dr["Temp"] = log.Rows[i][1].ToString();
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    first.Rows.Add(dr);
                }
            }
            gvreport.DataSource = first;
            gvreport.DataBind();
            n = N * 2;
            DataTable second = new DataTable();
            second.Columns.Add("Date", typeof(string));
            second.Columns.Add("Temp", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = second.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    string[] words = log.Rows[i][1].ToString().Split(':');
                    string newhour = words[0].Remove(words[0].Length - 4, 4);
                    string newmin = words[1].Remove(words[1].Length - 3, 3);
                    rcount1 += Convert.ToInt32(newhour.ToString()) * 60 + Convert.ToInt32(newmin);

                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["Temp"] = log.Rows[i][1].ToString();
                    second.Rows.Add(dr);
                }
            }
            gvreport1.DataSource = second;
            gvreport1.DataBind();
            n = N * 3;
            n = n + 1;
            DataTable third = new DataTable();
            third.Columns.Add("Date", typeof(string));
            third.Columns.Add("Temp", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = third.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    string[] words = log.Rows[i][1].ToString().Split(':');
                    string newhour = words[0].Remove(words[0].Length - 4, 4);
                    string newmin = words[1].Remove(words[1].Length - 3, 3);
                    rcount2 += Convert.ToInt32(newhour.ToString()) * 60 + Convert.ToInt32(newmin);

                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    dr["Temp"] = log.Rows[i][1].ToString();
                    third.Rows.Add(dr);
                }
            }
            gvreport2.DataSource = third;
            gvreport2.DataBind();
        }
        public void bindmonthtempold12()
        {
            DataTable log = bindmonthtempold();
            DataTable first = new DataTable();
            first.Columns.Add("Date", typeof(string));
            first.Columns.Add("Temp", typeof(string));
            int count = log.Rows.Count;
            Session["countlog"] = count;
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            int N = (int)(count / 3);
            int n = N;           
            int i = 0;
            for (i = 0; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = first.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    int ets = Convert.ToInt32(log.Rows[i][1].ToString());
                    rcount += ets;
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    Hours = ets / 60;
                    Minutes = ets % 60;
                    dr["Temp"] = Hours + "hrs :" + Minutes + "min";
                    // dr["Temp"] = ets.ToString();
                    first.Rows.Add(dr);
                }
               
            }
            gvreport.DataSource = first;
            gvreport.DataBind();
            n = N * 2;
            DataTable second = new DataTable();
            second.Columns.Add("Date", typeof(string));
            second.Columns.Add("Temp", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {

                    DataRow dr = second.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    // string ts = log.Rows[i][0].ToString();
                    //string ets = log.Rows[i][1].ToString();
                    int ets = Convert.ToInt32(log.Rows[i][1].ToString());
                    rcount1 += ets;
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    Hours = ets / 60;
                    Minutes = ets % 60;
                    dr["Temp"] = Hours + "hrs :" + Minutes + "min";
                    second.Rows.Add(dr);
                }
               
            }
            gvreport1.DataSource = second;
            gvreport1.DataBind();
            n = N * 3;
            n = n + 1;
            DataTable third = new DataTable();
            third.Columns.Add("Date", typeof(string));
            third.Columns.Add("Temp", typeof(string));
            for (i = i; i < n; i++)
            {
                if (i < count)
                {
                    DataRow dr = third.NewRow();
                    DateTime ts = Convert.ToDateTime(log.Rows[i][0].ToString());
                    //string ts = log.Rows[i][0].ToString();
                    // string ets = log.Rows[i][1].ToString();
                    int ets = Convert.ToInt32(log.Rows[i][1].ToString());
                    rcount2 += ets;
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    Hours = ets / 60;
                    Minutes = ets % 60;
                    dr["Temp"] = Hours + "hrs :" + Minutes + "min";
                    dr["Date"] = ts.ToString("dd/MM/yyyy ");
                    //dr["Temp"] = ets.ToString();
                    third.Rows.Add(dr);
                }
            }
            gvreport2.DataSource = third;
            gvreport2.DataBind();
        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dt1 = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                HeaderCell1.Text = "Report for the Month :" + ddlmonth.SelectedItem.Text + " " + DateTime.Now.Year;
                HeaderCell1.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell1.Font.Bold = true;
                // HeaderCell1.ForeColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);

            }
           
        }

        protected void gvreport1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dt1 = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Maximum Temperature :" + dt1.Rows[0][9].ToString() + "°C";
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport1.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                HeaderCell1.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell1.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell1.Font.Bold = true;
                // HeaderCell1.ForeColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                gvreport1.Controls[0].Controls.AddAt(1, HeaderGridRow1);


            }
         
        }

        protected void gvreport2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dt1 = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Minimum Temperature :" + dt1.Rows[0][13].ToString() + "°C";
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.Font.Bold = true;
                HeaderCell.BorderColor = System.Drawing.Color.Black;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                gvreport2.Controls[0].Controls.AddAt(0, HeaderGridRow);

                // int count = Convert.ToInt32(Session["countlog"].ToString());
                //int N = (int)(count % 3);
                //if (N == 0)
                //{
                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                HeaderCell1.Text = "............";//Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell1.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell1.Font.Bold = true;
                HeaderCell1.ForeColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                gvreport2.Controls[0].Controls.AddAt(1, HeaderGridRow1);
                // }

            }
        }


        protected void gvreport2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            if (e.Row.RowType == DataControlRowType.Header)
            {
                count = 0;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label;
                count = rcount + rcount1 + rcount2;
                // days = count / 1440;
                Hours = count / 60;
                Minutes = count % 60;
                ftotal.Text = Hours.ToString() + "  Hrs : " + Minutes.ToString() + "  Mins";
            }
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "" && ddlmonth.SelectedItem.Text != "--select Month--")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                string str1 = ddlmonth.SelectedItem.Text + dt.Year;// +"_" + fdt.Year;
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VKM_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);

                TableCell cell2 = new TableCell();
                cell2.Controls.Add(gvreport1);
                tr1.Cells.Add(cell2);

                TableCell cell3 = new TableCell();
                cell3.Controls.Add(gvreport2);
                tr1.Cells.Add(cell3);


                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;
namespace Tracking
{
    public partial class TemperatureGraph : System.Web.UI.Page
    {
        static string button_text = "";
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                
               // Chart1.Visible = false;
                if (Session["UserRole"].ToString() == "3")
                {

                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                    ddlMapTOVehicle.DataBind();
                    //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                    //ddlMapTOVehicle.DataBind();
                }
                else
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                    ddlMapTOVehicle.DataBind();
                }
            }
        }
        public void binddetatils()
        {
            lbltemp.Text = "";
            ddlMapTOVehicle.Enabled = false;
            date.Enabled = false;
            lblVehrtono.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
            lblreqtime.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
            DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
            DateTime hdt = Convert.ToDateTime(date.Text);
            lbltime.Text = "Report Date  :" + hdt.ToString("dd-MM-yyyy");
            lbldriname.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
            lbldrimob.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
            if (DriDet.Rows[0][9].ToString() != "")
            {
                if (DriDet.Rows[0][9].ToString() != "0" && DriDet.Rows[0][10].ToString() != "0")
                {
                    int max = Convert.ToInt32(DriDet.Rows[0][9].ToString());
                    int min = Convert.ToInt32(DriDet.Rows[0][10].ToString());
                    lbltemp.Text = "Alert Temperature :" + max.ToString() + " °C";
                    if (rbttype.SelectedValue.ToString() == "0")
                    {
                        Chart1.Series[1].Points.AddXY(0, max);
                        Chart1.Series[1].Points.AddXY(720, max);
                        Chart1.Series[2].Points.AddXY(0, 0);
                        Chart1.Series[2].Points.AddXY(720, 0);
                        Chart1.Series[5].Points.AddXY(0, min);
                        Chart1.Series[5].Points.AddXY(720, min);
                    }
                    else if (rbttype.SelectedValue.ToString() == "1")
                    {
                        Chart1.Series[1].Points.AddXY(0, max);
                        Chart1.Series[1].Points.AddXY(1440, max);
                        Chart1.Series[2].Points.AddXY(0, 0);
                        Chart1.Series[2].Points.AddXY(1440, 0);
                        Chart1.Series[5].Points.AddXY(0, min);
                        Chart1.Series[5].Points.AddXY(1440, min);
                    }
                }
            }
        }
        protected void GetRecord_Click(object sender, EventArgs e)
        {
            
            bindchartdata();
            Panel1.Visible = true;
            //Chart1.Visible = true;
            
            

        }
        public void bindchartdata()
        {
            if (rbttype.SelectedValue.ToString() == "0")
            {
                if (rbttype.SelectedValue.ToString() == "0" && button_text == "")
                {
                    Chart1.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                    Chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    Chart1.ChartAreas[0].AxisX.Minimum = 0;
                    Chart1.ChartAreas[0].AxisX.Maximum = 720;
                    Chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
                    Chart1.ChartAreas[0].AxisX.Interval = 60;
                    Chart1.ChartAreas[0].AxisX2.CustomLabels.Clear();
                    Chart1.ChartAreas[0].AxisX2.Enabled = AxisEnabled.False;                   
                    binddetatils();
                    double k;
                    int ss;
                   
                    DataTable acstat = db.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
                    if (acstat.Rows[0][1].ToString() == "On")
                    {
                        Chart1.Series["Series7"].IsVisibleInLegend = true;
                        Chart1.Series["Series8"].IsVisibleInLegend = true;
                        Chart1.Series["Series7"].LegendText = "AC ON";
                        Chart1.Series["Series8"].LegendText = "AC OFF";
                        Chart1.Series["Series7"].MarkerSize = 30;
                        Chart1.Series["Series8"].MarkerSize = 30;
                        string acon = acontime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                        string acoff = acofftime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                        Chart1.Titles[3].Text = "AC ON for   " + acon;// +"Ac OFF for   " + acoff;
                        Chart1.Titles[2].Text = "AC OFF for  " + acoff;
                    
                    }
                    DataTable myReader = binddata(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 AM", date.Text + " 12:00 PM");
                    for (int i = 0; i < myReader.Rows.Count; i++)
                    {
                        Chart1.Series[0].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(myReader.Rows[i][1].ToString()));
                        if (acstat.Rows[0][1].ToString() == "On")
                        {
                            k = Convert.ToDouble(myReader.Rows[i][2].ToString());

                            if (k == 0.0)
                            {
                                Chart1.Series["Series7"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                            }
                            else
                            {
                                Chart1.Series["Series8"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                            }
                        }
                            ss = Convert.ToInt32(myReader.Rows[i][3].ToString());

                            if (ss < 3)
                            {
                                Chart1.Series["Series9"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                            }
                            else
                            {
                                Chart1.Series["Series10"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                            }
                        

                    }
                    Chart1.Titles[1].Text = "Time of the Day in Hrs(12 AM To 12 Noon)";
                    //Chart1.ChartAreas[0].AxisX.Title = "Hours (12 AM To 12 Noon)";
                    //Chart1.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 8);
                    linknext.Visible = true;
                }
                else if (rbttype.SelectedValue.ToString() == "0" && button_text == "Next")
                {
                    Chart1.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                    Chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    Chart1.ChartAreas[0].AxisX.Minimum = 0;
                    Chart1.ChartAreas[0].AxisX.Maximum = 720;
                    Chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
                    Chart1.ChartAreas[0].AxisX.Interval = 60;
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Clear();
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(0, 20, "12");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(50, 70, "13");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(110, 130, "14");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(170, 190, "15");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(230, 250, "16");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(290, 310, "17");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(350, 370, "18");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(410, 430, "19");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(470, 490, "20");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(530, 550, "21");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(590, 610, "22");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(650, 670, "23");
                    Chart1.ChartAreas[0].AxisX.CustomLabels.Add(700, 730, "24");
                    Chart1.ChartAreas[0].AxisX2.CustomLabels.Clear();
                    Chart1.ChartAreas[0].AxisX2.Enabled = AxisEnabled.False;                   
                    binddetatils();
                    double k;
                    int ss;
                    DataTable acstat = db.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
                    if (acstat.Rows[0][1].ToString() == "On")
                    {
                        Chart1.Series["Series7"].IsVisibleInLegend = true;
                        Chart1.Series["Series8"].IsVisibleInLegend = true;
                        Chart1.Series["Series7"].LegendText = "AC ON";
                        Chart1.Series["Series8"].LegendText = "AC OFF";
                       
                        string acon = acontime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                        string acoff = acofftime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                        Chart1.Titles[3].Text = "AC ON for   " + acon;// +"Ac OFF for   " + acoff;
                        Chart1.Titles[2].Text = "AC OFF for  " + acoff;
                    
                    }
                   
                   
                    DataTable myReader = binddata(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 PM", date.Text + " 11:59 PM");
                    for (int i = 0; i < myReader.Rows.Count; i++)
                    {
                        Chart1.Series[0].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(myReader.Rows[i][1].ToString()));
                        if (acstat.Rows[0][1].ToString() == "On")
                        {
                            k = Convert.ToDouble(myReader.Rows[i][2].ToString());

                            if (k == 0.0)
                            {
                                Chart1.Series["Series7"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                            }
                            else
                            {
                                Chart1.Series["Series8"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                            }
                        }
                            ss = Convert.ToInt32(myReader.Rows[i][3].ToString());

                            if (ss < 3)
                            {
                                Chart1.Series["Series9"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                            }
                            else
                            {
                                Chart1.Series["Series10"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                            }
                       
                    }
                    Chart1.Titles[1].Text = "Time of the Day in Hrs(12 Noon To 12 AM)";
                    //Chart1.ChartAreas[0].AxisX.Title = "Hours (12 Noon To 12 AM)";
                    //Chart1.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 8);

                }
            }
            else if (rbttype.SelectedValue.ToString() == "1")
            {
                Chart1.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                Chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                Chart1.ChartAreas[0].AxisX.Minimum = 0;
                Chart1.ChartAreas[0].AxisX.Maximum = 1440;
                Chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
                Chart1.ChartAreas[0].AxisX.Interval = 60;
                Chart1.ChartAreas[0].AxisX2.Enabled = AxisEnabled.True;
                Chart1.ChartAreas[0].AxisX2.Minimum = 0;
                Chart1.ChartAreas[0].AxisX2.Maximum = 1440;
                Chart1.ChartAreas[0].AxisX2.IsStartedFromZero = true;
                Chart1.ChartAreas[0].AxisX2.IntervalAutoMode = IntervalAutoMode.FixedCount;
                Chart1.ChartAreas[0].AxisX2.Interval = 60;
                Chart1.ChartAreas[0].AxisX2.IsMarksNextToAxis = true;
                Chart1.ChartAreas[0].AxisX2.IsLabelAutoFit = false;
                Chart1.ChartAreas[0].AxisX2.CustomLabels.Add(0, 360, "Night");
                Chart1.ChartAreas[0].AxisX2.CustomLabels.Add(360, 1080, "Day");
                Chart1.ChartAreas[0].AxisX2.CustomLabels.Add(1080, 1440, "Night");
               
                binddetatils();
                int v;
                double k;
                int ss;
                DataTable acstat = db.Getacveh(ddlMapTOVehicle.SelectedValue.ToString());
                if (acstat.Rows[0][1].ToString() == "On")
                {
                    Chart1.Series["Series7"].IsVisibleInLegend = true;
                    Chart1.Series["Series8"].IsVisibleInLegend = true;
                    Chart1.Series["Series7"].LegendText = "AC ON";
                    Chart1.Series["Series8"].LegendText = "AC OFF";
                    string acon = acontime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                   
                    string acoff = acofftime(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
                    Chart1.Titles[3].Text ="AC ON for   " + acon;// +"Ac OFF for   " + acoff;
                    Chart1.Titles[2].Text ="AC OFF for  " + acoff;
                    

                }
               // Chart1.Legends["Default"].LegendStyle = LegendStyle.Table;
                DataTable myReader = binddata(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 AM", date.Text + " 11:59 PM");
                for (int i = 0; i < myReader.Rows.Count; i++)
                {

                    v = Convert.ToInt32(myReader.Rows[i][0].ToString());
                    if (v >= 0 && v <= 360)
                    {
                        Chart1.Series["Series1"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(myReader.Rows[i][1].ToString()));
                    }
                    else if (v > 360 && v <= 1080)
                    {
                        Chart1.Series["Series4"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(myReader.Rows[i][1].ToString()));
                    }
                    else if (v > 1080)
                    {
                        Chart1.Series["Series5"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(myReader.Rows[i][1].ToString()));
                    }

                    if (acstat.Rows[0][1].ToString() == "On")
                    {
                        k = Convert.ToDouble(myReader.Rows[i][2].ToString());
                       
                        if (k == 0.0)
                        {
                            Chart1.Series["Series7"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                        }
                        else
                        {
                            Chart1.Series["Series8"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(-1));
                        }
                    }

                  
                        ss = Convert.ToInt32 (myReader.Rows[i][3].ToString());

                        if (ss <3)
                        {
                            Chart1.Series["Series9"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                        }
                        else
                        {
                            Chart1.Series["Series10"].Points.AddXY(Convert.ToDouble(myReader.Rows[i][0].ToString()), Convert.ToDouble(1));
                        }
                    


                }
              
              
                Chart1.Titles[1].Text = "Time of the Day in Hrs (24 Hours)";
              
                //Chart1.Titles[2].Text = "Starting fuel in day ";// + fuelwe.ToString() + "";
                //Chart1.ChartAreas[0].AxisX.Title = "24 Hours";
                button_text = "";
                linknext.Visible = false;
                LinkButton1.Visible = false;
            }

        }
        public DataTable binddata(string Vno, string fdate, string tdate)
        {
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("Hours", typeof(string));
            dttemp.Columns.Add("sno", typeof(string));
            dttemp.Columns.Add("Temp", typeof(string));
            dttemp.Columns.Add("Ac", typeof(string));
            dttemp.Columns.Add("Speed", typeof(string));

             DateTime dhdh = Convert.ToDateTime(fdate);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                DataTable dt = db.Select_Temp(Vno, fdate, tdate);

                if (fre.Rows[0][1].ToString() == "Yes")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();
                        TimeSpan ts = Convert.ToDateTime(dt.Rows[i][0].ToString()) - Convert.ToDateTime(fdate);
                        dr["sno"] = ts.TotalMinutes.ToString();
                        //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {
                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                                //lbltemp.Text = " - " + temp.ToString() + " °C";
                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        else if (conditon1 < 1000)
                        {
                            temp = conditon1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        dttemp.Rows.Add(dr);
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();
                        TimeSpan ts = Convert.ToDateTime(dt.Rows[i][0].ToString()) - Convert.ToDateTime(fdate);
                        dr["sno"] = ts.TotalMinutes.ToString();
                        //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {
                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                                //lbltemp.Text = " - " + temp.ToString() + " °C";
                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        else if (conditon1 < 1000)
                        {
                            temp = conditon1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        dttemp.Rows.Add(dr);
                    }
                }
            }
            else
            {
                DataTable dt = db.Select_Tempold(Vno, fdate, tdate);

                if (fre.Rows[0][1].ToString() == "Yes")
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();
                        TimeSpan ts = Convert.ToDateTime(dt.Rows[i][0].ToString()) - Convert.ToDateTime(fdate);
                        dr["sno"] = ts.TotalMinutes.ToString();
                        //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {
                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                                //lbltemp.Text = " - " + temp.ToString() + " °C";
                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        else if (conditon1 < 1000)
                        {
                            temp = conditon1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        dttemp.Rows.Add(dr);
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dttemp.NewRow();
                        TimeSpan ts = Convert.ToDateTime(dt.Rows[i][0].ToString()) - Convert.ToDateTime(fdate);
                        dr["sno"] = ts.TotalMinutes.ToString();
                        //dr["Hours"] = Convert.ToDateTime(dt.Rows[i][0].ToString()).ToShortTimeString();
                        double conditon1 = Convert.ToDouble(dt.Rows[i][1].ToString());
                        double temp = 0;
                        if (conditon1 >= 10000)
                        {
                            double cond1 = conditon1 - 10000;
                            if (cond1 > 1000)
                            {
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = "-" + temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                                //lbltemp.Text = " - " + temp.ToString() + " °C";
                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                dr["Temp"] = temp.ToString();
                                dr["Ac"] = dt.Rows[i][2].ToString();
                                dr["Speed"] = dt.Rows[i][3].ToString();
                            }
                        }
                        else if (conditon1 >= 1000)
                        {
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = "-" + temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        else if (conditon1 < 1000)
                        {
                            temp = conditon1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            dr["Temp"] = temp.ToString();
                            dr["Ac"] = dt.Rows[i][2].ToString();
                            dr["Speed"] = dt.Rows[i][3].ToString();
                        }
                        dttemp.Rows.Add(dr);
                    }
                }

            }
            return dttemp;
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Chart1.Series[0].Points.Clear();
            Response.Redirect(Request.RawUrl);
        }

        protected void linknext_Click(object sender, EventArgs e)
        {
            button_text = "Next";
            bindchartdata();
            LinkButton1.Visible = true;
            linknext.Visible = false;

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            button_text = "";
            bindchartdata();
            LinkButton1.Visible = false;
            linknext.Visible = true;

        }

        protected void Download_Click(object sender, EventArgs e)
        {
            DateTime dt = Convert.ToDateTime(date.Text);
            string str = dt.ToString("dd-MM-yyyy");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            //str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VTC_" + vstr.Replace(" ", "") + "_" + str1 + ".pdf";
            bindchartdata();
            MemoryStream m = new MemoryStream();
            Document document = new Document(PageSize.A3.Rotate(), 60, 0, 30, 10);
            try
            {
                System.IO.StringWriter sw = new System.IO.StringWriter();
                XhtmlTextWriter hw = new XhtmlTextWriter(sw);
                //tblToExport.RenderControl(hw);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                PdfWriter.GetInstance(document, m);
                document.Open();
                PdfPTable table = new PdfPTable(3);
                PdfPCell cell = new PdfPCell(new Phrase(lblhead.Text, new Font(Font.TIMES_ROMAN, 24f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 0;
                PdfPCell cell0 = new PdfPCell(new Phrase("  "));
                cell0.Colspan = 3;
                cell0.BorderWidth = 0;
                PdfPCell cell1 = new PdfPCell(new Phrase(lblVehrtono.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell1.BorderWidth = 0;
                PdfPCell cell2 = new PdfPCell(new Phrase(lblreqtime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell2.BorderWidth = 0;
                PdfPCell cell3 = new PdfPCell(new Phrase(lbltemp.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell3.BorderWidth = 0;
                PdfPCell cell4 = new PdfPCell(new Phrase(lbltime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell4.BorderWidth = 0;
                PdfPCell cell5 = new PdfPCell(new Phrase(lbldriname.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell5.BorderWidth = 0;
                PdfPCell cell6 = new PdfPCell(new Phrase(lbldrimob.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell6.BorderWidth = 0;
                table.AddCell(cell);
                table.AddCell(cell0);
                table.AddCell(cell1);
                table.AddCell(cell2);
                table.AddCell(cell3);
                table.AddCell(cell4);
                table.AddCell(cell5);
                table.AddCell(cell6);
                document.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    Chart1.SaveImage(stream, ChartImageFormat.Png);
                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                    chartImage.ScalePercent(75f);
                    document.Add(chartImage);
                }

                document.Add(new Paragraph(sw.ToString()));
            }
            catch (DocumentException ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
                Console.Error.WriteLine(ex.Message);
            }
            document.Close();
            Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
        }
        public string acontime(string Vno,string date)
        {
 
 
            DBClass db = new DBClass();
 
 
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string actime = "";
            string stime = date;
            string stime1 = stime + " 12:00 AM";
            string ttime = stime + " 11:59 PM";
           
            DataTable acstat = db.Getacveh(Vno);
            DateTime dhdh = Convert.ToDateTime(stime);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {

                DataTable account = db.getacontime1(Vno, stime1, ttime);

                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (account.Rows.Count != 0)
                    {
                        int m = Convert.ToInt32(account.Rows[0][0].ToString());
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        actime = Hours.ToString() + "  Hr(s)  " + Minutes.ToString() + "  Mins";

                    }
                    else
                    {
                        actime = "0 Hours";
                    }
                }
                else
                {
                    actime = "Service Not Availed";
                }
            }
            else
            {
                DataTable account = db.getacontime1old(Vno, stime1, ttime);

                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (account.Rows.Count != 0)
                    {
                        int m = Convert.ToInt32(account.Rows[0][0].ToString());
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        actime = Hours.ToString() + "  Hr(s)  " + Minutes.ToString() + "  Mins";

                    }
                    else
                    {
                        actime = "0 Hours";
                    }
                }
                else
                {
                    actime = "Service Not Availed";
                }

            }
            return actime;

        }
        public string acofftime(string Vno, string date)
        {
 
 
            DBClass db = new DBClass();
 
 
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string actime = "";
            string stime = date;
            string stime1 = stime + " 12:00 AM";
            string ttime = stime + " 11:59 PM";

            DataTable acstat = db.Getacveh(Vno);
            DateTime dhdh = Convert.ToDateTime(stime);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {

                DataTable account = db.getacofftime1(Vno, stime1, ttime);

                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (account.Rows.Count != 0)
                    {
                        int m = Convert.ToInt32(account.Rows[0][0].ToString());
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        actime = Hours.ToString() + "  Hr(s)  " + Minutes.ToString() + "  Mins";

                    }
                    else
                    {
                        actime = "0 Hours";
                    }
                }
                else
                {
                    actime = "Service Not Availed";
                }
            }
            else
            {
                DataTable account = db.getacofftime1old(Vno, stime1, ttime);

                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (account.Rows.Count != 0)
                    {
                        int m = Convert.ToInt32(account.Rows[0][0].ToString());
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        actime = Hours.ToString() + "  Hr(s)  " + Minutes.ToString() + "  Mins";

                    }
                    else
                    {
                        actime = "0 Hours";
                    }
                }
                else
                {
                    actime = "Service Not Availed";
                }

            }
            return actime;

        }    


    }
}

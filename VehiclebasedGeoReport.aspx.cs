﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.IO;
using System.Collections.Generic;
namespace Tracking
{
    public partial class VehiclebasedGeoReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            if (!IsPostBack)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    gvrecords.DataSource = db.Get_AssingedRoute_Vehicles(Session["UserID"].ToString(), CUsers);
                    gvrecords.DataBind();
                }
                else
                {
                    gvrecords.DataSource = db.GetAssingedRouteVehicles(Session["UserID"].ToString());
                    gvrecords.DataBind();
                }
            }
        }
        protected void btMap_Click(object sender, EventArgs e)
        {
            binddata1();
            ddlrtype.Enabled = false;
            txtdate.Enabled = false;
        }

        public List<GLatLng> points(double Cx, double Cy, double radius)
        {
            double d2r = Math.PI / 180;   // degrees to radians
            double r2d = 180 / Math.PI;   // radians to degrees
            double earthsradius = 3963; // 3963 is the radius of the earth in miles
            double points = 30;
            double rlat = ((double)radius / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(Cx * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = Cy + (rlng * Math.Cos(theta));
                double ey = Cx + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            return extp;
        }
        private static bool IsPointInPolygon(List<GLatLng> poly, double Lt, double Lg)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].lat <= Lt) && (Lt < poly[j].lat)) |
                    ((poly[j].lat <= Lt) && (Lt < poly[i].lat))) &&
                    (Lg < (poly[j].lng - poly[i].lng) * (Lt - poly[i].lat) / (poly[j].lat - poly[i].lat) + poly[i].lng))
                    c = !c;
            }
            return c;
        }
        public void binddata1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Sno", typeof(string));
            dt.Columns.Add("Vno", typeof(string));
            dt.Columns.Add("RouteNo", typeof(string));
            int j = 1;
            bool valid = false;
            foreach (GridViewRow row in gvrecords.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rbt = row.FindControl("chkBxSelect") as CheckBox;
                    if (rbt.Checked == true)
                    {
                        Label lblvno = row.FindControl("lblSvno") as Label;
                        string Vno = lblvno.Text;
                        string userid = db.Get_UserID(Vno);
                        valid = true;
                        if (ddlrtype.SelectedValue.ToString() == "0" || ddlrtype.SelectedValue.ToString() == "1")
                        {
                            DataTable dt2 = db.get_Routeno_BetweenDate(Vno, userid, txtdate.Text);
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                DataRow dr = dt.NewRow();
                                DataTable routeno = db.get_Routeno_Routetyp(Vno, userid);
                                if (routeno.Rows.Count != 0)
                                {
                                    dr["Sno"] = j.ToString();
                                    dr["Vno"] = Vno;
                                    dr["RouteNo"] = dt2.Rows[i][0].ToString();
                                    j++;
                                    dt.Rows.Add(dr);
                                    txtdate.Enabled = false;
                                }
                                else
                                {
                                    lblnorec.Text = "No record found";
                                }
                                     
                            }
                        }
                        else if (ddlrtype.SelectedValue.ToString() == "2" || ddlrtype.SelectedValue.ToString() == "3" || ddlrtype.SelectedValue.ToString() == "4")
                        {
                            DataRow dr = dt.NewRow();
                            DataTable routeno = db.get_Routeno_Routetyp(Vno, userid);
                            string routetype = "";
                            string rno = "";
                            if (routeno.Rows.Count != 0)
                            {
                                rno = routeno.Rows[0][0].ToString();
                                routetype = routeno.Rows[0][1].ToString();
                                dr["Sno"] = j.ToString();
                                dr["Vno"] = Vno;
                                dr["RouteNo"] = rno;
                                j++;
                                dt.Rows.Add(dr);
                                valid = true;
                                txtdate.Enabled = false;
                            }

                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "Alert", "No Records found", true);
                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(),"Alert","No Records found",true);
                        }
                    }
                }
            }
            if (!valid && gvrecords.Rows.Count != 0)
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please select atleast one Vehicle');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gv = (GridView)e.Row.FindControl("gvchild");
                Label lvno = (Label)e.Row.FindControl("lblVno");
                Label lrno = (Label)e.Row.FindControl("lblRno");
                Label lblnt = (Label)e.Row.FindControl("lbnt");
                string Vno = lvno.Text;
                string Rno = lrno.Text;
                string userid = db.Get_UserID(Vno);
                DataTable dt = Getdata(Vno, Rno, userid);
                string r = Rno.Substring(0, 3);
                string Notrip = "";
                if (r == "TRB")
                {
                    if (dt.Rows.Count == 2)
                    {
                        int x = Convert.ToInt32(dt.Rows[0][4]);
                        int y = Convert.ToInt32(dt.Rows[1][4]);
                        Notrip = Math.Min(x, y).ToString();
                    }
                }
                lblnt.Text = Notrip;
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
        public DataTable Getdata(string Vno, string Rno, string uid)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Sno", typeof(string));
            dt.Columns.Add("PointName", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("OTime", typeof(string));
            dt.Columns.Add("notimes", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            DataTable dtnrec = db.Noof_Points(Rno, uid);
            for (int i = 0, j = 1; i < dtnrec.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr["Sno"] = j.ToString();
                dr["PointName"] = dtnrec.Rows[i][5].ToString();
                string v = "Not Valid";
                string rtype = dtnrec.Rows[i][6].ToString();
                DateTime dtp = DateTime.Now;
                //DateTime pdt = Convert.ToDateTime(txtdate.Text);
                List<GLatLng> ex;
                List<GLatLng> ex1;
                string fdate = DateTime .Now.ToString();
                string tdate = DateTime.Now.ToString();
                string[,] startpoint5 = new string[20, 2];
                if (rtype == "0" && dtnrec.Rows.Count == 2 && txtdate.Text != "")
                {
                    fdate = txtdate.Text + " " + "12:00 AM";
                    tdate = txtdate.Text + " " + "11:59 PM";
                    ex = points(Convert.ToDouble(dtnrec.Rows[0][0]), Convert.ToDouble(dtnrec.Rows[0][1]), Convert.ToDouble(dtnrec.Rows[0][2]) / 1609.3);
                    ex1 = points(Convert.ToDouble(dtnrec.Rows[1][0]), Convert.ToDouble(dtnrec.Rows[1][1]), Convert.ToDouble(dtnrec.Rows[1][2]) / 1609.3);
                    startpoint5 = CheckTime1(Vno, ex, ex1, fdate, tdate, i);

                }
                else if (rtype == "1" && txtdate.Text != "")
                {
                    fdate = txtdate.Text + " " + "12:00 AM";
                    tdate = txtdate.Text + " " + "11:59 PM";
                    ex = points(Convert.ToDouble(dtnrec.Rows[i][0]), Convert.ToDouble(dtnrec.Rows[i][1]), Convert.ToDouble(dtnrec.Rows[i][2]) / 1609.3);
                    startpoint5 = CheckTime2(Vno, ex, fdate, tdate);
                }
                else if (rtype == "2")
                {
                    ex = points(Convert.ToDouble(dtnrec.Rows[i][0]), Convert.ToDouble(dtnrec.Rows[i][1]), Convert.ToDouble(dtnrec.Rows[i][2]) / 1609.3);
                    fdate = dtnrec.Rows[i][3].ToString();
                    tdate = dtnrec.Rows[i][4].ToString();
                    startpoint5 = CheckTime2(Vno, ex, fdate, tdate);
                }
                else if (rtype == "3")
                {
                    ex = points(Convert.ToDouble(dtnrec.Rows[i][0]), Convert.ToDouble(dtnrec.Rows[i][1]), Convert.ToDouble(dtnrec.Rows[i][2]) / 1609.3);
                    fdate = dtnrec.Rows[i][3].ToString();
                    tdate = dtnrec.Rows[i][4].ToString();
                    startpoint5 = checktime3(Vno, ex, fdate, tdate);
                }                
                string pintime = "";
                string pouttime = "";
                int nocount = 0;
                for (int n = 0; n <= startpoint5.GetUpperBound(0); n++)
                {
                    if (startpoint5[n, 0] == null)
                    {
                        break;
                    }
                    else
                    {
                        string t = startpoint5[n, 0];
                        pintime += startpoint5[n, 0] + "    ";
                        for (int x = 1; x <= startpoint5.GetUpperBound(1); x++)
                        {
                            if (startpoint5[n, x] != null)
                            {
                                pouttime += startpoint5[n, x] + "    ";
                            }
                            else if (startpoint5[n, x] == null)
                            {
                                if (Convert.ToDateTime(tdate) < DateTime.Now)
                                {
                                    pouttime += "11:59 PM" + "    ";
                                }
                                else pouttime += "Expected" + "    ";

                            }

                        }
                        v = "Valid";
                        nocount++;
                    }
                }
                if (v == "Valid")
                {
                    db.Update_Status_GeoRoute(dtnrec.Rows[i][8].ToString(), uid, dtnrec.Rows[i][7].ToString());

                }
                else
                {
                    if (Convert.ToDateTime(tdate) > DateTime.Now)
                    {
                        v = "Geo Fenced";
                    }
                }
                dr["Time"] = pintime;
                dr["OTime"] = pouttime;
                dr["notimes"] = nocount.ToString();
                dr["Status"] = v;
                j++;
                dt.Rows.Add(dr);
            }
            return dt;

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable View = db.ViewExactCust(Session["UserID"].ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                string strdate = "";
                if (txtdate.Text != "")
                {
                    strdate = txtdate.Text;
                }
                else
                {
                    strdate = DateTime.Now.ToShortDateString();
                }
                DateTime dt = Convert.ToDateTime(strdate);
                HeaderCell.Text = "Daily Trip Report";
                HeaderCell.ColumnSpan = 5;
                HeaderCell.Font.Bold = true;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Date : " + dt.ToString("dd-MM-yyyy").ToString(); //;
                HeaderCell.ColumnSpan = 4;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell);
                TableCell HeaderCell3 = new TableCell();
                HeaderCell3.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Name of the Customer/Company :" + View.Rows[0]["Cx_Name"].ToString();
                HeaderCell3.ColumnSpan = 1;
                HeaderCell3.Font.Bold = true;
                HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell3);
                GridView1.Controls[0].Controls.AddAt(1, HeaderGridRow1);
            }
        }
        protected void Btn_Clear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        public string[,] CheckTime2(string Vno, List<GLatLng> ex, string fd, string td)
        {
            int svalid = 0, sv = 0, pinr = 0, val1 = 0;
            DataTable dtfirst = db.First_TrackString(Vno, fd, td);
            DataTable dtlast = db.Last_TrackString(Vno, fd, td);
            DataTable dt2 = db.GET_DAY_DATA(Vno, fd, td);
            if (dtfirst.Rows.Count != 0)
            {
                DataRow firstRow = dt2.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = dtfirst.Rows[0][0].ToString();
                firstRow["LGMI"] = dtfirst.Rows[0][1].ToString();
                firstRow["SPED"] = dtfirst.Rows[0][2].ToString();
                firstRow["GTIM"] = dtfirst.Rows[0][3].ToString();
                firstRow["IGST"] = dtfirst.Rows[0][4].ToString();
                dt2.Rows.InsertAt(firstRow, 0);

            }
            if (dtlast.Rows.Count != 0)
            {
                DataRow firstRow = dt2.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = dtlast.Rows[0][0].ToString();
                firstRow["LGMI"] = dtlast.Rows[0][1].ToString();
                firstRow["SPED"] = dtlast.Rows[0][2].ToString();
                firstRow["GTIM"] = dtlast.Rows[0][3].ToString();
                firstRow["IGST"] = dtlast.Rows[0][4].ToString();
                dt2.Rows.Add(firstRow);

            }
            string[,] startpoint = new string[15, 2];
            string fdate1 = "";
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                if (valid == true)
                {
                    fdate1 = Convert.ToString(dt2.Rows[j][3]);
                    DateTime dtime = Convert.ToDateTime(fdate1);
                    if (val1 == 0)
                    {
                        startpoint.SetValue(dtime.ToShortTimeString(), pinr, 0);
                        val1++; sv = 0;
                    }
                    else if (j == dt2.Rows.Count)
                    {
                        startpoint.SetValue(dtime.ToShortTimeString(), pinr, 1);
                        val1++; sv = 0;
                    }
                }
                else if (fdate1 != "")
                {
                    DateTime dtime = Convert.ToDateTime(fdate1);
                    if (sv == 0 && val1 != 0)
                    {
                        startpoint.SetValue(dtime.ToShortTimeString(), pinr, 1);
                        val1 = 0; pinr++; sv++;
                    }
                    svalid = svalid + 1;
                }

            }
            return startpoint;

        }
        public string[,] CheckTime1(string Vno, List<GLatLng> ex, List<GLatLng> ex1, string fd, string td, int n)
        {
            DataTable dtfirst = db.First_TrackString(Vno, fd, td);
            DataTable dtlast = db.Last_TrackString(Vno, fd, td);
            DataTable dt2 = db.GET_DAY_DATA(Vno, fd, td);
            if (dtfirst.Rows.Count != 0)
            {
                DataRow firstRow = dt2.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = dtfirst.Rows[0][0].ToString();
                firstRow["LGMI"] = dtfirst.Rows[0][1].ToString();
                firstRow["SPED"] = dtfirst.Rows[0][2].ToString();
                firstRow["GTIM"] = dtfirst.Rows[0][3].ToString();
                firstRow["IGST"] = dtfirst.Rows[0][4].ToString();
                dt2.Rows.InsertAt(firstRow, 0);

            }
            if (dtlast.Rows.Count != 0)
            {
                DataRow firstRow = dt2.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = dtlast.Rows[0][0].ToString();
                firstRow["LGMI"] = dtlast.Rows[0][1].ToString();
                firstRow["SPED"] = dtlast.Rows[0][2].ToString();
                firstRow["GTIM"] = dtlast.Rows[0][3].ToString();
                firstRow["IGST"] = dtlast.Rows[0][4].ToString();
                dt2.Rows.Add(firstRow);

            }
            string[,] startpoint = new string[15, 2];
            string fdate = fd;
            string tdate = td;
            int svalid = 0, evalid = 0, nrc = 0, fpnc = 0, spnc = 0;
            string fdate1 = "";
            string fdate2 = "";
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                if (valid == true)
                {
                    fdate1 = Convert.ToString(dt2.Rows[j][3]);
                    spnc = 0;
                    if (n == 0 && fpnc == 0 && (fdate2 == "" || Convert.ToDateTime(fdate1) > Convert.ToDateTime(fdate2)))
                    {
                        startpoint.SetValue(Convert.ToDateTime(fdate1).ToShortTimeString(), nrc, 0);
                        fpnc++;
                    }
                    fdate = fdate1;
                }
                else
                {
                    if (fdate1 != "")
                    {
                        if (fdate2 == "" || Convert.ToDateTime(fdate1) > Convert.ToDateTime(fdate2))
                        {
                            DateTime dtime = Convert.ToDateTime(fdate1);
                            if (n == 0 && fpnc == 1)
                            {
                                startpoint.SetValue(dtime.ToShortTimeString(), nrc, 1);
                                nrc++;
                            }
                            svalid = svalid + 1;
                            DataTable dt3 = db.GET_DAY_DATA(Vno, fdate, tdate);
                            fdate1 = "";
                            for (int k = 0; k < dt3.Rows.Count; k++)
                            {
                                bool val = IsPointInPolygon(ex1, Convert.ToDouble(dt3.Rows[k][0]), Convert.ToDouble(dt3.Rows[k][1]));
                                if (val == true)
                                {
                                    fdate1 = Convert.ToString(dt3.Rows[k][3]);
                                    fpnc = 0;
                                    if (n == 1 && spnc == 0)
                                    {
                                        startpoint.SetValue(Convert.ToDateTime(fdate1).ToShortTimeString(), nrc, 0);
                                        spnc++;
                                    }
                                    fdate = fdate1;
                                }
                                else
                                {
                                    if (fdate1 != "")
                                    {
                                        DateTime dtime1 = Convert.ToDateTime(fdate1);
                                        evalid = evalid + 1;
                                        if (n == 1 && spnc == 1)
                                        {
                                            startpoint.SetValue(dtime1.ToShortTimeString(), nrc, 1);
                                            nrc++; spnc = 0;
                                        }
                                        fdate2 = fdate1;
                                        fdate1 = "";
                                        break;
                                    }
                                }
                            }
                            if (fdate1 != "")
                            {
                                DateTime dtime1 = Convert.ToDateTime(fdate1);
                                evalid = evalid + 1;
                                if (n == 1 && spnc == 1)
                                {
                                    startpoint.SetValue(dtime1.ToShortTimeString(), nrc, 1);
                                    nrc++; spnc = 0;
                                }
                                fdate2 = fdate1;
                                fdate1 = "";
                            }
                        }
                    }
                }
            }
            return startpoint;
        }
        public string[,] checktime3(string Vno, List<GLatLng> ex, string fd, string td)
        {
            DataTable dt2 = db.search(Session["UserID"].ToString(), fd, td, Vno);
            string[,] startpoint = new string[15, 2];
            int firstvalid = 0, pinr = 0, lastvalid = 1;
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                if (valid == true && firstvalid == 0)
                {
                    startpoint.SetValue(Convert.ToDateTime(dt2.Rows[j][2]).ToShortTimeString(), pinr, 0);
                    lastvalid = 0; firstvalid++;
                }
                else if (valid == false && lastvalid == 0)
                {
                    startpoint.SetValue(Convert.ToDateTime(dt2.Rows[j][2]).ToShortTimeString(), pinr, 1);
                    firstvalid = 0; lastvalid++; pinr++;
                }
            }
            if (startpoint[0, 0] == null && firstvalid == 0 && lastvalid == 1)
            {
                startpoint = checkMidpoint(Vno, ex, fd, td);
            }
            return startpoint;
        }
        public string[,] checkMidpoint(string Vno, List<GLatLng> ex, string fd, string td)
        {
            DataTable dt = db.Select_All_Moving(Vno, fd, td);
            string[,] startpoint = new string[15, 2];
            int pinr = 0;
            for (int m = 0, n = 1; m < dt.Rows.Count - 1; m++, n++)
            {
                double mlt = (Convert.ToDouble(dt.Rows[m][0]) + Convert.ToDouble(dt.Rows[n][0])) / 2;
                double mlg = (Convert.ToDouble(dt.Rows[m][1]) + Convert.ToDouble(dt.Rows[n][1])) / 2;
                bool valid = IsPointInPolygon(ex, mlt, mlg);
                if (valid == true)
                {
                    startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                    startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                    break;
                }
                else
                {
                    double lflt1 = (Convert.ToDouble(dt.Rows[m][0]) + mlt) / 2;
                    double lflg1 = (Convert.ToDouble(dt.Rows[m][1]) + mlg) / 2;
                    bool valid7 = IsPointInPolygon(ex, lflt1, lflg1);
                    if (valid7 == true)
                    {
                        startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                        startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                        break;
                    }
                    else
                    {
                        double rlt1 = (Convert.ToDouble(dt.Rows[n][0]) + mlt) / 2;
                        double rlg1 = (Convert.ToDouble(dt.Rows[n][1]) + mlg) / 2;
                        bool valid2 = IsPointInPolygon(ex, rlt1, rlg1);
                        if (valid2 == true)
                        {
                            startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                            startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                            break;
                        }
                        else
                        {
                            double lflt2 = (Convert.ToDouble(dt.Rows[m][0]) + lflt1) / 2;
                            double lflg2 = (Convert.ToDouble(dt.Rows[m][1]) + lflg1) / 2;
                            bool valid3 = IsPointInPolygon(ex, lflt2, lflg2);
                            if (valid3 == true)
                            {
                                startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                                startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                                break;
                            }
                            else
                            {
                                double lflt3 = (mlt + lflt1) / 2;
                                double lflg3 = (mlg + lflg1) / 2;
                                bool valid4 = IsPointInPolygon(ex, lflt3, lflg3);
                                if (valid4 == true)
                                {
                                    startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                                    startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                                    break;
                                }
                                else
                                {
                                    double rlt2 = (rlt1 + mlt) / 2;
                                    double rlg2 = (rlg1 + mlg) / 2;
                                    bool valid5 = IsPointInPolygon(ex, rlt2, rlg2);
                                    if (valid5 == true)
                                    {
                                        startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                                        startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                                        break;
                                    }
                                    else
                                    {
                                        double rlt3 = (rlt1 + Convert.ToDouble(dt.Rows[n][0])) / 2;
                                        double rlg3 = (rlg1 + Convert.ToDouble(dt.Rows[n][1])) / 2;
                                        bool valid6 = IsPointInPolygon(ex, rlt3, rlg3);
                                        if (valid6 == true)
                                        {
                                            startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 0);
                                            startpoint.SetValue(Convert.ToDateTime(dt.Rows[m][2]).ToShortTimeString(), pinr, 1);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return startpoint;
        }
        protected void gvchild_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow gvMasterRow = (GridViewRow)e.Row.Parent.Parent.Parent.Parent;
                Label lblrno = ((Label)e.Row.Parent.Parent.Parent.FindControl("lblRno"));
                Label lbluid = ((Label)e.Row.Parent.Parent.Parent.FindControl("lblVno"));
                string userid = db.Get_UserID(lbluid.Text);
                DataTable dt = db.get_Geo_Route(lblrno.Text, userid);
                if (dt.Rows.Count != 0)
                {
                    e.Row.Cells[1].Text = "Route Name :" + dt.Rows[0][0].ToString();
                }
            }
            else if ((e.Row.RowType == DataControlRowType.DataRow))
            {
                Label lblstatus = (Label)e.Row.FindControl("lblstatus");
                if (lblstatus.Text == "Not Valid")
                {
                    e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void gvrecords_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                CheckBox chkBxSelect = (CheckBox)e.Row.Cells[0].FindControl("chkBxSelect");
                CheckBox chkBxHeader = (CheckBox)this.gvrecords.HeaderRow.FindControl("chkBxHeader");
                chkBxSelect.Attributes["onclick"] = string.Format("javascript:ChildClick(this,'{0}');", chkBxHeader.ClientID);
                if (ddlrtype.SelectedValue.ToString() != "--")
                {
                    chkBxHeader.Enabled = true;
                    chkBxSelect.Enabled = true;
                }
                else
                {
                    chkBxHeader.Enabled = false;
                    chkBxSelect.Enabled = false;
                }
            }

        }

        protected void ddlrtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrtype.SelectedValue.ToString() != "--")
            {
                if (ddlrtype.SelectedValue.ToString() == "0" || ddlrtype.SelectedValue.ToString() == "1")
                {
                    Label4.Visible = true;
                    txtdate.Visible = true;
                }
                else
                {
                    DataTable dt = new DataTable();
                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        dt = db.GetAssingedTripVehicles12(Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString(), CUsers);
                    }
                    else
                    {
                        dt = db.GetAssingedTripVehicles13(Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString());
                    }
                    gvrecords.DataSource = dt;
                    gvrecords.DataBind();
                    if (dt.Rows.Count == 0)
                    {
                        lblnorec.Text = "Route Type Not Assigned To Any Vehicle";
                    }
                    else
                    {
                        lblnorec.Text = "";
                        Label4.Visible = false;
                        txtdate.Visible = false;
                    }
                }
            }
        }

        protected void txtdate_TextChanged(object sender, EventArgs e)
        {
            if (ddlrtype.SelectedValue.ToString() == "0" || ddlrtype.SelectedValue.ToString() == "1" || ddlrtype.SelectedValue.ToString() == "4" && txtdate.Text != "")
            {
                // DataTable dt = db.GetAssingedTripVehicles(Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString(), txtdate.Text);
                DataTable dt = new DataTable();
                if (Session["UserRole"].ToString() == "3")
                {
                    DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    dt = db.GetAssingedTripVehicles1(Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString(), txtdate.Text, CUsers);
                }
                else
                {
                    dt = db.GetAssingedTripVehicles(Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString(), txtdate.Text);
                }
                gvrecords.DataSource = dt;
                gvrecords.DataBind();
                if (dt.Rows.Count == 0)
                {
                    lblnorec.Text = "Route Type Not Assigned To Any Vehicle";
                }
                else lblnorec.Text = "";
            }


        }

        protected void Btn_Download_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dt = DateTime.Now;
                if (txtdate.Text != "")
                {
                    dt = Convert.ToDateTime(txtdate.Text);
                }
                string str = dt.ToString("yyyy-MM-dd");
                string str1 = str.Replace("-", "");
                str1 = str1.Replace(" ", "");
                str1 = str1.Substring(2);
                string str2 = "DTR" + "_" + str1 + ".doc";
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-word";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                this.EnableViewState = false;
                System.IO.StringWriter sw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                GridView1.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
    }
}

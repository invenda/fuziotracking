﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Globalization;


namespace Tracking
{
    public partial class OverSpeedreport : System.Web.UI.Page
    {
        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";

        DBClass db = new DBClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    ddlmonthbind();
                }
            }
        }
        public void ddlmonthbind()
        {
            DateTime dt = DateTime.Now;
            int m = Convert.ToInt32(dt.Month);
            if (m == 1)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //  ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));

            }
            else if (m == 2)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));

            }
            else if (m == 3)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));

            }
            else if (m == 4)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
            }
            else if (m == 5)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
            }
            else if (m == 6)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
            }
            else if (m == 7)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                //ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));

            }
            else if (m == 8)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));

            }
            else if (m == 9)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));

            }
            else if (m == 10)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));

            }
            else if (m == 11)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));

            }
            else if (m == 12)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
                // ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));

            }
        }

        protected void rbtdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtdate.SelectedValue.ToString() == "1")
            {
                ddlmonth.Visible = true;
                //ddlyear.Visible = true;
                txtdate.Visible = false;
            }
            else
            {
                txtdate.Visible = true;
                ddlmonth.Visible = false;
                //ddlyear.Visible = false;
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            DataTable dt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());

            if (rbtdate.SelectedValue.ToString() == "0" && dt.Rows[0][3].ToString() != "0")
            {
                DateTime dhdh = Convert.ToDateTime(txtdate.Text);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    // DataTable dtdevice = db.getdevicesid(ddlMapTOVehicle.SelectedValue.ToString());
                    string overspeeddate = Convert.ToString(txtdate.Text);

                    int sp1 = Convert.ToInt32(dt.Rows[0][3].ToString());
                    sp1 = Convert.ToInt32(sp1 / 1.85);
                    //var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };

                    //DateTime span = Convert.ToDateTime(overspeeddate, dtfi);
                    //overspeeddate = span.ToShortDateString();
                    string overspeed = sp1.ToString();
                    //string overspeed = dt.Rows[0][3].ToString();
                    string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                    string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                    DataTable dtoverspeed = db.getalertsforspeed(ddlMapTOVehicle.SelectedValue.ToString(), overspeeddate1, overspeeddate2, overspeed);
                    DataTable dt1 = new DataTable();
                    dt1.Columns.Add("date", typeof(string));
                    dt1.Columns.Add("Location", typeof(string));
                    dt1.Columns.Add("time", typeof(string));
                    dt1.Columns.Add("speed", typeof(string));

                    for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                    {
                        DataRow dr = dt1.NewRow();
                        string date = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortDateString();

                        /* string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                         Subgurim.Controles.GeoCode objAddress = new Subgurim.Controles.GeoCode();
                         objAddress = Subgurim.Controles.GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dtoverspeed.Rows[i][0]), Convert.ToDouble(dtoverspeed.Rows[i][1])), sMapKey);

                         StringBuilder sb = new StringBuilder();
                         string address = "";
                         if (objAddress.valid)
                         {
                             sb.Append(objAddress.Placemark.address.ToString());
                             address = sb.ToString();
                         }*/

                        string address = "";
                        string latitude = dtoverspeed.Rows[i][0].ToString();
                        string longitude = dtoverspeed.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            address = formatted_address;
                        }

                        string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortTimeString();
                        int sp = Convert.ToInt32(dtoverspeed.Rows[i][2]);
                        sp = Convert.ToInt32(sp * 1.85);

                        dr["date"] = date;
                        dr["Location"] = address;
                        dr["time"] = ovsdate;
                        dr["speed"] = sp.ToString();
                        dt1.Rows.Add(dr);
                        btnprint.Visible = true;
                    }

                    if (dtoverspeed.Rows.Count == 0)
                    {
                        //lblvehno.Visible = true;
                        lbldata.Text = "Data Not Found";
                    }
                    gvoverspeed.DataSource = dt1;
                    gvoverspeed.DataBind();

                    //lbldate.Text = overspeeddate;
                }
                else
                {
                    // DataTable dtdevice = db.getdevicesid(ddlMapTOVehicle.SelectedValue.ToString());
                    string overspeeddate = Convert.ToString(txtdate.Text);

                    int sp1 = Convert.ToInt32(dt.Rows[0][3].ToString());
                    sp1 = Convert.ToInt32(sp1 / 1.85);
                    //var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };

                    //DateTime span = Convert.ToDateTime(overspeeddate, dtfi);
                    //overspeeddate = span.ToShortDateString();
                    string overspeed = sp1.ToString();
                    //string overspeed = dt.Rows[0][3].ToString();
                    string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                    string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                    DataTable dtoverspeed = db.getalertsforspeedold(ddlMapTOVehicle.SelectedValue.ToString(), overspeeddate1, overspeeddate2, overspeed);
                    DataTable dt1 = new DataTable();
                    dt1.Columns.Add("date", typeof(string));
                    dt1.Columns.Add("Location", typeof(string));
                    dt1.Columns.Add("time", typeof(string));
                    dt1.Columns.Add("speed", typeof(string));

                    for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                    {
                        DataRow dr = dt1.NewRow();
                        string date = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortDateString();

                        /* string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                         Subgurim.Controles.GeoCode objAddress = new Subgurim.Controles.GeoCode();
                         objAddress = Subgurim.Controles.GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dtoverspeed.Rows[i][0]), Convert.ToDouble(dtoverspeed.Rows[i][1])), sMapKey);

                         StringBuilder sb = new StringBuilder();
                         string address = "";
                         if (objAddress.valid)
                         {
                             sb.Append(objAddress.Placemark.address.ToString());
                             address = sb.ToString();
                         }*/
                        string address = "";
                        string latitude = dtoverspeed.Rows[i][0].ToString();
                        string longitude = dtoverspeed.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            address = formatted_address;
                        }

                        string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortTimeString();
                        int sp = Convert.ToInt32(dtoverspeed.Rows[i][2]);
                        sp = Convert.ToInt32(sp * 1.85);

                        dr["date"] = date;
                        dr["Location"] = address;
                        dr["time"] = ovsdate;
                        dr["speed"] = sp.ToString();
                        dt1.Rows.Add(dr);
                        btnprint.Visible = true;
                    }

                    if (dtoverspeed.Rows.Count == 0)
                    {
                        //lblvehno.Visible = true;
                        lbldata.Text = "Data Not Found";
                    }
                    gvoverspeed.DataSource = dt1;
                    gvoverspeed.DataBind();

                }
            }
            if (rbtdate.SelectedValue.ToString() == "1" && dt.Rows[0][3].ToString() != "0")
            {
                int m = Convert.ToInt32(ddlmonth.SelectedValue);
                // DateTime dhdh = Convert.ToDateTime(fdate);
                //int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (m == cmonth)
                {
                    DateTime datet = DateTime.Now;
                    int y = Convert.ToInt32(datet.Year);
                    //int y = Convert.ToInt32(ddlyear.SelectedValue);
                    string lastday = Convert.ToString(new DateTime(y, m, DateTime.DaysInMonth(y, m)).ToShortDateString());
                    string firstday = Convert.ToString(new DateTime(y, m, 01).ToShortDateString());

                    int sp1 = Convert.ToInt32(dt.Rows[0][3].ToString());
                    sp1 = Convert.ToInt32(sp1 / 1.85);
                    //var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };

                    //DateTime span = Convert.ToDateTime(overspeeddate, dtfi);
                    //overspeeddate = span.ToShortDateString();
                    string overspeed = sp1.ToString();
                    //string overspeed = dt.Rows[0][3].ToString();
                    string overspeeddate1 = firstday + " " + "12:00 AM";
                    string overspeeddate2 = lastday + " " + "11:59 PM";
                    DataTable dtoverspeed = db.getalertsforspeed(ddlMapTOVehicle.SelectedValue.ToString(), overspeeddate1, overspeeddate2, overspeed);
                    DataTable dt1 = new DataTable();
                    dt1.Columns.Add("date", typeof(string));
                    dt1.Columns.Add("Location", typeof(string));
                    dt1.Columns.Add("time", typeof(string));
                    dt1.Columns.Add("speed", typeof(string));
                    for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                    {
                        DataRow dr = dt1.NewRow();
                        string date = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortDateString();

                        /*string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                        Subgurim.Controles.GeoCode objAddress = new Subgurim.Controles.GeoCode();
                        objAddress = Subgurim.Controles.GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dtoverspeed.Rows[i][0]), Convert.ToDouble(dtoverspeed.Rows[i][1])), sMapKey);

                        StringBuilder sb = new StringBuilder();
                        string address = "";
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            address = sb.ToString();
                        }*/

                        string address = "";
                        string latitude = dtoverspeed.Rows[i][0].ToString();
                        string longitude = dtoverspeed.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            address = formatted_address;
                        }
                        if (address == "")
                        {
                            address = "No Location Found";

                        }
                        string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortTimeString();
                        int sp = Convert.ToInt32(dtoverspeed.Rows[i][2]);
                        sp = Convert.ToInt32(sp * 1.85);

                        dr["date"] = date;
                        dr["Location"] = address;
                        dr["time"] = ovsdate;
                        dr["speed"] = sp.ToString();
                        dt1.Rows.Add(dr);
                        btnprint.Visible = true;
                    }
                    if (dtoverspeed.Rows.Count == 0)
                    {
                        //lblvehno.Visible = true;
                        lbldata.Text = "Data Not Found";
                    }


                    gvoverspeed.Columns[0].Visible = true;
                    gvoverspeed.DataSource = dt1;
                    gvoverspeed.DataBind();
                }
                else
                {
                    DateTime datet = DateTime.Now;
                    int y = Convert.ToInt32(datet.Year);
                    //int y = Convert.ToInt32(ddlyear.SelectedValue);
                    string lastday = Convert.ToString(new DateTime(y, m, DateTime.DaysInMonth(y, m)).ToShortDateString());
                    string firstday = Convert.ToString(new DateTime(y, m, 01).ToShortDateString());

                    int sp1 = Convert.ToInt32(dt.Rows[0][3].ToString());
                    sp1 = Convert.ToInt32(sp1 / 1.85);
                    //var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };

                    //DateTime span = Convert.ToDateTime(overspeeddate, dtfi);
                    //overspeeddate = span.ToShortDateString();
                    string overspeed = sp1.ToString();
                    //string overspeed = dt.Rows[0][3].ToString();
                    string overspeeddate1 = firstday + " " + "12:00 AM";
                    string overspeeddate2 = lastday + " " + "11:59 PM";
                    DataTable dtoverspeed = db.getalertsforspeedold(ddlMapTOVehicle.SelectedValue.ToString(), overspeeddate1, overspeeddate2, overspeed);
                    DataTable dt1 = new DataTable();
                    dt1.Columns.Add("date", typeof(string));
                    dt1.Columns.Add("Location", typeof(string));
                    dt1.Columns.Add("time", typeof(string));
                    dt1.Columns.Add("speed", typeof(string));
                    for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                    {
                        DataRow dr = dt1.NewRow();
                        string date = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortDateString();

                        /*string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                        Subgurim.Controles.GeoCode objAddress = new Subgurim.Controles.GeoCode();
                        objAddress = Subgurim.Controles.GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dtoverspeed.Rows[i][0]), Convert.ToDouble(dtoverspeed.Rows[i][1])), sMapKey);

                        StringBuilder sb = new StringBuilder();
                        string address = "";
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            address = sb.ToString();
                        }*/
                        string address = "";
                        string latitude = dtoverspeed.Rows[i][0].ToString();
                        string longitude = dtoverspeed.Rows[i][1].ToString();
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            address = formatted_address;
                        }
                        if (address == "")
                        {
                            address = "No Location Found";

                        }
                        string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][4]).ToShortTimeString();
                        int sp = Convert.ToInt32(dtoverspeed.Rows[i][2]);
                        sp = Convert.ToInt32(sp * 1.85);

                        dr["date"] = date;
                        dr["Location"] = address;
                        dr["time"] = ovsdate;
                        dr["speed"] = sp.ToString();
                        dt1.Rows.Add(dr);
                        btnprint.Visible = true;
                    }
                    if (dtoverspeed.Rows.Count == 0)
                    {
                        //lblvehno.Visible = true;
                        lbldata.Text = "Data Not Found";
                    }


                    gvoverspeed.Columns[0].Visible = true;
                    gvoverspeed.DataSource = dt1;
                    gvoverspeed.DataBind();

                }
            }

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void btnprint_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=OverSpeed.doc");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/doc";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            Printdiv.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }

        protected void gvoverspeed_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable dt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                if (rbtdate.SelectedValue.ToString() == "0")
                {
                    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                    HeaderCell.ColumnSpan = 1;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Over Speed Limit KM/H " + dt.Rows[0][3].ToString();
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    gvoverspeed.Controls[0].Controls.AddAt(0, HeaderGridRow);

                    HeaderGridRow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Date 	:" + txtdate.Text;
                    HeaderCell.ColumnSpan = 3;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    gvoverspeed.Controls[0].Controls.AddAt(1, HeaderGridRow);
                }
                else if (rbtdate.SelectedValue.ToString() == "1")
                {

                    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Over Speed Limit KM/H " + dt.Rows[0][3].ToString();
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.ColumnSpan = 2;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    gvoverspeed.Controls[0].Controls.AddAt(0, HeaderGridRow);

                    HeaderGridRow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    HeaderCell = new TableCell();
                    HeaderCell.Text = "Date 	:" + ddlmonth.SelectedItem.ToString();
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderCell.ColumnSpan = 4;
                    HeaderCell.Font.Bold = true;
                    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                    HeaderGridRow.Cells.Add(HeaderCell);
                    gvoverspeed.Controls[0].Controls.AddAt(1, HeaderGridRow);
                }
            }
        }
    }

}

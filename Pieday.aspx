<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pieday.aspx.cs" Inherits="Tracking.Pieday"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="chart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report4" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }    
  
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 930px; background-color: White;">
        <div style="width: 100%; height: 40px; font-size: 25px; text-align: center; color: Blue;">
            <asp:Label ID="lblhead" Text="Vehicle Day summary pie Graphs" runat="server"></asp:Label></div>
        <div style="width: 100%; height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 200px;">
                                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 150px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 100px;">
                                <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>:
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                                    ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 130px;">
                                <asp:TextBox ID="date" runat="server" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="width: 100px;" align="right">
                                :
                            </td>
                            <td style="width: 150px;">
                                &nbsp;
                            </td>
                            <td style="width: 100px;">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" OnClick="GetRecord_Click"
                                    ValidationGroup="Group1" />
                            </td>
                            <td style="width: 120px;" align="center">
                                <asp:Button ID="Download" Text="Download" runat="server" Width="70px" OnClick="Download_Click"
                                    ValidationGroup="Group1" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: 30px;">
                    </div>
                    <div>
                        <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="false">
                            <div style="text-align: center;">
                                <table style="width: 100%; height: 50px;">
                                    <tr>
                                        <td style="width: 50px; height: 18px;">
                                        </td>
                                        <td style="height: 18px">
                                            <asp:Label ID="lblVehrtono" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td style="height: 18px">
                                            <asp:Label ID="lblreqtime" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td style="height: 18px">
                                            <asp:Label ID="lbltime" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td style="width: 50px; height: 18px;">
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr align="right">
                                        <td style="width: 200px">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="Statrionary " BackColor="Red" ForeColor="Red"
                                                Width="127px"></asp:Label>
                                        </td>
                                        <td style="width: 143px">
                                            <asp:Label ID="Label5" runat="server" Text="Stationary in hours"></asp:Label>
                                        </td>
                                        <td style="width: 80pt">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" BackColor="#006600" ForeColor="#006600" Text="Moving"
                                                Width="135px"></asp:Label>
                                        </td>
                                        <td style="width: 129px">
                                            <asp:Label ID="Label7" runat="server" Text="Moving in hours"></asp:Label>
                                        </td>
                                        <td style="width: 102px">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label8" runat="server" BackColor="#0033CC" ForeColor="Blue" Text="Ideal in hours"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text="Ideal in hours"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                            </div>
                            <div style="text-align: center;">
                                <asp:Chart ID="Chart1" runat="server" Height="600px" BorderlineDashStyle="Solid"
                                    BorderlineColor="CadetBlue" Width="1200px" BorderlineWidth="7">
                                    <BorderSkin SkinStyle="FrameThin1" />
                                    <Series>
                                        <chart:Series Name="Chartseries" ChartType="Pie" ChartArea="ChartArea1" IsValueShownAsLabel="True">
                                        </chart:Series>
                                    </Series>
                                    <ChartAreas>
                                        <chart:ChartArea Name="ChartArea1">
                                            <Area3DStyle Enable3D="True"></Area3DStyle>
                                        </chart:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                    </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div style="height: 30px;">
    </div>
</asp:Content>

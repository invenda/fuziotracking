﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;


namespace Tracking
{
    public partial class Fuel : System.Web.UI.Page
    {

        public double fuelstat = 0;

 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Session["UserID"] != null)
                {
                    Session["ChartNo"] = 1;

                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }

        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {

            DataTable fre = db.Getfuelimp(Session["Username"].ToString());
            if (fre.Rows[0][1].ToString() == "Yes")
            {

                bindchartdata();
                Panel1.Visible = true;
                Download.Enabled = true;
            }
            else
            {
                string message = "alert('Service Not Availed.... ')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                //string alertstring = " Service Not Availed";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
        }
        private DataTable fuelliter()
        {
            string RegNUM = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dttemp = new DataTable();
            dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            string gDate = date.Text;
            DataTable FuFTo = db.Fuel_fill2(RegNUM, gDate);
            int fliter = 0;
            if (FuFTo.Rows.Count != 0)
            {

                for (int i = 0; i < FuFTo.Rows.Count; i++)
                {
                    fliter += Convert.ToInt32(FuFTo.Rows[i][2]);
                }
                int n = 0;
                for (int m = 1; m < 1440; m++)
                {
                    DataRow dr = dttemp.NewRow();
                    n = n++;
                    dr["sno"] = n.ToString();
                    dr["fuel"] = fliter.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
        public void bindchartdata()
        {

            //fuel tank capacyty and litters filling
            string RegNUM = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dtf = db.GetVehiclesfuel(RegNUM, Session["UserId"].ToString());
            if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
            {
                int tank = Convert.ToInt32(dtf.Rows[0][0]);
                Chart2.ChartAreas[0].AxisY.Minimum = 0;
                Chart2.ChartAreas[0].AxisY.Maximum = tank + 5;
                Chart2.ChartAreas[0].AxisY.Interval = 10;

                double fuelwe = 0.0;

                DateTime dhdh = Convert.ToDateTime(date.Text);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    DataTable fuelp = fuelliter12();
                    if (fuelp.Rows.Count > 0)
                    {
                        fuelwe = Convert.ToDouble(fuelp.Rows[0][0].ToString());
                    }
                    
                }
                else
                {
                    DataTable fuelp = fuelliter12old();
                    if (fuelp.Rows.Count > 0)
                    {
                        fuelwe = Convert.ToDouble(fuelp.Rows[0][0].ToString());
                    }
                }

                DataTable fuel3 = fuelliter();
                for (int i = 0; i < fuel3.Rows.Count; i++)
                {
                    Chart2.Series["Fueltank"].Points.AddXY(Convert.ToDouble(fuel3.Rows[i][0].ToString()), Convert.ToDouble(fuel3.Rows[i][1].ToString()));
                }

                Chart2.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                Chart2.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                Chart2.ChartAreas[0].AxisX.Minimum = 0;
                Chart2.ChartAreas[0].AxisX.Maximum = 1440;
                Chart2.ChartAreas[0].AxisX.IsStartedFromZero = true;
                Chart2.ChartAreas[0].AxisX.Interval = 180;
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(-20, 40, "0");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(160, 200, "3");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(340, 380, "6");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(520, 560, "9");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(880, 920, "15");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(1060, 1100, "18");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(1240, 1280, "21");
                Chart2.ChartAreas[0].AxisX.CustomLabels.Add(1420, 1460, "24");
                Chart2.ChartAreas[0].AxisX2.CustomLabels.Clear();
                Chart2.Series["Fuel"].BorderWidth = 3;
                Chart2.Series["Fueltank"].BorderWidth = 3;
                Chart2.Series["Fuel"].ToolTip = " Litres:#VALY";
                DataTable myReader = loaddata();
                DataTable sss = RemoveDuplicateRows(myReader, "sno");
                // Create DataView and sorting            
                sss.DefaultView.Sort = "sno ASC";
                DataTable dtm = sss.DefaultView.ToTable();
                for (int i = 0; i < dtm.Rows.Count; i++)
                {
                    Chart2.Series["Fuel"].Points.AddXY(Convert.ToDouble(dtm.Rows[i][0].ToString()), Convert.ToDouble(dtm.Rows[i][1].ToString()));
                }
                Chart2.Titles[1].Text = "Time of the Day in Hrs(24 Hours)";
                Chart2.Titles[2].Text = "Starting fuel in day " + fuelwe.ToString() + "";

                //linknext.Visible = true;

                //kilometer graph
                Chart1.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                Chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                Chart1.ChartAreas[0].AxisX.Minimum = 0;
                Chart1.ChartAreas[0].AxisX.Maximum = 1440;
                Chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
                Chart1.ChartAreas[0].AxisX.Interval = 180;
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(-20, 40, "0");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(160, 200, "3");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(340, 380, "6");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(520, 560, "9");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(880, 920, "15");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1060, 1100, "18");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1240, 1280, "21");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1420, 1460, "24");
                Chart1.ChartAreas[0].AxisX2.CustomLabels.Clear();
                Chart1.Series["kilometers"].BorderWidth = 5;
                Chart1.Series["kilometers"].ToolTip = " Kilometers:#VALY";
                DataTable myReade = LoadChartData1();
                DataTable ssss = RemoveDuplicateRows(myReade, "sno");
                // Create DataView and sorting            
                ssss.DefaultView.Sort = "sno ASC";
                DataTable dtmm = ssss.DefaultView.ToTable();
                for (int i = 0; i < dtmm.Rows.Count; i++)
                {
                    Chart1.Series["kilometers"].Points.AddXY(Convert.ToDouble(dtmm.Rows[i][0].ToString()), Convert.ToDouble(dtmm.Rows[i][1].ToString()));
                }
                Chart1.Titles[1].Text = "Time of the Day in Hrs(24 Hours)";
                //Labe.Text = "Page 1";
                ddlMapTOVehicle.Enabled = false;
                date.Enabled = false;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Fuel parameters are not calibrated..');", true);
                //string alertstring = "Fuel parameters are not calibrated..";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));

            }
        }
        public DataTable binddata1()
        {
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable DrivDet = db1.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

            DataTable dt1 = new DataTable();

            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Rows.Add(0);
            string fdate12 = date.Text + " 12:00 AM";
            DateTime dhdh = Convert.ToDateTime(date.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                DataTable dt = db1.getRhours();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    double TKMR1 = 0;
                    if (dt2.Rows.Count != 0)
                    {
                        DataTable dt12 = db1.Select_TotalRunkm(ddlMapTOVehicle.SelectedValue.ToString(), fdate12, tdate);
                        for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                        {
                            TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                        }
                        if (dt12.Rows.Count != 0)
                        {
                            fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                        }
                        TKMR1 = (double)Math.Round(TKMR1, 2);
                    }
                    dr["TKMR"] = TKMR1.ToString();
                    dt1.Rows.Add(dr);
                }
            }
            else
            {
                DataTable dt = db1.getRhours();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.searchold(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    double TKMR1 = 0;
                    if (dt2.Rows.Count != 0)
                    {
                        DataTable dt12 = db1.Select_TotalRunkmold(ddlMapTOVehicle.SelectedValue.ToString(), fdate12, tdate);
                        for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                        {
                            TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                        }
                        if (dt12.Rows.Count != 0)
                        {
                            fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                        }
                        TKMR1 = (double)Math.Round(TKMR1, 2);
                    }
                    dr["TKMR"] = TKMR1.ToString();
                    dt1.Rows.Add(dr);
                }

            }
            return dt1;
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }


        public DataTable LoadChartData1()
        {
            DateTime date1 = DateTime.Now;
            string mdate = date.Text + " 12:00 AM";
            string sDate = date1.ToShortDateString();
            string ddd = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
            DataTable dt2 = binddata1();
            int startX = 0;
            Double startY = 0;
            int i = 0;
            //int Y = 0;
            //i = 0;
            //Y = 1;
            DataTable tkm = new DataTable();
            tkm.Columns.Add("sno", typeof(int));
            tkm.Columns.Add("km", typeof(string));
            DataRow dr = tkm.NewRow();
            dr["sno"] = "0";
            dr["km"] = "0";
            tkm.Rows.Add(dr);
            //if (i < Y)
            //{
            if (date.Text == sDate)
            {
                TimeSpan tsc = Convert.ToDateTime(ddd) - Convert.ToDateTime(mdate);
                int minit = Convert.ToInt32(tsc.Hours.ToString());
                for (int k = 0; k <= minit; k++)
                {
                    startX = k * 60;
                    // startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    // Chart2.Series["kilometers"].Points.AddXY(startX, startY);
                    dr["sno"] = startX.ToString();
                    dr["km"] = startY.ToString();

                    //tkm.Rows.Add(dr);
                    tkm.ImportRow(dr);
                    startX = 0;
                    i++;
                }

                DataRow row = (DataRow)tkm.Rows[tkm.Rows.Count - 1];
                //TimeSpan tsc = Convert.ToDateTime(ddd) - Convert.ToDateTime(mdate);
                //int mini = Convert.ToInt32(tsc.TotalMinutes.ToString());
                TimeSpan minitu = TimeSpan.FromHours(minit);
                int mini = Convert.ToInt32(minitu.TotalMinutes.ToString());

                int n = Convert.ToInt32(row[0]);
                for (int m = n; m < mini; m++)
                {
                    n++;
                    dr["sno"] = n.ToString();//ts.TotalMinutes.ToString();
                    dr["km"] = row[1].ToString();//fuel.ToString();
                    tkm.ImportRow(dr);
                }


            }
            else
            {
                for (int k = 0; k < 25; k++)
                {
                    startX = k * 60;
                    // startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    // Chart2.Series["kilometers"].Points.AddXY(startX, startY);
                    dr["sno"] = startX.ToString();
                    dr["km"] = startY.ToString();

                    //tkm.Rows.Add(dr);
                    tkm.ImportRow(dr);
                    startX = 0;
                    i++;
                }

            }
            return tkm;
        }

        public DataTable binddata()//string Vno, string sdate, string edate)
        {

            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            string sdate = date.Text + " 12:00 AM";
            string edate = date.Text + " 11:59 PM";

 
 
            DBClass db1 = new DBClass();
 
 

            DataTable fff = new DataTable();
            fff.Columns.Add("SPED", typeof(string));
            fff.Columns.Add("IGST", typeof(string));
            fff.Columns.Add("GTIM", typeof(string));
            fff.Columns.Add("FUEL_ADC", typeof(string));
            DateTime dhdh = Convert.ToDateTime(date.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {

                DataTable dts = db1.getRhours();

                for (int i = 0; i < dts.Rows.Count; i++)
                {
                    DataRow dr = fff.NewRow();
                    string fdate = date.Text + " " + dts.Rows[i][0];
                    string tdate = date.Text + " " + dts.Rows[i][1];

                    DataTable dt = db.Select_Fuel(Vno, fdate, tdate);
                    if (dt.Rows.Count != 0)
                    {
                        //for (int j = 0; j < dt.Rows.Count; i++)
                        //{

                        dr["SPED"] = dt.Rows[0][0].ToString();
                        dr["IGST"] = dt.Rows[0][1].ToString();
                        dr["GTIM"] = dt.Rows[0][2].ToString();
                        dr["FUEL_ADC"] = dt.Rows[0][3].ToString();
                        //fff.ImportRow(dr);
                        fff.Rows.Add(dr);
                        //}
                    }

                }
            }
            else
            {
                DataTable dts = db1.getRhours();

                for (int i = 0; i < dts.Rows.Count; i++)
                {
                    DataRow dr = fff.NewRow();
                    string fdate = date.Text + " " + dts.Rows[i][0];
                    string tdate = date.Text + " " + dts.Rows[i][1];

                    DataTable dt = db.Select_Fuelold(Vno, fdate, tdate);
                    if (dt.Rows.Count != 0)
                    {
                        //for (int j = 0; j < dt.Rows.Count; i++)
                        //{

                        dr["SPED"] = dt.Rows[0][0].ToString();
                        dr["IGST"] = dt.Rows[0][1].ToString();
                        dr["GTIM"] = dt.Rows[0][2].ToString();
                        dr["FUEL_ADC"] = dt.Rows[0][3].ToString();
                        //fff.ImportRow(dr);
                        fff.Rows.Add(dr);
                        //}
                    }

                }

            }

            return fff;
        }

        public DataTable loaddata()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dt = binddata();
            DataTable dttemp = new DataTable();
            dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            DateTime date1 = DateTime.Now;
            string mdate = date.Text + " 12:00 AM";
            string sDate = date1.ToShortDateString();
            string ddd = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

            if (dt.Rows.Count != 0)
            {
                DataTable fu=new DataTable ();
                DateTime dhdh = Convert.ToDateTime(date.Text);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    fu = db.prevfuelstaus1(Vno, date.Text);
                }
                else
                {
                    fu = db.prevfuelstaus1old(Vno, date.Text);
                }
                if (fu.Rows.Count != 0)
                {
                    for (int i = 0; i < fu.Rows.Count; i++)
                    {
                        DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                        DataRow dr = dttemp.NewRow();
                        TimeSpan st = Convert.ToDateTime(DateTime.Now) - Convert.ToDateTime(date.Text + " 12:00 AM");
                        double string_val_fuel = Convert.ToDouble(fu.Rows[i][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                        double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used

                        double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                        int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                        double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                        double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());


                        double persent = (tank_capacity * (0.05));
                        if (Tank_typ == 0)
                        {
                            double x = Fulltankvalue - Emptytankvalue;
                            double y = tank_capacity / x;

                            double z = string_val_fuel - Emptytankvalue;

                            double b = z * y;
                            b = (double)Math.Round(b);

                            if (b > persent)
                            {
                                dr["fuel"] = b.ToString();
                                dttemp.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            double x = Emptytankvalue - Fulltankvalue;
                            double y = tank_capacity / x;

                            double z = string_val_fuel - Fulltankvalue;

                            double b = z * y;
                            double f = tank_capacity - b;
                            f = (double)Math.Round(f);

                            if (f > persent)
                            {
                                //fuel = (string_val_fuel) / (Fuel_Cali);
                                //fuel = fuel * tank_capacity;
                                //fuel = tank_capacity - fuel;
                                //fuel = Math.Truncate(fuel * 100) / 100;
                                dr["fuel"] = f.ToString();
                                dttemp.Rows.Add(dr);
                            }
                        }

                        //int n = 0;

                        int startX = 0;

                        TimeSpan ts = Convert.ToDateTime(dt.Rows[i][2].ToString()) -Convert.ToDateTime(date.Text + " 12:00 AM");
                        // int min = Convert.ToInt32(ts.TotalMinutes.ToString());
                        int minit = Convert.ToInt32(ts.Hours.ToString());

                        for (int m = 0; m <= minit; m++)
                        {
                            //n++;
                            startX = m * 60;
                            dr["sno"] = startX.ToString();//ts.TotalMinutes.ToString();
                            dr["fuel"] = dttemp.Rows[0][1].ToString();//fuel.ToString();
                            dttemp.ImportRow(dr);
                        }
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                    DataRow dr = dttemp.NewRow();
                    TimeSpan ts = Convert.ToDateTime(dt.Rows[i][2].ToString()) - Convert.ToDateTime(date.Text + " 12:00 AM");
                    string min = ts.TotalMinutes.ToString();
                    dr["sno"] = min;
                    double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                    double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used

                    double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                    int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                    double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                    double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());

                    double persent = (tank_capacity * (0.05));

                    if (Tank_typ == 0)
                    {
                        double x = Fulltankvalue - Emptytankvalue;
                        double y = tank_capacity / x;

                        double z = string_val_fuel - Emptytankvalue;

                        double b = z * y;
                        b = (double)Math.Round(b);
                        if (b > persent)
                        {
                            //fuel = (string_val_fuel) / (Fuel_Cali);
                            //fuel = fuel * tank_capacity;
                            //fuel = Math.Truncate(fuel * 100) / 100;
                            dr["fuel"] = b.ToString();
                            dttemp.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        double x = Emptytankvalue - Fulltankvalue;
                        double y = tank_capacity / x;

                        double z = string_val_fuel - Fulltankvalue;

                        double b = z * y;
                        double f = tank_capacity - b;
                        f = (double)Math.Round(f);

                        if (f > persent)
                        {


                            //fuel = (string_val_fuel) / (Fuel_Cali);
                            //fuel = fuel * tank_capacity;
                            //fuel = tank_capacity - fuel;
                            //fuel = Math.Truncate(fuel * 100) / 100;
                            dr["fuel"] = f.ToString();
                            dttemp.Rows.Add(dr);
                        }
                    }
                    int startX = 0;
                    if (dt.Rows.Count == i + 1)
                    {
                        DataRow row = (DataRow)dttemp.Rows[dttemp.Rows.Count - 1];
                        int n = Convert.ToInt32(row[0]);
                        int h = n / 60;
                        TimeSpan dtsc = TimeSpan.FromHours(h);
                        int minit = Convert.ToInt32(dtsc.Hours.ToString());

                        if (date.Text == sDate)
                        {
                            TimeSpan tsc = Convert.ToDateTime(ddd) - Convert.ToDateTime(mdate);
                            int mini = Convert.ToInt32(tsc.TotalMinutes.ToString());
                            for (int m = n; m < mini; m++)
                            {
                                n++;
                                dr["sno"] = n.ToString();//ts.TotalMinutes.ToString();
                                dr["fuel"] = row[1].ToString();//fuel.ToString();
                                dttemp.ImportRow(dr);
                            }
                        }
                        else
                            for (int m = minit; m <= 24; m++)
                            {
                                // n++;
                                startX = m * 60;
                                dr["sno"] = startX.ToString();//ts.TotalMinutes.ToString();
                                dr["fuel"] = row[1].ToString();//fuel.ToString();
                                //dttemp.Rows.Add(dr);           
                                dttemp.ImportRow(dr);
                            }
                    }
                }
            }
            else if (dt.Rows.Count == 0)
            {
                //DataTable fu = db.prevfuelstaus1(Vno, date.Text);
                //DataTable fu = new DataTable();
                DataTable fu = new DataTable();
                DateTime dhdh = Convert.ToDateTime(date.Text);
                int emonth = dhdh.Month;
                DateTime curretmonth = DateTime.Now;
                int cmonth = curretmonth.Month;
                if (emonth == cmonth)
                {
                    fu = db.prevfuelstaus1(Vno, date.Text);
                }
                else
                {
                    fu = db.prevfuelstaus1old(Vno, date.Text);
                }
                if (fu.Rows.Count != 0)
                {
                    for (int i = 0; i < fu.Rows.Count; i++)
                    {
                        DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                        DataRow dr = dttemp.NewRow();
                        TimeSpan ts = Convert.ToDateTime(fu.Rows[i][2].ToString()) - Convert.ToDateTime(date.Text + " 12:00 AM");
                        dr["sno"] = "0";//ts.TotalMinutes.ToString();
                        TimeSpan st = Convert.ToDateTime(DateTime.Now) - Convert.ToDateTime(date.Text + " 12:00 AM");
                        double string_val_fuel = Convert.ToDouble(fu.Rows[i][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                        double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used
                        double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                        int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                        double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                        double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                        double persent = (tank_capacity * (0.08));

                        if (Tank_typ == 0)
                        {
                            double x = Fulltankvalue - Emptytankvalue;
                            double y = tank_capacity / x;

                            double z = string_val_fuel - Emptytankvalue;

                            double b = z * y;
                            b = (double)Math.Round(b);
                            if (b > persent)
                            {
                                //fuel = (string_val_fuel) / (Fuel_Cali);
                                //fuel = fuel * tank_capacity;
                                //fuel = Math.Truncate(fuel * 100) / 100;
                                dr["fuel"] = b.ToString();
                                dttemp.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            double x = Emptytankvalue - Fulltankvalue;
                            double y = tank_capacity / x;

                            double z = string_val_fuel - Fulltankvalue;

                            double b = z * y;
                            double f = tank_capacity - b;
                            f = (double)Math.Round(f);
                            if (f > persent)
                            {

                                //fuel = (string_val_fuel) / (Fuel_Cali);
                                //fuel = fuel * tank_capacity;
                                //fuel = tank_capacity - fuel;
                                //fuel = Math.Truncate(fuel * 100) / 100;
                                dr["fuel"] = f.ToString();
                                dttemp.Rows.Add(dr);
                            }
                        }
                        int startX = 0;
                        if (date.Text == sDate)
                        {
                            TimeSpan tsc = Convert.ToDateTime(ddd) - Convert.ToDateTime(mdate);
                            //int mini = Convert.ToInt32(tsc.TotalMinutes.ToString());
                            int minit = Convert.ToInt32(tsc.Hours.ToString());
                            for (int m = 0; m <= minit; m++)
                            {
                                // n++;
                                startX = m * 60;
                                dr["sno"] = startX.ToString();//ts.TotalMinutes.ToString();
                                dr["fuel"] = dttemp.Rows[0][1].ToString();//fuel.ToString();
                                dttemp.ImportRow(dr);
                            }
                        }
                        else
                            for (int m = 0; m <= 24; m++)
                            {
                                //n++;
                                startX = m * 60;
                                dr["sno"] = startX.ToString();//ts.TotalMinutes.ToString();
                                dr["fuel"] = dttemp.Rows[0][1].ToString();//fuel.ToString();
                                dttemp.ImportRow(dr);
                            }
                    }
                }
            }

            return dttemp;


        }
        public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {

            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }


        protected void Refresh_Click(object sender, EventArgs e)
        {
            Chart1.Series[0].Points.Clear();
            Chart2.Series[0].Points.Clear();
            ddlMapTOVehicle.SelectedValue = null;
            date.Text = string.Empty;
            btnCalculate.Enabled = true;
            Response.Redirect(Request.RawUrl);
        }
        private DataTable fuelliter12()
        {
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            DataTable fu = db.prevfuelstaus1(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
            if (fu.Rows.Count != 0)
            {
                DataRow dr = dttemp.NewRow();
                double string_val_fuel = Convert.ToDouble(fu.Rows[0][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used
                //double Empty_Tank = 0;
                DataTable dtf = db.GetVehiclesfuel(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserId"].ToString());
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());

                if (Tank_typ == 0)
                {
                    double x = Fulltankvalue - Emptytankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Emptytankvalue;

                    double b = z * y;
                    b = (double)Math.Round(b);


                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = b.ToString();
                    dttemp.Rows.Add(dr);
                }
                else
                {
                    double x = Emptytankvalue - Fulltankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Fulltankvalue;

                    double b = z * y;
                    double f = tank_capacity - b;
                    f = (double)Math.Round(f);

                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = tank_capacity - fuel;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = f.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }

        private DataTable fuelliter12old()
        {
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            DataTable fu = db.prevfuelstaus1old(ddlMapTOVehicle.SelectedValue.ToString(), date.Text);
            if (fu.Rows.Count != 0)
            {
                DataRow dr = dttemp.NewRow();
                double string_val_fuel = Convert.ToDouble(fu.Rows[0][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used
                //double Empty_Tank = 0;
                DataTable dtf = db.GetVehiclesfuel(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserId"].ToString());
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());

                if (Tank_typ == 0)
                {
                    double x = Fulltankvalue - Emptytankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Emptytankvalue;

                    double b = z * y;
                    b = (double)Math.Round(b);
                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = b.ToString();
                    dttemp.Rows.Add(dr);
                }
                else
                {
                    double x = Emptytankvalue - Fulltankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Fulltankvalue;

                    double b = z * y;
                    double f = tank_capacity - b;
                    f = (double)Math.Round(f);

                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = tank_capacity - fuel;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = f.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
        protected void Download_Click(object sender, EventArgs e)
        {
            DataTable fre = db.Getfuelimp(Session["Username"].ToString());
            if (fre.Rows[0][1].ToString() == "Yes")
            {
                DateTime dt = Convert.ToDateTime(date.Text);
                string str = dt.ToString("yyyy-MM-dd");
                string str1 = str.Replace("-", "");
                str1 = str1.Replace(" ", "");
                str1 = str1.Substring(2);
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VTC_" + vstr.Replace(" ", "") + "_" + str1 + ".pdf";
                // bindchartdata();
                MemoryStream m = new MemoryStream();
                Document document = new Document(PageSize.A3.Rotate(), 60, 0, 30, 10);
                try
                {
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    XhtmlTextWriter hw = new XhtmlTextWriter(sw);
                    //tblToExport.RenderControl(hw);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                    PdfWriter.GetInstance(document, m);
                    document.Open();
                    PdfPTable table = new PdfPTable(3);
                    PdfPCell cell = new PdfPCell(new Phrase(lblhead.Text, new Font(Font.TIMES_ROMAN, 24f, Font.BOLD, Color.BLUE)));
                    document.Add(table);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        Chart2.SaveImage(stream, ChartImageFormat.Png);
                        iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                        chartImage.ScalePercent(75f);
                        document.Add(chartImage);
                    }
                    using (MemoryStream stream = new MemoryStream())
                    {
                        Chart1.SaveImage(stream, ChartImageFormat.Png);
                        iTextSharp.text.Image chartImage1 = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                        chartImage1.ScalePercent(75f);
                        document.Add(chartImage1);

                    }

                    document.Add(new Paragraph(sw.ToString()));
                }
                catch (DocumentException ex)
                {
                    Console.Error.WriteLine(ex.StackTrace);
                    Console.Error.WriteLine(ex.Message);
                }
                document.Close();
                Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
            }
            else
            {
                string message = "alert('Service Not Availed.... ')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                //string alertstring = " Service Not Availed";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }

        }


        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            DataTable fre = db.Getfuelimp(Session["Username"].ToString());
            if (fre.Rows[0][1].ToString() == "Yes")
            {
                if (Convert.ToDateTime(date.Text) < DateTime.Today)
                {
                    string pageurl = "Calc.aspx?Veh=" + ddlMapTOVehicle.SelectedValue.ToString() + "&&Date=" + date.Text;
                    string s = "window.open('" + pageurl + "', 'popup_window','width=600,height=500,left=10,top=10,resizable=yes');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "script", s, true);
                    //  Response.Write("<script> window.open( '" + pageurl + "'); </script>");
                    //Response.End();
                    //Response.TransmitFile("Calc.aspx?Veh="+ddlMapTOVehicle.SelectedValue.ToString()+"&&Date="+ date.Text);
                    // string[] alertstring = getalertdetailsfunction();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0},\\n{1},\\n{2},\\n{3},\\n{4},\\n{4},\\n')</script> ", alertstring));
                }
            }
            else
            {
                string message = "alert('Service Not Availed.... ')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                //string alertstring = " Service Not Availed";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
        }
        public string[] getalertdetailsfunction()
        {
            List<string> stralerts = new List<string>();
            DataTable fuelop = fuelliter12();
            stralerts.Add("Opening Fuel :" + fuelop.Rows[0][0].ToString());

            DataTable fuelclo = fuelliter123();
            stralerts.Add("Closing Fuel :" + fuelclo.Rows[0][0].ToString());

            DataTable milage = db.getmilage(ddlMapTOVehicle.SelectedValue.ToString());
            stralerts.Add("Vehicle milage :" + milage.Rows[0][0].ToString());

            DataTable dt2 = binddata1();
            double km = 0;
            for (int i = 0; i <= 24; i++)
            {
                km += Convert.ToDouble(dt2.Rows[i][0].ToString());
            }
            string kms = km.ToString();
            stralerts.Add("Vehicle run km KM :" + km.ToString());

            string a = (Convert.ToDouble(kms) / Convert.ToDouble(milage.Rows[0][0].ToString())).ToString();
            //string a = (Convert.ToDouble(kms) / Convert.ToDouble(mil)).ToString();
            stralerts.Add("Approximate fuel filed as per data :" + a + "");

            string RegNUM = ddlMapTOVehicle.SelectedValue.ToString();

            string gDate = date.Text;
            DataTable FuFTo = db.Fuel_fill2(RegNUM, gDate);
            int fliter = 0;
            if (FuFTo.Rows.Count != 0)
            {

                for (int i = 0; i < FuFTo.Rows.Count; i++)
                {
                    fliter += Convert.ToInt32(FuFTo.Rows[i][2]);
                }
            }

            stralerts.Add("Actual fuel filled:" + fliter.ToString());
            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {
                salerts[m] = stralerts[m].ToString();
            }
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);

            return salerts;
        }
        private DataTable fuelliter123()
        {
            DataTable dttemp = new DataTable();
            //dttemp.Columns.Add("sno", typeof(int));
            dttemp.Columns.Add("fuel", typeof(string));
            string fdate14 = date.Text + " 12:00 AM";
            string tdate13 = date.Text + " 11:59 PM";
            DataTable fu = db.prevfuelstaus111(ddlMapTOVehicle.SelectedValue.ToString(), fdate14, tdate13);
            if (fu.Rows.Count != 0)
            {
                DataRow dr = dttemp.NewRow();
                double string_val_fuel = Convert.ToDouble(fu.Rows[0][3].ToString());
#pragma warning disable CS0219 // The variable 'fuel' is assigned but its value is never used
                double fuel = 0;
#pragma warning restore CS0219 // The variable 'fuel' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'Empty_Tank' is assigned but its value is never used
                double Empty_Tank = 0;
#pragma warning restore CS0219 // The variable 'Empty_Tank' is assigned but its value is never used
                DataTable dtf = db.GetVehiclesfuel(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserId"].ToString());
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                if (Tank_typ == 0)
                {
                    double x = Fulltankvalue - Emptytankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Emptytankvalue;

                    double b = z * y;
                    b = (double)Math.Round(b);


                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = b.ToString();
                    dttemp.Rows.Add(dr);
                }
                else
                {
                    double x = Emptytankvalue - Fulltankvalue;
                    double y = tank_capacity / x;

                    double z = string_val_fuel - Fulltankvalue;

                    double b = z * y;
                    double f = tank_capacity - b;
                    f = (double)Math.Round(f);

                    //fuel = (string_val_fuel) / (Fuel_Cali);
                    //fuel = fuel * tank_capacity;
                    //fuel = tank_capacity - fuel;
                    //fuel = Math.Truncate(fuel * 100) / 100;
                    dr["fuel"] = f.ToString();
                    dttemp.Rows.Add(dr);
                }
            }
            return dttemp;
        }
    }
}

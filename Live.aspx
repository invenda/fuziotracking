﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Live.aspx.cs" Inherits="Tracking.Live"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content1" runat="server">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
     function SelectSingleRadiobutton(rdbtnid) 
     {
        var rdBtn = document.getElementById(rdbtnid);
        var rdBtnList = document.getElementsByTagName("input");
        for (i = 0; i < rdBtnList.length; i++) 
         {
           if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id)
            {
              rdBtnList[i].checked = false;
            }
        }
       
     }  
       function alertsdetails(x)
        {        
         alert(x.join('\n'));  
        }
       
    </script>

    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:Timer ID="Timer1" runat="server" Interval="40000" OnTick="Timer1_Tick">
    </asp:Timer>
    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <table style="width: 100%; height: 10px; background-color: Gray;">
                    <tr>
                        <td style="width: 50%; height: 20px; vertical-align: middle; font-weight: bold;"
                            align="left">
                            <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Black" Text="Engine Status :"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:Image ID="imgst" runat="server" Width="15px" Height="15px" />
                            <asp:Label Text="Stopped" runat="server" ID="Lbl1" ForeColor="Black"></asp:Label>&nbsp;&nbsp;
                            <asp:Image ID="imgidl" runat="server" Width="15px" Height="15px" />
                            <asp:Label Text="Idle" runat="server" ID="lbl2" ForeColor="Black"></asp:Label>&nbsp;&nbsp;
                            <asp:Image ID="imgmv" runat="server" Width="15px" Height="15px" />
                            <asp:Label ID="lbl3" runat="server" Text="Moving" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 50%; height: 100%; vertical-align: middle;" align="center">
                            <asp:Label ID="lblmalerts" runat="server" Font-Bold="true" Visible="false" ForeColor="Black"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 20px; border-top-width: medium; vertical-align: middle;">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="width: 25%">
                        </td>
                        <td>
                            <div>
                                <marquee>
                        <strong > <asp:Label ID="lblalerts2" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                        <asp:Label ID="lbloffalerts" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                   </strong> 
                    </marquee>
                            </div>
                        </td>
                    </tr>
                </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <div style="position: relative; vertical-align: top; height: 400px; overflow: auto;
        background-color: White; top: 0px; left: 0px;">
        <asp:UpdatePanel ID="updatepannel" runat="server" UpdateMode ="Conditional" >
            <ContentTemplate>
                <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" BackColor="#CCFF99"
                    HorizontalAlign="Center" Width="100%" BorderColor="#FF33CC" 
                    ForeColor="Black" OnRowDataBound="GV_RowDataBound">
                    <RowStyle Height ="5px" />
                    <AlternatingRowStyle Height ="5px" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vehicle RTO no" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("VehicalNumber") %>'
                                    Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Left" Font-Bold="false " Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Engine status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Image ID="Simg" runat="server" Height="16px" Width="16px" BackColor="Transparent" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="status" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>' Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit seriol no" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="70px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Data" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="170px">
                            <ItemTemplate>
                                <asp:Label ID="lbllastdata" Text='<%# Bind("Lastdata") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="160px" />
                            <ItemStyle HorizontalAlign="Right" />
                            
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Gps" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="20">
                            <ItemTemplate>
                                <asp:Label ID="lblgps" Text='<%# Bind("Gps") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="20px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Over Speed">
                            <ItemTemplate>
                                <asp:Label ID="lblovs" Text='<%# Bind("OVS") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="150px" />
                             <ItemStyle Width ="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ignition" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="45px">
                            <ItemTemplate>
                                <asp:Label ID="lbling" Text='<%# Bind("ingnition") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="45px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField HeaderText="Battety voltage" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:Label ID="lblbvolt" Text='<%# Bind("bvolt") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                       
                         <asp:TemplateField HeaderText="Driver Name" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lbldname" Text='<%# Bind("Dname") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver mobile" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lbldmobile" Text='<%# Bind("dmobile") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Location" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Speed" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnspeed" runat="server" OnClick="btnspeed_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fuel" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnfuel" runat="server" OnClick="btnfuel_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today KM run" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnKM" runat="server" OnClick="btnKM_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Temp" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btntemp" runat="server" OnClick="btntemp_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30">
                            <ItemTemplate>
                                <asp:Button ID="btnavail" runat="server" OnClick="btnavail_Click" CssClass="butt"/>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Alert" HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnalert" runat="server" OnClick="btnalert_Click" CssClass="buttona " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#006600" ForeColor="White" Height="3px" />
                    <AlternatingRowStyle BackColor="#99CCFF" Height="6px" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

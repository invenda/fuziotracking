﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="plotGeoPoints.aspx.cs" Inherits="Tracking.plotGeoPoints" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Delete The Point ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }            
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%;">
        <div style="height: auto; width: 100%; color: Blue; text-align: center;">
            <h2>
                <a>Plot Geo Route</a>
                Points
            </h2>
        </div>
        <div style="height: auto;">
            <input id="gpoint" type="hidden" runat="server" />
            <input id="inpHide" type="hidden" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 100%; height: auto;">
            <table style="width: 100%;" border="1">
                <tr>
                    <td>
                        Select Route No
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlrno"
                            ValidationGroup="Group1" ErrorMessage="Select Route No" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                    </td>
                    <td>
                        Route Type
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlrttype"
                            ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                            ValueToCompare="--">*</asp:CompareValidator>
                    </td>
                    <td >
                        <asp:Label ID="lblrid" runat="server" Text="Route Name" Width="100px"></asp:Label>
                    </td>
                    <td style="border-right-style: none;">
                        Radius(In Mtrs)
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtradius"
                            ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$" ValidationGroup="Group1">*</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtradius"
                            ValidationGroup="Group1" ErrorMessage="Enter Circle Radius">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-left-style: none;">
                        <asp:Panel ID="pnldate" runat="server" Visible="false" BorderWidth="1" Height="38px">
                            Date:<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtdate"
                                ValidationGroup="Group1" ErrorMessage="Select Date">*</asp:RequiredFieldValidator>
                        </asp:Panel>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-left-style: none;">
                        <asp:Panel ID="pnltime" runat="server" Visible="false" BorderWidth="1" Height="38px" Width="210px">
                            In Time:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rbtnAMPM"
                                ErrorMessage="Select AM/PM" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlFromHOURS"
                                ValidationGroup="Group1" ErrorMessage="Select From Hours" Operator="NotEqual"
                                ValueToCompare="--">*</asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlFromMINUTES"
                                ValidationGroup="Group1" ErrorMessage="Select From Minutes" Operator="NotEqual"
                                ValueToCompare="--">*</asp:CompareValidator>
                        </asp:Panel>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-left-style: none;">
                        <asp:Panel ID="pnlotime" runat="server" Visible="false" BorderWidth="1" Height="38px" Width="210px">
                            Out Time:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rbtnDT"
                                ErrorMessage="Select TO AM/PM" ValidationGroup="Group1">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlToHOURS"
                                ValidationGroup="Group1" ErrorMessage="Select To Hours" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="ddlToMINUTES"
                                ValidationGroup="Group1" ErrorMessage="Select To Minutes" Operator="NotEqual"
                                ValueToCompare="--">*</asp:CompareValidator>
                        </asp:Panel>
                    </td>
                    <td style="text-align: center;">
                        <asp:Button ID="BtnClear" runat="server" Text="Un Set" Width="70" OnClick="BtnClear_Click" />
                    </td>
                    <td style="text-align: center;">
                        <asp:Button ID="Btn_Clear" runat="server" Text="Refresh" OnClick="Btn_Clear_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlrno" runat="server" Width="100px" DataValueField="Route_No"
                            DataTextField="Route_No" AutoPostBack="true" OnDataBound="ddlrno_DataBound" OnSelectedIndexChanged="ddlrno_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlrttype" runat="server" Width="100px">
                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                            <asp:ListItem Text="TRIP BASIS" Value="0"></asp:ListItem>
                            <asp:ListItem Text="DAILY BASIS" Value="1"></asp:ListItem>
                            <asp:ListItem Text="DATE BASIS" Value="2"></asp:ListItem>
                            <asp:ListItem Text="DATE/TIME BASIS" Value="3"></asp:ListItem>
                            <asp:ListItem Text="ROUTE BASIS" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblrtname" runat="server"></asp:Label>
                    </td>
                    <td style="border-right-style: none;">
                        <asp:TextBox ID="txtradius" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-top-style: none;
                        border-left-style: none;">
                        <asp:Panel ID="pnldate1" runat="server" Visible="false" BorderWidth="1" Height="38px">
                            <asp:TextBox ID="txtdate" runat="server" Width="100px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="txtdate">
                            </cc1:CalendarExtender>
                        </asp:Panel>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-top-style: none;
                        border-left-style: none;">
                        <asp:Panel ID="pnltime1" runat="server" Visible="false" BorderWidth="1" Height="38px"
                            Width="210px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="40px" DataValueField="Hours"
                                            DataTextField="Hours">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                            DataTextField="Minutes">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td style="border-bottom-style: none; border-right-style: none; border-top-style: none;
                        border-left-style: none;">
                        <asp:Panel ID="pnlotime1" runat="server" Visible="false" BorderWidth="1" Height="38px"
                            Width="210px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlToHOURS" runat="server" Width="40px" DataValueField="Hours"
                                            DataTextField="Hours">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                            DataTextField="Minutes">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td style="text-align: center;">
                        <asp:Button ID="BtnSubmit" runat="server" Text="Submit" Width="70" ValidationGroup="Group1"
                            OnClick="BtnSubmit_Click" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="600px" OnClick="GMap1_Click" />
        </div>
        <div style="width: 100%; background-color: Gray;">
            <asp:GridView ID="gvmappoints" runat="server" AutoGenerateColumns="false" DataKeyNames="ID" BorderColor="Black"
                Width="100%" HorizontalAlign="Center" RowStyle-ForeColor="Black" OnRowDataBound="gvmappoints_RowDataBound">
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px"
                    Height="30px" />
                <Columns>
                    <asp:TemplateField HeaderText="Sno" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblsno" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<% #Eval("ID")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ROute No" DataField="Route_No" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Route Name" DataField="Route_Name" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Location" DataField="Route_Address" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Radius(mtr)" DataField="Radius" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="10px"></asp:BoundField>
                    <%--<asp:TemplateField HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Button ID="remove" runat="server" Text="Delete" AutoPostBack="true" OnClientClick="ConfirmIt()"
                                OnClick="changed" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </div>
        <div style="height: 5px; background-color: Gray;">
        </div>
    </div>
</asp:Content>


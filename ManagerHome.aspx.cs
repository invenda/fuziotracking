﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;

namespace Tracking
{
    public partial class ManagerHome : System.Web.UI.Page
    {
        static int lblno = 0;
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["page"] = "All Vehicle-Dashboard";
            Session["page1"] = "";
          
            if (!IsPostBack)
            {
                //if (Session["UserID"] != null&&Session["UserRole"].ToString() == "3")
                //{
                Vehicle_Bind();
                // }
            }
        }
        public void Vehicle_Bind()
        {
            DataTable CUsers = db.Get_CAdminUsers(Session["Cusername"].ToString(), 0);
            if (CUsers.Rows.Count > 0)
            {
                DataTable stat = db.Select_GetVehicles(CUsers.Rows[0][1].ToString());
                // DataTable stat = db.Get_CAminVeh(Session["UserID"].ToString(), CUsers);
                if (stat.Columns.Count > 0)
                {
                    stat.Columns.Add("Status", typeof(string));
                    stat.Columns.Add("Lastdata", typeof(string));
                    stat.Columns.Add("Gps", typeof(string));
                    // stat.Columns.Add("OVS", typeof(string));
                    stat.Columns.Add("ingnition", typeof(string));
                    stat.Columns.Add("bvolt", typeof(string));
                    stat.Columns.Add("dmobile", typeof(string));
                    stat.Columns.Add("Dname", typeof(string));
                    for (int i = 0; i < stat.Rows.Count; i++)
                    {
                        string VN = Convert.ToString(stat.Rows[i][0].ToString());

                        DataTable VS = db.GetVehStat(Session["UserID"].ToString(), VN);
                        DataTable status = db.Select_GetVehiclePresentlocation(VN, Session["UserID"].ToString());

                        if (VS.Rows.Count != 0)
                        {
                            // DataTable status = db.Select_GetVehiclePresentlocation(VN, Session["UserID"].ToString());
                            DateTime sysDT = DateTime.Now;
                            DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                            StringDT = StringDT.AddMinutes(360);
                            if (sysDT > StringDT)
                            {
                                stat.Rows[i]["Status"] = "Status";// "  OFFLINE &nbsp; ?";

                            }
                            else //if(sysDT <= StringDT)
                            {
                                stat.Rows[i]["Status"] = "Status";//"  ONLINE &nbsp;  ?";

                            }
                            if (status.Rows.Count != 0)
                            {
                                //lastdata
                                DateTime fff = Convert.ToDateTime(status.Rows[0][4].ToString());
                                stat.Rows[i]["Lastdata"] = fff.ToString("dd/MM/yyyy hh:mm tt");//status.Rows[0][4].ToString();
                                //GPS
                                stat.Rows[i]["Gps"] = status.Rows[0][7].ToString();

                                //ingnition
                                if (status.Rows[0][8].ToString() == "0")
                                    stat.Rows[i]["ingnition"] = "Off";
                                else
                                    stat.Rows[i]["ingnition"] = "On";

                                //battery voltage
                                int bat = Convert.ToInt32(status.Rows[0][3].ToString());
                                double bvolt = bat / 42.5;
                                bvolt = Math.Round(bvolt, 2);
                                stat.Rows[i]["bvolt"] = bvolt.ToString();

                            }
                        }
                        else
                        {
                            DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), VN);
                            if (VSoff.Rows.Count > 0)
                            {
                                DataTable statusoff = db.GetVehiclePresentlocation1(VN, Session["UserID"].ToString());
                                DateTime sysDT = DateTime.Now;
                                DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                                StringDT = StringDT.AddMinutes(360);
                                if (sysDT > StringDT)
                                {
                                    stat.Rows[i]["Status"] = "Status";// "  OFFLINE &nbsp; ?";

                                }
                                else //if(sysDT <= StringDT)
                                {
                                    stat.Rows[i]["Status"] = "Status";//"  ONLINE &nbsp;  ?";

                                }

                                if (status.Rows.Count == 0)
                                {
                                    //lastdata
                                    DateTime fff = Convert.ToDateTime(statusoff.Rows[0][4].ToString());
                                    stat.Rows[i]["Lastdata"] = fff.ToString("dd/MM/yyyy hh:mm tt");//statusoff.Rows[0][4].ToString();
                                    //GPS
                                    stat.Rows[i]["Gps"] = statusoff.Rows[0][7].ToString();

                                    //ingnition
                                    if (statusoff.Rows[0][8].ToString() == "0")
                                        stat.Rows[i]["ingnition"] = "Off";
                                    else
                                        stat.Rows[i]["ingnition"] = "On";

                                    //battery voltage
                                    int bat = Convert.ToInt32(statusoff.Rows[0][3].ToString());
                                    double bvolt = bat / 42.5;
                                    bvolt = Math.Round(bvolt, 2);
                                    stat.Rows[i]["bvolt"] = bvolt.ToString();

                                }
                            }
                            else
                            {

                            }

                        }

                        //driver mobile no
                        DataTable DriDet = db.GerDriverDetails(VN, Session["UserID"].ToString());
                        stat.Rows[i]["dmobile"] = DriDet.Rows[0][2].ToString();

                        //Driver name
                        string dname11 = DriDet.Rows[0][0].ToString();
                        if (dname11.Length < 13)
                        {
                            stat.Rows[i]["Dname"] = dname11;
                        }
                        else
                        {
                            string str = dname11.Substring(0, 13);
                            stat.Rows[i]["Dname"] = str;// DriDet.Rows[0][0].ToString();

                        }


                    }

                }
                stat.AcceptChanges();
                GV.DataSource = stat;
                GV.DataBind();         
            }
        }

        protected void btnalert_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                //DataGridItem dgi = (DataGridItem)((Button)sender).Parent.Parent;
                //string key = gdVehicles.DataKeyField[dgi.ItemIndex].ToString();
                // string address = ((LinkButton)dgi.FindControl("vehicleno")).Text;
                string[] alertstring = getalertdetailsfunction(vhno);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            Timer1.Enabled = true;

        }
        public string[] getalertdetailsfunction(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 

            DataTable vdt = db.vehicledetailsalerts(Vno);
            string vinsEd = vdt.Rows[0][0].ToString();
            string rtorewd = vdt.Rows[0][1].ToString();
            string dlicexd = vdt.Rows[0][2].ToString();
            string overspeed = vdt.Rows[0][3].ToString();
            string conttodate = vdt.Rows[0][4].ToString();
            string gprsad = vdt.Rows[0][5].ToString();
            string fcdate = vdt.Rows[0][6].ToString();
            string emsdate = vdt.Rows[0][7].ToString();
            string roadtax = vdt.Rows[0][8].ToString();
            //string[] stralert = new string[10];
            List<string> stralert = new List<string>();
            stralert.Add("VEHICLE RTO No:");
            stralert.Add("  ");
            DateTime spdate = DateTime.Now;
            spdate = spdate.AddDays(30);
            DateTime spdate1 = DateTime.Now.AddDays(3);
            //DataRow dr = alertdt.NewRow();
            int j = 0;
            var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
            if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Insurance will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Insurance DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Insurance Expired Today");
                    j++;
                }
            }
            if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle RTO Permit will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle RTO Permit Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle RTO Permit Expired Today");
                    j++;
                }
            }
            if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Driver Driving Licence Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Driver Driving Licence Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Driver Driving Licence Expired Today");
                    j++;
                }
            }
            if (overspeed != "0" && overspeed != "")
            {
                DataTable dtdevice = db.getdevicesid(Vno);
                int ovrspeed = Convert.ToInt32(overspeed);
                ovrspeed = Convert.ToInt32(ovrspeed / 1.85);
                string ovs = ovrspeed.ToString();
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, ovs);
                if (dtoverspeed.Rows.Count != 0)
                {
                    stralert.Add("Vehicle Over Speed..!");
                    j++;

                }
            }
            if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Contract Period Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Contract Period Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Contract Period Expired Today");
                    j++;
                }
            }
            if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 3 && s > 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle GPRS Recharge Expired Today");
                    j++;
                }
            }
            if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle FC Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle FC DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle FC Expired Today");
                    j++;
                }
            }
            if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Emmission Test Date Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Emmission Test DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Emmission Test Expired Today");
                    j++;
                }
            }
            if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Road Tax Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Road Tax DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Road Tax Expired Today");
                    j++;
                }
            }
            int len = stralert.Count;
            string[] salerts = new string[len];
            if (j != 0)
            {
                for (int m = 0, n = 1; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else if (m == 1)
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                    else
                    {
                        salerts[m] = n + " : " + stralert[m].ToString();
                        n++;
                    }
                }
            }
            else
            {
                if (lblno == 0)
                {
                    //lblmalerts.Visible = false;
                }
                stralert.Add("NO Alert For This Vehicle");
                len = stralert.Count;
                salerts = new string[len];
                for (int m = 0; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                }
            }
            return salerts;
        }

        protected void Btndalerts_Click(object sender, GridViewRowEventArgs e)
        {

        }
        protected void rbtn_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;

            if (Session["UserID"] != null)
            {
 
 
                DBClass db = new DBClass();
 
 
                RadioButton btn = (RadioButton)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                Session["selectedVehicle"] = vhno;
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["cUser_id"].ToString());
                if (status.Rows.Count > 0)
                {
                    Response.Redirect("Track.aspx?userid=" + Session["cUser_id"].ToString() + "&vehNo=" + vhno);
                    //Response.Redirect("ManagerTrack.aspx");
                }
                else
                {
                    Timer1.Enabled = true;
                    string message = "alert(' " + vhno + " is offline please chek connection ')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                    //string alertstring = " There is no present data...";
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));

                }

            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            List<string> stralerts = new List<string>();
            try
            {

 
 
                DBClass db = new DBClass();
 
 
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                try
                {
                    GeoCode objAddress = new GeoCode();
                    objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                    StringBuilder sb = new StringBuilder();
                    if (objAddress.valid)
                    {
                        sb.Append(objAddress.Placemark.address.ToString());
                        stralerts.Add("Vehicle no:  " + vhno);
                        stralerts.Add(" ");
                        stralerts.Add("Vehicle Location:  " + sb.ToString());
                    }
                    else
                        stralerts.Add("Not found");
                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

                string[] salerts = new string[stralerts.Count];

                for (int m = 0; m < stralerts.Count; m++)
                {

                    salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

        }

        protected void btnspeed_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                double speed = Convert.ToDouble(status.Rows[0][2].ToString());
                if (speed > 3)
                {
                    string sped = Convert.ToString(speed * 1.85);
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                    // DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);

                }
                else if (speed < 3)
                {
                    string sped = "0";
                    DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }

        protected void btnfuel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            List<string> stralerts = new List<string>();
            //try
            // {
 
 
            DBClass db = new DBClass();
 
 
            DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
            if (fuelim.Rows[0][1].ToString() == "Yes")
            {
                DataTable dt2 = db.prevfuelstaus(vhno);
                if (dt2.Rows.Count != 0)
                {
                    if (Convert.ToInt32(dt2.Rows[0][1].ToString()) == 1)
                    {
#pragma warning disable CS0168 // The variable 'fulestat' is declared but never used
                        double fulestat;
#pragma warning restore CS0168 // The variable 'fulestat' is declared but never used
                        DataTable dtf = db.GetVehiclesfuel(vhno, Session["UserId"].ToString());

                        double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                        double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                        int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());

                        if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
                        {

                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                stralerts.Add("Vehicle no:   " + vhno);
                                stralerts.Add(" ");
                                stralerts.Add("Fuel in Ltr:  " + b);
                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                //fulestat = (string_val_fuel) / (Fuel_Cali);
                                //fulestat = fulestat * tank_capacity;
                                //fulestat = tank_capacity - fulestat;
                                //fulestat = Math.Truncate(fulestat * 100) / 100;
                                stralerts.Add("Vehicle no:   " + vhno);
                                stralerts.Add(" ");
                                stralerts.Add("Fuel in Ltr:  " + f);
                            }
                        }
                        else
                        {
                            stralerts.Add("Fuel parameters are not calibrated..");

                        }
                    }

                }
                else stralerts.Add("Waiting...");
            }
            else
            {
                stralerts.Add(" Service Not Availed");
                //lblFuel.Text = " Service Not Availed";
                // string alertstring = " Fuel thing not implimented";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
            //catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }


        public double checkavailabe(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DateTime dt1 = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 12:00 AM");
            //DataTable VS = db.GetVehStat(Session["UserID"].ToString(), Vno);
            // DateTime dt2 = Convert.ToDateTime(VS.Rows[0][0].ToString());
            DateTime dt2 = Convert.ToDateTime(DateTime.Now.ToString());
            if (dt2 < dt1)
            {

                dt1 = Convert.ToDateTime(dt2.ToShortDateString() + " 12:00 AM");
            }
            TimeSpan ts = dt2 - dt1;
            DataTable dt = db.getblackspot1(Vno, dt1.ToString(), dt2.ToString());
            double sysdata = Convert.ToDouble(dt.Rows[0][0].ToString());
            double av = ((ts.TotalMinutes - sysdata) / ts.TotalMinutes) * 100;
            av = 100 - av;
            av = (double)Math.Round(av, 1);
            if (av < 90 && av > 0)
            {
                av = av + 5;
            }
            else if (av < 95 && av > 0)
            {
                av = av + 2;
            }
            else if (av < 98 && av > 0)
            {
                av = av + 1;
            }
            else if (av >= 100)
            {
                av = 100;
            }
            return av;
        }

        protected void btnavail_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                string avilable = checkavailabe(vhno).ToString();

                if (avilable != null)
                {
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Available:  " + avilable + "%");
                }
                else
                {
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Not found");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            Timer1.Enabled = true;
        }


        protected void btnKM_Click(object sender, EventArgs e)
        {
            try
            {

                Timer1.Enabled = false;

                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                tkmh(vhno);


                List<string> stralerts = new List<string>();
                try
                {
 
 
                    DBClass db = new DBClass();
 
 
                    //total km run
                    tkmh(vhno);
                    DataTable tktable = db.gettotalkm(vhno);
                    double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                    // DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
                    tktkm = Math.Truncate(tktkm * 100) / 100;

                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Today KM run:  " + tktkm);

                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

                string[] salerts = new string[stralerts.Count];

                for (int m = 0; m < stralerts.Count; m++)
                {

                    salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
                Timer1.Enabled = true;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

        }
        public void tkmh(string vno)
        {

 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.gettotalkm(vno);
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() != Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                    string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                    db.uptotalkm(vno, fdate, tdate, "0", fdate);
                }
                else if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() == Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    double tkm = Convert.ToDouble(dt.Rows[0][4].ToString());
                    string fdate = dt.Rows[0][2].ToString();
                    string fdate1 = dt.Rows[0][5].ToString();
                    string tdate = dt.Rows[0][3].ToString();
                    DataTable dt1 = db.Select_TotalRunkm11(vno, fdate, tdate);
                    //DataTable KMhike = db.Get_kmhike(Session["Username"].ToString());
                    double kmhike11 = 0;
                    //if (!DBNull.Value.Equals(KMhike.Rows[0][1]))
                    //{
                    //    kmhike11 = (Convert.ToDouble (KMhike.Rows[0][1].ToString()));
                    //}

                    if (dt1.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                        {
                            tkm += CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()), kmhike11);
                        }
                        fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    }
                    else if (dt1.Rows.Count == 0)
                    {
                        fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

                    }
                    db.uptotalkm(vno, fdate, tdate, tkm.ToString(), fdate);
                }
            }
            else
            {
                string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                db.settotalkm(vno, fdate, tdate, "0");
            }
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2, double hikekm)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) * (hikekm);
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void btntemp_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;

                string[] salerts = gettempalert(vhno);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            Timer1.Enabled = true;
        }
        public string[] gettempalert(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            List<string> stralerts = new List<string>();
            try
            {
                DataTable getreefer = db.Getreeferveh(Vno.ToString());
                if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                {
                    string Acsta = ac(Vno.ToString());
                    string temp = Temp(Vno.ToString());
                    string acontime1 = acontime(Vno.ToString());
                    stralerts.Add("Vehicle no:  " + Vno);
                    stralerts.Add(" ");
                    stralerts.Add("Reefer Temp:  " + temp + "°C");
                    stralerts.Add(" ");
                    stralerts.Add("AC Status:  " + Acsta);
                    stralerts.Add(" ");
                    stralerts.Add("Today AC working for:  " + acontime1);
                }
                else
                {
                    string Acsta = ac(Vno.ToString());
                    string temp = Temp(Vno.ToString());
                    string acontime1 = acontime(Vno.ToString());
                    stralerts.Add("Vehicle no:  " + Vno);
                    stralerts.Add(" ");
                    stralerts.Add("Temp:  " + temp + "°C");
                    stralerts.Add(" ");
                    stralerts.Add("AC Status:  " + Acsta);
                    stralerts.Add(" ");
                    stralerts.Add("Today AC working for:  " + acontime1);

                }

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }
            return salerts;

        }
        public string ac(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string ac = "";
            DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            DataTable acstat = db.Getacveh(Vno);

            DateTime dtst = Convert.ToDateTime(acstatus.Rows[0][4].ToString());
            if (acstat.Rows[0][1].ToString() == "On")
            {

                if (acstatus.Rows[0][6].ToString() == "0.00")//&& pdt.Rows[0][1].ToString() == "0.00")
                {
                    DataTable pdt = db.Getprevacstring(acstatus.Rows[0][11].ToString());
                    DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                    TimeSpan ts = dtst - dtst1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    days = m / 1440;
                    Hours = m / 60;
                    Minutes = m % 60;
                    ac = "ON since  " + Hours.ToString() + "  Hrs  " + Minutes.ToString() + "  Mins";

                }
                else
                {
                    DataTable acoff = db.Getacoff(acstatus.Rows[0][11].ToString());
                    DateTime acof = Convert.ToDateTime(acoff.Rows[0][0].ToString());//"dd-MM-yyyy hh:mm tt");
                    string acofftime = acof.ToString("dd-MM-yyyy hh:mm tt");
                    ac = "OFF " + "( at " + acofftime + ")";
                }
            }
            else
            {
                ac = "Service Not Availed";
            }
            return ac;

        }

        public string acontime(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string actime = "";
            string stime = DateTime.Now.ToShortDateString();
            string stime1 = stime + " 12:00 AM";
            string ttime = DateTime.Now.ToString();
            DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            DataTable acstat = db.Getacveh(Vno);
            DataTable account = db.getacontime(acstatus.Rows[0][11].ToString(), stime1, ttime);

            if (acstat.Rows[0][1].ToString() == "On")
            {

                if (account.Rows.Count != 0)
                {
                    int m = Convert.ToInt32(account.Rows[0][0].ToString());
                    days = m / 1440;
                    Hours = m / 60;
                    Minutes = m % 60;
                    actime = Hours.ToString() + "  Hrs  " + Minutes.ToString() + "  Mins";

                }
                else
                {
                    actime = "0 Hours";
                }
            }
            else
            {
                actime = "Service Not Availed";
            }
            return actime;

        }


        public string Temp(string Vno)
        {

 
 
            DBClass db = new DBClass();
 
 
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            DataTable status = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            string condition = status.Rows[0][9].ToString();
            double conditon1 = Convert.ToDouble(condition);
            int days = 0;
            int Hours = 0;
            int Minutes = 0;
            double temp = 0;
            string temp12 = "";
            if (condition != "0")
            {
                if (fre.Rows[0][1].ToString() == "Yes")
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString();

                            }
                        }

                        else if (cond1 < 1000)
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            temp12 = temp.ToString();
                        }

                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());

                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            temp12 = "-" + temp.ToString();

                        }
                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();
                    }

                }


                else
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString();

                            }
                        }
                        else if (cond1 < 1000)
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            temp12 = temp.ToString();
                        }


                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());

                        // DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        // TimeSpan ts = dtst - dtst1;
                        // int m = Convert.ToInt32(ts.TotalMinutes);
                        // days = m / 1440;
                        //Hours = m / 60;
                        // Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = "-" + temp.ToString();


                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();

                    }

                }
            }
            return temp12;


        }
        //protected void Btndalerts_Click(object sender, EventArgs e)

        //{
        //    Timer1.Enabled = true;
        //    if ((lblalerts2.Text != "") || (lbloffalerts.Text != ""))
        //    {
        //        DBClass db = new DBClass();
        //        DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
        //        string stralert = "";
        //        string stralert1 = "";

        //        if (lblalerts2.Text != "")
        //        {
        //            stralert = "Daily Live Alerts ,";
        //        }
        //        if (lbloffalerts.Text != "")
        //        {
        //            stralert1 = ",,, Offline More than 6 Hr ,";
        //        }
        //        stralert += lblalerts2.Text;
        //        string stralert2 = stralert.Replace("||", ",");
        //        stralert2 = stralert2.Replace("<span style=\"color:Red;\">", "");
        //        stralert2 = stralert2.Replace("</span>", "");

        //        stralert1 += lbloffalerts.Text;
        //        string stralert3 = stralert1.Replace("||", ",");
        //        stralert3 = stralert3.Replace("<span style=\"color:Red;\">", "");
        //        stralert3 = stralert3.Replace("</span>", "");
        //        stralert2 = stralert2 + stralert3;

        //        string[] results = stralert2.Split(new[] { ',' });
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", results) + "'.split(','));", true);
        //        Timer1.Enabled = true;
        //    }

        //}


        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl = (Label)e.Row.FindControl("lblvehicleno");
 
 
                DBClass db = new DBClass();
 
 
                DataTable dts = db.GetStatus(lbl.Text);
                Label myLabel1 = (Label)e.Row.FindControl("lblstatus");
                DataTable VS = db.GetVehStat(Session["UserID"].ToString(), lbl.Text);
                if (VS.Rows.Count > 0)
                {
                    DataTable status = db.Select_GetVehiclePresentlocation(lbl.Text, Session["UserID"].ToString());
                    int gps = Convert.ToInt32(status.Rows[0][7].ToString());
                    DateTime sysDT = DateTime.Now;
                    DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                    DateTime dayindiffence = StringDT.AddHours(72);
                    StringDT = StringDT.AddMinutes(360);
                    if (sysDT > dayindiffence)
                    {
                        myLabel1.ToolTip = "Call Customer Service No:08026545588";//"  ONLINE &nbsp;  ?";

                    }
                    else if (sysDT > StringDT)
                    {
                        myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                    }
                    else if (sysDT <= StringDT)
                    {
                        myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                    }
                    if (gps < 1)
                    {

                        myLabel1.ToolTip = "No GPS is Available";
                    }
                }
                else
                {
                    DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), lbl.Text);
                    DataTable statusoff = db.GetVehiclePresentlocation1(lbl.Text, Session["UserID"].ToString());
                    int gps = Convert.ToInt32(statusoff.Rows[0][7].ToString());
                    DateTime sysDT = DateTime.Now;
                    DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                    DateTime dayindiffence = StringDT.AddHours(72);
                    StringDT = StringDT.AddMinutes(360);
                    if (sysDT > dayindiffence)
                    {
                        myLabel1.ToolTip = "Call Customer Service No:08026545588";//"  ONLINE &nbsp;  ?";

                    }
                    else if (sysDT > StringDT)
                    {
                        myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                    }
                    else if (sysDT <= StringDT)
                    {
                        myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                    }
                    if (gps < 1)
                    {

                        myLabel1.ToolTip = "No GPS is Available";
                    }
                }
                Image img = (Image)e.Row.FindControl("Simg");
                if (dts.Rows.Count > 0)
                {
                    if (dts.Rows[0][1].ToString() == "0")
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    }
                    if (myLabel1.Text == "Offline")
                    {
                        e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
                        DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
                        DateTime pdt = DateTime.Now;
                        TimeSpan ts = pdt - sdt;
                        if (ts.TotalHours >= 24)
                        {
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
                        }
                    }

                }
                else if (dts.Rows.Count <= 0)
                {
                    DataTable dtsa = db.GetStatusoffff(lbl.Text);
                    if (dtsa.Rows.Count > 0)
                    {
                        if (dtsa.Rows[0][1].ToString() == "0")
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                        }
                        else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) > 3)
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                        }
                        else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) <= 3)
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                        }
                        if (myLabel1.Text == "Offline")
                        {
                            e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
                            DateTime sdt = Convert.ToDateTime(dtsa.Rows[0][2].ToString());
                            DateTime pdt = DateTime.Now;
                            TimeSpan ts = pdt - sdt;
                            if (ts.TotalHours >= 24)
                            {
                                e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
                            }
                        }
                    }

                }
                else
                {

                }
            }
        }
        protected void btnlovs_Click(object sender, EventArgs e)
        {
            try
            {
                Timer1.Enabled = false;
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;

                List<string> stralerts = new List<string>();
                try
                {
                    int j = 0;
                    string ovss;
 
 
                    DBClass db = new DBClass();
 
 
                    stralerts.Add("Vehicle no:  ");// + vhno);

                    DataTable vdt = db.vehicledetailsalerts(vhno);
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                        // lblovs.Text = "(" + speed1.ToString() + " limit)";
                        speed1 = Convert.ToInt32(speed1 / 1.85);
                        string overspeed = speed1.ToString();
                        DataTable dtdevice = db.getdevicesid(vhno);
                        string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                        string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                        string overspeeddate2 = overspeeddate + " " + "11:59 PM";

                        DataTable dtoverspeed = db.getoverspeeddetails11(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                        if (dtoverspeed.Rows.Count != 0)
                        {
                            for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                            {
                                j++;
                                double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[i][0].ToString());
                                overspeed1 = overspeed1 * 1.85;
                                overspeed1 = (double)Math.Round(overspeed1);
                                string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][1]).ToShortTimeString();

                                ovss = Convert.ToString(overspeed1) + " " + "KM/h" + "  at  " + " " + ovsdate;
                                stralerts.Add(ovss);
                            }
                        }
                        else
                        {
                            //stralerts.Add("Vehicle no:  " + vhno);
                            // stralerts.Add(" ");
                            stralerts.Add("No Over Speed");
                        }
                    }
                    else
                    {
                        //stralerts.Add("Vehicle no:  " + vhno);
                        // stralerts.Add(" ");
                        stralerts.Add("No Over Speed");
                    }

                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

                string[] salerts = new string[stralerts.Count];

                for (int m = 0, n = 1; m < stralerts.Count; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralerts[m].ToString() + vhno;
                    }
                    else
                    {
                        salerts[m] = n + " " + " : " + stralerts[m].ToString();
                        //stralerts.Add(" ");
                        n++;

                    }
                    //salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
                Timer1.Enabled = true;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

        }
       //protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
       // {
       //     if (e.Row.RowType == DataControlRowType.DataRow)
       //     {
       //         e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
       //         e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
       //     }
       //     if (e.Row.RowType == DataControlRowType.DataRow)
       //     {
       //         Label lbl = (Label)e.Row.FindControl("lblvehicleno");
       //         DBClass db = new DBClass();
       //         DataTable dts = db.GetStatus(lbl.Text);
       //         Label myLabel1 = (Label)e.Row.FindControl("lblstatus");
       //         Image img = (Image)e.Row.FindControl("Simg");
       //         if (dts.Rows.Count > 0)
       //         {
       //             if (dts.Rows[0][1].ToString() == "0")
       //             {
       //                 img.ImageUrl = "http://google.com/mapfiles/ms/micons/red.png";
       //             }
       //             else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
       //             {
       //                 img.ImageUrl = "http://google.com/mapfiles/ms/micons/green.png";
       //             }
       //             else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
       //             {
       //                 img.ImageUrl = "http://google.com/mapfiles/ms/micons/blue.png";
       //             }
       //             if (myLabel1.Text == "Offline")
       //             {
       //                 e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
       //                 DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
       //                 DateTime pdt = DateTime.Now;
       //                 TimeSpan ts = pdt - sdt;
       //                 if (ts.TotalHours >= 24)
       //                 {
       //                     e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
       //                 }
       //             }

       //         }
       //         else if (dts.Rows.Count <= 0)
       //         {
       //             DataTable dtsa = db.GetStatusoffff(lbl.Text);
       //             if (dtsa.Rows.Count > 0)
       //             {
       //                 if (dtsa.Rows[0][1].ToString() == "0")
       //                 {
       //                     img.ImageUrl = "http://google.com/mapfiles/ms/micons/red.png";
       //                 }
       //                 else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) > 3)
       //                 {
       //                     img.ImageUrl = "http://google.com/mapfiles/ms/micons/green.png";
       //                 }
       //                 else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) <= 3)
       //                 {
       //                     img.ImageUrl = "http://google.com/mapfiles/ms/micons/blue.png";
       //                 }
       //                 if (myLabel1.Text == "Offline")
       //                 {
       //                     e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
       //                     DateTime sdt = Convert.ToDateTime(dtsa.Rows[0][2].ToString());
       //                     DateTime pdt = DateTime.Now;
       //                     TimeSpan ts = pdt - sdt;
       //                     if (ts.TotalHours >= 24)
       //                     {
       //                         e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
       //                     }
       //                 }
       //             }

       //         }
       //         else
       //         {

       //         }
       //     }
       //     //if (e.Row.RowType == DataControlRowType.DataRow)
       //     //{
       //     //    e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='LightGreen';");
       //     //    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;");
       //     //    LinkButton mylink = (LinkButton)e.Row.FindControl("vehicleno");
       //     //    DBClass db = new DBClass();
       //     //    DataTable dts = db.GetStatus(mylink.Text);
       //     //    Label myLabel1 = (Label)e.Row.FindControl("label1");
       //     //    Image img = (Image)e.Row.FindControl("Simg");
       //     //    if (dts.Rows.Count > 0)
       //     //    {
       //     //        if (dts.Rows[0][1].ToString() == "0")
       //     //        {
       //     //            img.ImageUrl = "~/Images/RedPoint.JPG";
       //     //        }
       //     //        else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
       //     //        {
       //     //            img.ImageUrl = "~/Images/GreenPoint.JPG";
       //     //        }
       //     //        else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
       //     //        {
       //     //            img.ImageUrl = "~/Images/BluePoint.JPG";
       //     //        }
       //     //        Button btn1 = (Button)e.Row.FindControl("btnalerts");
       //     //        if (btn1.Text == "0")
       //     //        {
       //     //            btn1.BackColor = System.Drawing.Color.Red;
       //     //            btn1.BorderColor = System.Drawing.Color.Red;
       //     //            btn1.Text = "Alerts";
       //     //        }
       //     //        else
       //     //        {
       //     //            btn1.BackColor = System.Drawing.Color.White;
       //     //            btn1.BorderColor = System.Drawing.Color.White;
       //     //            btn1.Text = "Alerts";
       //     //        }
       //     //        if (myLabel1.Text == "Offline")
       //     //        {
       //     //            e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
       //     //            DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
       //     //            DateTime pdt = DateTime.Now;
       //     //            TimeSpan ts = pdt - sdt;
       //     //            if (ts.TotalHours >= 6)
       //     //            {
       //     //                e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
       //     //            }
       //     //        }
       //     //    }
       //     //    else
       //     //    {
       //     //        DataTable dtsa = db.GetStatusoffff(mylink.Text);
       //     //        if (dtsa.Rows.Count > 0)
       //     //        {
       //     //            if (dtsa.Rows[0][1].ToString() == "0")
       //     //            {
       //     //                img.ImageUrl = "~/Images/RedPoint.JPG";
       //     //            }
       //     //            else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) > 3)
       //     //            {
       //     //                img.ImageUrl = "~/Images/GreenPoint.JPG";
       //     //            }
       //     //            else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) <= 3)
       //     //            {
       //     //                img.ImageUrl = "~/Images/BluePoint.JPG";
       //     //            }
       //     //            Button btn1 = (Button)e.Row.FindControl("btnalerts");
       //     //            if (btn1.Text == "0")
       //     //            {
       //     //                btn1.BackColor = System.Drawing.Color.Red;
       //     //                btn1.BorderColor = System.Drawing.Color.Red;
       //     //                btn1.Text = "Alerts";
       //     //            }
       //     //            else
       //     //            {
       //     //                btn1.BackColor = System.Drawing.Color.White;
       //     //                btn1.BorderColor = System.Drawing.Color.White;
       //     //                btn1.Text = "Alerts";
       //     //            }
       //     //            if (myLabel1.Text == "Offline")
       //     //            {
       //     //                e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
       //     //                DateTime sdt = Convert.ToDateTime(dtsa.Rows[0][2].ToString());
       //     //                DateTime pdt = DateTime.Now;
       //     //                TimeSpan ts = pdt - sdt;
       //     //                if (ts.TotalHours >= 6)
       //     //                {
       //     //                    e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
       //     //                }
       //     //            }

       //     //        }
       //     //    }
       //     //}
       // }
        //protected void vehicleno_Click(object sender, EventArgs e)
        //{
        //    if (Session["UserID"] != null)
        //    {
        //        LinkButton bt = (LinkButton)sender;
        //        Session["selectedVehicle"] = bt.Text;
        //        Response.Redirect("ManagerTrack.aspx");
        //    }
        //}
        //protected void btnalerts_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Button rb = sender as Button;
        //        GridViewRow gvr = rb.NamingContainer as GridViewRow;
        //        string address = GV.DataKeys[gvr.RowIndex].Value.ToString();
        //        string[] alertstring = getalertdetailsfunction(address);
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
        //    }
        //    catch (Exception ex) { }
        //}
        //public string[] getalertdetailsfunction(string Vno)
        //{
        //    string[] salerts = new string[1];
        //    try
        //    {
        //        DBClass db = new DBClass();
        //        DataTable vdt = db.vehicledetailsalerts(Vno);
        //        string vinsEd = vdt.Rows[0][0].ToString();
        //        string rtorewd = vdt.Rows[0][1].ToString();
        //        string dlicexd = vdt.Rows[0][2].ToString();
        //        string overspeed = vdt.Rows[0][3].ToString();
        //        string conttodate = vdt.Rows[0][4].ToString();
        //        string gprsad = vdt.Rows[0][5].ToString();
        //        string fcdate = vdt.Rows[0][6].ToString();
        //        string emsdate = vdt.Rows[0][7].ToString();
        //        string roadtax = vdt.Rows[0][8].ToString();
        //        //string[] stralert = new string[10];
        //        List<string> stralert = new List<string>();
        //        stralert.Add("        VEHICLE RTO No : ");
        //        stralert.Add("  ");
        //        DateTime spdate = DateTime.Now;
        //        spdate = spdate.AddDays(30);
        //        DateTime spdate1 = DateTime.Now.AddDays(3);
        //        //DataRow dr = alertdt.NewRow();
        //        int j = 0;
        //        var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };

        //        if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle Insurance will be Expired in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle Insurance DATE Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle Insurance Expired Today");
        //                j++;
        //            }
        //        }
        //        if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle RTO Permit will be Expired in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle RTO Permit Date Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle RTO Permit Expired Today");
        //                j++;
        //            }
        //        }
        //        if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
        //        {

        //            TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Driver Driving Licence Will be Expired in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Driver Driving Licence Date Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Driver Driving Licence Expired Today");
        //                j++;
        //            }
        //        }
        //        if (overspeed != "0" && overspeed != "")
        //        {
        //            DataTable dtdevice = db.getdevicesid(Vno);
        //            int ovrspeed = Convert.ToInt32(overspeed);
        //            ovrspeed = Convert.ToInt32(ovrspeed / 1.85);
        //            string ovs = ovrspeed.ToString();
        //            string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
        //            string overspeeddate1 = overspeeddate + " " + "12:00 AM";
        //            string overspeeddate2 = overspeeddate + " " + "11:59 PM";
        //            DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, ovs);
        //            if (dtoverspeed.Rows.Count != 0)
        //            {
        //                stralert.Add("Vehicle Over Speed.! see details below");
        //                j++;
        //            }
        //        }
        //        if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle Contract Period Will be Expired in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle Contract Period Date Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle Contract Period Expired Today");
        //                j++;
        //            }
        //        }
        //        if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 3 && s > 0)
        //            {
        //                stralert.Add("Vehicle GPRS Recharge DATE Will Expire in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle GPRS Recharge DATE Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle GPRS Recharge Expired Today");
        //                j++;
        //            }
        //        }
        //        if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle FC Will Expire in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle FC DATE Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle FC Expired Today");
        //                j++;
        //            }
        //        }
        //        if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle Emmission Test Date Will Expire in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle Emmission Test DATE Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle Emmission Test Expired Today");
        //                j++;
        //            }
        //        }
        //        if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
        //        {
        //            TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
        //            int s = Convert.ToInt32(span.Days);
        //            if (s <= 30 && s > 0)
        //            {
        //                stralert.Add("Vehicle Road Tax Will Expire in " + s + " days");
        //                j++;
        //            }
        //            else if (s < 0)
        //            {
        //                stralert.Add("Vehicle Road Tax DATE Expired");
        //                j++;
        //            }
        //            else if (s == 0)
        //            {
        //                stralert.Add("Vehicle Road Tax Expired Today");
        //                j++;
        //            }
        //        }
        //        int len = stralert.Count;
        //        salerts = new string[len];
        //        if (j != 0)
        //        {
        //            for (int m = 0, n = 1; m < len; m++)
        //            {
        //                if (m == 0)
        //                {
        //                    salerts[m] = stralert[m].ToString() + Vno;
        //                }
        //                else if (m == 1)
        //                {
        //                    salerts[m] = stralert[m].ToString();
        //                }
        //                else
        //                {
        //                    salerts[m] = n + " : " + stralert[m].ToString();
        //                    n++;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            stralert.Add("NO Alert For This Vehicle");
        //            len = stralert.Count;
        //            salerts = new string[len];
        //            for (int m = 0; m < len; m++)
        //            {
        //                if (m == 0)
        //                {
        //                    salerts[m] = stralert[m].ToString() + Vno;
        //                }
        //                else
        //                {
        //                    salerts[m] = stralert[m].ToString();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex) { }
        //    return salerts;
        //}
        //protected void rbtn_Click(object sender, EventArgs e)
        //{
        //    if (Session["UserID"] != null)
        //    {
        //        DBClass db = new DBClass();
        //        RadioButton rb = sender as RadioButton;
        //        GridViewRow gvr = rb.NamingContainer as GridViewRow;
        //        string address = GV.DataKeys[gvr.RowIndex].Value.ToString();
        //        Session["selectedVehicle"] = address;
        //        DataTable status = db.GetVehiclePresentlocation(address, Session["UserID"].ToString());
        //        if (status.Rows.Count > 0)
        //        {
        //            Response.Redirect("ManagerTrack.aspx");
        //        }
        //        else
        //        {
        //            Timer1.Enabled = true;
        //            string message = "alert(' " + address + " is offline please chek connection ')";
        //            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
        //        }
        //    }
           
        //}
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Vehicle_Bind();
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Changepassword : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbluname.Text = Session["UserName"].ToString();
            }

        }

        protected void Btn_submit_Click(object sender, EventArgs e)
        {
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                db.ChangePW(Session["UserID"].ToString(), Session["UserName"].ToString(), txtcp.Text.Trim());
                txtop.Attributes.Add("value", "");
                txtop.Text = string.Empty;
                txtnp.Text = string.Empty;
                txtcp.Text = string.Empty;
                refresh();
            }
        }
        public void refresh()
        {
            Session["timerc"] = null;
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Response.Redirect("Login.aspx");
        }
        protected void txtop_TextChanged(object sender, EventArgs e)
        {
            if (lblUser.Text == "Enter Correct Password")
            {
                lblUser.Text = "";
                lblUser.Visible = false;

            }
            string pw = txtop.Text;
            DataTable userdt = db.CheckUser(Session["UserName"].ToString(), txtop.Text.Trim());
            if (userdt.Rows.Count == 0)
            {
                lblUser.Visible = true;
                lblUser.Text = "Enter Correct Password";

            }
            else txtop.Attributes.Add("value", pw);

        }

        protected void Btn_logout_Click(object sender, EventArgs e)
        {
            Session["timerc"] = null;
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Response.Redirect("Login.aspx");
        }

    }
}

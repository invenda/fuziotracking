﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    //public partial class TrackingMaster : System.Web.UI.MasterPage
    //{
    //    public bool MenuVisible
    //    {
    //        get
    //        {
    //           return Menu2.Visible;
    //        }
    //        set
    //        {
    //            Menu2.Visible = value;
    //            Menu1.Visible = !value;
    //        }
    //    }
    //    protected void Page_Load(object sender, EventArgs e)
    //    {
            
    //    }

    //    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    //    {
    //        if (Menu1.SelectedItem.Text == "CreateUsers")
    //            Response.Redirect("CreateUsers.aspx");
    //        if (Menu1.SelectedItem.Text == "Create Devices")
    //            Response.Redirect("CreateDevices.aspx");
    //        if (Menu1.SelectedItem.Text == "Assign Devices To Cutomers")
    //            Response.Redirect("AssignDevices.aspx");           
    //    }
    //    protected void Menu2_MenuItemClick(object sender, MenuEventArgs e)
    //    {
    //        if (Menu2.SelectedItem.Text == "CreateUsers")
    //            Response.Redirect("");           
    //        if (Menu2.SelectedItem.Text == "Assign Devices To Vehicles")
    //            Response.Redirect("../Customer/AssignDevicesToVehicles.aspx");
    //        if (Menu2.SelectedItem.Text == "Create Vehicles")
    //            Response.Redirect("../Customer/CreateVehicles.aspx"); 
    //    }
    //    protected void lbtnLogOut_Click(object sender, EventArgs e)
    //    {
    //        Session["UserRole"] = null;
    //        Session["UserID"] = null;
    //        Response.Redirect("../Login.aspx"); 
    //    }
    //}
    public partial class TrackingMaster : System.Web.UI.MasterPage
    {
        public bool MenuVisible
        {
            get
            {
                return Menu2.Visible = false;
            }
            set
            {
                Menu2.Visible = false;//value;
                Menu1.Visible = false;//!value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string User = "1";

                if (Session["UserRole"].ToString() == User)
                {

                    Menu2.Visible = false;
                    Menu1.Visible = true;

                }
            }

        }

        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (Menu1.SelectedItem.Text == "CreateUsers")
                Response.Redirect("CreateUsers.aspx");
            if (Menu1.SelectedItem.Text == "Create Devices")
                Response.Redirect("CreateDevices.aspx");
            if (Menu1.SelectedItem.Text == "Assign Devices To Cutomers")
                Response.Redirect("AssignDevices.aspx");
        }
        protected void Menu2_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (Menu2.SelectedItem.Text == "CreateUsers")
                Response.Redirect("");
            if (Menu2.SelectedItem.Text == "Assign Devices To Vehicles")
                Response.Redirect("../AssignDevicesToVehicles.aspx");
            if (Menu2.SelectedItem.Text == "Create Vehicles")
                Response.Redirect("../Customer/CreateVehicles.aspx");
        }
        protected void lbtntracking_Click(object sender, EventArgs e)
        {
            Response.Redirect("Track.aspx?username=" + Session["UserName"].ToString());
        }
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Response.Redirect("Login.aspx");
        }
    }
}

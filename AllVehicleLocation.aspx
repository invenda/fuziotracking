﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllVehicleLocation.aspx.cs"
    Inherits="Tracking.AllVehicleLocation" MasterPageFile="~/Manager_geo.Master" %>

<asp:Content ContentPlaceHolderID="MainContent" ID="CPH1" runat="server">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        // SCRIPT FOR THE MOUSE EVENT.
        function MouseEvents(objRef, evt) {
            if (evt.type == "mouseover") { objRef.style.cursor = 'pointer'; objRef.style.backgroundColor = "Skyblue"; }
            else { if (evt.type == "mouseout") objRef.style.backgroundColor = "#CCFF99"; }
        }
    </script>
    <div style="height: 40px; text-align: center; vertical-align: middle; font-style: italic;
        font-size: x-large; color: Blue;">
         Location Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click"
            Enabled="false" />
    </div>
    <div style="position: relative; vertical-align: top; height: auto; overflow: hidden;
        background-color: White; top: 0px; left: 0px;">
        <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" BackColor="White"
            HorizontalAlign="Center" Width="60%" BorderColor="#FF33CC" ForeColor="Black"
            meta:resourcekey="GVResource1" Font-Bold="False" ShowFooter="True" 
            onrowcreated="GV_RowCreated">
            <RowStyle Height="5px" />
            <AlternatingRowStyle Height="5px" BackColor="#66FFFF" />
            <Columns>
                <asp:TemplateField HeaderText="User Name" meta:resourcekey="TemplateFieldResource2">
                    <ItemTemplate>
                        <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("Username") %>'
                            Font-Bold="false" meta:resourcekey="lblvehiclenoResource1"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="80px" />
                    <ItemStyle HorizontalAlign="Left" Font-Bold="False" Width="70px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vehicle no" meta:resourcekey="TemplateFieldResource2">
                    <ItemTemplate>
                        <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("Vno") %>'
                            Font-Bold="false" meta:resourcekey="lblvehiclenoResource1"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="80px" />
                    <ItemStyle HorizontalAlign="Right" Font-Bold="False" Width="80px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Device no" meta:resourcekey="TemplateFieldResource5">
                    <ItemTemplate>
                        <asp:Label ID="lblserial" Text='<%# Bind("Deviceno") %>' runat="server" Font-Bold="false"
                            meta:resourcekey="lblserialResource1"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="60px" />
                    <ItemStyle HorizontalAlign="Right" Width="60px" />
                </asp:TemplateField>
                <%-- <asp:TemplateField HeaderText="Last Data" meta:resourcekey="TemplateFieldResource6">
                    <ItemTemplate>
                        <asp:Label ID="lbllastdata" Text='<%# Bind("Lastdata") %>' runat="server" Font-Bold="false"
                            meta:resourcekey="lbllastdataResource1"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Location" meta:resourcekey="TemplateFieldResource7">
                    <ItemTemplate>
                        <asp:Label ID="lblgps" Text='<%# Bind("Location") %>' runat="server" Font-Bold="false"
                            meta:resourcekey="lblgpsResource1"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="300px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#66CCFF" />
            <HeaderStyle BackColor="#99CCFF" ForeColor="Black" Height="3px" 
               />
        </asp:GridView>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateVehicles.aspx.cs"
    Inherits="Tracking.Admin.CreateVehicles" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
        <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0; text-align: center;">
            <h1>
                Vehicle Registration Details
            </h1>
        </div>
    </div>
    <div style="width: 1100px; margin: 0 auto; padding: 0; padding: 0;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                </td>
            </tr>
            <tr>
                <td style="color: Black; font-size: large; width: 178px;">
                    Vehicle Details
                </td>
                <td style="width: 332px">
                </td>
                <td style="color: Black; font-size: large; width: 183px;">
                    Driver Details
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle Registered Name:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtVehRegName" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtVehRegName"
                        runat="server" ErrorMessage="Enter Vehicle Registered Name">*</asp:RequiredFieldValidator>
                </td>
                <td style="width: 183px">
                    Driver Name:
                </td>
                <td>
                    <asp:TextBox ID="txtDriverName" runat="server" Height="15px" Width="150px" 
                        MaxLength="13"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDriverName"
                        runat="server" ErrorMessage="Enter Driver Name">*</asp:RequiredFieldValidator>
                </td>
                <tr>
                    <td style="width: 178px">
                    </td>
                    <td style="width: 332px">
                        State Code&nbsp;&nbsp; District Code&nbsp;&nbsp;&nbsp;&nbsp;Plate Number
                    </td>
                    <td style="width: 183px">
                    </td>
                    <td>
                    </td>
                </tr>
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle RTO Number
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtscode" runat="server" Height="15px" Width="30px" MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtscode"
                        runat="server" ErrorMessage="Enter State Code">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtscode"
                        ErrorMessage="Enter Alphabet Only!" ValidationExpression="^[A-Z]+$">*</asp:RegularExpressionValidator>
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtdcode" runat="server" Height="15px" Width="30px"
                        MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtdcode"
                        runat="server" ErrorMessage="Enter District Code">*</asp:RequiredFieldValidator>
                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtdcode"
                        ErrorMessage="Enter numbers only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtn" runat="server" 
                        Height="15px" Width="25px" MaxLength="2"></asp:TextBox>
                    <asp:TextBox ID="txtnumber" runat="server" Height="15px" Width="50px" AutoPostBack="true"
                        MaxLength="4" OnTextChanged="txtnumber_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtnumber"
                        runat="server" ErrorMessage="Enter Plate Number">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtnumber"
                        ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                    <%-- <asp:TextBox ID="txtVehicleNo" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtVehicleNo"
                        runat="server" ErrorMessage="Enter Registration Number">*</asp:RequiredFieldValidator>--%>
                </td>
                <td style="width: 183px">
                    Driver Mobile Number:
                </td>
                <td>
                    <asp:TextBox ID="txtDriverMobNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtDriverMobNum"
                        runat="server" ErrorMessage="Enter Driver Mobile Number">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDriverMobNum"
                        ErrorMessage="Please enter valid Mobile number!" ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    (Capital Letters)
                </td>
                <td style="width: 332px">
                    <asp:Label ID="lblchkvno" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td style="width: 183px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px; height: 48px;">
                    Vehicle Model:
                </td>
                <td style="width: 332px; height: 48px;">
                    <asp:TextBox ID="txtVehModel" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtVehModel"
                        runat="server" ErrorMessage="Enter Vehicle Model">*</asp:RequiredFieldValidator>
                </td>
                <td style="height: 48px; width: 183px">
                    Address:
                </td>
                <td style="height: 48px">
                    <asp:TextBox ID="txtDriverADD" runat="server" Height="50px" Width="150px" TextMode="MultiLine"
                        MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtDriverADD"
                        runat="server" ErrorMessage="Enter Driver Address">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle Make:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtVehMake" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtVehMake"
                        runat="server" ErrorMessage="Enter Vehicle Make">*</asp:RequiredFieldValidator>
                </td>
                <td style="width: 183px">
                    Driving Licence Number:
                </td>
                <td>
                    <asp:TextBox ID="txtDrivLicNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtDrivLicNum"
                        runat="server" ErrorMessage="Enter Driving Licence Number">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle Category
                </td>
                <td style="width: 332px">
                    <asp:DropDownList ID="ddlvc" runat="server">
                        <asp:ListItem Text="Truck & Vans" Value="Truck & Vans"></asp:ListItem>
                        <asp:ListItem Text="Rented/Hired Vehicle" Value="Rented/Hired Vehicle"></asp:ListItem>
                        <asp:ListItem Text="Public Passenger" Value="Public Passenger"></asp:ListItem>
                        <asp:ListItem Text="Tourist Vehicles & Taxis" Value="Tourist Vehicles & Taxis"></asp:ListItem>
                        <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                        <asp:ListItem Text="Three Wheeler" Value="Three Wheeler"></asp:ListItem>
                         <asp:ListItem Text="Reefer Vehicles" Value="Reefer Vehicles"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 183px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle Insurance Number:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtVehInsNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtVehInsNum"
                        runat="server" ErrorMessage="Enter Vehicle Licence Number">*</asp:RequiredFieldValidator>
                </td>
               <td style="color: Black; font-size: large; width: 178px;">
                    SIM Details
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    Existing Meter Reading:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtExistMetRead" runat="server" Height="15px" Width="150px" MaxLength="6"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="txtExistMetRead"
                        runat="server" ErrorMessage="Enter Existing Meter Reading">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtExistMetRead"
                        runat="server" ErrorMessage="Please enter valid Existing Meter Reading" ValidationExpression="[0-9]*\.?[0-9]*">*</asp:RegularExpressionValidator>
                </td>
                 <td style="width: 178px">
                    Mobile Number:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtMobNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ControlToValidate="txtMobNum"
                        runat="server" ErrorMessage="Enter Mobile Number">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtMobNum"
                        ErrorMessage="Please enter valid Mobile number!" ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
               
            </tr>
            <tr>
                <td style="height: 36px; width: 178px;">
                    Kilometer Pulse:
                </td>
                <td style="height: 36px; width: 332px;">
                    <asp:TextBox ID="txtKiloPulse" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="txtKiloPulse"
                        runat="server" ErrorMessage="Enter Kilometer Pulse">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtKiloPulse"
                        ErrorMessage="Enter a valid Kilometer Pulse" ValidationExpression="\d{3,5}">*</asp:RegularExpressionValidator>
                </td>
                <td style="width: 178px">
                    SIM Provider:
                </td>
                <td style="height: 2px; width: 332px;">
                    <asp:TextBox ID="txtSIMProvider" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ControlToValidate="txtSIMProvider"
                        runat="server" ErrorMessage="Enter SIM Provider">*</asp:RequiredFieldValidator>
                </td>
               
               
            </tr>
            <tr>
                <td style="height: 23px; width: 178px;">
                    RTO Devision/Name:
                </td>
                <td style="height: 23px; width: 332px;">
                    <asp:TextBox ID="txtrtoname" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <br />
                </td>
                <td style="width: 178px">
                    GPRS Expiry Date:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtGPRSActDt" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ControlToValidate="txtGPRSActDt"
                        runat="server" ErrorMessage="Enter GPRS Activated Date">*</asp:RequiredFieldValidator>
                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtGPRSActDt">
                    </cc1:CalendarExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                        ControlToValidate="txtGPRSActDt" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$">*</asp:RegularExpressionValidator>
                        </td> 
                  
               
                
            </tr>
            <tr>
                <td style="width: 178px">
                   Fule Tank capacity</td>
                <td style="width: 332px">
                <asp:TextBox ID="txtTankCapacity" runat="server" Height="15px" Width="150px"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtTankCapacity"
                        runat="server" ErrorMessage="Enter Fuel tank capacity">*</asp:RequiredFieldValidator>
                    </td>
                     <td style="width: 178px">
                    Recharge Period:
                </td>
                <td style="width: 332px">
                    <asp:RadioButton ID="rbtnMonthly" runat="server" Text="Monthly" GroupName="Group2">
                    </asp:RadioButton>
                    <asp:RadioButton ID="rbtnYearly" runat="server" Text="Yearly" GroupName="Group2">
                    </asp:RadioButton>
                </td>
               
            </tr>
             <tr>
                <td style="width: 178px">
                    Battery Voltage Capacity:
                    </td>
                <td style="width: 332px">
                     <asp:RadioButton ID="rbtn12V" runat="server" Text="12V" Value="12" GroupName="Group1">
                    </asp:RadioButton>
                    <asp:RadioButton ID="rbtn24V" runat="server" Text="24V" Value="24" GroupName="Group1">
                    </asp:RadioButton>
                </td>
                 <td style="width: 178px">
                    &nbsp;
                </td>
                <td style="width: 332px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
               
            </tr>
            <tr>
                <td style="width: 178px">
                    Vehicle Milage /liter
                    </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtmilage" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="txtmilage"
                        runat="server" ErrorMessage="Enter Vehicle Milage">*</asp:RequiredFieldValidator>
                </td>
                 <td style="width: 178px">
                    &nbsp;
                </td>
                <td style="width: 332px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
               
            </tr>
            <tr>
                <td style="width: 183px">
                    Fuel Type
                </td>
                <td>
                    <asp:DropDownList ID="ddlpt" runat="server">
                        <asp:ListItem>Petrol</asp:ListItem>
                        <asp:ListItem>Diesel</asp:ListItem>
                        <asp:ListItem>LPG</asp:ListItem>
                    </asp:DropDownList>
                </td>
                 <td style="width: 178px">
                    &nbsp;
                </td>
                <td style="width: 332px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
               
            </tr>
            
              <tr>
                <td style="width: 178px">
                    &nbsp;
                </td>
                <td style="width: 332px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                 <td style="width: 178px">
                    &nbsp;
                </td>
                <td style="width: 332px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
               
            </tr>
            <tr>
                <td style="color: Black; font-size: large; width: 178px;">
                    Device Details
                </td>
                <td style="width: 332px">
                    &nbsp;
                </td>
               
            </tr>
            <tr>
                <td style="width: 178px">
                    Serial Number:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtSNum" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="txtSNum"
                        runat="server" ErrorMessage="Enter Serial Number">*</asp:RequiredFieldValidator>
                </td>
              
            </tr>
            <tr>
                <td style="width: 178px">
                    Date of Purchase:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtDOP" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDOP">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ControlToValidate="txtDOP"
                        runat="server" ErrorMessage="Enter Date of Purchase">*</asp:RequiredFieldValidator>
                </td>
                
                
            </tr>
            <tr>
                <td style="width: 178px">
                    Dealer Name:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtDealerName" runat="server" Height="15px" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ControlToValidate="txtDealerName"
                        runat="server" ErrorMessage="Enter Dealer Name">*</asp:RequiredFieldValidator>
                </td>
                <td style="width: 183px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    SMS Alert No:
                </td>
                <td style="width: 332px">
                    <asp:TextBox ID="txtsmsno" runat="server" Height="15px" Width="150px" Enabled="false"></asp:TextBox>
                    &nbsp;&nbsp;(Default)&nbsp;&nbsp;<br />
                    <asp:CheckBox ID="cbsmsno" runat="server" Text="New SMS Mobile NO" AutoPostBack="true"
                        OnCheckedChanged="cbsmsno_CheckedChanged" />
                </td>
                <td style="width: 183px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 178px">
                    <asp:Label ID="lblsmsno" runat="server" Text="SMS Alert New NO:" Visible="false"></asp:Label>
                </td>
                <td style="width: 332px">
                    <asp:TextBox runat="server" ID="txtmsmmno" Height="15px" Width="150px" 
                        Visible="false" ontextchanged="txtmsmmno_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" ControlToValidate="txtmsmmno"
                        runat="server" ErrorMessage="Enter New SMS No">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                        ControlToValidate="txtmsmmno" ErrorMessage="Please enter valid Mobile number!"
                        ValidationExpression="^([7-9]{1})([0-9]{9})$">*</asp:RegularExpressionValidator>
                </td>
                <td style="width: 183px">
                </td>
                <td>
                </td>
            </tr>
            
        </table>
        <div style="height: 20px; text-align: center;">
        </div>
        <div style="text-align: center;">
            <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" />
        </div>
        <div style="height: 20px; text-align: center;">
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class EditCustDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {

                        ddlState.DataSource = db.GetState(Session["ParentUser"].ToString());
                        ddlState.DataBind();
                        string id = Session["UserID"].ToString();
                        DataTable Edit = db.Load(id);

                        lblID.Text = Edit.Rows[0]["ID"].ToString();
                        string Company = Edit.Rows[0]["Cx_Type"].ToString();
                        if (Company == "Company")
                        {
                            rbtnCompany.Checked = true;
                            rbtnIndCust.Checked = false;
                        }
                        else
                        {
                            rbtnIndCust.Checked = true;
                            rbtnCompany.Checked = false;
                        }

                        txtCustName.Text = Edit.Rows[0]["Cx_Name"].ToString();
                        txtCustAddress1.Text = Edit.Rows[0]["Cx_BillingAddress1"].ToString();
                        txtCustAddress2.Text = Edit.Rows[0]["Cx_BillingAddress2"].ToString();
                        txtDistrict.Text = Edit.Rows[0]["Cx_District"].ToString();
                        string state = Edit.Rows[0]["Cx_State"].ToString();
                        ddlState.Text = state;
                        txtAreaCode.Text = Edit.Rows[0]["Cx_AreaCode"].ToString();
                        txtCustConNum.Text = Edit.Rows[0]["Cx_ContactNumber"].ToString();
                        txtCustMobNum.Text = Edit.Rows[0]["Cx_MobileNumber"].ToString();
                        txtDOR.Text = Edit.Rows[0]["Cx_DateOfRegistration"].ToString();
                        txtEmail.Text = Edit.Rows[0]["Cx_EmailID"].ToString();
                        txtaEmail .Text =Edit .Rows [0]["Cx_AlterEmailID"].ToString();
                       

                    }
                }
            }
        }

        //protected void imgBtnDone_Click(object sender, ImageClickEventArgs e)
        //{
        //    DBClass db = new DBClass();
        //    string Company;
        //    if (rbtnCompany.Checked)
        //    {
        //        Company = "Company";
        //    }
        //    else
        //    {
        //        Company = "Individual";
        //    }

        //    db.UpdateCustomer(Company, txtCustName.Text, txtCustAddress1.Text, txtCustAddress2.Text, txtDistrict.Text, ddlState.SelectedItem.Value, txtAreaCode.Text, txtCustConNum.Text, txtCustMobNum.Text, txtDOR.Text, txtEmail.Text, lblID.Text);
        //    //rbtnIndCust.Text = string.Empty;
        //    //rbtnCompany.Text = string.Empty;
        //    //txtCustName.Text = string.Empty;
        //    //txtCustAddress1.Text = string.Empty;
        //    //txtCustAddress2.Text = string.Empty;
        //    //txtDistrict.Text = string.Empty;
        //    //ddlState.SelectedItem.Value = string.Empty;
        //    //txtAreaCode.Text = string.Empty;
        //    //txtCustConNum.Text = string.Empty;
        //    //txtCustMobNum.Text = string.Empty;
        //    //txtDOR.Text = string.Empty;
        //    //txtEmail.Text = string.Empty;
        //    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Registration Successfully Edited.');</script>");
        //    //Response.Redirect("JustView.aspx");
        //}

        //protected void imgBtnHome_Click(object sender, ImageClickEventArgs e)
        //{
        //    string User = Session["UserName"].ToString();
        //    Response.Redirect("Track.aspx?username=" + User);
        //    //imgBtnClose.Attributes.Add("onclick", "closeWindow();");
        //    //Response.Write("<script language=javascript> window.close();</script>");
        //    //Response.End();

        //}
        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void btnConfirmEdit_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            string id = Session["UserID"].ToString();
            string Company;

            if (rbtnCompany.Checked)
            {
                Company = "Company";
            }
            else
            {
                Company = "Individual";
            }

            db.UpdateCustomer(Company, txtCustName.Text, txtCustAddress1.Text, txtCustAddress2.Text, txtDistrict.Text, ddlState.SelectedItem.Value, txtAreaCode.Text, txtCustConNum.Text, txtCustMobNum.Text, txtDOR.Text, txtEmail.Text,txtaEmail.Text , lblID.Text);
            rbtnIndCust.Checked = false;
            rbtnCompany.Checked = false;
            txtCustName.Text = string.Empty;
            txtCustAddress1.Text = string.Empty;
            txtCustAddress2.Text = string.Empty;
            txtDistrict.Text = string.Empty;
            ddlState.SelectedValue = null;
            txtAreaCode.Text = string.Empty;
            txtCustConNum.Text = string.Empty;
            txtCustMobNum.Text = string.Empty;
            txtDOR.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtaEmail.Text = string.Empty;
            //Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Registration Successfully Updated');</script>");
            Response.Redirect("JustView.aspx");
        }
    }
}

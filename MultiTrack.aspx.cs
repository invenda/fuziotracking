﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
namespace Tracking
{
    public partial class MultiTrack : System.Web.UI.Page
    {

        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";
        DBClass db = new DBClass();
 
  
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    //stat = db.GetVehicles(Session["cUser_id"].ToString());
                    db.delmultitrack1(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
                }
                else
                {
                    db.delmultitrack1(Session["UserID"].ToString(), Session["UserRole"].ToString());
                }


                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(17.509484, 79.133835);
                GMap1.setCenter(gLatLng, 5, GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;
                GControl control = new GControl(GControl.preBuilt.SmallMapControl);
                GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

                GMap1.Add(control);
                GMap1.Add(control2);

                DataTable statfuzionew;
                if (Session["UserRole"].ToString() == "3")
                {
                    statfuzionew = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

                }
                else
                {
                    statfuzionew = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

                }

                bindvehicle(statfuzionew);
                binddata(statfuzionew);
            }
        }
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Response.Redirect("Login.aspx");
        }
       
        protected void change(object sender, EventArgs e)
        {
            if (chkall.Checked)
            {
                chkall.Checked = false;
                foreach (GridViewRow gvrow in GVehicles.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("CheckBox1");
                    //if (chk != null & chk.Checked)
                    //{
                        //CheckBox ckb = sender as CheckBox;
                        GridViewRow gvr = chk.NamingContainer as GridViewRow;
                        string vid = GVehicles.DataKeys[gvr.RowIndex].Value.ToString();
                        //string userid = db.Get_UserID(vid);
                        if (Session["UserRole"].ToString() == "3")
                        {
                            if (chk.Checked == true)
                            {
                                db.setmultitrack(vid, Session["cUser_id"].ToString());
                            }
                            else
                            {
                                db.delmultitrack(vid, Session["cUser_id"].ToString());
                            }
                        }
                        else
                        {
                            if (chk.Checked == true)
                            {
                                db.setmultitrack(vid, Session["UserID"].ToString());
                            }
                            else
                            {
                                db.delmultitrack(vid, Session["UserID"].ToString());
                            }
                        }
                   // }
                   
                }
                DataTable statfuzionew;
                if (Session["UserRole"].ToString() == "3")
                {
                    statfuzionew = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

                }
                else
                {
                    statfuzionew = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

                }

                bindvehicle(statfuzionew);
                binddata(statfuzionew);

            }
            else if (chkall.Checked==false)            
            {
                foreach (GridViewRow gvrow in GVehicles.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("CheckBox1");
                    GridViewRow gvr = chk.NamingContainer as GridViewRow;
                    string vid = GVehicles.DataKeys[gvr.RowIndex].Value.ToString();
                    //string userid = db.Get_UserID(vid);
                    if (Session["UserRole"].ToString() == "3")
                    {
                        if (chk.Checked == true)
                        {
                            db.setmultitrack(vid, Session["cUser_id"].ToString());
                        }
                        else
                        {
                            db.delmultitrack(vid, Session["cUser_id"].ToString());
                        }
                    }
                    else
                    {
                        if (chk.Checked == true)
                        {
                            db.setmultitrack(vid, Session["UserID"].ToString());
                        }
                        else
                        {
                            db.delmultitrack(vid, Session["UserID"].ToString());
                        }
                    }
                }
                DataTable ckall;
                if (Session["UserRole"].ToString() == "3")
                {
                    ckall = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
                    // dt = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
                }
                else
                {
                    ckall = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
                }
                //ckall = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
                if (GVehicles.Rows.Count == ckall.Rows.Count)
                {
                    chkall.Checked = true;
                }
                else { chkall.Checked = false; }
                DataTable statfuzionew;
                if (Session["UserRole"].ToString() == "3")
                {
                    statfuzionew = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

                }
                else
                {
                    statfuzionew = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

                }

                bindvehicle(statfuzionew);
                binddata(statfuzionew);
            }
            else
            {
                foreach (GridViewRow gvrow in GVehicles.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("CheckBox1");
                    //if (chk != null & chk.Checked)
                    //{
                        //CheckBox ckb = sender as CheckBox;
                        GridViewRow gvr = chk.NamingContainer as GridViewRow;
                        string vid = GVehicles.DataKeys[gvr.RowIndex].Value.ToString();
                        //string userid = db.Get_UserID(vid);
                        if (Session["UserRole"].ToString() == "3")
                        {
                            if (chk.Checked == true)
                            {
                                db.setmultitrack(vid, Session["cUser_id"].ToString());
                            }
                            else
                            {
                                db.delmultitrack(vid, Session["cUser_id"].ToString());
                            }
                        }
                        else
                        {
                            if (chk.Checked == true)
                            {
                                db.setmultitrack(vid, Session["UserID"].ToString());
                            }
                            else
                            {
                                db.delmultitrack(vid, Session["UserID"].ToString());
                            }
                        }
                    //}
                }
                DataTable statfuzionew;
                if (Session["UserRole"].ToString() == "3")
                {
                    statfuzionew = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

                }
                else
                {
                    statfuzionew = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

                }

                bindvehicle(statfuzionew);
                binddata(statfuzionew);
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {

            DataTable statfuzio;
            if (Session["UserRole"].ToString() == "3")
            {
                statfuzio = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

            }
            else
            {
                statfuzio = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

            }
            bindvehicle(statfuzio);
            binddata(statfuzio);
        }
        public void bindvehicle(DataTable statfuzio)
        {
            DataTable stat = new DataTable();
            stat = statfuzio;
            /*
            if (Session["UserRole"].ToString() == "3")
            {
                stat = db.GetVehicles(Session["cUser_id"].ToString());

            }
            else
            {
                stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
            }
            */
            if (stat.Columns.Count > 0)
            {
                stat.Columns.Add("Status", typeof(string));
                stat.Columns.Add("serialno", typeof(string));
                for (int i = 0; i < stat.Rows.Count; i++)
                {
                    string VN = Convert.ToString(stat.Rows[i][12].ToString());

                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(stat.Rows[i][4].ToString());
                        StringDT = StringDT.AddMinutes(360);
                        if (sysDT > StringDT)
                        {
                            stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ✘";
                        }
                        else //if(sysDT <= StringDT)
                        {
                            stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ✔";
                        }

                    stat.Rows[i]["serialno"] = stat.Rows[i][15].ToString();
                }
            }
            stat.AcceptChanges();
            GVehicles.DataSource = stat;
            GVehicles.DataBind();
        }
        public void binddata(DataTable statfuzio)
        {
            GMap1.resetMarkers();
            DataTable dt;


            if (Session["UserRole"].ToString() == "3")
            {

                dt = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
            }
            else
            {
                dt = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataTable dt1;
                String matchString = dt.Rows[i][0].ToString();//assuming we have to find rows having key=ID0001
                DataTable dtTarget = new DataTable();
                dtTarget = statfuzio.Clone();
                DataRow[] rowsToCopy;
                rowsToCopy = statfuzio.Select("VehicalNumber='" + matchString + "'");
                foreach (DataRow temp in rowsToCopy)
                {
                    dtTarget.ImportRow(temp);
                }


                dt1 = dtTarget;

                if (dt1.Rows.Count > 0)
                {

                    Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt1.Rows[0][0]), Convert.ToDouble(dt1.Rows[0][1]));

                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
                    Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                    Subgurim.Controles.GIcon icon= new Subgurim.Controles.GIcon();//pinLetter.ToString(), pinLetter.Shadow());
                    if (dt1.Rows[0][4].ToString() == "1" && Convert.ToInt32(dt1.Rows[0][2].ToString()) > 3)
                    {
                        // icon.image = "http://google.com/mapfiles/ms/micons/green-dot.png";
                        SPin sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                    }
                    else if (dt1.Rows[0][4].ToString() == "1" && Convert.ToInt32(dt1.Rows[0][2].ToString()) < 3)
                    {
                        SPin sPin = new SPin(0.5, 0, System.Drawing.Color.Blue, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                    }
                    else
                    {
                        //icon.image = "http://google.com/mapfiles/ms/micons/red-dot.png";
                        SPin sPin1 = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin1.ToString());

                    }
                    // Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(sPin.ToString());
                    options.icon = icon;
                   // GMap1.addGMarker(oMarker);
                    double speed = Convert.ToDouble(dt1.Rows[0][2].ToString());
                    string sped = Convert.ToString(speed * 1.85);

                    DateTime LocDTL = Convert.ToDateTime(dt1.Rows[0][4].ToString());

                    options.title = "Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                    oMarker.options = options;
                   // GMap1.addGMarker(oMarker);
                    GMap1.Add(oMarker);
                }
                else
                {
                    DataTable dt2 = db.multitrackdetailsoff(dt.Rows[i][0].ToString());
                    Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt2.Rows[0][0]), Convert.ToDouble(dt2.Rows[0][1]));


                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
                    Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                    Subgurim.Controles.GIcon icon;//= new Subgurim.Controles.GIcon();//pinLetter.ToString(), pinLetter.Shadow());
                    if (dt2.Rows[0][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[0][2].ToString()) > 3)
                    {
                        // icon.image = "http://google.com/mapfiles/ms/micons/green-dot.png";
                        SPin sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                    }
                    else if (dt2.Rows[0][4].ToString() == "1" && Convert.ToInt32(dt2.Rows[0][2].ToString()) < 3)
                    {
                        SPin sPin = new SPin(0.5, 0, System.Drawing.Color.Blue, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                    }
                    else
                    {
                        //icon.image = "http://google.com/mapfiles/ms/micons/red-dot.png";
                        SPin sPin1 = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, dt.Rows[i][0].ToString());
                        icon = new Subgurim.Controles.GIcon(sPin1.ToString());

                    }
                    double speed = Convert.ToDouble(dt2.Rows[0][2].ToString());
                    string sped = Convert.ToString(speed * 1.85);

                    DateTime LocDTL = Convert.ToDateTime(dt2.Rows[0][3].ToString());

                    options.title = "Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                    options.icon = icon;
                    oMarker.options = options;
                    GMap1.Add(oMarker);

                }
            }
            // checkall();
        }
        public void checkall()
        {
            DataTable ckall;
            if (Session["UserRole"].ToString() == "3")
            {
                ckall = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
                // dt = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
            }
            else
            {
                ckall = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
            }
            //ckall = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
            if (GVehicles.Rows.Count == ckall.Rows.Count)
            {
                chkall.Checked = true;
            }
            else { chkall.Checked = false; }

        }

        protected void GVehicles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                CheckBox lb = (CheckBox)e.Row.FindControl("CheckBox1");
                DataTable dt;
                if (Session["UserRole"].ToString() == "3")
                {

                    dt = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
                }
                else
                {
                    dt = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label lbl = (Label)e.Row.FindControl("lblvehicleno");
                    if (lbl.Text == dt.Rows[i][0].ToString())
                    {
                        lb.Checked = true;
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else
                    {
                        // lb.Checked = false;
                        e.Row.BackColor = System.Drawing.Color.Gray;
                    }
                }
                Label lbstatus = (Label)e.Row.FindControl("lblstatus");
                if (lbstatus.Text == "Offline")
                {
                    lbstatus.ForeColor = System.Drawing.Color.Red;
                }
                if (lb.Checked)
                {
                    e.Row.BackColor = System.Drawing.Color.Orange;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Gray;
                }
            }
        }

        protected void chkall_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["UserRole"].ToString() == "3")
            {
                if (chkall.Checked == true)
                {
                    foreach (GridViewRow dr in GVehicles.Rows)
                    {
                        CheckBox chk = (CheckBox)dr.Cells[0].FindControl("CheckBox1");
                        Label lb = (Label)dr.Cells[1].FindControl("lblvehicleno");
                        //string userid = db.Get_UserID(lb.Text);
                        db.setmultitrack(lb.Text, Session["cUser_id"].ToString());
                        chk.Checked = true;
                    }
                }
                else if (chkall.Checked == false)
                {
                    foreach (GridViewRow dr in GVehicles.Rows)
                    {
                        CheckBox chk = (CheckBox)dr.Cells[0].FindControl("CheckBox1");
                        Label lb = (Label)dr.Cells[1].FindControl("lblvehicleno");
                        //string userid = db.Get_UserID(lb.Text);
                        db.delmultitrack(lb.Text, Session["cUser_id"].ToString());
                        chk.Checked = false;
                    }
                }
                // dt = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
            }
            else
            {
                if (chkall.Checked == true)
                {
                    foreach (GridViewRow dr in GVehicles.Rows)
                    {
                        CheckBox chk = (CheckBox)dr.Cells[0].FindControl("CheckBox1");
                        Label lb = (Label)dr.Cells[1].FindControl("lblvehicleno");
                        //string userid = db.Get_UserID(lb.Text);
                        db.setmultitrack(lb.Text, Session["UserID"].ToString());
                        chk.Checked = true;
                    }
                }
                else if (chkall.Checked == false)
                {
                    foreach (GridViewRow dr in GVehicles.Rows)
                    {
                        CheckBox chk = (CheckBox)dr.Cells[0].FindControl("CheckBox1");
                        Label lb = (Label)dr.Cells[1].FindControl("lblvehicleno");
                        //string userid = db.Get_UserID(lb.Text);
                        db.delmultitrack(lb.Text, Session["UserID"].ToString());
                        chk.Checked = false;
                    }
                }
            }
            DataTable statfuzionew;
            if (Session["UserRole"].ToString() == "3")
            {
                statfuzionew = db.Fuzio_Home(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

            }
            else
            {
                statfuzionew = db.Fuzio_Home(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

            }

            //bindvehicle(statfuzionew);
            binddata(statfuzionew);
        }

        protected void btnLocation_Click(object sender, EventArgs e)
        {
            DataTable dtloc;
            if (Session["UserRole"].ToString() == "3")
            {

                dtloc = db.getmultitrack(Session["cUser_id"].ToString(), Session["UserRole"].ToString());
            }
            else
            {
                dtloc = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString());
            }
            // DataTable dtloc = db.getmultitrack(Session["UserID"].ToString(), Session["UserRole"].ToString()); ;
            dtloc.Columns.Add("Location", typeof(string));
            for (int i = 0; i < dtloc.Rows.Count; i++)
            {
                string address = "";
                DataTable status = db.multitrackdetails(dtloc.Rows[i][0].ToString());
                if (status.Rows.Count > 0)
                {
                    /*string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
                    Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                    GMap gMap1 = new GMap();
                    gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                    StringBuilder sb = new StringBuilder();
                    if ((null != geocode) && geocode.valid)
                    {
                        sb.AppendFormat(geocode.Placemark.address);
                    }
                    address = sb.ToString();*/

                    string latitude = Convert.ToDouble(status.Rows[0][0]).ToString();
                    string longitude = Convert.ToDouble(status.Rows[0][1]).ToString();

                    System.Net.WebClient client = new System.Net.WebClient();
                    string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                    string response = client.DownloadString(urlstring);
                    dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                    if (jsonresponse.results.Count > 0)
                    {
                        string formatted_address = jsonresponse.results[0].formatted_address;
                        address = formatted_address;
                    }

                    if (address == "")
                    {
                        address = db.getgeoaddress(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    }
                    dtloc.Rows[i]["Location"] = address;
                }
                else
                {
                    DataTable status1 = db.multitrackdetailsoff(dtloc.Rows[i][0].ToString());

                    /*string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
                    Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(status1.Rows[0][0]), Convert.ToDouble(status1.Rows[0][1]));
                    GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                    GMap gMap1 = new GMap();
                    gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                    StringBuilder sb = new StringBuilder();
                    if ((null != geocode) && geocode.valid)
                    {
                        sb.AppendFormat(geocode.Placemark.address);
                    }
                    address = sb.ToString();*/
                    string latitude = Convert.ToDouble(status1.Rows[0][0]).ToString();
                    string longitude = Convert.ToDouble(status1.Rows[0][1]).ToString();

                    System.Net.WebClient client = new System.Net.WebClient();
                    string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                    string response = client.DownloadString(urlstring);
                    dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                    if (jsonresponse.results.Count > 0)
                    {
                        string formatted_address = jsonresponse.results[0].formatted_address;
                        address = formatted_address;
                    }
                    if (address == "")
                    {
                        address = db.getgeoaddress(Convert.ToDouble(status1.Rows[0][0]), Convert.ToDouble(status1.Rows[0][1]));
                    }
                    dtloc.Rows[i]["Location"] = address;

                }
            }
            dtloc.AcceptChanges();
            Session["multitable"] = dtloc;
            // Session["vehicleno"] = ddlMapTOVehicle.SelectedValue.ToString();
            Response.Redirect("multiviewreport.aspx");
        }

        protected void GVehicles_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow &&
            //   (e.Row.RowState == DataControlRowState.Normal ||
            //    e.Row.RowState == DataControlRowState.Alternate))
            //{
            //    CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("CheckBox1");
            //    CheckBox chkBxHeader = (CheckBox).FindControl("chkall");
            //    if (chkBxHeader != null)
            //    {
            //        chkBxSelect.Attributes["onclick"] = string.Format("javascript:ChildClick(this,'{0}');", chkBxHeader.ClientID);
            //    }
            //}
        }
    }
}

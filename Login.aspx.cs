﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Login : System.Web.UI.Page
    {
        string alertstring;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (chkbxACCEPT.Checked == true)
            {
                user_login.Enabled = true;
                user_pass.Enabled = true;
                btnLogin.Enabled = true;
                RbtUser.Enabled = true;
            }
            else if (chkbxACCEPT.Checked == false)
            {
                user_login.Enabled = false;
                user_pass.Enabled = false;
                btnLogin.Enabled = false;
                RbtUser.Enabled = false;
            }
        }

        protected void login_click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable userdt = db.CheckUser(user_login.Text, user_pass.Text);
            if (userdt.Rows.Count > 0)
            {
                Session["UserID"] = userdt.Rows[0][0];
                Session["UserName"] = userdt.Rows[0][1];
                DataTable alert = db.Getuseralert(Session["UserID"].ToString());
                if (alert.Rows.Count > 0)
                {
                    if (alert.Rows[0][0].ToString() == "Yes")
                    {
                        alertstring = "sms";// alert.Rows[0][1].ToString();
                        // this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
                    }

                }

                Session["UserID"] = userdt.Rows[0][0];
                Session["UserName"] = userdt.Rows[0][1];
                Session["UserRole"] = userdt.Rows[0][5];
                Session["ParentUser"] = userdt.Rows[0][7];
                Session["Logotext"] = userdt.Rows[0][9];
                Session["APIKey"] = userdt.Rows[0][10];
                if (RbtUser.SelectedValue.ToString() == Session["UserRole"].ToString())
                {
                    if (userdt.Rows[0]["Register"].ToString() == "")
                        Response.Redirect("CustDetails.aspx");
                    Session["UserID"] = userdt.Rows[0][0];
                    Session["UserRole"] = userdt.Rows[0][5];
                    Session["ParentUser"] = userdt.Rows[0][7];
                    if (Session["UserRole"].ToString() == "2")
                    {
                        if (alert.Rows[0][0].ToString() == "Yes")
                        {
                            Response.Redirect("Home.aspx?username=" + userdt.Rows[0][1] + "&&message=" + alertstring);//+ "RegNo=" + RegNum.Rows[0][0]);
                        }
                        else
                        {
                            Response.Redirect("Home.aspx?username=" + userdt.Rows[0][1]);

                        }
                    }
                    else if (Session["UserRole"].ToString() == "3")
                        Response.Redirect("ManagerHome1.aspx?username=" + Session["UserName"].ToString());
                    else
                        lblerror.Text = "Login Failed";
                }
                else lblerror.Text = "Login Failed";
            }
            else
            {
                lblerror.Text = "Login Failed";
            }


        }

    }
}

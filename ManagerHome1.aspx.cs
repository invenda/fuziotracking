﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;    
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Tracking
{
    public partial class MasterHome1 : System.Web.UI.Page
    {
        int count = 0;
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Binddata();
                //DataTable users12 = new DataTable();
                //users12.Columns.Add ("No_of_veh",typeof(string ));
                //DataTable getcount = db.GetVehiclescount(Session["cUser_id"].ToString());
                //DataTable users = db.getmanagerusers(Session["UserID"].ToString());
                //gvrecords.DataSource =users; //db.getmanagerusers(Session["UserID"].ToString());
                //gvrecords.DataBind();

            }

        }


        private void Binddata()
        {
            DataTable users = db.getmanagerusers(Session["UserID"].ToString());
             users.Columns.Add ("No_of_veh",typeof(string ));
            if (users.Rows.Count > 0)
            {
                for (int i = 0; i < users.Rows.Count; i++)
                {
                    DataTable getcount = db.GetVehiclescount(users .Rows[i][1].ToString());
                    DataTable getdesablecount = db.GetdesabledVehiclescount(users.Rows[i][1].ToString());
                    int total = (Int32)getcount.Rows[0][0] + (Int32)getdesablecount.Rows[0][0];

                    users.Rows[i]["No_of_veh"] = total.ToString();
                }

            }
            users.AcceptChanges();
            gvrecords.DataSource = users; //db.getmanagerusers(Session["UserID"].ToString());
            gvrecords.DataBind();

        }
        protected void btncustomer_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            GridViewRow grdrow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            Label lblbranch = new Label();
            lbl = (Label)grdrow.FindControl("lbluser_name");
            lblbranch = (Label)grdrow.FindControl("lblcname");
            Session["Cusername"] = lbl.Text;
            Session["Branchname"] = lblbranch.Text;
            DataTable getid = db.Getfreez(lbl.Text);
            Session["cUser_id"] = getid.Rows[0][0].ToString();
            Response.Redirect("ManagerHome.aspx?username=" + Session["Cusername"].ToString());
        }

        protected void gvrecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcount = e.Row.FindControl("lbldno") as Label;
                if (lblcount.Text != "&nbsp;" && lblcount.Text != "")
                {
                    count += Convert.ToInt32(lblcount.Text);
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblcount12 = e.Row.FindControl("lbltotal12") as Label;
                lblcount12.Text = count.ToString();
            }
        }
        protected void btnGetvehicles_Click(object sender, EventArgs e)
        {

        }

     


    }
}
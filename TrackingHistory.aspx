﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrackingHistory.aspx.cs"
    Inherits="Tracking.TrackingHistory" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="CPH1" runat="server">
    <div style="width: 100%;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div style="width: auto; height: 100%; background-color:Gray;">
            <table style="width: 100%; height: 20px; ">
                <tr >
                    <td style="width: 25%; height: 20px; vertical-align: middle; font-weight: bold; " 
                        align="left">
                        <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Black" Text="Engine Status :"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Image ID="imgst" runat="server" Width="14px" Height="14px" />
                        <asp:Label Text="Over Speed" runat="server" ID="Lbl1" ForeColor ="Black" ></asp:Label>&nbsp;&nbsp;
                        <asp:Image ID="imgidl" runat="server" Width="14px" Height="14px"  />
                        <asp:Label Text="Idle" runat="server" ID="lbl2"  ForeColor ="Black"></asp:Label >&nbsp;&nbsp;
                        <asp:Image ID="imgmv" runat="server" Width="14px" Height="14px" />
                        <asp:Label ID="lbl3" runat="server" Text="Moving" ForeColor ="Black"></asp:Label>
                        <asp:Image ID="imgfuel" runat="server" Width="14px" Height="14px" />
                        <asp:Label ID="lblfuell" runat="server" Text="Fuel" ForeColor ="Black"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <table style="color: Black; width: 100%;">
            <tr>
                <td colspan="2">
                    <table border="2" style="color: Black; width: 100%;">
                        <tr style="width: 100%;">
                            <td style="width: 176px">
                                <asp:Label ID="lblVehicle" Text="Select Vehicle:" runat="server" ForeColor="Black"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="Group1" ControlToValidate="ddlMapTOVehicle"
                                ErrorMessage="-Select the Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 393px">
                                <asp:Label ID="lblVehEngStat" Text="Vehicle Status Report:" runat="server" ForeColor="Black"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlVehStatRpt"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Status" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                            </td>
                            <td style="width: 392px">
                                Select  Date:
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                    ValidationGroup="Group1" ErrorMessage="Select Date">*</asp:RequiredFieldValidator>
                                </td>
                            <td align="left" style="width: 524px">
                                Select From Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rbtnAMPM"
                                    ValidationGroup="Group1" ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 569px">
                                Select To Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbtnDT"
                                    ValidationGroup="Group1" ErrorMessage="Select TO Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 476px">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click"
                                    Width="110px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 176px; height: 21px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 393px; height: 21px;">
                                <asp:DropDownList ID="ddlVehStatRpt" runat="server">
                                    <asp:ListItem Text="--" Value="--" />
                                    <asp:ListItem Text="Vehicle Moving" Value="1" />
                                    <asp:ListItem Text="Vehicle Stationary" Value="2" />
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 392px; height: 21px;">
                                <asp:TextBox ID="txtdate" runat="server" Height="16px" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="txtdate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="width: 524px; height: 21px;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="59px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="70px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal" 
                                                Height="23px" Width="112px">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 569px; height: 21px;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlToHOURS" runat="server" Width="66px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="65px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes ">*</asp:CompareValidator>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal" 
                                                Width="102px">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 476px; height: 21px;">
                                <asp:Button ID="btView" runat="server" Text="View History" OnClick="btView_Click"
                                    ValidationGroup="Group1" Width="110px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>                
                <td>
                    <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="800px" 
                        onclick="GMap1_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

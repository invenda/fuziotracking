﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetAlerts.aspx.cs" Inherits="Tracking.SetAlerts"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 100%;">
        <div style="width: 100%; height: 50px; vertical-align: middle; font-size: 25px; text-align: center;
            color: Blue;">
            <asp:Label ID="lblhead" Text="Set Vehicle Alerts" runat="server"></asp:Label>
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </div>
        <div style="width: 1200px; margin: 0 auto; padding: 0; padding: 0;">
            <div style="width: 100%; height: 50px; text-align: center; text-align: left;">
                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>:
                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                    AutoPostBack="true" DataTextField="Vehicalnumber" Height="23px" Width="170px"
                    Style="margin-left: 1px" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged"
                    OnDataBound="ddlMapTOVehicle_DataBound">
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label ID="Label2" Text="Daily Alerts" ForeColor="Black" Font-Bold="true" runat="server"></asp:Label>
            </div>
            <div>
                <table>
                    <tr>
                        <td>
                            Vehicle Stationary Duration:
                        </td>
                        <td>
                            <asp:Panel ID="PnlS" runat="server">
                                <table>
                                    <tr>
                                        <td style="width: 200px;">
                                            <asp:TextBox ID="txthr" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txthr"
                                                runat="server" ErrorMessage="Enter Hours">*</asp:RequiredFieldValidator>
                                            Hr:
                                            <asp:TextBox ID="txtmin" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txthr"
                                                runat="server" ErrorMessage="Enter Minutes">*</asp:RequiredFieldValidator>
                                            Min
                                        </td>
                                        <td style="width: 180px; text-align: right;">
                                            Working Hours From :
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="rbtnAMPM"
                                                ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="50px" Height="25px" DataValueField="Hours"
                                                            DataTextField="Hours">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="50px" Height="25px" DataValueField="Minutes"
                                                            DataTextField="Minutes">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            To Time:
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="RadioButtonList1"
                                                ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlToHOURS" runat="server" Width="50px" DataValueField="Hours"
                                                            DataTextField="Hours" Height="25px">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlToHOURS"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="50px" Height="25px" DataValueField="Minutes"
                                                            DataTextField="Minutes">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToMINUTES"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkbvs" runat="server" AutoPostBack="true" OnCheckedChanged="chkbvs_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vehicle Security Alert:
                        </td>
                        <td>
                            <asp:Panel ID="PnlSA" runat="server">
                                <table>
                                    <tr>
                                        <td style="width: 200px;">
                                            <asp:TextBox ID="txtsamin" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtsamin"
                                                runat="server" ErrorMessage="Enter Minutes">*</asp:RequiredFieldValidator>
                                            &nbsp;Min
                                        </td>
                                        <td style="height: 40px; width: 180px; text-align: right;">
                                            From Time:
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="RadioButtonList2"
                                                ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="height: 40px;">
                                            <table style="height: 40px;">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlFromHOURS1" runat="server" Width="50px" Height="25px" DataValueField="Hours"
                                                            DataTextField="Hours">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlFromHOURS1"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlFromMINUTES1" runat="server" Width="50px" Height="25px"
                                                            DataValueField="Minutes" DataTextField="Minutes">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlFromMINUTES1"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            To Time:
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="RadioButtonList3"
                                                ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlToHOURS1" runat="server" Width="50px" DataValueField="Hours"
                                                            DataTextField="Hours" Height="25px">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="ddlToHOURS1"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlToMINUTES1" runat="server" Width="50px" Height="25px" DataValueField="Minutes"
                                                            DataTextField="Minutes">
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="ddlToMINUTES1"
                                                            Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:CheckBox ID="CkbSecAMin" runat="server" AutoPostBack="true" OnCheckedChanged="CkbSecAMin_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vehicle Moving Duration:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txthr2" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txthr2"
                                            runat="server" ErrorMessage="Enter Hours">*</asp:RequiredFieldValidator>
                                        Hr:
                                        <asp:TextBox ID="txtmin2" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtmin2"
                                            runat="server" ErrorMessage="Enter Minutes">*</asp:RequiredFieldValidator>
                                        Min
                                        <asp:CheckBox ID="chkbvm" runat="server" AutoPostBack="true" OnCheckedChanged="chkbvm_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OverSpeed Limit KM/H:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtspeed" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="requiredfieldvalidator31" runat="server" ControlToValidate="txtspeed"
                                            ErrorMessage="Enter Over Spedd">*</asp:RequiredFieldValidator>
                                        <asp:CheckBox ID="cbovers" runat="server" AutoPostBack="true" OnCheckedChanged="cbovers_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Set Temperature Alert:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkbtemp" runat="server" AutoPostBack="true" OnCheckedChanged="chkbtemp_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnltemp" runat="server">
                                <table>
                                    <tr>
                                        <td style="width: 220px;">
                                            Maximum Threshold Temperature
                                        </td>
                                        <td>
                                            <asp:TextBox ID="maxtemp" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ControlToValidate="maxtemp"
                                                runat="server" ErrorMessage="Enter Alert Temperature">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 220px;">
                                            Minimum Threshold Temperature
                                        </td>
                                        <td>
                                            <asp:TextBox ID="mintemp" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="mintemp"
                                                runat="server" ErrorMessage="Enter Min Temperature">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 220px;">
                                            Delay Time
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtdelay" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtdelay"
                                                runat="server" ErrorMessage="Enter Delay Time">*</asp:RequiredFieldValidator>(1-360
                                            Minutes)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="font-size: 10px;">
                                            Note : If Threshold Temperature is Out of range for set delay time a alert is generates
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 75px;" colspan="2">
                            <asp:Label ID="Label3" Text="General Alerts" ForeColor="Black" Font-Bold="true" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 220px;">
                                        GPRS Expiry Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtGPRSActDt" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ControlToValidate="txtGPRSActDt"
                                            runat="server" ErrorMessage="Enter GPRS Activated Date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtGPRSActDt">
                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="regulaexpressionvalidator1" runat="server" ControlToValidate="txtGPRSActDt"
                                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Vehicle Insurance Expiry Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVehInsExpDt" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtVehInsExpDt"
                                            runat="server" ErrorMessage="Enter Vehicle Insurance Expiry Date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtVehInsExpDt">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cbins" runat="server" AutoPostBack="true" OnCheckedChanged="cbins_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtVehInsExpDt"
                                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        RTO Permit Renewal Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtrtodate" runat="server" Height="16px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="requiredfieldvalidator30" runat="server" ControlToValidate="txtrtodate"
                                            ErrorMessage="Enter RTO Date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" TargetControlID="txtrtodate">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cbrtodate" runat="server" AutoPostBack="true" OnCheckedChanged="cbrtodate_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtrtodate"
                                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Driving Licence Expiry Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDrivLic" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtDrivLic"
                                            runat="server" ErrorMessage="Enter Driving Licence Expiry Date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDrivLic">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cblicdate" runat="server" AutoPostBack="true" OnCheckedChanged="cblicdate_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtDrivLic"
                                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        RTO (FC-Renewal Date):
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtfcdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" ControlToValidate="txtfcdate"
                                            runat="server" ErrorMessage="Enter RTO FC DATE">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy" TargetControlID="txtfcdate">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cbfcdate" runat="server" AutoPostBack="true" OnCheckedChanged="cbfcdate_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtfcdate"
                                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Emission Check Upto:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtems" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" ControlToValidate="txtems"
                                            runat="server" ErrorMessage="Enter Emission date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd/MM/yyyy" TargetControlID="txtems">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cbems" runat="server" AutoPostBack="true" OnCheckedChanged="cbems_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                            ControlToValidate="txtems" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Road Tax Expiry Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtroadtax" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ControlToValidate="txtroadtax"
                                            runat="server" ErrorMessage="Enter Road Tax Expiry Date">*</asp:RequiredFieldValidator>
                                        <cc1:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtroadtax">
                                        </cc1:CalendarExtender>
                                        <asp:CheckBox ID="cbroadtax" runat="server" AutoPostBack="true" OnCheckedChanged="cbroadtax_CheckedChanged" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                            ControlToValidate="txtroadtax" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Contractor Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtconname" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ControlToValidate="txtconname"
                                            runat="server" ErrorMessage="Enter Contractor Name">*</asp:RequiredFieldValidator>
                                        <asp:CheckBox ID="cbcontd" runat="server" AutoPostBack="true" OnCheckedChanged="cbcontd_CheckedChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        Contract Period from Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtconfdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtconfdate">
                                        </cc1:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="requiredfieldvalidator3" runat="server" ControlToValidate="txtconfdate"
                                            ErrorMessage="Enter Contract Started Date">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 220px;">
                                        To Date:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcontdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                            TargetControlID="txtcontdate">
                                        </cc1:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="requiredfieldvalidator22" runat="server" ControlToValidate="txtcontdate"
                                            ErrorMessage="Enter Contract End Date">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                            ControlToValidate="txtcontdate" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: center;">
                <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                <asp:Button ID="btnclear" runat="server" Text="Home" ValidationGroup="group1" OnClick="btnclear_Click" />
            </div>
            <div style="height: 20px;">
            </div>
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicalDailyReport.aspx.cs"
    Inherits="Tracking.VehicalDailyReport" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="width: 100%; height: 50px; font-size: 25px; text-align: center; color: Blue;">
            Vehicle Activity Report
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 1300px; margin: 0 auto; padding: 0; padding: 0; height: auto;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table border="1" style="width: 100%; color: Black;">
                        <tbody align="left">
                            <tr align="left">
                                <td align="right" style="width: 183px">
                                    <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                        ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>&nbsp;:
                                </td>
                                <td style="width: 151px">
                                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                        DataTextField="Vehicalnumber" Height="26px" Width="134px">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 129px">
                                    <asp:RadioButtonList ID="rbtType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtType_SelectedIndexChanged"
                                        RepeatDirection="Vertical" Width="119px" Height="26px">
                                        <asp:ListItem Text="One Day" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Multiple days" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>

                                </td>
                                <td id="TdSearch" runat="server" visible="false" style="width: 287px;">
                                    <table border="2" style="color: Black;">
                                        <tr>
                                            <td style="width: 118px">
                                                <asp:Label ID="Label3" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                                    ControlToValidate="txtdate" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 100px">
                                                <asp:TextBox ID="txtdate" runat="server" Width="129px" Height="20px"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtdate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td id="TdSearch1" runat="server" visible="false" style="width: 900px;">
                                    <table border="1" style="color: Black;">
                                        <tr>
                                            <td style="width: 140px">
                                                <asp:Label ID="Label2" Text="From Date" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                                    ControlToValidate="txtfdate" ErrorMessage="- Select From Date">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 190px">
                                                <asp:TextBox ID="txtfdate" runat="server" Width="112px" Height="20px"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="txtfdate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 123px">
                                                <asp:Label ID="Label4" Text="To Date" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="Group1"
                                                    ControlToValidate="txttdate" ErrorMessage="- Select To Date">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 180px">
                                                <asp:TextBox ID="txttdate" runat="server" Width="122px" Height="20px"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="M/d/yyyy" TargetControlID="txttdate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 140px">
                                                <asp:Label ID="lbltime" Text="From Time" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rbtnAMPM"
                                                    ValidationGroup="Group1" ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 190px">
                                                <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="59px" DataValueField="Hours"
                                                    DataTextField="Hours" Height="26px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="60px" DataValueField="Minutes"
                                                    DataTextField="Minutes" Height="26px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                            </td>
                                            <td style="width: 95px">
                                                <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal" Height="20px"
                                                    Width="80px">
                                                    <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 123px">
                                                <asp:Label ID="Label5" Text="To Time" runat="server" Font-Bold="true"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rbtnDT"
                                                    ValidationGroup="Group1" ErrorMessage="Select TO Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 180px">
                                                <asp:DropDownList ID="ddlToHOURS" runat="server" Width="59px" DataValueField="Hours"
                                                    DataTextField="Hours" Height="26px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="58px" DataValueField="Minutes"
                                                    DataTextField="Minutes" Height="26px">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes ">*</asp:CompareValidator>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal" Width="82px"
                                                    Height="27px">
                                                    <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 60px">
                                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="Group1"
                                        OnClick="btnsubmit_Click" Width="59px" />
                                </td>
                                <td>
                                    <asp:Button ID="btndownload" runat="server" Text="Download" Visible="false" OnClick="btndownload_Click"
                                        Height="30px" Width="72px" />
                                </td>
                                <td>
                                    <asp:Button ID="btnclear" runat="server" Text="Clear" Width="53px" OnClick="btnclear_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: auto;">
                        <div style="height: 20px;">
                        </div>
                        <div style="vertical-align: middle; text-align: center;">
                            <asp:Label ID="lblnoact" runat="server" Font-Bold="true" Font-Size="20px" Visible ="false" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                            OnRowCreated="gvreport_RowCreated" ShowFooter="true" OnRowDataBound="gvreport_RowDataBound"
                            Width="1300px" RowStyle-ForeColor="Black">
                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px" />
                            <FooterStyle BackColor="#61A6F8" ForeColor="Black" Height="20px" Font-Size="14px"
                                BorderWidth="0" VerticalAlign="Middle" />
                            <Columns>
                                <asp:TemplateField HeaderText="Ignition  On" HeaderStyle-Width="150px" FooterStyle-BorderColor="#61A6F8"
                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblon" runat="server" Text='<%# Bind("ingon") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblmov" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <FooterStyle BorderColor="#61A6F8" HorizontalAlign="Right" />
                                    <HeaderStyle Width="150px" />
                                    <ItemStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Departure" HeaderStyle-Width="300px" FooterStyle-BorderColor="#61A6F8"
                                    ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldep" runat="server" Text='<%# Bind("dep") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltigson" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <FooterStyle BorderColor="#61A6F8" />
                                    <HeaderStyle Width="300px" />
                                    <ItemStyle Font-Size="13px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Arrival" HeaderStyle-Width="300px" FooterStyle-BorderColor="#61A6F8"
                                    FooterStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblar" runat="server" Text='<%# Bind("arrival") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltidl" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <FooterStyle BorderColor="#61A6F8" HorizontalAlign="Center" />
                                    <HeaderStyle Width="300px" />
                                    <ItemStyle Font-Size="13px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ignition Off" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right"
                                    FooterStyle-BorderColor="#61A6F8" HeaderStyle-Width="150px" ItemStyle-Font-Size="13px">
                                    <ItemTemplate>
                                        <asp:Label ID="lbloff" runat="server" Text='<%# Bind("ingoff") %>'></asp:Label>
                                    </ItemTemplate>
                                   <%-- <FooterTemplate>
                                        <asp:Label ID="lblts" runat="server"></asp:Label>
                                    </FooterTemplate>--%>
                                    <FooterStyle BorderColor="#61A6F8" HorizontalAlign="Right" />
                                    <HeaderStyle Width="150px" />
                                    <ItemStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Running Duration (hh:mm)" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="70px" ItemStyle-Font-Size="13px" FooterStyle-BorderColor="#61A6F8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblduration" runat="server" Text='<%# Bind("duration") %>'></asp:Label>
                                    </ItemTemplate>
                                   <%-- <FooterTemplate>
                                        <asp:Label ID="lbltsv" runat="server"></asp:Label>
                                    </FooterTemplate>--%>
                                    <FooterStyle BorderColor="#61A6F8" />
                                    <HeaderStyle Width="70px" />
                                    <ItemStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Engine Stopped (hh:mm)" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="13px" HeaderStyle-Width="120px" FooterStyle-BorderColor="#61A6F8"
                                    FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstop" runat="server" Text='<%# Bind("stop") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblffuel" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <FooterStyle BorderColor="#61A6F8" HorizontalAlign="Right" />
                                    <HeaderStyle Width="120px" />
                                    <ItemStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Travel Distance (KM)" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="13px" FooterStyle-BorderColor="#61A6F8" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblkm" runat="server" Text='<%# Bind("km") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblfuel" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <FooterStyle BorderColor="#61A6F8" />
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle ForeColor="Black" />
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btndownload" />
                </Triggers>
            </asp:UpdatePanel>
            <div style="height: 20px;">
            </div>
        </div>
</asp:Content>

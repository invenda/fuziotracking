﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FuelRpt.aspx.cs" Inherits="Tracking.FuelRpt"
    EnableEventValidation="false" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function ConfirmationBox(username) {

            var result = confirm('Are you sure you want to delete ' + username + ' Details');
            if (result) {

                return true;
            }
            else {
                return false;
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 800px; margin: 0 auto; padding: 0; padding: 0;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: 800px; margin: 0 auto; padding: 0; padding: 0;">
                    <tr>
                        <td align="left">
                            Select the Vehicle Number
                        </td>
                        <td align="left">
                            Select the Vehicle Fueling Date
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlVeh" runat="server" DataValueField="Vehicalnumber" Width="184px"
                                DataTextField="Vehicalnumber">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlVeh"
                                ValidationGroup="Group1" ErrorMessage="Select Vehicalnumber">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtVehFuRec" runat="server" Width="184px"></asp:TextBox>
                            &nbsp;&nbsp;<asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy"
                                TargetControlID="txtVehFuRec">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVehFuRec"
                                ValidationGroup="Group1" ErrorMessage="Select Date">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="center">
                            <asp:Button ID="btnFuelCapEnter" runat="server" Text="Find Records" ValidationGroup="Group1"
                                OnClick="GetRefNo" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button1" runat="server" Text="Download" ToolTip="Click to download records"
                                ValidationGroup="Group1" OnClick="BtnSavPrint_Click" />
                        </td>
                        <td>
                            <asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick="BtnClear_Click1" />
                        </td>
                    </tr>
                </table>
                </div>
                <div style="height: 20px;">
                </div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div>
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" HorizontalAlign="Center"
                        DataKeyNames="Fuel_Challan_No" HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="White"
                        OnRowDataBound="gvrecords_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="Fuel_Challan_No" HeaderText="Reference Number/Challan Number" />
                            <asp:BoundField DataField="Fuel_Station_Name" HeaderText="Name of the Filling Station" />
                            <asp:BoundField DataField="Fuel_Capacity" HeaderText="Number of Liters" />
                            <asp:TemplateField HeaderText="Delete Entries">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Button1" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="height: 20px;">
        </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DayGraph.aspx.cs" Inherits="Tracking.DayGraph"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report4" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }    
  
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 930px; background-color: White;">
        <div style="width: 100%; height: 40px; font-size: 25px; text-align: center; color: Blue;">
            <asp:Label ID="lblhead" Text="Vehicle Day summary Graphs" runat="server"></asp:Label></div>
        <div style="width: 100%; height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 200px;">
                                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 150px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 100px;">
                                <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>:
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                                    ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 130px;">
                                <asp:TextBox ID="date" runat="server" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                                <%--<asp:Label ID="lblErrorMsg" runat="server" Text="*You cannot select a day earlier than today!"
                    Visible="false" ForeColor="Red"></asp:Label>--%>
                            </td>
                            <td style="width: 100px;" align="right">
                                :
                            </td>
                            <td style="width: 150px;">
                                &nbsp;
                            </td>
                            <td style="width: 100px;">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" OnClick="GetRecord_Click" />
                            </td>
                            <td style="width: 120px;" align="center">
                                <%-- <input type="button" value="Print 1st Div" onclick="javascript:printDiv('Printdiv')" />
                       <input type="button" value="PrintDiv1" onclick="Print()" />--%>
                                <asp:Button ID="Download" Text="Download" runat="server" Width="70px" OnClick="Download_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="width: 100%; height: 30px;">
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="false" >
                        <div id="Printdiv">
                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td style="width: 100%; height: 50px;">
                                        <table style="width: 100%; height: 50px;">
                                            <tr>
                                                <td style="width: 50px;">
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVehrtono" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblreqtime" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltemp" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td style="width: 50px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50px;">
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltime" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbldriname" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbldrimob" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td style="width: 50px;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="center" style="width: 100%; height: 500px;">
                                        <asp:Chart ID="CrtDayReport" runat="server" Height="650px" Width="1200px" ImageLocation="~/tempImages/ChartPic_#SEQ(300,3)"
                                            Palette="None" BorderDashStyle="Solid" BorderWidth="2" BackColor="#213258">
                                            <Titles>
                                                <asp:Title Docking="Left" Font="Trebuchet MS, 10.25pt" Text="Minutes" BackColor="Transparent"
                                                    DockingOffset="0" ForeColor="White">
                                                </asp:Title>
                                                <asp:Title Docking="Bottom" Font="Trebuchet MS, 10.25pt" BackColor="Transparent"
                                                    DockingOffset="0" ForeColor="White">
                                                </asp:Title>
                                            </Titles>
                                            <Legends>
                                                <asp:Legend LegendStyle="Row" Docking="Bottom" Name="Default" BackColor="Transparent"
                                                    Font="Trebuchet MS, 7.25pt" ForeColor="WhiteSmoke" Alignment="Far">
                                                </asp:Legend>
                                            </Legends>
                                            <BorderSkin SkinStyle="Emboss"></BorderSkin>
                                            <Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series1" ChartType="Area" LegendText="Moving"
                                                    Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series2" ChartType="Area" LegendText="Idel"
                                                    Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series3" ChartType="Area" LegendText="Stationary"
                                                    Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series4" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series5" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series6" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series7" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series8" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series9" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series10" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series11" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series12" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series13" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series14" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series15" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series16" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series17" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series18" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series19" ChartType="Area" LegendText="Moving"
                                                    Color="Green" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series20" ChartType="Area" LegendText="Idel"
                                                    Color="Blue" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series21" ChartType="Area" LegendText="Stationary"
                                                    Color="Red" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series22" ChartType="Area" LegendText="Moving"
                                                    Color="Green" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series23" ChartType="Area" LegendText="Idel"
                                                    Color="Blue" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series24" ChartType="Area" LegendText="Stationary"
                                                    Color="Red" BorderWidth="1" IsVisibleInLegend="false">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series25" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series26" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series27" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series28" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series29" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series30" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series31" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series32" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series33" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series34" ChartType="Area" LegendText="Moving"
                                                    IsVisibleInLegend="false" Color="Green" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series35" ChartType="Area" LegendText="Idel"
                                                    IsVisibleInLegend="false" Color="Blue" BorderWidth="1">
                                                </asp:Series>
                                                <asp:Series ChartArea="ChartArea1" Name="Series36" ChartType="Area" LegendText="Stationary"
                                                    IsVisibleInLegend="false" Color="Red" BorderWidth="1">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                                                    BackColor="#FFF0F5" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                    <AxisY LineColor="64, 64, 64, 64" Maximum="60" Minimum="0" IsLabelAutoFit="false"
                                                        TitleForeColor="White" Crossing="0" Interval="10">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White" />
                                                        <MajorGrid LineColor="Gray" />
                                                    </AxisY>
                                                    <AxisX LineColor="64, 64, 64, 64" TitleForeColor="White" IntervalAutoMode="FixedCount"
                                                        TitleFont="Trebuchet MS, 10.25pt">
                                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White" />
                                                        <MajorGrid LineColor="Pink" />
                                                        <CustomLabels>
                                                            <asp:CustomLabel Text="" FromPosition="0" ToPosition="30"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="1" FromPosition="50" ToPosition="60"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="2" FromPosition="110" ToPosition="120"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="3" FromPosition="170" ToPosition="180"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="4" FromPosition="230" ToPosition="240"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="5" FromPosition="290" ToPosition="300"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="6" FromPosition="350" ToPosition="360"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="7" FromPosition="410" ToPosition="420"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="8" FromPosition="470" ToPosition="480"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="9" FromPosition="530" ToPosition="540"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="10" FromPosition="580" ToPosition="610"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="11" FromPosition="640" ToPosition="670"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="12" FromPosition="700" ToPosition="730"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="13" FromPosition="770" ToPosition="800"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="14" FromPosition="830" ToPosition="860"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="15" FromPosition="890" ToPosition="920"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="16" FromPosition="950" ToPosition="980"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="17" FromPosition="1010" ToPosition="1040"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="18" FromPosition="1070" ToPosition="1100"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="19" FromPosition="1130" ToPosition="1160"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="20" FromPosition="1190" ToPosition="1220"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="21" FromPosition="1250" ToPosition="1280"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="22" FromPosition="1310" ToPosition="1340"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="23" FromPosition="1370" ToPosition="1400"></asp:CustomLabel>
                                                            <asp:CustomLabel Text="24" FromPosition="1410" ToPosition="1450"></asp:CustomLabel>
                                                        </CustomLabels>
                                                    </AxisX>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 100%; height: 50px;">
                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td valign="top" style="width: 100%; height: 10px; text-align: right;">
                                        <asp:LinkButton ID="linknext" runat="server" Font-Bold="true" Text="Next >>" Font-Underline="false"
                                            ForeColor="Blue" ValidationGroup="Group1" OnClick="linknext_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="width: 100%; height: 10px; text-align: left;">
                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="true" Text="<< Previous"
                                            Font-Underline="false" ForeColor="Blue" OnClick="LinkButton1_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
</asp:Content>

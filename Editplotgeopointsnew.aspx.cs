﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class Editplotgeopointsnew : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt = 0, lng = 0;
        static int p = 0;
        static int sno = 0;
#pragma warning disable CS0414 // The field 'Editplotgeopointsnew.rname' is assigned but its value is never used
        static string rname = "";
#pragma warning restore CS0414 // The field 'Editplotgeopointsnew.rname' is assigned but its value is never used
        protected void Page_Load(object sender, EventArgs e)
        {
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;

                ddlrno.DataSource = db.get_Geo_RouteNo12(Session["userID"].ToString());
                ddlrno.DataBind();
               
            }


        }

        protected void ddlrno_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--");
        }

        protected void ddlrno_SelectedIndexChanged(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            if (ddlrno.SelectedValue != "--")
            {
                DataTable getdate = db.get_Geodate_Route12(ddlrno.SelectedValue.ToString(), Session["userID"].ToString());
                if (getdate.Rows.Count > 0 && getdate.Rows[0][0].ToString() != "")
                {
                    string ggg = ddlrno.SelectedValue.ToString();
                    string sub = ggg.Substring(0, 3);
                    if (sub == "ROU")
                    {
                        ddlrttype.SelectedValue = "4";
                    }                   
                    
                    bindpoints(ddlrno.SelectedValue.ToString());
                    binddata();
                  
                }
                else
                {
                    db.delete_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                    db.delete_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());                    
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Create Route again |');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'

                }
                GMap1.enableServerEvents = true;
            
            }
        }
        public void binddata()
        {
 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserID"] != null)
            {

                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

                //GMap1.enableScrollWheelZoom = true;
                GMap1.enableHookMouseWheelToZoom = true;
                DataTable getrno = db.get_Geornos(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());

                string TextBox1 = getrno.Rows[0][3].ToString();
                string TextBox3 = getrno.Rows[0][4].ToString();
                string VES = getrno.Rows[0][8].ToString();
                double rkm = 0;

                if (VES == "0")
                {
                    DataTable report = db.getMovinggeoData12(getrno.Rows[0][5].ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();
                    
                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        //GMap1.addControl(extMapType);
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;

                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        //GMap1.addPolyline(line);
                        GMap1.Add(line);


                    }
                }
                else if (VES == "1")
                {
                    DataTable report = db.searchOn2geoData12(Session["UserID"].ToString(), TextBox1, TextBox3, getrno.Rows[0][5].ToString());
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    double Lpkm = 0;
                    for (int i = 0; i < report.Rows.Count; i++)
                    {

                        Lpkm = Math.Truncate(rkm * 100) / 100;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
#pragma warning disable CS0618 // 'GMap.addControl(GControl)' is obsolete: 'Use Add(GControl) instead'
                        GMap1.addControl(extMapType);
#pragma warning restore CS0618 // 'GMap.addControl(GControl)' is obsolete: 'Use Add(GControl) instead'
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        ////GMap1.addPolyline(line);
                        GMap1.Add(line);

                    }
                }

            }

        }      
        
        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);

        }
        public void drawcircle(double lt, double lg, double r, int gp, string vn, string adress)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1);// 16, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            options.title = "Route No :-" + vn + "," + "Location:-" + adress;
            oMarker.options = options;

        }
        public void changed(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvrow = btn.NamingContainer as GridViewRow;
            string userid = gvmappoints.DataKeys[gvrow.RowIndex].Value.ToString();
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                db.delete_Geo_Routepointsnew(userid, Session["UserID"].ToString());
                if (ddlrno.SelectedValue != "--")
                {
                    bindpoints(ddlrno.SelectedValue.ToString());
                }

            }
        }
        public void bindpoints(string rno)
        {
            if (rno != "--")
            {
                GMap1.resetMarkers();
                GMap1.resetPolygon();
                GMap1.resetPolylines();
                DataTable dt = db.get_Geo_Routepointsnew(rno, Session["UserID"].ToString());
                for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                {
                    drawcircle(Convert.ToDouble(dt.Rows[i][3].ToString()), Convert.ToDouble(dt.Rows[i][4].ToString()), Convert.ToDouble(dt.Rows[i][5].ToString()) / 1609.3, j, rno, dt.Rows[i][6].ToString());
                }
                gvmappoints.DataSource = dt;
                gvmappoints.DataBind();
            }

        }

        protected void gvmappoints_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                sno = 1;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable rnname = db.get_Geornos(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                Label lblrnam = (Label)e.Row.FindControl("lblrnam");
                lblrnam.Text = rnname.Rows[0][7].ToString();
                Label lblsno = (Label)e.Row.FindControl("lblsno");
                lblsno.Text = sno.ToString();
                sno++;

                Button btndelete = (Button)e.Row.FindControl("Btnremove");
                if (btndelete.Text == "0")
                {
                    btndelete.Text = "Delete";
                    btndelete.Enabled = true;
                }
                else if (btndelete.Text == "1")
                {
                    btndelete.Text = "Delete";
                    btndelete.Enabled = false;
                    btndelete.BackColor = System.Drawing.Color.Gray;

                }
            }
        }
        public void refreshmap()
        {
            p = 0;
            lt = 0; lng = 0;
            txtradius.Text = string.Empty;
            txtlocation.Text = string.Empty;
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            GMap1.resetPolylines();
            bindpoints(ddlrno.SelectedValue.ToString());

        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            if (lt != 0 && lng != 0)
             {
                DataTable getrno = db.get_Geornos(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                string address = txtlocation.Text;
                int n;
               // string date = "";
                
                string date = DateTime.Now.ToString();
                


                n = db.create_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString(), lt.ToString(), lng.ToString(), txtradius.Text, address, ddlrttype.SelectedValue.ToString(), date);
                if (n == 0)
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Successfully Point Created ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                                      
                    bindpoints(ddlrno.SelectedValue.ToString());
                    refreshmap();
                    binddata();
                    btnfinish.Enabled = true;
                }
                else
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('You Reached Maximum Points ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                
                }

            }
            else
            {
                //Response.Redirect(Request.RawUrl);
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point On Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'


            }
            GMap1.enableServerEvents = true;
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            refreshmap();
            binddata();

            GMap1.enableServerEvents = true;
        }

        protected void btnfinish_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            p = 1;
            lt = 0; lng = 0;
            txtradius.Text = string.Empty;
            txtlocation.Text = string.Empty;
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            GMap1.resetPolylines();
            bindpoints(ddlrno.SelectedValue.ToString());
            BtnSubmit.Enabled = false;
            BtnClear.Enabled = false;
        }

        protected void Btn_Clear_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            p = 0;
            Response.Redirect(Request.RawUrl);
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="report.aspx.cs" Inherits="Tracking.report"
    MasterPageFile="~/TrackingMaster.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="report3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <div>
        <div style="width: 1312px; height: 42px; font-size: 25px; text-align: center; color: Blue;">
            Vehicle Day summary Report
        </div>
        <table width="100%">
            <tr>
                <td class="style1" align="right">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label1" Text="Please Select the Vehicle" runat="server"></asp:Label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                        ErrorMessage="Select the Vehicle Number" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>&nbsp;:
                </td>
                <td align="left" class="style10">
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        DataTextField="Vehicalnumber"  Height="23px" Width="188px">                     
                        
                    </asp:DropDownList>
                </td>
                <td class="style4" align="right">
                    <asp:Label ID="Label2" Text="Driver Name" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                </td>
                <td align="left" class="style14">
                    <asp:Label ID="drivername" runat="server"></asp:Label>
                </td>
                <td class="style6" align="right">  <asp:Label ID="Label9" Text="Transmission Frequency" runat="server"></asp:Label>
                    :&nbsp;
            </td>
            <td class="style2">
            <asp:Label ID="Label10" Text="60 sec" runat="server"></asp:Label>
            </td>
            </tr>
            <br />
            <tr>
                <td class="style1" align="right">
                   
                    <asp:Label ID="Label4" Text="Select Date" runat="server"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="date" ErrorMessage="*"></asp:RequiredFieldValidator>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                <td align="left" class="style10">
                    <asp:TextBox ID="date" runat="server" Width="185px"></asp:TextBox>
                             
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy"
                        TargetControlID="date">
                    </cc1:CalendarExtender>
                </td>
                <td class="style4" align="right">
                    <asp:Label ID="Label5" Text="Driver Mobile No" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                </td>
                <td align="left" class="style14">
                    <asp:Label ID="driverno"  runat="server"></asp:Label>
                </td><td class="style6" align="right"> <asp:Label ID="Label11" Text="Vehicle Maker" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; :
                </td>
                <td align="left">
                    <asp:Label ID="vehiclemodel" runat="server"></asp:Label>
                </td>
            </tr>
            
            <tr>
            <td class="style1" align="right">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label8" Text="Maximum Speed" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; :
            </td>
            <td class="style10">
            &nbsp;
            <asp:Label ID="maxspeed"  runat="server" ForeColor="Red"></asp:Label> &nbsp;
                <asp:Label ID="maxtime"  runat="server" ForeColor="Red"></asp:Label> 
            </td>
            <td class="style4">
            
            </td>
            </tr>
            
        </table>
        <div style=" height:50px;width:100%;">
        <br />
        <table width="100%">
        <tr><td class="style8"></td>
        <td class="style3">
        <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" 
                onclick="GetRecord_Click" />
        </td>
        <td class="style5">
            <asp:Button ID="Download" Text="Download Record" runat="server" 
                Width="120px" onclick="Download_Click" /></td>
        <td class="style13">
            <asp:Button ID="Refresh" Text="Refresh Record" runat="server" 
                Width="121px" onclick="Refresh_Click" /></td>
        <td class="style7"></td>
        <td></td>
        </tr>
       </table>
        <br />
            <div ><%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="allinminutes" runat="server" Text="<center>All in Minutes</center>" 
                    BorderWidth="2px" BorderColor="White" Font-Bold="true"  Width="369px" Visible="false" ForeColor="Black" BackColor="White" ></asp:Label> 
                  --%>
            
        </div>
        <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"  
                Width="80%" onrowdatabound="gv_RowDataBound" ShowFooter="true">
        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black"  />
        <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" />
        <FooterStyle Font-Bold="true" BackColor="#61A6F8" ForeColor="black" />
        <Columns>
        <asp:BoundField HeaderText="FromHours" DataField="Fhours" />
        <asp:BoundField HeaderText="ToHours" DataField="Thours" FooterText="Total" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Center" />
        <%--<asp:BoundField HeaderText="Ignition" DataField="igst"  />--%>
         <asp:TemplateField HeaderText="Ignition">
        <ItemTemplate>
        <asp:Label ID="igst9" runat="server" Text='<%# Bind("igst") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
        <asp:Label ID="ftigst" runat="server"></asp:Label>
        </FooterTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField HeaderText="Idle" DataField="idle"  />--%>
        <asp:TemplateField HeaderText="Idle">
        <ItemTemplate>
        <asp:Label ID="idle9" runat="server" Text='<%# Bind("idle") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
        <asp:Label ID="ftidle" runat="server"></asp:Label>
        </FooterTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField HeaderText="Stationary" DataField="Stat" />--%>
        <asp:TemplateField HeaderText="Stationary">
        <ItemTemplate>
        <asp:Label ID="Stat9" runat="server" Text='<%# Bind("Stat") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
        <asp:Label ID="ftstat" runat="server"></asp:Label>
        </FooterTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField HeaderText="Total K/m run" DataField="TKMR"  />--%>
        <asp:TemplateField HeaderText="Total KM run">
        <ItemTemplate>
        <asp:Label ID="TKMR9" runat="server" Text='<%# Bind("TKMR") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
        <asp:Label ID="ftTKMR" runat="server"></asp:Label>
        </FooterTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField HeaderText="Fuel Status" DataField="FuelS"  />--%>
        <asp:TemplateField HeaderText="Fuel Status">
        <ItemTemplate>
        <asp:Label ID="FuelS9" runat="server" Text='<%# Bind("FuelS") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
        <asp:Label ID="ftFuelS" runat="server"></asp:Label>
        </FooterTemplate>
        </asp:TemplateField>
        </Columns>
        <FooterStyle HorizontalAlign="Center" />
        
        </asp:GridView>
        <div>
        
       
        </div>
        
    </div>
    
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">

    <style type="text/css">
        .style2
        {
            width: 235px;
        }
        .style4
        {
            width: 187px;
        }
        .style5
        {
            width: 112px;
        }
        .style6
        {
            width: 135px;
        }
        .style7
        {
            width: 154px;
        }
        .style11
        {
            width: 149px;
        }
        .style12
        {
            width: 155px;
        }
        .style13
        {
            width: 247px;
        }
    </style>
<div style="width:100%"></div>
    <style type="text/css">
        .style1
        {
            width: 389px;
        }
        .style2
        {
            width: 244px;
        }
        .style3
        {
            width: 107px;
        }
        .style4
        {
            width: 357px;
        }
        .style5
        {
            width: 128px;
        }
        .style6
        {
            width: 175px;
        }
        .style7
        {
            width: 174px;
        }
        .style8
        {
            width: 178px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 384px;
        }
        .style2
        {
            width: 247px;
        }
        .style3
        {
            width: 113px;
        }
        .style4
        {
            width: 342px;
        }
        .style5
        {
            width: 130px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 1217px;
        }
        .style2
        {
            width: 1206px;
        }
        .style3
        {
            width: 1196px;
        }
        .style4
        {
            width: 1195px;
        }
        .style5
        {
            width: 1194px;
        }
        .style6
        {
            width: 1159px;
        }
        .style7
        {
            width: 1216px;
        }
        .style8
        {
            width: 1214px;
        }
        .style9
        {
            width: 1178px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 269px;
        }
        .style2
        {
            width: 266px;
        }
        .style4
        {
            width: 214px;
        }
        .style6
        {
            width: 230px;
        }
        .style7
        {
            width: 213px;
        }
        .style8
        {
            width: 211px;
        }
        .style9
        {
            width: 157px;
        }
        .style10
        {
            width: 218px;
        }
        .style11
        {
            width: 137px;
        }
        .style12
        {
            width: 158px;
        }
        .style13
        {
            width: 153px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 32%;
        }
        .style2
        {
            width: 31%;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 1155px;
        }
        .style2
        {
            width: 1150px;
        }
        .style3
        {
            width: 102px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 40%;
        }
        .style2
        {
            width: 41%;
        }
        .style3
        {
            width: 13%;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 331px;
        }
        .style2
        {
            width: 261px;
        }
        .style3
        {
            width: 112px;
        }
        .style4
        {
            width: 220px;
        }
        .style5
        {
            width: 148px;
        }
        .style6
        {
            width: 120px;
        }
        .style7
        {
            width: 125px;
        }
        .style9
        {
            width: 171px;
        }
        .style10
        {
            width: 179px;
        }
        .style11
        {
            width: 223px;
        }
    </style>
    <style type="text/css">
        .style2
        {
            width: 138px;
        }
        .style3
        {
            width: 140px;
        }
        .style5
        {
            width: 145px;
        }
        .style7
        {
            width: 148px;
        }
        .style10
        {
            width: 328px;
        }
        .style12
        {
            width: 184px;
        }
        .style13
        {
            width: 139px;
        }
        .style14
        {
            width: 105px;
        }
        .style15
        {
            width: 155px;
        }
        .style16
        {
            width: 397px;
        }
        .style17
        {
            width: 150px;
        }
    </style>
    <style type="text/css">
        .style1
        {
            width: 205px;
        }
        .style2
        {
            width: 233px;
        }
        .style3
        {
            width: 118px;
        }
        .style4
        {
            width: 173px;
        }
        .style5
        {
            width: 136px;
        }
        .style6
        {
            width: 188px;
        }
        .style7
        {
            width: 67px;
        }
        .style8
        {
            width: 366px;
        }
    </style>
</asp:Content>


﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Tracking
{
    public partial class EngineStatusReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["ParentUser"].ToString());
                        ddlMapTOVehicle.DataBind();

                        ddlFromHOURS.DataSource = db.GetFromHours(Session["ParentUser"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["ParentUser"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlToMINUTES.DataBind();

                        dgStatus.Visible = true;
                        dgSave.Visible = true;

                    }
                }
            }
        }

        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();

        protected void BtnFind_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 

            string AMPM, DT;
            string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
            string VES = ddlVehEngStat.SelectedValue.ToString();

            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();

            TextBox1.Text = TextBox1.Text + " " + fromtime + " " + AMPM;
            TextBox3.Text = TextBox3.Text + " " + totime + " " + DT;

            if (VES == "1")
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("VehicalNumber", typeof(string));
                dt.Columns.Add("Start Date & Time", typeof(string));
                dt.Columns.Add("End Date & Time", typeof(string));
                DataRow DR = dt.NewRow();
                DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
                DR["Start Date & Time"] = TextBox1.Text;
                DR["End Date & Time"] = TextBox3.Text;

                dt.Rows.Add(DR);
                dgSave.DataSource = dt;
                dgSave.DataBind();

                DataTable tab = db.VehEngStat(Session["ParentUser"].ToString(), TextBox1.Text, TextBox3.Text, ddlMapTOVehicle.SelectedValue.ToString());

                if (tab.Columns.Count > 0)
                {
                    tab.Columns.Add("LOC", typeof(string));

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        GeoCode objAddress = new GeoCode();

                        objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

                        StringBuilder sb = new StringBuilder();
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            string address = sb.ToString();
                            tab.Rows[i]["LOC"] = address;
                        }
                    }

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        double speed;
                        string sped;
                        speed = Convert.ToDouble(tab.Rows[i][3].ToString());
                        if (speed > 0)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            tab.Rows[i]["SPED"] = sped;
                        }
                        //else if (speed <= 3)
                        //{
                        //    sped = "0";
                        //    tab.Rows[i]["SPED"] = sped;
                        //}

                    }


                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        if (tab.Rows[i][4].ToString() == "0")
                        {
                            tab.Rows[i]["IGST"] = "Off";
                        }
                        else if (tab.Rows[i][4].ToString() == "1")
                        {
                            tab.Rows[i]["IGST"] = "On";
                        }
                    }

                }
                tab.AcceptChanges();
                dgStatus.DataSource = tab;
                dgStatus.DataBind();
                TextBox1.Enabled = false;
                TextBox3.Enabled = false;
                ddlMapTOVehicle.Enabled = false;
                ddlVehEngStat.Enabled = false;
                ddlFromHOURS.Enabled = false;
                ddlFromMINUTES.Enabled = false;
                ddlToHOURS.Enabled = false;
                ddlToMINUTES.Enabled = false;
                rbtnAMPM.Enabled = false;
                rbtnDT.Enabled = false;
                BtnFind.Enabled = false;
            }
            else if (VES == "2")
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("VehicalNumber", typeof(string));
                dt.Columns.Add("Start Date & Time", typeof(string));
                dt.Columns.Add("End Date & Time", typeof(string));
                DataRow DR = dt.NewRow();
                DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
                DR["Start Date & Time"] = TextBox1.Text;
                DR["End Date & Time"] = TextBox3.Text;

                dt.Rows.Add(DR);
                dgSave.DataSource = dt;
                dgSave.DataBind();

                DataTable tab = db.VehEngStat2(Session["ParentUser"].ToString(), TextBox1.Text, TextBox3.Text, ddlMapTOVehicle.SelectedValue.ToString());

                if (tab.Columns.Count > 0)
                {
                    tab.Columns.Add("LOC", typeof(string));

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        GeoCode objAddress = new GeoCode();

                        objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

                        StringBuilder sb = new StringBuilder();
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            string address = sb.ToString();
                            tab.Rows[i]["LOC"] = address;
                        }
                    }

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        double speed;
                        string sped;
                        speed = Convert.ToDouble(tab.Rows[i][3].ToString());
                        if (speed == 0)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            tab.Rows[i]["SPED"] = sped;
                        }
                        //else if (speed <= 3)
                        //{
                        //    sped = "0";
                        //    tab.Rows[i]["SPED"] = sped;
                        //}

                    }

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        if (tab.Rows[i][4].ToString() == "0")
                        {
                            tab.Rows[i]["IGST"] = "Off";
                        }
                        else if (tab.Rows[i][4].ToString() == "1")
                        {
                            tab.Rows[i]["IGST"] = "On";
                        }
                    }

                }
                tab.AcceptChanges();
                dgStatus.DataSource = tab;
                dgStatus.DataBind();
                TextBox1.Enabled = false;
                TextBox3.Enabled = false;
                ddlMapTOVehicle.Enabled = false;
                ddlVehEngStat.Enabled = false;
                ddlFromHOURS.Enabled = false;
                ddlFromMINUTES.Enabled = false;
                ddlToHOURS.Enabled = false;
                ddlToMINUTES.Enabled = false;
                rbtnAMPM.Enabled = false;
                rbtnDT.Enabled = false;
                BtnFind.Enabled = false;
            }
            else if (VES == "3")
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("VehicalNumber", typeof(string));
                dt.Columns.Add("Start Date & Time", typeof(string));
                dt.Columns.Add("End Date & Time", typeof(string));
                DataRow DR = dt.NewRow();
                DR["VehicalNumber"] = ddlMapTOVehicle.SelectedValue.ToString();
                DR["Start Date & Time"] = TextBox1.Text;
                DR["End Date & Time"] = TextBox3.Text;

                dt.Rows.Add(DR);
                dgSave.DataSource = dt;
                dgSave.DataBind();

                DataTable tab = db.VehEngStat3(Session["ParentUser"].ToString(), TextBox1.Text, TextBox3.Text, ddlMapTOVehicle.SelectedValue.ToString());

                if (tab.Columns.Count > 0)
                {
                    tab.Columns.Add("LOC", typeof(string));

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        GeoCode objAddress = new GeoCode();

                        objAddress = GMap1.getGeoCodeRequest(new GLatLng(Convert.ToDouble(tab.Rows[i][0]), Convert.ToDouble(tab.Rows[i][1])));

                        StringBuilder sb = new StringBuilder();
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            string address = sb.ToString();
                            tab.Rows[i]["LOC"] = address;
                        }
                    }

                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        double speed;
                        string sped;
                        speed = Convert.ToDouble(tab.Rows[i][3].ToString());
                        if (speed == 0)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            tab.Rows[i]["SPED"] = sped;
                        }
                        //else if (speed <= 3)
                        //{
                        //    sped = "0";
                        //    tab.Rows[i]["SPED"] = sped;
                        //}

                    }


                    for (int i = 0; i < tab.Rows.Count; i++)
                    {
                        if (tab.Rows[i][4].ToString() == "0")
                        {
                            tab.Rows[i]["IGST"] = "Off";
                        }
                        else if (tab.Rows[i][4].ToString() == "1")
                        {
                            tab.Rows[i]["IGST"] = "On";
                        }
                    }

                }
                tab.AcceptChanges();
                dgStatus.DataSource = tab;
                dgStatus.DataBind();
                TextBox1.Enabled = false;
                TextBox3.Enabled = false;
                ddlMapTOVehicle.Enabled = false;
                ddlVehEngStat.Enabled = false;
                ddlFromHOURS.Enabled = false;
                ddlFromMINUTES.Enabled = false;
                ddlToHOURS.Enabled = false;
                ddlToMINUTES.Enabled = false;
                rbtnAMPM.Enabled = false;
                rbtnDT.Enabled = false;
                BtnFind.Enabled = false;
            }
        }

        protected void BtnSavPrint_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=EngineStatusReport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(dgSave);
            tr1.Cells.Add(cell1);
            TableCell cell3 = new TableCell();
            cell3.Controls.Add(dgStatus);
            TableCell cell2 = new TableCell();
            cell2.Text = "&nbsp;";

            TableRow tr2 = new TableRow();
            tr2.Cells.Add(cell2);
            TableRow tr3 = new TableRow();
            tr3.Cells.Add(cell3);
            tb.Rows.Add(tr1);
            tb.Rows.Add(tr2);
            tb.Rows.Add(tr3);

            tb.RenderControl(hw);

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void BtnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeoTripdetails.aspx.cs"
    Inherits="Tracking.GeoTripdetails" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 1312px; height: 42px; font-size: 25px; text-align: center; color: Blue;">
        Vehicle Daily GeoTrip Report
    </div>
    <div style="width: 100%; height: 40px;">
        <table>
            <tr>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblvno" runat="server" Text="Select Vehicle :" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        DataTextField="Vehicalnumber" Width="130px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label1" runat="server" Text="Select Date :" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Width="130px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="TextBox1">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
                &nbsp;&nbsp;&nbsp;
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <br />
        <asp:Label ID="lbltrip" runat="server" Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label></div>
    <div style="text-align: center;">
        <asp:GridView ID="gvgeodetails" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
            HeaderStyle-ForeColor="Black" OnRowDataBound="gvgeodetails_RowDataBound" HeaderStyle-Font-Bold="true"
            HeaderStyle-Font-Size="16px" HeaderStyle-BackColor="#61A6F8">
            <Columns>
                <asp:BoundField HeaderText="Trip Count" DataField="Trip" />
                <asp:BoundField HeaderText="" DataField="time1" />
                <asp:BoundField HeaderText="" DataField="time2" />
                <asp:BoundField HeaderText="Total Time(hr)" DataField="totaltime" />
            </Columns>
        </asp:GridView>
    </div>
    <br />
    <br />
    <div style="text-align: center;">
        <asp:Button ID="btnexit" runat="server" Text="Exit" OnClick="btnexit_Click" Width="100px"
            Visible="false" />
    </div>
</asp:Content>

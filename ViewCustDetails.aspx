﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCustDetails.aspx.cs"
    Inherits="Tracking.ViewCustDetails" MasterPageFile="~/ESL.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
        <div style="text-align: center; color: Blue; height:30px;">
            <h2>
                View Customer Registration Details
            </h2>
        </div>
    </div>
    <div style="width: 1000px; height: 400px; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
        <table style="width: 100%;">
            <tr>
                <td colspan="4" style="height: 30px;">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Customer Type:
                </td>
                <td>
                    <asp:Label ID="lblCustType" runat="server"></asp:Label>
                </td>
                <td>
                    Name of the Customer/Company:
                </td>
                <td>
                    <asp:Label ID="lblCustName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Billing Address Line 1:
                </td>
                <td>
                    <asp:Label ID="lblBillAdd1" runat="server"></asp:Label>
                </td>
                <td>
                    Billing Address Line 2:
                </td>
                <td>
                    <asp:Label ID="lblBillAdd2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                </td>
                <td>
                    State:
                </td>
                <td>
                    <asp:Label ID="lblState" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Contact Number: (Area Code)
                </td>
                <td>
                    <asp:Label ID="lblAreaCode" runat="server"></asp:Label>
                    <asp:Label ID="lblConNum" runat="server"></asp:Label>
                </td>
                <td>
                   Default Mobile Number for SMS Alert:
                </td>
                <td>
                    <asp:Label ID="lblMobNum" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Registration:
                </td>
                <td>
                    <asp:Label ID="lblDOR" runat="server"></asp:Label>
                </td>
                <td>
                    Email ID:
                </td>
                <td>
                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="height: 30px;">
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Button ID="btnEditReg" runat="server" Text="Edit Registration" OnClick="btnEditReg_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnConfirmReg" runat="server" Text="Confirm Registration" OnClick="btnConfirmReg_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnLogout" runat="server" Text="Logout" OnClick="btnLogout_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

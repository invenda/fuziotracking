﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleTyresDetails.aspx.cs"
    Inherits="Tracking.VehicleTyresDetails" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="height: 10px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
            ValidationGroup="Group2" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
            ValidationGroup="Group3" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True"
            ValidationGroup="Group4" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary5" runat="server" ShowMessageBox="True"
            ValidationGroup="Group5" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary6" runat="server" ShowMessageBox="True"
            ValidationGroup="Group6" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary7" runat="server" ShowMessageBox="True"
            ValidationGroup="Group7" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary8" runat="server" ShowMessageBox="True"
            ValidationGroup="Group8" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary9" runat="server" ShowMessageBox="True"
            ValidationGroup="Group9" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary10" runat="server" ShowMessageBox="True"
            ValidationGroup="Group10" ShowSummary="False" Height="32px" />
        <asp:ValidationSummary ID="ValidationSummary11" runat="server" ShowMessageBox="True"
            ValidationGroup="Group11" ShowSummary="False" Height="32px" />
    </div>
    <script type="text/javascript" language="javascript">
        function checkenennumner() {
            var num = document.getElementById('txtNoOfTyres').value;
            if (num >= 4 && num <= 10) {

                if (num % 2 == 0)
                    return true;
                else
                    alert("please Ckeck  number of tyres you entered");

            }
            else {
                alert("please enter tyre numbers between 4-10");
            }

        }
    
    </script>
    <div style="width: 100%; height: 50px; font-size: 25px; text-align: center; color: Blue;">
        Set Tyre Mileage</div>
    <div style="width: 99%; font-size: 20px; text-align: center; margin: 0 auto; padding: 0;
        padding: 0;">
        <table width="100%">
            <tr>
                <td>
                    Select Vehicle
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        AutoPostBack="true" DataTextField="Vehicalnumber" Height="27px" Width="150px"
                        Style="margin-left: 1px" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                    </asp:DropDownList>
                    Number of tyres
                    <asp:TextBox ID="txtNoOfTyres" runat="server" Width="100px" Height="25px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoOfTyres"
                        ValidationGroup="Group1" ErrorMessage="Please Enter No tyres">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="txtNoOfTyres"
                        ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                    <asp:Button ID="btnSubmit1" runat="server" Text="Submit" Font-Bold="true" OnClick="btnSubmit1_Click"
                        ValidationGroup="Group1" OnClientClick="checkenennumner" />
                </td>
            </tr>
        </table>
        <div style="width: 100%; font-size: 25px; text-align: left; border-bottom-width: thin;
            margin: 0 auto; padding: 0; padding: 0;">
            <table>
                <tr>
                    <td id="TdSearch" runat="server" visible="false" style="width: 80px;">
                        <table border="1" style="width: 1250px">
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Label ID="lblledt" runat="server" Text=" Front Left side" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Label ID="Label1" runat="server" Text="Front Right side" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 95px">
                                    &nbsp;Front Left
                                </td>
                                <td style="width: 80px">
                                    <asp:TextBox ID="txtFtontLeft" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFtontLeft"
                                        ValidationGroup="Group2" ErrorMessage="Please Enter Left Front tyre Actuall Run Km   ">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFtontLeft"
                                        ValidationGroup="Group2" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 136px">
                                    Front left Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtFrontleftytrelimit" runat="server" Width="50px" 
                                        MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFrontleftytrelimit"
                                        ValidationGroup="Group2" ErrorMessage="Please Enter Left Front tyre Alert Run Km ">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrontleftytrelimit"
                                        ValidationGroup="Group2" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 75px">
                                    <asp:TextBox ID="txttotalleftfront" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                    &nbsp;Front Right
                                </td>
                                <td style="width: 78px">
                                    <asp:TextBox ID="txtFtontRight1" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFtontRight1"
                                        ValidationGroup="Group3" ErrorMessage="Please Enter Right Front tyre Actuall Run Km ">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFtontRight1"
                                        ValidationGroup="Group3" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 143px">
                                   Front Right Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtFronrighttytrelimit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFronrighttytrelimit"
                                        ValidationGroup="Group3" ErrorMessage="Please Enter Right Front tyre Alrt Run Km ">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtFronrighttytrelimit"
                                        ValidationGroup="Group3" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 102px">
                                    <asp:TextBox ID="txttotalleftRight" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitFrontLeft" runat="server" Text="Submit" Height="26px" OnClick="btnSubmitFrontLeft_Click"
                                        ValidationGroup="Group2" />
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitFrontRight" runat="server" Text="Submit" Height="26px" OnClick="btnSubmitFrontRight_Click"
                                        ValidationGroup="Group3" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Label ID="Label2" runat="server" Text=" Rear Left side" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Label ID="Label3" runat="server" Text="Rear Right side" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 95px">
                                    &nbsp;Rear Left 1
                                </td>
                                <td style="width: 80px">
                                    <asp:TextBox ID="txtrearleft1" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtrearleft1"
                                        ValidationGroup="Group4" ErrorMessage="Please Enter  Left Rear tyre1 Actuall Run Km ">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtrearleft1"
                                        ValidationGroup="Group4" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 136px">
                                    Rear left 1 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtrearleft1limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtrearleft1limit"
                                        ValidationGroup="Group4" ErrorMessage="Please Enter  Left Rear tyre1 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtrearleft1limit"
                                        ValidationGroup="Group4" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 75px">
                                    <asp:TextBox ID="txtrearleft1total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                    Rear Right 1
                                </td>
                                <td style="width: 78px">
                                    <asp:TextBox ID="txtrearRight1" runat="server" Width="50px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtrearRight1"
                                        ValidationGroup="Group5" ErrorMessage="Please Enter  Right Rear tyre1 Actuall Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtrearRight1"
                                        ValidationGroup="Group5" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 143px">
                                    Rear Right 1 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtreartight1limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtreartight1limit"
                                        ValidationGroup="Group5" ErrorMessage="Please Enter  Right Rear tyre1 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtreartight1limit"
                                        ValidationGroup="Group5" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 102px">
                                    <asp:TextBox ID="txtrearRight1total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearLeft1" runat="server" Text="Submit" OnClick="btnSubmitRearLeft1_Click"
                                        ValidationGroup="Group4" />
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearRight1" runat="server" Text="Submit" OnClick="btnSubmitRearRight1_Click"
                                        ValidationGroup="Group5" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 95px">
                                    Rear Left 2
                                </td>
                                <td style="width: 80px">
                                    <asp:TextBox ID="txtrearleft2" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtrearleft2"
                                        ValidationGroup="Group6" ErrorMessage="Please Enter  Left Rear tyre2 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtrearleft2"
                                        ValidationGroup="Group6" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 136px">
                                    Rear left 2 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtrearleft2limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtrearleft2limit"
                                        ValidationGroup="Group6" ErrorMessage="Please Enter  Left Rear tyre2 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                        ControlToValidate="txtrearleft2limit" ValidationGroup="Group6" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 75px">
                                    <asp:TextBox ID="txtrearleft2total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                   Rear Right 2
                                </td>
                                <td style="width: 78px">
                                    <asp:TextBox ID="txtrearRight2" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtrearRight2"
                                        ValidationGroup="Group7" ErrorMessage="Please Enter  Right Rear tyre2 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                        ControlToValidate="txtrearRight2" ValidationGroup="Group7" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 143px">
                                    Rear Right 2 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtreartight2limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtreartight2limit"
                                        ValidationGroup="Group7" ErrorMessage="Please Enter  Right Rear tyre2 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                        ControlToValidate="txtreartight2limit" ValidationGroup="Group7" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 102px">
                                    <asp:TextBox ID="txtrearRight2total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearLeft2" runat="server" Text="Submit" OnClick="btnSubmitRearLeft2_Click"
                                        ValidationGroup="Group6" />
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearRight2" runat="server" Text="Submit" OnClick="btnSubmitRearRight2_Click"
                                        ValidationGroup="Group7" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 95px">
                                    Rear Left 3
                                </td>
                                <td style="width: 80px">
                                    <asp:TextBox ID="txtrearleft3" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtrearleft3"
                                        ValidationGroup="Group8" ErrorMessage="Please Enter  Left Rear tyre3 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                        ControlToValidate="txtrearleft3" ValidationGroup="Group8" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 136px">
                                    Rear left 3 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtrearleft3limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtrearleft3limit"
                                        ValidationGroup="Group8" ErrorMessage="Please Enter  Left Rear tyre3 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                        ControlToValidate="txtrearleft3limit" ValidationGroup="Group8" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 75px">
                                    <asp:TextBox ID="txtrearleft3total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                    Rear Right 3
                                </td>
                                <td style="width: 78px">
                                    <asp:TextBox ID="txtrearRight3" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtrearRight3"
                                        ValidationGroup="Group9" ErrorMessage="Please Enter  Right Rear tyre3 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                        ControlToValidate="txtrearRight3" ValidationGroup="Group9" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 143px">
                                   Rear Right 3 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtreartight3limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtreartight3limit"
                                        ValidationGroup="Group9" ErrorMessage="Please Enter  Right Rear tyre3 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                        ControlToValidate="txtreartight3limit" ValidationGroup="Group9" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 102px">
                                    <asp:TextBox ID="txtrearRight3total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearLeft3" runat="server" Text="Submit" OnClick="btnSubmitRearLeft3_Click"
                                        ValidationGroup="Group8" />
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearRight3" runat="server" Text="Submit" OnClick="btnSubmitRearRight3_Click"
                                        ValidationGroup="Group9" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 95px">
                                    &nbsp;Rear Left 4
                                </td>
                                <td style="width: 80px">
                                    <asp:TextBox ID="txtrearleft4" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtrearleft4"
                                        ValidationGroup="Group10" ErrorMessage="Please Enter  Left Rear tyre4 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                        ControlToValidate="txtrearleft4" ValidationGroup="Group10" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 136px">
                                   Rear left 4 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtrearleft4limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtrearleft4limit"
                                        ValidationGroup="Group10" ErrorMessage="Please Enter  Left Rear tyre4 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                        ControlToValidate="txtrearleft4limit" ValidationGroup="Group10" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 75px">
                                    <asp:TextBox ID="txtrearleft4total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                                <td style="width: 104px">
                                    Rear Right 4
                                </td>
                                <td style="width: 78px">
                                    <asp:TextBox ID="txtrearRight4" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtrearRight4"
                                        ValidationGroup="Group11" ErrorMessage="Please Enter  Right Rear tyre4 Actual Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                        ControlToValidate="txtrearRight4" ValidationGroup="Group11" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 143px">
                                    Rear Right 4 Alert Km
                                </td>
                                <td style="width: 83px">
                                    <asp:TextBox ID="txtreartight4limit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtreartight4limit"
                                        ValidationGroup="Group11" ErrorMessage="Please Enter  Right Rear tyre4 Alert Run Km">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                        ControlToValidate="txtreartight4limit" ValidationGroup="Group11" ErrorMessage="Enter numbers Only!"
                                        ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 76px">
                                    Total km
                                </td>
                                <td style="width: 102px">
                                    <asp:TextBox ID="txtrearRight4total" runat="server" Width="50px" ReadOnly="true" MaxLength="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearLeft4" runat="server" Text="Submit" OnClick="btnSubmitRearLeft4_Click"
                                        ValidationGroup="Group10" />
                                </td>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmitRearRight4" runat="server" Text="Submit" OnClick="btnSubmitRearRight4_Click"
                                        ValidationGroup="Group11" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

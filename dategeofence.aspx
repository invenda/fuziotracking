﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dategeofence.aspx.cs" Inherits="Tracking.dategeofence"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="COntent1" runat="server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Delete The Point ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }
         
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="height: auto; width: 100%;">
        <div style="height:auto; width: 100%; background-color: White;">
            <table style="width: 100%; height:auto; border-color: Gray;" border="2">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblVehicle" Text="Select Vehicle:" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            AutoPostBack="true" DataTextField="Vehicalnumber" Width="150px" OnDataBound="ddlMapTOVehicle_DataBound"
                            OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblVehEngStat" Text="Circle Radius:" runat="server" Font-Bold="true"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtradius"
                            ErrorMessage="Enter radius" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtradius" runat="server" Width="130px"></asp:TextBox>
                        (In Meters)
                    </td>
                    <td align="right">
                        <asp:Label ID="Label1" Text="Select Date:" runat="server" Font-Bold="true"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                            ErrorMessage="Select the Start Date" ValidationGroup="circleinfo">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TextBox1" runat="server" Width="130px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="TextBox1">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                        <asp:Button ID="btView" runat="server" Text="SetGeoFence" Width="114px" ValidationGroup="circleinfo"
                            OnClick="btView_Click" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                        <asp:Button ID="btrefresh" Text="Refresh" runat="server" OnClick="btrefresh_Click" /><input
                            id="inpHide" type="hidden" runat="server" />
                    </td>
                </tr>
                <tr style="height: 400px; width: 100%;">
                    <td colspan="8">
                        <div >
                            <cc1:GMap ID="GMap1" runat="server" Height="400px" Width="100%" enableServerEvents="true"
                                OnClick="GMap1_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 100%; text-align: center; background-color:Gray;">
            <asp:GridView ID="geogv" runat="server" AutoGenerateColumns="false" BorderWidth="2"
                HorizontalAlign="Center" Width="100%" DataKeyNames="ID" OnRowDataBound="geogv_RowDataBound" RowStyle-ForeColor="Black">
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="16px" />
                <Columns>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<% #Eval("ID")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="SNo" DataField="sno" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Location" DataField="loc" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="550px" ItemStyle-Height="10px">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Radius(mtr)" DataField="radius" ItemStyle-Font-Size="10pt"
                        ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Date" DataField="date" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                    </asp:BoundField>
                    <%-- <asp:BoundField HeaderText="Time" DataField="time" ItemStyle-Font-Size="10pt" ItemStyle-Height="10px">
                </asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Image ID="imgval" runat="server" Height="14px" Width="14px" />
                            <asp:Label ID="lblval" runat="server" Text='<% #Eval("validate")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField HeaderText="validity" DataField="validate" ItemStyle-Font-Size="10pt"
                    ItemStyle-Height="12px"></asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Remove" ItemStyle-Height="10px">
                        <ItemTemplate>
                            <asp:Button ID="remove" runat="server" Text="Delete" AutoPostBack="true" OnClientClick="ConfirmIt()"
                                OnClick="changed" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

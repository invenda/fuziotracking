﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Globalization;

namespace Tracking
{
    public partial class VehicleTyresDetails : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        TdSearch.Visible = false;
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                        Totalrunkm(ddlMapTOVehicle.SelectedValue.ToString());
                        checktyres();

                    }
                }
            }
        }


        public void savedetails()
        {
            DataTable check = db.CheckvehicleTyres(ddlMapTOVehicle.SelectedValue.ToString());
            if (check.Rows.Count > 0)
            {
                TdSearch.Visible = true;
                Gettyredetails();
            }
            else
            {
                int num = Convert.ToInt32(txtNoOfTyres.Text);
                if (num >= 4 || num <= 10)
                {
                    if (num % 2 == 0)
                    {
                        int conform = db.InsertTyreDetails(ddlMapTOVehicle.SelectedValue.ToString(), txtNoOfTyres.Text);
                        if (conform == 1)
                        {
                            TdSearch.Visible = true;
                            Gettyredetails();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Ckeck number of tyres you entered');", true);
                    }
                }
            }
        }
        public void checktyres()
        {
            txtNoOfTyres.Text = string.Empty;
            DataTable check = db.CheckvehicleTyres(ddlMapTOVehicle.SelectedValue.ToString());
            if (check.Rows.Count > 0)
            {
                txtNoOfTyres.Text = check.Rows[0][1].ToString();

            }

        }
        public void Gettyredetails()
        {
            Clear();
            DataTable dt = db.Gettyredetails(ddlMapTOVehicle.SelectedValue.ToString());
            if (dt.Rows.Count > 0)
            {
                txtNoOfTyres.Text = dt.Rows[0][1].ToString();
                txtFtontLeft.Text = dt.Rows[0][2].ToString();
                txtFrontleftytrelimit.Text = dt.Rows[0][3].ToString();
                txtFtontRight1.Text = dt.Rows[0][5].ToString();
                txtFronrighttytrelimit.Text = dt.Rows[0][6].ToString();
                txtrearleft1.Text = dt.Rows[0][8].ToString();
                txtrearleft1limit.Text = dt.Rows[0][9].ToString();
                txtrearleft2.Text = dt.Rows[0][11].ToString();
                txtrearleft2limit.Text = dt.Rows[0][12].ToString();
                txtrearleft3.Text = dt.Rows[0][14].ToString();
                txtrearleft3limit.Text = dt.Rows[0][15].ToString();
                txtrearleft4.Text = dt.Rows[0][17].ToString();
                txtrearleft4limit.Text = dt.Rows[0][18].ToString();
                txtrearRight1.Text = dt.Rows[0][20].ToString();
                txtreartight1limit.Text = dt.Rows[0][21].ToString();
                txtrearRight2.Text = dt.Rows[0][23].ToString();
                txtreartight2limit.Text = dt.Rows[0][24].ToString();
                txtrearRight3.Text = dt.Rows[0][26].ToString();
                txtreartight3limit.Text = dt.Rows[0][27].ToString();
                txtrearRight4.Text = dt.Rows[0][29].ToString();
                txtreartight4limit.Text = dt.Rows[0][30].ToString();

                txttotalleftfront.Text = Session["totalrunkm"].ToString();
                txttotalleftRight.Text = Session["totalrunkm"].ToString();
                txtrearleft1total.Text = Session["totalrunkm"].ToString();
                txtrearleft2total.Text = Session["totalrunkm"].ToString();
                txtrearleft3total.Text = Session["totalrunkm"].ToString();
                txtrearleft4total.Text = Session["totalrunkm"].ToString();
                txtrearRight1total.Text = Session["totalrunkm"].ToString();
                txtrearRight2total.Text = Session["totalrunkm"].ToString();
                txtrearRight3total.Text = Session["totalrunkm"].ToString();
                txtrearRight4total.Text = Session["totalrunkm"].ToString();
            }
        }
        public void Clear()
        {
            txtNoOfTyres.Text = string.Empty;
            txtFtontLeft.Text = string.Empty;
            txtFrontleftytrelimit.Text = string.Empty;
            txtFtontRight1.Text = string.Empty;
            txtFronrighttytrelimit.Text = string.Empty;
            txtrearleft1.Text = string.Empty;
            txtrearleft1limit.Text = string.Empty;
            txtrearleft2.Text = string.Empty;
            txtrearleft2limit.Text = string.Empty;
            txtrearleft3.Text = string.Empty;
            txtrearleft3limit.Text = string.Empty;
            txtrearleft4.Text = string.Empty;
            txtrearleft4limit.Text = string.Empty;
            txtrearRight1.Text = string.Empty;
            txtreartight1limit.Text = string.Empty;
            txtrearRight2.Text = string.Empty;
            txtreartight2limit.Text = string.Empty;
            txtrearRight3.Text = string.Empty;
            txtreartight3limit.Text = string.Empty;
            txtrearRight4.Text = string.Empty;
            txtreartight4limit.Text = string.Empty;

        }

        protected void btnSubmit1_Click(object sender, EventArgs e)
        {

            DataTable totaltunkm = db.getTotalRunkm(ddlMapTOVehicle.SelectedValue.ToString());
            if (totaltunkm.Rows.Count > 0)
            {
                Session["totalrunkm"] = totaltunkm.Rows[0][3].ToString();
                savedetails();
            }



        }

        protected void btnSubmitFrontLeft_Click(object sender, EventArgs e)
        {

            int conform = db.UpdateFrontleftTyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtFtontLeft.Text, txtFrontleftytrelimit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Front Left Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Front Left Tyre details Not added sucessfully ....');", true);
            }

        }

        protected void btnSubmitFrontRight_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateFrontRightTyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtFtontRight1.Text, txtFronrighttytrelimit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Front Right Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Front Right Tyre details Not added sucessfully ....');", true);
            }

        }

        protected void btnSubmitRearLeft1_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateReartleft1Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearleft1.Text, txtrearleft1limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left1 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left1 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearRight1_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateRearRight1Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearRight1.Text, txtreartight1limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right1 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right1 Tyre details Not added sucessfully ....');", true);
            }

        }

        protected void btnSubmitRearLeft2_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateReartleft2Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearleft2.Text, txtrearleft2limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left2 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left2 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearRight2_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateRearRight2Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearRight2.Text, txtreartight2limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right2 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right2 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearLeft3_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateReartleft3Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearleft3.Text, txtrearleft3limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left3 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left3 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearRight3_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateRearRight3Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearRight3.Text, txtreartight3limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right3 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right3 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearLeft4_Click(object sender, EventArgs e)
        {

            int conform = db.UpdateReartleft4Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearleft4.Text, txtrearleft4limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left4 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Left4 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void btnSubmitRearRight4_Click(object sender, EventArgs e)
        {
            int conform = db.UpdateRearRight4Tyredetails(ddlMapTOVehicle.SelectedValue.ToString(), txtrearRight4.Text, txtreartight4limit.Text, Session["totalrunkm"].ToString());
            if (conform == 1)
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right4 Tyre details added sucessfully ...');", true);
            }
            else
            {
                Gettyredetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert(' Rear Right4 Tyre details Not added sucessfully ....');", true);
            }
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            TdSearch.Visible = false;
            Totalrunkm(ddlMapTOVehicle.SelectedValue.ToString());
            checktyres();
        }
        public void Totalrunkm(string vno)
        {
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
            BLClass bl12 = new BLClass();
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.getTotalRunkm(vno);
            if (dt.Rows.Count != 0)
            {
                string fdate = dt.Rows[0][2].ToString();
                double trkm = Convert.ToDouble(dt.Rows[0][3].ToString());
                string tdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                DataTable dt1 = db.Select_TotalRunkm(dt.Rows[0][1].ToString(), fdate, tdate);
                if (dt1.Rows.Count != 0)
                {
                    for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                    {
                        trkm += bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                    }
                    fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    trkm = (double)Math.Round(trkm, 2);

                }
                else
                {
                    fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                }

                db.IU_Totalrunkm(vno, fdate, trkm.ToString());
                //DataTable P = db.getPFE(vno);
                //DataTable CKMdt = db.Get_Correction_KM(vno);
                //double ckm = 0;
                //if (CKMdt.Rows[0][0].ToString() != "")
                //{
                //    ckm = Convert.ToDouble(CKMdt.Rows[0][0].ToString());
                //}
                //double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                //nw = trkm + nw + ckm;
                //lblTotKmRun.Text = nw.ToString() + "&nbsp;&nbsp;KM";
            }
            else
            {
                DataTable P = db.getPFE(vno);
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                string ftime = P.Rows[0][1].ToString();
                DateTime fdate = Convert.ToDateTime(ftime, dtfi);
                db.IU_Totalrunkm(vno, fdate.ToString(), "0");
            }

        }
    }
}
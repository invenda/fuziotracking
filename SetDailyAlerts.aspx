﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetDailyAlerts.aspx.cs"
    Inherits="Tracking.SetDailyAlerts" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="Ajaxified" Assembly="Ajaxified" Namespace="Ajaxified" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Contentpage1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <script language="javascript" type="text/javascript">
        function clientShowing(sender) {

        }
        function clientShown(sender) {

        }
        function clientHiding(sender) {

        }
        function clientHidden(sender) {
            
        }
        function selectionChanged(sender) {
            //alert(sender._selectedTime);
        }
        function ConfirmIt() {            
                 	alert('Alerts Updated Successfully');            
         }    
    </script>

    <div style="width: 1200px; margin: 0 auto; padding: 0; padding: 0; color: Black;">
        <div style="width: 100%; height: 50px; vertical-align: middle; font-size: 25px; text-align: center;
            color: Blue;">
            <asp:Label ID="lblhead" Text="Set Vehicle Daily Alerts" runat="server"></asp:Label>
            <div style="width: 100%; height: 20px;">
            </div>
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 13%;">
                        <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            AutoPostBack="true" DataTextField="Vehicalnumber" Height="23px" Width="170px"
                            Style="margin-left: 1px" OnDataBound="ddlMapTOVehicle_DataBound" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Panel ID="PnlCheck" runat="server" Visible="false">
            <div style="width: 1200px; margin: 0 auto; padding: 0; padding: 0; color: Black;"
                visible="false">
                <div style="width: 100%; height: 20px; text-align: center; text-align: left;">
                </div>
                <div>
                    <table border="2" style="width: 1200px;">
                        <tr>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Alert Status
                            </td>
                            <td style="font-weight: bold; font-size: 14px; width: 400px;" align="center">
                                Alert Type
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Hours
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Minutes
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                From Time
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                To Time
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkbvs" runat="server" AutoPostBack="true" OnCheckedChanged="chkbvs_CheckedChanged" />
                            </td>
                            <td>
                                Vehicle Stationary Alert - Duration in working/office Hours :
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txthr" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txthr"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txthr"
                                    runat="server" ErrorMessage="Enter Hours">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtmin" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtmin"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtmin"
                                    runat="server" ErrorMessage="Enter Minutes">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFtime" runat="server" Text="" Width="80px">
                                </asp:TextBox>
                                <Ajaxified:TimePicker ID="TimePicker1" runat="server" TargetControlID="txtFtime"
                                    HeaderCssClass="header" TitleCssClass="title" ItemCssClass="item" SelectedItemCssClass="selecteditem"
                                    TabCssClass="tab" SelectedTabCssClass="selectedtab" CloseOnSelection="true" OnClientShowing="clientShowing"
                                    OnClientShown="clientShown" OnClientHiding="clientHiding" OnClientHidden="clientHidden"
                                    OnClientSelectionChanged="selectionChanged"></Ajaxified:TimePicker>
                                <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFtime"
                                    ErrorMessage="Enter Time" ValidationExpression="^(1[0-2]|0[1-9]):[0-5][0-9]\040(AM|am|PM|pm)$">*</asp:RegularExpressionValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtFtime"
                                    runat="server" ErrorMessage="Enter From TIme">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTtime" runat="server" Text="" Width="80px">
                                </asp:TextBox>
                                <Ajaxified:TimePicker ID="TimePicker2" runat="server" TargetControlID="txtTtime"
                                    HeaderCssClass="header" TitleCssClass="title" ItemCssClass="item" SelectedItemCssClass="selecteditem"
                                    TabCssClass="tab" SelectedTabCssClass="selectedtab" CloseOnSelection="true" OnClientShowing="clientShowing"
                                    OnClientShown="clientShown" OnClientHiding="clientHiding" OnClientHidden="clientHidden"
                                    OnClientSelectionChanged="selectionChanged"></Ajaxified:TimePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtTtime"
                                    runat="server" ErrorMessage="Enter To TIme">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px;">
                                <asp:CheckBox ID="CkbSecAMin" runat="server" AutoPostBack="true" OnCheckedChanged="CkbSecAMin_CheckedChanged" />
                            </td>
                            <td>
                                Vehicle Security Alert - duration in Odd Hours:
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFtime1" runat="server" Text="" Width="80px">
                                </asp:TextBox>
                                <Ajaxified:TimePicker ID="TimePicker3" runat="server" TargetControlID="txtFtime1"
                                    HeaderCssClass="header" TitleCssClass="title" ItemCssClass="item" SelectedItemCssClass="selecteditem"
                                    TabCssClass="tab" SelectedTabCssClass="selectedtab" CloseOnSelection="true" OnClientShowing="clientShowing"
                                    OnClientShown="clientShown" OnClientHiding="clientHiding" OnClientHidden="clientHidden"
                                    OnClientSelectionChanged="selectionChanged"></Ajaxified:TimePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtFtime1"
                                    runat="server" ErrorMessage="Enter To TIme">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTtime1" runat="server" Text="" Width="80px">
                                </asp:TextBox>
                                <Ajaxified:TimePicker ID="TimePicker4" runat="server" TargetControlID="txtTtime1"
                                    HeaderCssClass="header" TitleCssClass="title" ItemCssClass="item" SelectedItemCssClass="selecteditem"
                                    TabCssClass="tab" SelectedTabCssClass="selectedtab" CloseOnSelection="true" OnClientShowing="clientShowing"
                                    OnClientShown="clientShown" OnClientHiding="clientHiding" OnClientHidden="clientHidden"
                                    OnClientSelectionChanged="selectionChanged"></Ajaxified:TimePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtTtime1"
                                    runat="server" ErrorMessage="Enter To TIme">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px;">
                                <asp:CheckBox ID="chkbvm" runat="server" AutoPostBack="true" OnCheckedChanged="chkbvm_CheckedChanged" />
                            </td>
                            <td>
                                Vehicle Non-Stop Moving Alert - Duration:
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txthr2" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txthr2"
                                    runat="server" ErrorMessage="Enter Hours">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txthr2"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtmin2" runat="server" Width="50px" Height="15px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtmin2"
                                    runat="server" ErrorMessage="Enter Minutes">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtmin2"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px;">
                                <asp:CheckBox ID="cbovers" runat="server" AutoPostBack="true" OnCheckedChanged="cbovers_CheckedChanged" />
                            </td>
                            <td style="width: 220px;">
                                OverSpeed Limit KM/H:
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtspeed" runat="server" Height="15px" Width="50px" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="requiredfieldvalidator31" runat="server" ControlToValidate="txtspeed"
                                    ErrorMessage="Enter Over Spedd">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtspeed"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 20px;">
                </div>
                <div>
                    <table border="2" style="width: 1200px;">
                        <tr>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Alert Status
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Alert Type
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Delay Time(1-360 Minutes)
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtdelay"
                                    ErrorMessage="Enter Delay Time">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Maximum Threshold Temperature
                            </td>
                            <td style="font-weight: bold; font-size: 14px;" align="center">
                                Minimum Threshold Temperature
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkbtemp" runat="server" AutoPostBack="true" OnCheckedChanged="chkbtemp_CheckedChanged" />
                            </td>
                            <td style="width: 200px;">
                                Set Temperature Alert:
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtdelay" runat="server" Height="15px" Width="100px" Visible="false"></asp:TextBox>
                                <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="txtdelay" ErrorMessage="Between 1 To 360"
                                    MaximumValue="360" MinimumValue="1">*</asp:RangeValidator>
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="maxtemp" runat="server" Height="15px" Width="100px" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ControlToValidate="maxtemp"
                                    runat="server" ErrorMessage="Enter Alert Temperature">*</asp:RequiredFieldValidator>
                               <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="maxtemp"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                            </td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="mintemp" runat="server" Height="15px" Width="100px" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="mintemp"
                                    runat="server" ErrorMessage="Enter Min Temperature">*</asp:RequiredFieldValidator>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="mintemp"
                                    ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div>
                <div style="width: 100%; height: 20px; text-align: left;">
                    Note : If Threshold Temperature is Out of range for set delay time a alert is generates
                </div>
                <div style="width: 100%; height: 20px; text-align: center;">
                </div>
                <div style="width: 100%; height: 20px; text-align: center;">
                    <asp:Button ID="Btn_Submit" runat="server" Text="Submit" OnClick="Btn_Submit_Click"
                        OnClientClick="if(Page_ClientValidate()) ConfirmIt()" />
                </div>
                <div style="width: 100%; height: 20px; text-align: center;">
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

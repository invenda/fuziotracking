﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Tracking
{
    public partial class Get_Manager_geovehicles : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt = db.Get_Manager_Geo(Session["UserID"].ToString());
                if (dt.Rows.Count >0)
                {
                    Binddata();
                }
                else
                {
                    lblheading.Text = "Vehicles in Geofence";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Create circle..');", true);
                    lblError.Text = "Please Create circle...";
                }
            }

        }
        private void Binddata()
        {

            DataTable vehicles = new DataTable();
            DataTable users = db.getmanagerusers(Session["UserID"].ToString());
            vehicles.Columns.Add("s_no", typeof(string));
            vehicles.Columns.Add("Vehicle_no", typeof(string));
            vehicles.Columns.Add("user_name", typeof(string));

            double lat = 0.0;
            double lang = 0.0;
            int radius = 0;
            DataTable get_latlan = db.Get_Manager_Geo_latlan(Session["UserID"].ToString());
            if (get_latlan.Rows.Count > 0)
            {
                lat = Convert.ToDouble(get_latlan.Rows[0][0].ToString());
                lang = Convert.ToDouble(get_latlan.Rows[0][1].ToString());
                radius = Convert.ToInt32(get_latlan.Rows[0][2].ToString());
                lblheading.Text = "Vehicles in" +"  "+ get_latlan.Rows[0][3].ToString();
            }
            int id = 0;
            if (users.Rows.Count > 0)
            {
                for (int i = 0; i < users.Rows.Count; i++)
                {

                    DataTable getuser_vehicles = db.GetVehicles(users.Rows[i][1].ToString());
                    if (getuser_vehicles.Rows.Count > 0)
                    {
                        for (int j = 0; j < getuser_vehicles.Rows.Count; j++)
                        {
                            string vehiclenoo = getuser_vehicles.Rows[j][0].ToString();
                            DataTable dt = db.GetVehiclePresentlocation(vehiclenoo, users.Rows[i][1].ToString());

                            if (dt.Rows.Count > 0)
                            {
                                double radius111 = (double)radius;
                                double distance = db.Getdistance_between_latlan(lat, lang, Convert.ToDouble(dt.Rows[0][0]), Convert.ToDouble(dt.Rows[0][1]));
                                double distanceinmeters = ConvertMilesToMeters(distance);
                                if (distanceinmeters <= radius111)
                                {
                                   ////DataTable branchname=db.CustRegNum(
                                    DataRow dr = vehicles.NewRow();
                                    id++;
                                    dr["s_no"] = id.ToString();
                                    dr["Vehicle_no"] = vehiclenoo.ToString();
                                    dr["user_name"] = users.Rows[i][2].ToString();
                                    vehicles.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }
            }
            if (vehicles.Rows.Count > 0)
            {
                vehicles.AcceptChanges();
                gvrecords.DataSource = vehicles; //db.getmanagerusers(Session["UserID"].ToString());
                gvrecords.DataBind();
            }
            else
            {
                lblError.Text = "No Vehicles found in the " + "  " + get_latlan.Rows[0][3].ToString();
            }
            Session["ss"] = vehicles;
        }

        public static double ConvertMilesToMeters(double miles)
        {
            return (miles * 1609.344);
        }

        protected void btnmapview_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManagerGeoVehicles_ViewInMap.aspx");
        }
    }
}
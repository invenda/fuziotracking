﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Start.aspx.cs" Inherits="Tracking.Start"
    MasterPageFile="~/ESL.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content1" runat="server">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
     function SelectSingleRadiobutton(rdbtnid) 
     {
        var rdBtn = document.getElementById(rdbtnid);
        var rdBtnList = document.getElementsByTagName("input");
        for (i = 0; i < rdBtnList.length; i++) 
         {
           if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id)
            {
              rdBtnList[i].checked = false;
            }
        }
        
     }    
        function myclick(a, b) {
            map.panTo(new GLatLng(a, b));
            //GEvent.trigger(gmarkers[i],"click");
        }
        function openTrackHistory() {
            window.open("TrackingHistory.aspx");
            return false;
        }
        function Daykm() {
            window.open("DayKMRunReport.aspx");
            return false;
            }
        function openRouteHistory() 
        {
            window.open("RouteHistory.aspx");
            return false;
        }
        function openDownloadReport()
        {
            window.open("GenerateReport.aspx"); 
            return false
        }
        function openCustomerMgnmt()
        {
            window.open("CustDetails.aspx"); 
            return false
        }
        function openViewDetails()
        {   
                
            window.open("JustView.aspx"); 
            return false
        }
        function openVehMgmt()
        {
            window.open("CreateVehicles.aspx"); 
            return false
        }
        function openVehView()
        {
            window.open("ViewCustVeh.aspx"); 
            return false
        }
        function openEngineStatusReport()
        {
            window.open("EngineStatusReport.aspx"); 
            return false
        }
        function openFuelEntry()
        {
            window.open("FuelFill.aspx"); 
            return false
        }
        function openFuelEntryRpt()
        {
            window.open("FuelRpt.aspx"); 
            return false
        }
        function openKMcor()
        {
            window.open("Corrections.aspx");
            return false
        }
        function openTotalKMRpt()
        {
        window.open("report2.aspx");
        return false;
        }
        function opengeo()
        {
        window.open("Geo.aspx");
        return false;
        }
        function openoverspeed()
        {
        window.open("OverSpeedreport.aspx");
        }
        function openalertreport()
        {
        window.open("VehicleAlertReport.aspx");
        }
        function opengeotrip()
        {
        window.open("GeoTrip.aspx");
        return false;
        }
         function alertsdetails(x)
        {        
         alert(x.join('\n'));  
        }
        function opendategeo()
        {
        window.open("dategeofence.aspx");
        return false;
        }
        function opendailyreport()
        {
        window.open("VehicalDailyReport.aspx");
        return false;
        }
        function stopreport()
        {
        window.open("Vehiclestoppagereport.aspx");
        return false;
        }
        function BSreport()
        {
        window.open("BlackSpotreport.aspx");
        return false;
        }         
        function openAlerts()
        {
        window.open("SetAlerts.aspx");
        return false;
        } 
        function opengeoview()
        {
        window.open("ViewGeoRoutePoints.aspx");
        return false;
        }
        function Towereport()
        {
        window.open("TowedReport.aspx");
        return false;
        }
        function openDaygraph()
        {   
            window.open("DayGraph.aspx"); 
            return false
        }
        function openpemplog()
        {   
            window.open("Tempdatalog.aspx"); 
            return false
        }
         function openFuelgraph()
        {     
            window.open("Fuelgraph.aspx"); 
            return false
        }
        function openpieDaygraph()
        {     
            window.open("Fuel.aspx"); 
            return false
        
        }
         function openpieDaygraph1()
        {     
            window.open("Pieday.aspx"); 
            return false
        
        }
       
    </script>

    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div style="width: auto; height: 100%; background-color: Gray;">
        <table style="width: 100%;">
            <tr>
                <td id="desktopNavbar" style="background-color: Olive;" colspan="2">
                    <ul>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="lnkCxMgnt" runat="server"
                                Text="My Account |" />
                        </a>
                            <ul id="Ul1">
                                <li><a>
                                    <asp:LinkButton ID="lnkCxView" Text="View Details" Font-Bold="true" runat="server"
                                        PostBackUrl="~/JustView.aspx"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton23" Text="Change Password" Font-Bold="true" runat="server"
                                        PostBackUrl="~/Changepassword.aspx"></asp:LinkButton></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="LinkButton1" runat="server"
                                Text="Vehicle Management |" /></a><ul id="Ul2">
                                    <li><a>
                                        <asp:LinkButton ID="lnkbtnVehMgmt" Font-Bold="true" Text="Create Vehicle" runat="server"
                                            PostBackUrl="~/CreateVehicles.aspx"></asp:LinkButton></a></li>
                                    <li><a>
                                        <asp:LinkButton ID="lnjbtnViewVeh" Font-Bold="true" Text="View Vehicles" runat="server"
                                            PostBackUrl="~/ViewCustVeh.aspx"></asp:LinkButton></a> </li>
                                    <li><a>
                                        <asp:LinkButton ID="LinkButton45" Font-Bold="true" Text="Set Alerts" runat="server"></asp:LinkButton></a>
                                        <ul id="Ul10">
                                            <li><a>
                                                <asp:LinkButton ID="LinkButton15" Font-Bold="true" Text="Daily Alert" runat="server"
                                                    PostBackUrl="~/SetDailyAlerts.aspx"></asp:LinkButton></a></li>
                                            <li><a>
                                                <asp:LinkButton ID="LinkButton35" Font-Bold="true" Text="General Alert" runat="server"
                                                    PostBackUrl="~/SetGeneralAlert.aspx"></asp:LinkButton></a> </li>
                                        </ul>
                                    </li>
                                </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" ID="lbtnAdmin" Text="Configure |" Font-Size="13px"
                                runat="server" PostBackUrl="AssignDevicesToVehicles.aspx"></asp:LinkButton></a>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" ID="lnkbtnRouteReplay" Text="Replay |" Font-Size="13px"
                                runat="server"></asp:LinkButton></a>
                            <ul id="Ul9">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton26" Font-Bold="true" Text="Route Replay" runat="server"
                                        PostBackUrl="~/TrackingHistory.aspx"></asp:LinkButton></a></li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton30" Font-Bold="true" Text="Towed Replay" runat="server"
                                        PostBackUrl="~/TowedRouteReplay.aspx"></asp:LinkButton></a> </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="lblReport" runat="server"
                                Text="Report |" /></a>
                            <ul id="MenueReports">
                                <li><a>
                                    <asp:LinkButton ID="lbtvr" Text="Daily Report" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul id="Ul5">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton32" Text="Day KM Report" runat="server" Font-Bold="true"
                                                OnClientClick="Daykm()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton10" Text="Day Activity" runat="server" Font-Bold="true"
                                                OnClientClick="opendailyreport()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton11" Text="Stoppage summary" runat="server" Font-Bold="true"
                                                OnClientClick="stopreport()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton31" Text="Towed summary" runat="server" Font-Bold="true"
                                                OnClientClick="Towereport()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton38" Text="Temparature data log" runat="server" Font-Bold="true"
                                                OnClientClick="openpemplog()"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton18" Text="Monthly Report" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton17" Text="KM Report" runat="server" Font-Bold="true"
                                                PostBackUrl="~/MonthlyKMReport.aspx"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton16" Text="Temperature Report" runat="server" Font-Bold="true"
                                                PostBackUrl="~/TemperatureReport.aspx"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="lbtnother" Text="Other Report" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul id="Ul6">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton2" Text="Day Summary" runat="server" Font-Bold="true"
                                                OnClientClick="openTotalKMRpt()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton5" Text="Over Speed Summary" runat="server" Font-Bold="true"
                                                OnClientClick="openoverspeed()"></asp:LinkButton></a> </li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton4" Text="Alert Summary" runat="server" Font-Bold="true"
                                                OnClientClick="openalertreport()"></asp:LinkButton></a> </li>
                                        <li><a>
                                            <asp:LinkButton ID="btnFuelEntryRpt" Text="Fuel Entry" runat="server" Font-Bold="true"
                                                OnClientClick="openFuelEntryRpt()"></asp:LinkButton></a> </li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton12" Text="Black Spot" runat="server" Font-Bold="true"
                                                OnClientClick="BSreport()"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="lnkbtnFuelMgmt" runat="server"
                                Text="Fuel Management |" /></a><ul id="Ul3">
                                    <li><a>
                                        <asp:LinkButton ID="lnkbtnFuelEntry" Text="Fuel Voucher Entry" Font-Bold="true" runat="server"
                                            OnClientClick="openFuelEntry()"></asp:LinkButton></a></li>
                                    <li><a>
                                        <asp:LinkButton ID="lnkbtnKMCorrection" Text="Kilometer Corrections" runat="server"
                                            Font-Bold="true" OnClientClick="openKMcor()"></asp:LinkButton></a> </li>
                                </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="LinkButton3" runat="server"
                                Text="GeoFencing |"></asp:LinkButton></a>
                            <ul id="Ul4">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton24" Text="Create Route" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton25" Text="Create Route" runat="server" Font-Bold="true"
                                                PostBackUrl="~/CreateRoute.aspx"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton28" Text="Delete Route" runat="server" Font-Bold="true"
                                                PostBackUrl="~/DeleteRoute.aspx"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton29" Text="Plot/Edit Route" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton6" Text="Plot Geo Points" runat="server" Font-Bold="true"
                                                PostBackUrl="~/plotGeoPoints.aspx"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton7" Text="Edit/Delete Points" runat="server" Font-Bold="true"
                                                PostBackUrl="~/CreateGeoRoute.aspx"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="LinkButton14" runat="server"
                                Text="MIS |"></asp:LinkButton></a>
                            <ul id="Ul8">
                                <li><a>
                                    <asp:LinkButton ID="LinkButton19" Text="Graphs" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                    <ul id="Ul7">
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton13" Text="Temperature Graph" runat="server" Font-Bold="true"
                                                OnClick="LinkButton13_Click"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton33" Text="Day summury Graph" runat="server" Font-Bold="true"
                                                OnClientClick="openDaygraph()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton36" Text="Day kilometer Graph" runat="server" Font-Bold="true"
                                                OnClientClick="openFuelgraph()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton37" Text="Fuel Graph" runat="server" Font-Bold="true"
                                                OnClientClick="openpieDaygraph()"></asp:LinkButton></a></li>
                                        <li><a>
                                            <asp:LinkButton ID="LinkButton39" Text="pai Graph" runat="server" Font-Bold="true"
                                                OnClientClick="openpieDaygraph1()"></asp:LinkButton></a></li>
                                    </ul>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton20" Text="ROI Calculation" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton21" Text="Vehicle Audit" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton22" Text="Cost Analysis" runat="server" Font-Bold="true"></asp:LinkButton></a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 12px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="LinkButton34" runat="server"
                                Text="Trip Management|"></asp:LinkButton></a>
                            <ul>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton8" Text="Trip Assigning" runat="server" Font-Bold="true"
                                        PostBackUrl="~/RouteAssigntoVehicle.aspx"></asp:LinkButton></a> </li>
                                <li><a>
                                    <asp:LinkButton ID="LinkButton27" Text="Trip Report" runat="server" Font-Bold="true"
                                        PostBackUrl="~/VehiclebasedGeoReport.aspx"></asp:LinkButton></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="LinkButton9" runat="server"
                                Text="Multi View |" OnClick="LinkButton9_Click"></asp:LinkButton></a> </li>
                        <li><a href="javascript:void(0)" style="color: Black; font-size: 14px;">
                            <asp:LinkButton ForeColor="White" Font-Size="13px" ID="lbtnLogOut" runat="server"
                                Text="Logout" OnClick="lbtnLogOut_Click"></asp:LinkButton></a> </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%; height: 40px;">
                    <table id="pnlToolbar" class="submenu-control" style="vertical-align: middle;" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td style="vertical-align: middle; border-bottom-style: solid; height: 40px; width: 200px;
                                    border-bottom-width: thin;">
                                    <div style="height: 20px; width: 200px; background-color: Green; border-bottom-style: solid;
                                        border-bottom-width: thin; text-align: center;">
                                        <asp:Label ID="lblmessage" runat="server" Font-Bold="true" ForeColor="White" Text="General Alerts:"></asp:Label>
                                    </div>
                                    <div style="height: 20px; width: 200px;">
                                    </div>
                                    <div>
                                        <asp:Timer ID="Timer1" runat="server" Interval="50000" ontick="Timer1_Tick">
                                        </asp:Timer>
                                </td>
                                <td style="width: 100%; text-align: left; border-bottom-style: solid; border-bottom-width: thin;">
                                    <div style="height: 20px; border-bottom-style: solid; border-bottom-width: thin;">
                                        <table style="width: 100%; height: 20px;">
                                            <tr>
                                                <td style="width: 50%; height: 100%; vertical-align: middle;" align="center">
                                                    <asp:Label ID="lblmalerts" runat="server" Font-Bold="true" Visible="false"></asp:Label>
                                                </td>
                                                <td style="width: 50%; height: 20px; vertical-align: middle; font-weight: bold;"
                                                    align="right">
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" ForeColor="Black" Text="Engine Status :"></asp:Label>
                                                    &nbsp;&nbsp;
                                                    <asp:Image ID="imgst" runat="server" Width="14px" Height="14px" />
                                                    <asp:Label Text="Stopped" runat="server" ID="Lbl1"></asp:Label>&nbsp;&nbsp;
                                                    <asp:Image ID="imgidl" runat="server" Width="14px" Height="14px" />
                                                    <asp:Label Text="Idle" runat="server" ID="lbl2"></asp:Label>&nbsp;&nbsp;
                                                    <asp:Image ID="imgmv" runat="server" Width="14px" Height="14px" />
                                                    <asp:Label ID="lbl3" runat="server" Text="Moving"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    Current Time &nbsp;:
                                                    <asp:Label ID="LblSCT" runat="server" Font-Bold="True" Font-Italic="False"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 20px; border-top-width: medium; vertical-align: middle;">
                                        <table style="width: 100%; height: 100%;">
                                                <caption>
                                                    <marquee>
                                                            <asp:Label ID="lblalerts2" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                                                            <asp:Label ID="lbloffalerts" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label></marquee>
                                                </caption>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    
    <br />
    <div style="position: relative; vertical-align: top; height: 400px; overflow: auto;
        background-color: White; top: 0px; left: 0px;">
        
        <asp:UpdatePanel ID="updatepannel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" BackColor="#CCFF99"
                    BorderWidth="3px" HorizontalAlign="Center" OnRowDataBound="GV_RowDataBound" BorderColor="#FF33CC">
                    <RowStyle BackColor="#CCFFFF" Height="5px" />
                    <Columns>
                        <asp:TemplateField HeaderText="select vno" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:RadioButton ID="rbtn" runat="server" OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                    Width="20px" Height="20px" AutoPostBack="true" OnCheckedChanged="rbtn_Click" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle RTO no" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <asp:Label ID="lblvehicleno" CommandName="AddToCart" runat="server" Text='<%# Bind("VehicalNumber") %>'
                                    Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="90px" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Engine status" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:Image ID="Simg" runat="server" Height="16px" Width="16px" BackColor="Transparent" />
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="status" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("Status") %>' Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit seriol no" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblserial" Text='<%# Bind("Serial_no") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="70px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Data" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="150px">
                            <ItemTemplate>
                                <asp:Label ID="lbllastdata" Text='<%# Bind("Lastdata") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="150px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Gps" ItemStyle-HorizontalAlign="Right ">
                            <ItemTemplate>
                                <asp:Label ID="lblgps" Text='<%# Bind("Gps") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Over speed">
                            <ItemTemplate>
                                <asp:Label ID="lblovs" Text='<%# Bind("OVS") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ignition" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Label ID="lbling" Text='<%# Bind("ingnition") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Battety voltage" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="30px">
                            <ItemTemplate>
                                <asp:Label ID="lblbvolt" Text='<%# Bind("bvolt") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver mobile" ItemStyle-HorizontalAlign="Right "
                            HeaderStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lbldmobile" Text='<%# Bind("dmobile") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Driver Name" ItemStyle-HorizontalAlign="Right " HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="lbldname" Text='<%# Bind("Dname") %>' runat="server" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="100px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" CssClass="button " />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Speed" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnspeed" runat="server" OnClick="btnspeed_Click" CssClass="button  " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fuel" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnfuel" runat="server" OnClick="btnfuel_Click" CssClass="button  " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today KM run" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btnKM" runat="server" OnClick="btnKM_Click" CssClass="button " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Temp" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40px">
                            <ItemTemplate>
                                <asp:Button ID="btntemp" runat="server" OnClick="btntemp_Click" CssClass="button " />
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Availability" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnavail" runat="server" OnClick="btnavail_Click" CssClass="button  " />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Alert" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnalert" runat="server" OnClick="btnalert_Click" CssClass="button " />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#006600" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#99CCFF" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnShowPopup"
        PopupControlID="pnlpopup" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Height="200px" Width="400px"
        Style="display: none">
        <table width="100%" style="border: Solid 3px #D55500; width: 100%; height: 100%"
            cellpadding="0" cellspacing="0">
            <tr style="background-color: #D55500">
                <td colspan="2" style="height: 10%; color: White; font-weight: bold; font-size: larger"
                    align="center">
                    User Details
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 45%">
                    UserId:
                </td>
                <td>
                    <asp:Label ID="lblID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    UserName:
                </td>
                <td>
                    <asp:Label ID="lblusername" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

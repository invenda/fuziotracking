﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class Tempreport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
#pragma warning disable CS0414 // The field 'Tempreport.rcount' is assigned but its value is never used
        int rcount = 0;
#pragma warning restore CS0414 // The field 'Tempreport.rcount' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'Tempreport.count' is assigned but its value is never used
        int count = 0;
#pragma warning restore CS0414 // The field 'Tempreport.count' is assigned but its value is never used
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            bindmonthtemp();
        }
        public void bindmonthtemp()
        {
            DateTime fromday = Convert.ToDateTime(txtdate.Text);
            DateTime today = Convert.ToDateTime(txtdate1.Text);
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
            int month = Convert.ToInt32(fromday.Month.ToString());
            DataTable fre = db.Getfreez(Session["Username"].ToString());


            if (fromday < today)
            {
                db.Delete_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fromday.Month.ToString());
                DataTable dt = new DataTable();
                dt.Columns.Add("date", typeof(string));
                DataTable dttemp = db.Select_TempMonth(ddlMapTOVehicle.SelectedValue.ToString(), fromday.Month.ToString());
                DataTable dtkm = new DataTable();
                DataTable dtkm1 = db.get_TempMonth(Vno, fromday.Month);
                dtkm = dtkm1.Clone();
                if (fre.Rows[0][1].ToString() == "Yes")
                {

                    for (int i = 0; i < 31; i++)
                    {
                        if (fromday <= today)
                        {
                            if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                            {
                                db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), dt.Rows[i][0].ToString(), month.ToString());

                                int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                                int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                                int maxtemp1 = maxtemp;
                                double maxxtemp;
                                double matemp;

                                string da = fromday.ToString();
                                DataRow dr = dt.NewRow();
                                dr["date"] = da;
                                //dt.ImportRow(dr);
                                dt.Rows.Add(dr);

                                DateTime day = Convert.ToDateTime(dt.Rows[i][0]);
                                DateTime fdate = Convert.ToDateTime(dt.Rows[i][0]);
                                DataTable dthours = db.getRhours();
                                if (maxtemp1 > 0)
                                {
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert(day.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, day.ToString("dd/MM/yyyy"));

                                    }
                                }
                                else if (maxtemp1 < 0)
                                {
                                    maxxtemp = Convert.ToInt32(gadt.Rows[0][9].ToString());
                                    if (maxxtemp <= -10)
                                    {
                                        matemp = maxxtemp / 1.7;
                                    }
                                    else
                                    {
                                        matemp = maxxtemp;
                                    }
                                    double mmaxtemp = Convert.ToDouble(matemp * 3.3);
                                    double maxtempp = mmaxtemp * -1 + 1000;
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert2old(day.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtempp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, day.ToString("dd/MM/yyyy"));

                                    }
                                }
                                fromday = fromday.AddDays(1);
                            }



                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 31; i++)
                    {
                        if (fromday <= today)
                        {
                            if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                            {
                                db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), dt.Rows[i][0].ToString(), month.ToString());

                                int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                                int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                                int maxtemp1 = maxtemp;
#pragma warning disable CS0168 // The variable 'maxxtemp' is declared but never used
                                double maxxtemp;
#pragma warning restore CS0168 // The variable 'maxxtemp' is declared but never used
#pragma warning disable CS0168 // The variable 'matemp' is declared but never used
                                double matemp;
#pragma warning restore CS0168 // The variable 'matemp' is declared but never used

                                string da = fromday.ToString();
                                DataRow dr = dt.NewRow();
                                dr["date"] = da;
                                //dt.ImportRow(dr);
                                dt.Rows.Add(dr);

                                DateTime day = Convert.ToDateTime(dt.Rows[i][0]);
                                DateTime fdate = Convert.ToDateTime(dt.Rows[i][0]);
                                DataTable dthours = db.getRhours();
                                if (maxtemp1 > 0)
                                {
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert(day.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, day.ToString("dd/MM/yyyy"));

                                    }
                                }
                                else if (maxtemp1 < 0)
                                {
                                    maxtemp = maxtemp1 * -1 + 1000;
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert1old(day.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), day.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, day.ToString("dd/MM/yyyy"));

                                    }
                                }
                                fromday = fromday.AddDays(1);
                            }

                        }
                    }
                }
            }
        }
    }
}

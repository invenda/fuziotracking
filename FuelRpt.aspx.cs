﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

namespace Tracking
{
    public partial class FuelRpt : System.Web.UI.Page
    {
        public string connectionstring = ConfigurationManager.AppSettings["Connectionstring"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null)
                {
 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserID"] != null)
                    {
                        if (Session["UserRole"].ToString() == "3")
                        {
                            DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                            ddlVeh.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                            ddlVeh.DataBind();
                        }
                        else
                        {
                            ddlVeh.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                            ddlVeh.DataBind();
                        }
                    }
                }
            }
        }
        protected void GetRefNo(object sender, EventArgs e)
        {
            string RegNUM = ddlVeh.SelectedValue.ToString();//Session["RegNo"].ToString();
            DateTime FuRecDT = Convert.ToDateTime(txtVehFuRec.Text);
 
 
            DBClass db = new DBClass();
 
 
            DataTable FuTab = db.GetFuRec(RegNUM, FuRecDT);
            gvrecords.DataSource = FuTab;
            gvrecords.DataBind();
        }
        protected void BtnSavPrint_Click(object sender, EventArgs e)
        {
            if (txtVehFuRec.Text != "")
            {
                Response.Clear();

                Response.AddHeader("content-disposition", "attachment;filename=FuelEntryReport.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                gvrecords.Columns[3].Visible = false;
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //PrepareForExport(dgSave);
                //PrepareForExport(dgSearch);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvrecords);
                tr1.Cells.Add(cell1);
                TableCell cell3 = new TableCell();
                //cell3.Controls.Add(dgGetRec);
                TableCell cell2 = new TableCell();
                cell2.Text = "&nbsp;";
                //if (rbPreference.SelectedValue == "2")
                //{
                //    tr1.Cells.Add(cell2);
                //    tr1.Cells.Add(cell3);
                //    tb.Rows.Add(tr1);
                //}
                //else
                //{
                TableRow tr2 = new TableRow();
                tr2.Cells.Add(cell2);
                TableRow tr3 = new TableRow();
                tr3.Cells.Add(cell3);
                tb.Rows.Add(tr1);
                tb.Rows.Add(tr2);
                tb.Rows.Add(tr3);
                //}
                tb.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                //Response.Clear();
                //Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
                //Response.Charset = "";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "application/vnd.xls";

                //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

                //dgSearch.RenderControl(htmlWrite);
                //Response.Write(stringWrite.ToString());
                //Response.End();
            }
        }
        protected void gvrecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //getting username from particular row
                string username = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Fuel_Challan_No"));
                //identifying the control in gridview
                LinkButton lnkbtnresult = (LinkButton)e.Row.FindControl("lnkdelete");
                //raising javascript confirmationbox whenver user clicks on link button
                lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + username + "')");
            }
        }
        protected void lnkdelete_Click(object sender, EventArgs e)
        {

            LinkButton lnkbtn = sender as LinkButton;
            //getting particular row linkbutton
            GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
            //getting userid of particular row
            string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
            string username = gvrow.Cells[0].Text;

            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();

            SqlCommand cmd = new SqlCommand("delete from Fuel_Entry where Fuel_Challan_No='" + userid + "'", sqlconn);
            cmd.ExecuteNonQuery().ToString();
            sqlconn.Close();
            //if (result == "1")
            //{
            BindDetails();
            //Displaying alert message after successfully deletion of user
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('" + username + " details deleted successfully')", true);
            //}
        }

        private void BindDetails()
        {
            string RegNUM = ddlVeh.SelectedValue.ToString();
            DateTime FuRecDT = Convert.ToDateTime(txtVehFuRec.Text);
            //connection open
            SqlConnection sqlconn = new SqlConnection(connectionstring);
            sqlconn.Open();
            //sql command to execute query from database
            SqlCommand cmd = new SqlCommand("Select * from Fuel_Entry where Fuel_Vehicle='" + RegNUM + "' and Fuel_DT='" + FuRecDT + "'", sqlconn);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            //Binding data to gridview
            gvrecords.DataSource = ds;
            gvrecords.DataBind();
            sqlconn.Close();

        }        

        protected void BtnClear_Click1(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class ViewVehDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        
                        string VehRegNUM = Session["VehRegNo"].ToString();
                        DataTable ViewVeh = db.ViewVehDetails(VehRegNUM,lblVehRegNum.Text, Session["UserID"].ToString(), lblDriName.Text, lblVehModel.Text, lblDriMobNum.Text, lblVehMake.Text, lblDriAddress.Text, lblVehInsNum.Text, lblDriLicNum.Text, lblVehInsExpDt.Text, lblDriLicExpDt.Text, lblTankCap.Text, lblBatVolt.Text, lblExistMetRead.Text, lblKiloPulse.Text, lblMobNum.Text, lblSerialNum.Text, lblSIMProvider.Text, lblDOP.Text, lblGPRSActivatedDT.Text, lblDealerName.Text, lblRechargePeriod.Text);
                        lblVehRegNum.Text = ViewVeh.Rows[0]["Vehicle_number"].ToString();
                        lblDriName.Text = ViewVeh.Rows[0]["Driver_Name"].ToString();
                        lblVehModel.Text = ViewVeh.Rows[0]["Vehicle_Model"].ToString();
                        lblDriMobNum.Text = ViewVeh.Rows[0]["Driver_Mobile_Number"].ToString();
                        lblVehMake.Text = ViewVeh.Rows[0]["Vehicle_Make"].ToString();
                        lblDriAddress.Text = ViewVeh.Rows[0]["Driver_Address"].ToString();
                        lblVehInsNum.Text = ViewVeh.Rows[0]["Vehicle_Insurance_Number"].ToString();
                        lblDriLicNum.Text = ViewVeh.Rows[0]["Driving_Licence_Number"].ToString();
                        lblVehInsExpDt.Text = ViewVeh.Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
                        lblDriLicExpDt.Text = ViewVeh.Rows[0]["Driving_Licence_Expiry_Date"].ToString();
                        lblTankCap.Text = ViewVeh.Rows[0]["Tank_Capacity"].ToString();
                        lblBatVolt.Text = ViewVeh.Rows[0]["Battery_Voltage"].ToString();
                        lblExistMetRead.Text = ViewVeh.Rows[0]["Existing_Meter_Reading"].ToString();
                        lblKiloPulse.Text = ViewVeh.Rows[0]["Kilometer_Pulse"].ToString();
                        lblMobNum.Text = ViewVeh.Rows[0]["Mobile_Number"].ToString();
                        lblSerialNum.Text = ViewVeh.Rows[0]["Serial_Number"].ToString();
                        lblSIMProvider.Text = ViewVeh.Rows[0]["SIM_Provider"].ToString();
                        lblDOP.Text = ViewVeh.Rows[0]["Date_of_Purchase"].ToString();
                        lblGPRSActivatedDT.Text = ViewVeh.Rows[0]["GPRS_Activated_Date"].ToString();
                        lblDealerName.Text = ViewVeh.Rows[0]["Dealer_Name"].ToString();
                        lblRechargePeriod.Text = ViewVeh.Rows[0]["Recharge_Period"].ToString();
                        lblrtodate.Text = ViewVeh.Rows[0]["Rto_Renewal_Date"].ToString();
                        lblrtoname.Text = ViewVeh.Rows[0]["Rto_Devision_name"].ToString();
                        lbloverspeed.Text = ViewVeh.Rows[0]["Overspeed"].ToString();
                        lblconfdate.Text = ViewVeh.Rows[0]["Contract_Fromdate"].ToString();
                        lblcontdate.Text = ViewVeh.Rows[0]["Contract_Todate"].ToString();
                        lblcontname.Text = ViewVeh.Rows[0]["Contract_Name"].ToString();
                        lblfcdate.Text = ViewVeh.Rows[0]["Rto_fc_date"].ToString();
                        lblemsdate.Text = ViewVeh.Rows[0]["Emission_date"].ToString();
                        lblroadtaxdate.Text = ViewVeh.Rows[0]["Road_tax_date"].ToString();

                    }
                }
            }
        }

        protected void btClose_Click(object sender, EventArgs e)
        {
            //Response.Write("<script language=javascript> window.close();</script>");
            //Response.End();
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Response.Redirect("Track.aspx?username=" + Session["UserName"].ToString());

        }
    }
}

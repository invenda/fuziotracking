﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteRoute.aspx.cs" Inherits="Tracking.DeleteRoute"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Delete The Point ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }            
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 600px; height: 400px; margin: 0 auto; padding: 0; padding: 0;
        color: Black;">
        <div id="sublogo" style="text-align: center; color: Blue;">
            <h2>
                <a>Delete/Display Route</a>
            </h2>
        </div>
        <div style="height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
            <input id="inpHide" type="hidden" runat="server" />
        </div>
        <div>
            <table style="width: 600px; height: auto;">
                <tr>
                    <td>
                        Select Route No
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlrno"
                            ValidationGroup="Group1" ErrorMessage="Select Route No" Operator="NotEqual" ValueToCompare="--Select Route--">*</asp:CompareValidator>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlrno" runat="server" DataValueField="Route_No" Width="150px"
                            DataTextField="Route_No" AutoPostBack="true" OnDataBound="ddlrno_DataBound" OnSelectedIndexChanged="ddlrno_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Route Type
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlrttype" runat="server" Width="150px">
                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                            <asp:ListItem Text="TRIP BASIS" Value="0"></asp:ListItem>
                            <asp:ListItem Text="DAILY BASIS" Value="1"></asp:ListItem>
                            <asp:ListItem Text="DATE BASIS" Value="2"></asp:ListItem>
                            <asp:ListItem Text="DATE/TIME BASIS" Value="3"></asp:ListItem>
                             <asp:ListItem Text="ROUTE BASIS" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Route Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtrname" runat="server" AutoCompleteType="Disabled" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    <asp:Button ID="Btn_del" runat="server" Text="Delete" Visible="false" 
                            onclick="Btn_del_Click" />
                        <asp:Button ID="Btn_submit" runat="server" Text="Delete" OnClientClick="if(Page_ClientValidate()) ConfirmIt()"
                            ValidationGroup="Group1" OnClick="Btn_submit_Click" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Btn_clr" runat="server" Text="Clear" OnClick="Btn_clr_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" Width="100%"
                            DataKeyNames="Vehicle_number" HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black"
                            CellPadding="3">
                            <Columns>
                                <asp:BoundField DataField="Vehicle_number" HeaderText="Vehical Number" />
                                <asp:BoundField DataField="Route_Name" HeaderText="Route Name" />
                                <asp:TemplateField HeaderText="Function" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" Font-Underline="false">Assigned </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class TrackingHistory : System.Web.UI.Page
    {

        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}";


        DBClass db = new DBClass();
 
 
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
        BLClass bl12 = new BLClass();
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
        protected void Page_Load(object sender, EventArgs e)
        {
            GMapUIOptions options = new GMapUIOptions();
            options.maptypes_hybrid = true;
            options.keyboard = false;
            options.maptypes_physical = false;
            options.zoom_scrollwheel = true;
            GMap1.Add(new GMapUI(options));
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
            // Response.AppendHeader("Refresh", "10; URL=" + Request.Url.AbsoluteUri);
            if (!IsPostBack)
            {
                //Timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"].ToString());
                if (Session["UserRole"] != null)
                {
 
 
                    DBClass db = new DBClass();
 
 
                    if (Session["UserID"] != null)
                    {
                        if (Session["UserRole"].ToString() == "3")
                        {
                            ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                            ddlMapTOVehicle.DataBind();
                            //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                            //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                            //ddlMapTOVehicle.DataBind();
                        }
                        else
                        {
                            ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                            ddlMapTOVehicle.DataBind();
                        }

                        ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlToMINUTES.DataBind();


                        string sStreetAddress = null;
                        string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                        Subgurim.Controles.GeoCode GeoCode = default(Subgurim.Controles.GeoCode);

                        sStreetAddress = "Jaynagar, Bangalore";
                        // GeoCode = GMap1.getGeoCodeRequest(sStreetAddress, sMapKey);
                        // double lat = GeoCode.Placemark.coordinates.lat;
                        // Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(12.9156065, 77.5791356);

                        //GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        //GMap1.addControl(extMapType);

                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, sStreetAddress, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);
                        if (jsonresponse.results.Count > 0)
                        {
                            string lat = jsonresponse.results[0].geometry.location.lat;
                            string lng = jsonresponse.results[0].geometry.location.lng;
                            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(double.Parse(lat), double.Parse(lng));
                            GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                        }
                       // GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                        imgst.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
                        imgidl.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                        imgmv.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                        imgfuel.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_purple.png";
                    }
                }
            }
        }

        protected void btView_Click(object sender, EventArgs e)
        {
            DateTime dhdh = Convert.ToDateTime(txtdate.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                binddata();

            }
            else
            {
                binddataold();

            }
        }
        public void binddata()
        {
            if (Session["UserID"] != null)
            {
                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

                GMap1.resetMarkers();
                GMap1.resetPolylines();

                GMap1.enableHookMouseWheelToZoom = true;

                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
                string VES = ddlVehStatRpt.SelectedValue.ToString();

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                string TextBox1 = txtdate.Text + " " + fromtime + " " + AMPM;
                string TextBox3 = txtdate.Text + " " + totime + " " + DT;
                double rkm = 0;
                if (VES == "1")
                {
                    string vhno = ddlMapTOVehicle.SelectedValue.ToString();
                    DataTable report = db.getMovingData(ddlMapTOVehicle.SelectedValue.ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));

                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    int ovs = 0;
                    DataTable vdt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        ovs = Convert.ToInt32(vdt.Rows[0][3]);
                        ovs = Convert.ToInt32(ovs / 1.85);
                    }
                    double Lpkm = 0;
                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        if (i > 0)
                        {
                            rkm += bl12.CalcDistance(Convert.ToDouble(report.Rows[i][0].ToString()), Convert.ToDouble(report.Rows[i][1].ToString()), Convert.ToDouble(report.Rows[i - 1][0].ToString()), Convert.ToDouble(report.Rows[i - 1][1].ToString()));
                        }
                        Lpkm = Math.Truncate(rkm * 100) / 100;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            if (Convert.ToInt32(report.Rows[i][3]) == 0)
                            {
                                sPin = new SPin(0.5, 0, System.Drawing.Color.Blue, 14, PinFontStyle.bold, "");
                            }

                            else if (ovs != 0 && Convert.ToInt32(report.Rows[i][3]) > ovs)
                            {

                                sPin = new SPin(0.5, 0, System.Drawing.Color.Yellow, 14, PinFontStyle.bold, "");

                            }
                            else if (Convert.ToInt32(report.Rows[i][4]) !=0 && Convert.ToInt32(report.Rows[i][3]) <2)
                            {

                                sPin = new SPin(0.5, 0, System.Drawing.Color.Purple, 14, PinFontStyle.bold, "");

                            }
                            else
                            {
                                sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                            }
                        }

                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();

                        options.icon = icon;

                        double speed;
                        string sped;
                        string fuelvalue12 = "";
                        speed = Convert.ToDouble(report.Rows[i][3].ToString());
                        double fuelvalue = Convert.ToDouble(report.Rows[i][6].ToString());

                        if (speed > 3)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            if (speed <= 3)
                            {
                                if (Convert.ToInt32(report.Rows[0][4].ToString()) == 1)
                                {
                                    fuelvalue12 = fuelstatus(vhno, fuelvalue);
                                }
                            }
                            else
                            {
                                fuelvalue12 = "NA..";
                            }
                        
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString() + "--km:" + Lpkm.ToString() + "--Fuel:" + fuelvalue12;//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            //GMap1.addGMarker(oMarker);
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);

                        }
                        else if (speed <= 3)
                        {
                              sped = Convert.ToString(speed * 1.85);
                              if (speed <= 3)
                              {
                                  if (Convert.ToInt32(report.Rows[0][4].ToString()) == 1)
                                  {
                                      fuelvalue12 = fuelstatus(vhno, fuelvalue);
                                  }
                              }
                              else
                              {
                                  fuelvalue12 = "NA..";
                              }
                            sped = "0";
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString() + "--km:" + Lpkm.ToString() + "--Fuel:" + fuelvalue12;//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);
                        }


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        GMap1.Add(line);

                    }
                }
                else if (VES == "2")
                {
                    DataTable report = db.searchOn2(Session["UserID"].ToString(), TextBox1, TextBox3, ddlMapTOVehicle.SelectedValue.ToString());

                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);

                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();

                        options.icon = icon;

                        double speed;
                        string sped;
                        speed = Convert.ToDouble(report.Rows[i][3].ToString());
                        if (speed > 3)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);

                        }
                        else if (speed <= 3)
                        {
                            sped = "0";
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);
                        }
                    }
                    if (report.Rows.Count > 0)
                    {
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        GMap1.Add(line);
                    }
                }
            }
            txtdate.Enabled = false;
            //TextBox3.Enabled = false;
            ddlMapTOVehicle.Enabled = false;
            ddlVehStatRpt.Enabled = false;
            ddlFromHOURS.Enabled = false;
            ddlFromMINUTES.Enabled = false;
            ddlToHOURS.Enabled = false;
            ddlToMINUTES.Enabled = false;
            rbtnAMPM.Enabled = false;
            rbtnDT.Enabled = false;
            btView.Enabled = false;

        }


        protected void Timer1_Tick(object sender, EventArgs e)
        {
           
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }


        public void binddataold()
        {
 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserID"] != null)
            {
                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

                GMap1.resetMarkers();
                GMap1.resetPolylines();

                GMap1.enableHookMouseWheelToZoom = true;

                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
                string VES = ddlVehStatRpt.SelectedValue.ToString();

                //string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                string TextBox1 = txtdate.Text + " " + fromtime + " " + AMPM;
                string TextBox3 = txtdate.Text + " " + totime + " " + DT;

                double rkm = 0;
                if (VES == "1")
                {
                    string vhno = ddlMapTOVehicle.SelectedValue.ToString();
                    //DataTable report = db.searchOn(Session["UserID"].ToString(), TextBox1, TextBox3, ddlMapTOVehicle.SelectedValue.ToString());
                    DataTable report = db.getMovingDataold(ddlMapTOVehicle.SelectedValue.ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));
                    //DataTable report = db.GetVehicleReport(Session["ParentUser"].ToString(), ddlMapTOVehicle.SelectedItem.Value, txtdate.Text, Calendar1.SelectedDate.AddDays(1).ToString("MM/dd/yyyy"));
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    int ovs = 0;
                    DataTable vdt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        ovs = Convert.ToInt32(vdt.Rows[0][3]);
                        ovs = Convert.ToInt32(ovs / 1.85);
                    }

                    double Lpkm = 0;
                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        if (i > 0)
                        {
                            rkm += bl12.CalcDistance(Convert.ToDouble(report.Rows[i][0].ToString()), Convert.ToDouble(report.Rows[i][1].ToString()), Convert.ToDouble(report.Rows[i - 1][0].ToString()), Convert.ToDouble(report.Rows[i - 1][1].ToString()));
                        }
                        Lpkm = Math.Truncate(rkm * 100) / 100;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            if (Convert.ToInt32(report.Rows[i][3]) == 0)
                            {
                                sPin = new SPin(0.5, 0, System.Drawing.Color.Blue, 14, PinFontStyle.bold, "");
                            }

                            else if (ovs != 0 && Convert.ToInt32(report.Rows[i][3]) > ovs)
                            {

                                sPin = new SPin(0.5, 0, System.Drawing.Color.Yellow, 14, PinFontStyle.bold, "");

                            }
                            else if (Convert.ToInt32(report.Rows[i][4]) != 0 && Convert.ToInt32(report.Rows[i][3]) < 2)
                            {

                                sPin = new SPin(0.5, 0, System.Drawing.Color.Purple, 14, PinFontStyle.bold, "");

                            }
                            else
                            {
                                sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                            }
                        }

                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();

                        options.icon = icon;

                        double speed;
                        string sped;
                        speed = Convert.ToDouble(report.Rows[i][3].ToString());
                        string fuelvalue12 = "";
                        double fuelvalue = Convert.ToDouble(report.Rows[i][6].ToString());

                        if (speed > 3)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            if (speed <= 3)
                            {
                                if (Convert.ToInt32(report.Rows[0][4].ToString()) == 1)
                                {
                                    fuelvalue12 = fuelstatus(vhno, fuelvalue);
                                }
                            }
                            else
                            {
                                fuelvalue12 = "NA";
                            }
                            
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString() + "--km:" + Lpkm.ToString() + "--Fuel:" + fuelvalue12; //report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);

                        }
                        else if (speed <= 3)
                        {
                            if (speed <= 3)
                            {
                                if (Convert.ToInt32(report.Rows[0][4].ToString()) == 1)
                                {
                                    fuelvalue12 = fuelstatus(vhno, fuelvalue);
                                }
                            }
                            else
                            {
                                fuelvalue12 = "NA";
                            }
                            sped = "0";
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString() + "--km:" + Lpkm.ToString() + "--Fuel:" + fuelvalue12; //report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);
                        }
                    

                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        GMap1.Add(line);
                    }
                }
                else if (VES == "2")
                {
                    DataTable report = db.searchOn2old(Session["UserID"].ToString(), TextBox1, TextBox3, ddlMapTOVehicle.SelectedValue.ToString());

                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();


                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);

                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();

                        options.icon = icon;

                        double speed;
                        string sped;
                        speed = Convert.ToDouble(report.Rows[i][3].ToString());
                        if (speed > 3)
                        {
                            sped = Convert.ToString(speed * 1.85);
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);

                        }
                        else if (speed <= 3)
                        {
                            sped = "0";
                            options.title = "Time:" + report.Rows[i][2].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                            oMarker.options = options;
                            GMap1.Add(oMarker);
                            latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                            points.Add(latlng);
                        }
                    }
                    if (report.Rows.Count > 0)
                    {
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        GMap1.Add(line);
                    }
                }

            }
            txtdate.Enabled = false;
            //TextBox3.Enabled = false;
            ddlMapTOVehicle.Enabled = false;
            ddlVehStatRpt.Enabled = false;
            ddlFromHOURS.Enabled = false;
            ddlFromMINUTES.Enabled = false;
            ddlToHOURS.Enabled = false;
            ddlToMINUTES.Enabled = false;
            rbtnAMPM.Enabled = false;
            rbtnDT.Enabled = false;
            btView.Enabled = false;
        }

        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            return default(string);
        }
        public string fuelstatus(string vhno, double fuelvalue)
        {
            string fuel = "";
            DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
            if (fuelim.Rows[0][1].ToString() == "Yes")
            {
                // DataTable dt2 = db.startfuelstaus(vhno, sdate, edate);
                if (fuelvalue != 0)
                {
                    DataTable dtf = db.GetVehiclesfuel(vhno, Session["UserId"].ToString());
                    DataTable tt = db.getfivestagefuel(vhno);
                    if (tt.Rows.Count > 0)
                    {
                        double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                        List<double> y = new List<double>();
                        y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                        y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                        y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                        y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                        y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                        List<double> x = new List<double>();
                        x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                        x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                        x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                        x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                        x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                        double string_val_fuel = fuelvalue;//Convert.ToDouble(dt2.Rows[0][3].ToString());

                        double[] yyy = y.ToArray();
                        double[] xxx = x.ToArray();
                        double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                        newY = (double)Math.Round(newY);
                        if (newY <= tankcapacity)
                        {
                            fuel = newY.ToString();
                        }
                        else if (newY > tankcapacity)
                        {
                            fuel = tankcapacity.ToString();
                        }
                        else
                        {
                            fuel = ("Waiting....");
                        }

                    }
                    else if (dtf.Rows.Count > 0)
                    {
                        double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                        int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());

                        double string_val_fuel = fuelvalue;//Convert.ToDouble(dt2.Rows[0][3].ToString());
                        if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
                        {

                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                            if (Tank_typ == 0)
                            {
                                double x = Fuel_Cali - Emptytankvalue;
                                double y = tank_capacity / x;

                                double z = string_val_fuel - Emptytankvalue;

                                double b = z * y;
                                b = (double)Math.Round(b);
                                fuel = b.ToString();
                            }
                            else
                            {
                                double x = Emptytankvalue - Fuel_Cali;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fuel_Cali;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);
                                fuel = f.ToString();
                            }
                        }
                    }
                    else
                    {
                        fuel = "Fuel parameters are not calibrated..";
                    }

                }
                else fuel = "Waiting...";
            }
            else
            {
                fuel = " Service Not Availed";
            }
            return fuel;
        }
    }
}

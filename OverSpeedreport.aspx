﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OverSpeedreport.aspx.cs"
    Inherits="Tracking.OverSpeedreport" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ValidationGroup="Group1" ShowSummary="False" />
    <div style="text-align: center;">
        <h1>
            Over Speed Summary Report</h1>
    </div>
    <div style="width: 1000px; margin: 0 auto; padding: 0; padding: 0;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="right">
                            Select Vehicle No :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                DataTextField="Vehicalnumber" Height="23px" Width="188px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                                ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Select Type :
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbtdate"
                                ValidationGroup="Group1" ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbtdate" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                OnSelectedIndexChanged="rbtdate_SelectedIndexChanged" Height="16px" Width="150px">
                                <asp:ListItem Text="Date" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Month" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Select Date/Month :
                        </td>
                        <td>
                            <asp:TextBox ID="txtdate" runat="server" Visible="false"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtdate">
                            </cc1:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                                ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlmonth" runat="server" Visible="false" Width="150px">
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlmonth"
                                ValidationGroup="Group1" ErrorMessage="Select Month" Operator="NotEqual" ValueToCompare="00">*</asp:CompareValidator>
                            &nbsp;&nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="submit" runat="server" Text="Submit" OnClick="submit_Click" ValidationGroup="Group1" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnprint" runat="server" Visible="false" Text="Print" OnClick="btnprint_Click"
                                ValidationGroup="Group1" />
                            &nbsp;&nbsp;&nbsp;<%----%>
                            <asp:Button ID="btnclear" runat="server" Text="Clear" OnClick="btnclear_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Label ID="lbldata" runat="server" Font-Bold="true" Font-Size="16px" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                </div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div id="Printdiv" runat="server" style="width: 100%; height: 100%; text-align: center;">
                    <table style="margin-right: auto; margin-left: auto;">
                        <tbody>
                            <tr style="width: 100%;" valign="top">
                                <td align="center" style="vertical-align: middle; text-align: center;" valign="top">
                                    <%--  <table style="font-weight:bold; color:Black;">
                            <tr style="font-size: 16px; font-weight: bold;">
                                <td align="left">
                                    <asp:Label ID="lblvehno" runat="server" Text="Vehicle RTO Number " Font-Bold="true" Visible="false"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblvehicleno" runat="server" Font-Bold="true"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:Label ID="lblovspedd" runat="server" Text="Over Speed Limit KM/H " Visible="false" Font-Bold="true"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lbloverspeed" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr style="font-size: 16px; font-weight: bold;">
                                <td align="left">
                                    <asp:Label ID="lbldt" runat="server" Text="Date " Visible="false" Font-Bold="true"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lbldate" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>--%>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    <asp:GridView ID="gvoverspeed" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true"
                                        HorizontalAlign="Center" HeaderStyle-ForeColor="Black" HeaderStyle-Font-Size="16px"
                                        RowStyle-ForeColor="Black" OnRowCreated="gvoverspeed_RowCreated">
                                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Date" DataField="date" ItemStyle-Font-Size="12px" Visible="false"
                                                ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Location" DataField="Location" ItemStyle-Font-Size="12px"
                                                ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Time" DataField="time" ItemStyle-Font-Size="12px" ItemStyle-VerticalAlign="Middle"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Speed Km/h" DataField="speed" ItemStyle-Font-Size="12px"
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div> 
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnprint" />
            </Triggers>
        </asp:UpdatePanel>
       
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Plotroutebasispoints.aspx.cs" Inherits="Tracking.Plotroutebasispoints" MasterPageFile="~/ESMaster.Master"%>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="MainContent" ID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
      function ConfirmIt() {
             var x = confirm("Do you Want to Delete The Point ??");
              var control = '<%=inpHide.ClientID%>';
             if (x == true) {
                 document.getElementById(control).value = "1";
             }
             else {
                 document.getElementById(control).value = "0";
             }
         }            
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%;">
        <div style="height: auto; width: 100%; color: Blue; text-align: center;">
            <h2>
                <a>Plot Geo Route Basis
                Points</a>
            </h2>
        </div>
        <div style="height: auto;">
            <input id="gpoint" type="hidden" runat="server" />
            <input id="inpHide" type="hidden" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 100%; height: auto;">
            <table style="width: 100%;" border="1">
                <tr>
                    <td>
                        Select Route No
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlrno"
                            ValidationGroup="Group1" ErrorMessage="Select Route No" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                    </td>
                    <td>
                        Route Type
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlrttype"
                            ValidationGroup="Group1" ErrorMessage="Select Route Type" Operator="NotEqual"
                            ValueToCompare="--">*</asp:CompareValidator>
                    </td>
                    
                    <td style="border-right-style: none;">
                        Radius(In Mtrs)
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtradius"
                            ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$" ValidationGroup="Group1">*</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtradius"
                            ValidationGroup="Group1" ErrorMessage="Enter Circle Radius">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 286px" >
                    Enter location name
                        
                    </td>                   
                  
                    <td style="text-align: center;">
                        <asp:Button ID="BtnClear" runat="server" Text="Un Set" Width="70" OnClick="BtnClear_Click" />
                    </td>
                    <td style="text-align: center;">
                        <asp:Button ID="Btn_Clear" runat="server" Text="Refresh" OnClick="Btn_Clear_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlrno" runat="server" Width="100px" DataValueField="Route_No"
                            DataTextField="Route_No" AutoPostBack="true" OnDataBound="ddlrno_DataBound" OnSelectedIndexChanged="ddlrno_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlrttype" runat="server" Width="100px">
                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                            <asp:ListItem Text="TRIP BASIS" Value="0"></asp:ListItem>
                            <asp:ListItem Text="DAILY BASIS" Value="1"></asp:ListItem>
                            <asp:ListItem Text="DATE BASIS" Value="2"></asp:ListItem>
                            <asp:ListItem Text="DATE/TIME BASIS" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                   
                    <td style="border-right-style: none;">
                        <asp:TextBox ID="txtradius" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td style="width: 286px" >
                    <asp:TextBox ID="TextBox1" runat="server" Width="274px"></asp:TextBox>
                        </td>
                   
                 
                    <td style="text-align: center;">
                        <asp:Button ID="BtnSubmit" runat="server" Text="Submit" Width="70" ValidationGroup="Group1"
                            OnClick="BtnSubmit_Click" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="600px" OnClick="GMap1_Click" />
        </div>
        <div style="width: 100%; background-color: Gray;">
            <asp:GridView ID="gvmappoints" runat="server" AutoGenerateColumns="false" DataKeyNames="ID" BorderColor="Black"
                Width="100%" HorizontalAlign="Center" RowStyle-ForeColor="Black" OnRowDataBound="gvmappoints_RowDataBound">
                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px"
                    Height="30px" />
                <Columns>
                    <asp:TemplateField HeaderText="Sno" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblsno" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<% #Eval("ID")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ROute No" DataField="Route_No" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Location" DataField="Route_Address" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Height="10px"></asp:BoundField>
                    <asp:BoundField HeaderText="Radius(mtr)" DataField="Radius" ItemStyle-Font-Size="10pt"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="10px"></asp:BoundField>
                    <%--<asp:TemplateField HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Button ID="remove" runat="server" Text="Delete" AutoPostBack="true" OnClientClick="ConfirmIt()"
                                OnClick="changed" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </div>
        <div style="height: 5px; background-color: Gray;">
        </div>
    </div>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TowedRouteReplay.aspx.cs"
    Inherits="Tracking.TowedRouteReplay" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="contentplaceholder1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="width: 100%;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <table style="color: Black; width: 100%;">
            <tr>
                <td colspan="2">
                    <table border="2" style="color: Black; width: 100%;">
                        <tr style="width: 100%;">
                            <td style="width: 490px">
                                <asp:Label ID="lblVehicle" Text="Select Vehicle:" runat="server" ForeColor="Black"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="Group1" ControlToValidate="ddlMapTOVehicle"
                                ErrorMessage="-Select the Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 422px">
                                <asp:Label ID="lblVehEngStat" Text="Vehicle Status Report:" runat="server" ForeColor="Black"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlVehStatRpt"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Status" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                            </td>
                            <td style="width: 471px">
                                Select From Date:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                    ValidationGroup="Group1" ErrorMessage="Select the Start Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 599px">
                                Select From Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rbtnAMPM"
                                    ValidationGroup="Group1" ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                            </td>
                            <%--<td align="left">
                                Select To Date:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox3"
                                    ValidationGroup="Group1" ErrorMessage="Select the End Date">*</asp:RequiredFieldValidator>
                            </td>--%>
                            <td align="left" style="width: 614px">
                                Select To Time:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbtnDT"
                                    ValidationGroup="Group1" ErrorMessage="Select TO Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                            </td>
                           <td style="width: 490px">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Width="110px" OnClick="btnRefresh_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 490px">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 422px">
                                <asp:DropDownList ID="ddlVehStatRpt" runat="server">
                                    <asp:ListItem Text="--" Value="--" />
                                    <asp:ListItem Text="Vehicle Towe" Value="1" />
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 471px">
                                <asp:TextBox ID="TextBox1" runat="server" Height="16px" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="TextBox1">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="width: 599px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="50px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes">*</asp:CompareValidator>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%--<td align="left">
                                <asp:TextBox ID="TextBox3" runat="server" Height="16px" Width="120px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="M/d/yyyy" TargetControlID="TextBox3">
                                </cc1:CalendarExtender>
                            </td>--%>
                            <td style="width: 614px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlToHOURS" runat="server" Width="50px" DataValueField="Hours"
                                                DataTextField="Hours">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours">*</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                                                DataTextField="Minutes">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                                                ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes ">*</asp:CompareValidator>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:Button ID="btView" runat="server" Text="View History" ValidationGroup="Group1"
                                    Width="110px" OnClick="btView_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:GMap ID="GMap1" runat="server" Width="100%" Height="800px" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

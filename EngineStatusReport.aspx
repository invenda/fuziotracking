﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EngineStatusReport.aspx.cs"
    Inherits="Tracking.EngineStatusReport" MasterPageFile="~/TrackingMaster.Master" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="sublogo">
            <h1>
                <a href="#">Download Vehicle Engine Status Report</a>
            </h1>
        </div>
    </div>
    <div>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label1" Text="Please Select the Vehicle:" runat="server"></asp:Label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                        ErrorMessage="Select the Vehicle Number" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        DataTextField="Vehicalnumber" Height="23px" Width="188px">
                    </asp:DropDownList>
                </td>
                <td align="left">
                    &nbsp;
                    <asp:Label ID="lblVehEngStat" Text="Please Select the Vehicle Engine Status:" runat="server"></asp:Label>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="ddlVehEngStat"
                        ErrorMessage="Select the Vehicle Engine Status" Operator="NotEqual" ValueToCompare="--">*</asp:CompareValidator>
                    <%--</td>
                <td align="left">--%>
                    <asp:DropDownList ID="ddlVehEngStat" runat="server" Height="23px" Width="188px">
                        <asp:ListItem Text="--" Value="--" />
                        <asp:ListItem Text="Vehicle Moving" Value="1" />
                        <asp:ListItem Text="Vehicle Stationary" Value="2" />
                        <asp:ListItem Text="Vehicle Engine Idling" Value="3" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr valign="top" height="25px">
                <td align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Select From Date:
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                        ErrorMessage="Select the Start Date">*</asp:RequiredFieldValidator>
                </td>
                <td align="left">
                    <asp:TextBox ID="TextBox1" runat="server" Height="16px" Width="184px"></asp:TextBox>
                    &nbsp;MM-DD-YYYY<cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy"
                        TargetControlID="TextBox1">
                    </cc1:CalendarExtender>
                </td>
                <td align="left">
                    &nbsp; Select From Time:
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rbtnAMPM"
                        ErrorMessage="Select FROM Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="40px" DataValueField="Hours"
                        DataTextField="Hours">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                        Operator="NotEqual" ValueToCompare="--"></asp:CompareValidator>
                    <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                        DataTextField="Minutes">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                        Operator="NotEqual" ValueToCompare="--"></asp:CompareValidator>
                    <%--<asp:DropDownList ID="ddlFromDT" runat="server" Width="80px" DataValueField="DT"
                        DataTextField="DT">
                    </asp:DropDownList>--%>
                    <asp:RadioButtonList ID="rbtnAMPM" runat="server">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                    <%--<asp:RadioButton ID="rbnFAM" runat="server" Text="AM" GroupName="Group1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rbnFPM" runat="server" Text="PM" GroupName="Group1" />--%>
                </td>
            </tr>
            <tr valign="top" height="25px">
                <td align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Select To Date:
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox3"
                        ErrorMessage="Select the End Date">*</asp:RequiredFieldValidator>
                </td>
                <td align="left">
                    <asp:TextBox ID="TextBox3" runat="server" Height="16px" Width="184px"></asp:TextBox>
                    &nbsp;MM-DD-YYYY<cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="M/d/yyyy"
                        TargetControlID="TextBox3">
                    </cc1:CalendarExtender>
                </td>
                <td align="left">
                    &nbsp; Select To Time:
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbtnDT"
                        ErrorMessage="Select TO Hours:Minutes and AM/PM">*</asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlToHOURS" runat="server" Width="40px" DataValueField="Hours"
                        DataTextField="Hours">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                        Operator="NotEqual" ValueToCompare="--"></asp:CompareValidator>
                    <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="50px" DataValueField="Minutes"
                        DataTextField="Minutes">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                        Operator="NotEqual" ValueToCompare="--"></asp:CompareValidator>
                    <asp:RadioButtonList ID="rbtnDT" runat="server">
                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    <asp:Button ID="BtnFind" runat="server" Text="Get Status" ToolTip="Click to get records"
                        OnClick="BtnFind_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="BtnSavPrint" runat="server" Text="Download" ToolTip="Click to download records"
                        OnClick="BtnSavPrint_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="BtnRefresh" runat="server" Text="Refresh" ToolTip="Click To Refresh Page for New Request"
                        OnClick="BtnRefresh_Click" />
                </td>
                <td>
                    <cc1:GMap ID="GMap1" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="right">
                    &nbsp;
                    <%--<asp:LinkButton ID="Linkbutton1" Width="16" CommandName="edit" Text="<img border = 0 title='Print Report' src='../Images/Save Icon.jpg'>"
                        Height="16" runat="server">
                    </asp:LinkButton>--%>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4">
                    <asp:DataGrid ID="dgSave" runat="server" Width="100%" AllowPaging="false" AutoGenerateColumns="False"
                        CellPadding="3" Visible="true">
                        <AlternatingItemStyle></AlternatingItemStyle>
                        <ItemStyle></ItemStyle>
                        <HeaderStyle></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="VehicalNumber" HeaderText="Vehicle Number">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Start Date & Time" HeaderText="Start Date & Time">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="End Date & Time" HeaderText="End Date & Time">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                    Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div style="position: relative; vertical-align: top; height: 300px; overflow: auto;">
                        <asp:DataGrid ID="dgStatus" runat="server" Width="100%" AllowPaging="false" AutoGenerateColumns="False"
                            CellPadding="3" Visible="true">
                            <AlternatingItemStyle></AlternatingItemStyle>
                            <ItemStyle></ItemStyle>
                            <HeaderStyle></HeaderStyle>
                            <Columns>
                                <%--<asp:BoundColumn DataField="SerialNo" HeaderText="S/No">OnPageIndexChanged="dgSearch_PageIndexChanged"
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>--%>
                                <%--<asp:BoundColumn DataField="deviceID" HeaderText="Vehicle Device ID">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="GTIM" HeaderText="Date and Time">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="true"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <%-- <asp:BoundColumn DataField="GTIM" HeaderText="To Time">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <%--<asp:BoundColumn DataField="LAMI" HeaderText="Latitude">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="LGMI" HeaderText="Longitude">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="LOC" HeaderText="Location">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="SPED" HeaderText="Speed (in km/h)">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="IGST" HeaderText="Ignition Status">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" BackColor="#0099ff" ForeColor="Black"
                                        Font-Bold="True"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <%--<asp:TemplateColumn>--%>
                                <%-- <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    &nbsp;
                                </HeaderTemplate>--%>
                                <%--<ItemTemplate>--%>
                                <%--<asp:LinkButton ID="Linkbutton1" Width="16" CommandName="edit" Text="<img border = 0 title='Print Report' src='../Images/print-icon.jpg'>"
                                        Height="16" runat="server">
                                    </asp:LinkButton>--%>
                                <%--&nbsp;&nbsp;--%>
                                <%-- &nbsp;&nbsp;CommandArgument='<%# DataBinder.Eval (Container.DataItem, "PK_ADM_ID") %>'
                                        <asp:LinkButton ID="Linkbutton3" Width="16" CommandName="view" CommandArgument='<%# DataBinder.Eval (Container.DataItem, "PK_ADM_ID") %>'
                                            Text="<img border = 0 title='View this Registration' src='../img/icon_preview.gif'>"
                                            Height="16" runat="server">
                                        </asp:LinkButton>--%>
                                <%-- </ItemTemplate>--%>
                                <%--</asp:TemplateColumn>--%>
                            </Columns>
                            <%--<PagerStyle Mode="NumericPages"></PagerStyle>--%>
                        </asp:DataGrid>
                    </div>
        </table>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Threading;



namespace Tracking
{
    public partial class Fuellog : System.Web.UI.Page
    {
        public double skm1 = 0;
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Session["ChartNo"] = 1;

                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }

        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            binddata();
            ddlMapTOVehicle.Enabled = false;
            txtdate.Enabled = false;
            GetRecord.Enabled = false;

        }
        public void binddata()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            string sdate = txtdate.Text + " 12:00 AM";
            string edate = txtdate.Text + " 11:59 PM";

            DataTable fff = new DataTable();
            fff.Columns.Add("latlong", typeof(string));
            //fff.Columns.Add("IGST", typeof(string));
            fff.Columns.Add("GTIM", typeof(string));
            fff.Columns.Add("FUEL_ADC", typeof(string));
            fff.Columns.Add("km", typeof(string));

            DateTime ontime = DateTime.Now;
            //DateTime offtime;

            DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());

            if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
            {

                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());

                //int num = 5;
                double persent = (tank_capacity * (0.08));

                if (Tank_typ == 0)
                {
                    DateTime dhdh = Convert.ToDateTime(sdate);
                    int emonth = dhdh.Month;
                    DateTime curretmonth = DateTime.Now;
                    int cmonth = curretmonth.Month;
                    if (emonth == cmonth)
                    {

                        DataTable dt = db.Select_Fueldata(Vno, sdate, edate);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                            {                               

                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double x = Fulltankvalue - Emptytankvalue;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Emptytankvalue;
                                double b = z * y;
                                b = (double)Math.Round(b);

                                //if (b > persent)
                                //{
                                dr["latlong"] = dt.Rows[i][0].ToString() + " , " + dt.Rows[i][1].ToString();
                                dr["GTIM"] = dt.Rows[i][2].ToString();
                                dr["FUEL_ADC"] = b.ToString();
                                //dr["km"] = skm.ToString();

                                fff.Rows.Add(dr);
                                //}
                                //}

                            }
                        }
                    }
                    else
                    {
                        DataTable dt = db.Select_Fueldataold(Vno, sdate, edate);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                            {                               
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double x = Fulltankvalue - Emptytankvalue;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Emptytankvalue;
                                double b = z * y;
                                b = (double)Math.Round(b);

                                //if (b > persent)
                                //{
                                dr["latlong"] = dt.Rows[i][0].ToString() + " , " + dt.Rows[i][1].ToString();
                                dr["GTIM"] = dt.Rows[i][2].ToString();
                                dr["FUEL_ADC"] = b.ToString();
                                // dr["km"] = skm.ToString();

                                fff.Rows.Add(dr);
                                //}
                                // }

                            }

                        }
                    }
                }
                else
                {
                    DateTime dhdh = Convert.ToDateTime(sdate);
                    int emonth = dhdh.Month;
                    DateTime curretmonth = DateTime.Now;
                    int cmonth = curretmonth.Month;
                    if (emonth == cmonth)
                    {
                        DataTable dt = db.Select_Fueldata(Vno, sdate, edate);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                            {
                               
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double x = Emptytankvalue - Fulltankvalue;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fulltankvalue;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);

                                //if (f > persent)
                                //{
                                dr["latlong"] = dt.Rows[i][0].ToString() + "," + dt.Rows[i][1].ToString();
                                dr["GTIM"] = dt.Rows[i][2].ToString();
                                dr["FUEL_ADC"] = f.ToString();
                                // dr["km"] = skm.ToString();
                                fff.Rows.Add(dr);
                                //}
                                // }
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = db.Select_Fueldataold(Vno, sdate, edate);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                            {
                               
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double x = Emptytankvalue - Fulltankvalue;
                                double y = tank_capacity / x;
                                double z = string_val_fuel - Fulltankvalue;
                                double b = z * y;
                                double f = tank_capacity - b;
                                f = (double)Math.Round(f);

                                //if (f > persent)
                                //{
                                dr["latlong"] = dt.Rows[i][0].ToString() + "," + dt.Rows[i][1].ToString();
                                dr["GTIM"] = dt.Rows[i][2].ToString();
                                dr["FUEL_ADC"] = f.ToString();
                                //dr["km"] = skm.ToString();
                                fff.Rows.Add(dr);
                                //}
                                // }
                            }
                        }

                    }
                }
                gvreport.DataSource = fff;
                gvreport.DataBind();
            }
            else
            {
                //string message = "alert('Fuel parameters are not calibrated..')";
                //ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Fuel parameters are not calibrated..');", true);
          


            }

        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void Download_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            DateTime dt = Convert.ToDateTime(txtdate.Text);
            string str = dt.ToString("dd-MM-yyyy");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            //str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "Vehicle no:" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();

            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gvreport);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
            tb.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            // string Vno = Session["RegNo"].ToString();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.ColumnSpan = 1;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.ColumnSpan = 1;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                HeaderCell1.Text = "User Name :" + DriDet.Rows[0][1].ToString();
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.Font.Bold = true;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell1);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);
                //if (e.Row.RowType == DataControlRowType.Header)
                //{
                //    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                //    TableCell HeaderCell = new TableCell();
                //    HeaderCell.Text = "Vehical No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                //    HeaderCell.ColumnSpan = 1;
                //    HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                //    HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                //    HeaderCell.Font.Bold = true;
                //    HeaderGridRow.Cells.Add(HeaderCell);
                //    HeaderCell = new TableCell();
                //    gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                //    DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

                //    GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                //    TableCell HeaderCell1 = new TableCell();
                //    DateTime hdt = Convert.ToDateTime(txtdate.Text);
                //    HeaderCell1.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString(); //; "Report Date  :" + hdt.ToString("dd-MM-yyyy");
                //    HeaderCell1.ColumnSpan = 2;
                //    HeaderCell1.Font.Bold = true;
                //    HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                //    HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                //    HeaderGridRow1.Cells.Add(HeaderCell1);
                //   // TableCell HeaderCell4 = new TableCell();
                //   // HeaderCell4.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                //    //HeaderCell4.ColumnSpan = 2;
                //    //HeaderCell4.Font.Bold = true;
                //    //HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                //    //HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                //    //HeaderGridRow1.Cells.Add(HeaderCell4);
                //    gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);
            }

        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {

                    //Tsta.Text = sta1.ToString();
                    Label lbl = e.Row.FindControl("lblkm") as Label;
                    if (lbl.Text != "&nbsp;" && lbl.Text != "")
                    {
                        skm1 += Convert.ToDouble(lbl.Text);
                    }


                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl1 = e.Row.FindControl("ftTKMR") as Label;
                lbl1.Text = "Total KM: " + skm1.ToString();

            }
        }


    }
}

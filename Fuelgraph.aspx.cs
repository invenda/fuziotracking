﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;
using System.ComponentModel;

namespace Tracking
{
    public partial class Fuelgraph : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    Session["ChartNo"] = 1;

                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }
        }
        private void LoadChartData()
        {
            DataTable dt2 = binddata();
            Double startX = 0;
            Double startY = 0;

            int i = 0; int Y = 0;
            //if (Session["ChartNo"] != null)
            //{
            //    int ChartNo = int.Parse(Session["ChartNo"].ToString());

            //    if (ChartNo == 1)
            //    {
            Chart1.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
            Chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
            Chart1.ChartAreas[0].AxisX.Minimum = 0;
            Chart1.ChartAreas[0].AxisX.Maximum = 1440;
            Chart1.ChartAreas[0].AxisX.IsStartedFromZero = true;
            Chart1.ChartAreas[0].AxisX.Interval = 60;
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(40, 80, "1");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(100, 140, "2");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(160, 200, "3");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(220, 260, "4");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(280, 320, "5");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(340, 380, "6");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(400, 440, "7");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(460, 500, "8");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(520, 560, "9");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(580, 620, "10");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(640, 680, "11");
            Chart1.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12");
            //Chart2.ChartAreas[0].AxisX2.CustomLabels.Clear();
            //i = 0; Y = 1;
            ////Chart2.Titles[1].Text = "Time of the Day in Hrs(12 AM To 12 Noon)";
            //LinkButton1.Enabled = false;
            //linknext.Enabled = true;



            //}
            //else if (ChartNo == 2)
            
                //Chart2.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                //Chart2.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                //Chart2.ChartAreas[0].AxisX.Minimum = 720;
                //Chart2.ChartAreas[0].AxisX.Maximum = 1440;
                //Chart2.ChartAreas[0].AxisX.IsStartedFromZero = true;
                //Chart2.ChartAreas[0].AxisX.Interval = 60;
                //Chart2.ChartAreas[0].AxisX.CustomLabels.Clear();
                //Chart2.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12pm");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(760, 800, "13");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(820, 860, "14");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(880, 920, "15");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(940, 980, "16");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1000, 1040, "17");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1060, 1100, "18");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1120, 1160, "19");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1180, 1220, "20");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1240, 1280, "21");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1300, 1340, "22");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1360, 1400, "23");
                Chart1.ChartAreas[0].AxisX.CustomLabels.Add(1420, 1460, "24");
                Chart1.ChartAreas[0].AxisX2.CustomLabels.Clear();
                i = 0; Y = 1;
                Chart1.Series["kilometers"].BorderWidth = 3;
                //Chart2.Titles[1].Text = "Time of the Day in Hrs(12 Noon To 12 AM)";
                //linknext.Enabled = false;
                //LinkButton1.Enabled = true;


                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);
                    startX = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }

                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }
                i++;
                Y++;
                if (i < Y)
                {
                    startX = i * 60;
                    startX += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    startY += Convert.ToDouble(dt2.Rows[i]["TKMR"].ToString());
                    Chart1.Series["kilometers"].Points.AddXY(startX, startY);

                    startX = 0;

                }



                // }
                //}
                //}
                //DataTable dt2 = binddata();
                //Chart1.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
                //double yValue = 0.0; ;
                //double yValue2 = 0.0;
                //Chart1.Series["kilometers"].BorderWidth = 3;
                //Chart1.Series["Fuel"].BorderWidth = 3;
                ////if (Chart1.Series["km"].Points.Count > 0)
                ////{
                ////    yValue = Chart1.Series["km"].Points[Chart1.Series["km"].Points.Count - 1].YValues[0];
                ////    //yValue2 = Chart1.Series["Series2"].Points[Chart1.Series["Series1"].Points.Count - 1].YValues[0];
                ////}            
                //for (int i = 0; i <= dt2.Rows.Count - 1; i++)
                //{
                //    yValue += Convert.ToDouble(dt2.Rows[i][0].ToString());
                //    yValue2 = Convert.ToDouble(dt2.Rows[i][1].ToString());
                //    //Chart1.AxisY2.Enabled = AxisEnabled.True;
                //    Chart1.Series["kilometers"].Points.AddY(yValue);
                //    Chart1.Series["Fuel"].Points.AddY(yValue2);
                //}

            
        }
        public DataTable binddata()
        {
 
 
            DBClass db1 = new DBClass();
 
 
            DataTable DrivDet = db1.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
            DataTable dt = db1.getRhours();
            DataTable dt1 = new DataTable();            
            dt1.Columns.Add("TKMR", typeof(string));
            dt1.Rows.Add(0);
            string fdate12 = date.Text + " 12:00 AM";
           
            for (int i = 0; i < dt.Rows.Count; i++)
            {
               
                DataRow dr = dt1.NewRow();
                string fdate = date.Text + " " + dt.Rows[i][0];
                string tdate = date.Text + " " + dt.Rows[i][1];
                DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
#pragma warning disable CS0219 // The variable 'tkm' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'fulestat' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'fkm' is assigned but its value is never used
                double fkm = 0; double tkm = 0; double TKMR1 = 0; double fulestat = 0;
#pragma warning restore CS0219 // The variable 'fkm' is assigned but its value is never used
#pragma warning restore CS0219 // The variable 'fulestat' is assigned but its value is never used
#pragma warning restore CS0219 // The variable 'tkm' is assigned but its value is never used

                if (dt2.Rows.Count != 0)
                {
                    DataTable dt12 = db1.Select_TotalRunkm(ddlMapTOVehicle.SelectedValue.ToString(), fdate12, tdate);                  
                    for (int m = 0, n = 1; m < dt12.Rows.Count - 1; m++, n++)
                    {
                        TKMR1 += CalcDistance(Convert.ToDouble(dt12.Rows[m][0].ToString()), Convert.ToDouble(dt12.Rows[m][1].ToString()), Convert.ToDouble(dt12.Rows[n][0].ToString()), Convert.ToDouble(dt12.Rows[n][1].ToString()));
                    }
                    if (dt12.Rows.Count != 0)
                    {
                        fdate12 = dt12.Rows[dt12.Rows.Count - 1][3].ToString();
                    }
                    TKMR1 = (double)Math.Round(TKMR1, 2);
                  
                }              
                dr["TKMR"] = TKMR1.ToString();               
                dt1.Rows.Add(dr);

            }
            return dt1;
        }


        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0))))) ;
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            LoadChartData();

        }      


        protected void Refresh_Click(object sender, EventArgs e)
        {

            Response.Redirect(Request.RawUrl);
        }

    }




}

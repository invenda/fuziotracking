﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fulefillingreport.aspx.cs"
    Inherits="Tracking.Fulefillingreport" MasterPageFile="~/ESMaster.Master" Culture="auto"
    meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600"
        CombineScripts="True">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="width: 100%; height: 30px; font-size: 25px; text-align: center; color: Blue;">
            Vehicle Fuel filling Report
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" meta:resourcekey="ValidationSummary1Resource1" />
        </div>
        <div style="width: 1300px; margin: 0 auto; padding: 0; padding: 0; height:auto ;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table border="1" style="width: 100%; color: Black;">
                        <tbody align="left">
                            <tr align="left">
                                <td align="right" style="width: 152px">
                                    <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="True" meta:resourcekey="Label1Resource1"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                        ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle" >*</asp:RequiredFieldValidator>&nbsp;:
                                </td>
                                <td style="width: 151px">
                                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                        DataTextField="Vehicalnumber" Height="26px" Width="134px" meta:resourcekey="ddlMapTOVehicleResource1">
                                    </asp:DropDownList>
                                </td>
                                <td id="TdSearch1" runat="server" visible="true" style="width: 900px;">
                                    <table border="1" style="color: Black;">
                                        <tr>
                                            <td style="width: 140px">
                                                <asp:Label ID="Label2" Text="From Date" runat="server" Font-Bold="True" meta:resourcekey="Label2Resource1"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                                    ControlToValidate="txtfdate" ErrorMessage="- Select From Date" meta:resourcekey="RequiredFieldValidator3Resource1">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 190px">
                                                <asp:TextBox ID="txtfdate" runat="server" Width="112px" Height="20px" meta:resourcekey="txtfdateResource1"></asp:TextBox>
                                                <cc1:CalendarExtender ID="Cal" runat="server" Format="MM/dd/yyyy" TargetControlID="txtfdate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 123px">
                                                <asp:Label ID="Label4" Text="To Date" runat="server" Font-Bold="True" meta:resourcekey="Label4Resource1"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="Group1"
                                                    ControlToValidate="txttdate" ErrorMessage="- Select To Date" meta:resourcekey="RequiredFieldValidator4Resource1">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 180px">
                                                <asp:TextBox ID="txttdate" runat="server" Width="122px" Height="20px" meta:resourcekey="txttdateResource1"></asp:TextBox>
                                                <cc1:CalendarExtender ID="Cal2" runat="server" Format="M/d/yyyy" TargetControlID="txttdate"
                                                    OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 140px">
                                                <asp:Label ID="lbltime" Text="From Time" runat="server" Font-Bold="True" meta:resourcekey="lbltimeResource1"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rbtnAMPM"
                                                    ValidationGroup="Group1" ErrorMessage="Select FROM Hours:Minutes and AM/PM" meta:resourcekey="RequiredFieldValidator5Resource1">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 190px">
                                                <asp:DropDownList ID="ddlFromHOURS" runat="server" Width="59px" DataValueField="Hours"
                                                    DataTextField="Hours" Height="26px" meta:resourcekey="ddlFromHOURSResource1">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlFromHOURS"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours"
                                                    meta:resourcekey="CompareValidator2Resource1">*</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlFromMINUTES" runat="server" Width="60px" DataValueField="Minutes"
                                                    DataTextField="Minutes" Height="26px" meta:resourcekey="ddlFromMINUTESResource1">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlFromMINUTES"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes"
                                                    meta:resourcekey="CompareValidator1Resource1">*</asp:CompareValidator>
                                            </td>
                                            <td style="width: 95px">
                                                <asp:RadioButtonList ID="rbtnAMPM" runat="server" RepeatDirection="Horizontal" Height="20px"
                                                    Width="80px" meta:resourcekey="rbtnAMPMResource1">
                                                    <asp:ListItem Text="AM" Value="AM" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="PM" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 123px">
                                                <asp:Label ID="Label5" Text="To Time" runat="server" Font-Bold="True" meta:resourcekey="Label5Resource1"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rbtnDT"
                                                    ValidationGroup="Group1" ErrorMessage="Select TO Hours:Minutes and AM/PM" meta:resourcekey="RequiredFieldValidator6Resource1">*</asp:RequiredFieldValidator>
                                                &nbsp;:
                                            </td>
                                            <td style="width: 180px">
                                                <asp:DropDownList ID="ddlToHOURS" runat="server" Width="59px" DataValueField="Hours"
                                                    DataTextField="Hours" Height="26px" meta:resourcekey="ddlToHOURSResource1">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlToHOURS"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Hours"
                                                    meta:resourcekey="CompareValidator4Resource1">*</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlToMINUTES" runat="server" Width="58px" DataValueField="Minutes"
                                                    DataTextField="Minutes" Height="26px" meta:resourcekey="ddlToMINUTESResource1">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlToMINUTES"
                                                    ValidationGroup="Group1" Operator="NotEqual" ValueToCompare="--" ErrorMessage="Select Minutes "
                                                    meta:resourcekey="CompareValidator5Resource1">*</asp:CompareValidator>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rbtnDT" runat="server" RepeatDirection="Horizontal" Width="82px"
                                                    Height="27px" meta:resourcekey="rbtnDTResource1">
                                                    <asp:ListItem Text="AM" Value="AM" meta:resourcekey="ListItemResource3"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="PM" meta:resourcekey="ListItemResource4"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 60px">
                                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" ValidationGroup="Group1"
                                        Width="59px" meta:resourcekey="btnsubmitResource1" OnClick="btnsubmit_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btndownload" runat="server" Text="Download" Height="30px" Width="72px" Enabled ="false "
                                        meta:resourcekey="btndownloadResource1" OnClick="btndownload_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnclear" runat="server" Text="Clear" Width="53px" meta:resourcekey="btnclearResource1"
                                        OnClick="btnclear_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div style ="height :30px;">
                    
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="text-align: center; color: Red;">
                        <asp:Label ID="lblempty" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div style="height :auto ; text-align:center ;">
                    <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                        ShowFooter="True" Width="1275px" RowStyle-ForeColor="Black" meta:resourcekey="gvreportResource1">
                        <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" Font-Size="14px" />
                        <FooterStyle BackColor="#61A6F8" ForeColor="Black" Height="20px" Font-Size="14px"
                            BorderWidth="0" />
                        <RowStyle ForeColor="Black"></RowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="180px" FooterStyle-BorderColor="#61A6F8"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px" FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# Bind("GTIM") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lblmov" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Opening Fuel" HeaderStyle-Width="100px" FooterStyle-BorderColor="#61A6F8"
                                ItemStyle-Font-Size="13px">
                                <ItemTemplate>
                                    <asp:Label ID="lblopn" runat="server" Text='<%# Bind("Openingfuel") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lbltigson" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Filled Fuel" HeaderStyle-Width="100px" FooterStyle-BorderColor="#61A6F8"
                                FooterStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px">
                                <ItemTemplate>
                                    <asp:Label ID="lblfilled" runat="server" Text='<%# Bind("Fuelfilled") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lbltidl" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Fuel" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right"
                                FooterStyle-BorderColor="#61A6F8" HeaderStyle-Width="100px" ItemStyle-Font-Size="13px">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotal" runat="server" Text='<%# Bind("Totalfuel") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lblts" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="400px"
                                ItemStyle-Font-Size="13px" FooterStyle-BorderColor="#61A6F8">
                                <ItemTemplate>
                                    <asp:Label ID="lblloc" runat="server" Text='<%# Bind("Adress") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                            <asp:Label ID="lbltsv" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="View in Map" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="13px"
                                HeaderStyle-Width="80px" FooterStyle-BorderColor="#61A6F8" FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:LinkButton ID="linkview" runat="server" OnClick="lnkView_Click">View</asp:LinkButton>
                                </ItemTemplate>
                                <%--<FooterTemplate>
                            <asp:Label ID="lblffuel" runat="server"></asp:Label>
                        </FooterTemplate>--%>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btndownload" />
                </Triggers>
                </asp:UpdatePanel> 
        </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using Subgurim.Controles.GoogleChartIconMaker;


namespace Tracking
{
    public partial class Createroutebasis : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            GMapUIOptions options = new GMapUIOptions();
            options.maptypes_hybrid = true;
            options.keyboard = false;
            options.maptypes_physical = false;
            options.zoom_scrollwheel = true;
            GMap1.Add(new GMapUI(options));
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
            GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
            GMap1.Add(extMapType);
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
            GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
            GMap1.enableHookMouseWheelToZoom = true;

            if (!IsPostBack)
            {

                if (Session["UserRole"] != null)
                {
                    // DBClass db = new DBClass();
                    if (Session["UserID"] != null)
                    {

                        if (Request.QueryString["rno"] != null)
                        {
                            txtrno.Text = Request.QueryString["rno"].ToString();
                        }
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();


                        ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlToMINUTES.DataBind();



                    }
                }
            }

        }

        protected void ddlrtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrtype.SelectedValue.ToString() != "--")
            {
                if (ddlrtype.SelectedValue.ToString() == "4")
                {
                    txtid.Text = "ROU";
                }
                txtid.Enabled = false;
            }
        }

        protected void Btn_clr_Click(object sender, EventArgs e)
        {

            Response.Redirect(Request.RawUrl);
        }



        protected void btndisplay_Click(object sender, EventArgs e)
        {

            ddlMapTOVehicle.Enabled = false;
            ddlrtype.Enabled = false;
            txtenddate.Enabled = false;
            txtstartdate.Enabled = false;
            ddlpointstype.Enabled = false;
            ddlFromHOURS.Enabled = false;
            ddlFromMINUTES.Enabled = false;
            ddlToHOURS.Enabled = false;
            ddlToMINUTES.Enabled = false;
            btndisplay.Enabled = false;
            DateTime dhdh = Convert.ToDateTime(txtstartdate.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {

                binddata();


            }
            else
            {
                binddataold();

            }
            ddlMapTOVehicle.Enabled = true;
        }


        public void binddata()
        {
            //DBClass db = new DBClass();
            if (Session["UserID"] != null)
            {
                GMap1.resetMarkers();
                GMap1.resetPolylines();
                //GMap1.enableScrollWheelZoom = true;
                GMap1.enableHookMouseWheelToZoom = true;

                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
                string VES = ddlpointstype.SelectedValue.ToString();

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                //string TextBox12= txtstartdate.Text + " 12:00 AM";
                //string TextBox34 = txtenddate.Text  +" 11:59 AM";
                string TextBox1 = txtstartdate.Text + " " + fromtime + " " + AMPM;
                string TextBox3 = txtenddate.Text + " " + totime + " " + DT;
               
                if (VES == "0")
                {

                    DataTable report = db.getMovingData(ddlMapTOVehicle.SelectedValue.ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();


                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        // db.Insert_Geo_Route_basis(report.Rows[i][0].ToString(), report.Rows[i][1].ToString(), report.Rows[i][3].ToString(), report.Rows[i][2].ToString(), report.Rows[i][4].ToString(), report.Rows[i][5].ToString());
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        //GMap1.addControl(extMapType);
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        //GMap1.addPolyline(line);
                        GMap1.Add(line);

                    }
                }
                else if (VES == "1")
                {
                    DataTable report = db.searchOn2(Session["UserID"].ToString(), TextBox1, TextBox3, ddlMapTOVehicle.SelectedValue.ToString());
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    // double Lpkm = 0;
                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        //db.Insert_Geo_Route_basis(report.Rows[i][0].ToString(), report.Rows[i][1].ToString(), report.Rows[i][3].ToString(), report.Rows[i][2].ToString(), report.Rows[i][4].ToString(), report.Rows[i][5].ToString());

                        //Lpkm = Math.Truncate(rkm * 100) / 100;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        //GMap1.addControl(extMapType);
                        GMap1.Add(extMapType); 
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                       // GMap1.addPolyline(line);
                        GMap1.Add(line);

                    }

                }

            }

        }

        protected void Btn_submit_Click(object sender, EventArgs e)
        {
            GMap1.resetMarkers();
            GMap1.resetPolylines();
            GMap1.enableServerEvents = false;
           // DBClass db = new DBClass();
           

           
            string AMPM, DT;
            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
            string sdatetime = txtstartdate.Text + " " + fromtime + " " + AMPM;
            string edatetime = txtenddate.Text + " " + totime + " " + DT;
            string rno = txtid.Text + "-" + txtrno.Text;


            string sdate1 = txtstartdate.Text + " 12:00 AM";
            string edate1 = txtenddate.Text + " 11:59 PM";
           
             DateTime dhdh = Convert.ToDateTime(txtstartdate.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                DataTable getroutrpont = db.Select_Georoutenew(ddlMapTOVehicle.SelectedValue.ToString(), sdate1, edate1);
                if (getroutrpont.Rows.Count > 0)
                {
                    for (int i = 0; i < getroutrpont.Rows.Count; i++)
                    {
                        db.Insert_Geo_Route_basis(getroutrpont.Rows[i][0].ToString(), getroutrpont.Rows[i][1].ToString(), getroutrpont.Rows[i][2].ToString(), getroutrpont.Rows[i][3].ToString(), getroutrpont.Rows[i][4].ToString(), getroutrpont.Rows[i][5].ToString());
                    }
                }           

            }
            else
            {
                DataTable getroutrpont = db.Select_Georouteold(ddlMapTOVehicle.SelectedValue.ToString(), sdate1, edate1);
                if (getroutrpont.Rows.Count > 0)
                {
                    for (int i = 0; i < getroutrpont.Rows.Count; i++)
                    {
                        db.Insert_Geo_Route_basis(getroutrpont.Rows[i][0].ToString(), getroutrpont.Rows[i][1].ToString(), getroutrpont.Rows[i][2].ToString(), getroutrpont.Rows[i][3].ToString(), getroutrpont.Rows[i][4].ToString(), getroutrpont.Rows[i][5].ToString());
                    }
                }    

            }
            // db.Insert_Geo_Route_basis(report.Rows[i][0].ToString(), report.Rows[i][1].ToString(), report.Rows[i][3].ToString(), report.Rows[i][2].ToString(), report.Rows[i][4].ToString(), report.Rows[i][5].ToString());
            db.update_Geo_Route123(rno, Session["UserID"].ToString(), Convert.ToDateTime(sdatetime), Convert.ToDateTime(edatetime), ddlMapTOVehicle.SelectedValue.ToString(), ddlpointstype.SelectedValue.ToString(),DateTime .Now);
            //lblalert.Text = "Route No :" + rno + "  Created";
            fresh();
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Sucessfully Route created....');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }
        public void fresh()
        {
            // Response.Redirect(Request.RawUrl);
            // ddlrtype.SelectedValue = null;
            txtstartdate.Text = string.Empty;
            txtenddate.Text = string.Empty;
            GMap1.resetMarkers();
            GMap1.resetPolylines();
            ddlFromHOURS.SelectedValue = null;
            ddlFromMINUTES.SelectedValue = null;
            ddlToHOURS.SelectedValue = null;
            ddlToMINUTES.SelectedValue = null;



        }
        public void binddataold()
        {

 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserID"] != null)
            {
                GMap1.resetMarkers();
                GMap1.resetPolylines();

                //GMap1.enableScrollWheelZoom = true;
                GMap1.enableHookMouseWheelToZoom = true;

                string AMPM, DT;
                string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
                string VES = ddlpointstype.SelectedValue.ToString();

                AMPM = rbtnAMPM.SelectedValue.ToString();
                DT = rbtnDT.SelectedValue.ToString();

                //string TextBox12= txtstartdate.Text + " 12:00 AM";
                //string TextBox34 = txtenddate.Text  +" 11:59 AM";
                string TextBox1 = txtstartdate.Text + " " + fromtime + " " + AMPM;
                string TextBox3 = txtenddate.Text + " " + totime + " " + DT;
#pragma warning disable CS0219 // The variable 'rkm' is assigned but its value is never used
                double rkm = 0;
#pragma warning restore CS0219 // The variable 'rkm' is assigned but its value is never used

                if (VES == "0")
                {

                    DataTable report = db.getMovingDataold(ddlMapTOVehicle.SelectedValue.ToString(), Convert.ToDateTime(TextBox1), Convert.ToDateTime(TextBox3));
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();


                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        // db.Insert_Geo_Route_basis(report.Rows[i][0].ToString(), report.Rows[i][1].ToString(), report.Rows[i][3].ToString(), report.Rows[i][2].ToString(), report.Rows[i][4].ToString(), report.Rows[i][5].ToString());
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                       // GMap1.addControl(extMapType);
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Green, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                        //GMap1.addPolyline(line);
                        GMap1.Add(line);


                    }
                }
                else if (VES == "1")
                {
                    DataTable report = db.searchOn2old(Session["UserID"].ToString(), TextBox1, TextBox3, ddlMapTOVehicle.SelectedValue.ToString());
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;

                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();

                    // double Lpkm = 0;
                    for (int i = 0; i < report.Rows.Count; i++)
                    {
                        //db.Insert_Geo_Route_basis(report.Rows[i][0].ToString(), report.Rows[i][1].ToString(), report.Rows[i][3].ToString(), report.Rows[i][2].ToString(), report.Rows[i][4].ToString(), report.Rows[i][5].ToString());

                        //Lpkm = Math.Truncate(rkm * 100) / 100;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                        //GMap1.addControl(extMapType);
                        GMap1.Add(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        //GMarker marker = new GMarker(gLatLng);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        //GMap1.setCenter(latlng);
                        SPin sPin;
                        if (i == 0)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "End Point");
                        }
                        else if (i < report.Rows.Count - 1)
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Red, 14, PinFontStyle.bold, "");
                        }
                        else
                        {
                            sPin = new SPin(0.5, 0, System.Drawing.Color.Gray, 14, PinFontStyle.bold, "Start Point");
                        }
                        //sPin.text = "Start Point";
                        icon = new Subgurim.Controles.GIcon(sPin.ToString());

                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        oMarker.options = options;
                        //GMap1.addGMarker(oMarker);
                        GMap1.Add(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(report.Rows[i][0]), Convert.ToDouble(report.Rows[i][1]));
                        points.Add(latlng);
                        options.icon = icon;


                    }
                    if (report.Rows.Count > 0)
                    {
                        //points.Add(latlng + new GLatLng(-0.5, 4.2));
                        GPolyline line = new GPolyline(points, "FF0000", 2);
                       // GMap1.addPolyline(line);
                        GMap1.Add(line);

                    }

                }

            }

        }

        //protected void txtrno_TextChanged(object sender, EventArgs e)
        //{
        //    if (lblckeckro.Text == "Route Number Exits")
        //    {
        //        lblckeckro.Text = "";
        //        lblckeckro.Visible = false;

        //    }
        //    string rno = txtid.Text + "-" + txtrno.Text;
        //    int n = db.Check_Route_No12(Session["UserID"].ToString(), rno, ddlMapTOVehicle.SelectedValue.ToString());
        //    if (n == 1)
        //    {
        //        lblckeckro.Visible = true;
        //        lblckeckro.Text = "Route Number Exits";
        //        txtenddate.Enabled = false;
        //        txtstartdate.Enabled = false;
        //        ddlToHOURS.Enabled = false;
        //        ddlToMINUTES.Enabled = false;
        //        ddlFromMINUTES.Enabled = false;
        //        ddlFromHOURS.Enabled = false;

        //    }
        //}



        protected void ddlpointstype_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--");
        }
        


        //'''

    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class EditVehDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        DataTable View = db.ViewExactCust(Session["UserID"].ToString());
                        txtsmsno.Text = View.Rows[0]["Cx_MobileNumber"].ToString();
                        txtsmsno.Enabled = false;

                        string VehRegNo = Session["RegNo"].ToString();
                        DataTable EditV = db.LoadVeh(VehRegNo);

                        //string TankType = EditV.Rows[0]["Tank_Type"].ToString();
                        //if (TankType == "0")
                        //{
                        //    rbtnPosTank.Checked = true;
                        //    rbtnNegTank.Checked = false;
                        //}
                        //else
                        //{
                        //    rbtnNegTank.Checked = true;
                        //    rbtnPosTank.Checked = false;
                        //}
                        string Battery = EditV.Rows[0]["Battery_Voltage"].ToString();
                        if (Battery == "12")
                        {
                            rbtn12V.Checked = true;
                            rbtn24V.Checked = false;
                        }
                        else
                        {
                            rbtn12V.Checked = false;
                            rbtn24V.Checked = true;
                        }

                        string Recharge = EditV.Rows[0]["Recharge_Period"].ToString();
                        if (Recharge == "Monthly")
                        {
                            rbtnMonthly.Checked = true;
                            rbtnYearly.Checked = false;
                        }
                        else
                        {
                            rbtnMonthly.Checked = false;
                            rbtnYearly.Checked = true;
                        }

                        txtVehicleNo.Text = EditV.Rows[0]["Vehicle_number"].ToString();
                        txtVehRegName.Text = EditV.Rows[0]["Vehicle_Reg_Name"].ToString();
                        txtDriverName.Text = EditV.Rows[0]["Driver_Name"].ToString();
                        txtVehModel.Text = EditV.Rows[0]["Vehicle_Model"].ToString();
                        txtDriverMobNum.Text = EditV.Rows[0]["Driver_Mobile_Number"].ToString();
                        txtVehMake.Text = EditV.Rows[0]["Vehicle_Make"].ToString();
                        txtDriverADD.Text = EditV.Rows[0]["Driver_Address"].ToString();
                        txtVehInsNum.Text = EditV.Rows[0]["Vehicle_Insurance_Number"].ToString();
                        txtDrivLicNum.Text = EditV.Rows[0]["Driving_Licence_Number"].ToString();
                        //txtVehInsExpDt.Text = EditV.Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
                        //txtDrivLic.Text = EditV.Rows[0]["Driving_Licence_Expiry_Date"].ToString();
                        txtTankCapacity.Text = EditV.Rows[0]["Tank_Capacity"].ToString();
                        txtExistMetRead.Text = EditV.Rows[0]["Existing_Meter_Reading"].ToString();
                        txtKiloPulse.Text = EditV.Rows[0]["Kilometer_Pulse"].ToString();
                        txtMobNum.Text = EditV.Rows[0]["Mobile_Number"].ToString();
                        txtSNum.Text = EditV.Rows[0]["Serial_Number"].ToString();
                        txtSIMProvider.Text = EditV.Rows[0]["SIM_Provider"].ToString();
                        txtDOP.Text = EditV.Rows[0]["Date_of_Purchase"].ToString();
                        txtGPRSActDt.Text = EditV.Rows[0]["GPRS_Activated_Date"].ToString();
                        txtDealerName.Text = EditV.Rows[0]["Dealer_Name"].ToString();
                      //  txtFCali.Text = EditV.Rows[0]["Fuel_Calibration"].ToString();
                        //txtrtodate.Text = EditV.Rows[0]["Rto_Renewal_Date"].ToString();
                        txtrtoname.Text = EditV.Rows[0]["Rto_Devision_name"].ToString();
                        txtmsmmno.Text = EditV.Rows[0]["SMS_No"].ToString();
                        txtmilage.Text = EditV.Rows[0]["milage"].ToString();
                        ddlpt.Text = EditV.Rows[0]["Fuel_type"].ToString();
                        // maxtemp.Text = EditV.Rows[0]["Max_Temp"].ToString();
                       // txtmis.Text = EditV.Rows[0]["Minusstring"].ToString();
                        ddlvc.Text = EditV.Rows[0]["Vehicle_Type"].ToString();
                        //ddlac.Text = EditV.Rows[0]["ACswitch"].ToString();                      
                        txtVehicleNo.Enabled = false;                      


                    }
                }
            }
        }

        protected void btSubmit_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 

            //----Important Code: Do not delete----
            //string id = Session["UserID"].ToString();
            //string VRegNo = Session["RegNo"].ToString();
            //----Important Code: Do not delete----

            string id = Session["UserID"].ToString();
            string VRegNo = txtVehicleNo.Text;//Session["RegNo"].ToString();
            string VN = Session["RegNo"].ToString();

            //string TankType;
            //if (rbtnPosTank.Checked)
            //{
            //    TankType = "0";
            //}
            //else
            //{
            //    TankType = "1";
            //}
            string Voltage;
            if (rbtn12V.Checked)
            {
                Voltage = "12";
            }
            else
            {
                Voltage = "24";
            }
            string Recharge;
            if (rbtnMonthly.Checked)
            {
                Recharge = "Monthly";
            }
            else
            {
                Recharge = "Yearly";
            }

            db.UpdateVehicle(VN, VRegNo, txtVehicleNo.Text, txtVehRegName.Text, Session["UserID"].ToString(), txtVehModel.Text, txtVehMake.Text, txtVehInsNum.Text, txtTankCapacity.Text, Voltage, txtExistMetRead.Text, txtKiloPulse.Text, txtDriverName.Text, txtDriverMobNum.Text, txtDriverADD.Text, txtDrivLicNum.Text, txtMobNum.Text, txtSIMProvider.Text, Recharge, txtSNum.Text, txtDOP.Text, txtDealerName.Text,  txtrtoname.Text, txtmsmmno.Text,txtmilage .Text ,ddlpt .SelectedValue .ToString (),ddlvc .SelectedValue .ToString ());
            db.UpdateVeh(VN, VRegNo, txtVehicleNo.Text, txtVehRegName.Text, Session["UserID"].ToString(), txtVehModel.Text, txtVehMake.Text, txtVehInsNum.Text, txtTankCapacity.Text, Voltage, txtExistMetRead.Text, txtKiloPulse.Text, txtDriverName.Text, txtDriverMobNum.Text, txtDriverADD.Text, txtDrivLicNum.Text, txtMobNum.Text, txtSIMProvider.Text, Recharge, txtSNum.Text, txtDOP.Text, txtDealerName.Text,  txtrtoname.Text, txtmsmmno.Text,txtmilage .Text ,ddlpt .SelectedValue .ToString (),ddlvc .SelectedValue .ToString ());
           
            Session["RegNo"] = VRegNo;

            Response.Redirect("JustVehView.aspx");
        }       

    }
}

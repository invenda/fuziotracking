﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetTyreDetails.aspx.cs"
    Inherits="Tracking.GetTyreDetails" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="height: 10px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ValidationGroup="Group1" ShowSummary="False" Height="32px" />
    </div>
    <div style="width: 100%; height: 50px; font-size: 25px; text-align: center; color: Blue;">
        View Tyre Mileage Details</div>
    <div style="width: 99%; font-size: 20px; text-align: center; margin: 0 auto; padding: 0;
        padding: 0;">
        <table width="100%">
            <tr>
                <td>
                    Select Vehicle
                    <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                        AutoPostBack="true" DataTextField="Vehicalnumber" Height="27px" Width="150px"
                        Style="margin-left: 1px" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                    </asp:DropDownList>
                    <%-- </td>
                <td align="right">--%>
                    Number of tyres
                    <%--</td>
                <td align="left">--%>
                    <asp:TextBox ID="txtNoOfTyres" runat="server" Width="100px" ReadOnly="true"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoOfTyres"
                        ValidationGroup="Group1" ErrorMessage="Please Enter No tyres">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="txtNoOfTyres"
                        ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                </td>
            </tr>
        </table>
        <div style="width: 100%; font-size: 25px; text-align: left; border-bottom-width: thin;
            margin: 0 auto; padding: 0; padding: 0;">
            <table border="1" style="width: 1290px">
                <tr>
                    <td colspan="4" align="center">
                        <asp:Label ID="lblledt" runat="server" Text="Left side" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                    <td colspan="4" align="center">
                        <asp:Label ID="Label1" runat="server" Text="Right side" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 163px">
                        Left Front Actuall Run Km
                    </td>
                    <td style="width: 119px">
                        <asp:TextBox ID="txtFtontLeft" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFtontLeft"
                            ValidationGroup="Group1" ErrorMessage="Please Enter Left Front tyre Actuall Run Km   ">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFtontLeft"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 162px">
                        Left front Alert Run Km
                    </td>
                    <td style="width: 123px">
                        <asp:TextBox ID="txtFrontleftytrelimit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFrontleftytrelimit"
                            ValidationGroup="Group1" ErrorMessage="Please Enter Left Front tyre Alert Run Km ">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrontleftytrelimit"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 172px">
                        Right Front Actuall Run Km
                    </td>
                    <td style="width: 129px">
                        <asp:TextBox ID="txtFtontRight1" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFtontRight1"
                            ValidationGroup="Group1" ErrorMessage="Please Enter Right Front tyre Actuall Run Km ">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFtontRight1"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 166px">
                        Right Front Alert Run Km
                    </td>
                    <td style="width: 122px">
                        <asp:TextBox ID="txtFronrighttytrelimit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFronrighttytrelimit"
                            ValidationGroup="Group1" ErrorMessage="Please Enter Right Front tyre Alrt Run Km ">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtFronrighttytrelimit"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttoatalfrontleft" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td style="width: 166px">
                        <asp:TextBox ID="txttoatalfrontright" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td colspan="4" align="center">
                        <asp:Label ID="Label2" runat="server" Text=" Rear Left side" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                    <td colspan="4" align="center">
                        <asp:Label ID="Label3" runat="server" Text="Rear Right side" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 163px">
                        Rear Left1 Actuall Run Km
                    </td>
                    <td style="width: 119px">
                        <asp:TextBox ID="txtrearleft1" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtrearleft1"
                            ValidationGroup="Group1" ErrorMessage="Please Enter  Left Rear tyre1 Actuall Run Km ">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtrearleft1"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 162px">
                        Rear Left1 Alert Run Km
                    </td>
                    <td style="width: 123px">
                        <asp:TextBox ID="txtrearleft1limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtrearleft1limit"
                            ValidationGroup="Group1" ErrorMessage="Please Enter  Left Rear tyre1 Alert Run Km">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtrearleft1limit"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 172px">
                        Rear Right1 Actuall Run Km
                    </td>
                    <td style="width: 129px">
                        <asp:TextBox ID="txtrearRight1" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtrearRight1"
                            ValidationGroup="Group1" ErrorMessage="Please Enter  Right Rear tyre1 Actuall Run Km">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtrearRight1"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 166px">
                        Rear Right1 Alert Run Km
                    </td>
                    <td style="width: 122px">
                        <asp:TextBox ID="txtreartight1limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtreartight1limit"
                            ValidationGroup="Group1" ErrorMessage="Please Enter  Right Rear tyre1 Alert Run Km">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtreartight1limit"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalreatleft1" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalrearight1" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                    </td>
                </tr>
                <tr>
                    <td style="width: 163px">
                        Rear Left2 Actuall Run Km
                    </td>
                    <td style="width: 119px">
                        <asp:TextBox ID="txtrearleft2" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtrearleft2"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtrearleft2"
                            ValidationGroup="Group1" ErrorMessage="Enter numbers Only!" ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 162px">
                        Rear Left2 Alert Run Km
                    </td>
                    <td style="width: 123px">
                        <asp:TextBox ID="txtrearleft2limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtrearleft2limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                            ControlToValidate="txtrearleft2limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 172px">
                        Rear Right2 Actuall Run Km
                    </td>
                    <td style="width: 129px">
                        <asp:TextBox ID="txtrearRight2" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtrearRight2"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                            ControlToValidate="txtrearRight2" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 166px">
                        Rear Right2 Alert Run Km
                    </td>
                    <td style="width: 122px">
                        <asp:TextBox ID="txtreartight2limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtreartight2limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                            ControlToValidate="txtreartight2limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalreatleft2" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalrearight2" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                    </td>
                </tr>
                <tr>
                    <td style="width: 163px">
                        Rear Left3 Actuall Run Km
                    </td>
                    <td style="width: 119px">
                        <asp:TextBox ID="txtrearleft3" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtrearleft3"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                            ControlToValidate="txtrearleft3" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 162px">
                        Rear Left3 Alert Run Km
                    </td>
                    <td style="width: 123px">
                        <asp:TextBox ID="txtrearleft3limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtrearleft3limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                            ControlToValidate="txtrearleft3limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 172px">
                        Rear Right3 Actuall Run Km
                    </td>
                    <td style="width: 129px">
                        <asp:TextBox ID="txtrearRight3" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtrearRight3"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                            ControlToValidate="txtrearRight3" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 166px">
                        Rear Right3 Alert Run Km
                    </td>
                    <td style="width: 122px">
                        <asp:TextBox ID="txtreartight3limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtreartight3limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                            ControlToValidate="txtreartight3limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalreatleft3" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalrearight3" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                    </td>
                </tr>
                <tr>
                    <td style="width: 163px">
                        Rear Left4 Actuall Run Km
                    </td>
                    <td style="width: 119px">
                        <asp:TextBox ID="txtrearleft4" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtrearleft4"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                            ControlToValidate="txtrearleft4" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 162px">
                        Rear Left4 Alert Run Km
                    </td>
                    <td style="width: 123px">
                        <asp:TextBox ID="txtrearleft4limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtrearleft4limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                            ControlToValidate="txtrearleft4limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 172px">
                        Rear Right4 Actuall Run Km
                    </td>
                    <td style="width: 129px">
                        <asp:TextBox ID="txtrearRight4" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtrearRight4"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                            ControlToValidate="txtrearRight4" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                    <td style="width: 166px">
                        Rear Right4 Alert Run Km
                    </td>
                    <td style="width: 122px">
                        <asp:TextBox ID="txtreartight4limit" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtreartight4limit"
                            ValidationGroup="Group1" ErrorMessage="Please Select Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                            ControlToValidate="txtreartight4limit" ValidationGroup="Group1" ErrorMessage="Enter numbers Only!"
                            ValidationExpression="^\d+$">*</asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalreatleft4" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td colspan="2" align="right">
                        Total Run Km
                    </td>
                    <td colspan="2" align="left">
                        <asp:TextBox ID="txttotalrearight4" runat="server" Width="95px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                    </td>
                </tr>
                <%--   <tr>
                    <td colspan="8" align="center">
                        <asp:Button ID="btnSubmit" Text="Submit" runat="server" Font-Bold="true" ValidationGroup="Group1"
                            OnClick="btnSubmit_Click" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnUpdate" Text="Update" runat="server" Font-Bold="true" 
                            ValidationGroup="Group1" onclick="btnUpdate_Click" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnclear" Text="Clear" runat="server" Font-Bold="true" 
                            onclick="btnclear_Click" />
                    </td>
                </tr>--%>
            </table>
        </div>
    </div>
</asp:Content>

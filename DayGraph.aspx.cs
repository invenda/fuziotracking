﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;
using System.ComponentModel;

namespace Tracking
{
    public partial class DayGraph : System.Web.UI.Page
    {
        public int igst = 0;
        public int idle = 0;
        public int sta = 0;
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                Download.Enabled = false;
                if (Session["UserID"] != null)
                {
                    Session["ChartNo"] = 1;  

                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        ddlMapTOVehicle.DataBind();                                             
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                    linknext.Enabled = false;
                    LinkButton1.Enabled = false;
               
                }


            }
        }
        //private void LoadChartData(DataTable initialDataSource)
        //{
        //    foreach (DataRow row in initialDataSource.Rows)
        //    {
        //        // For each Row add a new series
        //        string seriesName = row["no"].ToString();
        //        Chart1.Series.Add(seriesName);
        //        Chart1.Series[seriesName].ChartType = SeriesChartType.Line;
        //        Chart1.Series[seriesName].BorderWidth = 2;

        //        for (int colIndex = 1; colIndex < initialDataSource.Columns.Count; colIndex++)
        //        {
        //            // For each column (column 1 and onward) add the value as a point
        //            string columnName = myDataSet.Tables["Query"].Columns[colIndex].ColumnName;
        //            int YVal = (int)row[columnName];

        //            Chart1.Series[seriesName].Points.AddXY(columnName, YVal);
        //        }
        //    }
        //    for (int i = 1; i < initialDataSource.Columns.Count; i++)
        //    {
                
        //        Series series = new Series();
        //        foreach (DataRow dr in initialDataSource.Rows)
        //        {
        //            float y = float.Parse(dr[i].ToString());//, CultureInfo.InvariantCulture.NumberFormat);
        //            //int y = (int)dr[i];
        //            series.Points.AddXY(dr["no"].ToString(), y);
        //            //series.Points.AddXY(dr["no"].ToString(), YVal);
        //        }
        //       // CrtDayReport.Series.Add(series);
                
        //    }
        //}

        private DataTable GetData_Fuzio()
        {
 
            DBClass db1 = new DBClass(); 
           
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("no", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            string fdate12 = date.Text + " 12:00 AM";
           DateTime dhdh = Convert.ToDateTime(date.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;

            DataTable dt = db1.fuzio_daysummary(ddlMapTOVehicle.SelectedValue.ToString(), date.Text, "Fuzio_TKMRH");
            
            if (emonth == cmonth)
            {
                               
                //loop for all 24rows in tkrmh table for that date - siva 
                int no = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    no++;
                    DataRow dr = dt1.NewRow();
                    dr["no"] = no.ToString();
                    dr["igst"] = dt.Rows[i][7].ToString();
                    dr["idle"] = dt.Rows[i][8].ToString();
                    dr["Stat"] = dt.Rows[i][6].ToString();
                    dt1.Rows.Add(dr);
                }
            }
            return dt1;
        }
       


        private DataTable GetData()
        {
 
 
            DBClass db1 = new DBClass();
 
 
           
            DataTable dt = db1.getRhours();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("no", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            string fdate12 = date.Text + " 12:00 AM";
           DateTime dhdh = Convert.ToDateTime(date.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                int no = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    no++;
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    int igst = 0; int stat = 0; int idel = 0;
                    for (int k = 0; k < dt2.Rows.Count; k++)
                    {

                        if (dt2.Rows[k][4].ToString() == "0")
                        {
                            stat++;

                        }
                        if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            igst++;
                        }
                        if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            idel++;
                        }
                    }
                    dr["no"] = no.ToString();
                    dr["igst"] = igst.ToString();
                    dr["idle"] = idel.ToString();
                    dr["Stat"] = stat.ToString();
                    dt1.Rows.Add(dr);
                }
            }
            else
            {
                int no = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    no++;
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.searchold(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    int igst = 0; int stat = 0; int idel = 0;
                    for (int k = 0; k < dt2.Rows.Count; k++)
                    {

                        if (dt2.Rows[k][4].ToString() == "0")
                        {
                            stat++;

                        }
                        if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            igst++;
                        }
                        if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            idel++;
                        }
                    }
                    dr["no"] = no.ToString();
                    dr["igst"] = igst.ToString();
                    dr["idle"] = idel.ToString();
                    dr["Stat"] = stat.ToString();
                    dt1.Rows.Add(dr);
                }

            }
            return dt1;
        }
       




        protected void GetRecord_Click(object sender, EventArgs e)
        {
            BindChart();
            Panel1.Visible = true;
            Download.Enabled = true;
        }



        public void BindChart()
        {

            string mdate = date.Text + " 12:00 AM";
            DateTime dhdh = Convert.ToDateTime(mdate);

            string cmdate = "9/24/2018" + " 12:00 AM";
            DateTime cdhdh = Convert.ToDateTime(cmdate);

            string cmdatenew = "12/19/2018" + " 12:00 AM";
            DateTime cdhdhnew = Convert.ToDateTime(cmdatenew);

            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            DataTable dt2;
            if (dhdh > cdhdhnew)
            {
                 dt2 = GetData_Fuzio();
            }
            else
            {
                dt2 = GetData();
            }
            Double startX = 0;
            Double startY = 0;
            int i = 0; int Y = 0;

            if (Session["ChartNo"] != null )
            {
                int ChartNo = int.Parse(Session["ChartNo"].ToString());
                
                if (ChartNo == 1)
                {
                    CrtDayReport.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                    CrtDayReport.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    CrtDayReport.ChartAreas[0].AxisX.Minimum = 0;
                    CrtDayReport.ChartAreas[0].AxisX.Maximum = 730;
                    CrtDayReport.ChartAreas[0].AxisX.IsStartedFromZero = true;
                    CrtDayReport.ChartAreas[0].AxisX.Interval = 60;
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Clear();
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(40, 80, "1");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(100, 140, "2");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(160, 200, "3");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(220, 260, "4");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(280, 320, "5");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(340, 380, "6");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(400, 440, "7");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(460, 500, "8");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(520, 560, "9");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(580, 620, "10");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(640, 680, "11");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12"); 
                    CrtDayReport.ChartAreas[0].AxisX2.CustomLabels.Clear();
                    i = 0; Y = 1;
                    CrtDayReport.Titles[1].Text = "Time of the Day in Hrs(12 AM To 12 Noon)";
                    LinkButton1.Enabled = false;
                    linknext.Enabled = true;
                    

                }
                else if (ChartNo == 2)
                {
                    CrtDayReport.ChartAreas[0].AxisX.IsMarksNextToAxis = true;
                    CrtDayReport.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    CrtDayReport.ChartAreas[0].AxisX.Minimum = 720;
                    CrtDayReport.ChartAreas[0].AxisX.Maximum = 1445;
                    CrtDayReport.ChartAreas[0].AxisX.IsStartedFromZero = true;
                    CrtDayReport.ChartAreas[0].AxisX.Interval = 60;
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Clear();
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(700, 740, "12");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(760, 800, "1");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(820, 860, "2");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(880, 920, "3");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(940, 980, "4");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1000, 1040, "5");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1060, 1100, "6");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1120, 1160, "7");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1180, 1220, "8");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1240, 1280, "9");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1300, 1340, "10");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1360, 1400, "11");
                    CrtDayReport.ChartAreas[0].AxisX.CustomLabels.Add(1420, 1460, "12");   
                    CrtDayReport.ChartAreas[0].AxisX2.CustomLabels.Clear();
                    i = 12; Y = 13;
                    CrtDayReport.Titles[1].Text = "Time of the Day in Hrs(12 Noon To 12 AM)";
                    linknext.Enabled = false;
                    LinkButton1.Enabled = true;
                }
                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series1"].Points.AddXY(startX, startY);
                    //CrtDayReport.Series["Series1"].IsValueShownAsLabel = true;

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series1"].Points.AddXY(startX, startY);
                   

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series2"].Points.AddXY(startX, startY);
                    //CrtDayReport.Series["Series2"].IsValueShownAsLabel = true;

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series2"].Points.AddXY(startX, startY);
                   

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series3"].Points.AddXY(startX, startY);
                   // CrtDayReport.Series["Series3"].IsValueShownAsLabel = true;


                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series3"].Points.AddXY(startX, startY);
                   
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series4"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series4"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series5"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series5"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series6"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series6"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series7"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series7"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series8"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series8"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series9"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series9"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series10"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series10"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series11"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series11"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series12"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series12"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series13"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series13"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series14"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series14"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series15"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series15"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series16"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series16"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series17"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series17"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series18"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series18"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series19"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series19"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series20"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series20"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series21"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series21"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series22"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series22"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series23"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series23"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series24"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series24"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series25"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series25"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series26"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series26"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series27"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series27"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series28"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series28"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series29"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series29"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series30"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series30"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series31"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series31"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series32"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series32"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series33"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series33"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;

                if (i < Y)
                {
                    startX = i * 60;
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series34"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["igst"].ToString());
                    CrtDayReport.Series["Series34"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series35"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["idle"].ToString());
                    CrtDayReport.Series["Series35"].Points.AddXY(startX, startY);

                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series36"].Points.AddXY(startX, startY);

                    startX += Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    startY = Convert.ToDouble(dt2.Rows[i]["Stat"].ToString());
                    CrtDayReport.Series["Series36"].Points.AddXY(startX, startY);
                    startX = 0;
                    startY = 0;
                }
                i++;
                Y++;
            }


        }
        protected void Refresh_Click(object sender, EventArgs e)
        {          
            Response.Redirect(Request.RawUrl);  
        }

        protected void linknext_Click(object sender, EventArgs e)
        {
            if (Session["ChartNo"] != null)
            {
               
                Session["ChartNo"] = int.Parse(Session["ChartNo"].ToString()) + 1;
                BindChart();
               
            }
           
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Session["ChartNo"] != null)
            {
               
                Session["ChartNo"] = int.Parse(Session["ChartNo"].ToString()) - 1;
                BindChart();
               
            }
           
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            DateTime dt = Convert.ToDateTime(date.Text);
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VTC_" + vstr.Replace(" ", "") + "_" + str1 + ".pdf";
           // bindchartdata();
            BindChart();
            MemoryStream m = new MemoryStream();
            Document document = new Document(PageSize.A3.Rotate(), 60, 0, 30, 10);
            try
            {
                System.IO.StringWriter sw = new System.IO.StringWriter();
                XhtmlTextWriter hw = new XhtmlTextWriter(sw);
                //tblToExport.RenderControl(hw);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                PdfWriter.GetInstance(document, m);
                document.Open();
                PdfPTable table = new PdfPTable(3);
                PdfPCell cell = new PdfPCell(new Phrase(lblhead.Text, new Font(Font.TIMES_ROMAN, 24f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 0;
                PdfPCell cell0 = new PdfPCell(new Phrase("  "));
                cell0.Colspan = 3;
                cell0.BorderWidth = 0;
                PdfPCell cell1 = new PdfPCell(new Phrase(lblVehrtono.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell1.BorderWidth = 0;
                PdfPCell cell2 = new PdfPCell(new Phrase(lblreqtime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell2.BorderWidth = 0;
                PdfPCell cell3 = new PdfPCell(new Phrase(lbltemp.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell3.BorderWidth = 0;
                PdfPCell cell4 = new PdfPCell(new Phrase(lbltime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell4.BorderWidth = 0;
                PdfPCell cell5 = new PdfPCell(new Phrase(lbldriname.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell5.BorderWidth = 0;
                PdfPCell cell6 = new PdfPCell(new Phrase(lbldrimob.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell6.BorderWidth = 0;
                table.AddCell(cell);
                table.AddCell(cell0);
                table.AddCell(cell1);
                table.AddCell(cell2);
                table.AddCell(cell3);
                table.AddCell(cell4);
                table.AddCell(cell5);
                table.AddCell(cell6);
                document.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    CrtDayReport.SaveImage(stream, ChartImageFormat.Png);
                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                    chartImage.ScalePercent(75f);
                    document.Add(chartImage);
                }

                document.Add(new Paragraph(sw.ToString()));
            }
            catch (DocumentException ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
                Console.Error.WriteLine(ex.Message);
            }
            document.Close();
            Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
        }
    }
}

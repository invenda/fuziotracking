﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="View_Manager_geovehicels.aspx.cs" MasterPageFile="~/Manager_geo.Master" Inherits="Tracking.View_Manager_geovehicels" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">  
    <script type = "text/javascript">
     function Confirm() {
         var confirm_value = document.createElement("INPUT");
         confirm_value.type = "hidden";
         confirm_value.name = "confirm_value";
         if (confirm("Do you want delete circle?")) {
             confirm_value.value = "Yes";
         } else {
             confirm_value.value = "No";
         }
         document.forms[0].appendChild(confirm_value);
     }
    </script>
    <div style="width: 100%;">
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ValidationGroup="circleinfo" ShowSummary="False" />
    <div >
        <div style="text-align: center; color: Blue; font-size: x-large; font-style: italic;">
        <asp:Label ID="lblheading" runat="server" ></asp:Label>
        </div>  
        <div style="height:4    5px; text-align:right;">
            <asp:Button ID="btndelete" runat="server"  Font-Bold="true" 
                Font-Italic="true" ForeColor="White" 
                BackColor="BlueViolet" Text="Delete circle" OnClientClick="Confirm()" 
                onclick="btndelete_Click" BorderStyle="Solid" CssClass="button123" />&nbsp;&nbsp;&nbsp;&nbsp;
        
        </div>     
        <div>
            <cc1:GMap ID="GMap1" runat="server"  enableServerEvents="true" Width="100%" Height="600px"/>
        </div>
    </div>
   </div>
</asp:Content>
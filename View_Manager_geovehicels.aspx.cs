﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class View_Manager_geovehicels : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            //GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
            //GMap1.Add(extMapType);
            //Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
            //GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
            //GMap1.enableHookMouseWheelToZoom = true;
            if (!IsPostBack)
            {
                DataTable dt = db.Get_Manager_Geo(Session["UserID"].ToString());
                if (dt.Rows.Count > 0)
                {
                    bindpoints();
                }
                else
                {
                    lblheading.Text = " View/Delete Manager Geofence Circle";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Create  Geofence circle');", true);
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GMap1.Add(extMapType);
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                    GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.enableHookMouseWheelToZoom = true;
                }
            }

        }

        public void bindpoints()
        {          
           
                GMap1.resetMarkers();
                GMap1.resetPolygon();
                DataTable dt = db.Get_Manager_Geo_latlan(Session["userID"].ToString());
                if (dt.Rows.Count > 0)
                {
                    
                    for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                    {
                        drawcircle(Convert.ToDouble(dt.Rows[i][0].ToString()), Convert.ToDouble(dt.Rows[i][1].ToString()), Convert.ToDouble(dt.Rows[i][2].ToString()) / 1609.3, j);
                    }
                    lblheading.Text = "View/Delete Geofence at" +" "+ dt.Rows[0][3].ToString();
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GMap1.Add(extMapType);
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[0][0].ToString()), Convert.ToDouble(dt.Rows[0][1].ToString()));
                    GMap1.setCenter(gLatLng, 15, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.enableHookMouseWheelToZoom = true;
                }
                else
                {
                    lblheading.Text = "View/Delete Geofence Circle";
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                    GMap1.Add(extMapType);
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                    GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.enableHookMouseWheelToZoom = true;

                }
                
            

        }
        public void drawcircle(double lt, double lg, double r, int gp)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1);// 16, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
            //GMap1.addGMarker(oMarker);
            GMap1.Add(oMarker);
           // options.title = "Route No " + vn;
            oMarker.options = options;

        }

        protected void btndelete_Click(object sender, EventArgs e)
        {string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            DataTable dt = db.Get_Manager_Geo(Session["UserID"].ToString());
            if (dt.Rows.Count > 0)
            {
                db.Delete_Manager_Geo(Session["UserID"].ToString());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Geofence circle deleted sucessfully..');", true);
                bindpoints();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('There is No circle to delete..Please Create Geofence circle');", true);
            }
        }

        }

    }
}
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Admin
{
    public partial class CreateDevices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = true;
                }
                else
                {
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = false;
                }
            }
        }
        protected void btSubmit_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            db.CreateDevice(txtDeviceID.Text, txtdate.Text);
            txtDeviceID.Text = string.Empty;
            txtdate.Text = string.Empty;
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Device Created Successfully');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }
        protected void btClear_click(object sender, EventArgs e)
        {
            txtDeviceID.Text = string.Empty;
            txtdate.Text = string.Empty;
           
        }

        protected void submit_Click(object sender, EventArgs e)
        {

        }
    }
}

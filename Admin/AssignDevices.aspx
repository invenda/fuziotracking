﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignDevices.aspx.cs"
    Inherits="Tracking.Admin.AssignDevices" MasterPageFile="~/TrackingMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <div >
        <div id="sublogo">
            <h1>
                <a href="#">Assign Devices To Customers</a>
            </h1>               
        </div>
    </div>
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblselect" Text="Select Customer" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCustomers" runat="server" DataValueField="ID" DataTextField="UserName" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomers_Changed">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <table style="height: 319px; width: 456px">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label1" Text="Available Devices" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" Text="Assinged Devices" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListBox ID="AvailableDevices" DataTextField="Device_ID" runat="server" Height="297px"
                                            Width="146px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btInsert" Text=">>" runat="server" OnClick="btInsert_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td>
                                                    <asp:Button ID="btDelete" Text="<<" runat="server" OnClick="btDelete_Click" />
                                                </td>--%>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="AssingedDevices" runat="server" Height="297px" Width="146px" SelectionMode="Multiple" DataTextField="Device_ID">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
           <tr>
                <td>               
                   <asp:Button ID="btSubmit" runat="server" Text="Submit" 
                        onclick="btSubmit_Click"  />
                </td>
                <td>
                   <asp:Button ID="btClear" runat="server" Text="Clear" onclick="btClear_Click"  />
                </td>
            </tr>
        </table>
    </div>
   
</asp:Content>

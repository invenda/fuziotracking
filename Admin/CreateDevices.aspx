﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateDevices.aspx.cs" Inherits="Tracking.Admin.CreateDevices" 
MasterPageFile="~/TrackingMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <div >
        <div id="sublogo">
            <h1>
                <a href="#">Create Devices</a>
            </h1>               
        </div>
    </div>
    <div>
     <table>
            <tr>
                <td>
                    <asp:Label ID="lblDevice" Text="DeviceID" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDeviceID" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfusername" runat="server" 
                        ControlToValidate="txtDeviceID" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbldate" Text="Manufacture Date" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtdate" runat="server" ></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtdate" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>
               
                   <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" />
                </td>
                <td>
                   <asp:Button ID="btClear" runat="server" Text="Clear" OnClick="btClear_click" />
                </td>
            </tr>
            </table>
    </div>
   
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUsers.aspx.cs" Inherits="Tracking.Admin.CreateUsers"
    MasterPageFile="~/TrackingMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="sublogo">
            <h1>
                <a href="#">Create Users</a>
            </h1>
        </div>
    </div>
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblUsername" Text="UserName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfusername" runat="server" ControlToValidate="txtUsername"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPassword" Text="Password" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblretypePassword" Text="RetypePassword" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtRetypePassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRetypePassword"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password incorrect"
                        ControlToValidate="txtRetypePassword" ControlToCompare="txtPassword"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFirstName" Text="FirstName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFirstName"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLastName" Text="LastName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLastName"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserRole" Text="UserRole" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlUserRole" runat="server" DataValueField="UserRole_ID" DataTextField="UserRole_Name">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" />
                </td>
                <td>
                    <asp:Button ID="btClear" runat="server" Text="Clear" OnClick="btClear_click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Admin
{
    public partial class CreateUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
 
 
                DBClass db = new DBClass();
 
 
                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
                    ddlUserRole.DataMember = "UserRole_ID";
                    ddlUserRole.DataSource = db.GetUserRoles(Session["UserRole"].ToString());
                    ddlUserRole.DataBind();
                    ddlUserRole.Items.Remove("FactoryAdmin");
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = true;
                }
                else
                {
                    ddlUserRole.DataMember = "UserRole_ID";
                    ddlUserRole.DataSource = db.GetUserRoles(Session["UserRole"].ToString());
                    ddlUserRole.DataBind();                   
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = false;
                }
            }
        }
        protected void btSubmit_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserRole"] != null )
            {
                db.CreateUser(txtUsername.Text, txtPassword.Text, txtFirstName.Text, txtLastName.Text, ddlUserRole.SelectedItem.Value, Session["UserID"].ToString());
            }
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtRetypePassword.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('User Created Successfully');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }
        protected void btClear_click(object sender, EventArgs e)
        {
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtRetypePassword.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;            
        }
    }
}

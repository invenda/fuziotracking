﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking.Admin
{
    public partial class AssignDevices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 
                ddlCustomers.DataMember = "UserName";
                ddlCustomers.DataSource = db.GetAllCustomers();
                ddlCustomers.DataBind();

                AvailableDevices.DataSource = db.GetAllDevices();
                AvailableDevices.DataBind();

                AssingedDevices.DataSource = db.GetAssingedDevices(ddlCustomers.SelectedValue);
                AssingedDevices.DataBind();


                if (Session["UserRole"] != null && Session["UserRole"].ToString() == "2")
                {
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = true;
                }
                else
                {
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    TrackingMaster tm = (TrackingMaster)Page.Master;
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
 // The type 'TrackingMaster' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs' conflicts with the imported type 'TrackingMaster' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\TrackingMaster.Master.cs'.
                    tm.MenuVisible = false;
                }
            }
        }
        private void btnMoveRight_Click(object sender, System.EventArgs e)
        {
            for (int i = AvailableDevices.Items.Count - 1; i >= 0; i--)
            {
                if (AvailableDevices.Items[i].Selected == true)
                {
                    AssingedDevices.Items.Add(AvailableDevices.Items[i]);
                    ListItem li = AvailableDevices.Items[i];
                    AvailableDevices.Items.Remove(li);
                }
            }
        }

        private void btnMoveLeft_Click(object sender, System.EventArgs e)
        {
            for (int i = AssingedDevices.Items.Count - 1; i >= 0; i--)
            {
                if (AssingedDevices.Items[i].Selected == true)
                {
                    AvailableDevices.Items.Add(AssingedDevices.Items[i]);
                    ListItem li = AssingedDevices.Items[i];
                    AssingedDevices.Items.Remove(li);
                }
            }
        }

        protected void btInsert_Click(object sender, EventArgs e)
        {
            for (int i = AvailableDevices.Items.Count - 1; i >= 0; i--)
            {
                if (AvailableDevices.Items[i].Selected == true)
                {
                    AssingedDevices.Items.Add(AvailableDevices.Items[i]);
                    ListItem li = AvailableDevices.Items[i];
                    AvailableDevices.Items.Remove(li);
                }
            }

        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            for (int i = AssingedDevices.Items.Count - 1; i >= 0; i--)
            {
                if (AssingedDevices.Items[i].Selected == true)
                {
                    AvailableDevices.Items.Add(AssingedDevices.Items[i]);
                    ListItem li = AssingedDevices.Items[i];
                    AssingedDevices.Items.Remove(li);
                }
            }
        }

        protected void btSubmit_Click(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            for (int i = AssingedDevices.Items.Count - 1; i >= 0; i--)
            {
                if (AssingedDevices.Items[i].Selected == true)
                {
                    db.AssignDevices(AssingedDevices.Items[i].Text, ddlCustomers.SelectedValue);
                }
            }
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Assinged Successfully');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }

        protected void btClear_Click(object sender, EventArgs e)
        {

        }
        protected void ddlCustomers_Changed(object sender, EventArgs e)
        {
 
 
            DBClass db = new DBClass();
 
 
            AvailableDevices.DataSource = db.GetAllDevices();
            AvailableDevices.DataBind();

            AssingedDevices.DataSource = db.GetAssingedDevices(ddlCustomers.SelectedValue);
            AssingedDevices.DataBind();
        }
    }
}

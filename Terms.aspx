﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="Tracking.Terms" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terms & Conditions</title>

    <script language="javascript" type="text/javascript">
  function disableCtrlKeyCombination(e)
        {
                //list all CTRL + key combinations you want to disable
                var forbiddenKeys = new Array("a", "s", "c");
                var key;
                var isCtrl;

                if(window.event)
                {
                        key = window.event.keyCode;     //IE
                        if(window.event.ctrlKey)
                                isCtrl = true;
                        else
                                isCtrl = false;
                }
                else
                {
                        key = e.which;     //firefox
                        if(e.ctrlKey)
                                isCtrl = true;
                        else
                                isCtrl = false;
                }

                //if ctrl is pressed check if other key is in forbidenKeys array
                if(isCtrl)
                {
                    for (i = 0; i < forbiddenKeys.length; i++)
                        {
                                //case-insensitive comparation
                            if (forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                                {
//                                    alert("Key combination CTRL + "
//                                                + String.fromCharCode(key)
//                                                + " has been disabled.");                                    
                                        return false;
                                }
                        }
                }
                return true;
        }
    </script>

</head>
<body style="background-color: Silver" oncontextmenu="return false" onkeypress="return disableCtrlKeyCombination(event);"
    onkeydown="return disableCtrlKeyCombination(event);">
    <form id="form1" runat="server">
    <div>
        <p style="font-size: 12px">
            <span style="color: Black; font-size: x-large; text-decoration: underline">Terms and
                Conditions</span><br />
            <br />
            <span style="color: Black; font-size: large">For Use of Vehicle Tracking Solutions ESlabsTM,
                www.eslabs.co.in, & Vehicle Tracking websites</span>
            <br />
            <br />
            <span style="color: Black; font-size: large">TERMS OF USE AND LEGAL NOTICES- (version-
                2012.3.31)</span>
            <br />
            <br />
            <span style="color: Black; font-size: large">Agreement to be Bound</span><br />
            Welcome to the website (the “Site”) And Vehicle Tracking Solutions, a service of
            Empowered Security Labs Pvt Limited (ESlabs). By accessing or using the Product
            & this Site, you agree to be bound by the terms of use set forth below (the “Terms
            of Use”).<br />
            <br />
            This Subscription Service Agreement and Terms of Purchase (“Agreement”) is made
            and entered into immediately upon acceptance of its terms and conditions by you,
            or immediately upon your use of Services, as defined herein, and is between you
            and Company, as defined in the last paragraph of this Agreement.<br />
            This Agreement contains the terms and conditions which apply to the GPS product(s)
            you have purchased or are purchasing, whether purchased from Company, a Reseller,
            an agent or from any entity acquired by Company (collectively the “Product” or “Products”)
            and upon which Company will provide or has provided to you the Services, as defined
            below, respecting such Products. You agree that this Agreement governs any dispute
            related to the Products or Services even if such dispute arose prior to this Agreement.<br />
            <br />
            If you do not wish to be bound by these Terms of Use, you may not access or use
            the Site & the product. ESlabs may modify these Terms of Use at any time, and such
            modifications shall be effective immediately upon posting of the modified Terms
            of Use. By continuing to use the Site after such changes are posted, you accept
            these Terms of Use as modified.<br />
            <br />
            <span style="color: Black; font-size: large">Use of Information</span><br />
            Unless otherwise specified, the entire contents of the Site are copyrighted under
            the India copyright laws. The owner of the copyright is ESlabs and its partners,
            where applicable. To the extent that ESlabs has the right to do so without compensation
            to third parties, and except for materials or information specifically provided
            under other terms, ESlabs grants you permission to use the Site in connection with
            our ESlabs service and to copy or otherwise download from the Site, information
            and materials including related graphics, subject to the following restrictions:<br />
            <br />
            <span style="color: Black; font-weight: bold">1.</span> You may not remove, or disassociate,
            from any of the materials & you acknowledge and agree that the information and Services
            provided by Company&nbsp; are accessed by you in part through the Website. You accept
            and agree to comply with the Terms of Use, Privacy Policy and copyright and trademark
            notices of Company posted on the Website and in effect from time to time.<br />
            <span style="color: Black; font-weight: bold">2.</span> You may not modify, reproduce,
            display, perform, distribute, prepare derivative works from, or otherwise use the
            materials for any purpose not expressly permitted by these terms of use.<br />
            <span style="color: Black; font-weight: bold">3.</span> You may not disclose account
            information (including any access codes) to any third party and you may not transfer
            the materials to any other person unless you give them notice of, and they agree
            to accept, the obligations arising under these Terms of Use; and<br />
            <span style="color: Black; font-weight: bold">4.</span> You may not use this Site
            in violation of any applicable laws or in any manner that is harassing, libelous,
            defamatory, obscene, or threatening.<br />
            <span style="color: Black; font-weight: bold">5.</span> Documents posted by ESlabs
            on the Site may contain other proprietary notices or describe products, services,
            processes or technologies owned by ESlabs or third parties. Nothing contained herein
            shall be construed by implication, estoppels or otherwise as granting to you a license
            under any copyright, trademark, patent or other intellectual property right of ESlabs
            or any third party.<br />
            <span style="color: Black; font-weight: bold">6.</span> You acknowledge and agree
            that, because the Services are provided in part through the Website, it is necessary
            for you to have computer equipment and an internet connection that meets minimum
            specifications published by Company from time to time on the Website, and you acknowledge
            and agree to periodically update your computer equipment or internet connection
            to meet such minimum specifications.<br />
            <span style="color: Black; font-weight: bold">7.</span> You acknowledge that the
            Services may be interrupted due to (a) Website downtime for scheduled maintenance
            at Company’s sole discretion, or (b) interruptions in internet connectivity or other
            Website downtime caused by circumstances beyond Company’s control, including, without
            limitation, acts of God, acts of government, flood, fire, earthquakes, civil unrest,
            acts of terror, strikes or other labor problems, computer or telecommunications
            failures, delays involving hardware or software not within Company’s control, network
            intrusions, or denial of service attacks. You agree that Company shall not, in any
            way, be liable for, or have responsibility with respect to, any such Service interruptions.<br />
            <br />
            <span style="color: Black; font-size: large">Services and Charges</span><br />
            In consideration of payment of the Charges (as defined below), Company will provide
            the Services (as defined below). The term “Services” means, collectively, (a) the
            provision to you of location, operation and other information (including, without
            limitation, the location of the Monitored Vehicle, notifications of when the Monitored
            Vehicle goes into motion and such other information concerning the location or operation
            of the Monitored Vehicle as Company may elect to provide from time to time); (b)
            if you purchased such option, the provision to you of start, stop and idle times,
            fuel consumption, and top speeds of the Monitored Vehicle; (c) if you purchased
            such option, the ability to disable and enable the ignition system of the Monitored
            Vehicle using the; (d) if you purchased such option, the ability to lock and unlock
            the entry doors of the Monitored Vehicle using the Website; and (e) such other services
            as Company may elect to provide to you in its sole and absolute discretion from
            time to time. With respect to all Services, Services will only be available for
            a vehicle on which the Product is properly installed and is properly registered
            (the “Monitored Vehicle”) and will only be provided through a Company website providing
            Services (“Website”) or using such other means as Company may elect from time to
            time. There are no other services provided under this Agreement. The term “Charges”
            means the total amount due for the Product and Services, including without limit
            any applicable late charges, penalties or interest, purchased by you from time to
            time and all sales, use and other taxes, fees and charges that may be imposed by
            any governmental body relating to the sale of Products and provision of Services.
            In the event Service was deactivated for a Product and you request the reactivation
            of the Service, you agree to pay Company a reactivation fee specified by Company
            or Reseller, or such other reasonable amount as may be established by Company. If
            you have prepaid for Service and elect to cancel the Service prior to the end of
            the prepaid term, you are not entitled to a refund for the unused Service, except
            as otherwise provided herein.<br />
            <br />
            <span style="color: Black; font-size: large">User Id and Password</span><br />
            During the registration process for your Product a user name and password will be
            created which allows you to have access to the Services through the Website. You
            will not provide your user name or password to access Services to any other person
            or entity, or allow any other person or entity to access Services provided to you
            under your user name and password. You agree that you are solely responsible for
            any actions that occur under your user name and password. In the event that your
            user name and password become known by a third party, you agree to notify Company
            immediately.<br />
            <br />
            <span style="color: Black; font-size: large">Website</span><br />
            You acknowledge and agree that the information and Services provided by Company
            are accessed by you in part through the Website. You accept and agree to comply
            with the Terms of Use, Privacy Policy and copyright and trademark notices of Company
            posted on the Website and in effect from time to time. You acknowledge and agree
            that, because the Services are provided in part through the Website, it is necessary
            for you to have computer equipment and an internet connection that meets minimum
            specifications published by Company from time to time on the Website, and you acknowledge
            and agree to periodically update your computer equipment or internet connection
            to meet such minimum specifications. You acknowledge that the Services may be interrupted
            due to (a) Website downtime for scheduled maintenance at Company’s sole discretion,
            or (b) interruptions in internet connectivity or other Website downtime caused by
            circumstances beyond Company’s control, including, without limitation, acts of God,
            acts of government, flood, fire, earthquakes, civil unrest, acts of terror, strikes
            or other labor problems, computer or telecommunications failures, delays involving
            hardware or software not within Company’s control, network intrusions, or denial
            of service attacks. You agree that Company shall not, in any way, be liable for,
            or have responsibility with respect to, any such Service interruptions.<br />
            <br />
            <span style="color: Black; font-size: large">Trademarks</span><br />
            The ESlabs name and logo and all related product and service names, design marks
            and slogans are trademarks, service marks or registered trademarks of ESlabs and
            may not be used in any manner without the prior written consent of ESlabs. Other
            products and service marks are trademarks of their respective owners.
            <br />
            <br />
            <span style="color: Black; font-size: large">Consent to Monitoring</span><br />
            ESlabs is under no obligation to monitor the information residing or transmitted
            to the Site. However, you agree that ESlabs may monitor the Site to (1) comply with
            any necessary laws, regulations or governmental requests; (2) to, in its sole discretion,
            operate the Site in a manner it deems proper or to protect against conduct it deems
            inappropriate. ESlabs shall have the right but not the obligation, to reject or
            eliminate any information residing on or transmitted to the Site that it, in its
            sole discretion, believes is unacceptable or inconsistent with these Terms of Use.<br />
            <br />
            <span style="color: Black; font-size: large">Equipment</span><br />
            The Subscriber is responsible for choosing the Equipment to ensure it is suitable
            for his purpose. The Subscriber shall not nor permit any other person to tamper
            with, modify, add, dismantle or otherwise interfere with the Equipment, Title for
            all equipment provided by ESlabs under the terms of this agreement shall remain
            with ESlabs in any event until all the dues are paid. ESlabs reserves the right
            to charge the Subscriber for any costs incurred as a result of breach of this clause.<br />
            The Subscriber is responsible for maintaining the Equipment in a good state of repair
            and condition and ensuring the Equipment is used for its intended purpose and must
            inform ESlabs of any faults immediately.<br />
            ESlabs reserves the right to inspect the Equipment at all times upon reasonable
            notice and allow safety or ownership labels to be affixed to the Equipment.<br />
            ESlabs do not guarantee or warranty or condition (express or implied) about the
            description, suitability or fitness for purpose of the Equipment or that it is of
            satisfactory quality and, to the extent permitted by law, any such warranties and
            conditions implied by statute or otherwise are excluded.<br />
            Should the Equipment fail for any reason, the Subscriber must notify ESlabs immediately
            and allow ESlabs a reasonable time period to rectify the problem. The Subscriber
            must continue to pay all dues regardless of any technical issue with the Equipment
            and ESlabs shall make any reasonable credit payments back to the Subscriber if it
            is found that a real problem or issue caused the Equipment to fail. And the maximum
            liability for such failures shall not be more than Rs. 5000/- after installation
            and activation of the equipment.<br />
            ESlabs reserve the right to charge an administration fee for each maintenance visit
            conducted by ESlabs engineers when an engineer is requested by the Subscriber to
            service or fix a fault or issue with the Equipment after the warranty period.<br />
            The Subscriber shall be responsible in present or in future for paying any governmental
            fees, fines, duties, insurance premiums and other payments due for the Equipment.<br />
            <br />
            <span style="color: Black; font-size: large">Software</span><br />
            The Subscriber is responsible for choosing any software in the Equipment to ensure
            it is suitable for his purpose. ESlabs shall not be held responsible for access
            problems or faults with the Service via the internet or for interrupted access to
            the Service. The Subscriber shall abide by all software licences provided by the
            software suppliers and shall indemnify ESlabs against any claim made against ESlabs
            in relation to any breach of any licence. The Subscriber shall be responsible for
            paying any licence fees, fines, duties, insurance premiums and other payments due
            for the software.<br />
            <br />
            <span style="color: Black; font-size: large">Limited Product Warranty</span><br />
            Company hereby warrants (“Limited Warranty”) only to the purchaser that first activates
            the Product and not to any third party, that the Product will be free from defects
            in workmanship and materials for a period (“Limited Warranty Period”) of ONE (1)
            calendar year after the date a rate plan is assigned to your Product, unless you
            have purchased a two or three year warranty, in which case the warranty lasts for
            the amount of warranty coverage you purchase, which cannot exceed three years. The
            Limited Warranty does not apply to normal wear and tear, damage to the Product caused
            by tampering, misuse, accident, abuse, neglect, improper installation, misapplication,
            alteration of any kind, disaster, defects due to repairs or modifications made by
            anyone other than Company or an authorized service representative of Company, physical
            damage of any nature whatsoever to the Product, including any opening or attempted
            opening of the Product, or reception problems caused by signal conditions or cable
            or antenna systems outside the Product. THE REPAIR OR REPLACEMENT OF THE PRODUCT
            AS PROVIDED UNDER THIS LIMITED WARRANTY IS THE SOLE AND EXCLUSIVE REMEDY. THE SOFTWARE
            LOADED ON THE PRODUCT IS PROVIDED “AS IS” WITHOUT WARRANTY. TO THE MAXIMUM EXTENT
            PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL COMPANY BE LIABLE FOR ANY INCIDENTAL,
            CONSEQUENTIAL, SPECIAL, INDIRECT, PUNITIVE OR EXEMPLARY DAMAGES OR LOST PROFITS
            WHATSOEVER ARISING OUT OF THE USE OR INABILITY TO USE THE PRODUCT, OR FOR ANY BREACH
            OF THIS LIMITED WARRANTY OR OTHERWISE, EVEN IF COMPANY HAS BEEN ADVISED OF THE POSSIBILITY
            OF SUCH DAMAGES OR SUCH DAMAGES COULD REASONABLY HAVE BEEN FORESEEN. TO THE MAXIMUM
            EXTENT PERMITTED BY LAW, COMPANY EXPRESSLY DISCLAIMS, AND YOU EXPRESSLY WAIVE, ALL
            OTHER WARRANTIES, WHETHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING WITHOUT LIMITATION
            ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR USE,
            OR ANY WARRANTY ARISING OUT OF ANY PROPOSAL, SPECIFICATION, OR SAMPLE. THE TERM
            OF ANY IMPLIED WARRANTIES THAT CANNOT BE DISCLAIMED UNDER APPLICABLE LAW SHALL BE
            LIMITED TO THE DURATION OF THE FOREGOING LIMITED WARRANTY PERIOD. You agree that
            neither Company nor any other party has made any representations or warranties,
            nor have you relied on any representations or warranties, express or implied, including
            any implied warranty of merchantability or fitness for any particular purpose with
            respect to the Products. You acknowledge that no affirmation of fact or statement
            (whether written or oral) made by Company, its representatives, a Reseller or any
            other party outside of this Agreement with respect to the Products shall be deemed
            to create any express or implied warranty on the part of Company, its representatives,
            or a Reseller. To obtain warranty service, contact the Customer Care using the support
            number located within the installation guide for the Product or the Reseller from
            which you purchased the product. Provide them with the ESN (Equipment Serial Number)
            for each Product, for warranty coverage verification. Upon verification of coverage
            you can Package the Product and send to Reseller or Company or, with the Invoice
            copy ship to: Empowered Security Labs Private Limited, 1st Floor, No 345/21, 38th
            Cross, 28th Main, 9th Block, Jayanagar, Bangalore, Karnataka, India. Pin: 560 069
            (Note: You are responsible for shipping charges to the returns department.) Company
            will test all properly returned products to determine if they are defective. If
            the product is defective, Company will replace the defective product(s), and Company
            is responsible for shipping charges back to you.<br />
            <br />
            <span style="color: Black; font-size: large">Third Party Interfaces</span><br />
            ESlabs may provide mechanisms to export fleet location data in a format that is
            compatible with third-party software, for example, Google Earth KML format, for
            your convenience. ESlabs does not in any manner endorse, support, resell, or license
            such third-party software. You will need to directly contact these software vendors/providers
            for information on their license and support.<br />
            <br />
            <span style="color: Black; font-size: large">Information Provided by You</span><br />
            You agree that all information provided by you shall be, to the best of your knowledge,
            accurate, current, correct, and complete. ESlabs is not responsible for incomplete
            or inaccurate information. You agree not to provide any information that would violate
            any third party’s intellectual property rights. You agree not to post viruses or
            any harmful software or data that would interfere with the functioning of or damage
            of this Site.<br />
            <br />
            <span style="color: Black; font-size: large">Endorsements and Linked Sites</span><br />
            Some of the sites listed as links herein are not under the control of ESlabs. Accordingly,
            ESlabs makes no representations whatsoever concerning the content of those sites.
            The fact that ESlabs has provided a link to a site is NOT an endorsement, authorization,
            sponsorship, or affiliation by ESlabs with respect to such site, its owners, or
            its providers. ESlabs is providing these links only as a convenience to you. ESlabs
            has not tested any information, software, or products found on these sites and therefore
            cannot make any representations whatsoever with respect thereto. There are risks
            in using any information, software, or products found on the Internet; and ESlabs
            cautions you to make sure that you completely understand these risks before retrieving,
            using, relying upon, or purchasing anything via the Internet.<br />
            <br />
            <span style="color: Black; font-size: large">Warranty and Liability</span><br />
            Notwithstanding any other provision of this Agreement,<br />
            ESlabs does not seek to exclude or restrict its liability for fraud, or for death
            or personal injury caused by its negligence.<br />
            ESlabs shall not be liable to the Subscriber for loss of profits or any other indirect
            or consequential loss whether arising from tort (including negligence) or breach
            of contract or otherwise.<br />
            The Equipment is an electronic assembly that consumes an electrical current. It
            is designed to draw small amounts of current when the vehicle is not being used
            and there is a small drain on the vehicle battery that may adversely affect vehicles
            that are not in regular use. ESlabs accepts no liability for any consequences of
            the battery drain associated with use of the Equipment and recommends that the battery
            should be recharged periodically to ensure maximum performance.<br />
            The Equipment (together with any optional extras supplied by ESlabs and installed
            in the vehicle) must not be used in any way which would or may affect the ability
            of the driver of the vehicle to drive safely. ESlabs shall not be liable for any
            loss or damage caused by the acts or omissions of the driver of the vehicle.<br />
            The Subscriber acknowledges that, owing to the nature of the technology comprised
            in the Equipment, the operation of the Service may from time to time be adversely
            affected by physical features such as underpasses, atmospheric conditions and other
            causes of interference beyond ESlabs’s control. ESlabs can give no guarantee that
            the vehicle, or other Services, will work in adverse conditions or in the event
            of force majeure. In particular, the operation of the Equipment and, therefore,
            the provision of the Service in accordance with this Agreement, depend to some extent
            upon the operation of the digital cellular telecommunications technology with which
            the Equipment operates, and this technology is not operative in all parts of India.<br />
            The Subscriber acknowledges that by putting this Equipment does not in any way mitigate
            the duty to obtain adequate insurance for the vehicle.<br />
            ESlabs will accept no liability for damage or loss incurred as a result of the Subscriber’s
            breach of these Terms and Conditions<br />
            Access to the site (including any information or materials therein) is provided
            on an “as is” basis, without warranties of any kind, either express or implied,
            including, but not limited to, warranties of title, non-infringement or implied
            warranties of merchantability or fitness for a particular purpose. No advice or
            information given by ESlabs, its affiliates or their respective employees shall
            create any warranty. Neither ESlabs nor its affiliates warrant that the information
            or materials on, or access to, any site will be without interruption or error free.
            Eslabs reserves the right to modify any or the entire site without notice.<br />
            You assume all responsibility and risk for the use of this site and the internet
            generally. In no event shall ESlabs or its affiliates be liable for any direct,
            special, indirect, or consequential damages or any damages whatsoever; including
            but not limited to, loss of use, data, or profits, without regard to the form of
            any action, including but not limited to, contract, negligence, or other tortuous
            actions, arising out of or in connection with the use, copying, or display of the
            content resulting from access to or use of this site, or the internet generally,
            under contract, tort or any other cause of action or legal theory.<br />
            Although ESlabs believes the content to be accurate, complete, and current, eslabs
            makes no warranty as to the accuracy, completeness or currency of the content. It
            is your responsibility to verify any information before relying on it. The content
            of this site may include technical inaccuracies or typographical errors.<br />
            <br />
            <span style="color: Black; font-size: large">Limitation of Liability</span><br />
            ESlabs shall not be liable to you or any other person for any general, direct, special,
            incidental, lost profits, exemplary, punitive or consequential damages in connection
            with the services, including, without limitation, loss of profit or revenues, loss
            of use, loss of data, incorrect or corrupted data, cost of capital, cost of substitute
            goods, facilities, services or replacement power, downtime cost, or claims of you
            for such damages, even if company knew of or should have known of the possibility
            of such damages. Without limiting the foregoing or any other limitation of liability
            herein, regardless of the form of action, whether for breach of contract, warranty,
            negligence, strict liability in tort or otherwise, your exclusive remedy and the
            total liability of company or any supplier of services to company arising in any
            way in connection with this agreement, for any cause whatsoever, including, but
            not limited to, any failure or disruption of the services, shall be limited to payment
            by company of damages in an amount equal to the amount of Rs 5000/- per equipment
            provided under this agreement. Company shall have no liability whatsoever to you
            for any claims of patent, copyright, or other intellectual property right infringement
            or misappropriation of trade secrets, made against you incident to the purchase
            or use of products or services.<br />
            <br />
            <span style="color: Black; font-size: large">Company Not an Insurer</span><br />
            ESlabs is not an insurer and you must obtain from an insurer any insurance you desire.
            The amount you pay Company is based upon the Services Company performs and the limited
            liability Company assumes under this Agreement and is unrelated to the value of
            your property, any vehicle in which a Product is installed or any property located
            in any vehicle in which a Product is installed. In the event of any loss or injury
            to any person or property, you agree to look exclusively to your insurer to recover
            damages. You waive all subrogation and other rights of recovery against Company
            that any insurer or other person may have as a result of paying any claim for loss
            or injury to any other person.<br />
            <br />
            <span style="color: Black; font-size: large">Other Party's Limitation</span><br />
            If you purchased Services or the Product through a Reseller, another business or
            another person, or from Company through a referral from a Reseller, another business
            or another person, you agree that the Reseller, business or person acts solely as
            an independent contractor. Such Reseller, business or person shall have no responsibility
            or liability to you for the performance or nonperformance of the Services Company
            provides under this Agreement. Without limiting the above, you agree that the liability
            of such Reseller, business or person is, in any event, limited in accordance with
            the provisions of this Agreement. You agree that such Reseller, business or person
            and its agents, employees, subsidiaries, affiliates and parent companies may invoke
            all of Company’s rights under this Section.<br />
            <br />
            <span style="color: Black; font-size: large">Disclaimer & Limitation Of Liability Related
                To GPS And Cellular Service</span><br />
            The Product receives signals from the Global Positioning Satellite ("GPS") system
            and transmits signals to, and receives signals from, a third party Telecom Service
            Provider. Your Services are provided by an independent Telecom Service Provider
            which you select. You understand that the Product installed in the Monitored Vehicle
            uses cellular telephone technology as the transmission mode for sending signals
            to the Telecom Service Provider. Services are available to you only within the Telecom
            Service Provider signal range only when the Product is within the operating range
            of the Wireless Carrier (as defined below). Services may be temporarily refused,
            interrupted, curtailed, limited or discontinued, without liability to Company or
            the Wireless Carrier, due to many conditions, including: (a) wireless transmission
            capacity limitations and cellular telephone network capacity limitations; (b) atmospheric,
            terrain and geographic conditions; (c) other natural or artificial environment conditions
            beyond Company’s control; (d) limitations of the electrical system design and architecture
            of your Product; (e) the condition of the Product (for example, the Product will
            not function if its power supply is not available as when, for example, the unit
            is not connected to a live power source, or if essential Product components are
            damaged (accidentally or otherwise)); (f) government regulations or limitations;
            (g) restrictions by the Wireless Carrier (for example, wireless carrier equipment
            limitations and inter-carrier roaming agreements); (h) usage concentrations, modifications,
            upgrades, relocation and repairs of transmission facilities for the cellular telephone
            network; (i) Company’s efforts to combat fraudulent use; and (j) other legitimate
            business and operational reasons. Global positioning capabilities used for some
            location-based services are not available if satellite signals are obstructed; you
            must be outside with a clear line of sight between you and the satellites. You understand
            that the Product’s usage of the GPS system and the cellular telephone network are
            fundamental to Company’s ability to provide Services. You understand that due to
            the very nature of cellular telephone, network and GPS technologies, there will
            be times when (1) the Product is unable to secure, maintain or transmit signals,
            or (2) the information transmitted is not reliable, and thus, Company will be unable
            to receive such signals. You also understand that Company does not receive signals
            when the transmission mode is or becomes non-operational and that signals from the
            Product cannot be received by Company when the Product is damaged, does not have
            an adequate power source or is otherwise non-operational. Accordingly, you agree
            that Company shall not, in any way, be liable for, or have responsibility with respect
            to, the GPS system, the cellular telephone network, any of the information obtained
            there from, or for interruptions in service for any reason whatsoever. You further
            acknowledge and agree that Company shall not have any liability for the interruption
            of services due to electrical storms, power failures, interruption or unavailability
            of telephone service, cellular and radio frequency or other conditions beyond Company’s
            control, including, without limit, due to strikes, riots, floods, fires or acts
            of God. You acknowledge that the use of radio frequencies and cellular devices,
            including those provided by Company to you as Services under this Agreement, if
            they are controlled and limited by the any Government Commission and other governmental
            authorities which, from time to time have jurisdiction, and that changes in rules,
            regulations and policies may necessitate discontinuing such transmission devices
            by Company or the Wireless Carrier at Company’s or the Wireless Carrier’s option.
            In no event shall Company or the Wireless Carrier be liable for any cost, delay,
            failure or disruption of the Wireless Service (as defined below), lost profits,
            or incidental, special, punitive, exemplary or consequential damages.<br />
            <br />
            <span style="color: Black; font-size: large">Disclaimer & Limitation Of Liability Related
                To Use Of Product</span><br />
            In no event shall Company be liable for losses, damages, or claims arising out of
            your use of product in your Monitoring Vehicle & You also understand and agree that
            you have no contractual relationship with any third party Telecom Provider and that
            you are not a third party beneficiary of any agreement between Company and any third
            party Telecom Provider. In addition, you expressly understand and agree that any
            third party Telecom Provider shall have no legal, equitable, or other liability
            of any kind to you, and you hereby waive any and all such claims or demands.<br />
            <br />
            <span style="color: Black; font-size: large">Subscription Period and Termination</span><br />
            This Agreement shall commence upon the Commencement Date and shall continue for
            the Minimum Period detailed on the quote or verbally agreed never less than 12 months
            unless formally written in and thereafter shall automatically be renewed for a further
            minimum period of 12 months on each anniversary of the Commencement Date unless
            terminated by either party giving 90 days notice prior to the next anniversary of
            Commencement Date. Notice of cancellation shall be sent to ESlabs registered office
            (or such other address as ESlabs may nominate from time to time)<br />
            Upon termination of this Agreement the accrued rights and liabilities of the parties
            shall not be affected and payments made by Subscribers prior to termination are
            non-refundable unless agreed in writing by ESlabs prior to termination.<br />
            Either party may, by written notice to the other party, terminate this Agreement
            in the event that: (a) the other party is in material or persistent breach of this
            Agreement; and/or (b) the other party becomes insolvent or bankrupt, enters into
            liquidation, whether voluntary or compulsory, passes a resolution for its winding
            up, has a receiver or administrator appointed over the whole or any part of its
            assets, makes any composition or arrangement with its creditors or takes or suffers
            any similar action in consequence of its debt. (c) ESlabs may terminate this Agreement
            with immediate effect on written notice to the Subscriber in the event that: (1)
            any government or other regulatory approvals for the use of the Equipment are withdrawn,
            suspended or amended at any time; and/or (2) the Subscriber or any Authorised User
            raises false alarms in circumstances where, he or she had (in ESlabs opinion) no
            reasonable grounds, on which to believe that the Vehicle had been stolen or that
            genuine emergency or breakdown conditions exist.<br />
            Termination of this Agreement by the Subscriber will take effect at the end of the
            relevant Subscription period.<br />
            <br />
            <span style="color: Black; font-size: large">Notices</span><br />
            Except as specifically provided in this Agreement, all notices required hereunder
            shall be in writing and shall be given by personal delivery, overnight courier service
            or first class mail postage prepaid, at the parties’ addresses set forth herein
            or at such other address(es) as shall be specified in writing by such party to the
            other party in accordance with the terms and conditions of this Section. All notices
            shall be deemed effective upon personal delivery, or one business day following
            deposit with any overnight courier service, or three business days following deposit
            with the Indian Postal System, first class postage attached, in accordance with
            this Section. Notices for you shall be sent to the address you provide to Company
            upon registration of the Product. Notices for Company shall be sent to the address
            set forth for Company in this Agreement.<br />
            <br />
            <span style="color: Black; font-size: large">Mediation/Arbitration</span><br />
            The parties agree to settle any dispute, disagreement, claim or any other cause
            of action of any kind or nature, including without limit those sounding in contract,
            tort, under any statute of any country, state, local or other jurisdiction, related
            to, under, or otherwise in connection with this Agreement. The parties hereto desire
            to avoid litigation. Accordingly, the aggrieved party will give notice of the dispute
            (the “Dispute Notice”) to the other party and both parties will attempt to settle
            the dispute during the thirty (30) day period following such notice (the “Direct
            Negotiation Period”). If such dispute remains unsettled or if the parties do not
            meet within the Direct Negotiation Period, the parties agree to then submit such
            dispute to mediation. If the parties cannot agree on a mediator, each will select
            a mediator and the two chosen mediators will select a third mediator who shall alone
            hear the dispute. Such mediation will, if possible, be conducted during the sixty
            (60) day period following the expiration of the Direct Negotiation Period. If such
            mediation fails to resolve the dispute, or if a party fails to name a mediator within
            thirty (30) calendar days after the expiration of the Direct Negotiation Period,
            the parties agree such dispute will be submitted to final and binding arbitration
            in accordance with the rules of the Indian Council of Arbitration. Unless otherwise
            directed by the arbitrator, such arbitration must be concluded within ninety (90)
            days of the expiration of the sixty (60) day period previously specified for mediation.
            If the parties cannot agree on a single arbitrator, each will select an arbitrator,
            and the two chosen arbitrators will select a third arbitrator who shall alone decide
            the dispute; provided that if the parties cannot agree on a single arbitrator and
            either party fails to select an arbitrator within thirty (30) days after a party
            requests arbitration, the arbitrator shall be named by the Indian Council of Arbitration
            upon the request of either party. Any mediation or arbitration conducted hereunder
            will be conducted in Bangalore, Karnataka, India. The parties hereto shall equally
            share the costs of mediation (including the mediator’s fees and expenses and costs
            directly related to the conduct of the mediation, but excluding each party’s direct
            costs for transportation, attorneys, etc., for which each will be responsible).
            If any party fails to participate in mediation or arbitration after receipt of notice
            thereof, then each party hereto agrees that the other party shall have the right
            to proceed immediately to arbitration and that such other party shall be entitled
            to select the arbitrator in its sole discretion. Each party further agrees that,
            in such event, such arbitrator shall have the right to decide the dispute as if
            the non-participating party were participating in the arbitration and that such
            decision shall be final and binding upon each party hereto.<br />
            <br />
            <span style="color: Black; font-size: large">Venue</span><br />
            It is the express intent of the parties that any dispute under this Agreement be
            decided in accordance with the mediation and arbitration provisions contained in
            above Section hereof. Notwithstanding the foregoing, in the event a court refuses
            to enforce the provisions contained in above section for any dispute or, in the
            event a court is asked to decide a dispute concerning the provisions contained in
            above section, the parties expressly agree that jurisdiction and venue for any actions
            under or pursuant to this Agreement shall be solely in Bangalore, Karnataka state
            , India<br />
            <br />
            <span style="color: Black; font-size: large">Release of Information</span><br />
            Company may disclose, and you authorize disclosure of, information gathered from
            your use of the Services if disclosure is made pursuant to any court order, subpoena,
            discovery demand, or request therefore by any law enforcement agency.<br />
            <br />
            <span style="color: Black; font-size: large">Prior Agreements</span><br />
            You specifically agree that this Agreement supersedes any prior or contemporaneous
            agreement between you and any person or entity, whether written or oral, with respect
            to any Product or Services, as defined herein. Notwithstanding the foregoing, if
            you are a distributor, sales representative or other person or entity who has entered
            into a Master Marketing Agreement or Master Reseller Agreement with Company to purchase
            Products for the purpose of resale, then the terms of this Agreement will not supersede
            the Master Marketing Agreement or Master Reseller Agreement between you and Company
            to the extent the provisions of the Master Marketing Agreement or Master Reseller
            Agreement conflict with the terms herein, it being the intent of all parties that
            the terms of the Master Marketing Agreement or Master Reseller Agreement shall govern.<br />
            <br />
            <span style="color: Black; font-size: large">Security</span><br />
            You agree that you will comply with any security processes and procedures (such
            as passwords) specified by ESlabs with respect to access to or use of the Site.
            Further, you agree not to access or attempt to access any areas of or through the
            Site which are not intended for general public access, unless you have been provided
            with explicit written authorization to do so by ESlabs. You agree to notify ESlabs
            immediately if you become aware of any security breach.<br />
            <br />
            <span style="color: Black; font-size: large">Failure to Comply</span><br />
            ESlabs has the right to terminate or restrict your access to the Site, unilaterally
            and without notice, in the event you violate any of these Terms of Use. ESlabs also
            reserves any and all remedies at law or equity in connection with violation of these
            Terms of Use.<br />
            <br />
            <span style="color: Black; font-size: large">No Interruption</span><br />
            You agree not to interrupt or attempt to interrupt the operation of the Site in
            any way.<br />
            <br />
            <span style="color: Black; font-size: large">No Soliciting</span><br />
            You agree not to use the Site to advertise or to solicit anyone to buy or sell products
            or services, or to make donations of any kind.<br />
            <br />
            <span style="color: Black; font-size: large">International Users and Choice of Law</span><br />
            The site is controlled, operated and administered by ESlabs or its agents from its
            offices within India. ESlabs makes no representation that materials at this Site
            are appropriate or available for transmission to or from, or use in, locations outside
            India and accessing the Site from territories where the Site’s contents are illegal
            is prohibited. You may not use the Site or export the materials in violation any
            export laws and regulations. If you access this Site from a location outside India,
            you are responsible for compliance with all local laws.<br />
            <br />
            <span style="color: Black; font-size: large">Privacy Policy</span><br />
            This privacy policy (this “Privacy Policy”) discloses the privacy practices of ESlabs
            for the Site. The privacy policy does not cover information ESlabs collects by means
            other than the Site.<br />
            <br />
            <span style="color: Black; font-size: large">Information Collected</span><br />
            ESlabs collects personally identifiable information about you when you voluntarily
            submit such information to ESlabs by, for example, filling out a registration form.
            The type of personal information that may be requested is often limited to your
            name, address, e-mail address and telephone number, but may include other information
            as needed to provide the services you request.<br />
            <br />
            <span style="color: Black; font-size: large">Use of Information</span><br />
            In general, ESlabs will only use the information you provide it for the purpose
            for which such information was provided. While ESlabs may also use this information
            to deliver to you information about ESlabs and promotional material, ESlabs will
            take commercially reasonable steps to safeguard the information from unauthorized
            access. ESlabs will not make customer-specific information that is gathered on the
            Site available to unaffiliated organizations for commercial purposes unrelated to
            the business of ESlabs, except with the customer’s permission, as required by law,
            for safety reasons, or to survey customer satisfaction, under nondisclosure protection.
            ESlabs does not sell customer information from the Site to any outside party.<br />
            <br />
            <span style="color: Black; font-size: large">Use of Generic Data</span><br />
            When you visit the Site, ESlabs logs general data, including your domain , ESlabs
            uses this information to continually monitor and improve the Site.<br />
            <br />
            <span style="color: Black; font-size: large">Security</span><br />
            The Site has industry-standard encryption and security measures in place to protect
            the loss, misuse and alteration of the information under ESlabs’s control. While
            ESlabs will use all reasonable efforts to safeguard the confidentiality of your
            personal information, transmissions made by means of the Internet cannot be made
            absolutely secure. ESlabs will have no liability for disclosure of your personal
            information due to errors in transmission or unauthorized acts of third parties.<br />
            <br />
            <span style="color: Black; font-size: large">Indemnification</span><br />
            You agree to indemnify, defend and hold Resellers, Company, the Wireless Carrier,
            and the officers, directors, employees, agents, contractors, subsidiaries, affiliates
            or parent companies of each of them (each an “Indemnified Person”) harmless from
            any loss, cost, expense (including attorney’s fees, expert’s fees, and expenses),
            demand, claim, liability, damages or cause of action of any kind or character (collectively
            referred to as “Claim”), including without limitation, for any personal injury or
            death, in any manner arising out of or relating to your, or your officers, directors,
            employees, agents, assigns, invitees or other users using your Product, whether
            authorized or not (i) violating or otherwise breaching any provision of this Agreement;
            (ii) acts or omissions in the conduct of your business, including, without limitation,
            the marketing and sale of the Products and Services; (iii) statements, representations,
            warranties or other conduct in connection with any transaction involving the Products
            or Services, other than as expressly provided to you by Company or otherwise expressly
            authorized by Company in writing; and (iv) negligence, recklessness or intentional
            misconduct. You further agree to indemnify, defend and hold each Indemnified Person
            harmless from any Claim, including without limitation, for any personal injury or
            death, in any manner arising out of or relating to (i) the provision, failure, or
            use of the Products or the Services, including, without limitation, the compliance
            with any and all laws (whether statutory, under common law or otherwise), rules
            or regulations applicable to the use of the Products or Services; (ii) the inability
            to use the Services or the Product; (iii) the use, failure to use, or inability
            to use the Number; (iv) the installation of the Product in the Monitored Vehicle;
            and (v) Company’s refusal to provide Services because you or any other Service user
            has (A) not paid due to Company for Products or Services or (B) violated any provision
            of this Agreement. These obligations will apply even if such lawsuit or other claim
            arises out of an Indemnified Person’s negligence, gross negligence, failure to perform
            duties under this Agreement, strict liability, failure to comply with any applicable
            law, or other fault. This provision shall survive the termination of this Agreement.<br />
            <br />
            <span style="color: Black; font-size: large">Use of Cookies</span><br />
            When you view this Site, we may store information on your computer. This information
            will be in the form of a “cookie” or similar file. Cookies are small pieces of information
            stored on your hard drive. Cookies do not spy on you or otherwise invade your privacy,
            and they cannot invade your hard drive and steal information. Rather, cookies help
            you navigate a web site as easily as possible. ESlabs may use cookies to deliver
            content specific to your interests and to prevent you from having to reenter your
            registration data each time you visit this Site.<br />
            <br />
            <span style="color: Black; font-size: large">Other Web Sites</span><br />
            This Site may contain links to other websites. ESlabs is not responsible for the
            content or privacy policies of other websites. This Privacy Policy only applies
            to information collected on the Site by ESlabs.<br />
            <br />
            <span style="color: Black; font-size: large">Correction of Information</span> To
            correct any inaccuracies, please contact ESlabs via e-mail at sales@eslabs.co.in<br />
            <br />
            <span style="color: Black; font-size: large">Changes to this Privacy Policy</span><br />
            ESlabs reserves the right, at its sole discretion, to change, modify, add or remove
            any portion of this Privacy Policy, in whole or in part, at any time. The current
            Privacy Policy will be posted on the Site. BY USING THIS WEBSITE, YOU AGREE TO THIS
            CHANGE PROCEDURE.<br />
            <br />
            <span style="color: Black; font-size: large">Miscellaneous</span><br />
            The terms and conditions hereof shall be governed by and construed in accordance
            with the Indian laws without resort to its law of conflicts. The invalidity, in
            whole or in part, of any term or condition hereof shall not affect the validity
            of the remainder hereof. The failure of either Company or you to enforce at any
            time any of the terms and conditions hereof shall not constitute or be construed
            to be a waiver of such terms and conditions or of the right of such party thereafter
            to enforce any such terms and conditions. You are solely responsible for complying
            with any orders, rules or regulations of the Telecom Policy, or any other federal,
            state or local government authority, applicable to the purchase, installation, and
            operation of Product. Except as expressly provided herein, the terms and conditions
            hereof are for the benefit of Company and you and no other party. Company has made
            no representation, warranty, or covenant not contained in this Agreement. Further,
            no amendment, modification, or waiver of, or supplement to, this Agreement shall
            be effective, unless it is in writing. The agreements made herein may not be modified,
            supplemented or changed in whole or in part by any waiver (other than a written
            waiver signed by the party to be charged), oral representation, or course of dealing.
            The terms and conditions of this Agreement shall govern notwithstanding any inconsistent
            or additional terms and conditions of any other document submitted by you.<br />
            <br />
            <span style="color: Black; font-size: large">Feedback</span><br />
            ESlabs welcomes your questions, comments, and concerns about privacy. Please send
            us any and all feedback pertaining to this, or any other issue to info@eslabs.co.in<br />
            <br />
            <span style="color: Black; font-size: large">COMPANY</span><br />
            Company, as used throughout this Agreement, means Empowered Security Labs Private
            Limited, (ESlabs), whose offices are located at 1st Floor, No 345/21, 38th Cross,
            28th Main, 9th Block, Jayanagar, Bangalore, Karnataka, India. Pin: 560 069, and
            its successors and/or assigns.<br />
            <br />
            <span style="color: Black; font-size: large">Notice</span><br />
            ESlabs’s vehicle tracking systems are not intended to violate personal privacy rights,
            nor disobey any local & state law. In no way will ESlabs or its subsidiaries, dealers
            or partners be held responsible for the inappropriate use of these products.<br />
            <br />
            IT IS THE SOLE RESPONSIBILITY OF THE BUYER TO SEEK LEGAL COUNSEL REGARDING THE USE
            OF THESE PRODUCTS AND TO INTERPRET ANY LAWS WHICH MIGHT APPLY IN THEIR AREA OF INTENDED
            USE FROM TIME TO TIME.
        </p>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetGeneralAlert.aspx.cs"
    Inherits="Tracking.SetGeneralAlert" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 50px; vertical-align: middle; font-size: 30px; text-align: center;
        color: Blue;">
        <asp:Label ID="lblhead" Text="Set Vehicle General Alerts" runat="server"></asp:Label>
    </div>
    <div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </div>
    <div>
        <div style=" margin:0 Auto; padding:0; width:800px;">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                            AutoPostBack="true" DataTextField="Vehicalnumber" Height="23px" Width="170px"
                            Style="margin-left: 1px" OnDataBound="ddlMapTOVehicle_DataBound" OnSelectedIndexChanged="ddlMapTOVehicle_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="height: 20px;">
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        GPRS Expiry Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtGPRSActDt" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ControlToValidate="txtGPRSActDt"
                            runat="server" ErrorMessage="Enter GPRS Activated Date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtGPRSActDt">
                        </cc1:CalendarExtender>
                        <asp:RegularExpressionValidator ID="regulaexpressionvalidator1" runat="server" ControlToValidate="txtGPRSActDt"
                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Vehicle Insurance Expiry Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtVehInsExpDt" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtVehInsExpDt"
                            runat="server" ErrorMessage="Enter Vehicle Insurance Expiry Date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtVehInsExpDt">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cbins" runat="server" AutoPostBack="true" OnCheckedChanged="cbins_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtVehInsExpDt"
                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        RTO Permit Renewal Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtrtodate" runat="server" Height="16px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="requiredfieldvalidator30" runat="server" ControlToValidate="txtrtodate"
                            ErrorMessage="Enter RTO Date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" TargetControlID="txtrtodate">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cbrtodate" runat="server" AutoPostBack="true" OnCheckedChanged="cbrtodate_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtrtodate"
                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Driving Licence Expiry Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDrivLic" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtDrivLic"
                            runat="server" ErrorMessage="Enter Driving Licence Expiry Date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDrivLic">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cblicdate" runat="server" AutoPostBack="true" OnCheckedChanged="cblicdate_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtDrivLic"
                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        RTO (FC-Renewal Date):
                    </td>
                    <td>
                        <asp:TextBox ID="txtfcdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" ControlToValidate="txtfcdate"
                            runat="server" ErrorMessage="Enter RTO FC DATE">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy" TargetControlID="txtfcdate">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cbfcdate" runat="server" AutoPostBack="true" OnCheckedChanged="cbfcdate_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtfcdate"
                            ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Emission Check Upto:
                    </td>
                    <td>
                        <asp:TextBox ID="txtems" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" ControlToValidate="txtems"
                            runat="server" ErrorMessage="Enter Emission date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd/MM/yyyy" TargetControlID="txtems">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cbems" runat="server" AutoPostBack="true" OnCheckedChanged="cbems_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                            ControlToValidate="txtems" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Road Tax Expiry Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtroadtax" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ControlToValidate="txtroadtax"
                            runat="server" ErrorMessage="Enter Road Tax Expiry Date">*</asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtroadtax">
                        </cc1:CalendarExtender>
                        <asp:CheckBox ID="cbroadtax" runat="server" AutoPostBack="true" OnCheckedChanged="cbroadtax_CheckedChanged" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                            ControlToValidate="txtroadtax" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Contractor Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtconname" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ControlToValidate="txtconname"
                            runat="server" ErrorMessage="Enter Contractor Name">*</asp:RequiredFieldValidator>
                        <asp:CheckBox ID="cbcontd" runat="server" AutoPostBack="true" OnCheckedChanged="cbcontd_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        Contract Period from Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtconfdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtconfdate">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="requiredfieldvalidator3" runat="server" ControlToValidate="txtconfdate"
                            ErrorMessage="Enter Contract Started Date">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 220px;">
                        To Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtcontdate" runat="server" Height="15px" Width="100px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="txtcontdate">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="requiredfieldvalidator22" runat="server" ControlToValidate="txtcontdate"
                            ErrorMessage="Enter Contract End Date">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                            ControlToValidate="txtcontdate" ErrorMessage="Date format in DD/MM/YYYY" ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 100%; height: 20px; text-align: center;">
        </div>
        <div style="width: 100%; height: 20px; text-align: center;">
            <asp:Button ID="Btn_Submit" runat="server" Text="Submit" OnClick="Btn_Submit_Click" />
        </div>
        <div style="width: 100%; height: 20px; text-align: center;">
        </div>
    </div>
</asp:Content>

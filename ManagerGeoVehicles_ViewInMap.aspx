﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagerGeoVehicles_ViewInMap.aspx.cs"
    MasterPageFile="~/Manager_geo.Master" Inherits="Tracking.ManagerGeoVehicles_ViewInMap" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="width: 100%;">
        <div style="text-align: left; color: Black; font-size: x-large; font-style: italic;">
        </div>
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
            ValidationGroup="circleinfo" ShowSummary="False" />
        <div>
            <div>
                <table width="100%">
                    <tr>
                        <td style="width: 104px">
                            <asp:Button ID="btnBack" runat="server" Text="Back" Height="30px" ForeColor="White"
                                Font-Italic="true" Font-Bold="true" BackColor="BlueViolet" CssClass="button"
                                Width="91px" OnClick="btnBack_Click" />
                        </td>
                        <td  align="center" >
                          <div style="text-align: center; color: Blue; font-size: x-large; font-style: italic;">
                            <asp:Label ID="lblhead" runat="server" Text="View Manager Geofenced Vehicle"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <cc1:GMap ID="GMap1" runat="server" enableServerEvents="true" Width="100%" Height="600px" />
            </div>
        </div>
    </div>
</asp:Content>

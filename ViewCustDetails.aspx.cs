﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class ViewCustDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 

                if (Session["UserRole"] != null)
                {
                    if (Session["UserID"] != null)
                    {
                        string id = Session["UserID"].ToString();
                        DataTable View = db.ViewDetails(id, lblID.Text, lblCustType.Text, lblCustName.Text, lblBillAdd1.Text, lblBillAdd2.Text, lblCity.Text, lblState.Text, lblAreaCode.Text, lblConNum.Text, lblMobNum.Text, lblDOR.Text, lblEmail.Text);
                        lblID.Text = View.Rows[0]["ID"].ToString();
                        lblCustType.Text = View.Rows[0]["Cx_Type"].ToString();
                        lblCustName.Text = View.Rows[0]["Cx_Name"].ToString();
                        lblBillAdd1.Text = View.Rows[0]["Cx_BillingAddress1"].ToString();
                        lblBillAdd2.Text = View.Rows[0]["Cx_BillingAddress2"].ToString();
                        lblCity.Text = View.Rows[0]["Cx_District"].ToString();
                        lblState.Text = View.Rows[0]["Cx_State"].ToString();
                        lblAreaCode.Text = View.Rows[0]["Cx_AreaCode"].ToString();
                        lblConNum.Text = View.Rows[0]["Cx_ContactNumber"].ToString();
                        lblMobNum.Text = View.Rows[0]["Cx_MobileNumber"].ToString();
                        lblDOR.Text = View.Rows[0]["Cx_DateOfRegistration"].ToString();
                        lblEmail.Text = View.Rows[0]["Cx_EmailID"].ToString();

                    }
                }
            }
        }

        //protected void imgBtnEdit_Click(object sender, ImageClickEventArgs e)
        //{
        //    Response.Redirect("EditCustDetails.aspx");
        //}

        //protected void imgBtnConfirmReg_Click(object sender, ImageClickEventArgs e)
        //{
        //    imgBtnEdit.Enabled = false;
        //    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Congratulations ! Registration Confirmed.');</script>");
        //}
        protected void imgBtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string User = Session["UserName"].ToString();
            Response.Redirect("Track.aspx?username="+ User);
            //imgBtnClose.Attributes.Add("onclick", "closeWindow();");
            //Response.Write("<script language=javascript> window.close();</script>");
            //Response.End();
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void btnConfirmReg_Click(object sender, EventArgs e)
        {
            btnEditReg.Enabled = false;
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Congratulations ! Registration Confirmed.');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
        }

        protected void btnEditReg_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditCustDetails.aspx");
        }


    }
}

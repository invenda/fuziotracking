﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleAlertReport.aspx.cs"
    Inherits="Tracking.VehicleAlertReport" MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="text-align: center; color: Blue;">
        <h1>
            Vehicle Alerts Report</h1>
        <div style="width: 100%; text-align: center;">
            <asp:Button ID="btnprint" runat="server" Text="Print " OnClick="btnprint_Click" />
        </div>
    </div>
    <div style="width: 100%; height: 100%; text-align: center;">
        <table style="margin-right: auto; margin-left: auto;">
            <tr>
                <td>
                    <asp:GridView ID="gvparent" runat="server" GridLines="None" Width="100%" AutoGenerateColumns="false"
                        RowStyle-ForeColor="Black" OnRowDataBound="gvparent_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="vrtno" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" ItemStyle-Font-Size="16px" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="100%" style="width: 30px;">
                                            <asp:GridView ID="gvchild" runat="server" AutoGenerateColumns="false" Width="855px"
                                                RowStyle-ForeColor="Black" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="White" Font-Size="12px" />
                                                <Columns>
                                                    <asp:BoundField HeaderText="Alert Type" DataField="Alert Type" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="Vehicle Insurance" DataField="Vehicle Insurance" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="RTO Permit" DataField="RTO Permit" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="Driving Licence" DataField="Driving Licence" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="FC Renewal" DataField="FC Renewal" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="Emission Date" DataField="Emission Date" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="Road Tax Date" DataField="Road Tax Date" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="Contract Date" DataField="Contract Date" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                    <asp:BoundField HeaderText="GPRS Activated" DataField="GPRS Activated" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-BackColor="White" ItemStyle-ForeColor="Black" ItemStyle-Font-Size="12px" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 30px;">
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <div style="height:20px;">
        </div>
    </div>
</asp:Content>

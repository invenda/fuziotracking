﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class JustVehView : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable View = db.ViewExactCust(Session["UserID"].ToString());
            lbldsmsno.Text = View.Rows[0]["Cx_MobileNumber"].ToString();
            string RegNUM = Session["RegNo"].ToString();
            DataTable VV = db.ViewVD(RegNUM, lblVehRegNum.Text, lblVehRegName.Text, Session["UserID"].ToString(), lblDriName.Text, lblVehModel.Text, lblDriMobNum.Text, lblVehMake.Text, lblDriAddress.Text, lblVehInsNum.Text, lblDriLicNum.Text, lblTankCap.Text, lblBatVolt.Text, lblExistMetRead.Text, lblKiloPulse.Text, lblMobNum.Text, lblSerialNum.Text, lblSIMProvider.Text, lblDOP.Text, lblGPRSActivatedDT.Text, lblDealerName.Text, lblRechargePeriod.Text, Milage.Text  ,Fueltype.Text,vehcate .Text);
            lblVehRegNum.Text = VV.Rows[0]["Vehicle_number"].ToString();
            lblVehRegName.Text = VV.Rows[0]["Vehicle_Reg_Name"].ToString();
            lblDriName.Text = VV.Rows[0]["Driver_Name"].ToString();
            lblVehModel.Text = VV.Rows[0]["Vehicle_Model"].ToString();
            lblDriMobNum.Text = VV.Rows[0]["Driver_Mobile_Number"].ToString();
            lblVehMake.Text = VV.Rows[0]["Vehicle_Make"].ToString();
            lblDriAddress.Text = VV.Rows[0]["Driver_Address"].ToString();
            lblVehInsNum.Text = VV.Rows[0]["Vehicle_Insurance_Number"].ToString();
            lblDriLicNum.Text = VV.Rows[0]["Driving_Licence_Number"].ToString();
            // lblVehInsExpDt.Text = VV.Rows[0]["Vehicle_Insurance_Expiry_Date"].ToString();
            // lblDriLicExpDt.Text = VV.Rows[0]["Driving_Licence_Expiry_Date"].ToString();
            lblTankCap.Text = VV.Rows[0]["Tank_Capacity"].ToString();
            lblBatVolt.Text = VV.Rows[0]["Battery_Voltage"].ToString();
            lblExistMetRead.Text = VV.Rows[0]["Existing_Meter_Reading"].ToString();
            lblKiloPulse.Text = VV.Rows[0]["Kilometer_Pulse"].ToString();
            lblMobNum.Text = VV.Rows[0]["Mobile_Number"].ToString();
            lblSerialNum.Text = VV.Rows[0]["Serial_Number"].ToString();
            lblSIMProvider.Text = VV.Rows[0]["SIM_Provider"].ToString();
            lblDOP.Text = VV.Rows[0]["Date_of_Purchase"].ToString();
            lblGPRSActivatedDT.Text = VV.Rows[0]["GPRS_Activated_Date"].ToString();
            lblDealerName.Text = VV.Rows[0]["Dealer_Name"].ToString();
            lblRechargePeriod.Text = VV.Rows[0]["Recharge_Period"].ToString();
              Milage .Text  = VV.Rows[0]["milage"].ToString();
              Fueltype .Text  = VV.Rows[0]["Fuel_type"].ToString();
              vehcate.Text = VV.Rows[0]["Vehicle_Type"].ToString();
             

            //if (VV.Rows[0]["Tank_Type"].ToString() == "0")
            //{
            //    lblTankType.Text = "Positive Vehicle Tank";
            //}
            //else if (VV.Rows[0]["Tank_Type"].ToString() == "1")
            //{
            //    lblTankType.Text = "Negative Vehicle Tank";
            //}
            //lblFCali.Text = VV.Rows[0]["Fuel_Calibration"].ToString();
            // lblrtodate.Text = VV.Rows[0]["Rto_Renewal_Date"].ToString();
            lblrtoname.Text = VV.Rows[0]["Rto_Devision_name"].ToString();
            // lblovers.Text = VV.Rows[0]["Overspeed"].ToString();
            //lblconfdate.Text = VV.Rows[0]["Contract_Fromdate"].ToString();
            //lblcontdate.Text = VV.Rows[0]["Contract_Todate"].ToString();
            //lblconname.Text = VV.Rows[0]["Contract_Name"].ToString();
            //lblfcdate.Text = VV.Rows[0]["Rto_fc_date"].ToString();
            //lblemsdate.Text = VV.Rows[0]["Emission_date"].ToString();
            // lblroadtaxdate.Text = VV.Rows[0]["Road_tax_date"].ToString();
            lblsmsmno.Text = VV.Rows[0]["SMS_No"].ToString();
            // lbltemp.Text = VV.Rows[0]["Max_Temp"].ToString();
           // lblmis.Text = VV.Rows[0]["Minusstring"].ToString();
        }

        protected void btnEditVeh_Click(object sender, EventArgs e)
        {
            string RegNUM = Session["RegNo"].ToString();
            Response.Redirect("EditVehDetails.aspx?RegNo=" + RegNUM);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx?username=" + Session["UserName"].ToString());

        }

        //protected void lnkbtnCorrection_Click(object sender, EventArgs e)
        //{
        //    OpenNewWindow("Corrections.aspx");
        //}
        //public void OpenNewWindow(string url)
        //{

        //    ClientScript.RegisterStartupScript(this.GetType(), "newWindow", String.Format("<script>window.open('{0}');</script>", url));

        //}

    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class ESMaster : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
               // Response.Redirect("~/Login.aspx");
                Response.Redirect("~/sessiontimeout.aspx");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
 
 
                        DBClass db = new DBClass();
 
 
                        lblusername.Text = Session["UserName"].ToString();
                        lbldate.Text = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt");
                        DataTable dt = db.Set_Image_Logo(Session["UserID"].ToString());
                        if (dt.Rows[0][0].ToString() != "" && dt.Rows[0][1].ToString() != "")
                        {
                            Image1.ImageUrl = "Handler.ashx?EmpID=" + Session["UserID"].ToString();
                            Image1.Width = Convert.ToInt32(dt.Rows[0][1].ToString());
                        }
                        else
                        {
                            Image1.ImageUrl = "~/Images/ESL1.JPG";
                            Image1.Width = 50;
                        }
                        if (Session["Logotext"].ToString() != "")
                        {
                            lblhead.Text = Session["Logotext"].ToString();
                        }
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex) { Session.Clear(); Response.Redirect("Login.aspx"); }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                }
                else { Session.Clear(); Response.Redirect("Login.aspx"); }
            }
        }

        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (Menu1.SelectedItem.Text == "Home")
            {
                if (Session["UserRole"].ToString() == "2")
                {
                    Session["selectedVehicle"] = null;
                    Session["address"] = null;
                    Session["timerc"] = null;
                    Response.Redirect("Home.aspx?username=" + Session["UserName"].ToString());
                }
                else
                {
                    Session["selectedVehicle"] = null;
                    Session["address"] = null;
                    Session["timerc"] = null;
                    Response.Redirect("ManagerHome.aspx?username=" + Session["UserName"].ToString());
                }
            }
        }
    }
}

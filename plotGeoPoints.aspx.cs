﻿ using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class plotGeoPoints : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt = 0, lng = 0;
        static int p = 0;
        static int sno = 0;
        static string rname = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;

                DataTable rnos = new DataTable();
                rnos.Columns.Add("Route_No", typeof(string));
                DataTable routno = db.get_Geo_RouteNo(Session["userID"].ToString());
                for (int i = 0; i < routno.Rows.Count; i++)
                {
                    DataRow dr = rnos.NewRow();
                    string rno = routno.Rows[i][0].ToString();
                    string sub = rno.Substring(0, 3);
                    if (sub != "ROU")
                    {
                        dr["Route_No"] = rno;
                        rnos.Rows.Add(dr);
                    }

                }

                ddlrno.DataSource = rnos;//db.get_Geo_RouteNo2(Session["userID"].ToString());
                ddlrno.DataBind();
            }

        }

        public void binddatedetails()
        {
            ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlFromHOURS.DataBind();

            ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
            ddlFromMINUTES.DataBind();

            ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
            ddlToHOURS.DataBind();

            ddlToMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
            ddlToMINUTES.DataBind();

        }


        protected void ddlrno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrno.SelectedValue != "--")
            {
                GMap1.enableServerEvents = true;
                DataTable dt = db.get_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                rname = dt.Rows[0][0].ToString();
                lblrtname.Text = rname;
                ddlrttype.SelectedValue = dt.Rows[0][1].ToString();

                if (dt.Rows[0][1].ToString() == "2")
                {
                    pnldate.Visible = true;
                    pnldate1.Visible = true;
                    pnltime.Visible = false;
                    pnltime1.Visible = false;
                    pnlotime.Visible = false;
                    pnlotime1.Visible = false;

                }
                else if (dt.Rows[0][1].ToString() == "3")
                {
                    pnldate.Visible = true;
                    pnldate1.Visible = true;
                    pnltime.Visible = true;
                    pnltime1.Visible = true;
                    pnlotime.Visible = true;
                    pnlotime1.Visible = true;
                    binddatedetails();
                }
                else if (dt.Rows[0][1].ToString() == "4")
                {
                //    DataTable getdate = db.get_Geodate_Route12(ddlrno.SelectedValue.ToString(), Session["userID"].ToString());
                //    if (getdate.Rows.Count > 0 && getdate .Rows [0][0].ToString ()!="")
                //    {
                //        Response.Redirect("plotGeopointsnew.aspx");
                //    }
                //    else
                //    {
                //        db.delete_Geo_Route(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                //        db.delete_Geo_Routenew(ddlrno.SelectedValue.ToString(), Session["UserID"].ToString());
                //       // GMap1.enableServerEvents = false;
                //        p = 0;
                //        ddlrno.SelectedValue = null;
                //        ddlrttype.SelectedValue = null;
                //        ddlrno.Enabled = true ;
                //        ddlrttype.Enabled = true ;
                //        refreshmap();
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please open Plot Gro Route points page...');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                //                            }
                    
                }
                else
                {
                    pnldate.Visible = false;
                    pnldate1.Visible = false;
                    pnltime.Visible = false;
                    pnltime1.Visible = false;
                    pnlotime.Visible = false;
                    pnlotime1.Visible = false;
                }

                ddlrno.Enabled = false;
                ddlrttype.Enabled = false;

                bindpoints(ddlrno.SelectedValue.ToString());

            }
        }
        public void bindpoints(string rno)
        {
            if (rno != "--")
            {
                GMap1.resetMarkers();
                GMap1.resetPolygon();
                DataTable dt = db.get_Geo_Routepoints(rno, Session["UserID"].ToString());
                for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
                {
                    drawcircle(Convert.ToDouble(dt.Rows[i][4].ToString()), Convert.ToDouble(dt.Rows[i][5].ToString()), Convert.ToDouble(dt.Rows[i][6].ToString()) / 1609.3, j, rno);
                }
                gvmappoints.DataSource = dt;
                gvmappoints.DataBind();
            }

        }
        public void drawcircle(double lt, double lg, double r, int gp, string vn)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            //this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
            GPolyline line = new GPolyline(extp, "#08088A", 3);
            GMap1.Add(line);
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1);// 16, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            options.title = "Route No " + vn;
            oMarker.options = options;

        }

        protected void ddlrno_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--");
        }


        protected void BtnClear_Click(object sender, EventArgs e)
        {
            refreshmap();
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (lt != 0 && lng != 0)
            {
                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                GeoCode objAddress = new GeoCode();
                objAddress = GMap.geoCodeRequest(new GLatLng(lt, lng), sMapKey);
                StringBuilder sb = new StringBuilder();
                string address = "";
                if (objAddress.valid)
                {
                    sb.Append(objAddress.Placemark.address.ToString());
                    address = sb.ToString();
                }
                int n;
                string date = "";
                string ftime = "";
                string ttime = "";

                if (ddlrttype.SelectedValue.ToString() == "2")
                {
                    date = txtdate.Text;
                    ftime = txtdate.Text + " 12:00 AM";
                    ttime = txtdate.Text + " 11:59 PM";

                }
                else if (ddlrttype.SelectedValue.ToString() == "3")
                {
                    string AMPM, DT;
                    string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
                    string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

                    AMPM = rbtnAMPM.SelectedValue.ToString();
                    DT = rbtnDT.SelectedValue.ToString();

                    date = txtdate.Text;
                    ftime = Convert.ToString(txtdate.Text) + " " + fromtime + " " + AMPM;
                    ttime = Convert.ToString(txtdate.Text) + " " + totime + " " + DT;
                }

                n = db.create_Geo_Route(ddlrno.SelectedValue.ToString(), rname, Session["UserID"].ToString(), lt.ToString(), lng.ToString(), txtradius.Text, address, ddlrttype.SelectedValue.ToString(), date, ftime, ttime);
                if (n == 0)
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Successfully Point Created ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    refreshmap();
                }
                else
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('You Reached Maximum Points ');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                }

            }
            else
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point On Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            }

        }
        public void refreshmap()
        {
            p = 0;
            lt = 0; lng = 0;
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            txtradius.Text = string.Empty;
            ddlFromHOURS.SelectedValue = null;
            ddlFromMINUTES.SelectedValue = null;
            ddlToHOURS.SelectedValue = null;
            ddlToMINUTES.SelectedValue = null;
            rbtnAMPM.SelectedValue = null;
            rbtnDT.SelectedValue = null;
            txtdate.Text = string.Empty;
            bindpoints(ddlrno.SelectedValue.ToString());

        }
        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);
        }
        public void changed(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvrow = btn.NamingContainer as GridViewRow;
            string userid = gvmappoints.DataKeys[gvrow.RowIndex].Value.ToString();
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                db.delete_Geo_Routepoints(userid, Session["UserID"].ToString());
                if (ddlrno.SelectedValue != "--")
                {
                    bindpoints(ddlrno.SelectedValue.ToString());
                }

            }
        }

        protected void gvmappoints_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                sno = 1;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblsno = (Label)e.Row.FindControl("lblsno");
                lblsno.Text = sno.ToString();
                sno++;
            }
        }

        protected void Btn_Clear_Click(object sender, EventArgs e)
        {
            GMap1.enableServerEvents = false;
            p = 0;
            Response.Redirect(Request.RawUrl);
        }
    }
}


﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;

namespace Tracking
{
    public partial class BlackSpotreport : System.Web.UI.Page
    {
        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";

        DBClass db = new DBClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }
                }
            }

        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false;
            date.Enabled = false;
        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            disable();
            string fdate = date.Text + " 12:00 AM";
            string tdate = date.Text + " 11:59 PM";

            DateTime dhdh = Convert.ToDateTime(fdate);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                binddata(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
            }
            else
            {
                binddataold(fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());

            }
        }
        public void binddata(string fdate, string tdate, string vid)
        {
            DataTable dt = db.Select_tkm(vid, fdate, tdate);
            DateTime testf = Convert.ToDateTime(date.Text + " 12:00 AM");
            DateTime testt = Convert.ToDateTime(date.Text + " 11:59 PM");
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng();
            Subgurim.Controles.GLatLng gLatLng2 = new Subgurim.Controles.GLatLng();
            string key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            int f = 0, l = 0, s = 1;
            string floc = "", tloc = "";
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Sno", typeof(string));
            dt1.Columns.Add("ftime", typeof(string));
            dt1.Columns.Add("floc", typeof(string));
            dt1.Columns.Add("ttime", typeof(string));
            dt1.Columns.Add("tloc", typeof(string));
            dt1.Columns.Add("duration", typeof(string));
            DateTime fdate1, tdate1;
            //GeoCode objAddress = new GeoCode();
            if (dt.Rows.Count != 0)
            {
                if (testf != Convert.ToDateTime(dt.Rows[0][3].ToString()) && testf < DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = testf.ToString();
                    firstRow["IGST"] = "1";
                    dt.Rows.InsertAt(firstRow, 0);
                }
                if (testt != Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][3].ToString()) && testt < DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = testt.ToString();
                    firstRow["IGST"] = "1";
                    dt.Rows.Add(firstRow);

                }
                else if (testt != Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][3].ToString()) && testt > DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                    firstRow["IGST"] = "1";
                    dt.Rows.Add(firstRow);

                }

            }
            else if (dt.Rows.Count == 0)
            {
                DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = "0";
                firstRow["LGMI"] = "0";
                firstRow["SPED"] = "0";
                firstRow["GTIM"] = testf.ToString();
                firstRow["IGST"] = "1";
                dt.Rows.Add(firstRow);
                DataRow firstRow1 = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow1["LAMI"] = "0";
                firstRow1["LGMI"] = "0";
                firstRow1["SPED"] = "0";
                firstRow1["GTIM"] = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                firstRow1["IGST"] = "1";
                dt.Rows.Add(firstRow1);
            }
            for (int i = 0, j = 1; i < dt.Rows.Count - 1; i++, j++)
            {

                fdate1 = Convert.ToDateTime(dt.Rows[i][3].ToString());
                tdate1 = Convert.ToDateTime(dt.Rows[j][3].ToString());
                TimeSpan ts = tdate1 - fdate1;
                if (ts.TotalMinutes >= 10)
                {
                    DataRow dr = dt1.NewRow();
                    //string gLatLng1.lat = dt.Rows[i][0].ToString();
                    //string gLatLng1.lng = dt.Rows[i][1].ToString();

                    //string gLatLng2.lat = dt.Rows[j][0].ToString();
                    //string gLatLng2.lng = dt.Rows[j][1].ToString();


                    gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1]));
                    gLatLng2 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                    //GeoCode geocode;
                    //GMap gMap1 = new GMap();
                    if (gLatLng1.lat != 0 && gLatLng1.lng != 0)
                    {
                        /*geocode = GMap.geoCodeRequest(gLatLng1, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        StringBuilder sb = new StringBuilder();
                        gMap1.getGeoCodeRequest(gLatLng1, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }*/

                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, gLatLng1.lat + "," + gLatLng1.lng, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            floc = formatted_address;
                        }

                        //  floc = location;
                    }
                    else
                    {
                        floc = "Location Not Found";
                    }
                    if (gLatLng2.lat != 0 && gLatLng2.lng != 0)
                    {

                        /*geocode = GMap.geoCodeRequest(gLatLng2, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        gMap1.getGeoCodeRequest(gLatLng2, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        StringBuilder sb1 = new StringBuilder();
                        if ((null != geocode) && geocode.valid)
                        {
                            sb1.AppendFormat(geocode.Placemark.address);
                        }*/
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, gLatLng2.lat + "," + gLatLng2.lng, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            tloc = formatted_address;
                        }
                        // tloc = location;
                    }
                    else
                    {
                        tloc = "Location Not Found";
                    }
                    dr["Sno"] = s.ToString();
                    dr["ftime"] = fdate1.ToShortTimeString();
                    dr["floc"] = floc;
                    dr["ttime"] = tdate1.ToShortTimeString();
                    dr["tloc"] = tloc; ;
                    dr["duration"] = ts.Hours + " Hr " + ts.Minutes + " Min ";
                    dt1.Rows.Add(dr);
                    s++; floc = ""; tloc = "";

                }


            }
            if (dt1.Rows.Count != 0)
            {
                Download.Visible = true;
            }
            else if (dt1.Rows.Count == 0)
            {
                lblnorec.Text = "No Record Found";
            }
            gvreport.DataSource = dt1;
            gvreport.DataBind();

        }

        protected void Download_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            DateTime dt = Convert.ToDateTime(date.Text);
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Substring(2);
            str1 = str1.Replace(" ", "");
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VBSR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gvreport);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
            tb.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        public void binddataold(string fdate, string tdate, string vid)
        {
            DataTable dt = db.Select_tkmold(vid, fdate, tdate);
            DateTime testf = Convert.ToDateTime(date.Text + " 12:00 AM");
            DateTime testt = Convert.ToDateTime(date.Text + " 11:59 PM");
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng();
            Subgurim.Controles.GLatLng gLatLng2 = new Subgurim.Controles.GLatLng();
            string key = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            int f = 0, l = 0, s = 1;
            string floc = "", tloc = "";
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Sno", typeof(string));
            dt1.Columns.Add("ftime", typeof(string));
            dt1.Columns.Add("floc", typeof(string));
            dt1.Columns.Add("ttime", typeof(string));
            dt1.Columns.Add("tloc", typeof(string));
            dt1.Columns.Add("duration", typeof(string));
            DateTime fdate1, tdate1;
            GeoCode objAddress = new GeoCode();
            if (dt.Rows.Count != 0)
            {
                if (testf != Convert.ToDateTime(dt.Rows[0][3].ToString()) && testf < DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = testf.ToString();
                    firstRow["IGST"] = "1";
                    dt.Rows.InsertAt(firstRow, 0);
                }
                if (testt != Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][3].ToString()) && testt < DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = testt.ToString();
                    firstRow["IGST"] = "1";
                    dt.Rows.Add(firstRow);

                }
                else if (testt != Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][3].ToString()) && testt > DateTime.Now)
                {
                    DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                    firstRow["LAMI"] = "0";
                    firstRow["LGMI"] = "0";
                    firstRow["SPED"] = "0";
                    firstRow["GTIM"] = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                    firstRow["IGST"] = "1";
                    dt.Rows.Add(firstRow);

                }

            }
            else if (dt.Rows.Count == 0)
            {
                DataRow firstRow = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow["LAMI"] = "0";
                firstRow["LGMI"] = "0";
                firstRow["SPED"] = "0";
                firstRow["GTIM"] = testf.ToString();
                firstRow["IGST"] = "1";
                dt.Rows.Add(firstRow);
                DataRow firstRow1 = dt.NewRow(); //LAMI,LGMI,SPED,GTIM,IGST
                firstRow1["LAMI"] = "0";
                firstRow1["LGMI"] = "0";
                firstRow1["SPED"] = "0";
                firstRow1["GTIM"] = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                firstRow1["IGST"] = "1";
                dt.Rows.Add(firstRow1);
            }
            for (int i = 0, j = 1; i < dt.Rows.Count - 1; i++, j++)
            {

                fdate1 = Convert.ToDateTime(dt.Rows[i][3].ToString());
                tdate1 = Convert.ToDateTime(dt.Rows[j][3].ToString());
                TimeSpan ts = tdate1 - fdate1;
                if (ts.TotalMinutes >= 10)
                {
                    DataRow dr = dt1.NewRow();
                    gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1]));
                    gLatLng2 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                    /*GeoCode geocode;
                    GMap gMap1 = new GMap();*/
                    //  string gLatLng1.lat = dt2.Rows[i][0].ToString();
                    //	string gLatLng1.lng = dt2.Rows[i][1].ToString();

                    //string gLatLng2.lat = dt2.Rows[j][0].ToString();
                    //string gLatLng2.lng = dt2.Rows[j][1].ToString();
                    if (gLatLng1.lat != 0 && gLatLng1.lng != 0)
                    {
                        /*geocode = GMap.geoCodeRequest(gLatLng1, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        StringBuilder sb = new StringBuilder();
                        gMap1.getGeoCodeRequest(gLatLng1, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                        if ((null != geocode) && geocode.valid)
                        {
                            sb.AppendFormat(geocode.Placemark.address);
                        }*/
                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, gLatLng1.lat + "," + gLatLng1.lng, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            floc = formatted_address;
                        }

                        // floc = location;
                    }
                    else
                    {
                        floc = "Location Not Found";
                    }
                    if (gLatLng2.lat != 0 && gLatLng2.lng != 0)
                    {
                        /* geocode = GMap.geoCodeRequest(gLatLng2, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                         gMap1.getGeoCodeRequest(gLatLng2, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                         StringBuilder sb1 = new StringBuilder();
                         if ((null != geocode) && geocode.valid)
                         {
                             sb1.AppendFormat(geocode.Placemark.address);
                         }*/

                        System.Net.WebClient client = new System.Net.WebClient();

                        string urlstring = string.Format(reverseGeocodeFormatString, gLatLng2.lat + "," + gLatLng2.lng, googleMapsAPIKey);
                        string response = client.DownloadString(urlstring);
                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                        if (jsonresponse.results.Count > 0)
                        {
                            string formatted_address = jsonresponse.results[0].formatted_address;
                            tloc = formatted_address;
                        }

                        //tloc = location;
                    }
                    else
                    {
                        tloc = "Location Not Found";
                    }
                    dr["Sno"] = s.ToString();
                    dr["ftime"] = fdate1.ToShortTimeString();
                    dr["floc"] = floc;
                    dr["ttime"] = tdate1.ToShortTimeString();
                    dr["tloc"] = tloc; ;
                    dr["duration"] = ts.Hours + " Hr " + ts.Minutes + " Min ";
                    dt1.Rows.Add(dr);
                    s++; floc = ""; tloc = "";

                }


            }
            if (dt1.Rows.Count != 0)
            {
                Download.Visible = true;
            }
            else if (dt1.Rows.Count == 0)
            {
                lblnorec.Text = "No Record Found";
            }
            gvreport.DataSource = dt1;
            gvreport.DataBind();

        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.ColumnSpan = 3;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.ColumnSpan = 3;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderGridRow.Cells.Add(HeaderCell);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                DateTime hdt = Convert.ToDateTime(date.Text);
                HeaderCell1.Text = "Date  :" + hdt.ToString("dd-MM-yyyy");
                HeaderCell1.ColumnSpan = 2;
                HeaderCell1.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                TableCell HeaderCell3 = new TableCell();
                HeaderCell3.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
                HeaderCell3.ColumnSpan = 1;
                HeaderCell3.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell3);
                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
                HeaderCell4.ColumnSpan = 3;
                HeaderCell4.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow1.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);
            }


        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {

                Label flab = (Label)e.Row.FindControl("lblftotal");
                if (DateTime.Now > Convert.ToDateTime(date.Text + " 11:59 PM"))
                {
                    DataTable dt = db.getblackspot(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 AM", date.Text + " 11:59 PM");
                    int total = 1440 - Convert.ToInt32(dt.Rows[0][0].ToString());
                    int value = total;
                    int hours = value / 60; // 2
                    int minutes = value % 60; // 1
                    flab.Text = hours + " Hr " + minutes + " Min";
                }
                else
                {
                    DateTime dhdh = Convert.ToDateTime(date.Text);
                    int emonth = dhdh.Month;
                    DateTime curretmonth = DateTime.Now;
                    int cmonth = curretmonth.Month;
                    if (emonth == cmonth)
                    {

                        DataTable dt = db.getblackspot(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 AM", DateTime.Now.ToString("MM/dd/yyy hh:mm tt"));
                        TimeSpan ts = DateTime.Now - Convert.ToDateTime(date.Text + " 12:00 AM");
                        int total = Convert.ToInt32(ts.TotalMinutes) - Convert.ToInt32(dt.Rows[0][0].ToString());
                        int value = total;
                        int hours = value / 60; // 2
                        int minutes = value % 60; // 1
                        flab.Text = hours + " Hr " + minutes + " Min";
                    }
                    else
                    {
                        DataTable dt = db.getblackspotold(ddlMapTOVehicle.SelectedValue.ToString(), date.Text + " 12:00 AM", DateTime.Now.ToString("MM/dd/yyy hh:mm tt"));
                        TimeSpan ts = DateTime.Now - Convert.ToDateTime(date.Text + " 12:00 AM");
                        int total = Convert.ToInt32(ts.TotalMinutes) - Convert.ToInt32(dt.Rows[0][0].ToString());
                        int value = total;
                        int hours = value / 60; // 2
                        int minutes = value % 60; // 1
                        flab.Text = hours + " Hr " + minutes + " Min";

                    }
                }

            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;
using System.ComponentModel;
//using System.Drawing;
using System.Collections.Generic;

namespace Tracking
{
    public partial class Pieday : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {               

                if (Session["UserID"] != null)
                {
                    Session["ChartNo"] = 1;

                    if (Session["UserRole"].ToString() == "3")
                    {
                        DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    }


                }
            }
        }
        private DataTable GetData()
        {
 
 
            DBClass db1 = new DBClass();
 
 

            DataTable dt1 = new DataTable();
            //dt1.Columns.Add("no", typeof(string));
            dt1.Columns.Add("igst", typeof(string));
            dt1.Columns.Add("idle", typeof(string));
            dt1.Columns.Add("Stat", typeof(string));
            string fdate12 = date.Text + " 12:00 AM";
            DateTime dhdh = Convert.ToDateTime(date.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                //int no = 0;
                DataTable dt = db1.getRhours();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //no++;               
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.search(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    int igst = 0; int stat = 0; int idel = 0;
                    for (int k = 0; k < dt2.Rows.Count; k++)
                    {

                        if (dt2.Rows[k][4].ToString() == "0")
                        {
                            stat++;

                        }
                        if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            igst++;
                        }
                        if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            idel++;
                        }
                    }
                    //dr["no"] = no.ToString();
                    dr["igst"] = igst.ToString();
                    dr["idle"] = idel.ToString();
                    dr["Stat"] = stat.ToString();
                    dt1.Rows.Add(dr);
                }
            }
            else
            {
                DataTable dt = db1.getRhours();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //no++;               
                    DataRow dr = dt1.NewRow();
                    string fdate = date.Text + " " + dt.Rows[i][0];
                    string tdate = date.Text + " " + dt.Rows[i][1];
                    DataTable dt2 = db1.searchold(Session["ParentUser"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                    int igst = 0; int stat = 0; int idel = 0;
                    for (int k = 0; k < dt2.Rows.Count; k++)
                    {

                        if (dt2.Rows[k][4].ToString() == "0")
                        {
                            stat++;

                        }
                        if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            igst++;
                        }
                        if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                        {
                            idel++;
                        }
                    }
                    //dr["no"] = no.ToString();
                    dr["igst"] = igst.ToString();
                    dr["idle"] = idel.ToString();
                    dr["Stat"] = stat.ToString();
                    dt1.Rows.Add(dr);
                }

            }
            return dt1;
        }
        private DataTable LoadChartData()
        {
            DataTable dt2 = GetData();


            // Chart1.DataBind();
            DataTable dt1 = new DataTable();
            // dt1.Columns.Add("no", typeof(string));
            dt1.Columns.Add("Igston", typeof(string));
            dt1.Columns.Add("Ideal", typeof(string));
            dt1.Columns.Add("stop", typeof(string));

            double yValue = 0;
            double yValue2 = 0;
            double yValue3 = 0;
            DataRow dr = dt1.NewRow();
#pragma warning disable CS0219 // The variable 'no' is assigned but its value is never used
            int no = 0;
#pragma warning restore CS0219 // The variable 'no' is assigned but its value is never used
            for (int i = 0; i < dt2.Rows.Count; i++)
            {

                yValue += Convert.ToInt32(dt2.Rows[i][0].ToString());
                yValue2 += Convert.ToInt32(dt2.Rows[i][1].ToString());
                yValue3 += Convert.ToInt32(dt2.Rows[i][2].ToString());



            }
            yValue = yValue / 60;
            yValue = (double)Math.Round(yValue, 2);
            yValue2 = yValue2 / 60;
            yValue2 = (double)Math.Round(yValue2, 2);
            yValue3 = yValue3 / 60;
            yValue3 = (double)Math.Round(yValue3, 2);

            dr["Igston"] = yValue.ToString();
            dr["Ideal"] = yValue2.ToString();
            dr["stop"] = yValue3.ToString();
            dt1.Rows.Add(dr);

            return dt1;

            //{
            //    Chart1.Series["Moving"].Points.AddY(yValue);
            //    Chart1.Series["Ideal"].Points.AddY(yValue2);
            //    Chart1.Series["Stop"].Points.AddY(yValue3);
            //}
        }
        public DataTable binddata()
        {
            DataTable dt1 = LoadChartData();

            DataTable dt5 = new DataTable("chart");
            dt5.Columns.Add("Igon", typeof(string));
            dt5.Columns.Add("color", typeof(string));
            dt5.Columns.Add("st", typeof(string));

            DataRow dr2 = dt5.NewRow();
            dr2["Igon"] = "igson";
            dr2["color"] = "Green";
            dr2["st"] = dt1.Rows[0][0].ToString();
            dt5.Rows.Add(dr2);

            dr2 = dt5.NewRow();
            dr2["Igon"] = "Ideal";
            dr2["color"] = "Blue";
            dr2["st"] = dt1.Rows[0][1].ToString();
            dt5.Rows.Add(dr2);

            dr2 = dt5.NewRow();
            dr2["Igon"] = "stop";
            dr2["color"] = "Red";
            dr2["st"] = dt1.Rows[0][2].ToString();
            dt5.Rows.Add(dr2);

            return dt5;

        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            bindchart();
            Panel1.Visible = true;
        }
        public void bindchart()
        {
            DataTable bind = binddata();
            createpai(bind);
            binddetatils();
        }
        void createpai(DataTable data)
        {
            foreach (DataRow dr in data.Rows)
            {

                DataPoint dpoint = new DataPoint(0, double.Parse(dr["st"].ToString()));
                dpoint.Color = System.Drawing.Color.FromName(dr["color"].ToString());
                Chart1.Series["Chartseries"].Points.Add(dpoint);

            }
            Chart1.Series["Chartseries"]["PointWidth"] = "0.1";
            Chart1.Series["Chartseries"]["DrawingStyle"] = "Cylinder";
            Chart1.Series["Chartseries"]["PaiLableStyle"] = "Inside";

            //Chart1.ChartAreas["ChartArea1"].InnerPlotPosition.Width = 100;
            //Chart1.ChartAreas["ChartArea1"].InnerPlotPosition.Height = 90;
            //Chart1.ChartAreas["ChartArea1"].InnerPlotPosition.X = 20;
            //Chart1.ChartAreas["ChartArea1"].InnerPlotPosition.Y = 25;
        }


        public void binddetatils()
        {

            //lbltemp.Text = "";
            ddlMapTOVehicle.Enabled = false;
            date.Enabled = false;
            lblVehrtono.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
            lblreqtime.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
            DataTable DriDet = db.GerDriverDetails(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
            DateTime hdt = Convert.ToDateTime(date.Text);
            lbltime.Text = "Report Date  :" + hdt.ToString("dd-MM-yyyy");
            //  lbldriname.Text = "Driver Name :" + DriDet.Rows[0][0].ToString();
            // lbldrimob.Text = "Owner/Transporter :" + DriDet.Rows[0][1].ToString();
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.SelectedValue = null;
            date.Text = string.Empty;
            Chart1.Series.Clear();

        }

        protected void Download_Click(object sender, EventArgs e)
        {
            DateTime dt = Convert.ToDateTime(date.Text);
            string str = dt.ToString("yyyy-MM-dd");
            string str1 = str.Replace("-", "");
            str1 = str1.Replace(" ", "");
            str1 = str1.Substring(2);
            string vstr = ddlMapTOVehicle.SelectedValue.ToString();
            string str2 = "VTC_" + vstr.Replace(" ", "") + "_" + str1 + ".pdf";
            // bindchartdata();
            bindchart();
            MemoryStream m = new MemoryStream();
            Document document = new Document(PageSize.A3.Rotate(), 60, 0, 30, 10);
            try
            {
                System.IO.StringWriter sw = new System.IO.StringWriter();
                XhtmlTextWriter hw = new XhtmlTextWriter(sw);
                //tblToExport.RenderControl(hw);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                PdfWriter.GetInstance(document, m);
                document.Open();
                PdfPTable table = new PdfPTable(3);
                PdfPCell cell = new PdfPCell(new Phrase(lblhead.Text, new Font(Font.TIMES_ROMAN, 24f, Font.BOLD, Color.BLUE)));
                cell.Colspan = 3;
                cell.HorizontalAlignment = 1;
                cell.BorderWidth = 0;
                PdfPCell cell0 = new PdfPCell(new Phrase("  "));
                cell0.Colspan = 3;
                cell0.BorderWidth = 0;
                PdfPCell cell1 = new PdfPCell(new Phrase(lblVehrtono.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell1.BorderWidth = 0;
                PdfPCell cell2 = new PdfPCell(new Phrase(lblreqtime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell2.BorderWidth = 0;
                PdfPCell cell3 = new PdfPCell(new Phrase(Label2.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell3.BorderWidth = 0;
                PdfPCell cell4 = new PdfPCell(new Phrase(lbltime.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell4.BorderWidth = 0;
                PdfPCell cell5 = new PdfPCell(new Phrase(Label6.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell5.BorderWidth = 0;
                PdfPCell cell6 = new PdfPCell(new Phrase(Label8.Text, new Font(Font.TIMES_ROMAN, 12f, Font.NORMAL, Color.BLACK)));
                cell6.BorderWidth = 0;
                table.AddCell(cell);
                table.AddCell(cell0);
                table.AddCell(cell1);
                table.AddCell(cell2);
                table.AddCell(cell3);
                table.AddCell(cell4);
                table.AddCell(cell5);
                table.AddCell(cell6);
                document.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    Chart1.SaveImage(stream, ChartImageFormat.Png);
                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                    chartImage.ScalePercent(75f);
                    document.Add(chartImage);
                }

                document.Add(new Paragraph(sw.ToString()));
            }
            catch (DocumentException ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
                Console.Error.WriteLine(ex.Message);
            }
            document.Close();
            Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
        }

    }
}

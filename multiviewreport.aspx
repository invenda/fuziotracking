﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="multiviewreport.aspx.cs"
    Inherits="Tracking.multiviewreport" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <div style="width: 1312px; height: 42px; font-size: 25px; text-align: center; color: Blue;">
            Multiple Vehicle Locations Details
        </div>
        <div style="text-align: center; height: 700px;">
            <asp:GridView ID="gvdetails" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                HeaderStyle-ForeColor="Black" BorderWidth="2" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="16px"
                HeaderStyle-BackColor="#61A6F8" Width="1000px" HeaderStyle-Height="25px" RowStyle-ForeColor="Black">
                <Columns>
                    <asp:BoundField HeaderText="Vehical RTO No." DataField="VehicalNumber" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Height="20px" ItemStyle-BorderWidth="2" />
                    <asp:BoundField HeaderText="Location" DataField="Location" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Height="20px" ItemStyle-BorderWidth="2" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:Button ID="btnExit" runat="server" Text="Exit" Width="100px" OnClick="btnExit_Click" />
        </div>
    </div>
</asp:Content>

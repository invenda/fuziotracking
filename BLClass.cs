﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Xml;

namespace Tracking
{
    public class BLClass
    {
        public  double lagrange(double x, double[] xd, double[] yd)
        {
            if (xd.Length != yd.Length)
            {
                throw new ArgumentException("Arrays must be of equal length."); //$NON-NLS-1$
            }
            double temp = 1, sum = 0;
            double[] f = new double[10];
#pragma warning disable CS0168 // The variable 'c' is declared but never used
            int i, n = xd.Length, j, k = 0, c;
#pragma warning restore CS0168 // The variable 'c' is declared but never used

            for (i = 0; i < xd.Length; i++)
            {
                temp = 1;
                k = i;
                for (j = 0; j < n; j++)
                {
                    if (k == j)
                    {
                        continue;
                    }
                    else
                    {
                        temp = temp * ((x - xd[j]) / (xd[k] - xd[j]));
                    }
                }
                f[i] = yd[i] * temp;
            }

            for (i = 0; i < n; i++)
            {
                sum = sum + f[i];
            }
            return sum;
        }
        public  double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }
    }
}
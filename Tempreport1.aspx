﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tempreport1.aspx.cs" Inherits="Tracking.Tempreport1"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > yesday) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = yesday;
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600"
        AsyncPostBackErrorMessage="Please Ttry agaian">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="text-align: center; color: Blue; height: 32px;">
            <h2>
                Temperature Monitoring Report
            </h2>
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 1100px; height: auto; margin: 0 auto 0 85px; padding: 0; padding: 0;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="110%">
                        <tr align="left">
                            <td align="right" style="width: 147px">
                                <asp:Label ID="Label1" Text="Select Vehicle :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 194px">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="150px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="right" style="width: 163px">
                                <asp:Label ID="Label4" Text="Select month :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlmonth"
                                    ValidationGroup="Group1" ErrorMessage="Select the Month" Operator="NotEqual"
                                    ValueToCompare="00">*</asp:CompareValidator>
                            </td>
                            <td align="left" style="width: 189px">
                                <asp:DropDownList ID="ddlmonth" runat="server" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <%--<asp:DropDownList ID="ddlmonth" runat="server" Width="150px">
                        </asp:DropDownList>--%>
                            <td style="width: 160px">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                    OnClick="GetRecord_Click" />
                            </td>
                            <td style="width: 126px">
                                <asp:Button ID="Download" Text="Download Record" runat="server" Width="120px" Enabled="False"
                                    CausesValidation="false" OnClick="Download_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: 20px;">
                    </div>
                    <div style="width: 100%; height: 100%; text-align: center; vertical-align: top">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <table cellpadding="0" cellspacing="0" style="width: auto; height: auto; margin-right: auto;
                                border-style: double; margin-left: auto;">
                                <tr>
                                    <td valign="top">
                                        <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="321px" OnRowCreated="gvreport_RowCreated">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                                BorderWidth="1" />
                                            <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="200px" FooterStyle-BorderColor="#61A6F8">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle BorderColor="Black"></FooterStyle>
                                                    <HeaderStyle BorderColor="Black" Width="150px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Duration" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("Temp") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="120px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 50px; border-style: dotted;">
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvreport1" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="321px" OnRowCreated="gvreport1_RowCreated">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                                BorderWidth="0" />
                                            <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="200px" FooterStyle-BorderColor="#61A6F8">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle BorderColor="Black"></FooterStyle>
                                                    <HeaderStyle Width="150px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Duration" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("Temp") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="120px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 50px; border-style: dotted;">
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvreport2" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="350px" OnRowCreated="gvreport2_RowCreated"
                                            OnRowDataBound="gvreport2_RowDataBound">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center" />
                                            <RowStyle VerticalAlign="Bottom" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="200px" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotall" runat="server" ForeColor="Black" Font-Bold="true" Text="Total Duration:"></asp:Label>
                                                    </FooterTemplate>
                                                    <HeaderStyle Width="150px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Duration" HeaderStyle-Width="250px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("Temp") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="250px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div style="height: 20px;">
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class SetDailyAlerts : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
            }
        }

        protected void chkbvs_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbvs.Checked == true)
            {
                txthr.Visible = true;
                txtmin.Visible = true;
                txtFtime.Visible = true;
                txtTtime.Visible = true;
                chkbvs.Text = "Disable";
            }
            else
            {
                txthr.Text = "";
                txtmin.Text = "";
                txtFtime.Text = "";
                txtTtime.Text = "";
                txthr.Visible = false;
                txtmin.Visible = false;
                txtFtime.Visible = false;
                txtTtime.Visible = false;
                chkbvs.Text = "Enable";
            }
        }

        protected void CkbSecAMin_CheckedChanged(object sender, EventArgs e)
        {

            if (CkbSecAMin.Checked == true)
            {
                txtFtime1.Visible = true;
                txtTtime1.Visible = true;
                CkbSecAMin.Text = "Disable";
            }
            else
            {
                txtFtime1.Text = "";
                txtTtime1.Text = "";
                txtFtime1.Visible = false;
                txtTtime1.Visible = false;
                CkbSecAMin.Text = "Enable";
            }
        }

        protected void chkbvm_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbvm.Checked == true)
            {
                txthr2.Visible = true;
                txtmin2.Visible = true;
                chkbvm.Text = "Disable";
            }
            else
            {
                txthr2.Text = "";
                txtmin2.Text = "";
                txthr2.Visible = false;
                txtmin2.Visible = false;
                chkbvm.Text = "Enable";
            }
        }

        protected void cbovers_CheckedChanged(object sender, EventArgs e)
        {
            if (cbovers.Checked == true)
            {
                txtspeed.Visible = true;
                cbovers.Text = "Disable";
            }
            else
            {
                txtspeed.Text = "";
                txtspeed.Visible = false;
                cbovers.Text = "Enable";
            }
        }

        protected void chkbtemp_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbtemp.Checked == true)
            {
                maxtemp.Visible = true;
                mintemp.Visible = true;
                txtdelay.Visible = true;
                chkbtemp.Text = "Disable";
            }
            else
            {
                maxtemp.Text = "";
                mintemp.Text = "";
                txtdelay.Text = "";
                maxtemp.Visible = false;
                mintemp.Visible = false;
                txtdelay.Visible = false;
                chkbtemp.Text = "Enable";
            }
        }

        protected void Btn_Submit_Click(object sender, EventArgs e)
        {
            int vs = 0, vi = 0, vm = 0, hr = 0, min = 0, Tempmin = 0, TempMax = 0, tempd = 0;
            string Vsft = "", vstt = "", vsaft = "", vsatt = "";
            string Message = "";
            int CheckTime = 0;
            if (txthr.Text != "" && txtmin.Text != "")
            {
                hr = Convert.ToInt32(txthr.Text);
                min = Convert.ToInt32(txtmin.Text);
                vs = hr * 60 + min;
            }
            if (txthr2.Text != "" && txtmin2.Text != "")
            {
                hr = Convert.ToInt32(txthr2.Text);
                min = Convert.ToInt32(txtmin2.Text);
                vm = hr * 60 + min;
            }
            if (chkbvs.Checked == true)
            {
                Vsft = txtFtime.Text;
                vstt = txtTtime.Text;
                if (Convert.ToDateTime(Vsft) > Convert.ToDateTime(vstt))
                {
                    CheckTime = 1;
                    Message = "Please Check The Time (From Time) Greater Than (To Time)";
                }
            }
            if (chkbtemp.Checked == true)
            {
                Tempmin = Convert.ToInt32(mintemp.Text);
                TempMax = Convert.ToInt32(maxtemp.Text);
                tempd = Convert.ToInt32(txtdelay.Text);

            }
            if (CkbSecAMin.Checked == true)
            {
                vsaft = txtFtime1.Text;
                vsatt = txtTtime1.Text;

            }
            if (chkbvs.Checked == true && CkbSecAMin.Checked == true)
            {
                DateTime dtf = Convert.ToDateTime(Vsft);
                DateTime dtt = Convert.ToDateTime(vstt);
                DateTime dtaf = Convert.ToDateTime(vsaft);
                DateTime dtat = Convert.ToDateTime(vsatt);
                if (dtf < dtaf && dtaf < dtt)
                {
                    CheckTime = 1;
                    Message = "Please Check The Time Working Hours and Odd Hours Should Not be Same";
                }
                if (dtf < dtat && dtat < dtt)
                {
                    CheckTime = 1;
                    Message = "Please Check The Time Working Hours and Odd Hours Should Not be Same";
                }

            }
            if (CheckTime == 0)
            {
                db.Update_Vehicles_DailyAlerts(ddlMapTOVehicle.SelectedValue.ToString(), txtspeed.Text, TempMax.ToString(), vs.ToString(), vi.ToString(), vm.ToString(), Tempmin.ToString(), tempd.ToString(), Vsft, vstt, vsaft, vsatt, "5");
                db.Update_Veh_DailyAlerts(ddlMapTOVehicle.SelectedValue.ToString(), txtspeed.Text, TempMax.ToString(), vs.ToString(), vi.ToString(), vm.ToString(), Tempmin.ToString(), tempd.ToString(), Vsft, vstt, vsaft, vsatt, "5");
                Message = "Updated successfully";
                refresh();
            }
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
            Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('" + Message + "');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'


        }

        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Vehicle--");
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue != "--Select Vehicle--")
            {
                PnlCheck.Visible = true;
                bindvehicledata(ddlMapTOVehicle.SelectedValue.ToString());
            }
            else
            {
                refresh();
            }
        }
        public void bindvehicledata(string Vno)
        {
#pragma warning disable CS0168 // The variable 'vi1' is declared but never used
            int vs1, vi1, vm1 = 0;
#pragma warning restore CS0168 // The variable 'vi1' is declared but never used
            int hour, min = 0;
            DataTable EditV = db.LoadVeh(Vno);
            txtspeed.Text = EditV.Rows[0]["Overspeed"].ToString();
            maxtemp.Text = EditV.Rows[0]["Max_Temp"].ToString();
            string vs = EditV.Rows[0]["Alert_stationary"].ToString();
            string vi = EditV.Rows[0]["Alert_idling"].ToString();
            string vm = EditV.Rows[0]["Alert_moving"].ToString();
            mintemp.Text = EditV.Rows[0]["Min_Temp"].ToString();
            txtdelay.Text = EditV.Rows[0]["Delay_Temp"].ToString();
            string sft = EditV.Rows[0]["Stationary_Fromtime"].ToString();
            string stt = EditV.Rows[0]["Stationary_Totime"].ToString();
            string saft = EditV.Rows[0]["Vehicle_Security_Ftime"].ToString();
            string satt = EditV.Rows[0]["Vehicle_Security_Ttime"].ToString();
            string saMin = EditV.Rows[0]["Vehicle_Security_Min"].ToString();
            if (txtspeed.Text == "" || txtspeed.Text == "0")
            {
                txtspeed.Visible = false;
                cbovers.Checked = false;
                cbovers.Text = "Enable";
            }
            else
            {
                txtspeed.Visible = true;
                cbovers.Checked = true;
                cbovers.Text = "Disable";
            }

            if ((maxtemp.Text == "" || maxtemp.Text == "0") && (mintemp.Text == "" || mintemp.Text == "0") && (txtdelay.Text == "" || txtdelay.Text == "0"))
            {
                chkbtemp.Checked = false;
                maxtemp.Text = "";
                maxtemp.Visible = false;
                mintemp.Text = "";
                mintemp.Visible = false;
                txtdelay.Text = "";
                txtdelay.Visible = false;
                chkbtemp.Text = "Enable";
            }
            else
            {

                maxtemp.Visible = true;
                mintemp.Visible = true;
                txtdelay.Visible = true;
                chkbtemp.Checked = true;
                chkbtemp.Text = "Disable";
            }
            if (vs != "" && vs != "0")
            {
                vs1 = Convert.ToInt32(vs);
                hour = vs1 / 60;
                min = vs1 % 60;
                txthr.Text = hour.ToString();
                txtmin.Text = min.ToString();
                txthr.Visible = true;
                txtmin.Visible = true;
                chkbvs.Checked = true;
                txtFtime.Visible = true;
                txtTtime.Visible = true;
                txtFtime.Text = sft;
                txtTtime.Text = stt;
                chkbvs.Text = "Disable";
            }
            else
            {
                chkbvs.Checked = false;
                chkbvs.Text = "Enable";
                txthr.Visible = false;
                txtmin.Visible = false;
                txtFtime.Visible = false;
                txtTtime.Visible = false;
            }

            if (saft != "0" && saft != "")
            {

                CkbSecAMin.Checked = true;
                CkbSecAMin.Text = "Disable";
                txtFtime1.Visible = true;
                txtTtime1.Visible = true;
                txtFtime1.Text = saft;
                txtTtime1.Text = satt;
            }
            else
            {

                CkbSecAMin.Checked = false;
                CkbSecAMin.Text = "Enable";
                txtFtime1.Visible = false;
                txtTtime1.Visible = false;
            }

            if (vm != "" && vm != "0")
            {
                vm1 = Convert.ToInt32(vm);
                hour = vm1 / 60;
                min = vm1 % 60;
                txthr2.Text = hour.ToString();
                txtmin2.Text = min.ToString();
                chkbvm.Checked = true;
                chkbvm.Text = "Disable";
                txthr2.Visible = true;
                txtmin2.Visible = true;

            }
            else
            {
                txthr2.Text = "";
                txtmin2.Text = "";
                chkbvm.Checked = false;
                chkbvm.Text = "Enable";
                txthr2.Visible = false;
                txtmin2.Visible = false;

            }
        }
        public void refresh()
        {
            Response.Redirect(Request.RawUrl);


        }
    }
}

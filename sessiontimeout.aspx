﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sessiontimeout.aspx.cs"
    Inherits="Tracking.sessiontimeout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ESL Device Tracking</title>
    <style type="text/css">
        .style1
        {
            width: 607px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%; height: 600px; text-align: center;">
        <%--style="width: auto; height: 600px; text-align: Center;">--%>
        <table style="text-align:justify; height: 600px; width:100%;">
            <%--style="text-align: justify; height: 300px;width: auto;">--%>
            <tr align ="center" >
                <%-- align="center ">--%>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Your Session has Expired Please again"
                        ForeColor="Red" Font-Bold="True" Font-Size="X-Large"></asp:Label>
                    <asp:LinkButton ID="lnk1" runat="server" Font-Bold="true" Font-Size ="X-Large" PostBackUrl="~/Login.aspx" >Login Here</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace Tracking
{
    public partial class report : System.Web.UI.Page
    {
        public int igst1 = 0;
        public int idle1 = 0;
        public int sta1 = 0;
        public double tkmr1 = 0;
        public double Fuelstat = 0;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"] != null)
                {
                    DBClass db = new DBClass();
                    if (Session["UserID"] != null)
                    {
                        //this.Page.Title = "Vehicle Day Summary";
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["ParentUser"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable RegNUM = db.VehRegNUM(Session["ParentUser"].ToString());
                    }
                }
            }
            if (IsPostBack)
            {
                DBClass db = new DBClass();
                string VID = ddlMapTOVehicle.SelectedValue;
                DataTable dt = db.DriverDetails(VID, Session["UserID"].ToString());
                drivername.Text = dt.Rows[0][0].ToString();
                driverno.Text = dt.Rows[0][2].ToString();
                vehiclemodel.Text = dt.Rows[0][3].ToString();
                 DataTable dt2 = db.getRhours();
                string fdate=date.Text+" "+dt2.Rows[0][0].ToString();
                string tdate=date.Text+" "+dt2.Rows[dt2.Rows.Count-1][1].ToString();
                DataTable dt3 = db.search(Session["UserID"].ToString(), fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
                try
                {
                    DataTable dt1 = db.getMaxsped(dt3.Rows[0][5].ToString(), fdate, tdate);
                    string time = dt1.Rows[0][0].ToString();
                    double maxsped = Convert.ToDouble(dt1.Rows[0][1].ToString());
                    maxsped = maxsped * 1.85;
                    maxsped = Math.Floor(maxsped);
                    maxspeed.Text = Convert.ToString(maxsped)+" Km/H";
                    maxtime.Text = time;
                    
                }
                catch (Exception ex) { }
                finally 
                {
                    if (maxspeed.Text == "")
                    {
                        maxspeed.Text = "0";
                    }
                
                }

               
            }

        }
        double getkm(string PULSE_CTR, string date, string vn)
        {
            DBClass1 db1=new DBClass1();
            string vid = ddlMapTOVehicle.SelectedValue.ToString();
            string vno = vn;
            double tkmr=0;
            DataTable vdt = db1.DriverDetails(vid, Session["UserId"].ToString());
            double KM_Pulse =Convert.ToDouble(vdt.Rows[0][4].ToString());
            double PCTR = Convert.ToDouble(PULSE_CTR);
            double KMR = PCTR / KM_Pulse;
            double TOTKMRU = 0;
            if (PCTR != 100000)
            {
                DataTable TOTKMR = db1.getTOTKMR1(vno, date);
                TOTKMRU = Convert.ToDouble(TOTKMR.Rows[0][0].ToString());
                TOTKMRU = TOTKMRU / KM_Pulse;
            }
                KMR = KMR + TOTKMRU;

            DataTable NwEMR = db1.SelectNEMR(Session["UserID"].ToString(), vid);
            if (NwEMR.Rows.Count == 0)
            {
                //double nw = 0;
                DataTable P = db1.getPFE(vid);
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                KMR = KMR + nw;

                tkmr = KMR;
                //return KMR;
                //lblTotKmRun.Text = KMR.ToString();
            }
            else if (NwEMR.Rows.Count != 0)
            {
                DataTable P = db1.getPFE(vid);
                double nw1 = Convert.ToDouble(P.Rows[0][0].ToString());
                double nw = Convert.ToDouble(NwEMR.Rows[0][0].ToString());
                KMR = KMR + nw+nw1;
                tkmr = KMR;
               // return KMR;
                //lblTotKmRun.Text = KMR.ToString();
            }
            return tkmr;
        }


        protected void GetRecord_Click(object sender, EventArgs e)
        {
            //allinminutes.Visible = true;
            string d = Session["ParentUser"].ToString();
             DBClass1 db1 = new DBClass1();
             DataTable dt = db1.getRhours();
             DataTable dt1 = new DataTable();
             dt1.Columns.Add("Fhours", typeof(string));
             dt1.Columns.Add("Thours", typeof(string));
             dt1.Columns.Add("igst", typeof(string));
             dt1.Columns.Add("idle", typeof(string));
             dt1.Columns.Add("Stat", typeof(string));
             dt1.Columns.Add("TKMR", typeof(string));
             dt1.Columns.Add("FuelS", typeof(string));
            for(int i=0;i<dt.Rows.Count;i++)
            {
                if (i == 12)
                {
                    DataRow dr1 = dt1.NewRow();
                    dr1["Fhours"] = "";
                    dr1["Thours"] = "";
                    dr1["igst"] = "";
                    dr1["idle"] = "";
                    dr1["Stat"] = "";
                    dr1["TKMR"] = "";
                    dr1["FuelS"] = "";
                    dt1.Rows.Add(dr1);
                }
                DataRow dr = dt1.NewRow();
                string fdate = date.Text + " " + dt.Rows[i][0];
                string tdate = date.Text + " " + dt.Rows[i][1];
                dr["Fhours"]= date.Text + " " + dt.Rows[i][0];
                dr["Thours"] = date.Text + " " + dt.Rows[i][1];
                           
            //for (int j = 0; j < dt.Rows.Count; j++)
            //{
            DataTable dt2 = db1.search(Session["ParentUser"].ToString(),fdate, tdate, ddlMapTOVehicle.SelectedValue.ToString());
            int igst = 0; int stat = 0; int idel = 0; double fkm = 0; double tkm = 0; double TKMR1 = 0; double fulestat = 0;
            for (int k = 0; k < dt2.Rows.Count; k++)
            {
                if (dt2.Rows[k][4].ToString() == "0")
                {
                    stat++;

                }
                if (dt2.Rows[k][3].ToString() != "0" && dt2.Rows[k][4].ToString() == "1")
                {
                    igst++;
                }
                if (dt2.Rows[k][3].ToString() == "0" && dt2.Rows[k][4].ToString() == "1")
                {
                    idel++;
                }

                fkm = getkm(dt2.Rows[0][6].ToString(), dt2.Rows[0][2].ToString(), dt2.Rows[0][5].ToString());
                tkm = getkm(dt2.Rows[dt2.Rows.Count - 1][6].ToString(), dt2.Rows[dt2.Rows.Count - 1][2].ToString(), dt2.Rows[dt2.Rows.Count - 1][5].ToString());

               TKMR1 = tkm - fkm;
            }
               for (int m = dt2.Rows.Count - 1; m > 0; m--)
               {
                  // int sp=Convert.ToInt32(dt2.Rows[m][3].ToString());
                   if (dt2.Rows[m][3].ToString() =="3" && dt2.Rows[m][4].ToString() == "1")

                   {
                       string vid = ddlMapTOVehicle.SelectedValue.ToString();
                       DataTable dtf = db1.GetVehiclesfuel(vid, Session["UserId"].ToString());

                       double string_val_fuel = Convert.ToDouble(dt2.Rows[m][7].ToString());
                       double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                       int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                       double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());

                       if (Tank_typ == 0)
                       {
                           fulestat = (string_val_fuel) / (Fuel_Cali);
                           fulestat = fulestat * tank_capacity;
                           fulestat = Math.Truncate(fulestat * 100) / 100;
                           //lblFuel.Text = total_fuel.ToString();

                       }
                       else 
                       {
                           fulestat = (string_val_fuel) / (Fuel_Cali);
                           fulestat = fulestat * tank_capacity;
                           fulestat = tank_capacity - fulestat;
                           fulestat = Math.Truncate(fulestat * 100) / 100;
                           //lblFuel.Text = total_fuel.ToString();
                       
                       }

                   }
               
               }
              
            
            dr["igst"] = igst.ToString();
            dr["idle"] = idel.ToString();
            dr["Stat"] = stat.ToString();
            dr["TKMR"] = TKMR1.ToString();
            dr["FuelS"] = fulestat.ToString();
            dt1.Rows.Add(dr);
            
            }
            //}

            gv.DataSource = dt1;
            
            gv.DataBind();

            ddlMapTOVehicle.Enabled = false; 
        } 

       

        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Items.Clear();
            Response.Redirect(Request.RawUrl);
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            Table tb = new Table();
            TableRow tr1 = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Controls.Add(gv);
            tr1.Cells.Add(cell1);
            tb.Rows.Add(tr1);
                
            tb.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           if(e.Row.RowType==DataControlRowType.DataRow)
            {
                try
                {
                    if (e.Row.Cells[1].Text == "&nbsp;")
                    {
                        e.Row.BackColor = System.Drawing.Color.White;
                    }
                    Label lbl = e.Row.FindControl("igst9") as Label;
                    //lbl.Text = igst9.ToString();
                    if (lbl.Text != "&nbsp;")
                    {
                        igst1 += Convert.ToInt32(lbl.Text);//e.Row.Cells[2].Text.ToString());
                    }
                    //Tigst.Text = igst1.ToString();
                    Label lbl1 = e.Row.FindControl("idle9") as Label;
                    if (lbl1.Text != "&nbsp;")
                    {
                        idle1 += Convert.ToInt32(lbl1.Text);
                    }

                    //Tidle.Text = idle1.ToString();
                    Label lbl2 = e.Row.FindControl("Stat9") as Label;
                    if (lbl2.Text != "&nbsp;")
                    {
                        sta1 += Convert.ToInt32(lbl2.Text);
                    }
                    //Tsta.Text = sta1.ToString();
                    Label lbl3 = e.Row.FindControl("TKMR9") as Label;
                    if (lbl3.Text != "&nbsp;")
                    {
                        tkmr1 += Convert.ToDouble(lbl3.Text);
                    }
                    //Tkmr.Text = tkmr1.ToString();
                    Label lbl4 = e.Row.FindControl("FuelS9") as Label;
                    if (lbl4.Text != "&nbsp;")
                    {
                        Fuelstat= Convert.ToDouble(lbl4.Text);
                    }

                    //Tfuel.Text = Fuelstat.ToString();
                    
                }
                catch (Exception ex) { }
            }
           if (e.Row.RowType == DataControlRowType.Footer)
           {
               Label lbl = e.Row.FindControl("ftigst") as Label;
               int value = igst1;
               int hours = value / 60; // 2
               int minutes = value % 60; // 1

               string strHours = hours.ToString();
               string strMinutes = minutes.ToString();
               lbl.Text = strHours + " Hr " + strMinutes + " Min";

               Label lbl1 = e.Row.FindControl("ftidle") as Label;
               int value1 = idle1;
               int hours1 = value1 / 60; // 2
               int minutes1 = value1 % 60; // 1

               string strHours1 = hours1.ToString();
               string strMinutes1 = minutes1.ToString();

               lbl1.Text = strHours1 + " Hr " + strMinutes1 + " Min";

               Label lbl2 = e.Row.FindControl("ftstat") as Label;
               int value2 = sta1;
               int hours2 = value2 / 60; // 2
               int minutes2 = value2 % 60; // 1

               string strHours2 = hours2.ToString();
               string strMinutes2 = minutes2.ToString();
               lbl2.Text = strHours2 + " Hr " + strMinutes2 + " Min";

               Label lbl3 = e.Row.FindControl("ftTKMR") as Label;
               lbl3.Text = tkmr1.ToString() + " KM";
               Label lbl4 = e.Row.FindControl("ftFuelS") as Label;
               lbl4.Text = Fuelstat.ToString()+" Ltr";
           }
        }
    }
}
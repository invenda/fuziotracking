<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fuellog.aspx.cs" Inherits="Tracking.Fuellog"
    MasterPageFile="~/ESLMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout ="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto; background-color: White;">
        <div style="width: 100%; height: 30px; font-size: 23px; text-align: center; color: Blue;">
            <asp:Label ID="lblhead" Text="Fuel Data Log" runat="server" Font-Underline="true"></asp:Label>
        </div>
        <div style="width: 100%; height: 20px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 200px;">
                                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 150px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 100px;">
                                <asp:Label ID="Label2" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="txtdate" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 138px;">
                                <asp:TextBox ID="txtdate" runat="server" Width="175px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="M/d/yyyy" TargetControlID="txtdate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="width: 100px;" align="right">
                            </td>
                            <td style="width: 100px;">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                    OnClick="GetRecord_Click" />
                            </td>
                            <td style="width: 120px;" align="center">
                                <asp:Button ID="Download" Text="Download" runat="server" Width="87px" ValidationGroup="Group1"
                                    OnClick="Download_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 30px">
                </div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div style="width: 100%; height: 100%; text-align: center; vertical-align: middle">
                    <asp:Panel ID="Panel1" runat="server">
                        <table border="1" cellpadding="0" cellspacing="0" style="width: auto; height: auto;
                            margin-right: auto; margin-left: auto;">
                            <tr>
                                <td valign="top">
                                    <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HeaderStyle-VerticalAlign="Top"
                                        RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RowStyle-ForeColor="Black"
                                        ShowFooter="true" Font-Size="8pt" OnRowCreated="gvreport_RowCreated">
                                        <HeaderStyle BackColor="LightBlue" Font-Bold="true" ForeColor="Black" Font-Size="15px" />
                                        <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" BorderWidth="0" />
                                        <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" Font-Size="3px" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Date and Time" HeaderStyle-Width="170px" FooterStyle-BorderColor="#61A6F8"
                                                ItemStyle-Font-Size="2px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgtim" runat="server" Text='<% #Bind("GTIM") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                                <HeaderStyle Width="190px"></HeaderStyle>
                                                <ItemStyle Font-Size="13px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText=" Latitude & Longitude " HeaderStyle-Width="100px"
                                                FooterStyle-BorderColor="#61A6F8" ItemStyle-Font-Size="4px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllatlong" runat="server" Text='<% #Bind("latlong") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                                <HeaderStyle Width="300px"></HeaderStyle>
                                                <ItemStyle Font-Size="13px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fuel in Ltr" HeaderStyle-Width="50px" FooterStyle-BorderColor="#61A6F8"
                                                ItemStyle-Font-Size="10px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFUEL_ADC" runat="server" Text='<% #Bind("FUEL_ADC") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                                <HeaderStyle Width="100px"></HeaderStyle>
                                                <ItemStyle Font-Size="13px"></ItemStyle>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Distance in KM" HeaderStyle-Width="70px" FooterStyle-BorderColor="#61A6F8"
                                        ItemStyle-Font-Size="10px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblkm" runat="server" Text='<% #Bind("km") %>'></asp:Label>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                     <asp:Label ID="ftTKMR" runat="server"></asp:Label>
                                         </FooterTemplate>
                                        <FooterStyle BorderColor="#61A6F8"></FooterStyle>
                                        <HeaderStyle Width="150px"></HeaderStyle>
                                        <ItemStyle Font-Size="13px"></ItemStyle>
                                    </asp:TemplateField>--%>
                                        </Columns>
                                        <EditRowStyle BorderColor="Black" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Download" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

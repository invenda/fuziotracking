﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class multiviewreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt = (DataTable)Session["multitable"];
                if (dt.Rows.Count != 0)
                {
                    gvdetails.DataSource = dt;
                    gvdetails.DataBind();
                }
            
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("MultiTrack.aspx");
        }
    }
}

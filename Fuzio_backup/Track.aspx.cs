﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;
using System.Threading;

namespace Tracking
{
    public partial class Track : System.Web.UI.Page
    {
        DBClass dbt1 = new DBClass();
        BLClass bl12 = new BLClass();
        static int lblno = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["page1"] = "Live Tracking";
            Session["page"] = "";
            GMapUIOptions options = new GMapUIOptions();
            options.maptypes_hybrid = true;
            options.keyboard = false;
            options.maptypes_physical = false;
            options.zoom_scrollwheel = true;
            GMap1.Add(new GMapUI(options));
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.MenuMapTypeControl, new GControlPosition(GControlPosition.position.Top_Right));

            GMap1.Add(control);
            GMap1.Add(control2);

            if (!IsPostBack)
            {
                Timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"].ToString());
                //Timer2.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval1"].ToString());
                DBClass db = new DBClass();
                if (Session["UserID"] != null)
                {
                    //if (Session["UserRole"].ToString() == "3")
                    //{
                    //    // lbtnAdmin.Visible = false;
                    //}
                    //for page redirect to selected vehicle 
                    if (Request.QueryString["vehNo"] != null)
                    {
                        Session["selectedVehicle"] = Request.QueryString["vehNo"].ToString();
                        Session["timerc"] = 0;
                        Timer2.Enabled = true;
                        Vehicle_Status(Session["selectedVehicle"].ToString());
                    }
                    DataTable stat;
                    if (Session["UserRole"].ToString() == "3")
                    {
                        stat = db.Select_GetVehicles(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());

                    }
                    else
                    {
                        stat = db.Select_GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());

                    }
                    
                    
                    //DataTable stat = db.Select_GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                    if (stat.Columns.Count > 0)
                    {
                        stat.Columns.Add("Status", typeof(string));
                        for (int i = 0; i < stat.Rows.Count; i++)
                        {
                            string VN = Convert.ToString(stat.Rows[i][0].ToString());
                            DataTable VS = db.GetVehStat(Session["UserID"].ToString(), VN);
                            if (VS.Rows.Count != 0)
                            {
                                stat.Rows[i]["Status"] = "Status";

                                //DateTime sysDT = DateTime.Now;
                                //DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                                //StringDT = StringDT.AddMinutes(360);
                                //if (sysDT > StringDT)
                                //{
                                //    stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ?";
                                //}
                                //else //if(sysDT <= StringDT)
                                //{
                                //    stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ?";
                                //}

                            }
                            else
                            {
                                stat.Rows[i]["Status"] = "Status";
                                //DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), VN);

                                //DateTime sysDT = DateTime.Now;
                                //DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                                //StringDT = StringDT.AddMinutes(360);
                                //if (sysDT > StringDT)
                                //{
                                //    stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ?";
                                //}
                                //else //if(sysDT <= StringDT)
                                //{
                                //    stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ?";
                                //}

                            }

                        }
                    }

                    stat.AcceptChanges();
                    gdVehicles.DataSource = stat;
                    gdVehicles.DataBind();
                    GMap1.reset();
                    GMap1.resetMarkers();
                    GMap1.resetPolylines();
                    imgst.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    imgidl.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    imgmv.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                    GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.GZoom = 4;
                    LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    Session["Status"] = stat;
                    Session["CheckAlerts"] = null;
                    check_General_Alerts();
                }
            }
        }
        public void check_General_Alerts()
        {
            DBClass db = new DBClass();
            DataTable dt;
            if (Session["UserRole"].ToString() == "3")
            {
                dt = db.Check_Genaral_Alerts(Session["cUser_id"].ToString());
                lblbranch.Visible = true;
                lblbranch.Text = "Branch Nmae:" +""+ Session["Branchname"].ToString();
            }
            else
            {
                dt = db.Check_Genaral_Alerts(Session["UserID"].ToString());            
            //DataTable dt = db.Check_Genaral_Alerts(Session["UserID"].ToString());
                if (dt.Rows.Count > 0)
                {
                    lblmalerts.Visible = true;
                    StringBuilder sba = new StringBuilder();
                    sba.Append("You have ALERT-click The ");
                    sba.Append("<span style=\"color:Red;\">");
                    sba.Append("RED Button ");
                    sba.Append("</span>");
                    sba.Append("Corresponding to The Vehicle No");
                    lblmalerts.Text = sba.ToString();
                }
            }
        }
        public void Geofencestatus(string Vno)
        {
            DBClass db = new DBClass();
            string Gv;
            if (Session["UserRole"].ToString() == "3")
            {
                Gv = db.Geo_Validation(Session["cUser_id"].ToString(), Vno);
            }
            else
            {
                Gv = db.Geo_Validation(Session["UserID"].ToString(), Vno);
            }
            //string Gv = db.Geo_Validation(Session["UserID"].ToString(), Vno);
            if (Gv == "0" || Gv == "")
            {
                lblgeofence.Text = "Not Assigned";
                lblgeotype.Text = "";
                lblgeofence.ForeColor = System.Drawing.Color.Black;
                lbtnview.Visible = false;
            }
            else
            {
                lblgeofence.ForeColor = System.Drawing.Color.Red;
                lblgeofence.Text = "Assigned";
                string[] parts = Gv.Split(' ');
                lblgeotype.Text = "(" + parts[1].ToString() + ")";
                lbtnview.Visible = true;
            }
        }
        protected void vehicleno_Click(object sender, EventArgs e)
        {
            DBClass db = new DBClass();
            if (Session["UserID"] != null)
            {
                LinkButton bt = (LinkButton)sender;
                Session["selectedVehicle"] = bt.Text;
                Session["timerc"] = 0;
                DataTable status;
                if (Session["UserRole"].ToString() == "3")
                {
                    status = db.GetVehiclePresentlocation(bt.Text, Session["cUser_id"].ToString());                 
                }
                else
                {
                    status = db.GetVehiclePresentlocation(bt.Text, Session["UserID"].ToString());
                }
               // DataTable status = db.GetVehiclePresentlocation(bt.Text, Session["UserID"].ToString());
                if (status.Rows.Count > 0)
                {

                    Timer2.Enabled = true;
                    Vehicle_Status(bt.Text);
                }
                else
                {
                    Timer1.Enabled = false;
                    string message = "alert(' " + bt.Text +" is offline please chek connection ')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (Session["UserID"] != null && Session["selectedVehicle"] != null)
            {
                string vno = Session["selectedVehicle"].ToString();
                tkmh(vno);
                DBClass db = new DBClass();
                DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
                DateTime LocDT = Convert.ToDateTime(status.Rows[0][4]);
                DateTime sysDT = DateTime.Now;
                DateTime addDT = LocDT.AddMinutes(4);               
                Select_Vehicle(status, vno);                
                if (status.Rows[0][8].ToString() == "1" && status.Rows[0][2].ToString() != "0" && (sysDT < addDT))
                {
                    //---Update Location-----
                    try
                    {
                        string sMapKey = "";
                        if (Session["APIKey"].ToString() != "")
                        {
                            sMapKey = Session["APIKey"].ToString();
                        }
                        else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                        GeoCode objAddress = new GeoCode();
                        objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                        StringBuilder sb = new StringBuilder();
                        if (objAddress.valid)
                        {
                            sb.Append(objAddress.Placemark.address.ToString());
                            Session["address"] = sb.ToString();
                        }
                        Timer2.Enabled = true;
                        Session["timerc"] = 1;
                    }
                    catch (Exception ex) { }
                    //-------Total KM Run-------
                    tkmh(vno);
                    DataTable tktable = db.gettotalkm(vno);
                    double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                    DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
                    tktkm = Math.Truncate(tktkm * 100) / 100;
                    lbltkm.Text = tktkm.ToString() + "&nbsp;&nbsp;KM  at " + dtkmt.ToShortTimeString();
                    Totalrunkm(vno);

                    //------Over SPeed ---------            
                    DataTable vdt = db.vehicledetailsalerts(vno);
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                        lblovs.Text = "(" + speed1.ToString() + " limit)";
                        speed1 = Convert.ToInt32(speed1 / 1.85);
                        string overspeed = speed1.ToString();
                        DataTable dtdevice = db.getdevicesid(vno);
                        string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                        string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                        string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                        DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                        if (dtoverspeed.Rows.Count != 0)
                        {
                            double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[0][0].ToString());
                            overspeed1 = overspeed1 * 1.85;
                            string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[0][1]).ToShortTimeString();
                            lbloverspeed.Text = overspeed1 + " Km/h at " + ovsdate;
                        }
                        else { lbloverspeed.Text = "No Over Speed"; }
                    }
                    else { lbloverspeed.Text = "No Over Speed"; lblovs.Text = ""; }

                }
                else if (status.Rows[0][8].ToString() == "1" && status.Rows[0][2].ToString() == "0" && (sysDT < addDT))
                {
                    //----Fuel Status------
                    Fuel_Status(vno);
                }
                try
                {
                    lblPlace.Text = Session["address"].ToString();
                }
                catch (Exception ex) { }
                if (Session["timerc"].ToString() == "0")
                {
                    Session["timerc"] = 1;
                    Timer2.Enabled = true;
                }
            }
            Bind_Vehicles();
            if (lblalerts2.Text != "" || lbloffalerts.Text != "")
            {
                Btndalerts.Text = "Daily  Alerts Click For Details";
            }
            else
            {
                Btndalerts.Text = "Daily  Alerts :";
            }

        }
        public void Vehicle_Status(string vno)
        {
            DBClass db = new DBClass();
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());

            Select_Vehicle(status, vno);

            DataTable DriDet = db.GerDriverDetails(vno, Session["UserID"].ToString());
            string dname11 = DriDet.Rows[0][0].ToString();
            if (dname11.Length < 13)
            {
                lblDriverName.Text = dname11;
                //stat.Rows[i]["Dname"] = dname11;
            }
            else
            {
                string str = dname11.Substring(0, 13);
                lblDriverName.Text = str;
                //stat.Rows[i]["Dname"] = str;// DriDet.Rows[0][0].ToString();

            }
            //lblDriverName.Text = DriDet.Rows[0][0].ToString();
            lblOwnerName.Text = DriDet.Rows[0][1].ToString();
            lblDriverMobNo.Text = DriDet.Rows[0][2].ToString();
            lblVehModel.Text = DriDet.Rows[0][3].ToString();
            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            //-----Geofence------
            Geofencestatus(vno);
            //---Fuel Status------
            Fuel_Status(vno);
            //-----Location-----
            try
            {
                GeoCode objAddress = new GeoCode();
                objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                StringBuilder sb = new StringBuilder();
                if (objAddress.valid)
                {
                    sb.Append(objAddress.Placemark.address.ToString());
                    Session["address"] = sb.ToString();
                    lblPlace.Text = Session["address"].ToString();
                }
            }
            catch (Exception ex) { }
            //-------Total KM Run-------
            tkmh(vno);
            DataTable tktable = db.gettotalkm(vno);
            double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
            DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
            tktkm = Math.Truncate(tktkm * 100) / 100;
            lbltkm.Text = tktkm.ToString() + "&nbsp;&nbsp;KM  at " + dtkmt.ToShortTimeString();
            Totalrunkm(vno);

            //------Over SPeed ---------            
            DataTable vdt = db.vehicledetailsalerts(vno);
            if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
            {
                int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                lblovs.Text = "(" + speed1.ToString() + " limit)";
                speed1 = Convert.ToInt32(speed1 / 1.85);
                string overspeed = speed1.ToString();
                DataTable dtdevice = db.getdevicesid(vno);
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                if (dtoverspeed.Rows.Count != 0)
                {
                    double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[0][0].ToString());
                    overspeed1 = overspeed1 * 1.85;
                    string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[0][1]).ToShortTimeString();
                    lbloverspeed.Text = overspeed1 + " Km/h at " + ovsdate;
                }
                else { lbloverspeed.Text = "No Over Speed"; }
            }
            else { lbloverspeed.Text = "No Over Speed"; lblovs.Text = ""; }
            gdVehicles.DataSource = (DataTable)Session["Status"];
            gdVehicles.DataBind();

        }
        public void Select_Vehicle(DataTable status, string vno)
        {
            DBClass db = new DBClass();
            //----LAT&lNG----
            double loc = Convert.ToDouble(status.Rows[0][0]);
            double loca = Convert.ToDouble(status.Rows[0][1]);
            lblLocation.Text = Convert.ToString(loc) + " &nbsp;&nbsp;  " + Convert.ToString(loca);
            //----Check Availability----
            checkavailabe(vno);
            //----Enginr ON/OFF Status----
            if (status.Rows[0][8].ToString() == "0")
                lblEngine.Text = "OFF";
            else
                lblEngine.Text = "ON";
            //----BAT_ADC----
            double batADC = Convert.ToDouble(status.Rows[0][3].ToString());
            batADC = Convert.ToDouble(batADC / 42.50);
            double ten = 10;
            batADC = Math.Truncate(batADC * 100) / 100;
            lblVehBatStat.Text = batADC.ToString();
            if (Convert.ToDouble(status.Rows[0][3]) <= ten)
            {
                lblVehBatStat.Text = "Battery Not Connected";
            }
            else if (status.Rows[0][8].ToString() == "0")
            {
                lblVehBatStat.Text = "Discharging" + "&nbsp;&nbsp;" + batADC.ToString() + "&nbsp;" + " Volts ";
            }
            else if (status.Rows[0][8].ToString() == "1" && status.Rows[0][3].ToString() != "0")
            {
                lblVehBatStat.Text = "Charging" + "&nbsp;&nbsp;" + batADC.ToString() + "&nbsp;" + " Volts ";
            }

            //------LastString Date---------
            DateTime LocDT = Convert.ToDateTime(status.Rows[0][4]);
            lblLocDT.Text = LocDT.ToString("dd-MM-yyyy hh:mm tt");
            lblVehNo.Text = status.Rows[0][12].ToString();
            //----Check No of Satilites----
            DateTime sysDT = DateTime.Now;
            DateTime DT = LocDT;
            DateTime dayindiffence = LocDT.AddHours(24);
            DateTime addDT = LocDT.AddMinutes(360);
            if (sysDT > dayindiffence)
            {
                lblUnitStat.Text = "Call Customer Service No:08026545588";//"  ONLINE &nbsp;  ?";
                lblUnitStat.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            }
            else if (sysDT > addDT)
            {
                lblUnitStat.Text = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";
                lblUnitStat.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            }
            else if (sysDT <= addDT)
            {
                lblUnitStat.Text =  "Online";//"  ONLINE &nbsp;  ?";
                lblUnitStat.Attributes.Add("style", "text-decoration:none;color:white;font-weight:bold");
            }
          
            //if (sysDT > addDT)
            //{
            //    lblUnitStat.Text = "Offline";
            //    lblUnitStat.Text += " - 0 Satellites locked";
            //    lblUnitStat.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            //}
            //else
            //{
            //    lblUnitStat.Text = "Online";
            //    lblUnitStat.Text += " - " + status.Rows[0][7].ToString() + " Satellites locked";
            //    lblUnitStat.Attributes.Add("style", "text-decoration:none;color:white;font-weight:bold");
            //}
            //------- Power ------
            string Power = status.Rows[0][5].ToString();
            if (Power == "1")
            {
                lblPower.Text = "Main Battery";
                lblPower.Attributes.Add("style", "text-decoration:none;color:black;font-weight:bold");
            }
            else if (Power == "0")
            {
                lblPower.Text = "Backup Battery";
                lblPower.Attributes.Add("style", "text-decoration:none;color:red;font-weight:bold");
            }
            //------GPS Antenna------- updated on 21/6/2013
            string condition = status.Rows[0][9].ToString();
            string nos = status.Rows[0][7].ToString();
            //if (condition == "100" || condition.Length == 5)
            if (nos == "0" || condition.Length == 5)
            {
                lblGPSant.Text = "Not Available";
                lblGPSant.Attributes.Add("style", "text-decoration:blink;color:red;font-weight:bold");
            }
            else if (condition == "0" || condition.Length < 5)
            {
                lblGPSant.Text = "Available";
                lblGPSant.Attributes.Add("style", "text-decoration:none;color:white;font-weight:bold");
            }
            //----Tempature----
            if (condition != "0") // && condition != "100")
            {
                Temp(Session["selectedVehicle"].ToString());

                //double temp = 0;
                //double conditon1 = Convert.ToDouble(condition);
                //if (conditon1 >= 10000)
                //{
                //    double cond1 = conditon1 - 10000;
                //    if (cond1 > 1000)
                //    {
                //        double cond2 = cond1 - 1000;
                //        temp = cond2 / 3.3;
                //        temp = (double)Math.Round(temp, 1);
                //        lbltemp.ForeColor = System.Drawing.Color.Red;
                //        lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                //    }
                //    else if (cond1 < 1000)
                //    {
                //        temp = cond1 / 3.3;
                //        temp = (double)Math.Round(temp, 1);
                //        lbltemp.ForeColor = System.Drawing.Color.Black;
                //        lbltemp.Text = temp.ToString() + " °C";
                //    }
                //}
                //else if (conditon1 >= 1000)
                //{
                //    double con1 = conditon1 - 1000;
                //    temp = con1 / 3.3;
                //    temp = (double)Math.Round(temp, 1);
                //    lbltemp.ForeColor = System.Drawing.Color.Red;
                //    lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";

                //}
                //else if (conditon1 < 1000)
                //{
                //    temp = conditon1 / 3.3;
                //    temp = (double)Math.Round(temp, 1);
                //    lbltemp.ForeColor = System.Drawing.Color.Black;
                //    lbltemp.Text = temp.ToString() + " °C";
                //}

            }
            else if (condition == "0")
            {
                lbltemp.Text = "0 °C";
            }
            //-----check Ac on or off---
           // ac(Session["selectedVehicle"].ToString());

            LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            //----Check Engine Status----
            Enginestatus(vno);
            //-----Check Speed------
            double speed = Convert.ToDouble(status.Rows[0][2].ToString());
            if (speed > 3)
            {
                string sped = Convert.ToString(speed * 1.85);
                lblspeed.Text = sped;
                DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);

            }
            else if (speed < 3)
            {
                string sped = "0";
                DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                lblspeed.Text = sped;
            }
        }
        public void Fuel_Status(string Vno)
        {
            DBClass db = new DBClass();
            DataTable fuelim;
            if (Session["UserRole"].ToString() == "3")
            {
                fuelim = db.Getfuelimp(Session["Cusername"].ToString());
            }
            else
            {
                fuelim = db.Getfuelimp(Session["Username"].ToString());
            }
           // DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
            if (fuelim.Rows[0][1].ToString() == "Yes")
            {

                DataTable dt2 = db.prevfuelstaus(Vno);
                if (dt2.Rows.Count != 0)
                {
                    if (Convert.ToInt32(dt2.Rows[0][1].ToString()) == 1)
                    {
                        double fulestat;
                        DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(Vno);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity = Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY = bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity)
                            {

                                lblFuel.Text = newY.ToString();
                            }
                            else if (newY > tankcapacity)
                            {
                                lblFuel.Text = newY.ToString();
                            }
                            else
                            {
                                lblFuel.Text ="Waiting....";
                            }
                            lblFuel.Text = newY.ToString();
                        }
                        else if (dtf.Rows.Count > 0)
                        {
                            // DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
                            {
                                double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                                if (Tank_typ == 0)
                                {
                                    double x = Fuel_Cali - Emptytankvalue;
                                    double y = tank_capacity / x;

                                    double z = string_val_fuel - Emptytankvalue;

                                    double b = z * y;
                                    b = (double)Math.Round(b);
                                    //fulestat = (string_val_fuel) / (Fuel_Cali);
                                    //fulestat = fulestat * tank_capacity;
                                    //fulestat = Math.Truncate(fulestat * 100) / 100;
                                    lblFuel.Text = b.ToString();
                                }
                                else
                                {
                                    double x = Emptytankvalue - Fuel_Cali;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel - Fuel_Cali;
                                    double b = z * y;
                                    double f = tank_capacity - b;
                                    f = (double)Math.Round(f);
                                    //fulestat = (string_val_fuel) / (Fuel_Cali);
                                    //fulestat = fulestat * tank_capacity;
                                    //fulestat = tank_capacity - fulestat;
                                    //fulestat = Math.Truncate(fulestat * 100) / 100;
                                    lblFuel.Text = f.ToString();
                                }
                            }
                            else
                            {
                                lblFuel.Text = "Fuel parameters are not calibrated..";


                            }
                        }
                    }
                    else lblFuel.Text = "Waiting...";
                }
                else lblFuel.Text = "Not Available...";
            }
            else
            {
                lblFuel.Text = " Service Not Availed";
                // string alertstring = " Fuel thing not implimented";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
        }
        protected void gdVehicles_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='LightGreen'");
            }
            if (e.Item.ItemType == ListItemType.Item)
            {
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='Gray'");
            }
            else
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='Gray'");
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton mylink = (LinkButton)e.Item.FindControl("vehicleno");
                DBClass db = new DBClass();
                DataTable dts = db.GetStatus(mylink.Text);
                Label myLabel1 = (Label)e.Item.FindControl("label1");
                Image img = (Image)e.Item.FindControl("Simg");
                if (dts.Rows.Count != 0)
                {

                    if (dts.Rows[0][1].ToString() == "0")
                    {
                        img.ImageUrl = "~/Images/RedPoint.JPG";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
                    {
                        img.ImageUrl = "~/Images/GreenPoint.JPG";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
                    {
                        img.ImageUrl = "~/Images/BluePoint.JPG";
                    }


                    DataTable VS = db.GetVehStat(Session["UserID"].ToString(), mylink.Text);
                    DataTable status = db.Select_GetVehiclePresentlocation(mylink.Text, Session["UserID"].ToString());
                    if (VS.Rows.Count > 0)
                    {
                        int gps = Convert.ToInt32(status.Rows[0][7].ToString());
                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                        DateTime dayindiffence = StringDT.AddHours(72);
                        StringDT = StringDT.AddMinutes(360);
                        if (sysDT > dayindiffence)
                        {
                            myLabel1.ToolTip = "Call Customer Service No:08026545588";//"  ONLINE &nbsp;  ?";

                        }
                        else if (sysDT > StringDT)
                        {
                            myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                        }
                        else if (sysDT <= StringDT)
                        {
                            myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                        }
                        if (gps < 1)
                        {

                            myLabel1.ToolTip = "No GPS is Available";
                        }
                    }
                    else
                    {
                        DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), mylink.Text);
                        DataTable statusoff = db.GetVehiclePresentlocation1(mylink.Text, Session["UserID"].ToString());
                        int gps = Convert.ToInt32(statusoff.Rows[0][7].ToString());
                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                        DateTime dayindiffence = StringDT.AddHours(72);
                        StringDT = StringDT.AddMinutes(360);
                        if (sysDT > dayindiffence)
                        {
                            myLabel1.ToolTip = "Call Customer Service No:08026545588";//"  ONLINE &nbsp;  ?";

                        }
                        else if (sysDT > StringDT)
                        {
                            myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                        }
                        else if (sysDT <= StringDT)
                        {
                            myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                        }
                        if (gps < 1)
                        {

                            myLabel1.ToolTip = "No GPS is Available";
                        }

                    }
                    //if (myLabel1.Text == "Offline")
                    //{
                    //    e.Item.Cells[3].ForeColor = System.Drawing.Color.Red;
                    //    DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
                    //    DateTime pdt = DateTime.Now;
                    //    TimeSpan ts = pdt - sdt;
                    //    if (ts.TotalHours >= 24)
                    //    {
                    //       // e.Item.Cells[3].BackColor = System.Drawing.Color.Black;
                    //        e.Item.Cells[3].BackColor = System.Drawing.Color.Black;
                    //    }
                    //}
                    string str = lblVehNo.Text;
                    if (mylink.Text == str)
                    {
                        e.Item.Cells[0].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[1].BackColor = System.Drawing.Color.Orange;
                        // e.Item.Cells[2].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[3].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[4].BackColor = System.Drawing.Color.Orange;
                        

                    }
                    Button btn1 = (Button)e.Item.FindControl("btnalerts");
                    if (btn1.Text == "0")
                    {
                        btn1.BackColor = System.Drawing.Color.Red;
                        btn1.BorderColor = System.Drawing.Color.Red;
                        btn1.Text = "Alerts";
                    }
                    else
                    {
                        btn1.BackColor = System.Drawing.Color.White;
                        btn1.BorderColor = System.Drawing.Color.White;
                        btn1.Text = "Alerts";
                    }
                    RadioButton rbt = (RadioButton)e.Item.FindControl("rbtn");
                    if (Session["selectedVehicle"] != null)
                    {
                        if (Session["selectedVehicle"].ToString() == mylink.Text)
                        {
                            rbt.Checked = true;
                        }
                        else rbt.Checked = false;
                    }
                }
                else
                {

                    DataTable dtsold = db.GetStatusoffff(mylink.Text);


                    if (dtsold.Rows[0][1].ToString() == "0")
                    {
                        img.ImageUrl = "~/Images/RedPoint.JPG";
                    }
                    else if (dtsold.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsold.Rows[0][0].ToString()) > 3)
                    {
                        img.ImageUrl = "~/Images/GreenPoint.JPG";
                    }
                    else if (dtsold.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsold.Rows[0][0].ToString()) <= 3)
                    {
                        img.ImageUrl = "~/Images/BluePoint.JPG";
                    }
                    if (myLabel1.Text == "Offline")
                    {
                        e.Item.Cells[3].ForeColor = System.Drawing.Color.Red;
                        DateTime sdt = Convert.ToDateTime(dtsold.Rows[0][2].ToString());
                        DateTime pdt = DateTime.Now;
                        TimeSpan ts = pdt - sdt;
                        if (ts.TotalHours >= 24)
                        {
                            e.Item.Cells[3].BackColor = System.Drawing.Color.Black;
                        }
                    }
                    string str = lblVehNo.Text;
                    if (mylink.Text == str)
                    {
                        e.Item.Cells[0].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[1].BackColor = System.Drawing.Color.Orange;
                        // e.Item.Cells[2].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[3].BackColor = System.Drawing.Color.Orange;
                        e.Item.Cells[4].BackColor = System.Drawing.Color.Orange;

                    }
                    Button btn1 = (Button)e.Item.FindControl("btnalerts");
                    if (btn1.Text == "0")
                    {
                        btn1.BackColor = System.Drawing.Color.Red;
                        btn1.BorderColor = System.Drawing.Color.Red;
                        btn1.Text = "Alerts";
                    }
                    else
                    {
                        btn1.BackColor = System.Drawing.Color.White;
                        btn1.BorderColor = System.Drawing.Color.White;
                        btn1.Text = "Alerts";
                    }
                    RadioButton rbt = (RadioButton)e.Item.FindControl("rbtn");
                    if (Session["selectedVehicle"] != null)
                    {
                        if (Session["selectedVehicle"].ToString() == mylink.Text)
                        {
                            rbt.Checked = true;
                        }
                        else rbt.Checked = false;
                    }

                }
            }
        }
        protected string alert2(string Vno, int no)
        {
            lbloffalerts.Text = string.Empty;
            lblalerts2.Text = string.Empty;
            StringBuilder builder = new StringBuilder();
            int maxtemp = 0, min1 = 0, delay = 0;
            DBClass db = new DBClass();
            DateTime today = DateTime.Now;
            DateTime tomarrow = today.AddDays(1);
            DataTable VS = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            if (VS.Rows.Count > 0)
            {
                DateTime tt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (no == 0 && tt < tomarrow)
                {

                    DataTable gadt = db.vehicledetailsalerts(Vno);
                    if (gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][14].ToString() != "")
                    {
                        maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                        min1 = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                        delay = Convert.ToInt32(gadt.Rows[0][14].ToString());
                        int dttemp = 0;
                        if (maxtemp != 0)
                        {
                            if (maxtemp > 0)
                            {
                                dttemp = db.Temp_alert2(Vno, min1.ToString(), maxtemp.ToString());
                            }
                            else if (maxtemp < 0)
                            {
                                maxtemp = maxtemp * -1 + 1000;
                                dttemp = db.Temp_alert3(Vno, min1.ToString(), maxtemp.ToString());
                            }
                            if (delay <= dttemp)
                            {
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Temperature");
                                builder.Append("</span>");
                                builder.Append("> " + gadt.Rows[0][9].ToString() + "°C ");
                            }
                        }
                    }
                    int vsta = 0, vid = 0, vmng = 0, hours, min;
                    if (VS.Rows[0][8].ToString() == "1" && VS.Rows[0][2].ToString() != "0")
                    {
                        if (gadt.Rows[0][12].ToString() != "" && gadt.Rows[0][12].ToString() != "0")
                        {
                            DataTable pmdt = db.GetPreviMNG(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            vmng = Convert.ToInt32(gadt.Rows[0][12].ToString());
                            hours = vmng / 60;
                            min = vmng % 60;
                            DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                            if (pmdt.Rows.Count != 0)
                            {
                                DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                                TimeSpan ts = dtmng - dtmng1;
                                if (vmng < ts.TotalMinutes)
                                {
                                    //alerttext3 = lbla2.Text+ Vno + "- Moving > "+hours.ToString()+":"+min.ToString()+" Hr ";
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Moving");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                            else
                            {
                                DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                                TimeSpan ts = dtmng - dtmng1;
                                if (vmng < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Moving");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                        }
                    }
                    else
                    {
                        string sft = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][15].ToString();
                        string stt = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][16].ToString();
                        if (gadt.Rows[0][10].ToString() != "" && gadt.Rows[0][10].ToString() != "0" && Convert.ToDateTime(sft) < DateTime.Now && Convert.ToDateTime(stt) > DateTime.Now)
                        {
                            DataTable pdt = db.GetPreviIGST11(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            vsta = Convert.ToInt32(gadt.Rows[0][10].ToString());
                            DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                            hours = vsta / 60;
                            min = vsta % 60;
                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                if (dtst1 > Convert.ToDateTime(sft))
                                {
                                    TimeSpan ts = dtst - dtst1;
                                    if (vsta < ts.TotalMinutes)
                                    {
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("||");
                                        builder.Append("</span>");
                                        builder.Append(" " + Vno + "- ");
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("Stationary");
                                        builder.Append("</span>");
                                        builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");

                                    }
                                }
                                else
                                {
                                    DataTable dtchk = db.firstigston(Convert.ToDateTime(sft).ToString(), DateTime.Now.ToString(), VS.Rows[0][11].ToString());
                                    TimeSpan ts = DateTime.Now - Convert.ToDateTime(sft);
                                    if (dtchk.Rows.Count == 0 && vsta < ts.TotalMinutes)
                                    {
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("||");
                                        builder.Append("</span>");
                                        builder.Append(" " + Vno + "- ");
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("Stationary");
                                        builder.Append("</span>");
                                        builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                    }
                                }
                            }
                            else
                            {
                                DataTable pdt1 = db.GetPreviIGST22(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                if (pdt1.Rows.Count != 0)
                                {
                                    DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                                    TimeSpan ts = dtst - dtst1;
                                    if (vsta < ts.TotalMinutes)
                                    {
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("||");
                                        builder.Append("</span>");
                                        builder.Append(" " + Vno + "- ");
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("Stationary");
                                        builder.Append("</span>");
                                        builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                    }
                                }
                            }
                        }
                    }
                    string dnt = DateTime.Now.ToShortDateString();
                    DateTime ftime = Convert.ToDateTime(dnt + " 12:00 AM");
                    DateTime ttime = Convert.ToDateTime(dnt + " 11:59 PM");
                    DataTable Towesdt = db.Towed_Alerts(Vno, "TOWED", ftime.ToString(), ttime.ToString());
                    if (Towesdt.Rows.Count >= 8)
                    {
                        //DateTime dnt1 = Convert.ToDateTime(Towesdt.Rows[0][0].ToString());
                        builder.Append("<span style=\"color:Red;\">");
                        builder.Append("||");
                        builder.Append("</span>");
                        builder.Append(" " + Vno + "- ");
                        builder.Append("<span style=\"color:Red;\">");
                        builder.Append("Towed At");
                        builder.Append("</span>");
                        // builder.Append(": " + dnt1.ToShortTimeString());
                    }
                }
                else if (no == 1)
                {
                    //DateTime spdt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                    //DateTime pdt = DateTime.Now;
                    //TimeSpan ts = pdt - spdt;
                    //if (ts.TotalHours >= 6)
                    //{
                        builder.Append("<span style=\"color:Red;\">");
                        builder.Append("||");
                        builder.Append("</span>");
                        builder.Append(" " + Vno + "- ");
                        builder.Append("<span style=\"color:Red;\">");
                        builder.Append("Offline");
                        builder.Append("</span>");
                        builder.Append(" > 6Hr ");

                   // }
                }
            }
            else if (no == 1)
            {
                //DateTime spdt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                //DateTime pdt = DateTime.Now;
                //TimeSpan ts = pdt - spdt;
                //if (ts.TotalHours >= 6)
                //{
                builder.Append("<span style=\"color:Red;\">");
                builder.Append("||");
                builder.Append("</span>");
                builder.Append(" " + Vno + "- ");
                builder.Append("<span style=\"color:Red;\">");
                builder.Append("Offline");
                builder.Append("</span>");
                builder.Append(" > 6Hr ");

                // }
            }
            return builder.ToString();

        }

        protected void btnalerts_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridItem dgi = (DataGridItem)((Button)sender).Parent.Parent;
                //string key = gdVehicles.DataKeyField[dgi.ItemIndex].ToString();
                string address = ((LinkButton)dgi.FindControl("vehicleno")).Text;
                string[] alertstring = getalertdetailsfunction(address);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
            }
            catch (Exception ex) { }

        }
        public string[] getalertdetailsfunction(string Vno)
        {
            DBClass db = new DBClass();
            DataTable vdt = db.vehicledetailsalerts(Vno);
            string vinsEd = vdt.Rows[0][0].ToString();
            string rtorewd = vdt.Rows[0][1].ToString();
            string dlicexd = vdt.Rows[0][2].ToString();
            string overspeed = vdt.Rows[0][3].ToString();
            string conttodate = vdt.Rows[0][4].ToString();
            string gprsad = vdt.Rows[0][5].ToString();
            string fcdate = vdt.Rows[0][6].ToString();
            string emsdate = vdt.Rows[0][7].ToString();
            string roadtax = vdt.Rows[0][8].ToString();
            //string[] stralert = new string[10];
            List<string> stralert = new List<string>();
            stralert.Add("        VEHICLE RTO No : ");
            stralert.Add("  ");
            DateTime spdate = DateTime.Now;
            spdate = spdate.AddDays(30);
            DateTime spdate1 = DateTime.Now.AddDays(3);
            //DataRow dr = alertdt.NewRow();
            int j = 0;
            var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
            if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Insurance will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Insurance DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Insurance Expired Today");
                    j++;
                }
            }
            if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle RTO Permit will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle RTO Permit Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle RTO Permit Expired Today");
                    j++;
                }
            }
            if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Driver Driving Licence Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Driver Driving Licence Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Driver Driving Licence Expired Today");
                    j++;
                }
            }
            if (overspeed != "0" && overspeed != "")
            {
                DataTable dtdevice = db.getdevicesid(Vno);
                int ovrspeed = Convert.ToInt32(overspeed);
                ovrspeed = Convert.ToInt32(ovrspeed / 1.85);
                string ovs = ovrspeed.ToString();
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, ovs);
                if (dtoverspeed.Rows.Count != 0)
                {
                    stralert.Add("Vehicle Over Speed.! see details below");
                    j++;

                }
            }
            if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Contract Period Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Contract Period Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Contract Period Expired Today");
                    j++;
                }
            }
            if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 3 && s > 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle GPRS Recharge Expired Today");
                    j++;
                }
            }
            if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle FC Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle FC DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle FC Expired Today");
                    j++;
                }
            }
            if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Emmission Test Date Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Emmission Test DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Emmission Test Expired Today");
                    j++;
                }
            }
            if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Road Tax Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Road Tax DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Road Tax Expired Today");
                    j++;
                }
            }
            int len = stralert.Count;
            string[] salerts = new string[len];
            if (j != 0)
            {
                for (int m = 0, n = 1; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else if (m == 1)
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                    else
                    {
                        salerts[m] = n + " : " + stralert[m].ToString();
                        n++;
                    }
                }
            }
            else
            {
                if (lblno == 0)
                {
                    lblmalerts.Visible = false;
                }
                stralert.Add("NO Alert For This Vehicle");
                len = stralert.Count;
                salerts = new string[len];
                for (int m = 0; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                }
            }
            return salerts;
        }
        protected void LinkButton9_Click(object sender, EventArgs e)
        {
            Response.Redirect("MultiTrack.aspx");
        }
        public void tkmh(string vno)
        {
            DBClass db = new DBClass();
            DataTable dt = db.gettotalkm(vno);
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() != Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                    string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                    db.uptotalkm(vno, fdate, tdate, "0", fdate);
                }
                else if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() == Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    double tkm = Convert.ToDouble(dt.Rows[0][4].ToString());
                    string fdate = dt.Rows[0][2].ToString();
                    string fdate1 = dt.Rows[0][5].ToString();
                    string tdate = dt.Rows[0][3].ToString();
                    DataTable dt1 = db.Select_TotalRunkm(vno, fdate, tdate);
                    if (dt1.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                        {
                            tkm += bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                        }
                        fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    }
                    else if (dt1.Rows.Count == 0)
                    {
                        fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

                    }
                    db.uptotalkm(vno, fdate, tdate, tkm.ToString(), fdate);
                }
            }
            else
            {
                string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                db.settotalkm(vno, fdate, tdate, "0");
            }
        }      
        public void Totalrunkm(string vno)
        {
            DBClass db = new DBClass();
            DataTable dt = db.getTotalRunkm(vno);
            if (dt.Rows.Count != 0)
            {
                string fdate = dt.Rows[0][2].ToString();
                double trkm = Convert.ToDouble(dt.Rows[0][3].ToString());
                string tdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                DataTable dt1 = db.Select_TotalRunkm(dt.Rows[0][1].ToString(), fdate, tdate);
                if (dt1.Rows.Count != 0)
                {
                    for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                    {
                        trkm += bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                    }
                    fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    trkm = (double)Math.Round(trkm, 2);

                }
                else
                {
                    fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                }

                db.IU_Totalrunkm(vno, fdate, trkm.ToString());
                DataTable P = db.getPFE(vno);
                DataTable CKMdt = db.Get_Correction_KM(vno);
                double ckm = 0;
                if (CKMdt.Rows[0][0].ToString() != "")
                {
                    ckm = Convert.ToDouble(CKMdt.Rows[0][0].ToString());
                }
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                nw = trkm + nw + ckm;
                lblTotKmRun.Text = nw.ToString() + "&nbsp;&nbsp;KM";
            }
            else
            {
                DataTable P = db.getPFE(vno);
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                string ftime = P.Rows[0][1].ToString();
                DateTime fdate = Convert.ToDateTime(ftime, dtfi);
                db.IU_Totalrunkm(vno, fdate.ToString(), "0");
            }

        }
        public void checkavailabe(string Vno)
        {
            DBClass db = new DBClass();
            DateTime dt1 = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 12:00 AM");
            //DataTable VS = db.GetVehStat(Session["UserID"].ToString(), Vno);
            // DateTime dt2 = Convert.ToDateTime(VS.Rows[0][0].ToString());
            DateTime dt2 = Convert.ToDateTime(DateTime.Now.ToString());
            if (dt2 < dt1)
            {
                dt1 = Convert.ToDateTime(dt2.ToShortDateString() + " 12:00 AM");
            }
            TimeSpan ts = dt2 - dt1;
            DataTable dt = db.getblackspot(Vno, dt1.ToString(), dt2.ToString());
            double sysdata = Convert.ToDouble(dt.Rows[0][0].ToString());
            double av = ((ts.TotalMinutes - sysdata) / ts.TotalMinutes) * 100;
            av = 100 - av;
            av = (double)Math.Round(av, 1);
            if (av < 90 && av > 0)
            {
                av = av + 5;
            }
            else if (av < 95 && av > 0)
            {
                av = av + 2;
            }
            else if (av < 98 && av > 0)
            {
                av = av + 1;
            }
            else if (av >= 100)
            {
                av = 100;
            }
            lblavailable.Text = " Working at " + av.ToString() + " % Uptime";

        }
        protected void LinkButton13_Click(object sender, EventArgs e)
        {
            Response.Redirect("TemperatureGraph.aspx");
        }
        public void Enginestatus(string Vno)
        {
            DBClass db = new DBClass();
            DataTable VS = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            int Hours = 0;
            int Minutes = 0;
            if (VS.Rows[0][8].ToString() == "0")
            {
                DataTable pdt = db.GetPreviIGST121(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pdt.Rows.Count != 0)
                {
                    DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                    TimeSpan ts = dtst - dtst1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    if (m == 1)
                    {
                        Timer2.Enabled = true;
                    }
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Stationary since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pdt1 = db.GetPreviIGST221(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    if (pdt1.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        Hours = m / 60;
                        Minutes = m % 60;
                        lblEnginestatus.Text = "Stationary since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";

                    }
                }

            }
            else if (VS.Rows[0][8].ToString() == "1" && Convert.ToInt32(VS.Rows[0][2].ToString()) <= 3)
            {

                DataTable pidt = db.GetPreviING1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtid = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pidt.Rows.Count != 0)
                {
                    DateTime dtid1 = Convert.ToDateTime(pidt.Rows[0][0].ToString());
                    TimeSpan ts = dtid - dtid1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    if (m == 1)
                    {
                        Timer2.Enabled = true;
                    }
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Idling since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pidt1 = db.GetPreviIGST33(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtid1 = Convert.ToDateTime(pidt1.Rows[0][0].ToString());
                    TimeSpan ts = dtid - dtid1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Idling since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
            }
            else if (VS.Rows[0][8].ToString() == "1" && Convert.ToInt32(VS.Rows[0][2].ToString()) > 3)
            {
                DataTable pmdt = db.GetPreviMNG2(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                if (pmdt.Rows.Count != 0)
                {
                    DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                    TimeSpan ts = dtmng - dtmng1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Moving since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
                else
                {
                    DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                    TimeSpan ts = dtmng - dtmng1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    Hours = m / 60;
                    Minutes = m % 60;
                    lblEnginestatus.Text = "Moving since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins";
                }
            }
        }
        protected void Timer2_Tick(object sender, EventArgs e)
        {
            if (Session["UserID"] != null && Session["selectedVehicle"] != null && Session["timerc"] != null)
            {
                DBClass db = new DBClass();
                GMap1.resetMarkers();
                GMap1.resetPolylines();
                if (Session["timerc"].ToString() == "1")
                {
                    DataTable status = db.GetPStatus(Session["UserID"].ToString(), Session["selectedVehicle"].ToString());
                    List<GLatLng> points = new List<GLatLng>();
                    List<GLatLng> LatLoc = new List<GLatLng>();
                    Subgurim.Controles.GIcon icon1 = new Subgurim.Controles.GIcon();
                    icon1.image = "http://labs.google.com/ridefinder/images/mm_20_black.png";
                    Subgurim.Controles.GIcon Bicon = new Subgurim.Controles.GIcon();
                    //Bicon.image = "http://google.com/mapfiles/ms/micons/blue-dot.png";
                    Bicon.image = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    Subgurim.Controles.GIcon Ricon = new Subgurim.Controles.GIcon();
                    //Ricon.image = "http://www.google.com/mapfiles/ms/micons/red-dot.png";
                    Ricon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    Subgurim.Controles.GIcon Gicon = new Subgurim.Controles.GIcon();
                    //Gicon.image = "http://www.google.com/mapfiles/ms/micons/green-dot.png";
                    Gicon.image = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    Subgurim.Controles.GIcon Ticon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Ticon.image = "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
                    points.Add(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])));
                    DataTable tktable = db.gettotalkm(Session["selectedVehicle"].ToString());
                    double Lpkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                    Lpkm = Math.Truncate(Lpkm * 100) / 100;
                    double km = 0;
                    for (int i = 1; i < status.Rows.Count; i++)
                    {
                        if (status.Rows[i][2].ToString() != "0")
                        {
                            points.Add(new GLatLng(Convert.ToDouble(status.Rows[i][0]), Convert.ToDouble(status.Rows[i][1])));
                            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[i][0]), Convert.ToDouble(status.Rows[i][1]));
                            double speed = Convert.ToDouble(status.Rows[i][2].ToString());
                            int igst1 = Convert.ToInt32(status.Rows[i][8].ToString());
                            km = 0;
                            km +=bl12.CalcDistance(Convert.ToDouble(status.Rows[i - 1][0].ToString()), Convert.ToDouble(status.Rows[i - 1][1].ToString()), Convert.ToDouble(status.Rows[i][0].ToString()), Convert.ToDouble(status.Rows[i][1].ToString()));
                            Lpkm = Lpkm - km;
                            Lpkm = Math.Truncate(Lpkm * 100) / 100;
                            if (igst1 == 1 || (igst1 == 0 && speed < 3))
                            {
                                GMap1.setCenter(gLatLng1, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                                Subgurim.Controles.GMarker LMark = new Subgurim.Controles.GMarker(gLatLng1);
                                Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();

                                if (igst1 == 1 && speed > 0)
                                {
                                    string sped = Convert.ToString(speed * 1.85);
                                    DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                    options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString() + "--KM :" + Lpkm.ToString();
                                    options.icon = icon1;
                                    LMark.options = options;

                                }
                                else if (igst1 == 0 && speed == 0)
                                {
                                    string sped = "0";
                                    DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                    options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString() + "--KM :" + Lpkm.ToString();
                                    options.icon = Bicon;
                                    LMark.options = options;
                                }
                                else if (speed <= 3 && igst1 == 0)
                                {
                                    string sped = "0";
                                    DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                    options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString() + "--KM :" + Lpkm.ToString();
                                    options.icon = icon1;
                                    LMark.options = options;
                                }
                                GMap1.Add(LMark);
                            }
                            else
                            {
                                Subgurim.Controles.GMarker TMark = new Subgurim.Controles.GMarker(gLatLng1);
                                Subgurim.Controles.GMarkerOptions Toptions = new Subgurim.Controles.GMarkerOptions();
                                Toptions.icon = Ticon;
                                string sped = Convert.ToString(speed * 1.85);
                                DateTime LocDTL = Convert.ToDateTime(status.Rows[i][4]);
                                Toptions.title = Session["selectedVehicle"].ToString() + "--Towed At :" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString() + "--KM :" + Lpkm.ToString();
                                TMark.options = Toptions;
                                GMap1.Add(TMark);
                            }
                        }
                    }
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
                    Subgurim.Controles.GMarkerOptions options1 = new Subgurim.Controles.GMarkerOptions();
                    GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    double speed1 = Convert.ToDouble(status.Rows[0][2].ToString());
                    int igst = Convert.ToInt32(status.Rows[0][8].ToString());
                    if (igst == 0 && speed1 == 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        //options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Ricon;
                        oMarker.options = options1;
                    }
                    else if (speed1 > 3 && igst == 1)
                    {
                        string sped = Convert.ToString(speed1 * 1.85);
                        lblspeed.Text = sped;
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        // options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Gicon;
                        oMarker.options = options1;
                    }
                    else if (speed1 <= 3 && igst == 1)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        //options1.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        options1.icon = Bicon;
                        oMarker.options = options1;
                        lblspeed.Text = sped;
                    }
                    else if (igst == 0 && speed1 != 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options1.title = Session["selectedVehicle"].ToString() + "Towed At:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options1;
                        lblspeed.Text = sped;
                        options1.icon = Ticon;
                    }
                    GMap1.Add(oMarker);
                    GPolyline line = new GPolyline(points, "#008000", 2);
                    GMap1.Add(line);
                }
                else if (Session["timerc"].ToString() == "0")
                {
                    DataTable status = db.GetVehiclePresentlocation(Session["selectedVehicle"].ToString(), Session["UserID"].ToString());
                    Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));
                    GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    GMap1.GZoom = 11;
                    Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
                    Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                    Subgurim.Controles.GIcon Iicon = new Subgurim.Controles.GIcon();
                    //Iicon.image = "http://google.com/mapfiles/ms/micons/blue.png";
                    Iicon.image = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();
                    //icon.image = "http://www.google.com/mapfiles/ms/micons/red.png";
                    icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    Subgurim.Controles.GIcon Licon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Licon.image = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    Subgurim.Controles.GIcon Ticon = new Subgurim.Controles.GIcon();
                    // Licon.image = "http://google.com/mapfiles/ms/micons/green.png";
                    Ticon.image = "http://labs.google.com/ridefinder/images/mm_20_yellow.png";
                    double speed = Convert.ToDouble(status.Rows[0][2].ToString());
                    int igst = Convert.ToInt32(status.Rows[0][8].ToString());
                    if (igst == 0 && speed == 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        options.icon = icon;
                    }
                    else if (speed > 3 && igst == 1)
                    {
                        string sped = Convert.ToString(speed * 1.85);
                        lblspeed.Text = sped;
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        options.icon = Licon;
                    }
                    else if (speed <= 3 && igst == 1)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "--Time:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        lblspeed.Text = sped;
                        options.icon = Iicon;
                    }
                    else if (igst == 0 && speed != 0)
                    {
                        string sped = "0";
                        DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                        options.title = Session["selectedVehicle"].ToString() + "Towed At:" + LocDTL.ToString("hh:mm tt") + "--Speed:" + sped.ToString();
                        oMarker.options = options;
                        lblspeed.Text = sped;
                        options.icon = Ticon;
                    }
                    GMap1.Add(oMarker);
                }
            }
            Timer2.Enabled = false;
        }
        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Session["timerc"] = null;
            Session["UserName"] = null;
            Session["ParentUser"] = null;
            Session["Logotext"] = null;
            Session["APIKey"] = null;
            Response.Redirect("Login.aspx");
        }
        protected void rbtn_Click(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                Timer1.Enabled = true ;
                DBClass db = new DBClass();
                DataGridItem dgi = (DataGridItem)((RadioButton)sender).Parent.Parent;
                string address = ((LinkButton)dgi.FindControl("vehicleno")).Text;
                Session["selectedVehicle"] = address;
                Session["timerc"] = 0;
                DataTable status = db.GetVehiclePresentlocation(address, Session["UserID"].ToString());
                if (status.Rows.Count > 0)
                {

                    Timer2.Enabled = true;
                    Vehicle_Status(address);
                }
                else
                {
                    Timer1.Enabled = false;
                    string message = "alert('Please check connections for " + address + "')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
        }
        protected void Btndalerts_Click(object sender, EventArgs e)
        {
            if ((lblalerts2.Text != "") || (lbloffalerts.Text != ""))
            {
                DBClass db = new DBClass();
                DataTable stat;
                if (Session["UserRole"].ToString() == "3")
                {
                    stat = db.GetVehicles(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());
                }
                else
                {
                    stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                }
                //DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                string stralert = "";
                string stralert1 = "";

                if (lblalerts2.Text != "")
                {
                    stralert = "Daily Live Alerts ,";
                }
                if (lbloffalerts.Text != "")
                {
                    stralert1 = "";
                    //stralert1 = ",,, Offline More than 6 Hr ,";
                }
                stralert += lblalerts2.Text;
                string stralert2 = stralert.Replace("||", ",");
                stralert2 = stralert2.Replace("<span style=\"color:Red;\">", "");
                stralert2 = stralert2.Replace("</span>", "");

                stralert1 += lbloffalerts.Text;
                string stralert3 = stralert1.Replace("||", ",");
                stralert3 = stralert3.Replace("<span style=\"color:Red;\">", "");
                stralert3 = stralert3.Replace("</span>", "");
                stralert2 = stralert2 + stralert3;

                string[] results = stralert2.Split(new[] { ',' });
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", results) + "'.split(','));", true);

            }
        }
        public void Bind_Vehicles()
        {
            DBClass dbk = new DBClass();
            DataTable stat;
            if (Session["UserRole"].ToString() == "3")
            {
                stat = dbk.GetVehicles(Session["cUser_id"].ToString());//, Session["ParentUser"].ToString());
            }
            else
            {
                stat = dbk.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
            }
           // DataTable stat = dbk.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
            lbloffalerts.Text = "";
            lblalerts2.Text = "";
            if (stat.Columns.Count > 0)
            {
                stat.Columns.Add("Status", typeof(string));
                for (int i = 0; i < stat.Rows.Count; i++)
                {
                    string VN = Convert.ToString(stat.Rows[i][0].ToString());
                    DataTable VS = dbk.GetVehStat(Session["UserID"].ToString(), VN);
                    if (VS.Rows.Count > 0)
                    {
                        stat.Rows[i]["Status"] = "Status";
                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                        StringDT = StringDT.AddMinutes(360);
                        if (sysDT > StringDT)
                        {
                            //stat.Rows[i]["Status"] = "Status";// "  OFFLINE &nbsp; ✘";
                            string lblalerts = alert2(stat.Rows[i]["VehicalNumber"].ToString(), 1);
                            lbloffalerts.Text += lblalerts;
                        }
                        else
                        {
                            //stat.Rows[i]["Status"] = "Status";// "  ONLINE &nbsp; ✔";
                            string lblalerts = alert2(stat.Rows[i]["VehicalNumber"].ToString(), 0);
                            lblalerts2.Text += lblalerts;
                        }
                    }
                    else
                    {
                        stat.Rows[i]["Status"] = "Status";
                    }
                    if (Session["CheckAlerts"] == null)
                    {
                        string[] stralt = getalertdetailsfunction(VN);
                        if (stralt[2].ToString() != "NO Alert For This Vehicle")
                        {
                            dbk.Vehicle_Alert_Status(VN, Session["UserID"].ToString(), 0);
                            stat.Rows[i]["Vehicle_Alerts_Status"] = "0";
                        }
                        else
                        {
                            dbk.Vehicle_Alert_Status(VN, Session["UserID"].ToString(), 1);
                            stat.Rows[i]["Vehicle_Alerts_Status"] = "1";
                        }
                    }
                }
                Session["CheckAlerts"] = 1;
            }
            stat.AcceptChanges();
            gdVehicles.DataSource = stat;
            gdVehicles.DataBind();
            Session["Status"] = stat;
        }
        public void Temp(string Vno)
        {
            DBClass db = new DBClass();
            DataTable fre;
            if (Session["UserRole"].ToString() == "3")
            {
                fre = db.Getfreez(Session["Cusername"].ToString());
            }
            else
            {
                fre = db.Getfreez(Session["Username"].ToString());
            }
            //DataTable fre = db.Getfreez(Session["Username"].ToString());
            string vno = Session["selectedVehicle"].ToString();
            DataTable getreefer = db.Getreeferveh(Vno.ToString());
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
            string condition = status.Rows[0][9].ToString();
            double conditon1 = Convert.ToDouble(condition);
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            if (fre.Rows[0][1].ToString() == "Yes")
            {

                if (conditon1 >= 10000)
                {
                    double temp = 0;
                    double cond1 = conditon1 - 10000;
                    if (cond1 > 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            if (m == 1)
                            {
                                Timer2.Enabled = true;
                            }
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double cond2 = cond1 - 1000;
                            temp = cond2 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                            {
                                lbltemp.ForeColor = System.Drawing.Color.Red;
                                lbltemp2.ForeColor = System.Drawing.Color.Red;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                //lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezer since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                // lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }
                            else
                            {

                                lbltemp.ForeColor = System.Drawing.Color.Red;
                                lbltemp2.ForeColor = System.Drawing.Color.Red;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                //lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezer since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                // lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }

                        }
                    }
                    else if (cond1 < 1000)
                    {

                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = false;
                        }
                        else
                        {

                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = true;
                            lblreefer.Visible = false;
                            lbltemp2.Visible = false;
                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                        }
                    }


                }
                else if (conditon1 >= 1000)
                {
                    DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                    double temp = 0;

                    if (pdt.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        if (m == 1)
                        {
                            Timer2.Enabled = true;
                        }
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        if (temp <= 7)
                        {
                            temp = temp * 1.9;
                        }
                        else if (temp <= 10)
                        {
                            temp = temp * 1.7;
                        }
                        else if (temp <= 14)
                        {
                            temp = temp * 1.4;
                        }
                        else if (temp <= 17)
                        {
                            temp = temp * 1.3;
                        }
                        else if (temp <= 20)
                        {
                            temp = temp * 1.1;
                        }
                        else if (temp > 20)
                        {
                            temp = temp * 1;
                        }
                        temp = (double)Math.Round(temp, 1);
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            //lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");

                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                        }
                        else
                        {
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            //lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");

                            //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                        }

                    }
                }
                else if (conditon1 < 1000)
                {
                    if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = false;
                        lblreefer.Visible = true;
                        lbltemp2.Visible = false;
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                    }
                    else
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = true;
                        lblreefer.Visible = false;
                        lbltemp2.Visible = false;

                    }
                }

            }
            else
            {
                if (conditon1 >= 10000)
                {
                    double temp = 0;
                    double cond1 = conditon1 - 10000;
                    if (cond1 > 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            if (m == 1)
                            {
                                Timer2.Enabled = true;
                            }
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double cond2 = cond1 - 1000;
                            temp = cond2 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Red;
                            lbltemp2.ForeColor = System.Drawing.Color.Red;
                            if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                            {
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                // lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                            }
                            else
                            {
                                lblcabin.Visible = false;
                                lblreefer.Visible = true;
                                lbltemp2.Visible = true;
                                // lbltemp.Attributes.Add("style", "text-decoration:blink");
                                // lbltemp.Text = " - " + temp.ToString() + " °C " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + "&nbsp;Mins");
                                //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                                lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                                lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                            }


                        }
                    }

                    else if (cond1 < 1000)
                    {
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = false;
                        }
                        else
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            lbltemp.ForeColor = System.Drawing.Color.Black;
                            lbltemp.Text = temp.ToString() + " °C";
                            lblcabin.Visible = true;
                            lblreefer.Visible = false;
                            lbltemp2.Visible = false;

                        }
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);
                    }


                }
                else if (conditon1 >= 1000)
                {
                    DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                    DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                    double temp = 0;

                    if (pdt.Rows.Count != 0)
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        if (m == 1)
                        {
                            Timer2.Enabled = true;
                        }
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Red;
                        lbltemp2.ForeColor = System.Drawing.Color.Red;
                        if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                        {
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            // lbltemp2.Visible = true;
                            // lbltemp.Attributes.Add("style", "text-decoration:blink");
                            // lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing)";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  (Freezing) since" + Hours.ToString() + "Hrs:" + Minutes.ToString() + "Min:";
                            //lbltemp.Text = " - " + temp.ToString() + " °C  " + ("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";
                        }
                        else
                        {
                            lblcabin.Visible = false;
                            lblreefer.Visible = true;
                            lbltemp2.Visible = true;
                            lbltemp.Text = " - " + temp.ToString() + " °C  "; //("Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins");
                            lbltemp2.Text = "(" + "Freezing since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins" + ")";

                        }
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp);


                    }
                }
                else if (conditon1 < 1000)
                {
                    if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Attributes.Add("style", "text-decoration:blink");

                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = false;
                        lblreefer.Visible = true;
                        lbltemp2.Visible = false;
                        //lbltemp.Text = String.Format(": <span style='color:red'>{0}</style> </span>", temp );
                        //lbltemp.Attributes.Add("style", "text-decoration:blink");
                    }
                    else
                    {
                        double temp = 0;
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        lbltemp.ForeColor = System.Drawing.Color.Black;
                        lbltemp.Attributes.Add("style", "text-decoration:blink");

                        lbltemp.Text = temp.ToString() + " °C";
                        lblcabin.Visible = true;
                        lblreefer.Visible = false;
                        lbltemp2.Visible = false;

                    }
                }

            }
        }
        public void ac(string Vno)
        {
            try
            {
                DBClass db = new DBClass();
                int Hours = 0;
                int days = 0;
                int Minutes = 0;
                DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
                DataTable acstat = db.Getacveh(Vno);
                DataTable pdt = db.Getprevacstring(acstatus.Rows[0][11].ToString());
                DateTime dtst = Convert.ToDateTime(acstatus.Rows[0][4].ToString());
                if (acstat.Rows[0][1].ToString() == "On")
                {

                    if (acstatus.Rows[0][6].ToString() == "0.00")// && pdt.Rows[0][1].ToString() == "0.00")
                    {
                        DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        TimeSpan ts = dtst - dtst1;
                        int m = Convert.ToInt32(ts.TotalMinutes);
                        days = m / 1440;
                        Hours = m / 60;
                        Minutes = m % 60;
                        lblac.ForeColor = System.Drawing.Color.Red;
                        lblac.Text = "AC ON since &nbsp;" + Hours.ToString() + "&nbsp; Hrs &nbsp;" + Minutes.ToString() + " &nbsp;Mins";

                    }
                    else
                    {
                        DataTable acoff = db.Getacoff(acstatus.Rows[0][11].ToString());
                        DateTime acof = Convert.ToDateTime(acoff.Rows[0][0].ToString());//"dd-MM-yyyy hh:mm tt");
                        string acofftime = acof.ToString("dd-MM-yyyy hh:mm tt");
                        lblac.Text = "OFF " + "( at " + acofftime + ")";
                        //lblac.Text = "OFF";
                    }
                }
                else
                {
                    lblac.Text = "Service Not Availed";
                }
            }
            catch (Exception ex) { }

        }
        
        protected void btnhome_Click1(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Timer2.Enabled = false;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Session["timerc"] = null;
            if (Session["UserRole"].ToString() == "3")
            {
                Response.Redirect("ManagerHome.aspx?ManagerName=" + Session["UserName"].ToString());
            }
            else
            {
                Response.Redirect("Home.aspx?username=" + Session["UserName"].ToString());
            }
           // Response.Redirect("Home.aspx?username=" + Session["UserName"].ToString());           
        }

        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            return default(string);
        }




    }
}


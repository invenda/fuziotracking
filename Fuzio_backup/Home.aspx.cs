﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;
namespace Tracking
{
    public partial class Home : System.Web.UI.Page
    {
        static int lblno = 0;
        DBClass dbt1 = new DBClass();
        BLClass bl12 = new BLClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["page"] = "All Vehicle-Dashboard";
            Session["page1"] = "";

            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            if (!IsPostBack)
            {
                binggrid();
                DataTable getcount = dbt1.GetVehiclescount(Session["UserID"].ToString());
                if (getcount.Rows.Count > 0)
                    Nofodevices.Text = "Number of Devices Enabled :" + " " + (getcount.Rows[0][0].ToString());
                DataTable getdesablecount = dbt1.GetdesabledVehiclescount(Session["UserID"].ToString());
                if (getdesablecount.Rows.Count > 0)
                    noofdesabled.Text = "Number of Devices Disabled :" + " " + (getdesablecount.Rows[0][0].ToString());
                if (!string.IsNullOrEmpty(Request.QueryString["message"]))
                {
                    DataTable alert = dbt1.Getuseralert(Session["UserID"].ToString());

                    if (alert.Rows.Count > 0)
                    {
                        if (alert.Rows[0][0].ToString() == "Yes")
                        {
                            string alertstring = alert.Rows[0][1].ToString();
                            this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
                        }

                    }


                }
            }

            // Response.Redirect("Home.aspx?username=" + Session["UserName"].ToString());

        }
        protected void LinkButton13_Click(object sender, EventArgs e)
        {
            Response.Redirect("TemperatureGraph.aspx");
        }
        protected void LinkButton36_Click(object sender, EventArgs e)
        {
            Response.Redirect("Fuel.aspx");
        }
        protected void LinkButton10_Click(object sender, EventArgs e)
        {
            Response.Redirect("VehicalDailyReport.aspx");
        }
        protected void LinkButton9_Click(object sender, EventArgs e)
        {
            Response.Redirect("MultiTrack.aspx");
        }
        public void binggrid()
        {
            lbloffalerts.Text = string.Empty;
            lblalerts2.Text = "";
            DBClass db = new DBClass();
            if (Session["UserID"] != null)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    // lbtnAdmin.Visible = false;
                }
                DataTable stat = db.Select_GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                if (stat.Columns.Count > 0)
                {
                    stat.Columns.Add("Status", typeof(string));
                    stat.Columns.Add("Lastdata", typeof(string));
                    stat.Columns.Add("Gps", typeof(string));
                    // stat.Columns.Add("OVS", typeof(string));
                    stat.Columns.Add("ingnition", typeof(string));
                    stat.Columns.Add("bvolt", typeof(string));
                    stat.Columns.Add("dmobile", typeof(string));
                    stat.Columns.Add("Dname", typeof(string));


                    for (int i = 0; i < stat.Rows.Count; i++)
                    {
                        string VN = Convert.ToString(stat.Rows[i][0].ToString());

                        DataTable VS = db.GetVehStat(Session["UserID"].ToString(), VN);
                        DataTable status = db.Select_GetVehiclePresentlocation(VN, Session["UserID"].ToString());

                        if (VS.Rows.Count != 0)
                        {
                            // DataTable status = db.Select_GetVehiclePresentlocation(VN, Session["UserID"].ToString());
                            DateTime sysDT = DateTime.Now;
                            DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                            StringDT = StringDT.AddMinutes(360);
                            if (sysDT > StringDT)
                            {
                                stat.Rows[i]["Status"] = "Status";// "  OFFLINE &nbsp; ?";
                                string lblalerts = alert2(VN, 1);
                               // lbloffalerts.Text += lblalerts;
                            }
                            else //if(sysDT <= StringDT)
                            {
                                stat.Rows[i]["Status"] = "Status";//"  ONLINE &nbsp;  ?";
                                string lblalerts = alert2(VN, 0);
                               // lblalerts2.Text += lblalerts;
                            }
                            if (status.Rows.Count != 0)
                            {
                                //lastdata
                                DateTime fff = Convert.ToDateTime(status.Rows[0][4].ToString());
                                stat.Rows[i]["Lastdata"] = fff.ToString("dd/MM/yyyy hh:mm tt");//status.Rows[0][4].ToString();
                                //GPS
                                stat.Rows[i]["Gps"] = status.Rows[0][7].ToString();

                                //ingnition
                                if (status.Rows[0][8].ToString() == "0")
                                    stat.Rows[i]["ingnition"] = "Off";
                                else
                                    stat.Rows[i]["ingnition"] = "On";

                                //battery voltage
                                int bat = Convert.ToInt32(status.Rows[0][3].ToString());
                                double bvolt = bat / 42.5;
                                bvolt = Math.Round(bvolt, 2);
                                stat.Rows[i]["bvolt"] = bvolt.ToString();

                            }
                        }
                        else
                        {
                            DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), VN);
                            if (VSoff.Rows.Count > 0)
                            {
                                DataTable statusoff = db.GetVehiclePresentlocation1(VN, Session["UserID"].ToString());
                                DateTime sysDT = DateTime.Now;
                                DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                                StringDT = StringDT.AddMinutes(360);
                                if (sysDT > StringDT)
                                {
                                    stat.Rows[i]["Status"] = "Status";// "  OFFLINE &nbsp; ?";
                                    string lblalerts = alert2(VN, 1);
                                   // lbloffalerts.Text += lblalerts;

                                }
                                else //if(sysDT <= StringDT)
                                {
                                    stat.Rows[i]["Status"] = "Status";//"  ONLINE &nbsp;  ?";
                                    string lblalerts = alert2(VN, 0);
                                   // lblalerts2.Text += lblalerts;
                                }

                                if (status.Rows.Count == 0)
                                {
                                    //lastdata
                                    DateTime fff = Convert.ToDateTime(statusoff.Rows[0][4].ToString());
                                    stat.Rows[i]["Lastdata"] = fff.ToString("dd/MM/yyyy hh:mm tt");//statusoff.Rows[0][4].ToString();
                                    //GPS
                                    stat.Rows[i]["Gps"] = statusoff.Rows[0][7].ToString();

                                    //ingnition
                                    if (statusoff.Rows[0][8].ToString() == "0")
                                        stat.Rows[i]["ingnition"] = "Off";
                                    else
                                        stat.Rows[i]["ingnition"] = "On";

                                    //battery voltage
                                    int bat = Convert.ToInt32(statusoff.Rows[0][3].ToString());
                                    double bvolt = bat / 42.5;
                                    bvolt = Math.Round(bvolt, 2);
                                    stat.Rows[i]["bvolt"] = bvolt.ToString();

                                }
                            }
                            else
                            {

                            }

                        }

                        //driver mobile no
                        DataTable DriDet = db.GerDriverDetails(VN, Session["UserID"].ToString());
                        stat.Rows[i]["dmobile"] = DriDet.Rows[0][2].ToString();

                        //Driver name
                        string dname11 = DriDet.Rows[0][0].ToString();
                        if (dname11.Length < 13)
                        {
                            stat.Rows[i]["Dname"] = dname11;
                        }
                        else
                        {
                            string str = dname11.Substring(0, 13);
                            stat.Rows[i]["Dname"] = str;// DriDet.Rows[0][0].ToString();

                        }


                    }

                }
                stat.AcceptChanges();
                GV.DataSource = stat;
                GV.DataBind();
                imgst.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                imgidl.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                imgmv.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                LblSCT.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                Session["Status"] = stat;
                Session["CheckAlerts"] = null;
            }

        }
        protected void btnalert_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                //DataGridItem dgi = (DataGridItem)((Button)sender).Parent.Parent;
                //string key = gdVehicles.DataKeyField[dgi.ItemIndex].ToString();
                // string address = ((LinkButton)dgi.FindControl("vehicleno")).Text;
                string[] alertstring = getalertdetailsfunction(vhno);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
            }
            catch (Exception ex) { }
            Timer1.Enabled = true;

        }
        public string[] getalertdetailsfunction(string Vno)
        {
            DBClass db = new DBClass();

            DataTable vdt = db.vehicledetailsalerts(Vno);
            string vinsEd = vdt.Rows[0][0].ToString();
            string rtorewd = vdt.Rows[0][1].ToString();
            string dlicexd = vdt.Rows[0][2].ToString();
            string overspeed = vdt.Rows[0][3].ToString();
            string conttodate = vdt.Rows[0][4].ToString();
            string gprsad = vdt.Rows[0][5].ToString();
            string fcdate = vdt.Rows[0][6].ToString();
            string emsdate = vdt.Rows[0][7].ToString();
            string roadtax = vdt.Rows[0][8].ToString();
            //string[] stralert = new string[10];
            List<string> stralert = new List<string>();
            stralert.Add("VEHICLE RTO No:");
            stralert.Add("  ");
            DateTime spdate = DateTime.Now;
            spdate = spdate.AddDays(30);
            DateTime spdate1 = DateTime.Now.AddDays(3);
            //DataRow dr = alertdt.NewRow();
            int j = 0;
            var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
            if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Insurance will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Insurance DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Insurance Expired Today");
                    j++;
                }
            }
            if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle RTO Permit will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle RTO Permit Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle RTO Permit Expired Today");
                    j++;
                }
            }
            if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Driver Driving Licence Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Driver Driving Licence Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Driver Driving Licence Expired Today");
                    j++;
                }
            }
            if (overspeed != "0" && overspeed != "")
            {
                DataTable dtdevice = db.getdevicesid(Vno);
                int ovrspeed = Convert.ToInt32(overspeed);
                ovrspeed = Convert.ToInt32(ovrspeed / 1.85);
                string ovs = ovrspeed.ToString();
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, ovs);
                if (dtoverspeed.Rows.Count != 0)
                {
                    stralert.Add("Vehicle Over Speed..!");
                    j++;

                }
            }
            if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Contract Period Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Contract Period Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Contract Period Expired Today");
                    j++;
                }
            }
            if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 3 && s > 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle GPRS Recharge Expired Today");
                    j++;
                }
            }
            if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle FC Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle FC DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle FC Expired Today");
                    j++;
                }
            }
            if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Emmission Test Date Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Emmission Test DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Emmission Test Expired Today");
                    j++;
                }
            }
            if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Road Tax Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Road Tax DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Road Tax Expired Today");
                    j++;
                }
            }
            int len = stralert.Count;
            string[] salerts = new string[len];
            if (j != 0)
            {
                for (int m = 0, n = 1; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else if (m == 1)
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                    else
                    {
                        salerts[m] = n + " : " + stralert[m].ToString();
                        n++;
                    }
                }
            }
            else
            {
                if (lblno == 0)
                {
                    lblmalerts.Visible = false;
                }
                stralert.Add("NO Alert For This Vehicle");
                len = stralert.Count;
                salerts = new string[len];
                for (int m = 0; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                }
            }
            return salerts;
        }

        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session["UserRole"] = null;
            Session["UserID"] = null;
            Session["selectedVehicle"] = null;
            Session["address"] = null;
            Session["timerc"] = null;
            Session["UserName"] = null;
            Session["ParentUser"] = null;
            Session["Logotext"] = null;
            Session["APIKey"] = null;
            Response.Redirect("Login.aspx");

        }

        protected void Btndalerts_Click(object sender, GridViewRowEventArgs e)
        {

        }
        protected void rbtn_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;

            if (Session["UserID"] != null)
            {
                DBClass db = new DBClass();
                RadioButton btn = (RadioButton)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                if (status.Rows.Count > 0)
                {
                    Response.Redirect("Track.aspx?userid=" + Session["UserID"].ToString() + "&vehNo=" + vhno);
                }
                else
                {
                    Timer1.Enabled = true;
                    string message = "alert('Please check connections for " + vhno + " ')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                    //string alertstring = " There is no present data...";
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));

                }

            }
        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            List<string> stralerts = new List<string>();
            try
            {

                DBClass db = new DBClass();
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                try
                {
                    GeoCode objAddress = new GeoCode();
                    objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                    StringBuilder sb = new StringBuilder();
                    if (objAddress.valid)
                    {
                        sb.Append(objAddress.Placemark.address.ToString());
                        stralerts.Add("Vehicle no:  " + vhno);
                        stralerts.Add(" ");
                        stralerts.Add("Vehicle Location:  " + sb.ToString());
                    }
                    else
                        stralerts.Add("Not found");
                }
                catch (Exception ex) { }

                string[] salerts = new string[stralerts.Count];

                for (int m = 0; m < stralerts.Count; m++)
                {

                    salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
            catch (Exception ex) { }

        }

        protected void btnspeed_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
                DBClass db = new DBClass();
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                double speed = Convert.ToDouble(status.Rows[0][2].ToString());
                if (speed > 3)
                {
                    string sped = Convert.ToString(speed * 1.85);
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                    // DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);

                }
                else if (speed < 3)
                {
                    string sped = "0";
                    //DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                }

            }
            catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }
        
        protected void btnfuel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            List<string> stralerts = new List<string>();
            //try
            // {
            DBClass db = new DBClass();
            DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
            if (fuelim.Rows[0][1].ToString() == "Yes")
            {
                DataTable dt2 = db.prevfuelstaus(vhno);
                if (dt2.Rows.Count != 0)
                {
                    if (Convert.ToInt32(dt2.Rows[0][1].ToString()) == 1)
                    {
                        //double fulestat;
                        DataTable dtf = db.GetVehiclesfuel(vhno, Session["UserId"].ToString());
                        DataTable tt = db.getfivestagefuel(vhno);
                        if (tt.Rows.Count > 0)
                        {
                            double tankcapacity =Convert.ToDouble(tt.Rows[0][2].ToString());
                            List<double> y = new List<double>();
                            y.Add(Convert.ToDouble(tt.Rows[0][3].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][5].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][7].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][9].ToString()));
                            y.Add(Convert.ToDouble(tt.Rows[0][11].ToString()));
                            List<double> x = new List<double>();
                            x.Add(Convert.ToDouble(tt.Rows[0][4].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][6].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][8].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][10].ToString()));
                            x.Add(Convert.ToDouble(tt.Rows[0][12].ToString()));
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());

                            double[] yyy = y.ToArray();
                            double[] xxx = x.ToArray();
                            double newY =bl12.lagrange(string_val_fuel, xxx, yyy);
                            newY = (double)Math.Round(newY);
                            if (newY <= tankcapacity )
                            {
                               
                                stralerts.Add("Vehicle no:   " + vhno);
                                stralerts.Add(" ");
                                stralerts.Add("Fuel in Ltr:  " + newY.ToString());
                            }
                            else if (newY > tankcapacity)
                            {
                                stralerts.Add("Vehicle no:   " + vhno);
                                stralerts.Add(" ");
                                stralerts.Add("Fuel in Ltr:  " + tankcapacity.ToString());
                            }
                            else
                            {
                                stralerts.Add("Vehicle no:   " + vhno);
                                stralerts.Add(" ");
                                stralerts.Add("Waiting....");
                            }

                        }
                        else if (dtf.Rows.Count > 0)
                        {
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());

                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());

                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
                            {

                                double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                                if (Tank_typ == 0)
                                {
                                    double x = Fuel_Cali - Emptytankvalue;
                                    double y = tank_capacity / x;

                                    double z = string_val_fuel - Emptytankvalue;

                                    double b = z * y;
                                    b = (double)Math.Round(b);
                                    //fulestat = (string_val_fuel) / (Fuel_Cali);
                                    //fulestat = fulestat * tank_capacity;;
                                    //fulestat = Math.Truncate(fulestat * 100) / 100
                                    stralerts.Add("Vehicle no:   " + vhno);
                                    stralerts.Add(" ");
                                    stralerts.Add("Fuel in Ltr:  " + b);
                                }
                                else
                                {
                                    double x = Emptytankvalue - Fuel_Cali;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel - Fuel_Cali;
                                    double b = z * y;
                                    double f = tank_capacity - b;
                                    f = (double)Math.Round(f);
                                    //fulestat = (string_val_fuel) / (Fuel_Cali);
                                    //fulestat = fulestat * tank_capacity;
                                    //fulestat = tank_capacity - fulestat;
                                    //fulestat = Math.Truncate(fulestat * 100) / 100;
                                    stralerts.Add("Vehicle no:   " + vhno);
                                    stralerts.Add(" ");
                                    stralerts.Add("Fuel in Ltr:  " + f);
                                }
                            }                          
                        }
                        else
                        {
                            stralerts.Add("Fuel parameters are not calibrated..");
                        }
                       
                    }

                }
                else stralerts.Add("Waiting...");
            }
            else
            {
                stralerts.Add(" Service Not Availed");
                //lblFuel.Text = " Service Not Availed";
                // string alertstring = " Fuel thing not implimented";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
            }
            //catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }


        public double checkavailabe(string Vno)
        {
            DBClass db = new DBClass();
            DateTime dt1 = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 12:00 AM");
            //DataTable VS = db.GetVehStat(Session["UserID"].ToString(), Vno);
            // DateTime dt2 = Convert.ToDateTime(VS.Rows[0][0].ToString());
            DateTime dt2 = Convert.ToDateTime(DateTime.Now.ToString());
            if (dt2 < dt1)
            {

                dt1 = Convert.ToDateTime(dt2.ToShortDateString() + " 12:00 AM");
            }
            TimeSpan ts = dt2 - dt1;
            DataTable dt = db.getblackspot1(Vno, dt1.ToString(), dt2.ToString());
            double sysdata = Convert.ToDouble(dt.Rows[0][0].ToString());
            double av = ((ts.TotalMinutes - sysdata) / ts.TotalMinutes) * 100;
            av = 100 - av;
            av = (double)Math.Round(av, 1);
            if (av < 90 && av > 0)
            {
                av = av + 5;
            }
            else if (av < 95 && av > 0)
            {
                av = av + 2;
            }
            else if (av < 98 && av > 0)
            {
                av = av + 1;
            }
            else if (av >= 100)
            {
                av = 100;
            }
            return av;
        }

        protected void btnavail_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
                DBClass db = new DBClass();
                string avilable = checkavailabe(vhno).ToString();

                if (avilable != null)
                {
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Available:  " + avilable + "%");
                }
                else
                {
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Not found");
                }
            }
            catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            Timer1.Enabled = true;
        }


        protected void btnKM_Click(object sender, EventArgs e)
        {
            try
            {

                Timer1.Enabled = false;

                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                tkmh(vhno);
                //----------this is for update cumilative kilometer report-------------
               // Totalrunkm(vhno);

                List<string> stralerts = new List<string>();
                try
                {
                    DBClass db = new DBClass();
                    //total km run
                    tkmh(vhno);
                    DataTable tktable = db.gettotalkm(vhno);
                    double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                    // DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
                    tktkm = Math.Truncate(tktkm * 100) / 100;

                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Today KM run:  " + tktkm);

                }
                catch (Exception ex) { }

                string[] salerts = new string[stralerts.Count];

                for (int m = 0; m < stralerts.Count; m++)
                {

                    salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
                Timer1.Enabled = true;
            }
            catch (Exception ex) { }

        }
        public void tkmh(string vno)
        {

            DBClass db = new DBClass();
            DataTable dt = db.gettotalkm(vno);
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() != Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                    string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                    db.uptotalkm(vno, fdate, tdate, "0", fdate);
                }
                else if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() == Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    double tkm = Convert.ToDouble(dt.Rows[0][4].ToString());
                    string fdate = dt.Rows[0][2].ToString();
                    string fdate1 = dt.Rows[0][5].ToString();
                    string tdate = dt.Rows[0][3].ToString();
                    DataTable dt1 = db.Select_TotalRunkm11(vno, fdate, tdate);                   

                    if (dt1.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                        {
                            tkm +=bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                        }
                        fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    }
                    else if (dt1.Rows.Count == 0)
                    {
                        fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

                    }
                    db.uptotalkm(vno, fdate, tdate, tkm.ToString(), fdate);
                }
            }
            else
            {
                string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                db.settotalkm(vno, fdate, tdate, "0");
            }
        }

        public void Totalrunkm(string vno)
        {
            DBClass db = new DBClass();
            DataTable dt = db.getTotalRunkm(vno);
            if (dt.Rows.Count != 0)
            {
                string fdate = dt.Rows[0][2].ToString();
                double trkm = Convert.ToDouble(dt.Rows[0][3].ToString());
                string tdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                DataTable dt1 = db.Select_TotalRunkm(dt.Rows[0][1].ToString(), fdate, tdate);
                if (dt1.Rows.Count != 0)
                {
                    for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                    {
                        trkm += bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                    }
                    fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    trkm = (double)Math.Round(trkm, 2);

                }
                else
                {
                    fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                }

                db.IU_Totalrunkm(vno, fdate, trkm.ToString());
               // DataTable P = db.getPFE(vno);
               // DataTable CKMdt = db.Get_Correction_KM(vno);
               // double ckm = 0;
              //  if (CKMdt.Rows[0][0].ToString() != "")
               // {
                   // ckm = Convert.ToDouble(CKMdt.Rows[0][0].ToString());
               // }
               // double nw = Convert.ToDouble(P.Rows[0][0].ToString());
               // nw = trkm + nw + ckm;
               // lblTotKmRun.Text = nw.ToString() + "&nbsp;&nbsp;KM";
            }
            else
            {
                DataTable P = db.getPFE(vno);
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                string ftime = P.Rows[0][1].ToString();
                DateTime fdate = Convert.ToDateTime(ftime, dtfi);
                db.IU_Totalrunkm(vno, fdate.ToString(), "0");
            }

        }
        protected void btntemp_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;

                string[] salerts = gettempalert(vhno);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
            catch (Exception ex) { }
            Timer1.Enabled = true;
        }
        public string[] gettempalert(string Vno)
        {
            DBClass db = new DBClass();
            List<string> stralerts = new List<string>();
            try
            {
                DataTable getreefer = db.Getreeferveh(Vno.ToString());
                if (getreefer.Rows[0][1].ToString() == "Reefer Vehicles")
                {
                    string Acsta = ac(Vno.ToString());
                    string temp = Temp(Vno.ToString());
                    string acontime1 = acontime(Vno.ToString());
                    stralerts.Add("Vehicle no:  " + Vno);
                    stralerts.Add(" ");
                    stralerts.Add("Reefer Temp:  " + temp + "°C");
                    stralerts.Add(" ");
                    stralerts.Add("AC Status:  " + Acsta);
                    stralerts.Add(" ");
                    stralerts.Add("Today AC working for:  " + acontime1);
                }
                else
                {
                    string Acsta = ac(Vno.ToString());
                    string temp = Temp(Vno.ToString());
                    string acontime1 = acontime(Vno.ToString());
                    stralerts.Add("Vehicle no:  " + Vno);
                    stralerts.Add(" ");
                    stralerts.Add("Temp:  " + temp + "°C");
                    stralerts.Add(" ");
                    stralerts.Add("AC Status:  " + Acsta);
                    stralerts.Add(" ");
                    stralerts.Add("Today AC working for:  " + acontime1);

                }

            }
            catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }
            return salerts;

        }
        public string ac(string Vno)
        {
            DBClass db = new DBClass();
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string ac = "";
            DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            DataTable acstat = db.Getacveh(Vno);

            DateTime dtst = Convert.ToDateTime(acstatus.Rows[0][4].ToString());
            if (acstat.Rows[0][1].ToString() == "On")
            {

                if (acstatus.Rows[0][6].ToString() == "0.00")//&& pdt.Rows[0][1].ToString() == "0.00")
                {
                    DataTable pdt = db.Getprevacstring(acstatus.Rows[0][11].ToString());
                    DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                    TimeSpan ts = dtst - dtst1;
                    int m = Convert.ToInt32(ts.TotalMinutes);
                    days = m / 1440;
                    Hours = m / 60;
                    Minutes = m % 60;
                    ac = "ON since  " + Hours.ToString() + "  Hrs  " + Minutes.ToString() + "  Mins";

                }
                else
                {
                    DataTable acoff = db.Getacoff(acstatus.Rows[0][11].ToString());
                    DateTime acof = Convert.ToDateTime(acoff.Rows[0][0].ToString());//"dd-MM-yyyy hh:mm tt");
                    string acofftime = acof.ToString("dd-MM-yyyy hh:mm tt");
                    ac = "OFF " + "( at " + acofftime + ")";
                }
            }
            else
            {
                ac = "Service Not Availed";
            }
            return ac;

        }

        public string acontime(string Vno)
        {
            DBClass db = new DBClass();
            int Hours = 0;
            int days = 0;
            int Minutes = 0;
            string actime = "";
            string stime = DateTime.Now.ToShortDateString();
            string stime1 = stime + " 12:00 AM";
            string ttime = DateTime.Now.ToString();
            DataTable acstatus = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            DataTable acstat = db.Getacveh(Vno);
            DataTable account = db.getacontime(acstatus.Rows[0][11].ToString(), stime1, ttime);

            if (acstat.Rows[0][1].ToString() == "On")
            {

                if (account.Rows.Count != 0)
                {
                    int m = Convert.ToInt32(account.Rows[0][0].ToString());
                    days = m / 1440;
                    Hours = m / 60;
                    Minutes = m % 60;
                    actime = Hours.ToString() + "  Hrs  " + Minutes.ToString() + "  Mins";

                }
                else
                {
                    actime = "0 Hours";
                }
            }
            else
            {
                actime = "Service Not Availed";
            }
            return actime;

        }


        public string Temp(string Vno)
        {

            DBClass db = new DBClass();
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            DataTable status = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            string condition = status.Rows[0][9].ToString();
            double conditon1 = Convert.ToDouble(condition);
            int days = 0;
            int Hours = 0;
            int Minutes = 0;
            double temp = 0;
            string temp12 = "";
            if (condition != "0")
            {
                if (fre.Rows[0][1].ToString() == "Yes")
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString();

                            }
                        }

                        else if (cond1 < 1000)
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            temp12 = temp.ToString();
                        }

                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());

                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            temp12 = "-" + temp.ToString();

                        }
                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();
                    }

                }


                else
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString();

                            }
                        }
                        else if (cond1 < 1000)
                        {
                            temp = cond1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            temp12 = temp.ToString();
                        }


                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());

                        // DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                        // TimeSpan ts = dtst - dtst1;
                        // int m = Convert.ToInt32(ts.TotalMinutes);
                        // days = m / 1440;
                        //Hours = m / 60;
                        // Minutes = m % 60;
                        double con1 = conditon1 - 1000;
                        temp = con1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = "-" + temp.ToString();


                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();

                    }

                }
            }
            return temp12;


        }
        protected void Btndalerts_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
            if ((lblalerts2.Text != "") || (lbloffalerts.Text != ""))
            {
                DBClass db = new DBClass();
                DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                string stralert = "";
                string stralert1 = "";

                if (lblalerts2.Text != "")
                {
                    stralert = "Daily Live Alerts ,";
                }
                if (lbloffalerts.Text != "")
                {
                    stralert1 = ",,, Offline More than 6 Hr ,";
                }
                stralert += lblalerts2.Text;
                string stralert2 = stralert.Replace("||", ",");
                stralert2 = stralert2.Replace("<span style=\"color:Red;\">", "");
                stralert2 = stralert2.Replace("</span>", "");

                stralert1 += lbloffalerts.Text;
                string stralert3 = stralert1.Replace("||", ",");
                stralert3 = stralert3.Replace("<span style=\"color:Red;\">", "");
                stralert3 = stralert3.Replace("</span>", "");
                stralert2 = stralert2 + stralert3;

                string[] results = stralert2.Split(new[] { ',' });
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", results) + "'.split(','));", true);
                Timer1.Enabled = true;
            }

        }


        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl = (Label)e.Row.FindControl("lblvehicleno");
                DBClass db = new DBClass();
                DataTable dts = db.GetStatus(lbl.Text);
                Label myLabel1 = (Label)e.Row.FindControl("lblstatus");
                DataTable VS = db.GetVehStat(Session["UserID"].ToString(), lbl.Text);
                if (VS.Rows.Count > 0)
                {
                    DataTable status = db.Select_GetVehiclePresentlocation(lbl.Text, Session["UserID"].ToString());
                    int gps = Convert.ToInt32(status.Rows[0][7].ToString());
                    DateTime sysDT = DateTime.Now;
                    DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                    DateTime dayindiffence = StringDT.AddHours(72);
                    StringDT = StringDT.AddMinutes(360);
                    if (sysDT > dayindiffence)
                    {
                        myLabel1.ToolTip = "Submit Complaint Below";//"  ONLINE &nbsp;  ?";

                    }
                    else if (sysDT > StringDT)
                    {
                        myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                    }
                    else if (sysDT <= StringDT)
                    {
                        myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                    }
                    if (gps < 1)
                    {

                        myLabel1.ToolTip = "No GPS is Available";
                    }
                }
                else
                {
                    DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), lbl.Text);
                    DataTable statusoff = db.GetVehiclePresentlocation1(lbl.Text, Session["UserID"].ToString());
                    int gps = Convert.ToInt32(statusoff.Rows[0][7].ToString());
                    DateTime sysDT = DateTime.Now;
                    DateTime StringDT = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                    DateTime dayindiffence = StringDT.AddHours(72);
                    StringDT = StringDT.AddMinutes(360);
                    if (sysDT > dayindiffence)
                    {
                        myLabel1.ToolTip = "Submit Complaint Below";//"  ONLINE &nbsp;  ?";

                    }
                    else if (sysDT > StringDT)
                    {
                        myLabel1.ToolTip = "Power Disconnected Check connections in vehicle";// "  OFFLINE &nbsp; ?";

                    }
                    else if (sysDT <= StringDT)
                    {
                        myLabel1.ToolTip = "Online";//"  ONLINE &nbsp;  ?";

                    }
                    if (gps < 1)
                    {

                        myLabel1.ToolTip = "No GPS is Available";
                    }
                }
                Image img = (Image)e.Row.FindControl("Simg");
                if (dts.Rows.Count > 0)
                {
                    if (dts.Rows[0][1].ToString() == "0")
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                    }
                    else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
                    {
                        img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                    }
                    //if (myLabel1.Text == "Offline")
                    //{
                    //    e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
                    //    DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
                    //    DateTime pdt = DateTime.Now;
                    //    TimeSpan ts = pdt - sdt;
                    //    if (ts.TotalHours >= 24)
                    //    {
                    //        e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
                    //    }
                    //}

                }
                else if (dts.Rows.Count <= 0)
                {
                    DataTable dtsa = db.GetStatusoffff(lbl.Text);
                    if (dtsa.Rows.Count > 0)
                    {
                        if (dtsa.Rows[0][1].ToString() == "0")
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                        }
                        else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) > 3)
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_green.png";
                        }
                        else if (dtsa.Rows[0][1].ToString() == "1" && Convert.ToInt32(dtsa.Rows[0][0].ToString()) <= 3)
                        {
                            img.ImageUrl = "http://labs.google.com/ridefinder/images/mm_20_blue.png";
                        }
                        //if (myLabel1.Text == "Offline")
                        //{
                        //    e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
                        //    DateTime sdt = Convert.ToDateTime(dtsa.Rows[0][2].ToString());
                        //    DateTime pdt = DateTime.Now;
                        //    TimeSpan ts = pdt - sdt;
                        //    if (ts.TotalHours >= 24)
                        //    {
                        //        e.Row.Cells[3].BackColor = System.Drawing.Color.Black;
                        //    }
                        //}
                    }

                }
                else
                {

                }
            }
        }
        protected void btnlovs_Click(object sender, EventArgs e)
        {
            try
            {
                Timer1.Enabled = false;
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;

                List<string> stralerts = new List<string>();
                try
                {
                    int j = 0;
                    string ovss;
                    DBClass db = new DBClass();
                    stralerts.Add("Vehicle no:  ");// + vhno);

                    DataTable vdt = db.vehicledetailsalerts(vhno);
                    if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                    {
                        int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                        // lblovs.Text = "(" + speed1.ToString() + " limit)";
                        speed1 = Convert.ToInt32(speed1 / 1.85);
                        string overspeed = speed1.ToString();
                        DataTable dtdevice = db.getdevicesid(vhno);
                        string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                        string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                        string overspeeddate2 = overspeeddate + " " + "11:59 PM";

                        DataTable dtoverspeed = db.getoverspeeddetails11(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                        if (dtoverspeed.Rows.Count != 0)
                        {
                            for (int i = 0; i < dtoverspeed.Rows.Count; i++)
                            {
                                j++;
                                double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[i][0].ToString());
                                overspeed1 = overspeed1 * 1.85;
                                overspeed1 = (double)Math.Round(overspeed1);
                                string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[i][1]).ToShortTimeString();

                                ovss = Convert.ToString(overspeed1) + " " + "KM/h" + "  at  " + " " + ovsdate;
                                stralerts.Add(ovss);
                            }
                        }
                        else
                        {
                            //stralerts.Add("Vehicle no:  " + vhno);
                            // stralerts.Add(" ");
                            stralerts.Add("No Over Speed");
                        }
                    }
                    else
                    {
                        //stralerts.Add("Vehicle no:  " + vhno);
                        // stralerts.Add(" ");
                        stralerts.Add("No Over Speed");
                    }

                }
                catch (Exception ex) { }

                string[] salerts = new string[stralerts.Count];

                for (int m = 0, n = 1; m < stralerts.Count; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralerts[m].ToString() + vhno;
                    }
                    else
                    {
                        salerts[m] = n + " " + " : " + stralerts[m].ToString();
                        //stralerts.Add(" ");
                        n++;

                    }
                    //salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
                Timer1.Enabled = true;
            }
            catch (Exception ex) { }

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            binggrid();
        }

        protected string alert2(string Vno, int no)
        {
            StringBuilder builder = new StringBuilder();
            try
            {

                int maxtemp = 0, min1 = 0, delay = 0;
                DBClass db = new DBClass();
                DateTime today = DateTime.Now;
                DateTime tomarrow = today.AddDays(1);
                DataTable VS = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
                if (VS.Rows.Count > 0)
                {
                    DateTime tt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                    if (no == 0 && tt < tomarrow)
                    {

                        DataTable gadt = db.vehicledetailsalerts(Vno);
                        if (gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][14].ToString() != "")
                        {
                            maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                            min1 = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                            delay = Convert.ToInt32(gadt.Rows[0][14].ToString());
                            int dttemp = 0;
                            if (maxtemp != 0)
                            {
                                if (maxtemp > 0)
                                {
                                    dttemp = db.Temp_alert2(Vno, min1.ToString(), maxtemp.ToString());
                                }
                                else if (maxtemp < 0)
                                {
                                    maxtemp = maxtemp * -1 + 1000;
                                    dttemp = db.Temp_alert3(Vno, min1.ToString(), maxtemp.ToString());
                                }
                                if (delay <= dttemp)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Temperature");
                                    builder.Append("</span>");
                                    builder.Append("> " + gadt.Rows[0][9].ToString() + "°C ");
                                }
                            }
                        }
                        int vsta = 0, vid = 0, vmng = 0, hours, min;
                        if (VS.Rows[0][8].ToString() == "1" && VS.Rows[0][2].ToString() != "0")
                        {
                            if (gadt.Rows[0][12].ToString() != "" && gadt.Rows[0][12].ToString() != "0")
                            {
                                DataTable pmdt = db.GetPreviMNG(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                vmng = Convert.ToInt32(gadt.Rows[0][12].ToString());
                                hours = vmng / 60;
                                min = vmng % 60;
                                DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                                if (pmdt.Rows.Count != 0)
                                {
                                    DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                                    TimeSpan ts = dtmng - dtmng1;
                                    if (vmng < ts.TotalMinutes)
                                    {
                                        //alerttext3 = lbla2.Text+ Vno + "- Moving > "+hours.ToString()+":"+min.ToString()+" Hr ";
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("||");
                                        builder.Append("</span>");
                                        builder.Append(" " + Vno + "- ");
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("Moving");
                                        builder.Append("</span>");
                                        builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                    }
                                }
                                else
                                {
                                    DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                    DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                                    TimeSpan ts = dtmng - dtmng1;
                                    if (vmng < ts.TotalMinutes)
                                    {
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("||");
                                        builder.Append("</span>");
                                        builder.Append(" " + Vno + "- ");
                                        builder.Append("<span style=\"color:Red;\">");
                                        builder.Append("Moving");
                                        builder.Append("</span>");
                                        builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                    }
                                }
                            }
                        }
                        else
                        {
                            string sft = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][15].ToString();
                            string stt = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][16].ToString();
                            if (gadt.Rows[0][10].ToString() != "" && gadt.Rows[0][10].ToString() != "0" && Convert.ToDateTime(sft) < DateTime.Now && Convert.ToDateTime(stt) > DateTime.Now)
                            {
                                DataTable pdt = db.GetPreviIGST11(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                vsta = Convert.ToInt32(gadt.Rows[0][10].ToString());
                                DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                                hours = vsta / 60;
                                min = vsta % 60;
                                if (pdt.Rows.Count != 0)
                                {
                                    DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                    if (dtst1 > Convert.ToDateTime(sft))
                                    {
                                        TimeSpan ts = dtst - dtst1;
                                        if (vsta < ts.TotalMinutes)
                                        {
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("||");
                                            builder.Append("</span>");
                                            builder.Append(" " + Vno + "- ");
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("Stationary");
                                            builder.Append("</span>");
                                            builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");

                                        }
                                    }
                                    else
                                    {
                                        DataTable dtchk = db.firstigston(Convert.ToDateTime(sft).ToString(), DateTime.Now.ToString(), VS.Rows[0][11].ToString());
                                        TimeSpan ts = DateTime.Now - Convert.ToDateTime(sft);
                                        if (dtchk.Rows.Count == 0 && vsta < ts.TotalMinutes)
                                        {
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("||");
                                            builder.Append("</span>");
                                            builder.Append(" " + Vno + "- ");
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("Stationary");
                                            builder.Append("</span>");
                                            builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                        }
                                    }
                                }
                                else
                                {
                                    DataTable pdt1 = db.GetPreviIGST22(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                                    if (pdt1.Rows.Count != 0)
                                    {
                                        DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                                        TimeSpan ts = dtst - dtst1;
                                        if (vsta < ts.TotalMinutes)
                                        {
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("||");
                                            builder.Append("</span>");
                                            builder.Append(" " + Vno + "- ");
                                            builder.Append("<span style=\"color:Red;\">");
                                            builder.Append("Stationary");
                                            builder.Append("</span>");
                                            builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                        }
                                    }
                                }
                            }
                        }
                        string dnt = DateTime.Now.ToShortDateString();
                        DateTime ftime = Convert.ToDateTime(dnt + " 12:00 AM");
                        DateTime ttime = Convert.ToDateTime(dnt + " 11:59 PM");
                        DataTable Towesdt = db.Towed_Alerts(Vno, "TOWED", ftime.ToString(), ttime.ToString());
                        if (Towesdt.Rows.Count >= 8)
                        {
                            //DateTime dnt1 = Convert.ToDateTime(Towesdt.Rows[0][0].ToString());
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("||");
                            builder.Append("</span>");
                            builder.Append(" " + Vno + "- ");
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("Towed At");
                            builder.Append("</span>");
                            // builder.Append(": " + dnt1.ToShortTimeString());
                        }
                    }
                    else if (no == 1)
                    {
                        DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), Vno);
                        if (VSoff.Rows.Count > 0)
                        {
                            DateTime spdt = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                            DateTime pdt = DateTime.Now;
                            TimeSpan ts = pdt - spdt;
                            if (ts.TotalHours >= 72)
                            {
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Submit Complaint");
                                builder.Append("</span>");
                                //builder.Append(" > 72Hr ");
                            }

                        }
                    }
                }

                else
                {
                    DataTable VSoff = db.GetVehStat1(Session["UserID"].ToString(), Vno);
                    if (VSoff.Rows.Count > 0)
                    {
                        DateTime spdt = Convert.ToDateTime(VSoff.Rows[0][0].ToString());
                        DateTime pdt = DateTime.Now;
                        TimeSpan ts = pdt - spdt;
                        if (ts.TotalHours >= 72)
                        {
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("||");
                            builder.Append("</span>");
                            builder.Append(" " + Vno + "- ");
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("Submit Complaint");
                            builder.Append("</span>");
                            //builder.Append(" > 72Hr ");
                        }

                    }
                }


            }
            catch (Exception ex)
            {
            }
            return builder.ToString();
        }

    }
}


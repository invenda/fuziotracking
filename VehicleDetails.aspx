﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleDetails.aspx.cs"
    Inherits="Tracking.VehicleDetails" MasterPageFile="~/TrackingMaster.Master" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="sublogo">
            <h1>
                <a href="#">Vehicle Registration Details</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%--<a href="#"><asp:LinkButton ID="lnkbtnVehicle" runat="server" 
                    Text="Vehicle Details" Visible="false"  ></asp:LinkButton></a>--%><%--onclick="lnkbtnVehicle_Click"--%>
            </h1>
        </div>
    </div>
    <div>
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table width="100%">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="color: White; font-size: large">
                    Vehicle Details
                </td>
                <td style="color: White; font-size: large">
                    Driver Details
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Registration Number:&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtVehRegNum"
                        runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Driver Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtDriverName" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Model:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtVehModel" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Driver Mobile Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDriverMobNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Make:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtVehMake" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtDriverADD" runat="server" Height="50px" Width="192px" TextMode="MultiLine"
                        MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Insurance Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtVehInsNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Driving Licence Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtDrivLicNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Insurance Expiry Date:&nbsp;&nbsp;<asp:TextBox ID="txtVehInsExpDt" runat="server"
                        Height="15px" Width="192px"></asp:TextBox>
                    &nbsp;<cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                        TargetControlID="txtVehInsExpDt">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    Driving Licence Expiry Date:&nbsp;&nbsp;<asp:TextBox ID="txtDrivLic" runat="server"
                        Height="15px" Width="192px"></asp:TextBox>
                    &nbsp;<cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                        TargetControlID="txtDrivLic">
                    </cc1:CalendarExtender>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="color: White; font-size: large">
                    SIM Details
                </td>
                <td style="color: White; font-size: large">
                    Device Details
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtMobNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Serial Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                        ID="txtSNum" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    SIM Provider:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtSIMProvider" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
                <td>
                    Date of Purchase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDOP" runat="server" Height="15px" Width="192px"></asp:TextBox>
                    &nbsp;<cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy"
                        TargetControlID="txtDOP">
                    </cc1:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    GPRS Activated Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtGPRSActDt" runat="server" Height="15px" Width="192px"></asp:TextBox>
                    &nbsp;<cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy"
                        TargetControlID="txtGPRSActDt">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    Dealer Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDealerName" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Recharge Period:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                        ID="rbtnMonthly" runat="server" Text="Monthly" GroupName="Group1"></asp:RadioButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtnYearly" runat="server" Text="Yearly" GroupName="Group1">
                    </asp:RadioButton>
                </td>
                <%--<td>
                    Device ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtDeviceID" runat="server" Height="15px" Width="192px"></asp:TextBox>
                </td>--%>
            </tr>
            <%--<tr>
                <td>
                    Transmission Frequency:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtTransFreq" runat="server" Height="15px" Width="192px" Text="1"
                        Enabled="false"></asp:TextBox>&nbsp;&nbsp;in minutes
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>--%>
        </table>
    </div>
</asp:Content>

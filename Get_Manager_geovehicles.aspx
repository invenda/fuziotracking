﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Get_Manager_geovehicles.aspx.cs"
    MasterPageFile="~/Manager_geo.Master" Inherits="Tracking.Get_Manager_geovehicles" %>

<%@ Register Assembly="GMaps" Namespace="Subgurim.Controles" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center; color: Blue; font-size: x-large; font-style: italic;">
      <asp:Label ID="lblheading" runat="server" ></asp:Label>
    </div>
    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePageMethods="True">
    </asp:ScriptManager>
    <div style="width: 700px;  height:25px; margin: 0 auto; padding: 0; padding: 0; height: 26px; text-align:center">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Italic="true" Font-Bold="true" Font-Size ="Large"></asp:Label>
     </div>
    <div style="width: 700px; margin: 0 auto; padding: 0; padding: 0; height: auto;">
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="False" Width="99%"
                        HeaderStyle-BackColor="#0099ff" HeaderStyle-ForeColor="Black" ForeColor="Black"
                        BackColor="#FBF6D9" ShowFooter="True">
                        <AlternatingRowStyle BorderColor="#990099" />
                        <Columns>
                            <asp:TemplateField HeaderText="S.no">
                                <ItemTemplate>
                                    <asp:Label ID="lblsno" CommandName="AddToCart" runat="server" Text='<%# Bind("s_no") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="70px" />
                                <ItemStyle HorizontalAlign="Center" Font-Bold="False" Width="100px" />
                            </asp:TemplateField>                           
                            <asp:TemplateField HeaderText="Vehicle No">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicle" CommandName="AddToCart" runat="server" Text='<%# Bind("Vehicle_no") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <HeaderStyle Width="100px" />
                               <%-- <FooterTemplate>
                                    <asp:Label ID="lbltotal" runat="server" Text="Total Vehicles:" Font-Bold="True"></asp:Label>
                                </FooterTemplate>--%>
                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUname" CommandName="AddToCart" runat="server" Text='<%# Bind("user_name") %>'
                                        Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal12" runat="server" Font-Bold="True"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" />
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Width="50px" />
                            </asp:TemplateField>                           
                        </Columns>
                        <FooterStyle BackColor="#3399FF" />
                    </asp:GridView>
                </td>              
            </tr>
            <tr>
                <td align="center" style="height: 20px;">
                    <asp:Button ID="btnmapview" runat="server" Text="View in Map" BackColor="Aqua" 
                        onclick="btnmapview_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

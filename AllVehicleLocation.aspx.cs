﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;
using System.Threading;
using System.IO;

namespace Tracking
{
    public partial class AllVehicleLocation : System.Web.UI.Page
    {
        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";

        DBClass db = new DBClass();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Binddata();
            }
        }

        private void Binddata()
        {
            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];

            DataTable stat = new DataTable();
            stat.Columns.Add("Username", typeof(string));
            stat.Columns.Add("Vno", typeof(string));
            stat.Columns.Add("Deviceno", typeof(string));
            //stat.Columns.Add("Lastdata", typeof(string));
            stat.Columns.Add("Location", typeof(string));

            DataTable users = db.getmanagerusers(Session["UserID"].ToString());
            if (users.Rows.Count > 0)
            {

                for (int i = 0; i < users.Rows.Count; i++)
                {
                    DataTable uvehicles = db.Select_GetVehicles(users.Rows[i][1].ToString());
                    if (uvehicles.Columns.Count > 0)
                    {
                        for (int j = 0; j < uvehicles.Rows.Count; j++)
                        {
                            DataRow dr = stat.NewRow();
                            string uname = users.Rows[i][0].ToString();
                            string VN = uvehicles.Rows[j][0].ToString();
                            string location = "";
                            string deviceno = "";
                            // string Lastdata = "";

                            DataTable status = db.Select_GetVehiclePresentlocation(VN, Session["UserID"].ToString());
                            if (status.Rows.Count != 0)
                            {
                                deviceno = status.Rows[0][11].ToString();

                                //lastdata
                                //  DateTime fff = Convert.ToDateTime(status.Rows[0][4].ToString());
                                // Lastdata = fff.ToString("dd/MM/yyyy hh:mm tt");

                                //Location
                                try
                                {
                                    /* GeoCode objAddress = new GeoCode();
                                     objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                                     StringBuilder sb = new StringBuilder();
                                     if (objAddress.valid)
                                     {
                                         sb.Append(objAddress.Placemark.address.ToString());
                                         location = sb.ToString();
                                     }*/

                                    string latitude = status.Rows[0][0].ToString();
                                    string longitude = status.Rows[0][1].ToString();
                                    System.Net.WebClient client = new System.Net.WebClient();

                                    string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                    string response = client.DownloadString(urlstring);
                                    dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                    if (jsonresponse.results.Count > 0)
                                    {
                                        string formatted_address = jsonresponse.results[0].formatted_address;
                                        location = formatted_address;
                                    }

                                }
                                catch (Exception ex) { }
                            }
                            else
                            {
                                DataTable statusoff = db.GetVehiclePresentlocation1(VN, Session["UserID"].ToString());
                                if (statusoff.Rows.Count > 0)
                                {
                                    deviceno = statusoff.Rows[0][11].ToString();

                                    //lastdata
                                    // DateTime fff = Convert.ToDateTime(statusoff.Rows[0][4].ToString());
                                    // Lastdata = fff.ToString("dd/MM/yyyy hh:mm tt");

                                    //Location
                                    try
                                    {
                                        /* GeoCode objAddress = new GeoCode();
                                         objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(statusoff.Rows[0][0]), Convert.ToDouble(statusoff.Rows[0][1])), sMapKey);
                                         StringBuilder sb = new StringBuilder();
                                         if (objAddress.valid)
                                         {
                                             sb.Append(objAddress.Placemark.address.ToString());
                                             location = sb.ToString();
                                         }*/
                                        string latitude = statusoff.Rows[0][0].ToString();
                                        string longitude = statusoff.Rows[0][1].ToString();
                                        System.Net.WebClient client = new System.Net.WebClient();

                                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                        string response = client.DownloadString(urlstring);
                                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                        if (jsonresponse.results.Count > 0)
                                        {
                                            string formatted_address = jsonresponse.results[0].formatted_address;
                                            location = formatted_address;
                                        }
                                    }
                                    catch (Exception ex) { }
                                }
                            }
                            dr["Username"] = uname;
                            dr["Vno"] = VN;
                            dr["Deviceno"] = deviceno;
                            //dr["Lastdata"] = Lastdata;
                            dr["Location"] = location;
                            stat.Rows.Add(dr);
                        }
                    }
                }

            }
            if (stat.Rows.Count > 0)
            {
                btnDownload.Enabled = true;
            }
            GV.DataSource = stat;
            GV.DataBind();

        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (GV.Rows.Count > 0)
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                string str1 = DateTime.Now.ToShortDateString();
                //string device = ddldevice_history.SelectedValue.ToString();
                string str2 = "ALl vehicle location" + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(GV);
                tr1.Cells.Add(cell1);
                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                //Response.Write(style);
                //Response.Write(hw.ToString());
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void GV_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Requested By:" + Session["UserName"].ToString();
                HeaderCell.ColumnSpan = 3;
                HeaderCell.Font.Bold = true;

                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested date time :" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                HeaderCell.ColumnSpan = 1;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                GV.Controls[0].Controls.AddAt(0, HeaderGridRow);
            }
        }

    }


}
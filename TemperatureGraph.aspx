

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemperatureGraph.aspx.cs"
    Inherits="Tracking.TemperatureGraph" MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report3" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
        function WaterMark(txtName, event) {
            var defaultText = "Select Date";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        } 
  
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: 930px; background-color: White;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="width: 100%; height: 40px; font-size: 25px; text-align: center; color: Blue;">
                    <asp:Label ID="lblhead" Text="Vehicle Temperature Graphs" runat="server"></asp:Label></div>
                <div style="width: 100%; height: 40px;">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ValidationGroup="Group1" ShowSummary="False" />
                </div>
                <div>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 200px;">
                                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 150px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 100px;">
                                <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>:
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                                    ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 130px;">
                                <asp:TextBox ID="date" runat="server" Width="120px" ForeColor="Gray"  Text="Select Date" onblur = "WaterMark(this, event);" onfocus = "WaterMark(this, event);"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                                <%--<asp:Label ID="lblErrorMsg" runat="server" Text="*You cannot select a day earlier than today!"
                    Visible="false" ForeColor="Red"></asp:Label>--%>
                            </td>
                            <td style="width: 100px;" align="right">
                                <asp:Label ID="Label2" Text="Chart Type" runat="server" Font-Bold="true"></asp:Label>:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="rbttype" ErrorMessage="- Select Type">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 150px;">
                                <asp:RadioButtonList ID="rbttype" runat="server" Style="margin-left: 0px">
                                    <asp:ListItem Text="12 Hours" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="24 Hours" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 100px;">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                    OnClick="GetRecord_Click" />
                            </td>
                            <td style="width: 120px;" align="center">
                                <%-- <input type="button" value="Print 1st Div" onclick="javascript:printDiv('Printdiv')" />
                       <input type="button" value="PrintDiv1" onclick="Print()" />--%>
                                <asp:Button ID="Download" Text="Download" runat="server" Width="70px" OnClick="Download_Click"
                                    ValidationGroup="Group1" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 100%; height: 30px;">
                </div>
                <div style="height: auto; text-align: center; color: #FE9A2E;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <h1>
                                                <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                            </h1>
                                        </td>
                                        <td>
                                            <h3>
                                                Please wait.....
                                            </h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="false">
                    <%-- <asp:Panel ID="Panel1" runat="server" Height="650px">--%>
                    <div id="Printdiv">
                        <table style="width: 100%; height: 100%; text-align: center">
                            <tr>
                                <td style="width: 100%; height: 50px;">
                                    <table style="width: 100%; height: 50px;">
                                        <tr>
                                            <td style="width: 50px;">
                                            </td>
                                            <td>
                                                <asp:Label ID="lblVehrtono" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblreqtime" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltemp" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td style="width: 50px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px;">
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltime" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldriname" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldrimob" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="width: 50px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="width: 100%; height: 500px;">
                                    <asp:Chart ID="Chart1" runat="server" Height="650px" Width="1200px" ImageLocation="~/tempImages/ChartPic_#SEQ(300,3)"
                                        Palette="Pastel" BorderDashStyle="Solid" BorderWidth="1" BackColor="#213258">
                                        <BorderSkin BorderWidth="0" SkinStyle="Emboss" />
                                        <Titles>
                                            <asp:Title Docking="Left" Font="Trebuchet MS, 10.25pt" Text="Temperature �C" BackColor="Transparent"
                                                DockingOffset="0" ForeColor="White">
                                            </asp:Title>
                                            <asp:Title Docking="Bottom" Font="Trebuchet MS, 10.25pt" BackColor="Transparent"
                                                DockingOffset="2" ForeColor="White">
                                            </asp:Title>
                                            <asp:Title Docking="Bottom" Font="Trebuchet MS, 10.25pt" BackColor="Transparent"
                                                Alignment="BottomRight" DockedToChartArea="ChartArea1" DockingOffset="1" ForeColor="Black">
                                            </asp:Title>
                                            <asp:Title Docking="Bottom" Font="Trebuchet MS, 10.25pt" BackColor="Transparent"
                                                Alignment="BottomRight" DockedToChartArea="ChartArea1" DockingOffset="2" ForeColor="Black">
                                            </asp:Title>
                                        </Titles>
                                        <Legends>
                                            <%--<asp:Legend LegendStyle="Row" IsTextAutoFit="False" DockedToChartArea="ChartArea1"
                                    Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8.25pt"
                                    ForeColor="Black" Alignment="Far">
                                </asp:Legend>--%>
                                            <asp:Legend Docking="Bottom" Name="Default" BackColor="Transparent" DockedToChartArea="ChartArea1"
                                                Enabled="true" Font="Trebuchet MS, 7.25pt" ForeColor="Black" Alignment="Near">
                                            </asp:Legend>
                                        </Legends>
                                        <Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series1" ChartType="SplineArea" BorderWidth="2"
                                                IsVisibleInLegend="false" Color="#9FB6CD">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series2" ChartType="Line" BorderColor="180, 26, 59, 105"
                                                IsVisibleInLegend="true" LegendText="Alert Temp�C" Color="Blue" BorderWidth="5">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series3" ChartType="Line" BorderColor="180, 26, 59, 105"
                                                IsVisibleInLegend="false" BorderWidth="2" Color="Black">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series4" ChartType="SplineArea" IsVisibleInLegend="false"
                                                Color="#CC9933">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series5" ChartType="SplineArea" IsVisibleInLegend="false"
                                                Color="#9FB6CD">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series6" ChartType="Line" BorderColor="180, 26, 59, 105"
                                                IsVisibleInLegend="true" LegendText="Min Temp�C" Color="Chocolate" BorderWidth="5">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series7" ChartType="Stock" BorderWidth="5"
                                                IsVisibleInLegend="false" Color="Yellow">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series8" ChartType="Stock" BorderWidth="5"
                                                IsVisibleInLegend="false" Color="Black">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series9" ChartType="Stock" BorderWidth="5"
                                                IsVisibleInLegend="true" LegendText="Vehicle Stationary" Color="Red">
                                            </asp:Series>
                                            <asp:Series ChartArea="ChartArea1" Name="Series10" ChartType="Stock" BorderWidth="5"
                                                IsVisibleInLegend="true" LegendText="Vehicle Moving" Color="Green">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                                                BackColor="#FFF0F5" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                <AxisY2 Enabled="False">
                                                </AxisY2>
                                                <AxisX2 Enabled="True" LineColor="64, 64, 64, 64">
                                                    <MajorGrid LineColor="64, 64, 64, 64" />
                                                    <LabelStyle Font="Trebuchet MS, 14.25pt, style=Bold" ForeColor="Black" />
                                                </AxisX2>
                                                <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                                                    WallWidth="0" IsClustered="False"></Area3DStyle>
                                                <AxisY LineColor="64, 64, 64, 64" Maximum="50" Minimum="-30" IsLabelAutoFit="false"
                                                    TitleForeColor="White" Crossing="0" Interval="5">
                                                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White" />
                                                    <MajorGrid LineColor="64, 64, 64, 64" />
                                                </AxisY>
                                                <AxisX LineColor="64, 64, 64, 64" IntervalAutoMode="FixedCount" TitleForeColor="White"
                                                    TitleFont="Trebuchet MS, 10.25pt">
                                                    <MajorGrid LineColor="64, 64, 64, 64" />
                                                    <CustomLabels>
                                                        <asp:CustomLabel Text="" FromPosition="0" ToPosition="30"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="1" FromPosition="50" ToPosition="60"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="2" FromPosition="110" ToPosition="120"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="3" FromPosition="170" ToPosition="180"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="4" FromPosition="230" ToPosition="240"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="5" FromPosition="290" ToPosition="300"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="6" FromPosition="350" ToPosition="360"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="7" FromPosition="410" ToPosition="420"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="8" FromPosition="470" ToPosition="480"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="9" FromPosition="530" ToPosition="540"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="10" FromPosition="580" ToPosition="610"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="11" FromPosition="640" ToPosition="670"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="12" FromPosition="700" ToPosition="730"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="13" FromPosition="770" ToPosition="800"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="14" FromPosition="830" ToPosition="860"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="15" FromPosition="890" ToPosition="920"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="16" FromPosition="950" ToPosition="980"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="17" FromPosition="1010" ToPosition="1040"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="18" FromPosition="1070" ToPosition="1100"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="19" FromPosition="1130" ToPosition="1160"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="20" FromPosition="1190" ToPosition="1220"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="21" FromPosition="1250" ToPosition="1280"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="22" FromPosition="1310" ToPosition="1340"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="23" FromPosition="1370" ToPosition="1400"></asp:CustomLabel>
                                                        <asp:CustomLabel Text="24" FromPosition="1410" ToPosition="1450"></asp:CustomLabel>
                                                    </CustomLabels>
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                    <%-- <asp:Chart ID="Chart1" runat="server" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)"
                            ImageType="Png" BackColor="WhiteSmoke" BorderWidth="2" BackGradientStyle="TopBottom"
                            BackSecondaryColor="White" Palette="BrightPastel" BorderDashStyle="Solid" BorderColor="26, 59, 105"
                            Height="500px" Width="950px" IsMapAreaAttributesEncoded="True">
                            <Legends>
                                <asp:Legend Enabled="False" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                                    Font="Trebuchet MS, 8.25pt, style=Bold">
                                </asp:Legend>
                            </Legends>
                            <BorderSkin SkinStyle="Emboss"></BorderSkin>
                            <Series>
                                <asp:Series ChartArea="ChartArea1" XValueType="DateTime" Name="Series1" ChartType="FastLine"
                                    ShadowColor="Black" BorderColor="180, 26, 59, 105" IsValueShownAsLabel="true">
                                </asp:Series>
                                <asp:Series Name="Series2" ChartType="Spline" ChartArea="ChartArea1" BorderColor="180, 26, 59, 105" Color="Red" XValueType="DateTime" XAxisType="Secondary"
                    YValueType="Double" BorderWidth="5" ShadowColor="254, 0, 0, 0" ShadowOffset="1">
                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                                    BackSecondaryColor="White" BackColor="Gainsboro" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                    <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                                        WallWidth="0" IsClustered="False"></Area3DStyle>
                                    <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Minimum="-30" Maximum="50"
                                        Interval="5">
                                        <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />
                                    </AxisY>
                                    <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="true" IntervalAutoMode="FixedCount"
                                        Interval="Auto" IsStartedFromZero="true" IntervalType="Hours" ArrowStyle="SharpTriangle"
                                        IntervalOffsetType="Hours">
                                        <LabelStyle Font="Trebuchet MS, 6.75pt, style=Bold" />
                                        <MajorGrid LineColor="64, 64, 64, 64" />--%>
                                    <%--<CustomLabels>
                                            <asp:CustomLabel Text="12:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="1:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="2:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="3:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="4:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="5:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="6:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="7:00 AM"></asp:CustomLabel>
                                            <asp:CustomLabel Text="8:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="9:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="10:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="11:00 AM" ></asp:CustomLabel>
                                            <asp:CustomLabel Text="12:00 PM" ></asp:CustomLabel>
                                        </CustomLabels>--%>
                                    <%--     </AxisX>
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 100%; height: 50px;">
                        <table style="width: 100%; height: 100%;">
                            <tr>
                                <td valign="top" style="width: 100%; height: 10px; text-align: right;">
                                    <asp:LinkButton ID="linknext" runat="server" Font-Bold="true" Text="Next >>" Font-Underline="false"
                                        ForeColor="Blue" OnClick="linknext_Click" ValidationGroup="Group1"></asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 100%; height: 10px; text-align: left;">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="true" Text="<< Previous"
                                        Visible="false" Font-Underline="false" ForeColor="Blue" OnClick="LinkButton1_Click"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Download" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

﻿.<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Tracking.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Login - Welcome to ESlabs Vehicle Tracking </title>
    <link rel="SHORTCUT ICON" href="Images/favicon.ico" />
    <link rel="stylesheet" href="CSS/login.css" type="text/css" />

    <script language="javascript" type="text/javascript">
        function openTERMS()
        {
            window.open("Terms.aspx");
        }
        var bpPopup;function popUp(url, width, height)
{

bpPopup = window.open(url, "bpPopup", 'left=162px, top=134px, width=999px, height=500px, status=no, resizable= no, scrollbars=yes, toolbar=no, location=no, menubar=no');
bpPopup.focus();

}

    </script>

    <style type="text/css">
        #strip
        {
            height: 162px;
        }
        #Div1
        {
            height: 45px;
        }
    </style>
</head>
<body style="background-color: White">




 <form id="Form1" name="form1" runat="server">
    <div id="strip" style="height: 170px;">
        <table width="100%" style="height: 170px; border-spacing: 0px 0px;">
            <tr style="height: 170px; border-spacing: 0;">
                <td align="right" valign="bottom" style="background-color: #2F2F4F; height: 170px;
                    border-spacing: 0px; width: 42%">
                    <asp:Label ID="lbl1" runat="server" Text="Powered By" ForeColor="White" Font-Size="10"></asp:Label>&nbsp;
                </td>
               <td style="background-color: #2F2F4F; height: 170px; border-spacing: 0px;">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ESL1.jpg" Height="176px"
                        BorderColor="White" Width="165px" />
                </td>
            </tr>
        </table>
    </div>
    <div id='parent' style="border-spacing: 0px 0px;">
        <div class='child child_left'>
        </div>
  <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/MaskOne.JPG" ImageAlign="Left"
            Height="203px" Width="595px" /><br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/words.JPG" ImageAlign="Middle"
            Height="155px" Width="400px" />
        <div class='child child_right'>
            <table style="margin: 0 auto; padding: 0;">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Login Name:" ForeColor="Black" 
                            Font-Size="14px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="user_login" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="user_login">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Password:" ForeColor="Black" Font-Size="14px"></asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="user_pass" runat="server" TextMode="Password" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="user_pass">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Login Type:" ForeColor="Black" 
                            Font-Size="14px"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="RbtUser" runat="server" RepeatDirection="Horizontal" Width="150px">
                            <asp:ListItem Text="User" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Manager" Value="3"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RbtUser">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="left">
                        <asp:Button ID="btnLogin" Text="Login »" runat="server" OnClick="login_click" />
                        <asp:Label ID="lblerror" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <asp:CheckBox ID="chkbxACCEPT" runat="server" AutoPostBack="true" />
            &nbsp;&nbsp; I Accept, that I have fully read, understood </br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&
            agree to abide by all
          
            <asp:HyperLink ID="HyperLinkTerms" runat="server" Text="Terms & Conditions" ForeColor="Red"
                Font-Underline="true" NavigateUrl="#" onclick="popUp('Terms.aspx', '100','100')"></asp:HyperLink>
        </div>
    </div>
    <div id="Div1">
        <br />
        <table width="100%" style="border-spacing: 0px 0px;">
            <br />
            <br />
            <tr>
                <td align="center" style="background-color: #2F2F4F; height: 60px; border-bottom-color: White;">
                    <p align="center" style="color: White">
                        © ESlabs Vehicle Tracking 2011
                              </p>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

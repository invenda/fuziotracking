﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.IO;


namespace Tracking
{
    public partial class Fulefillingreport : System.Web.UI.Page
    {
        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?latlng={0}&key={1}";

        DBClass db = new DBClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();

                        ddlFromHOURS.DataSource = db.GetFromHours(Session["cUser_id"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["cUser_id"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["cUser_id"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["cUser_id"].ToString());
                        ddlToMINUTES.DataBind();
                        btndownload.Enabled = false;

                    }
                    else
                    {

                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();

                        ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlFromHOURS.DataBind();

                        ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
                        ddlFromMINUTES.DataBind();

                        ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                        ddlToHOURS.DataBind();

                        ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                        ddlToMINUTES.DataBind();
                        btndownload.Enabled = false;
                    }
                }
            }

        }


        public void Binddata(string fdate, string tdate, string Vno)
        {
            //DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
            //if (fuelim.Rows[0][1].ToString() == "Yes")
            //{
            DataTable fff = new DataTable();
            fff.Columns.Add("GTIM", typeof(string));
            fff.Columns.Add("Openingfuel", typeof(string));
            fff.Columns.Add("Fuelfilled", typeof(string));
            fff.Columns.Add("Totalfuel", typeof(string));
            fff.Columns.Add("Adress", typeof(string));

            string key = ConfigurationManager.AppSettings.Get("googlemaps.subgurim.net");
            Subgurim.Controles.GLatLng gLatLng0 = new Subgurim.Controles.GLatLng();
            double lt1 = 0, lg1 = 0;

            DataTable dtf = db.GetVehiclesfuel(Vno, Session["UserId"].ToString());
            if (dtf.Rows[0][2].ToString() != "0" && dtf.Rows[0][4].ToString() != "0")
            {
                double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                double Fulltankvalue = Convert.ToDouble(dtf.Rows[0][2].ToString());
                double Emptytankvalue = Convert.ToDouble(dtf.Rows[0][4].ToString());
                // double persent = (tank_capacity * (0.08));

                if (Tank_typ == 0)
                {
                    double fulltank12 = Fulltankvalue;
                    double fulltank123 = Fulltankvalue - 30.0;

                    double xxx = (Fulltankvalue - Emptytankvalue) / 4;
                    double e = 0.0;
                    DateTime dhdh = Convert.ToDateTime(fdate);
                    int emonth = dhdh.Month;
                    DateTime curretmonth = DateTime.Now;
                    int cmonth = curretmonth.Month;
                    if (emonth == cmonth)
                    {
                        DataTable dt = db.Select_Fueldata(Vno, fdate, tdate);
                        if (dt.Rows.Count > 4)
                        {
                            for (int i = 0, j = 1, k = 2, l = 3, m = 4; i < dt.Rows.Count - 4; i++, j++, k++, l++, m++)
                            {
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double string_val_fuel222 = Convert.ToDouble(dt.Rows[j][3].ToString());
                                double string_val_fuel333 = Convert.ToDouble(dt.Rows[k][3].ToString());
                                double string_val_fuel444 = Convert.ToDouble(dt.Rows[l][3].ToString());
                                double string_val_fuel555 = Convert.ToDouble(dt.Rows[m][3].ToString());

                                if (string_val_fuel333 >= fulltank123 && string_val_fuel333 <= fulltank12 && string_val_fuel444 >= fulltank123 && string_val_fuel444 <= fulltank12 && string_val_fuel555 >= fulltank123 && string_val_fuel555 <= fulltank12)
                                {
                                    double x = Fulltankvalue - Emptytankvalue;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel333 - Emptytankvalue;
                                    double b = z * y;
                                    b = (double)Math.Round(b);

                                    if (string_val_fuel <= string_val_fuel333 - xxx && string_val_fuel222 <= string_val_fuel333 - xxx)
                                    {
                                        double x1 = Fulltankvalue - Emptytankvalue;
                                        double y1 = tank_capacity / x;
                                        double z1 = string_val_fuel222 - Emptytankvalue;
                                        double b1 = z1 * y1;
                                        e = (double)Math.Round(b1);

                                        /*gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[k][0]), Convert.ToDouble(dt.Rows[k][1]));
                                        lt1 = gLatLng0.lat;
                                        lg1 = gLatLng0.lng;
                                        GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                        GMap gMap1 = new GMap();
                                        gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                        StringBuilder sb = new StringBuilder();
                                        if ((null != geocode) && geocode.valid)
                                        {
                                            sb.AppendFormat(geocode.Placemark.address);
                                        }*/

                                        string latitude = dt.Rows[k][0].ToString();
                                        string longitude = dt.Rows[k][1].ToString();

                                        System.Net.WebClient client = new System.Net.WebClient();

                                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                        string response = client.DownloadString(urlstring);
                                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                        if (jsonresponse.results.Count > 0)
                                        {
                                            string formatted_address = jsonresponse.results[0].formatted_address;
                                            dr["Adress"] = formatted_address;
                                        }

                                        double filledfuel = b - e;

                                        DateTime filldate = Convert.ToDateTime(dt.Rows[k][2].ToString());

                                        //  dr["Adress"] = onadd;
                                        dr["GTIM"] = filldate.ToString("dd/MM/yyyy hh:mm tt");
                                        dr["Fuelfilled"] = filledfuel.ToString();
                                        dr["Totalfuel"] = b.ToString();
                                        dr["Openingfuel"] = e.ToString();
                                        fff.Rows.Add(dr);
                                    }
                                }
                                //double x = Fulltankvalue - Emptytankvalue;
                                //double y = tank_capacity / x;
                                //double z = string_val_fuel - Emptytankvalue;
                                //double b = z * y;
                                //b = (double)Math.Round(b);
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = db.Select_Fueldataold(Vno, fdate, tdate);
                        if (dt.Rows.Count > 4)
                        {
                            for (int i = 0, j = 1, k = 2, l = 3, m = 4; i < dt.Rows.Count - 4; i++, j++, k++, l++, m++)
                            {
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double string_val_fuel222 = Convert.ToDouble(dt.Rows[j][3].ToString());
                                double string_val_fuel333 = Convert.ToDouble(dt.Rows[k][3].ToString());
                                double string_val_fuel444 = Convert.ToDouble(dt.Rows[l][3].ToString());
                                double string_val_fuel555 = Convert.ToDouble(dt.Rows[m][3].ToString());


                                if (string_val_fuel333 >= fulltank123 && string_val_fuel333 <= fulltank12 && string_val_fuel444 >= fulltank123 && string_val_fuel444 <= fulltank12 && string_val_fuel555 >= fulltank123 && string_val_fuel555 <= fulltank12)
                                {
                                    double x = Fulltankvalue - Emptytankvalue;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel333 - Emptytankvalue;
                                    double b = z * y;
                                    b = (double)Math.Round(b);

                                    if (string_val_fuel <= string_val_fuel333 - xxx && string_val_fuel222 <= string_val_fuel333 - xxx)
                                    {
                                        double x1 = Fulltankvalue - Emptytankvalue;
                                        double y1 = tank_capacity / x;
                                        double z1 = string_val_fuel222 - Emptytankvalue;
                                        double b1 = z1 * y1;
                                        e = (double)Math.Round(b1);

                                        /*gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[k][0]), Convert.ToDouble(dt.Rows[k][1]));
                                        lt1 = gLatLng0.lat;
                                        lg1 = gLatLng0.lng;
                                        GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                        GMap gMap1 = new GMap();
                                        gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                        StringBuilder sb = new StringBuilder();
                                        if ((null != geocode) && geocode.valid)
                                        {
                                            sb.AppendFormat(geocode.Placemark.address);
                                        }*/
                                        string latitude = dt.Rows[k][0].ToString();
                                        string longitude = dt.Rows[k][1].ToString();

                                        System.Net.WebClient client = new System.Net.WebClient();

                                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                        string response = client.DownloadString(urlstring);
                                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                        if (jsonresponse.results.Count > 0)
                                        {
                                            string formatted_address = jsonresponse.results[0].formatted_address;
                                            dr["Adress"] = formatted_address;
                                        }

                                        double filledfuel = b - e;

                                        DateTime filldate = Convert.ToDateTime(dt.Rows[k][2].ToString());

                                        // dr["Adress"] = onadd;
                                        dr["GTIM"] = filldate.ToString("dd/MM/yyyy hh:mm tt");
                                        dr["Fuelfilled"] = filledfuel.ToString();
                                        dr["Totalfuel"] = b.ToString();
                                        dr["Openingfuel"] = e.ToString();
                                        fff.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    double fulltank12 = Fulltankvalue + 20.0;
                    double fulltank123 = Fulltankvalue;
                    //
                    double xxx = (Emptytankvalue - Fulltankvalue) / 4;

                    DateTime dhdh = Convert.ToDateTime(fdate);
                    int emonth = dhdh.Month;
                    DateTime curretmonth = DateTime.Now;
                    int cmonth = curretmonth.Month;
                    if (emonth == cmonth)
                    {
                        DataTable dt = db.Select_Fueldata(Vno, fdate, tdate);
                        if (dt.Rows.Count > 5)
                        {
                            double e = 0.0;
                            for (int i = 0, j = 1, k = 2, l = 3, m = 4; i < dt.Rows.Count - 4; i++, j++, k++, l++, m++)
                            {
                                DataRow dr = fff.NewRow();
                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double string_val_fuel222 = Convert.ToDouble(dt.Rows[j][3].ToString());
                                double string_val_fuel333 = Convert.ToDouble(dt.Rows[k][3].ToString());
                                double string_val_fuel444 = Convert.ToDouble(dt.Rows[l][3].ToString());
                                double string_val_fuel555 = Convert.ToDouble(dt.Rows[m][3].ToString());


                                if (string_val_fuel333 >= fulltank123 && string_val_fuel333 <= fulltank12 && string_val_fuel444 >= fulltank123 && string_val_fuel444 <= fulltank12 && string_val_fuel555 >= fulltank123 && string_val_fuel555 <= fulltank12)
                                {
                                    double x = Emptytankvalue - Fulltankvalue;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel333 - Fulltankvalue;
                                    double b = z * y;
                                    double f = tank_capacity - b;
                                    f = (double)Math.Round(f);


                                    if (string_val_fuel >= string_val_fuel333 + xxx && string_val_fuel222 >= string_val_fuel333 + xxx)
                                    {

                                        double x1 = Emptytankvalue - Fulltankvalue;
                                        double y1 = tank_capacity / x;
                                        double z1 = string_val_fuel222 - Fulltankvalue;
                                        double b1 = z1 * y1;
                                        e = tank_capacity - b1;
                                        e = (double)Math.Round(e);

                                        /* gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[k][0]), Convert.ToDouble(dt.Rows[k][1]));
                                         lt1 = gLatLng0.lat;
                                         lg1 = gLatLng0.lng;
                                         GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                         GMap gMap1 = new GMap();
                                         gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                         StringBuilder sb = new StringBuilder();
                                         if ((null != geocode) && geocode.valid)
                                         {
                                             sb.AppendFormat(geocode.Placemark.address);
                                         }*/
                                        string latitude = dt.Rows[k][0].ToString();
                                        string longitude = dt.Rows[k][1].ToString();

                                        System.Net.WebClient client = new System.Net.WebClient();

                                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                        string response = client.DownloadString(urlstring);
                                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                        if (jsonresponse.results.Count > 0)
                                        {
                                            string formatted_address = jsonresponse.results[0].formatted_address;
                                            dr["Adress"] = formatted_address;
                                        }

                                        double filledfuel = f - e;

                                        DateTime filldate = Convert.ToDateTime(dt.Rows[k][2].ToString());

                                        // dr["Adress"] = onadd;
                                        dr["GTIM"] = filldate.ToString("dd/MM/yyyy hh:mm tt");
                                        dr["Fuelfilled"] = filledfuel.ToString();
                                        dr["Totalfuel"] = f.ToString();
                                        dr["Openingfuel"] = e.ToString();
                                        fff.Rows.Add(dr);
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = db.Select_Fueldataold(Vno, fdate, tdate);
                        if (dt.Rows.Count > 5)
                        {
                            double e = 0.0;
                            for (int i = 0, j = 1, k = 2, l = 3, m = 4; i < dt.Rows.Count - 4; i++, j++, k++, l++, m++)
                            {
                                DataRow dr = fff.NewRow();

                                double string_val_fuel = Convert.ToDouble(dt.Rows[i][3].ToString());
                                double string_val_fuel222 = Convert.ToDouble(dt.Rows[j][3].ToString());
                                double string_val_fuel333 = Convert.ToDouble(dt.Rows[k][3].ToString());
                                double string_val_fuel444 = Convert.ToDouble(dt.Rows[l][3].ToString());
                                double string_val_fuel555 = Convert.ToDouble(dt.Rows[m][3].ToString());

                                if (string_val_fuel333 >= fulltank123 && string_val_fuel333 <= fulltank12 && string_val_fuel444 >= fulltank123 && string_val_fuel444 <= fulltank12 && string_val_fuel555 >= fulltank123 && string_val_fuel555 <= fulltank12)
                                {
                                    double x = Emptytankvalue - Fulltankvalue;
                                    double y = tank_capacity / x;
                                    double z = string_val_fuel333 - Fulltankvalue;
                                    double b = z * y;
                                    double f = tank_capacity - b;
                                    f = (double)Math.Round(f);

                                    if (string_val_fuel >= string_val_fuel333 + xxx && string_val_fuel222 >= string_val_fuel333 + xxx)
                                    {
                                        double x1 = Emptytankvalue - Fulltankvalue;
                                        double y1 = tank_capacity / x;
                                        double z1 = string_val_fuel222 - Fulltankvalue;
                                        double b1 = z1 * y1;
                                        e = tank_capacity - b1;
                                        e = (double)Math.Round(e);

                                        /* gLatLng0 = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[k][0]), Convert.ToDouble(dt.Rows[k][1]));
                                         lt1 = gLatLng0.lat;
                                         lg1 = gLatLng0.lng;
                                         GeoCode geocode = GMap.geoCodeRequest(gLatLng0, key, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                         GMap gMap1 = new GMap();
                                         gMap1.getGeoCodeRequest(gLatLng0, new GLatLngBounds(new GLatLng(40, 10), new GLatLng(50, 20)));
                                         StringBuilder sb = new StringBuilder();
                                         if ((null != geocode) && geocode.valid)
                                         {
                                             sb.AppendFormat(geocode.Placemark.address);
                                         }*/
                                        string latitude = dt.Rows[k][0].ToString();
                                        string longitude = dt.Rows[k][1].ToString();

                                        System.Net.WebClient client = new System.Net.WebClient();

                                        string urlstring = string.Format(reverseGeocodeFormatString, latitude + "," + longitude, googleMapsAPIKey);
                                        string response = client.DownloadString(urlstring);
                                        dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);



                                        if (jsonresponse.results.Count > 0)
                                        {
                                            string formatted_address = jsonresponse.results[0].formatted_address;
                                            dr["Adress"] = formatted_address;
                                        }


                                        double filledfuel = f - e;

                                        DateTime filldate = Convert.ToDateTime(dt.Rows[k][2].ToString());

                                        //  dr["Adress"] = onadd;
                                        dr["GTIM"] = filldate.ToString("dd/MM/yyyy hh:mm tt");
                                        dr["Fuelfilled"] = filledfuel.ToString();
                                        dr["Totalfuel"] = f.ToString();
                                        dr["Openingfuel"] = e.ToString();
                                        fff.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }
                if (fff.Rows.Count <= 0)
                {
                    lblempty.Visible = true;
                    lblempty.Text = "No Records found...";
                    //lblempty.ForeColor = System.Drawing.Color.Red;

                }
                gvreport.DataSource = fff;
                gvreport.DataBind();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Fuel parameters are not calibrated..');", true);
                //string alertstring1 = "Fuel parameters are not calibrated..";
                //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring1));

            }

            //}
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Service Not Availed..');", true);
            //    //string alertstring1 = "Service Not Availed";
            //    //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring1));

            //}
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            gvreport.DataSource = null;
            gvreport.DataBind();
            lblempty.Text = "";
            string AMPM, DT;
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;

            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();

            string fdate = txtfdate.Text + " " + fromtime + " " + AMPM;
            string tdate = txttdate.Text + " " + totime + " " + DT;
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            if (Session["UserID"] != null)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    DataTable fre = db.Getfuelimp(Session["Cusername"].ToString());
                    if (fre.Rows[0][1].ToString() == "Yes")
                    {
                        Binddata(fdate, tdate, Vno);
                        disable();
                    }
                    else
                    {
                        //string alertstring1 = "Plese select dates in same month";
                        // this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring1));
                        string message = "alert('Service Not Availed.... ')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                    }
                }
                else
                {
                    DataTable fre = db.Getfuelimp(Session["Username"].ToString());
                    if (fre.Rows[0][1].ToString() == "Yes")
                    {

                        Binddata(fdate, tdate, Vno);
                        disable();
                    }
                    else
                    {
                        //string alertstring1 = "Plese select dates in same month";
                        // this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring1));
                        string message = "alert('Service Not Availed.... ')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                    }
                }
            }

        }


        protected void lnkView_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lbldate");
            string date = lbl.Text;

            string vno1 = ddlMapTOVehicle.SelectedValue.ToString();
            string pageurl = "fuelfillmap.aspx?RegNo=" + vno1 + "&&Date=" + date;
            //ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('" + pageurl + "','_newtab');", true);
            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "OpenWindow", "window.open('" + pageurl + "','_newtab');", true);

        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        public void disable()
        {
            ddlMapTOVehicle.Enabled = false;
            btndownload.Enabled = true;
            ddlFromHOURS.Enabled = false;
            ddlFromMINUTES.Enabled = false;
            ddlToHOURS.Enabled = false;
            ddlToMINUTES.Enabled = false;
            rbtnAMPM.Enabled = false;
            rbtnDT.Enabled = false;

        }
        protected void btndownload_Click(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue.ToString() != "" && txtfdate.Text != "" && txttdate.Text != "")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                //int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                //DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                DateTime fromday = Convert.ToDateTime(txtfdate.Text);
                string str1 = fromday.ToString("dd/MM/yyyy");//fromday.Month + "_" + fromday.Year;
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "Fuel filling Report" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);
                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                //Response.Write(style);
                Response.Write(hw.ToString());
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
    }
}

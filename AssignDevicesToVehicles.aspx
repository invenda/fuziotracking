﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignDevicesToVehicles.aspx.cs"
    Inherits="Tracking.AssignDevicesToVehicles"  MasterPageFile="~/ESMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript">
function ConfirmationBox(username) {

var result = confirm('Are you sure you want to delete '+username+' Details' );
if (result) {

return true;
}
else {
return false;
}
}
    </script>

    <div>
        <div id="sublogo" style="text-align: center;">
            <h1>
                <a>Assign Devices To Vehicles</a>
            </h1>
        </div>
    </div>
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <div style="width: 500px; margin: 0 auto; padding: 0; padding: 0;">
     <table style="width:100%;">
            <tr>
                <td>
                    <asp:Label ID="lbldevices" Text="SelectDevices" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label1" Text="Map To Vehicle" runat="server" Font-Bold="true" ForeColor="Black"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList Width="180px" ID="ddlDevices" runat="server" DataValueField="Device_ID"
                        DataTextField="Device_ID">
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDevices" ValidationGroup="Group1"
                        ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:DropDownList Width="180px" ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicle_number"
                        DataTextField="Vehicle_number">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlMapTOVehicle" ValidationGroup="Group1"
                        ErrorMessage="Select Date/Month">*</asp:RequiredFieldValidator>
                </td>
                <td align="center">
                    <asp:Button ID="btMap" runat="server" Text="Map" ValidationGroup="Group1" OnClick="btMap_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 500px; margin: 0 auto; padding: 0; padding: 0; height:auto;">
        <table style="width:100%;">
            <tr>
            <td style="height:20px;"></td>
            </tr>
            <tr>
                <td>                    
                    <asp:GridView runat="server" ID="gvrecords" AutoGenerateColumns="false" Width="120%" 
                        DataKeyNames="Device_ID" OnRowDataBound="gvrecords_RowDataBound" HeaderStyle-BackColor="#0099ff"
                        HeaderStyle-ForeColor="Black" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="VehicalNumber" HeaderText="Vehical Number" ControlStyle-Width ="250px" HeaderStyle-Width ="250px" />
                            <asp:BoundField DataField="Device_ID" HeaderText="Device ID" ControlStyle-Width ="90px" HeaderStyle-Width ="90px"/>
                            <asp:BoundField DataField="IMEI" HeaderText="IMEI No"  ControlStyle-Width ="200px" HeaderStyle-Width ="200px"/>                            
                            <asp:BoundField DataField="Model" HeaderText="Model No"  ControlStyle-Width ="90px" HeaderStyle-Width ="90px"/>
                            <asp:BoundField DataField="Instal_date" HeaderText="Instal Date" ControlStyle-Width ="100px" HeaderStyle-Width ="100px"/>
                            
                            <asp:TemplateField HeaderText="Function">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click">UnMap</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
            <td style="height:20px;"></td>
            </tr>
        </table>
    </div>
</asp:Content>
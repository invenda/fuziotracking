﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;
using System.Globalization;

namespace Tracking
{
    public partial class Live : System.Web.UI.Page
    {
        static int lblno = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Timer1.Enabled = true;
                binggrid();

            }
        }
        public void binggrid()
        {
            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
 
 
            DBClass db = new DBClass();
 
 
            if (Session["UserID"] != null)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                   // lbtnAdmin.Visible = false;
                }
                DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                if (stat.Columns.Count > 0)
                {
                    stat.Columns.Add("Status", typeof(string));
                    stat.Columns.Add("Lastdata", typeof(string));
                    stat.Columns.Add("Gps", typeof(string));
                    stat.Columns.Add("OVS", typeof(string));
                    stat.Columns.Add("ingnition", typeof(string));
                    stat.Columns.Add("bvolt", typeof(string));
                    stat.Columns.Add("dmobile", typeof(string));
                    stat.Columns.Add("Dname", typeof(string));


                    for (int i = 0; i < stat.Rows.Count; i++)
                    {
                        string VN = Convert.ToString(stat.Rows[i][0].ToString());

                        DataTable VS = db.GetVehStat(Session["UserID"].ToString(), VN);
                        DataTable status = db.GetVehiclePresentlocation(VN, Session["UserID"].ToString());
                        DateTime sysDT = DateTime.Now;
                        DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                        StringDT = StringDT.AddMinutes(30);
                        if (sysDT > StringDT)
                        {
                            stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ?";

                        }
                        else //if(sysDT <= StringDT)
                        {
                            stat.Rows[i]["Status"] = "Online";//"  ONLINE &nbsp;  ?";

                        }
                        if (status.Rows.Count != 0)
                        {
                            //lastdata
                            stat.Rows[i]["Lastdata"] = status.Rows[0][4].ToString();
                            //GPS
                            stat.Rows[i]["Gps"] = status.Rows[0][7].ToString();

                            //latlon
                            //double loc = Convert.ToDouble(status.Rows[0][0]);
                            //double loca = Convert.ToDouble(status.Rows[0][1]);
                            //stat.Rows[i]["latlon"] = Convert.ToString(loc) + " &nbsp;&nbsp;  " + Convert.ToString(loca);

                            //Overspeed

                            DataTable vdt = db.vehicledetailsalerts(VN);
                            if (vdt.Rows[0][3].ToString() != "" && vdt.Rows[0][3].ToString() != "0")
                            {
                                int speed1 = Convert.ToInt32(vdt.Rows[0][3].ToString());
                               // lblovs.Text = "(" + speed1.ToString() + " limit)";
                                speed1 = Convert.ToInt32(speed1 / 1.85);
                                string overspeed = speed1.ToString();
                                DataTable dtdevice = db.getdevicesid(VN);
                                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, overspeed);
                                if (dtoverspeed.Rows.Count != 0)
                                {
                                    double overspeed1 = Convert.ToInt32(dtoverspeed.Rows[0][0].ToString());
                                    overspeed1 = overspeed1 * 1.85;
                                    string ovsdate = Convert.ToDateTime(dtoverspeed.Rows[0][1]).ToShortTimeString();
                                    stat.Rows[i]["OVS"] = Convert.ToString(overspeed1) + " Km/h at " + ovsdate;

                                    //lbloverspeed.Text = overspeed1 + " Km/h at " + ovsdate;
                                }
                                else { stat.Rows[i]["OVS"] = "No Over Speed"; }
                            }
                            else { stat.Rows[i]["OVS"] = "No Over Speed"; }
            
          


                            //ingnition
                            if (status.Rows[0][8].ToString() == "0")
                                stat.Rows[i]["ingnition"] = "Off";
                            else
                                stat.Rows[i]["ingnition"] = "On";


                            //battery voltage
                            int bat = Convert.ToInt32(status.Rows[0][3].ToString());
                            double bvolt = bat / 42.5;
                            bvolt = Math.Round(bvolt, 2);
                            stat.Rows[i]["bvolt"] = bvolt.ToString();

                        }

                        //driver mobile no
                        DataTable DriDet = db.GerDriverDetails(VN, Session["UserID"].ToString());
                        stat.Rows[i]["dmobile"] = DriDet.Rows[0][2].ToString();

                        //Driver name
                        stat.Rows[i]["Dname"] = DriDet.Rows[0][0].ToString();


                    }

                }
                stat.AcceptChanges();
                GV.DataSource = stat;
                GV.DataBind();
                // GMap1.reset();
                // GMap1.resetMarkers();
                // GMap1.resetPolylines();
                imgst.ImageUrl = "http://google.com/mapfiles/ms/micons/red.png";
                imgidl.ImageUrl = "http://google.com/mapfiles/ms/micons/blue.png";
                imgmv.ImageUrl = "http://google.com/mapfiles/ms/micons/green.png";
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                // GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                // GMap1.GZoom = 4;                
                Session["Status"] = stat;
                Session["CheckAlerts"] = null;
                // check_General_Alerts();
                //}
            }

        }
           
        public double checkavailabe(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DateTime dt1 = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 12:00 AM");
            DataTable VS = db.GetVehStat(Session["UserID"].ToString(), Vno);
            DateTime dt2 = Convert.ToDateTime(VS.Rows[0][0].ToString());
            if (dt2 < dt1)
            {
                dt1 = Convert.ToDateTime(dt2.ToShortDateString() + " 12:00 AM");
            }
            TimeSpan ts = dt2 - dt1;
            DataTable dt = db.getblackspot(Vno, dt1.ToString(), dt2.ToString());
            double sysdata = Convert.ToDouble(dt.Rows[0][0].ToString());
            double av = ((ts.TotalMinutes - sysdata) / ts.TotalMinutes) * 100;
            av = 100 - av;
            av = (double)Math.Round(av, 1);
            if (av < 90 && av > 0)
            {
                av = av + 5;
            }
            else if (av < 95 && av > 0)
            {
                av = av + 2;
            }
            else if (av < 98 && av > 0)
            {
                av = av + 1;
            }
            else if (av >= 100)
            {
                av = 100;
            }
            return av;
        }
        public void tkmh(string vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.gettotalkm(vno);
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() != Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                    string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                    db.uptotalkm(vno, fdate, tdate, "0", fdate);
                }
                else if (Convert.ToDateTime(dt.Rows[0][2].ToString()).ToShortDateString() == Convert.ToDateTime(DateTime.Now).ToShortDateString())
                {
                    double tkm = Convert.ToDouble(dt.Rows[0][4].ToString());
                    string fdate = dt.Rows[0][2].ToString();
                    string fdate1 = dt.Rows[0][5].ToString();
                    string tdate = dt.Rows[0][3].ToString();
                    DataTable dt1 = db.Select_TotalRunkm(vno, fdate, tdate);
                    if (dt1.Rows.Count != 0)
                    {
                        for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                        {
                            tkm += CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                        }
                        fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    }
                    else if (dt1.Rows.Count == 0)
                    {
                        fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

                    }
                    db.uptotalkm(vno, fdate, tdate, tkm.ToString(), fdate);
                }
            }
            else
            {
                string fdate = DateTime.Now.ToShortDateString() + " " + "12:00 AM";
                string tdate = DateTime.Now.ToShortDateString() + " " + "11:59 PM";
                db.settotalkm(vno, fdate, tdate, "0");
            }
        }
        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radius = 6367.0;
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        public string Temp(string Vno)
        {

 
 
            DBClass db = new DBClass();
 
 
            DataTable fre = db.Getfreez(Session["Username"].ToString());              
            DataTable status = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            string condition = status.Rows[0][9].ToString();
            double conditon1 = Convert.ToDouble(condition);
            int days = 0;
            int Hours = 0;
            int Minutes = 0;
            double temp = 0;
            string temp12 = "";
            if (condition != "0")
            {
                if (fre.Rows[0][1].ToString() == "Yes")
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());


                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                if (temp <= 7)
                                {
                                    temp = temp * 1.9;
                                }
                                else if (temp <= 10)
                                {
                                    temp = temp * 1.7;
                                }
                                else if (temp <= 14)
                                {
                                    temp = temp * 1.4;
                                }
                                else if (temp <= 17)
                                {
                                    temp = temp * 1.3;
                                }
                                else if (temp <= 20)
                                {
                                    temp = temp * 1.1;
                                }
                                else if (temp > 20)
                                {
                                    temp = temp * 1;
                                }
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString ();

                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                temp12 = temp.ToString();
                            }

                        }
                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());

                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            if (temp <= 7)
                            {
                                temp = temp * 1.9;
                            }
                            else if (temp <= 10)
                            {
                                temp = temp * 1.7;
                            }
                            else if (temp <= 14)
                            {
                                temp = temp * 1.4;
                            }
                            else if (temp <= 17)
                            {
                                temp = temp * 1.3;
                            }
                            else if (temp <= 20)
                            {
                                temp = temp * 1.1;
                            }
                            else if (temp > 20)
                            {
                                temp = temp * 1;
                            }
                            temp = (double)Math.Round(temp, 1);
                            temp12 = "-" + temp.ToString();

                        }
                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();
                    }

                }


                else
                {
                    if (conditon1 >= 10000)
                    {
                        double cond1 = conditon1 - 10000;
                        if (cond1 > 1000)
                        {
                            DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                            if (pdt.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                int m = Convert.ToInt32(ts.TotalMinutes);
                                days = m / 1440;
                                Hours = m / 60;
                                Minutes = m % 60;
                                double cond2 = cond1 - 1000;
                                temp = cond2 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                temp12 = "-" + temp.ToString();

                            }
                            else if (cond1 < 1000)
                            {
                                temp = cond1 / 3.3;
                                temp = (double)Math.Round(temp, 1);
                                temp12 = temp.ToString();
                            }

                        }
                    }
                    else if (conditon1 >= 1000)
                    {
                        DataTable pdt = db.GetPreviIGST144(status.Rows[0][11].ToString(), Session["UserID"].ToString());
                        DateTime dtst = Convert.ToDateTime(status.Rows[0][4].ToString());
                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            TimeSpan ts = dtst - dtst1;
                            int m = Convert.ToInt32(ts.TotalMinutes);
                            days = m / 1440;
                            Hours = m / 60;
                            Minutes = m % 60;
                            double con1 = conditon1 - 1000;
                            temp = con1 / 3.3;
                            temp = (double)Math.Round(temp, 1);
                            temp12 = "-" + temp.ToString();

                        }
                    }
                    else if (conditon1 < 1000)
                    {
                        temp = conditon1 / 3.3;
                        temp = (double)Math.Round(temp, 1);
                        temp12 = temp.ToString();

                    }

                }
            }
            return temp12;

        }


        protected void Timer1_Tick(object sender, EventArgs e)
        {
            binggrid();

        }
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            string sMapKey = "";
            if (Session["APIKey"].ToString() != "")
            {
                sMapKey = Session["APIKey"].ToString();
            }
            else sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            List<string> stralerts = new List<string>();
            try
            {

 
 
                DBClass db = new DBClass();
 
 
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                try
                {
                    GeoCode objAddress = new GeoCode();
                    objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1])), sMapKey);
                    StringBuilder sb = new StringBuilder();
                    if (objAddress.valid)
                    {
                        sb.Append(objAddress.Placemark.address.ToString());
                        stralerts.Add("Vehicle no:  " + vhno);
                        stralerts.Add(" ");
                        stralerts.Add("Vehicle Location:    " + sb.ToString());
                    }
                    else
                        stralerts.Add("Not found");
                }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

                string[] salerts = new string[stralerts.Count];

                for (int m = 0; m < stralerts.Count; m++)
                {

                    salerts[m] = stralerts[m].ToString();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

        }
        protected void btnavail_Click(object sender, EventArgs e)
        {
             Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                string avilable = checkavailabe(vhno).ToString();

                if (avilable != null)
                {
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Available:   " + avilable + "%");
                }
                else
                {
                    stralerts.Add("Vehicle no:   " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Not found");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);

        }
        protected void btnspeed_Click(object sender, EventArgs e)
        {
             Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable status = db.GetVehiclePresentlocation(vhno, Session["UserID"].ToString());
                double speed = Convert.ToDouble(status.Rows[0][2].ToString());
                if (speed > 3)
                {
                    string sped = Convert.ToString(speed * 1.85);
                    stralerts.Add("Vehicle no:   " +  vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                    // DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);

                }
                else if (speed < 3)
                {
                    string sped = "0";
                    DateTime LocDTL = Convert.ToDateTime(status.Rows[0][4]);
                    stralerts.Add("Vehicle no:  " + vhno);
                    stralerts.Add(" ");
                    stralerts.Add("Speed in KM:  " + sped);
                }
                
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }
        protected void btnfuel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;

            List<string> stralerts = new List<string>();
            //try
           // {
 
 
                DBClass db = new DBClass();
 
 
                DataTable fuelim = db.Getfuelimp(Session["Username"].ToString());
                if (fuelim.Rows[0][1].ToString() == "Yes")
                {
                    DataTable dt2 = db.prevfuelstaus(vhno);
                    if (dt2.Rows.Count != 0)
                    {
                        if (Convert.ToInt32(dt2.Rows[0][1].ToString()) == 1)
                        {
                            double fulestat;
                            DataTable dtf = db.GetVehiclesfuel(vhno, Session["UserId"].ToString());
                            double string_val_fuel = Convert.ToDouble(dt2.Rows[0][3].ToString());
                            double tank_capacity = Convert.ToDouble(dtf.Rows[0][0].ToString());
                            int Tank_typ = Convert.ToInt32(dtf.Rows[0][3].ToString());
                            double Fuel_Cali = Convert.ToDouble(dtf.Rows[0][2].ToString());
                            if (Tank_typ == 0)
                            {
                                fulestat = (string_val_fuel) / (Fuel_Cali);
                                fulestat = fulestat * tank_capacity;
                                fulestat = Math.Truncate(fulestat * 100) / 100;
                                 stralerts.Add("Vehicle no:   " +  vhno);
                                 stralerts.Add(" ");
                                 stralerts.Add("Fuel in Ltr:  " + fulestat);
                            }
                            else
                            {
                                fulestat = (string_val_fuel) / (Fuel_Cali);
                                fulestat = fulestat * tank_capacity;
                                fulestat = tank_capacity - fulestat;
                                fulestat = Math.Truncate(fulestat * 100) / 100;
                                 stralerts.Add("Vehicle no:   " +  vhno);
                                 stralerts.Add(" ");
                                 stralerts.Add("Fuel in Ltr:  " + fulestat);
                            }
                        }
                    }
                    else  stralerts.Add("Waiting...");
                }
                else
                {
                    stralerts.Add(" Service Not Availed");
                    //lblFuel.Text = " Service Not Availed";
                    // string alertstring = " Fuel thing not implimented";
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "ShowMessage", string.Format("<script type='text/javascript'>alert('{0}')</script> ", alertstring));
                }
                //catch (Exception ex) { }

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }

         
        
        protected void btnKM_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
            Label lbl = new Label();
            lbl = (Label)grdRow.FindControl("lblvehicleno");
            string vhno = lbl.Text;


            List<string> stralerts = new List<string>();
            try
            {
 
 
                DBClass db = new DBClass();
 
 
                //total km run
                tkmh(vhno);
                DataTable tktable = db.gettotalkm(vhno);
                double tktkm = Convert.ToDouble(tktable.Rows[0][4].ToString());
                DateTime dtkmt = Convert.ToDateTime(tktable.Rows[0][5].ToString());
                tktkm = Math.Truncate(tktkm * 100) / 100;
                stralerts.Add("Vehicle no:  " + vhno);
                stralerts.Add(" ");
                stralerts.Add("Today KM run:    " + tktkm);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
        }

        protected void btntemp_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;

                string[] salerts = gettempalert(vhno);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", salerts) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
        }
        public string[] gettempalert(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 
            List<string> stralerts = new List<string>();
            try
            {

                string temp = Temp(Vno).ToString();

                stralerts.Add("Vehicle no:    " + Vno);
                stralerts.Add(" ");
                stralerts.Add("Temp:     "+ temp + "°C");

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used

            string[] salerts = new string[stralerts.Count];

            for (int m = 0; m < stralerts.Count; m++)
            {

                salerts[m] = stralerts[m].ToString();
            }
            return salerts;
          
        }

        protected void btnalert_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow grdRow = (GridViewRow)btn.NamingContainer;
                Label lbl = new Label();
                lbl = (Label)grdRow.FindControl("lblvehicleno");
                string vhno = lbl.Text;
                //DataGridItem dgi = (DataGridItem)((Button)sender).Parent.Parent;
                //string key = gdVehicles.DataKeyField[dgi.ItemIndex].ToString();
                // string address = ((LinkButton)dgi.FindControl("vehicleno")).Text;
                string[] alertstring = getalertdetailsfunction(vhno);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", alertstring) + "'.split(','));", true);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
           
        }
        public string[] getalertdetailsfunction(string Vno)
        {
 
 
            DBClass db = new DBClass();
 
 

            DataTable vdt = db.vehicledetailsalerts(Vno);
            string vinsEd = vdt.Rows[0][0].ToString();
            string rtorewd = vdt.Rows[0][1].ToString();
            string dlicexd = vdt.Rows[0][2].ToString();
            string overspeed = vdt.Rows[0][3].ToString();
            string conttodate = vdt.Rows[0][4].ToString();
            string gprsad = vdt.Rows[0][5].ToString();
            string fcdate = vdt.Rows[0][6].ToString();
            string emsdate = vdt.Rows[0][7].ToString();
            string roadtax = vdt.Rows[0][8].ToString();
            //string[] stralert = new string[10];
            List<string> stralert = new List<string>();
            stralert.Add("        VEHICLE RTO No : ");
            stralert.Add("  ");
            DateTime spdate = DateTime.Now;
            spdate = spdate.AddDays(30);
            DateTime spdate1 = DateTime.Now.AddDays(3);
            //DataRow dr = alertdt.NewRow();
            int j = 0;
            var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
            if (vinsEd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][0].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(vinsEd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Insurance will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Insurance DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Insurance Expired Today");
                    j++;
                }
            }
            if (rtorewd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][1].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(rtorewd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle RTO Permit will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle RTO Permit Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle RTO Permit Expired Today");
                    j++;
                }
            }
            if (dlicexd != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][2].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(dlicexd, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Driver Driving Licence Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Driver Driving Licence Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Driver Driving Licence Expired Today");
                    j++;
                }
            }
            if (overspeed != "0" && overspeed != "")
            {
                DataTable dtdevice = db.getdevicesid(Vno);
                int ovrspeed = Convert.ToInt32(overspeed);
                ovrspeed = Convert.ToInt32(ovrspeed / 1.85);
                string ovs = ovrspeed.ToString();
                string overspeeddate = Convert.ToString(DateTime.Now.ToShortDateString());
                string overspeeddate1 = overspeeddate + " " + "12:00 AM";
                string overspeeddate2 = overspeeddate + " " + "11:59 PM";
                DataTable dtoverspeed = db.getoverspeeddetails(dtdevice.Rows[0][0].ToString(), overspeeddate1, overspeeddate2, ovs);
                if (dtoverspeed.Rows.Count != 0)
                {
                    stralert.Add("Vehicle Over Speed.! ");
                    j++;

                }
            }
            if (conttodate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][4].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(conttodate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Contract Period Will be Expired in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Contract Period Date Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Contract Period Expired Today");
                    j++;
                }
            }
            if (gprsad != "")// && spdate1 >= Convert.ToDateTime(vdt.Rows[0][5].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(gprsad, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 3 && s > 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle GPRS Recharge DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle GPRS Recharge Expired Today");
                    j++;
                }
            }
            if (fcdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][6].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(fcdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle FC Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle FC DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle FC Expired Today");
                    j++;
                }
            }
            if (emsdate != "")//&& spdate >= Convert.ToDateTime(vdt.Rows[0][7].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(emsdate, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Emmission Test Date Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Emmission Test DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Emmission Test Expired Today");
                    j++;
                }
            }
            if (roadtax != "")// && spdate >= Convert.ToDateTime(vdt.Rows[0][8].ToString(), dtfi))
            {
                TimeSpan span = Convert.ToDateTime(roadtax, dtfi) - Convert.ToDateTime(DateTime.Now.ToShortDateString());
                int s = Convert.ToInt32(span.Days);
                if (s <= 30 && s > 0)
                {
                    stralert.Add("Vehicle Road Tax Will Expire in " + s + " days");
                    j++;
                }
                else if (s < 0)
                {
                    stralert.Add("Vehicle Road Tax DATE Expired");
                    j++;
                }
                else if (s == 0)
                {
                    stralert.Add("Vehicle Road Tax Expired Today");
                    j++;
                }
            }
            int len = stralert.Count;
            string[] salerts = new string[len];
            if (j != 0)
            {
                for (int m = 0, n = 1; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else if (m == 1)
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                    else
                    {
                        salerts[m] = n + " : " + stralert[m].ToString();
                        n++;
                    }
                }
            }
            else
            {
                if (lblno == 0)
                {
                    lblmalerts.Visible = false;
                }
                stralert.Add("NO Alert For This Vehicle");
                len = stralert.Count;
                salerts = new string[len];
                for (int m = 0; m < len; m++)
                {
                    if (m == 0)
                    {
                        salerts[m] = stralert[m].ToString() + Vno;
                    }
                    else
                    {
                        salerts[m] = stralert[m].ToString();
                    }
                }
            }
            return salerts;
        }


        protected string alert2(string Vno, int no)
        {
            StringBuilder builder = new StringBuilder();
            int maxtemp = 0, min1 = 0, delay = 0;
 
 
            DBClass db = new DBClass();
 
 
            DataTable VS = db.GetVehiclePresentlocation(Vno, Session["UserID"].ToString());
            if (no == 0)
            {
                DataTable gadt = db.vehicledetailsalerts(Vno);
                if (gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][14].ToString() != "")
                {
                    maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    min1 = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    delay = Convert.ToInt32(gadt.Rows[0][14].ToString());
                    int dttemp = 0;
                    if (maxtemp != 0)
                    {
                        if (maxtemp > 0)
                        {
                            dttemp = db.Temp_alert2(Vno, min1.ToString(), maxtemp.ToString());
                        }
                        else if (maxtemp < 0)
                        {
                            maxtemp = maxtemp * -1 + 1000;
                            dttemp = db.Temp_alert3(Vno, min1.ToString(), maxtemp.ToString());
                        }
                        if (delay <= dttemp)
                        {
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("||");
                            builder.Append("</span>");
                            builder.Append(" " + Vno + "- ");
                            builder.Append("<span style=\"color:Red;\">");
                            builder.Append("Temperature");
                            builder.Append("</span>");
                            builder.Append("> " + gadt.Rows[0][9].ToString() + "°C ");
                        }
                    }
                }
#pragma warning disable CS0219 // The variable 'vid' is assigned but its value is never used
                int vsta = 0, vid = 0, vmng = 0, hours, min;
#pragma warning restore CS0219 // The variable 'vid' is assigned but its value is never used
                if (VS.Rows[0][8].ToString() == "1" && VS.Rows[0][2].ToString() != "0")
                {
                    if (gadt.Rows[0][12].ToString() != "" && gadt.Rows[0][12].ToString() != "0")
                    {
                        DataTable pmdt = db.GetPreviMNG(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                        vmng = Convert.ToInt32(gadt.Rows[0][12].ToString());
                        hours = vmng / 60;
                        min = vmng % 60;
                        DateTime dtmng = Convert.ToDateTime(VS.Rows[0][4].ToString());
                        if (pmdt.Rows.Count != 0)
                        {
                            DateTime dtmng1 = Convert.ToDateTime(pmdt.Rows[0][0].ToString());
                            TimeSpan ts = dtmng - dtmng1;
                            if (vmng < ts.TotalMinutes)
                            {
                                //alerttext3 = lbla2.Text+ Vno + "- Moving > "+hours.ToString()+":"+min.ToString()+" Hr ";
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Moving");
                                builder.Append("</span>");
                                builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                            }
                        }
                        else
                        {
                            DataTable pmdt1 = db.GetPreviMNG1(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            DateTime dtmng1 = Convert.ToDateTime(pmdt1.Rows[0][0].ToString());
                            TimeSpan ts = dtmng - dtmng1;
                            if (vmng < ts.TotalMinutes)
                            {
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("||");
                                builder.Append("</span>");
                                builder.Append(" " + Vno + "- ");
                                builder.Append("<span style=\"color:Red;\">");
                                builder.Append("Moving");
                                builder.Append("</span>");
                                builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                            }
                        }
                    }
                }
                else
                {
                    string sft = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][15].ToString();
                    string stt = DateTime.Now.ToShortDateString() + " " + gadt.Rows[0][16].ToString();
                    if (gadt.Rows[0][10].ToString() != "" && gadt.Rows[0][10].ToString() != "0" && Convert.ToDateTime(sft) < DateTime.Now && Convert.ToDateTime(stt) > DateTime.Now)
                    {
                        DataTable pdt = db.GetPreviIGST11(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                        vsta = Convert.ToInt32(gadt.Rows[0][10].ToString());
                        DateTime dtst = Convert.ToDateTime(VS.Rows[0][4].ToString());
                        hours = vsta / 60;
                        min = vsta % 60;
                        if (pdt.Rows.Count != 0)
                        {
                            DateTime dtst1 = Convert.ToDateTime(pdt.Rows[0][0].ToString());
                            if (dtst1 > Convert.ToDateTime(sft))
                            {
                                TimeSpan ts = dtst - dtst1;
                                if (vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");

                                }
                            }
                            else
                            {
                                DataTable dtchk = db.firstigston(Convert.ToDateTime(sft).ToString(), DateTime.Now.ToString(), VS.Rows[0][11].ToString());
                                TimeSpan ts = DateTime.Now - Convert.ToDateTime(sft);
                                if (dtchk.Rows.Count == 0 && vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                        }
                        else
                        {
                            DataTable pdt1 = db.GetPreviIGST22(VS.Rows[0][11].ToString(), Session["UserID"].ToString());
                            if (pdt1.Rows.Count != 0)
                            {
                                DateTime dtst1 = Convert.ToDateTime(pdt1.Rows[0][0].ToString());
                                TimeSpan ts = dtst - dtst1;
                                if (vsta < ts.TotalMinutes)
                                {
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("||");
                                    builder.Append("</span>");
                                    builder.Append(" " + Vno + "- ");
                                    builder.Append("<span style=\"color:Red;\">");
                                    builder.Append("Stationary");
                                    builder.Append("</span>");
                                    builder.Append("> " + hours.ToString() + ":" + min.ToString() + " Hr ");
                                }
                            }
                        }
                    }
                }
                string dnt = DateTime.Now.ToShortDateString();
                DateTime ftime = Convert.ToDateTime(dnt + " 12:00 AM");
                DateTime ttime = Convert.ToDateTime(dnt + " 11:59 PM");
                DataTable Towesdt = db.Towed_Alerts(Vno, "TOWED", ftime.ToString(), ttime.ToString());
                if (Towesdt.Rows.Count >= 8)
                {
                    //DateTime dnt1 = Convert.ToDateTime(Towesdt.Rows[0][0].ToString());
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("||");
                    builder.Append("</span>");
                    builder.Append(" " + Vno + "- ");
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("Towed At");
                    builder.Append("</span>");
                    // builder.Append(": " + dnt1.ToShortTimeString());
                }
            }
            else if (no == 1)
            {
                DateTime spdt = Convert.ToDateTime(VS.Rows[0][4].ToString());
                DateTime pdt = DateTime.Now;
                TimeSpan ts = pdt - spdt;
                if (ts.TotalHours >= 6)
                {
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("||");
                    builder.Append("</span>");
                    builder.Append(" " + Vno + "- ");
                    builder.Append("<span style=\"color:Red;\">");
                    builder.Append("Offline");
                    builder.Append("</span>");
                    builder.Append(" > 6Hr ");

                }
            }
            return builder.ToString();
        }
        public void check_General_Alerts()
        {
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.Check_Genaral_Alerts(Session["UserID"].ToString());
            if (dt.Rows.Count > 0)
            {
                lblmalerts.Visible = true;
                StringBuilder sba = new StringBuilder();
                sba.Append("You have ALERT-click The ");
                sba.Append("<span style=\"color:Red;\">");
                sba.Append("Alert Button ");
                sba.Append("</span>");
                sba.Append("Corresponding to The Vehicle No");
                lblmalerts.Text = sba.ToString();
            }
        }

        public void Bind_Vehicles()
        {
 
 
            DBClass dbk = new DBClass();
 
 

            DataTable stat = dbk.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
            lbloffalerts.Text = "";
            lblalerts2.Text = "";
            if (stat.Columns.Count > 0)
            {
                stat.Columns.Add("Status", typeof(string));
                for (int i = 0; i < stat.Rows.Count; i++)
                {
                    string VN = Convert.ToString(stat.Rows[i][0].ToString());
                    DataTable VS = dbk.GetVehStat(Session["UserID"].ToString(), VN);
                    DateTime sysDT = DateTime.Now;
                    DateTime StringDT = Convert.ToDateTime(VS.Rows[0][0].ToString());
                    StringDT = StringDT.AddMinutes(30);
                    if (sysDT > StringDT)
                    {
                        stat.Rows[i]["Status"] = "Offline";// "  OFFLINE &nbsp; ✘";
                        string lblalerts = alert2(stat.Rows[i]["VehicalNumber"].ToString(), 1);
                        lbloffalerts.Text += lblalerts;
                    }
                    else
                    {
                        stat.Rows[i]["Status"] = "Online";// "  ONLINE &nbsp; ✔";
                        string lblalerts = alert2(stat.Rows[i]["VehicalNumber"].ToString(), 0);
                        lblalerts2.Text += lblalerts;
                    }
                    if (Session["CheckAlerts"] == null)
                    {
                        string[] stralt = getalertdetailsfunction(VN);
                        if (stralt[2].ToString() != "NO Alert For This Vehicle")
                        {
                            dbk.Vehicle_Alert_Status(VN, Session["UserID"].ToString(), 0);
                            stat.Rows[i]["Vehicle_Alerts_Status"] = "0";
                        }
                        else
                        {
                            dbk.Vehicle_Alert_Status(VN, Session["UserID"].ToString(), 1);
                            stat.Rows[i]["Vehicle_Alerts_Status"] = "1";
                        }
                    }
                }
                Session["CheckAlerts"] = 1;
            }
            stat.AcceptChanges();
            GV.DataSource = stat;
            GV.DataBind();
            Session["Status"] = stat;
        }
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
             if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl = (Label)e.Row.FindControl("lblvehicleno");
 
 
                DBClass db = new DBClass();
 
 
                DataTable dts = db.GetStatus(lbl.Text);
                Label myLabel1 = (Label)e.Row.FindControl("lblstatus");
                Image img = (Image)e.Row.FindControl("Simg");
                if (dts.Rows[0][1].ToString() == "0")
                {
                    img.ImageUrl = "http://google.com/mapfiles/ms/micons/red.png";
                }
                else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) > 3)
                {
                    img.ImageUrl = "http://google.com/mapfiles/ms/micons/green.png";
                }
                else if (dts.Rows[0][1].ToString() == "1" && Convert.ToInt32(dts.Rows[0][0].ToString()) <= 3)
                {
                    img.ImageUrl = "http://google.com/mapfiles/ms/micons/blue.png";
                }
                if (myLabel1.Text == "Offline")
                {
                    e.Row .Cells[3].ForeColor = System.Drawing.Color.Red;
                    DateTime sdt = Convert.ToDateTime(dts.Rows[0][2].ToString());
                    DateTime pdt = DateTime.Now;
                    TimeSpan ts = pdt - sdt;
                    if (ts.TotalHours >= 6)
                    {
                        e.Row .Cells[3].BackColor = System.Drawing.Color.Black;
                    }
                }

                //Button btn1 = (Button)e.Row.FindControl("btnalert");
                //if (btn1.Text == "0")
                //{
                //    btn1.BackColor = System.Drawing.Color.Red;
                //    btn1.BorderColor = System.Drawing.Color.Red;
                   
                //}
                //else
                //{
                //    btn1.BackColor = System.Drawing.Color.White;
                //    btn1.BorderColor = System.Drawing.Color.White;
                   
                //}
             }}
              protected void Btndalerts_Click(object sender, EventArgs e)
        {
            if ((lblalerts2.Text != "") || (lbloffalerts.Text != ""))
            {
 
 
                DBClass db = new DBClass();
 
 
                DataTable stat = db.GetVehicles(Session["UserID"].ToString());//, Session["ParentUser"].ToString());
                string stralert = "";
                string stralert1 = "";

                if (lblalerts2.Text != "")
                {
                    stralert = "Daily Live Alerts ,";
                }
                if (lbloffalerts.Text != "")
                {
                    stralert1 = ",,, Offline More than 6 Hr ,";
                }
                stralert += lblalerts2.Text;
                string stralert2 = stralert.Replace("||", ",");
                stralert2 = stralert2.Replace("<span style=\"color:Red;\">", "");
                stralert2 = stralert2.Replace("</span>", "");

                stralert1 += lbloffalerts.Text;
                string stralert3 = stralert1.Replace("||", ",");
                stralert3 = stralert3.Replace("<span style=\"color:Red;\">", "");
                stralert3 = stralert3.Replace("</span>", "");
                stralert2 = stralert2 + stralert3;

                string[] results = stralert2.Split(new[] { ',' });
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alertsdetails('" + String.Join(",", results) + "'.split(','));", true);

            
        }
        

        }

        

    }
}



﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace Tracking
{
    public partial class TemperatureReport : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        int rcount = 0;
        int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserRole"].ToString() == "3")
                {
                    DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                    ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                    ddlMapTOVehicle.DataBind();
                }
                else
                {
                    ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                    ddlMapTOVehicle.DataBind();
                }
                ddlmonthbind();
            }

        }
        public void ddlmonthbind()
        {
            DateTime dt = DateTime.Now;
            int m = Convert.ToInt32(dt.Month);
            if (m == 1)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));

            }
            else if (m == 2)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("December", "12"));
                ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));

            }
            else if (m == 3)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("January", "01"));
                ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));

            }
            else if (m == 4)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("February", "02"));
                ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
            }
            else if (m == 5)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
              //  ddlmonth.Items.Add(new ListItem("March", "03"));
                ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
            }
            else if (m == 6)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("April", "04"));
                ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
            }
            else if (m == 7)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("May", "05"));
                ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));

            }
            else if (m == 8)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("June", "06"));
                ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));

            }
            else if (m == 9)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("July", "07"));
                ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));

            }
            else if (m == 10)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("August", "08"));
                ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));

            }
            else if (m == 11)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("September", "09"));
                ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));

            }
            else if (m == 12)
            {
                ddlmonth.Items.Add(new ListItem("--select Month--", "00"));
               // ddlmonth.Items.Add(new ListItem("October", "10"));
                ddlmonth.Items.Add(new ListItem("November", "11"));
                ddlmonth.Items.Add(new ListItem("December", "12"));

            }

        }

        protected void GetRecord_Click(object sender, EventArgs e)
        {
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
             DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (month == cmonth)
            {
                bindmonthtemp();
            }
            else
            {
                bindmonthtempold();

            }

        }
        public void bindmonthtemp()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dttemp = db.Select_TempMonth(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            if (fre.Rows[0][1].ToString() == "Yes")
            {
                db.Delete_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
                DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dthours = db.getRhours();
                if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                {
                    DateTime dt = DateTime.Now;
                    DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                    DateTime ldt = LastDayOfMonthFromDateTime(dt, month);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    double maxxtemp;
                    double matemp;
                    for (int i = 0; i < 31; i++)
                    {
                        if ((fdt.Month <= month && fdt < dt))
                        {
                            db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                            if (maxtemp1 > 0)
                            {
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            else if (maxtemp1 < 0)
                            {
                                maxxtemp = Convert.ToInt32(gadt.Rows[0][9].ToString());
                                if (maxxtemp <= -10)
                                {
                                    matemp = maxxtemp / 1.7;
                                }
                                else
                                {
                                    matemp = maxxtemp;
                                }
                                double mmaxtemp = Convert.ToDouble(matemp * 3.3);
                                double maxtempp = mmaxtemp * -1 + 1000;
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert2(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtempp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }                                
                            }
                            fdt = fdt.AddDays(1);
                        }
                        else break;
                    }
                }
            }
            else
            {
                DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dthours = db.getRhours();
                if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                {
                    DateTime dt = DateTime.Now;
                    DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                    DateTime ldt = LastDayOfMonthFromDateTime(dt, month);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    for (int i = 0; i < 31; i++)
                    {
                        if ((fdt.Month <= month && fdt < dt))
                        {
                            db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                            if (maxtemp1 > 0)
                            {
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            else if (maxtemp1 < 0)
                            {
                                maxtemp = maxtemp1 * -1 + 1000;
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert1(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            fdt = fdt.AddDays(1);
                        }
                        else break;

                    }
                }
                else if (dttemp.Rows.Count != 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0")
                {
                    DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    string oldstr = dttemp.Rows[0][3].ToString();
                    string strDate = DateTime.ParseExact(oldstr, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");
                    DateTime fdt = Convert.ToDateTime(strDate);
                    DateTime ldt1 = LastDayOfMonthFromDateTime(fdt, fdt.Month);
                    DateTime fdt1 = fdt;
                    DateTime ldt = dt.AddDays(-1);
                    fdt = fdt.AddDays(1);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    if (fdt1 < ldt)
                    {
                        for (int i = 0; i < 31; i++)
                        {
                            if ((fdt.Month <= month && fdt < dt))
                            {
                                db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                                if (maxtemp1 > 0)
                                {
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                    }
                                }
                                else if (maxtemp1 < 0)
                                {
                                    maxtemp = maxtemp1 * -1 + 1000;
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert1(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));
                                    }
                                }
                            }
                            else
                                break;
                            fdt = fdt.AddDays(1);
                        }
                    }
                }
            }
                Enable();
                DataTable dt1 = binddata(Vno, month);
                gvreport.DataSource = dt1;
                gvreport.DataBind();

            
        }
        public void Enable()
        {

            ddlMapTOVehicle.Enabled = false;
            ddlmonth.Enabled = false;
        }
        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime, int Month)
        {
            return new DateTime(dateTime.Year, Month, 1);
        }
        public DateTime LastDayOfMonthFromDateTime(DateTime dateTime, int Month)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public void bindmonthtempold()
        {
            string Vno = ddlMapTOVehicle.SelectedValue.ToString();
            DataTable dttemp = db.Select_TempMonth(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
            int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
            DataTable fre = db.Getfreez(Session["Username"].ToString());
            if (fre.Rows[0][1].ToString() == "Yes")
            {
                db.Delete_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), ddlmonth.SelectedValue.ToString());
                DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dthours = db.getRhours();
                if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                {
                    DateTime dt = DateTime.Now;
                    DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                    DateTime ldt = LastDayOfMonthFromDateTime(dt, month);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    double maxxtemp;
                    double matemp;
                    for (int i = 0; i < 31; i++)
                    {
                        if ((fdt.Month <= month && fdt < dt))
                        {
                            db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                            if (maxtemp1 > 0)
                            {
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalertold(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            else if (maxtemp1 < 0)
                            {
                                maxxtemp = Convert.ToInt32(gadt.Rows[0][9].ToString());
                                if (maxxtemp <= -10)
                                {
                                    matemp = maxxtemp / 1.7;
                                }
                                else
                                {
                                    matemp = maxxtemp;
                                }
                                double mmaxtemp = Convert.ToDouble(matemp * 3.3);
                                double maxtempp = mmaxtemp * -1 + 1000;
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert2old(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtempp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            fdt = fdt.AddDays(1);
                        }
                        else break;
                    }
                }
            }
            else
            {
                DataTable gadt = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dthours = db.getRhours();
                if (dttemp.Rows.Count == 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0") // && gadt.Rows[0][13].ToString() != "0")
                {
                    DateTime dt = DateTime.Now;
                    DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                    DateTime ldt = LastDayOfMonthFromDateTime(dt, month);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    for (int i = 0; i < 31; i++)
                    {
                        if ((fdt.Month <= month && fdt < dt))
                        {
                            db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                            if (maxtemp1 > 0)
                            {
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalertold(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            else if (maxtemp1 < 0)
                            {
                                maxtemp = maxtemp1 * -1 + 1000;
                                for (int j = 0; j < dthours.Rows.Count; j++)
                                {
                                    DataTable dt3 = db.Temp_Noofalert1old(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                    db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                }
                            }
                            fdt = fdt.AddDays(1);
                        }
                        else break;

                    }
                }
                else if (dttemp.Rows.Count != 0 && gadt.Rows[0][9].ToString() != "" && gadt.Rows[0][13].ToString() != "" && gadt.Rows[0][9].ToString() != "0")
                {
                    DateTime dt = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    string oldstr = dttemp.Rows[0][3].ToString();
                    string strDate = DateTime.ParseExact(oldstr, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");
                    DateTime fdt = Convert.ToDateTime(strDate);
                    DateTime ldt1 = LastDayOfMonthFromDateTime(fdt, fdt.Month);
                    DateTime fdt1 = fdt;
                    DateTime ldt = dt.AddDays(-1);
                    fdt = fdt.AddDays(1);
                    int maxtemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][9].ToString()) * 3.3);
                    int mintemp = Convert.ToInt32(Convert.ToInt32(gadt.Rows[0][13].ToString()) * 3.3);
                    int maxtemp1 = maxtemp;
                    if (fdt1 < ldt)
                    {
                        for (int i = 0; i < 31; i++)
                        {
                            if ((fdt.Month <= month && fdt < dt))
                            {
                                db.Insert_Monthtemp(ddlMapTOVehicle.SelectedValue.ToString(), fdt.ToString("dd/MM/yyyy"), month.ToString());
                                if (maxtemp1 > 0)
                                {
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalertold(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));

                                    }
                                }
                                else if (maxtemp1 < 0)
                                {
                                    maxtemp = maxtemp1 * -1 + 1000;
                                    for (int j = 0; j < dthours.Rows.Count; j++)
                                    {
                                        DataTable dt3 = db.Temp_Noofalert1old(fdt.ToShortDateString() + " " + dthours.Rows[j][0].ToString(), fdt.ToShortDateString() + " " + dthours.Rows[j][1].ToString(), ddlMapTOVehicle.SelectedValue.ToString(), maxtemp, mintemp);
                                        db.Update_TempMonth(Vno, month.ToString(), dt3.Rows[0][0].ToString(), j, fdt.ToString("dd/MM/yyyy"));
                                    }
                                }
                            }
                            else
                                break;
                            fdt = fdt.AddDays(1);
                        }
                    }
                }
            }
            Enable();
            DataTable dt1 = binddata(Vno, month);
            gvreport.DataSource = dt1;
            gvreport.DataBind();


        }
        public DataTable binddata(string vno, int month)
        {
            DataTable dt = db.get_TempMonth(vno, month);
            return dt;

        }

        protected void gvreport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable dt = db.DriverDetails(ddlMapTOVehicle.SelectedValue.ToString());
                DataTable dt1 = db.vehicledetailsalerts(ddlMapTOVehicle.SelectedValue.ToString());
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "Vehical RTO No. :" + ddlMapTOVehicle.SelectedValue.ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 9;
                HeaderCell.Font.Bold = true;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = "Requested Time:" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt").ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell.ColumnSpan = 8;
                HeaderCell.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);
                TableCell HeaderCell4 = new TableCell();
                HeaderCell4.Text = "Owner/Transporter :" + dt.Rows[0][1].ToString();
                HeaderCell4.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell4.ColumnSpan = 9;
                HeaderCell4.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell4.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell4.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell4);
                gvreport.Controls[0].Controls.AddAt(0, HeaderGridRow);

                GridViewRow HeaderGridRow1 = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell1 = new TableCell();
                HeaderCell1.Text = "Maximum Temperature :" + dt1.Rows[0][9].ToString() + "°C";
                HeaderCell1.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell1.ColumnSpan = 9;
                HeaderCell1.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell1.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell1.Font.Bold = true;
                HeaderGridRow1.Cells.Add(HeaderCell1);
                TableCell HeaderCell3 = new TableCell();
                HeaderCell3.Text = "Minimum Temperature :" + dt1.Rows[0][13].ToString() + "°C";
                HeaderCell3.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell3.ColumnSpan = 8;
                HeaderCell3.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell3.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell3.Font.Bold = true;
                HeaderGridRow1.Cells.Add(HeaderCell3);
                TableCell HeaderCell5 = new TableCell();
                HeaderCell5.Text = "Driver Mobile No :" + dt.Rows[0][2].ToString();
                HeaderCell5.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell5.ColumnSpan = 9;
                HeaderCell5.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell5.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell5.Font.Bold = true;
                HeaderGridRow1.Cells.Add(HeaderCell5);
                gvreport.Controls[0].Controls.AddAt(1, HeaderGridRow1);

                GridViewRow HeaderGridRow3 = new GridViewRow(2, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell12 = new TableCell();
                HeaderCell12.Text = "Report for the Month :" + ddlmonth.SelectedItem.Text + " " + DateTime.Now.Year;
                HeaderCell12.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell12.ColumnSpan = 26;
                HeaderCell12.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell12.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell12.Font.Bold = true;
                HeaderGridRow3.Cells.Add(HeaderCell12);
                gvreport.Controls[0].Controls.AddAt(2, HeaderGridRow3);

                GridViewRow HeaderGridRow2 = new GridViewRow(3, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell21 = new TableCell();
                HeaderCell21.Text = "";
                HeaderCell21.HorizontalAlign = HorizontalAlign.Right;
                HeaderCell21.ColumnSpan = 1;
                HeaderCell21.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell21.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell21.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell21);
                TableCell HeaderCell22 = new TableCell();
                HeaderCell22.Text = "Time of the Day in Hrs (24 Hours)";
                HeaderCell22.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell22.ColumnSpan = 24;
                HeaderCell22.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell22.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell22.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell22);
                TableCell HeaderCell23 = new TableCell();
                HeaderCell23.Text = "";
                HeaderCell23.HorizontalAlign = HorizontalAlign.Right;
                HeaderCell23.ColumnSpan = 1;
                HeaderCell23.BackColor = System.Drawing.Color.LightGreen;
                HeaderCell23.BorderColor = System.Drawing.Color.LightGreen;
                HeaderCell23.Font.Bold = true;
                HeaderGridRow2.Cells.Add(HeaderCell23);
                gvreport.Controls[0].Controls.AddAt(3, HeaderGridRow2);


            }
        }

        protected void gvreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                rcount = 0; count = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl1 = e.Row.FindControl("lblH1") as Label;
                rcount += Convert.ToInt32(lbl1.Text);
                if (Convert.ToInt32(lbl1.Text) == 0)
                {
                    lbl1.Text = "  ";
                }
                else if (Convert.ToInt32(lbl1.Text) > 0)
                {
                    e.Row.Cells[1].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl2 = e.Row.FindControl("lblH2") as Label;
                rcount += Convert.ToInt32(lbl2.Text);
                if (Convert.ToInt32(lbl2.Text) == 0)
                {
                    lbl2.Text = "  ";
                }
                else if (Convert.ToInt32(lbl2.Text) > 0)
                {
                    e.Row.Cells[2].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl3 = e.Row.FindControl("lblH3") as Label;
                rcount += Convert.ToInt32(lbl3.Text);
                if (Convert.ToInt32(lbl3.Text) == 0)
                {
                    lbl3.Text = "  ";
                }
                else if (Convert.ToInt32(lbl3.Text) > 0)
                {
                    e.Row.Cells[3].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl4 = e.Row.FindControl("lblH4") as Label;
                rcount += Convert.ToInt32(lbl4.Text);
                if (Convert.ToInt32(lbl4.Text) == 0)
                {
                    lbl4.Text = "  ";
                }
                else if (Convert.ToInt32(lbl4.Text) > 0)
                {
                    e.Row.Cells[4].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl5 = e.Row.FindControl("lblH5") as Label;
                rcount += Convert.ToInt32(lbl5.Text);
                if (Convert.ToInt32(lbl5.Text) == 0)
                {
                    lbl5.Text = "  ";
                }
                else if (Convert.ToInt32(lbl5.Text) > 0)
                {
                    e.Row.Cells[5].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl6 = e.Row.FindControl("lblH6") as Label;
                rcount += Convert.ToInt32(lbl6.Text);
                if (Convert.ToInt32(lbl6.Text) == 0)
                {
                    lbl6.Text = "  ";
                }
                else if (Convert.ToInt32(lbl6.Text) > 0)
                {
                    e.Row.Cells[6].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl7 = e.Row.FindControl("lblH7") as Label;
                rcount += Convert.ToInt32(lbl7.Text);
                if (Convert.ToInt32(lbl7.Text) == 0)
                {
                    lbl7.Text = "  ";
                }
                else if (Convert.ToInt32(lbl7.Text) > 0)
                {
                    e.Row.Cells[7].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl8 = e.Row.FindControl("lblH8") as Label;
                rcount += Convert.ToInt32(lbl8.Text);
                if (Convert.ToInt32(lbl8.Text) == 0)
                {
                    lbl8.Text = "  ";
                }
                else if (Convert.ToInt32(lbl8.Text) > 0)
                {
                    e.Row.Cells[8].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl9 = e.Row.FindControl("lblH9") as Label;
                rcount += Convert.ToInt32(lbl9.Text);
                if (Convert.ToInt32(lbl9.Text) == 0)
                {
                    lbl9.Text = "  ";
                }
                else if (Convert.ToInt32(lbl9.Text) > 0)
                {
                    e.Row.Cells[9].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl10 = e.Row.FindControl("lblH10") as Label;
                rcount += Convert.ToInt32(lbl10.Text);
                if (Convert.ToInt32(lbl10.Text) == 0)
                {
                    lbl10.Text = "  ";
                }
                else if (Convert.ToInt32(lbl10.Text) > 0)
                {
                    e.Row.Cells[10].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl11 = e.Row.FindControl("lblH11") as Label;
                rcount += Convert.ToInt32(lbl11.Text);
                if (Convert.ToInt32(lbl11.Text) == 0)
                {
                    lbl11.Text = "  ";
                }
                else if (Convert.ToInt32(lbl11.Text) > 0)
                {
                    e.Row.Cells[11].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl12 = e.Row.FindControl("lblH12") as Label;
                rcount += Convert.ToInt32(lbl12.Text);
                if (Convert.ToInt32(lbl12.Text) == 0)
                {
                    lbl12.Text = "  ";
                }
                else if (Convert.ToInt32(lbl12.Text) > 0)
                {
                    e.Row.Cells[12].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl13 = e.Row.FindControl("lblH13") as Label;
                rcount += Convert.ToInt32(lbl13.Text);
                if (Convert.ToInt32(lbl13.Text) == 0)
                {
                    lbl13.Text = "  ";
                }
                else if (Convert.ToInt32(lbl13.Text) > 0)
                {
                    e.Row.Cells[13].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl14 = e.Row.FindControl("lblH14") as Label;
                rcount += Convert.ToInt32(lbl14.Text);
                if (Convert.ToInt32(lbl14.Text) == 0)
                {
                    lbl14.Text = "  ";
                }
                else if (Convert.ToInt32(lbl14.Text) > 0)
                {
                    e.Row.Cells[14].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl15 = e.Row.FindControl("lblH15") as Label;
                rcount += Convert.ToInt32(lbl15.Text);
                if (Convert.ToInt32(lbl15.Text) == 0)
                {
                    lbl15.Text = "  ";
                }
                else if (Convert.ToInt32(lbl15.Text) > 0)
                {
                    e.Row.Cells[15].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl16 = e.Row.FindControl("lblH16") as Label;
                rcount += Convert.ToInt32(lbl16.Text);
                if (Convert.ToInt32(lbl16.Text) == 0)
                {
                    lbl16.Text = "  ";
                }
                else if (Convert.ToInt32(lbl16.Text) > 0)
                {
                    e.Row.Cells[16].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl17 = e.Row.FindControl("lblH17") as Label;
                rcount += Convert.ToInt32(lbl17.Text);
                if (Convert.ToInt32(lbl17.Text) == 0)
                {
                    lbl17.Text = "  ";
                }
                else if (Convert.ToInt32(lbl17.Text) > 0)
                {
                    e.Row.Cells[17].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl18 = e.Row.FindControl("lblH18") as Label;
                rcount += Convert.ToInt32(lbl18.Text);
                if (Convert.ToInt32(lbl18.Text) == 0)
                {
                    lbl18.Text = "  ";
                }
                else if (Convert.ToInt32(lbl18.Text) > 0)
                {
                    e.Row.Cells[18].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl19 = e.Row.FindControl("lblH19") as Label;
                rcount += Convert.ToInt32(lbl19.Text);
                if (Convert.ToInt32(lbl19.Text) == 0)
                {
                    lbl19.Text = "  ";
                }
                else if (Convert.ToInt32(lbl19.Text) > 0)
                {
                    e.Row.Cells[19].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl20 = e.Row.FindControl("lblH20") as Label;
                rcount += Convert.ToInt32(lbl20.Text);
                if (Convert.ToInt32(lbl20.Text) == 0)
                {
                    lbl20.Text = "  ";
                }
                else if (Convert.ToInt32(lbl20.Text) > 0)
                {
                    e.Row.Cells[20].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl21 = e.Row.FindControl("lblH21") as Label;
                rcount += Convert.ToInt32(lbl21.Text);
                if (Convert.ToInt32(lbl21.Text) == 0)
                {
                    lbl21.Text = "  ";
                }
                else if (Convert.ToInt32(lbl21.Text) > 0)
                {
                    e.Row.Cells[21].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl22 = e.Row.FindControl("lblH22") as Label;
                rcount += Convert.ToInt32(lbl22.Text);
                if (Convert.ToInt32(lbl22.Text) == 0)
                {
                    lbl22.Text = "  ";
                }
                else if (Convert.ToInt32(lbl22.Text) > 0)
                {
                    e.Row.Cells[22].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl23 = e.Row.FindControl("lblH23") as Label;
                rcount += Convert.ToInt32(lbl23.Text);
                if (Convert.ToInt32(lbl23.Text) == 0)
                {
                    lbl23.Text = "  ";
                }
                else if (Convert.ToInt32(lbl23.Text) > 0)
                {
                    e.Row.Cells[23].BackColor = System.Drawing.Color.LightPink;
                }

                Label lbl24 = e.Row.FindControl("lblH24") as Label;
                rcount += Convert.ToInt32(lbl24.Text);
                if (Convert.ToInt32(lbl24.Text) == 0)
                {
                    lbl24.Text = "  ";
                }
                else if (Convert.ToInt32(lbl24.Text) > 0)
                {
                    e.Row.Cells[24].BackColor = System.Drawing.Color.LightPink;
                }

                Label total = e.Row.FindControl("lbltotal") as Label;
                int hour = rcount / 60;
                int min = rcount % 60;
                string st1 = string.Format("{0:00}", min);
                total.Text = hour.ToString() + " Hr " + st1 + " Min";               
                count += rcount;
                rcount = 0;

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label ftotal = e.Row.FindControl("ftotal") as Label;
                int h = count / 60;
                int m = count % 60;
                ftotal.Text = h.ToString() + " Hr " + m.ToString() + " Min";

            }
        }
        protected void Download_Click(object sender, EventArgs e)
        {
            if (ddlmonth.SelectedValue.ToString() != "00")
            {
                Response.Clear();
                Response.Buffer = true;
                DateTime dt = DateTime.Now;
                int month = Convert.ToInt32(ddlmonth.SelectedValue.ToString());
                DateTime fdt = FirstDayOfMonthFromDateTime(dt, month);
                string str1 = ddlmonth.SelectedItem.Text + "_" + fdt.Year;
                string vstr = ddlMapTOVehicle.SelectedValue.ToString();
                string str2 = "VTR_" + vstr.Replace(" ", "") + "_" + str1 + ".xls";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", str2));
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                Table tb = new Table();
                TableRow tr1 = new TableRow();
                TableCell cell1 = new TableCell();
                cell1.Controls.Add(gvreport);
                tr1.Cells.Add(cell1);
                tb.Rows.Add(tr1);
                tb.RenderControl(hw);

                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

            }
        }
        protected void Refresh_Click(object sender, EventArgs e)
        {
            ddlMapTOVehicle.Items.Clear();
            Response.Redirect(Request.RawUrl);
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;


namespace Tracking
{
    public partial class RouteAssigntoVehicle : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
#pragma warning disable CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
#pragma warning restore CS0618 // 'ExtMapTypeControl' is obsolete: 'Not available in Google Maps v3'
                GMap1.Add(extMapType);
                Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(15.3172775, 75.7138884);
                GMap1.setCenter(gLatLng, 4, Subgurim.Controles.GMapType.GTypes.Normal);
                GMap1.enableHookMouseWheelToZoom = true;

                binddata();
            }

        }
        public void binddata()
        {
            DataTable routenos = db.get_Geo_RouteNo1(Session["UserID"].ToString());
                DataTable routno = db.get_Geo_RouteNo1new(Session["UserID"].ToString());
                for (int i = 0; i < routno.Rows .Count ; i++)
                {
                    DataRow dr = routenos.NewRow();
                    dr["Route_No"] = routno.Rows[i][0].ToString();
                    routenos.Rows.Add(dr);

                }
            GMap1.resetMarkers();
            GMap1.resetPolygon();
            lblsrno.Text = "";
            lblsvno.Text = "";
            ddlRoutes.DataSource = routenos;//db.get_Geo_RouteNo1(Session["UserID"].ToString());
            ddlRoutes.DataBind();

            ddlMapTOVehicle.DataSource = db.GetUnAssingedRouteVehicles(Session["UserID"].ToString());
            ddlMapTOVehicle.DataBind();

            gvrecords.DataSource = db.GetAssingedRouteVehicles(Session["UserID"].ToString());
            gvrecords.DataBind();

        }
        protected void btMap_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            DateTime dtlast = dt.AddDays(90);
            db.AssignRoutetoVehicle(ddlRoutes.SelectedValue.ToString(), ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString(), DateTime.Now.ToString("dd-MM-yyyy hh:mm tt"));
            db.Assign_Routes_Vehicle(ddlRoutes.SelectedValue.ToString(), ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString(), dt.ToShortDateString(), dtlast.ToShortDateString() + " " + "11:59 PM");
            binddata();
        }
        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
                string username = gvrow.Cells[1].Text;
                string Route_no = gvrow.Cells[2].Text;
                db.DeAssingedRouteVehicles(Session["UserID"].ToString(), username);
                db.DeAssinged_Routes_Vehicles(Session["UserID"].ToString(), username, Route_no, DateTime.Now.ToShortDateString() + " " + "11:59 PM");
                binddata();
            }

        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvrow = btn.NamingContainer as GridViewRow;
            string userid = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
            string username = gvrow.Cells[2].Text;
            string sub = username.Substring(0, 3);
            if (sub == "ROU")
            {
                string Vno = gvrow.Cells[1].Text;
                lblsrno.Text = "Selected Route No :" + username;
                lblsvno.Text = "Selected Vehicle No :" + Vno;
                bindpointsnew1(username, Vno);

            }
            else
            {
                string Vno = gvrow.Cells[1].Text;
                lblsrno.Text = "Selected Route No :" + username;
                lblsvno.Text = "Selected Vehicle No :" + Vno;
                bindpoints(username, Vno);
            }

        }
        public void bindpoints(string rno, string vno)
        {

            GMap1.resetMarkers();
            GMap1.resetPolygon();
            vehiclepresentloc(vno);
            DataTable dt = db.get_Geo_Routepoints(rno, Session["UserID"].ToString());
            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
            {
                drawcircle(Convert.ToDouble(dt.Rows[i][4].ToString()), Convert.ToDouble(dt.Rows[i][5].ToString()), Convert.ToDouble(dt.Rows[i][6].ToString()) / 1609.3, j, rno);
            }
        }
        public void bindpointsnew1(string rno, string vno)
        {

            GMap1.resetMarkers();
            GMap1.resetPolygon();
            vehiclepresentloc(vno);
            DataTable dt = db.get_Geo_Routepointsnew(rno, Session["UserID"].ToString());
            
            for (int i = 0, j = 1; i < dt.Rows.Count; i++, j++)
            {
                string rname = dt.Rows[i][6].ToString();
                drawcircle1(Convert.ToDouble(dt.Rows[i][3].ToString()), Convert.ToDouble(dt.Rows[i][4].ToString()), Convert.ToDouble(dt.Rows[i][5].ToString()) / 1609.3, j, rno,rname);
            }
        }
        public void drawcircle(double lt, double lg, double r, int gp, string vn)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }   
            GMap1.Add(new GPolygon(extp, "#00FF00", 0.1));
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 10, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
            GMap1.Add(oMarker);
            options.title = "Route No " + vn ;
            oMarker.options = options;
        }
        public void drawcircle1(double lt, double lg, double r, int gp, string vn, string rname)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            GMap1.Add(new GPolygon(extp, "#00FF00", 0.1));
            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 10, Subgurim.Controles.GMapType.GTypes.Normal);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
            GMap1.Add(oMarker);
            options.title = "Route No " + vn + "," + "Location:-" + rname; 
            oMarker.options = options;
        }
        public void vehiclepresentloc(string vno)
        {
            DataTable status = db.GetVehiclePresentlocation(vno, Session["UserID"].ToString());
            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(status.Rows[0][0]), Convert.ToDouble(status.Rows[0][1]));

            GMap1.setCenter(gLatLng);
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter("V", System.Drawing.Color.Green, System.Drawing.Color.Red);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
            GMap1.Add(oMarker);
            options.title = vno;
            oMarker.options = options;

        }
    }
}

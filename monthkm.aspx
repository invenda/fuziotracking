
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="monthkm.aspx.cs" Inherits="Tracking.monthkm"
    MasterPageFile="~/ESMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > yesday) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = yesday;  
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="600"
        AsyncPostBackErrorMessage="Please Ttry agaian">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto;">
        <div style="text-align: center; color: Blue; height: 32px;">
            <h2>
                Month KM Report
            </h2>
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div style="width: 1100px; height: auto; margin: 0 auto 0 85px; padding: 0; padding: 0;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="110%">
                        <tr align="left">
                            <td align="right">
                                <asp:Label ID="Label1" Text="Select Vehicle :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlMapTOVehicle"
                                    ValidationGroup="Group1" ErrorMessage="Select the Vehicle Number" Operator="NotEqual"
                                    ValueToCompare="--">*</asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="150px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Label ID="Label4" Text="Select month :" runat="server" Font-Bold="true"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlmonth"
                                    ValidationGroup="Group1" ErrorMessage="Select the Month" Operator="NotEqual"
                                    ValueToCompare="00">*</asp:CompareValidator>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlmonth" runat="server" Width="150px" ValidationGroup="Group1">
                                </asp:DropDownList>
                            </td>
                            <%--<asp:DropDownList ID="ddlmonth" runat="server" Width="150px">
                        </asp:DropDownList>--%>
                            <td>
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                    OnClick="GetRecord_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Download" Text="Download Record" runat="server" Width="120px" OnClick="Download_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh Record" runat="server" Width="121px" OnClick="Refresh_Click" />
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div style="height: 20px;">
                    </div>
                    <div style="width: 100%; height: auto; text-align: center; vertical-align: top">
                        <asp:Panel ID="Panel1" runat="server" Visible="false" Width="100%">
                            <table cellpadding="0" cellspacing="0" style="width: auto; height: auto; margin-right: auto;
                                margin-left: auto; border-style: double;">
                                <tr align="right">
                                    <td valign="top">
                                        <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="355px" OnRowCreated="gvreport_RowCreated"
                                            OnRowDataBound="gvreport_RowDataBound">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                                BorderWidth="1" />
                                            <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="150px" FooterStyle-BorderColor="#61A6F8">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle BorderColor="Black"></FooterStyle>
                                                    <HeaderStyle BorderColor="Black" Width="120px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Run KM" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("KM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="100px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fuel Fill In liter" HeaderStyle-Width="200px" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfuel3" runat="server" Text='<% #Bind("Fuel") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="90px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 10px; border-style: dotted;">
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvreport1" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="355px" OnRowCreated="gvreport1_RowCreated"
                                            OnRowDataBound="gvreport1_RowDataBound">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center"
                                                BorderWidth="0" />
                                            <RowStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="150px" FooterStyle-BorderColor="#61A6F8">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle BorderColor="Black"></FooterStyle>
                                                    <HeaderStyle Width="120px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Run KM" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("KM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="100px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fuel Fill In liter" HeaderStyle-Width="200px" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfuel1" runat="server" Text='<% #Bind("Fuel") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="90px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 10px; border-style: dotted;">
                                        <%--  border-style: dotted;">--%>
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="gvreport2" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center"
                                            GridLines="Both" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            RowStyle-ForeColor="Black" ShowFooter="true" Width="355px" OnRowCreated="gvreport2_RowCreated"
                                            OnRowDataBound="gvreport2_RowDataBound">
                                            <HeaderStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" />
                                            <FooterStyle BackColor="#61A6F8" Font-Bold="true" ForeColor="Black" HorizontalAlign="Center" />
                                            <RowStyle VerticalAlign="Bottom" HorizontalAlign="Center" ForeColor="Black" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="150px" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<% #Bind("Date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotall" runat="server" ForeColor="Black" Font-Bold="true" Text="Total :"></asp:Label>
                                                    </FooterTemplate>
                                                    <HeaderStyle Width="120px"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Run KM" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotal" runat="server" Text='<% #Bind("KM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="ftotal" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                    <HeaderStyle HorizontalAlign="Right" Width="100px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fuel Fill In liter" HeaderStyle-Width="200px" FooterStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfuel2" runat="server" Text='<% #Bind("Fuel") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lbltotallFuel" runat="server" ForeColor="Black" Font-Bold="true"></asp:Label>
                                                    </FooterTemplate>
                                                    <HeaderStyle Width="90px"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div style="height: 20px;">
        </div>
    </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fuel.aspx.cs" Inherits="Tracking.Fuel"
    MasterPageFile="~/ESMaster.Master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="report4" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            var today = new Date();
            var yesday = new Date();
            yesday.setDate(today.getDate() - 1);
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))

            } else if (sender._selectedDate <= today && sender._selectedDate > yesday) {
                var btn = document.getElementById("<%=btnCalculate.ClientID%>");
                btn.disabled = true;
            }
        }

        function WaterMark(txtName, event) {
            var defaultText = "Select Date";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        } 
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="width: 100%; height: auto; background-color: White;">
        <div style="width: 100%; height: 40px; font-size: 25px; text-align: center; color: Blue;">
            <asp:Label ID="lblhead" Text="Vehicle fuel V/S Kilometer Graph" runat="server"></asp:Label>
        </div>
        <div style="width: 100%; height: 40px;">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ValidationGroup="Group1" ShowSummary="False" />
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td align="right" style="width: 200px;">
                                <asp:Label ID="Label1" Text="Select Vehicle" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                    ControlToValidate="ddlMapTOVehicle" ErrorMessage="- Select Vehicle">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 150px;">
                                <asp:DropDownList ID="ddlMapTOVehicle" runat="server" DataValueField="Vehicalnumber"
                                    DataTextField="Vehicalnumber" Height="23px" Width="130px" Style="margin-left: 1px">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 100px;">
                                <asp:Label ID="Label4" Text="Select Date" runat="server" Font-Bold="true"></asp:Label>:
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="Group1" ControlToValidate="date"
                                    ErrorMessage="- Select Date">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 130px; margin-left: 40px;">
                                <asp:TextBox ID="date" runat="server" Width="120px" Text="Select Date" ForeColor="Gray" onblur = "WaterMark(this, event);" onfocus = "WaterMark(this, event);"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="M/d/yyyy" TargetControlID="date" OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="width: 100px;" align="right">
                            </td>
                            <td style="width: 100px;">
                                <asp:Button ID="GetRecord" Text="Get Record" runat="server" Height="25px" ValidationGroup="Group1"
                                    OnClick="GetRecord_Click" />
                            </td>
                            <td style="width: 120px;" align="center">
                                <asp:Button ID="Download" Text="Download" runat="server" Width="87px" ValidationGroup="Group1"
                                    Enabled="false" OnClick="Download_Click" />
                            </td>
                            <td>
                                <asp:Button ID="Refresh" Text="Refresh" runat="server" Width="70px" OnClick="Refresh_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCalculate" Text="Calculate" runat="server" Width="70px" ValidationGroup="Group1"
                                    OnClick="btnCalculate_Click" />
                            </td>
                        </tr>
                    </table>
                    <div style="height: 15px;">
                    </div>
                    <%--<asp:UpdatePanel ID="pnlCharts" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                    <div style="height: auto; text-align: center; color: #FE9A2E;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h1>
                                                    <img alt="" src="Images/Processing.gif" style="height: 120px" />
                                                </h1>
                                            </td>
                                            <td>
                                                <h3>
                                                    Please wait.....
                                                </h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="false">
                        <div style="text-align: center;">
                            <asp:Chart ID="Chart2" runat="server" Height="500px" Width="1300px" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)"
                                ImageType="Png" BackGradientStyle="TopBottom" BackSecondaryColor="White" Palette="None"
                                BorderlineDashStyle="Solid" BorderColor="26, 59, 105" BorderlineWidth="0" EnableViewState="true"
                                SuppressExceptions="True">
                                <BorderSkin SkinStyle="FrameThin6" />
                                <Titles>
                                    <asp:Title Docking="Left" Font="Trebuchet MS, 10.25pt,style=Bold" Text="Fuel Tank Capacity in Litres"
                                        BackColor="Transparent" DockingOffset="0" ForeColor="Chocolate">
                                    </asp:Title>
                                    <asp:Title Docking="Bottom" Font="Trebuchet MS, 9pt" BackColor="Transparent" DockingOffset="2"
                                        ForeColor="Blue">
                                    </asp:Title>
                                    <asp:Title Docking="Bottom" Font="Trebuchet MS, 9pt" BackColor="Transparent" DockingOffset="2"
                                        Alignment="BottomLeft" ForeColor="Blue">
                                    </asp:Title>
                                </Titles>
                                <Legends>
                                    <asp:Legend Enabled="false" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                                        LegendStyle="Row" Docking="Bottom" Font="Trebuchet MS, 10pt, style=Bold">
                                    </asp:Legend>
                                </Legends>
                                <Series>
                                    <asp:Series Name="Fueltank" ChartType="Line" ShadowColor="Black" BorderColor="180, 26, 59, 105"
                                        YAxisType="Primary" Color="Blue">
                                    </asp:Series>
                                    <asp:Series Name="Fuel" ChartType="Spline" ShadowColor="Black" BorderColor="180, 26, 59, 105"
                                        YAxisType="Primary" Color="Red" MarkerStyle="Diamond" MarkerBorderWidth="3" MarkerColor="Red">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                                        BackSecondaryColor="White" BackColor="Gainsboro" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                        <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                                            WallWidth="0" IsClustered="False"></Area3DStyle>
                                        <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                        </AxisY>
                                        <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                        <div style="text-align: center;">
                            <asp:Chart ID="Chart1" runat="server" Height="550px" Width="1300px" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)"
                                ImageType="Png" BackColor="WhiteSmoke" BorderWidth="0" BackGradientStyle="TopBottom"
                                EnableViewState="true" BackSecondaryColor="White" Palette="None" BorderlineDashStyle="Solid"
                                BorderColor="26, 59, 105" BorderlineWidth="0" SuppressExceptions="True">
                                <BorderSkin SkinStyle="FrameThin6" />
                                <Titles>
                                    <asp:Title Docking="Left" Font="Trebuchet MS, 10.25pt,style=Bold" Text="Vehicle Run Kilometers"
                                        BackColor="Transparent" DockingOffset="0" ForeColor="Chocolate">
                                    </asp:Title>
                                    <asp:Title Docking="Bottom" Font="Trebuchet MS, 10pt" BackColor="Transparent" DockingOffset="-1"
                                        ForeColor="Blue">
                                    </asp:Title>
                                </Titles>
                                <Legends>
                                    <asp:Legend Enabled="false" IsTextAutoFit="False" Name="Default" BackColor="Transparent"
                                        LegendStyle="Row" Docking="Bottom" Font="Trebuchet MS, 10pt, style=Bold">
                                    </asp:Legend>
                                </Legends>
                                <Series>
                                    <asp:Series Name="kilometers" ChartType="Spline" ShadowColor="Black" BorderColor="180, 26, 59, 105"
                                        YAxisType="Primary" Color="Green" MarkerStyle="Diamond" MarkerBorderWidth="2"
                                        MarkerColor="Green">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BorderDashStyle="Solid"
                                        BackSecondaryColor="White" BackColor="Gainsboro" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                        <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="False"
                                            WallWidth="0" IsClustered="False"></Area3DStyle>
                                        <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False" Maximum="600" Minimum="0"
                                            Interval="20">
                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                        </AxisY>
                                        <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Download" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <%-- </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="GetRecord" />
                <asp:PostBackTrigger ControlID="Refresh" />
            </Triggers>
        </asp:UpdatePanel>--%>
        <%--<table style="width: 100%">
            <tr>
                <td valign="top" style="width: 12%; height: 20px; text-align: left;">
                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="true" Text="<< Previous"
                        Font-Underline="false" ForeColor="Blue" OnClick="LinkButton1_Click"> </asp:LinkButton>
                </td>
               <td valign="top" style="width: 78%; height: 20px; text-align:center;">
                <asp:Label ID="Labe" runat="server" Font-Bold="true"></asp:Label>:
                </td>
                <td valign="top" style="width: 100%; height: 20px; text-align: right;">
                    <asp:LinkButton ID="linknext" runat="server" Font-Bold="true" Text="Next >>" Font-Underline="false"
                        ForeColor="Blue" ValidationGroup="Group1" OnClick="linknext_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>--%>
    </div>
</asp:Content>

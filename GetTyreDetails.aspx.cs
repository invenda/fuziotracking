﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Globalization;


namespace Tracking
{
    public partial class GetTyreDetails : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();

                    }
                    else
                    {
                        string jbj = ((Label)Master.FindControl("Label1")).ToString();
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                        DataTable totaltunkm = db.getTotalRunkm(ddlMapTOVehicle.SelectedValue.ToString());
                        if (totaltunkm.Rows.Count > 0)
                        {
                            Session["totalrunkm"] = totaltunkm.Rows[0][3].ToString();

                        }
                        Totalrunkm(ddlMapTOVehicle.SelectedValue.ToString());
                        Gettyredetails();
                        
                    }

                }
            }

        }
        public void Gettyredetails()
        {
            Clear();
            DataTable dt = db.Gettyredetails(ddlMapTOVehicle.SelectedValue.ToString());
            if (dt.Rows.Count > 0)
            {
                txtNoOfTyres.Text = dt.Rows[0][1].ToString();
                txtFtontLeft.Text = dt.Rows[0][2].ToString();
                txtFrontleftytrelimit.Text = dt.Rows[0][3].ToString();
                txtFtontRight1.Text = dt.Rows[0][5].ToString();
                txtFronrighttytrelimit.Text = dt.Rows[0][6].ToString();
                txtrearleft1.Text = dt.Rows[0][8].ToString();
                txtrearleft1limit.Text = dt.Rows[0][9].ToString();
                txtrearleft2.Text = dt.Rows[0][11].ToString();
                txtrearleft2limit.Text = dt.Rows[0][12].ToString();
                txtrearleft3.Text = dt.Rows[0][14].ToString();
                txtrearleft3limit.Text = dt.Rows[0][15].ToString();
                txtrearleft4.Text = dt.Rows[0][17].ToString();
                txtrearleft4limit.Text = dt.Rows[0][18].ToString();
                txtrearRight1.Text = dt.Rows[0][20].ToString();
                txtreartight1limit.Text = dt.Rows[0][21].ToString();
                txtrearRight2.Text = dt.Rows[0][23].ToString();
                txtreartight2limit.Text = dt.Rows[0][24].ToString();
                txtrearRight3.Text = dt.Rows[0][26].ToString();
                txtreartight3limit.Text = dt.Rows[0][27].ToString();
                txtrearRight4.Text = dt.Rows[0][29].ToString();
                txtreartight4limit.Text = dt.Rows[0][30].ToString();
                //DataTable totaltunkm = db.getTotalRunkm(ddlMapTOVehicle.SelectedValue.ToString());
                if (Session["totalrunkm"] != null)
                {
                    double trkm = Convert.ToDouble(Session["totalrunkm"].ToString());
                    if (!DBNull.Value.Equals(dt.Rows[0][4]))
                    {
                        double v1 = Convert.ToDouble(dt.Rows[0][2].ToString()) +Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][4].ToString()));
                        
                        txttoatalfrontleft.Text =v1.ToString();
                        if (v1 >= Convert.ToDouble(dt.Rows[0][3].ToString()))
                        {
                            txttoatalfrontleft.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }

                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][7]))
                    {
                        double v2 = Convert.ToDouble(dt.Rows[0][5].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][7].ToString()));
                        txttoatalfrontright.Text = v2.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][7].ToString()));
                        if (v2 >= Convert.ToDouble(dt.Rows[0][6].ToString()))
                        {
                            txttoatalfrontright.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][10]))
                    {
                        double v3 = Convert.ToDouble(dt.Rows[0][8].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][10].ToString()));
                        txttotalreatleft1.Text = v3.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][10].ToString()));
                        if (v3 >= Convert.ToDouble(dt.Rows[0][9].ToString()))
                        {
                            txttotalreatleft1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][13]))
                    {
                        double v4 = Convert.ToDouble(dt.Rows[0][11].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][13].ToString()));
                        txttotalreatleft2.Text = v4.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][13].ToString()));
                        if (v4 >= Convert.ToDouble(dt.Rows[0][12].ToString()))
                        {
                            txttotalreatleft2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][16]))
                    {
                        double v5 = Convert.ToDouble(dt.Rows[0][14].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][16].ToString()));
                        txttotalreatleft3.Text = v5.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][16].ToString()));
                        if (v5 >= Convert.ToDouble(dt.Rows[0][15].ToString()))
                        {
                            txttotalreatleft3.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][19]))
                    {
                        double v6 = Convert.ToDouble(dt.Rows[0][17].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][19].ToString()));
                        txttotalreatleft4.Text = v6.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][19].ToString()));
                        if (v6 >= Convert.ToDouble(dt.Rows[0][18].ToString()))
                        {
                            txttotalreatleft4.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][22]))
                    {
                        double v7 = Convert.ToDouble(dt.Rows[0][20].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][22].ToString()));
                        txttotalrearight1.Text = v7.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][22].ToString()));
                        if (v7 >= Convert.ToDouble(dt.Rows[0][21].ToString()))
                        {
                            txttotalrearight1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][25]))
                    {
                        double v8 = Convert.ToDouble(dt.Rows[0][23].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][25].ToString()));
                        txttotalrearight2.Text = v8.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][25].ToString()));
                        if (v8 >= Convert.ToDouble(dt.Rows[0][24].ToString()))
                        {
                            txttotalrearight2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                       
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][28]))
                    {
                        double v9 = Convert.ToDouble(dt.Rows[0][26].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][28].ToString()));
                        txttotalrearight3.Text = v9.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][28].ToString()));
                        if (v9 >= Convert.ToDouble(dt.Rows[0][27].ToString()))
                        {
                            txttotalrearight3.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }

                       
                    }
                    if (!DBNull.Value.Equals(dt.Rows[0][31]))
                    {
                        double v10 = Convert.ToDouble(dt.Rows[0][29].ToString()) + Convert.ToDouble(trkm - Convert.ToDouble(dt.Rows[0][31].ToString()));
                        txttotalrearight4.Text = v10.ToString();// Convert.ToString(trkm - Convert.ToDouble(dt.Rows[0][31].ToString()));
                        if (v10 >= Convert.ToDouble(dt.Rows[0][30].ToString()))
                        {
                            txttotalrearight4.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        }
                       
                    }
                }
                else
                {
                    txttoatalfrontleft.Text = dt.Rows[0][4].ToString();
                    txttoatalfrontright.Text = dt.Rows[0][7].ToString();
                    txttotalreatleft1.Text = dt.Rows[0][10].ToString();
                    txttotalreatleft2.Text = dt.Rows[0][13].ToString();
                    txttotalreatleft3.Text = dt.Rows[0][16].ToString();
                    txttotalreatleft4.Text = dt.Rows[0][19].ToString();
                    txttotalrearight1.Text = dt.Rows[0][22].ToString();
                    txttotalrearight2.Text = dt.Rows[0][25].ToString();
                    txttotalrearight3.Text = dt.Rows[0][28].ToString();
                    txttotalrearight4.Text = dt.Rows[0][31].ToString();
                }
            }
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable totaltunkm = db.getTotalRunkm(ddlMapTOVehicle.SelectedValue.ToString());
            if (totaltunkm.Rows.Count > 0)
            {
                Session["totalrunkm"] = totaltunkm.Rows[0][3].ToString();

            }
            Totalrunkm(ddlMapTOVehicle.SelectedValue.ToString());
            Gettyredetails();
           
        }
        public void Clear()
        {
            txtNoOfTyres.Text = string.Empty;
            txtFtontLeft.Text = string.Empty;
            txtFrontleftytrelimit.Text = string.Empty;
            txtFtontRight1.Text = string.Empty;
            txtFronrighttytrelimit.Text = string.Empty;
            txtrearleft1.Text = string.Empty;
            txtrearleft1limit.Text = string.Empty;
            txtrearleft2.Text = string.Empty;
            txtrearleft2limit.Text = string.Empty;
            txtrearleft3.Text = string.Empty;
            txtrearleft3limit.Text = string.Empty;
            txtrearleft4.Text = string.Empty;
            txtrearleft4limit.Text = string.Empty;
            txtrearRight1.Text = string.Empty;
            txtreartight1limit.Text = string.Empty;
            txtrearRight2.Text = string.Empty;
            txtreartight2limit.Text = string.Empty;
            txtrearRight3.Text = string.Empty;
            txtreartight3limit.Text = string.Empty;
            txtrearRight4.Text = string.Empty;
            txtreartight4limit.Text = string.Empty;
            txttoatalfrontleft.Text = string.Empty;
            txttoatalfrontright.Text = string.Empty;
            txttotalreatleft1.Text = string.Empty;
            txttotalreatleft2.Text = string.Empty;
            txttotalreatleft3.Text = string.Empty;
            txttotalreatleft4.Text = string.Empty;
            txttotalrearight1.Text = string.Empty;
            txttotalrearight2.Text = string.Empty;
            txttotalrearight3.Text = string.Empty;
            txttotalrearight4.Text = string.Empty;
            //clear the back color
            txttoatalfrontleft.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttoatalfrontright.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalreatleft1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalreatleft2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalreatleft3.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalreatleft4.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalrearight1.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalrearight2.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalrearight3.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            txttotalrearight4.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");


        }
        public void Totalrunkm(string vno)
        {
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
            BLClass bl12 = new BLClass();
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 // The type 'BLClass' in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs' conflicts with the imported type 'BLClass' in 'Tracking, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null'. Using the type defined in 'C:\Users\user\Downloads\Tracking-20180927T123440Z-001\Tracking\BLClass.cs'.
 
 
            DBClass db = new DBClass();
 
 
            DataTable dt = db.getTotalRunkm(vno);
            if (dt.Rows.Count != 0)
            {
                string fdate = dt.Rows[0][2].ToString();
                double trkm = Convert.ToDouble(dt.Rows[0][3].ToString());
                string tdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                DataTable dt1 = db.Select_TotalRunkm(dt.Rows[0][1].ToString(), fdate, tdate);
                if (dt1.Rows.Count != 0)
                {
                    for (int m = 0, n = 1; m < dt1.Rows.Count - 1; m++, n++)
                    {
                        trkm += bl12.CalcDistance(Convert.ToDouble(dt1.Rows[m][0].ToString()), Convert.ToDouble(dt1.Rows[m][1].ToString()), Convert.ToDouble(dt1.Rows[n][0].ToString()), Convert.ToDouble(dt1.Rows[n][1].ToString()));
                    }
                    fdate = dt1.Rows[dt1.Rows.Count - 1][3].ToString();
                    trkm = (double)Math.Round(trkm, 2);

                }
                else
                {
                    fdate = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                }

                db.IU_Totalrunkm(vno, fdate, trkm.ToString());
                //DataTable P = db.getPFE(vno);
                //DataTable CKMdt = db.Get_Correction_KM(vno);
                //double ckm = 0;
                //if (CKMdt.Rows[0][0].ToString() != "")
                //{
                //    ckm = Convert.ToDouble(CKMdt.Rows[0][0].ToString());
                //}
                //double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                //nw = trkm + nw + ckm;
                //lblTotKmRun.Text = nw.ToString() + "&nbsp;&nbsp;KM";
            }
            else
            {
                DataTable P = db.getPFE(vno);
                var dtfi = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", DateSeparator = "/" };
                double nw = Convert.ToDouble(P.Rows[0][0].ToString());
                string ftime = P.Rows[0][1].ToString();
                DateTime fdate = Convert.ToDateTime(ftime, dtfi);
                db.IU_Totalrunkm(vno, fdate.ToString(), "0");
            }

        }
    }
}
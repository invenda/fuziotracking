﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Subgurim.Controles.GoogleChartIconMaker;

namespace Tracking
{
    public partial class TowedRouteReplay : System.Web.UI.Page
    {
        static string googleMapsAPIKey = "AIzaSyCa4aj-rxLS6zU8DPrcY-aOQm4mnoGULT4";
        static string reverseGeocodeFormatString = "https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}";

        protected void Page_Load(object sender, EventArgs e)
        {
            GMapUIOptions options = new GMapUIOptions();
            options.maptypes_hybrid = true;
            options.keyboard = false;
            options.maptypes_physical = false;
            options.zoom_scrollwheel = true;
            GMap1.Add(new GMapUI(options));
            //for zoom control
            GControl control = new GControl(GControl.preBuilt.SmallMapControl);
            GControl control2 = new GControl(GControl.preBuilt.SmallZoomControl, new GControlPosition(GControlPosition.position.Top_Left));
            GMap1.Add(control);
            GMap1.Add(control2);
            if (!IsPostBack)
            {
 
 
                DBClass db = new DBClass();
 
 
                if (Session["UserID"] != null)
                {
                    if (Session["UserRole"].ToString() == "3")
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["cUser_id"].ToString());
                        ddlMapTOVehicle.DataBind();
                        //DataTable CUsers = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
                        //ddlMapTOVehicle.DataSource = db.Get_CAdminVehicles(Session["UserID"].ToString(), CUsers);
                        //ddlMapTOVehicle.DataBind();
                    }
                    else
                    {
                        ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                        ddlMapTOVehicle.DataBind();
                    } 

                    ddlFromHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                    ddlFromHOURS.DataBind();

                    ddlFromMINUTES.DataSource = db.GetFromMinutes(Session["UserID"].ToString());
                    ddlFromMINUTES.DataBind();

                    ddlToHOURS.DataSource = db.GetFromHours(Session["UserID"].ToString());
                    ddlToHOURS.DataBind();

                    ddlToMINUTES.DataSource = db.GetFromMinutes(Session["ParentUser"].ToString());
                    ddlToMINUTES.DataBind();
                    string sStreetAddress = null;
                    string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                    Subgurim.Controles.GeoCode GeoCode = default(Subgurim.Controles.GeoCode);

                    sStreetAddress = "Jaynagar, Bangalore";
                    //GeoCode = GMap1.getGeoCodeRequest(sStreetAddress, sMapKey);

                  //  double lat = GeoCode.Placemark.coordinates.lat;

                    System.Net.WebClient client = new System.Net.WebClient();

                    string urlstring = string.Format(reverseGeocodeFormatString, sStreetAddress, googleMapsAPIKey);
                    string response = client.DownloadString(urlstring);
                    dynamic jsonresponse = Newtonsoft.Json.JsonConvert.DeserializeObject(response);

                    if (jsonresponse.results.Count > 0)
                    {
                        string lat = jsonresponse.results[0].geometry.location.lat;
                        string lng = jsonresponse.results[0].geometry.location.lng;
                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(double.Parse(lat), double.Parse(lng));
                        GMap1.setCenter(gLatLng, 16, Subgurim.Controles.GMapType.GTypes.Normal);
                    }


                    

                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btView_Click(object sender, EventArgs e)
        {
            DateTime dhdh = Convert.ToDateTime(TextBox1.Text);
            int emonth = dhdh.Month;
            DateTime curretmonth = DateTime.Now;
            int cmonth = curretmonth.Month;
            if (emonth == cmonth)
            {
                binddata();

            }
            else
            {
                binddataold();

            }
        }
        public void binddata()
        {
    
 
 
            DBClass db = new DBClass();
 
 
            string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            GMap1.resetMarkers();
            GMap1.resetPolylines();
            GMap1.enableScrollWheelZoom = true;
            string AMPM, DT;
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
            string VES = ddlVehStatRpt.SelectedValue.ToString();
            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();
            DateTime fdate = Convert.ToDateTime(TextBox1.Text + " " + fromtime + " " + AMPM);
            DateTime tdate = Convert.ToDateTime(TextBox1.Text + " " + totime + " " + DT);
            string vno = ddlMapTOVehicle.SelectedValue.ToString();
            if (VES == "1")
            {
                DataTable Towesdt = db.Towed_Alerts(vno, "TOWED", fdate.ToString(), tdate.ToString());
                for (int i = 0; i < Towesdt.Rows.Count; i++)
                {
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;
                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();
                    string date = Towesdt.Rows[i][0].ToString();
                    string hr = Towesdt.Rows[i][1].ToString();
                    string min = Towesdt.Rows[i][2].ToString() + "9";
                    DateTime ttime = Convert.ToDateTime(date + " " + hr + ":" + min);
                    DateTime ftime = ttime.AddMinutes(-10);
                    DataTable dt = db.Get_Towed_Points(vno, ftime.ToString(), ttime.ToString());
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {

                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.addControl(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        SPin sPin;

                            sPin = new SPin(0.5, 0, System.Drawing.Color.Yellow, 14, PinFontStyle.bold, "");

                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        options.icon = icon;
                        double speed;
                        string sped;
                        speed = Convert.ToDouble(dt.Rows[j][2].ToString());
                        sped = Convert.ToString(speed * 1.85);
                        options.title = "Time :" + dt.Rows[j][5].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                        oMarker.options = options;
                        GMap1.addGMarker(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                        points.Add(latlng);

                        GPolyline line = new GPolyline(points, "40FF00", 2);
                        GMap1.addPolyline(line);

                    }
                }
            }
        }
        public void binddataold()
        {

 
 
            DBClass db = new DBClass();
 
 
            string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
            GMap1.resetMarkers();
            GMap1.resetPolylines();
            GMap1.enableScrollWheelZoom = true;
            string AMPM, DT;
            string fromtime = ddlFromHOURS.SelectedValue + ddlFromMINUTES.SelectedValue;
            string totime = ddlToHOURS.SelectedValue + ddlToMINUTES.SelectedValue;
            string VES = ddlVehStatRpt.SelectedValue.ToString();
            AMPM = rbtnAMPM.SelectedValue.ToString();
            DT = rbtnDT.SelectedValue.ToString();
            DateTime fdate = Convert.ToDateTime(TextBox1.Text + " " + fromtime + " " + AMPM);
            DateTime tdate = Convert.ToDateTime(TextBox1.Text + " " + totime + " " + DT);
            string vno = ddlMapTOVehicle.SelectedValue.ToString();
            if (VES == "1")
            {
                DataTable Towesdt = db.Towed_Alertsold(vno, "TOWED", fdate.ToString(), tdate.ToString());
                for (int i = 0; i < Towesdt.Rows.Count; i++)
                {
                    List<GLatLng> points = new List<GLatLng>();
                    GLatLng latlng = null;
                    Subgurim.Controles.GMarker oMarker = null;
                    Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon();
                    string date = Towesdt.Rows[i][0].ToString();
                    string hr = Towesdt.Rows[i][1].ToString();
                    string min = Towesdt.Rows[i][2].ToString() + "9";
                    DateTime ttime = Convert.ToDateTime(date + " " + hr + ":" + min);
                    DateTime ftime = ttime.AddMinutes(-10);
                    DataTable dt = db.Get_Towed_Points(vno, ftime.ToString(), ttime.ToString());
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {

                        Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                        GControl extMapType = new GControl(new ExtMapTypeControl(false, true, true, true, false));
                        GMap1.addControl(extMapType);
                        GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
                        oMarker = new Subgurim.Controles.GMarker(gLatLng);
                        SPin sPin;

                        sPin = new SPin(0.5, 0, System.Drawing.Color.Yellow, 14, PinFontStyle.bold, "");

                        icon = new Subgurim.Controles.GIcon(sPin.ToString());
                        Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
                        options.icon = icon;
                        double speed;
                        string sped;
                        speed = Convert.ToDouble(dt.Rows[j][2].ToString());
                        sped = Convert.ToString(speed * 1.85);
                        options.title = "Time :" + dt.Rows[j][5].ToString() + " -- Speed :" + sped.ToString();//report.Rows[i][3].ToString();
                        oMarker.options = options;
                        GMap1.addGMarker(oMarker);
                        latlng = new GLatLng(Convert.ToDouble(dt.Rows[j][0]), Convert.ToDouble(dt.Rows[j][1]));
                        points.Add(latlng);

                        GPolyline line = new GPolyline(points, "40FF00", 2);
                        GMap1.addPolyline(line);

                    }
                }
            }
        }
    }
}

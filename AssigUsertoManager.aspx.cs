﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace Tracking
{
    public partial class AssigUsertoManager : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {               
                ddlManagerName.Items.Add(Session["UserName"].ToString());                
                binddata();
            }
        }
        public void binddata()
        {
            ddlMapTOUsers.DataSource = db.Get_CAdminUsers(Session["UserID"].ToString(), 1);
            ddlMapTOUsers.DataBind();
            gvrecords.DataSource = db.Get_CAdminUsers(Session["UserID"].ToString(), 0);
            gvrecords.DataBind();        
        }
        protected void btMap_Click(object sender, EventArgs e)
        {
            db.Set_CAdminUsers_Status(ddlMapTOUsers.SelectedItem.Text, ddlMapTOUsers.SelectedValue.ToString(), 0);
            binddata();
        }
        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                string username = gvrecords.DataKeys[gvrow.RowIndex].Value.ToString();
                string userid = gvrow.Cells[0].Text;
                db.Set_CAdminUsers_Status(userid, username, 1);
                binddata();
            }

        }
    }
}

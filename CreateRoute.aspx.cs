﻿
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Tracking
{
    public partial class CreateRoute : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Btn_submit_Click(object sender, EventArgs e)
        {
            string rno = txtid.Text + "-" + txtrno.Text;
            string rname = txtfname.Text + "_" + txttname.Text;

            if (Session["UserRole"].ToString() == "3")
            {
                int n = db.Insert_Geo_Route(rno, rname, Session["cUser_id"].ToString(), ddlrtype.SelectedValue.ToString());
                  if (n == 0)
                  {

                      if (txtid.Text == "ROU")
                      {
                          db.Insert_Geo_Route12(rno, Session["cUser_id"].ToString(), ddlrtype.SelectedValue.ToString(), rname);
                          string rno11 = txtrno.Text;
                          Response.Redirect("Createroutebasis.aspx?rno=" + rno11);
                          fresh();
                      }
                      lblalert.Text = "Route No :" + rno + " AND Route Name :" + rname + "  Created";
                  }
            }
            else
            {

                int n = db.Insert_Geo_Route(rno, rname, Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString());
                if (n == 0)
                {

                    if (txtid.Text == "ROU")
                    {
                        db.Insert_Geo_Route12(rno, Session["UserID"].ToString(), ddlrtype.SelectedValue.ToString(), rname);
                        string rno11 = txtrno.Text;
                        Response.Redirect("Createroutebasis.aspx?rno=" + rno11);
                        fresh();
                        //Response.Redirect(Request.RawUrl);
                        //string pageurl = "Createroutebasis.aspx?rno=" + rno11;
                        //Response.Write("<script> window.open( '" + pageurl + "','_blank'); </script>");
                        //// Response.Write('<script> window.open( ‘New.aspx’,'_blank' ); </script>');
                        //Response.End();
                        //Response.Redirect(Request.RawUrl);

                        //string s = "window.open('" + pageurl + "');";//, 'popup_window','width=600,height=500,left=10,top=10,resizable=yes');";
                        //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                    }
                    lblalert.Text = "Route No :" + rno + " AND Route Name :" + rname + "  Created";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('Route No :" + rno + " AND Route Name :" + rname + " Created')", true);
                }
            }
           
            
        }
        public void fresh()
        {
            ddlrtype.SelectedValue = null;
            txtid.Enabled = true;
            txtid.Text = string.Empty;
            txtrno.Text = string.Empty;
            txtfname.Text = string.Empty;
            txttname.Text = string.Empty;
        }
        protected void ddlrtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrtype.SelectedValue.ToString() != "--")
            {
                if (ddlrtype.SelectedValue.ToString() == "0")
                {
                    txtid.Text = "TRB";
                }
                else if (ddlrtype.SelectedValue.ToString() == "1")
                {
                    txtid.Text = "DLB" + txtrno.Text;
                }
                else if (ddlrtype.SelectedValue.ToString() == "2")
                {
                    txtid.Text = "DAB" + txtrno.Text;
                }
                else if (ddlrtype.SelectedValue.ToString() == "3")
                {
                    txtid.Text = "DTB" + txtrno.Text;
                }
                else if (ddlrtype.SelectedValue.ToString() == "4")
                {
                    txtid.Text = "ROU" + txtrno.Text;
                }
                txtid.Enabled = false;
            }
        }

        protected void Btn_clr_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);

        }

        protected void txtrno_TextChanged(object sender, EventArgs e)
        {
            if (lblckeckro.Text == "Route Number Exits")
            {
                lblckeckro.Text = "";
                lblckeckro.Visible = false;

            }
            string rno = txtid.Text + "-" + txtrno.Text;
            if (Session["UserRole"].ToString() == "3")
            {
                int n = db.Check_Route_No(Session["cUser_id"].ToString(), rno);
                if (n == 1)
                {
                    lblckeckro.Visible = true;
                    lblckeckro.Text = "Route Number Exits";

                }
            }
            else
            {
                int n = db.Check_Route_No(Session["UserID"].ToString(), rno);
                if (n == 1)
                {
                    lblckeckro.Visible = true;
                    lblckeckro.Text = "Route Number Exits";

                }
            }

        }
    }
}

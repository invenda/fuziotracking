﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Subgurim.Controles;
using System.Text;
using System.Collections.Generic;
using Subgurim.Controles.GoogleChartIconMaker;


namespace Tracking
{
    public partial class dategeofence : System.Web.UI.Page
    {
 
 
        DBClass db = new DBClass();
 
 
        static double lt, lng;
        static string valid1time;
        static string valid1;
        static int p = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlMapTOVehicle.DataSource = db.GetAssingedVehicles(Session["UserID"].ToString());
                ddlMapTOVehicle.DataBind();
                gmapsettings();

            }
        }
        public void gmapsettings()
        {

            Subgurim.Controles.GLatLng gLatLng = new Subgurim.Controles.GLatLng(12.9274529, 77.590597);//GeoCode.Placemark.coordinates.lat, GeoCode.Placemark.coordinates.lng);
            GMap1.setCenter(gLatLng, 12, Subgurim.Controles.GMapType.GTypes.Normal);
            GMap1.enableHookMouseWheelToZoom = true;

        }

        protected void btView_Click(object sender, EventArgs e)
        {
            DataTable dtn = db.getgeodatenof(ddlMapTOVehicle.SelectedValue.ToString(), Session["UserID"].ToString());
            if (Convert.ToInt32(dtn.Rows[0][0]) >= 10)
            {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Maximum 10 Circles ONLY');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                
            }
            else
            {
                double lt1 = lt;
                double lng1 = lng;
                if (lt1 == 0 && lng1 == 0)
                {
#pragma warning disable CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                    Page.RegisterStartupScript("myScript", "<script language=JavaScript>alert('Please Select Point on Map');</script>");
#pragma warning restore CS0618 // 'Page.RegisterStartupScript(string, string)' is obsolete: 'The recommended alternative is ClientScript.RegisterStartupScript(Type type, string key, string script). http://go.microsoft.com/fwlink/?linkid=14202'
                }
                else
                {
                    int gnp = Convert.ToInt32(dtn.Rows[0][0]);
                    int np = 1 + gnp;
                    double radius = Convert.ToDouble(txtradius.Text) / 1609.3;
                    string dt = TextBox1.Text;
                    string Vno = ddlMapTOVehicle.SelectedValue.ToString();
                    if (Vno != "--Select Vehicle--")
                    {
                        DataTable dt1 = db.getdevicesid(Vno);
                        int did = Convert.ToInt32(dt1.Rows[0][0].ToString());
                        int uid = Convert.ToInt32(Session["UserID"].ToString());
                        db.setgeodate(lt1, lng1, radius, dt, Vno, uid, did);
                        binddata();
                        refresh();
                    }
                }

            }
        }

        protected void btrefresh_Click(object sender, EventArgs e)
        {
            binddata(); 
            refresh();
        }
        public void changed(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvrow = btn.NamingContainer as GridViewRow;
            string userid = geogv.DataKeys[gvrow.RowIndex].Value.ToString();
            int DiagResult = int.Parse(inpHide.Value);
            if (DiagResult == 1)
            {
                db.deletegeodate(userid);

            }
            binddata();
            //vehiclePresent();
        }
        public void drawcircle(double lt, double lg, double r, int gp)
        {

            var d2r = Math.PI / 180;   // degrees to radians
            var r2d = 180 / Math.PI;   // radians to degrees
            var earthsradius = 3963; // 3963 is the radius of the earth in miles
            var points = 30;
            //var radius = 10;    
            double rlat = ((double)r / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(lt * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = lg + (rlng * Math.Cos(theta));
                double ey = lt + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
#pragma warning disable CS0618 // 'GMap.addPolygon(GPolygon)' is obsolete: 'Use Add(GPolygon) instead.'
            this.GMap1.addPolygon(new GPolygon(extp, "#00FF00", 0.1));
#pragma warning restore CS0618 // 'GMap.addPolygon(GPolygon)' is obsolete: 'Use Add(GPolygon) instead.'

            Subgurim.Controles.GLatLng gLatLng1 = new Subgurim.Controles.GLatLng(lt, lg);
            GMap1.setCenter(gLatLng1, 16, Subgurim.Controles.GMapType.GTypes.Normal);
            GMap1.GZoom = 16;
            Subgurim.Controles.GMarker oMarker = new Subgurim.Controles.GMarker(gLatLng1);
            Subgurim.Controles.GMarkerOptions options = new Subgurim.Controles.GMarkerOptions();
            PinLetter pinLetter = new PinLetter(gp.ToString(), System.Drawing.Color.Red, System.Drawing.Color.Black);
            Subgurim.Controles.GIcon icon = new Subgurim.Controles.GIcon(pinLetter.ToString(), pinLetter.Shadow());
            options.icon = icon;
#pragma warning disable CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            GMap1.addGMarker(oMarker);
#pragma warning restore CS0618 // 'GMap.addGMarker(GMarker)' is obsolete: 'Use Add(GMarker) instead.'
            //options.title = vn + " Time:" + ct;
            oMarker.options = options;

        }

        protected string GMap1_Click(object s, GAjaxServerEventArgs e)
        {
            string i;
            if (p == 0)
            {
                i = "subgurim_GMap1";
                lt = e.point.lat;
                lng = e.point.lng;
                p++;
            }
            else
            {
                i = "NULL";

            }
            return new GMarker(e.point).ToString(i);

        }
        public void binddata()
        {
            gmapsettings();
            GMap1.resetPolygon();
            GMap1.resetMarkers();
            DataTable dt = db.getgeodate(ddlMapTOVehicle.SelectedValue.ToString());
            for (int i = 0, j = 0; i < dt.Rows.Count; i++)
            {
                j = 1 + i;
                drawcircle(Convert.ToDouble(dt.Rows[i][0].ToString()), Convert.ToDouble(dt.Rows[i][1].ToString()), Convert.ToDouble(dt.Rows[i][2].ToString()), j);

            }

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("ID", typeof(string));
            dt1.Columns.Add("sno", typeof(string));
            dt1.Columns.Add("loc", typeof(string));
            dt1.Columns.Add("radius", typeof(string));
            dt1.Columns.Add("date", typeof(string));
            dt1.Columns.Add("validate", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt1.NewRow();
                string sid = dt.Rows[i][6].ToString();
                int sno = i + 1;

                string sMapKey = ConfigurationManager.AppSettings["googlemaps.subgurim.net"];
                GeoCode objAddress = new GeoCode();
                objAddress = GMap.geoCodeRequest(new GLatLng(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1])), sMapKey);

                StringBuilder sb = new StringBuilder();
                string address = "";
                if (objAddress.valid)
                {
                    sb.Append(objAddress.Placemark.address.ToString());
                    address = sb.ToString();
                }
                double rad = Convert.ToDouble(dt.Rows[i][2]) * 1609.3;
                double rad12 = Math.Truncate(rad * 100) / 100;
                double rad1 = Math.Ceiling(rad12);
                DateTime dat = Convert.ToDateTime(dt.Rows[i][3]);
                dat.ToShortDateString();
                string fdate = dat.ToShortDateString() + " " + "12:00 AM";
                string tdate = dat.ToShortDateString() + " " + "11:59 PM";

                List<GLatLng> ex = points(Convert.ToDouble(dt.Rows[i][0]), Convert.ToDouble(dt.Rows[i][1]), Convert.ToDouble(dt.Rows[i][2]));
                DataTable dt2 = db.getgeotrack1(Convert.ToString(dt.Rows[i][5]), fdate, tdate);
                DateTime ftime = Convert.ToDateTime(fdate);
                DateTime ttime = Convert.ToDateTime(tdate);
                DateTime sysdt = DateTime.Now;
                if (dt2.Rows.Count == 0)
                {
                    if (sysdt < ttime)
                    {
                        valid1 = "Geo Fenced";
                    }
                    else if (sysdt > ttime)
                    {
                        valid1 = "Invalid";
                        valid1time = string.Empty;
                    }
                }
                else
                {

                    if (sysdt < ftime)
                    {
                        valid1 = "Geo Fenced";
                    }
                    else if (sysdt > ftime || sysdt > ttime)
                    {
                        for (int j = 0; j < dt2.Rows.Count; j++)
                        {
                            bool valid = IsPointInPolygon(ex, Convert.ToDouble(dt2.Rows[j][0]), Convert.ToDouble(dt2.Rows[j][1]));
                            if (valid == true)
                            {
                                valid1 = "Valid";
                                valid1time = Convert.ToString(dt2.Rows[j][2]);
                                break;
                                //validtime = Convert.ToString(dt2.Rows[i][2]);
                            }
                            else
                            {
                                valid1 = "Geo Fenced";
                                valid1time = string.Empty;
                            }
                        }
                    }

                    if (valid1 != "Valid")
                    {
                        if (sysdt > ftime || sysdt > ttime)
                        {
                            for (int m = 0, n = 1; m < dt2.Rows.Count - 1; m++, n++)
                            {
                                double mlt = (Convert.ToDouble(dt2.Rows[m][0]) + Convert.ToDouble(dt2.Rows[n][0])) / 2;
                                double mlg = (Convert.ToDouble(dt2.Rows[m][1]) + Convert.ToDouble(dt2.Rows[n][1])) / 2;
                                bool valid = IsPointInPolygon(ex, mlt, mlg);
                                if (valid == true)
                                {
                                    valid1 = "Valid";
                                    valid1time = Convert.ToString(dt2.Rows[n][2]);
                                    break;
                                }
                                else
                                {
                                    double lflt1 = (Convert.ToDouble(dt2.Rows[m][0]) + mlt) / 2;
                                    double lflg1 = (Convert.ToDouble(dt2.Rows[m][1]) + mlg) / 2;
                                    bool valid7 = IsPointInPolygon(ex, lflt1, lflg1);
                                    if (valid7 == true)
                                    {
                                        valid1 = "Valid";
                                        valid1time = Convert.ToString(dt2.Rows[n][2]);
                                        break;
                                    }
                                    else
                                    {
                                        double rlt1 = (Convert.ToDouble(dt2.Rows[n][0]) + mlt) / 2;
                                        double rlg1 = (Convert.ToDouble(dt2.Rows[n][1]) + mlg) / 2;
                                        bool valid2 = IsPointInPolygon(ex, rlt1, rlg1);
                                        if (valid2 == true)
                                        {
                                            valid1 = "Valid";
                                            valid1time = Convert.ToString(dt2.Rows[n][2]);
                                            break;
                                        }
                                        else
                                        {
                                            double lflt2 = (Convert.ToDouble(dt2.Rows[m][0]) + lflt1) / 2;
                                            double lflg2 = (Convert.ToDouble(dt2.Rows[m][1]) + lflg1) / 2;
                                            bool valid3 = IsPointInPolygon(ex, lflt2, lflg2);
                                            if (valid3 == true)
                                            {
                                                valid1 = "Valid";
                                                valid1time = Convert.ToString(dt2.Rows[n][2]);
                                                break;
                                            }
                                            else
                                            {
                                                double lflt3 = (mlt + lflt1) / 2;
                                                double lflg3 = (mlg + lflg1) / 2;
                                                bool valid4 = IsPointInPolygon(ex, lflt3, lflg3);
                                                if (valid4 == true)
                                                {
                                                    valid1 = "Valid";
                                                    valid1time = Convert.ToString(dt2.Rows[n][2]);
                                                    break;
                                                }
                                                else
                                                {
                                                    double rlt2 = (rlt1 + mlt) / 2;
                                                    double rlg2 = (rlg1 + mlg) / 2;
                                                    bool valid5 = IsPointInPolygon(ex, rlt2, rlg2);
                                                    if (valid5 == true)
                                                    {
                                                        valid1 = "Valid";
                                                        valid1time = Convert.ToString(dt2.Rows[n][2]);
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        double rlt3 = (rlt1 + Convert.ToDouble(dt2.Rows[n][0])) / 2;
                                                        double rlg3 = (rlg1 + Convert.ToDouble(dt2.Rows[n][1])) / 2;
                                                        bool valid6 = IsPointInPolygon(ex, rlt3, rlg3);
                                                        if (valid6 == true)
                                                        {
                                                            valid1 = "Valid";
                                                            valid1time = Convert.ToString(dt2.Rows[n][2]);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (sysdt > ttime)
                    {
                        if (valid1 != "Valid")
                        {
                            valid1 = "Invalid";
                        }

                    }
                }
                //DateTime oldDate = Convert.ToDateTime(dat);
                //DateTime newDate = DateTime.Parse(oldDate.ToString("dd/MM/yyyy"));
                //string datetime = newDate.ToShortDateString();
                dr["ID"] = sid;
                dr["sno"] = sno.ToString();
                dr["loc"] = address;
                dr["radius"] = rad1.ToString();
                dr["date"] = dat.ToShortDateString();
                dr["validate"] = valid1 + "  " + valid1time;
                valid1time = "";
                dt1.Rows.Add(dr);
            }
            geogv.DataSource = dt1;
            geogv.DataBind();
        }
        public List<GLatLng> points(double Cx, double Cy, double radius)
        {
            double d2r = Math.PI / 180;   // degrees to radians
            double r2d = 180 / Math.PI;   // radians to degrees
            double earthsradius = 3963; // 3963 is the radius of the earth in miles
            double points = 30;
            double rlat = ((double)radius / earthsradius) * r2d;
            double rlng = rlat / Math.Cos(Cx * d2r);
            List<GLatLng> extp = new List<GLatLng>();
            for (var i = 0; i < points + 1; i++)
            {
                double theta = Math.PI * (i / (double)(points / 2));
                double ex = Cy + (rlng * Math.Cos(theta));
                double ey = Cx + (rlat * Math.Sin(theta));
                extp.Add(new GLatLng(ey, ex));
            }
            return extp;
        }
        private static bool IsPointInPolygon(List<GLatLng> poly, double Lt, double Lg)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].lat <= Lt) && (Lt < poly[j].lat)) |
                    ((poly[j].lat <= Lt) && (Lt < poly[i].lat))) &&
                    (Lg < (poly[j].lng - poly[i].lng) * (Lt - poly[i].lat) / (poly[j].lat - poly[i].lat) + poly[i].lng))
                    c = !c;
            }
            return c;
        }
        public void refresh()
        {
            lt = 0;
            lng = 0;
            txtradius.Text = string.Empty;
            TextBox1.Text = string.Empty;
            if (p > 0)
            {
                p = 0;
            }
        }

        protected void ddlMapTOVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMapTOVehicle.SelectedValue != "--Select Vehicle--")
            {

                binddata();
            }
        }

        protected void ddlMapTOVehicle_DataBound(object sender, EventArgs e)
        {
            DropDownList list = sender as DropDownList;
            if (list != null)
                list.Items.Insert(0, "--Select Vehicle--");
        }

        protected void geogv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lb = (Label)e.Row.FindControl("lblval");
                if (lb.Text == "Invalid  ")
                {
                    Image ig = (Image)e.Row.FindControl("imgval");
                    ig.ImageUrl = "~/Images/red.jpg";

                }
                else if (lb.Text == "Geo Fenced  ")
                {
                    Image ig = (Image)e.Row.FindControl("imgval");
                    ig.ImageUrl = "~/Images/orange.JPG";
                }
                else
                {
                    Image ig = (Image)e.Row.FindControl("imgval");
                    ig.ImageUrl = "~/Images/green.jpg";
                }

            }
        }


    }
}
